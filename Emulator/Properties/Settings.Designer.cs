﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Emulator.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool MasterControllerVirt {
            get {
                return ((bool)(this["MasterControllerVirt"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool MasterControllerWork {
            get {
                return ((bool)(this["MasterControllerWork"]));
            }
            set {
                this["MasterControllerWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool InternalDoorVirt {
            get {
                return ((bool)(this["InternalDoorVirt"]));
            }
            set {
                this["InternalDoorVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool InternalDoorOpen {
            get {
                return ((bool)(this["InternalDoorOpen"]));
            }
            set {
                this["InternalDoorOpen"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ExternalDoorVirt {
            get {
                return ((bool)(this["ExternalDoorVirt"]));
            }
            set {
                this["ExternalDoorVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ExternalDoorOpen {
            get {
                return ((bool)(this["ExternalDoorOpen"]));
            }
            set {
                this["ExternalDoorOpen"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CardDispenserVirt {
            get {
                return ((bool)(this["CardDispenserVirt"]));
            }
            set {
                this["CardDispenserVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CardDispenserWork {
            get {
                return ((bool)(this["CardDispenserWork"]));
            }
            set {
                this["CardDispenserWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CardReaderVirt {
            get {
                return ((bool)(this["CardReaderVirt"]));
            }
            set {
                this["CardReaderVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CardReaderWork {
            get {
                return ((bool)(this["CardReaderWork"]));
            }
            set {
                this["CardReaderWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool KKMVirt {
            get {
                return ((bool)(this["KKMVirt"]));
            }
            set {
                this["KKMVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool KKMWork {
            get {
                return ((bool)(this["KKMWork"]));
            }
            set {
                this["KKMWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool LDCVirt {
            get {
                return ((bool)(this["LDCVirt"]));
            }
            set {
                this["LDCVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool LDCWork {
            get {
                return ((bool)(this["LDCWork"]));
            }
            set {
                this["LDCWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CashAcceptorVirt {
            get {
                return ((bool)(this["CashAcceptorVirt"]));
            }
            set {
                this["CashAcceptorVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CashAcceptorWork {
            get {
                return ((bool)(this["CashAcceptorWork"]));
            }
            set {
                this["CashAcceptorWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CashAcceptorNeeded {
            get {
                return ((bool)(this["CashAcceptorNeeded"]));
            }
            set {
                this["CashAcceptorNeeded"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CashDispenserVirt {
            get {
                return ((bool)(this["CashDispenserVirt"]));
            }
            set {
                this["CashDispenserVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CashDispenserWork {
            get {
                return ((bool)(this["CashDispenserWork"]));
            }
            set {
                this["CashDispenserWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CashDispenserNeeded {
            get {
                return ((bool)(this["CashDispenserNeeded"]));
            }
            set {
                this["CashDispenserNeeded"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CoinAcceptorVirt {
            get {
                return ((bool)(this["CoinAcceptorVirt"]));
            }
            set {
                this["CoinAcceptorVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CoinAcceptorWork {
            get {
                return ((bool)(this["CoinAcceptorWork"]));
            }
            set {
                this["CoinAcceptorWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool CoinAcceptorNeeded {
            get {
                return ((bool)(this["CoinAcceptorNeeded"]));
            }
            set {
                this["CoinAcceptorNeeded"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool HopperVirt1 {
            get {
                return ((bool)(this["HopperVirt1"]));
            }
            set {
                this["HopperVirt1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool HopperWork1 {
            get {
                return ((bool)(this["HopperWork1"]));
            }
            set {
                this["HopperWork1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool HopperNeeded1 {
            get {
                return ((bool)(this["HopperNeeded1"]));
            }
            set {
                this["HopperNeeded1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool HopperVirt2 {
            get {
                return ((bool)(this["HopperVirt2"]));
            }
            set {
                this["HopperVirt2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool HopperWork2 {
            get {
                return ((bool)(this["HopperWork2"]));
            }
            set {
                this["HopperWork2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool HopperNeeded2 {
            get {
                return ((bool)(this["HopperNeeded2"]));
            }
            set {
                this["HopperNeeded2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool BankModuleWork {
            get {
                return ((bool)(this["BankModuleWork"]));
            }
            set {
                this["BankModuleWork"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool BankModuleNeeded {
            get {
                return ((bool)(this["BankModuleNeeded"]));
            }
            set {
                this["BankModuleNeeded"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool BankModuleVirt {
            get {
                return ((bool)(this["BankModuleVirt"]));
            }
            set {
                this["BankModuleVirt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public string CashAcceptorCurrent {
            get {
                return ((string)(this["CashAcceptorCurrent"]));
            }
            set {
                this["CashAcceptorCurrent"] = value;
            }
        }
    }
}
