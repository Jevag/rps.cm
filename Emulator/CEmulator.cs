﻿using CommonClassLibrary;
using System;
using System.Collections.ObjectModel;
using System.Resources;
using System.Windows.Forms;

namespace Emulator
{
    public partial class CEmulator : UserControl
    {
        Random rnd;

        public TerminalStatusT terminalStatus = TerminalStatusT.Empty;

        public TerminalWorkT TerminalWork = TerminalWorkT.TerminalClosed;
        public PaimentMethodT Paiments = PaimentMethodT.PaimentsAll, paiments_main;
        public BankModuleStatusT BankModuleStatus = BankModuleStatusT.BankCardEmpty;

        public int PaidBanknote, PaidCoin;
        public int PaidBanknoteCurrent, PaidCoinCurrent;
        public Collection<int> PaidBanknoteList = new Collection<int>(), PaidCoinList = new Collection<int>();
        public string EntranceTimeStatus = "", AmountDueStatus = "", PaidStatus = "";
        public bool TerminalStateClosed = true;
        public Collection<string> sLog = new Collection<string>();
        public string CasseKey;
        public string dt_format;
        private ResourceManager rm = new global::System.Resources.ResourceManager("Emulator.Properties.Resources", typeof(Emulator.Properties.Resources).Assembly);

        public CEmulator()
        {
            InitializeComponent();
            rnd = new Random();
        }

        private void ToLog(string what)
        {
            sLog.Add(what);
        }

        public void InsertCard_Click(object sender, EventArgs e)
        {
            ToLog(Properties.Resources.ToLogCardInserted);
            ReaderIDCard.Text = "";
            ReaderCasseKey.Text = CasseKey;
            ReaderTariffID.Text = "";
            ClientType.SelectedIndex = 0;
            ReaderEntranceTime.Text = "";
            AmountSum.Text = "";
            CardSum.Text = "";
            PaidCoin = 0;
            PaidBanknote = 0;
            CardGotStuck.Enabled = true;
            ReleaseCard.Enabled = false;
            terminalStatus = TerminalStatusT.CardInserted;
            DateTime dt = DateTime.Now;
            int hours = rnd.Next(1, 24);
            TimeSpan ts = new TimeSpan(hours, 0, 0);
            DateTime dt1 = dt.Subtract(ts);
            ReaderEntranceTime.Text = dt1.ToString(dt_format);
            Paiments = paiments_main;
        }

        private void TakeCard_Click(object sender, EventArgs e)
        {
            CardGotStuck.Enabled = false;
            CardGotStuck.Checked = false;
            ReleaseCard.Enabled = false;
            if (terminalStatus != TerminalStatusT.Empty)
                terminalStatus = TerminalStatusT.Empty;
            Paiments = paiments_main;
        }

        private void CardGotStuck_Click(object sender, EventArgs e)
        {
            ToLog(Properties.Resources.ToLogCardReleased);
            CardGotStuck.Enabled = false;
            CardGotStuck.Checked = false;
            TakeCard.Enabled = true;
            ReleaseCard.Enabled = false;
            terminalStatus = TerminalStatusT.CardEjected;
            Paiments = paiments_main;
        }

        private void ReadData_Click(object sender, EventArgs e)
        {
            terminalStatus = TerminalStatusT.CardReaded;
        }

        void CoinAcceptorCurrentAdd()
        {
            try
            {
                PaidCoinCurrent = Convert.ToInt32(CoinAcceptorCurrent.Text);
            }
            catch
            {
                PaidCoinCurrent = 0;
            }
            PaidCoinCurrent++;
            CoinAcceptorCurrent.Text = PaidCoinCurrent.ToString();
        }

        void CashAcceptorCurrentAdd()
        {
            try
            {
                PaidBanknoteCurrent = Convert.ToInt32(CashAcceptorCurrent.Text);
            }
            catch
            {
                PaidBanknoteCurrent = 0;
            }
            PaidBanknoteCurrent++;
            CashAcceptorCurrent.Text = PaidBanknoteCurrent.ToString();
        }

        private void b10_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 10;
                PaidBanknoteList.Add(10);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 10");
            }
        }

        private void b50_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 50;
                PaidBanknoteList.Add(50);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 50");
            }
        }

        private void b100_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 100;
                PaidBanknoteList.Add(100);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 100");
            }
        }

        private void b500_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 500;
                PaidBanknoteList.Add(500);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 500");
            }
        }

        private void b1000_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 1000;
                PaidBanknoteList.Add(1000);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 1000");
            }
        }

        private void b5000_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidBanknote += 5000;
                PaidBanknoteList.Add(5000);
                CashAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCashAdd + " 5000");
            }
        }

        private void m1_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidCoin += 1;
                PaidCoinList.Add(1);
                CoinAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCoinAdd + " 1");
            }
        }

        private void m2_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidCoin += 2;
                PaidCoinList.Add(2);
                CoinAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCoinAdd + " 2");
            }
        }

        private void m5_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidCoin += 5;
                PaidCoinList.Add(5);
                CoinAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCoinAdd + " 5");
            }
        }

        private void m10_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsBankModule)
            {
                Paiments = PaimentMethodT.PaimentsCash;
                PaidCoin += 10;
                PaidCoinList.Add(10);
                CoinAcceptorCurrentAdd();
                ToLog(Properties.Resources.ToLogCoinAdd + " 10");
            }
        }

        private void ExternalDoorVirt_CheckedChanged(object sender, EventArgs e)
        {
            ExternalDoorOpen.Visible = ExternalDoorVirt.Checked;
        }

        private void InternalDoorVirt_CheckedChanged(object sender, EventArgs e)
        {
            InternalDoorOpen.Visible = InternalDoorVirt.Checked;
        }

        private void CardDispenserVirt_CheckedChanged(object sender, EventArgs e)
        {
            CardDispenserWork.Enabled = CardDispenserVirt.Checked;
        }

        private void CardGotStuck_CheckedChanged(object sender, EventArgs e)
        {
            if (CardGotStuck.Checked)
            {
                ToLog(Properties.Resources.ToLogCardGotStuck);
                ReleaseCard.Enabled = true;
                TakeCard.Enabled = false;
                terminalStatus = TerminalStatusT.CardJam;
                CardGotStuck.Enabled = false;
            }
        }

        private void CardReaderVirt_CheckedChanged(object sender, EventArgs e)
        {
            CardReaderWork.Enabled = CardReaderVirt.Checked;
        }

        private void KKMVirt_CheckedChanged(object sender, EventArgs e)
        {
            KKMWork.Enabled = KKMVirt.Checked;
        }

        private void CashAcceptorVirt_CheckedChanged(object sender, EventArgs e)
        {
            CashAcceptorWork.Enabled = CashAcceptorVirt.Checked;
        }

        private void DispenserVirt_CheckedChanged(object sender, EventArgs e)
        {
            CashDispenserWork.Enabled = CashDispenserVirt.Checked;
        }

        private void CoinAcceptorVirt_CheckedChanged(object sender, EventArgs e)
        {
            CoinAcceptorWork.Enabled = CoinAcceptorVirt.Checked;
        }

        private void HopperVirt1_CheckedChanged(object sender, EventArgs e)
        {
            HopperWork1.Enabled = HopperVirt1.Checked;
        }

        private void HopperVirt2_CheckedChanged(object sender, EventArgs e)
        {
            HopperWork2.Enabled = HopperVirt2.Checked;
        }

        private void BankModuleVirt_CheckedChanged(object sender, EventArgs e)
        {
            BankModuleWork.Enabled = BankModuleVirt.Checked;
        }


        private void BankCardInsert_Click(object sender, EventArgs e)
        {
            if (Paiments != PaimentMethodT.PaimentsCash)
            {
                Paiments = PaimentMethodT.PaimentsBankModule;
            }
            BankCardSuccess.Enabled = true;
            BankCardNotSuccess.Enabled = true;
            BankCardTake.Enabled = false;
            BankCardInsert.Enabled = false;
            BankModuleStatus = BankModuleStatusT.BankCardInserted;
            ToLog(Properties.Resources.ToLogBankCardInserted);
        }

        private void BankCardSuccess_Click(object sender, EventArgs e)
        {
            BankCardNotSuccess.Enabled = false;
            BankCardSuccess.Enabled = false;
            BankCardInsert.Enabled = false;
            BankCardTake.Enabled = true;
            BankModuleStatus = BankModuleStatusT.BankCardSuccess;
            ToLog(Properties.Resources.ToLogBankCardSuccess);
        }

        public void ReadCarNumber_Click(object sender, EventArgs e)
        {
            ReaderIDCard.Text = "";
            ReaderCasseKey.Text = CasseKey;
            ReaderTariffID.Text = "";
            ClientType.SelectedIndex = -1;
            ReaderEntranceTime.Text = "";
            AmountSum.Text = "";
            CardSum.Text = "";
            PaidCoin = 0;
            PaidBanknote = 0;
            CardGotStuck.Enabled = true;
            ReleaseCard.Enabled = false;
            terminalStatus = TerminalStatusT.CardInserted;
            Paiments = paiments_main;
        }

        private void BankCardNotSuccess_Click(object sender, EventArgs e)
        {
            BankCardNotSuccess.Enabled = false;
            BankCardSuccess.Enabled = false;
            BankCardInsert.Enabled = false;
            BankCardTake.Enabled = true;
            BankModuleStatus = BankModuleStatusT.BankCardNotSuccess;
            ToLog(Properties.Resources.ToLogBankCardNotSuccess);
        }

        private void BankCardTake_Click(object sender, EventArgs e)
        {
            BankCardNotSuccess.Enabled = false;
            BankCardSuccess.Enabled = false;
            BankCardTake.Enabled = false;
            BankCardInsert.Enabled = true;
            if (BankModuleStatus == BankModuleStatusT.BankCardSuccess)
                terminalStatus = TerminalStatusT.CardPaid;
            else if ((BankModuleStatus == BankModuleStatusT.BankCardNotSuccess) && (terminalStatus != TerminalStatusT.PenalCardPrePaiment))
                terminalStatus = TerminalStatusT.CardReaded;

            BankModuleStatus = BankModuleStatusT.BankCardEjected;
            ToLog(Properties.Resources.ToLogBankCardEjected);
            if (BankModuleEditsGroupBox.Visible)
                BankModuleEditsGroupBox.Hide();
            Paiments = PaimentMethodT.PaimentsAll;
            TakeCard.Enabled = true;
        }

        private void ReadDataError_Click(object sender, EventArgs e)
        {
            terminalStatus = TerminalStatusT.CardReadedError;
        }

        private void CoinAcceptorCurrentRead_Click(object sender, EventArgs e)
        {
            try
            {
                PaidCoinCurrent = Convert.ToInt32(CoinAcceptorCurrent.Text);
            }
            catch
            {
                PaidCoinCurrent = 0;
            }
        }

        private void CashAcceptorCurrentRead_Click(object sender, EventArgs e)
        {
            try
            {
                PaidBanknoteCurrent = Convert.ToInt32(CashAcceptorCurrent.Text);
            }
            catch
            {
                PaidBanknoteCurrent = 0;
            }
        }

        private void ExternalDoorOpen_CheckedChanged(object sender, EventArgs e)
        {
            /*if (ExternalDoorOpen.Checked)
            {
                InternalDoorGroupBox.Enabled = true;
            }
            else
            {
                InternalDoorGroupBox.Enabled = false;
            }
            */
        }

    }
}
