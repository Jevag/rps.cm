﻿namespace Emulator
{
    partial class CEmulator
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CEmulator));
            this.panel1 = new System.Windows.Forms.Panel();
            this.PrinterGroupBox = new System.Windows.Forms.Panel();
            this.PrinterWork = new System.Windows.Forms.CheckBox();
            this.PrinterVirt = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BankModuleEditsGroupBox = new System.Windows.Forms.Panel();
            this.BankCardTake = new System.Windows.Forms.Button();
            this.BankCardNotSuccess = new System.Windows.Forms.Button();
            this.BankCardSuccess = new System.Windows.Forms.Button();
            this.BankCardInsert = new System.Windows.Forms.Button();
            this.BankModuleGroupBox = new System.Windows.Forms.Panel();
            this.BankModuleNeeded = new System.Windows.Forms.CheckBox();
            this.BankModuleWork = new System.Windows.Forms.CheckBox();
            this.BankModuleVirt = new System.Windows.Forms.CheckBox();
            this.BankModuleLabel = new System.Windows.Forms.Label();
            this.HopperGroupBox2 = new System.Windows.Forms.Panel();
            this.HopperNeeded2 = new System.Windows.Forms.CheckBox();
            this.HopperWork2 = new System.Windows.Forms.CheckBox();
            this.HopperVirt2 = new System.Windows.Forms.CheckBox();
            this.HopperLabel2 = new System.Windows.Forms.Label();
            this.HopperGroupBox1 = new System.Windows.Forms.Panel();
            this.HopperNeeded1 = new System.Windows.Forms.CheckBox();
            this.HopperWork1 = new System.Windows.Forms.CheckBox();
            this.HopperVirt1 = new System.Windows.Forms.CheckBox();
            this.HopperLabel1 = new System.Windows.Forms.Label();
            this.CoinAcceptorEditsGroupBox2 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.CoinAcceptorMax = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.CoinAcceptorMaxCount = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.CoinAcceptorCurrentRead = new System.Windows.Forms.Button();
            this.CoinAcceptorCurrent = new System.Windows.Forms.MaskedTextBox();
            this.CoinAcceptorEditsGroupBox1 = new System.Windows.Forms.Panel();
            this.m6 = new System.Windows.Forms.Button();
            this.m5 = new System.Windows.Forms.Button();
            this.m4 = new System.Windows.Forms.Button();
            this.m3 = new System.Windows.Forms.Button();
            this.m2 = new System.Windows.Forms.Button();
            this.m1 = new System.Windows.Forms.Button();
            this.CoinAcceptorGroupBox = new System.Windows.Forms.Panel();
            this.CoinAcceptorNeeded = new System.Windows.Forms.CheckBox();
            this.CoinAcceptorWork = new System.Windows.Forms.CheckBox();
            this.CoinAcceptorVirt = new System.Windows.Forms.CheckBox();
            this.CoinAcceptorLabel = new System.Windows.Forms.Label();
            this.CashDispenserGroupBox = new System.Windows.Forms.Panel();
            this.CashDispenserNeeded = new System.Windows.Forms.CheckBox();
            this.CashDispenserWork = new System.Windows.Forms.CheckBox();
            this.CashDispenserVirt = new System.Windows.Forms.CheckBox();
            this.DispenserLabel = new System.Windows.Forms.Label();
            this.CashAcceptorEditsGroupBox2 = new System.Windows.Forms.Panel();
            this.label78 = new System.Windows.Forms.Label();
            this.CashAcceptorMax = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.CashAcceptorMaxCount = new System.Windows.Forms.MaskedTextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.CashAcceptorCurrentRead = new System.Windows.Forms.Button();
            this.CashAcceptorCurrent = new System.Windows.Forms.MaskedTextBox();
            this.CashAcceptorEditsGroupBox1 = new System.Windows.Forms.Panel();
            this.b5000 = new System.Windows.Forms.Button();
            this.b1000 = new System.Windows.Forms.Button();
            this.b500 = new System.Windows.Forms.Button();
            this.b100 = new System.Windows.Forms.Button();
            this.b50 = new System.Windows.Forms.Button();
            this.b10 = new System.Windows.Forms.Button();
            this.CashAcceptorGroupBox = new System.Windows.Forms.Panel();
            this.CashAcceptorNeeded = new System.Windows.Forms.CheckBox();
            this.CashAcceptorWork = new System.Windows.Forms.CheckBox();
            this.CashAcceptorVirt = new System.Windows.Forms.CheckBox();
            this.CashAcceptorLabel = new System.Windows.Forms.Label();
            this.KKMGroupBox = new System.Windows.Forms.Panel();
            this.KKMWork = new System.Windows.Forms.CheckBox();
            this.KKMVirt = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.CarNumberPanel = new System.Windows.Forms.Panel();
            this.ReadCarNumber = new System.Windows.Forms.Button();
            this.CarNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CardReaderEditsGroupBox = new System.Windows.Forms.Panel();
            this.ReaderSheduleID = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ClientType = new System.Windows.Forms.ComboBox();
            this.ReaderTariffID = new System.Windows.Forms.ComboBox();
            this.CardSum = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ReadDataError = new System.Windows.Forms.Button();
            this.ReaderEntranceTime = new System.Windows.Forms.MaskedTextBox();
            this.AmountSum = new System.Windows.Forms.MaskedTextBox();
            this.ReadData = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.ReaderCasseKey = new System.Windows.Forms.MaskedTextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.ReaderIDCard = new System.Windows.Forms.MaskedTextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.CardReaderGroupBox = new System.Windows.Forms.Panel();
            this.CardReaderWork = new System.Windows.Forms.CheckBox();
            this.CardReaderVirt = new System.Windows.Forms.CheckBox();
            this.label53 = new System.Windows.Forms.Label();
            this.CardDispenserEditsGroupBox = new System.Windows.Forms.Panel();
            this.CardGotStuck = new System.Windows.Forms.CheckBox();
            this.ReleaseCard = new System.Windows.Forms.Button();
            this.TakeCard = new System.Windows.Forms.Button();
            this.InsertCard = new System.Windows.Forms.Button();
            this.CardDispenserGroupBox = new System.Windows.Forms.Panel();
            this.CardDispenserWork = new System.Windows.Forms.CheckBox();
            this.CardDispenserVirt = new System.Windows.Forms.CheckBox();
            this.label52 = new System.Windows.Forms.Label();
            this.InternalDoorGroupBox = new System.Windows.Forms.Panel();
            this.InternalDoorOpen = new System.Windows.Forms.CheckBox();
            this.InternalDoorVirt = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.ExternalDoorGroupBox = new System.Windows.Forms.Panel();
            this.ExternalDoorOpen = new System.Windows.Forms.CheckBox();
            this.ExternalDoorVirt = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.ShiftStatusPanel = new System.Windows.Forms.Panel();
            this.ShiftStatusLabel1 = new System.Windows.Forms.Label();
            this.CasseStatusLabelPanel = new System.Windows.Forms.Panel();
            this.CasseStatusLabel1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.PrinterGroupBox.SuspendLayout();
            this.BankModuleEditsGroupBox.SuspendLayout();
            this.BankModuleGroupBox.SuspendLayout();
            this.HopperGroupBox2.SuspendLayout();
            this.HopperGroupBox1.SuspendLayout();
            this.CoinAcceptorEditsGroupBox2.SuspendLayout();
            this.CoinAcceptorEditsGroupBox1.SuspendLayout();
            this.CoinAcceptorGroupBox.SuspendLayout();
            this.CashDispenserGroupBox.SuspendLayout();
            this.CashAcceptorEditsGroupBox2.SuspendLayout();
            this.CashAcceptorEditsGroupBox1.SuspendLayout();
            this.CashAcceptorGroupBox.SuspendLayout();
            this.KKMGroupBox.SuspendLayout();
            this.CarNumberPanel.SuspendLayout();
            this.CardReaderEditsGroupBox.SuspendLayout();
            this.CardReaderGroupBox.SuspendLayout();
            this.CardDispenserEditsGroupBox.SuspendLayout();
            this.CardDispenserGroupBox.SuspendLayout();
            this.InternalDoorGroupBox.SuspendLayout();
            this.ExternalDoorGroupBox.SuspendLayout();
            this.ShiftStatusPanel.SuspendLayout();
            this.CasseStatusLabelPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.PrinterGroupBox);
            this.panel1.Controls.Add(this.BankModuleEditsGroupBox);
            this.panel1.Controls.Add(this.BankModuleGroupBox);
            this.panel1.Controls.Add(this.HopperGroupBox2);
            this.panel1.Controls.Add(this.HopperGroupBox1);
            this.panel1.Controls.Add(this.CoinAcceptorEditsGroupBox2);
            this.panel1.Controls.Add(this.CoinAcceptorEditsGroupBox1);
            this.panel1.Controls.Add(this.CoinAcceptorGroupBox);
            this.panel1.Controls.Add(this.CashDispenserGroupBox);
            this.panel1.Controls.Add(this.CashAcceptorEditsGroupBox2);
            this.panel1.Controls.Add(this.CashAcceptorEditsGroupBox1);
            this.panel1.Controls.Add(this.CashAcceptorGroupBox);
            this.panel1.Controls.Add(this.KKMGroupBox);
            this.panel1.Controls.Add(this.CarNumberPanel);
            this.panel1.Controls.Add(this.CardReaderEditsGroupBox);
            this.panel1.Controls.Add(this.CardReaderGroupBox);
            this.panel1.Controls.Add(this.CardDispenserEditsGroupBox);
            this.panel1.Controls.Add(this.CardDispenserGroupBox);
            this.panel1.Controls.Add(this.InternalDoorGroupBox);
            this.panel1.Controls.Add(this.ExternalDoorGroupBox);
            this.panel1.Controls.Add(this.ShiftStatusPanel);
            this.panel1.Controls.Add(this.CasseStatusLabelPanel);
            this.panel1.Name = "panel1";
            // 
            // PrinterGroupBox
            // 
            resources.ApplyResources(this.PrinterGroupBox, "PrinterGroupBox");
            this.PrinterGroupBox.Controls.Add(this.PrinterWork);
            this.PrinterGroupBox.Controls.Add(this.PrinterVirt);
            this.PrinterGroupBox.Controls.Add(this.label4);
            this.PrinterGroupBox.Name = "PrinterGroupBox";
            // 
            // PrinterWork
            // 
            resources.ApplyResources(this.PrinterWork, "PrinterWork");
            this.PrinterWork.Checked = global::Emulator.Properties.Settings.Default.KKMWork;
            this.PrinterWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "KKMWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PrinterWork.Name = "PrinterWork";
            this.PrinterWork.UseVisualStyleBackColor = true;
            // 
            // PrinterVirt
            // 
            resources.ApplyResources(this.PrinterVirt, "PrinterVirt");
            this.PrinterVirt.Checked = global::Emulator.Properties.Settings.Default.KKMVirt;
            this.PrinterVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "KKMVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PrinterVirt.Name = "PrinterVirt";
            this.PrinterVirt.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // BankModuleEditsGroupBox
            // 
            resources.ApplyResources(this.BankModuleEditsGroupBox, "BankModuleEditsGroupBox");
            this.BankModuleEditsGroupBox.Controls.Add(this.BankCardTake);
            this.BankModuleEditsGroupBox.Controls.Add(this.BankCardNotSuccess);
            this.BankModuleEditsGroupBox.Controls.Add(this.BankCardSuccess);
            this.BankModuleEditsGroupBox.Controls.Add(this.BankCardInsert);
            this.BankModuleEditsGroupBox.Name = "BankModuleEditsGroupBox";
            // 
            // BankCardTake
            // 
            resources.ApplyResources(this.BankCardTake, "BankCardTake");
            this.BankCardTake.Name = "BankCardTake";
            this.BankCardTake.UseVisualStyleBackColor = true;
            this.BankCardTake.Click += new System.EventHandler(this.BankCardTake_Click);
            // 
            // BankCardNotSuccess
            // 
            resources.ApplyResources(this.BankCardNotSuccess, "BankCardNotSuccess");
            this.BankCardNotSuccess.Name = "BankCardNotSuccess";
            this.BankCardNotSuccess.UseVisualStyleBackColor = true;
            this.BankCardNotSuccess.Click += new System.EventHandler(this.BankCardNotSuccess_Click);
            // 
            // BankCardSuccess
            // 
            resources.ApplyResources(this.BankCardSuccess, "BankCardSuccess");
            this.BankCardSuccess.Name = "BankCardSuccess";
            this.BankCardSuccess.UseVisualStyleBackColor = true;
            this.BankCardSuccess.Click += new System.EventHandler(this.BankCardSuccess_Click);
            // 
            // BankCardInsert
            // 
            resources.ApplyResources(this.BankCardInsert, "BankCardInsert");
            this.BankCardInsert.Name = "BankCardInsert";
            this.BankCardInsert.UseVisualStyleBackColor = true;
            this.BankCardInsert.Click += new System.EventHandler(this.BankCardInsert_Click);
            // 
            // BankModuleGroupBox
            // 
            resources.ApplyResources(this.BankModuleGroupBox, "BankModuleGroupBox");
            this.BankModuleGroupBox.Controls.Add(this.BankModuleNeeded);
            this.BankModuleGroupBox.Controls.Add(this.BankModuleWork);
            this.BankModuleGroupBox.Controls.Add(this.BankModuleVirt);
            this.BankModuleGroupBox.Controls.Add(this.BankModuleLabel);
            this.BankModuleGroupBox.Name = "BankModuleGroupBox";
            // 
            // BankModuleNeeded
            // 
            resources.ApplyResources(this.BankModuleNeeded, "BankModuleNeeded");
            this.BankModuleNeeded.Checked = global::Emulator.Properties.Settings.Default.BankModuleNeeded;
            this.BankModuleNeeded.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "BankModuleNeeded", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BankModuleNeeded.Name = "BankModuleNeeded";
            this.BankModuleNeeded.UseVisualStyleBackColor = true;
            // 
            // BankModuleWork
            // 
            resources.ApplyResources(this.BankModuleWork, "BankModuleWork");
            this.BankModuleWork.Checked = global::Emulator.Properties.Settings.Default.BankModuleWork;
            this.BankModuleWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BankModuleWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "BankModuleWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BankModuleWork.Name = "BankModuleWork";
            this.BankModuleWork.UseVisualStyleBackColor = true;
            // 
            // BankModuleVirt
            // 
            resources.ApplyResources(this.BankModuleVirt, "BankModuleVirt");
            this.BankModuleVirt.Checked = global::Emulator.Properties.Settings.Default.BankModuleVirt;
            this.BankModuleVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "BankModuleVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BankModuleVirt.Name = "BankModuleVirt";
            this.BankModuleVirt.UseVisualStyleBackColor = true;
            this.BankModuleVirt.CheckedChanged += new System.EventHandler(this.BankModuleVirt_CheckedChanged);
            // 
            // BankModuleLabel
            // 
            resources.ApplyResources(this.BankModuleLabel, "BankModuleLabel");
            this.BankModuleLabel.Name = "BankModuleLabel";
            // 
            // HopperGroupBox2
            // 
            resources.ApplyResources(this.HopperGroupBox2, "HopperGroupBox2");
            this.HopperGroupBox2.Controls.Add(this.HopperNeeded2);
            this.HopperGroupBox2.Controls.Add(this.HopperWork2);
            this.HopperGroupBox2.Controls.Add(this.HopperVirt2);
            this.HopperGroupBox2.Controls.Add(this.HopperLabel2);
            this.HopperGroupBox2.Name = "HopperGroupBox2";
            // 
            // HopperNeeded2
            // 
            resources.ApplyResources(this.HopperNeeded2, "HopperNeeded2");
            this.HopperNeeded2.Checked = global::Emulator.Properties.Settings.Default.HopperNeeded2;
            this.HopperNeeded2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperNeeded2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperNeeded2.Name = "HopperNeeded2";
            this.HopperNeeded2.UseVisualStyleBackColor = true;
            // 
            // HopperWork2
            // 
            resources.ApplyResources(this.HopperWork2, "HopperWork2");
            this.HopperWork2.Checked = global::Emulator.Properties.Settings.Default.HopperWork2;
            this.HopperWork2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.HopperWork2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperWork2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperWork2.Name = "HopperWork2";
            this.HopperWork2.UseVisualStyleBackColor = true;
            // 
            // HopperVirt2
            // 
            resources.ApplyResources(this.HopperVirt2, "HopperVirt2");
            this.HopperVirt2.Checked = global::Emulator.Properties.Settings.Default.HopperVirt2;
            this.HopperVirt2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperVirt2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperVirt2.Name = "HopperVirt2";
            this.HopperVirt2.UseVisualStyleBackColor = true;
            this.HopperVirt2.CheckedChanged += new System.EventHandler(this.HopperVirt2_CheckedChanged);
            // 
            // HopperLabel2
            // 
            resources.ApplyResources(this.HopperLabel2, "HopperLabel2");
            this.HopperLabel2.Name = "HopperLabel2";
            // 
            // HopperGroupBox1
            // 
            resources.ApplyResources(this.HopperGroupBox1, "HopperGroupBox1");
            this.HopperGroupBox1.Controls.Add(this.HopperNeeded1);
            this.HopperGroupBox1.Controls.Add(this.HopperWork1);
            this.HopperGroupBox1.Controls.Add(this.HopperVirt1);
            this.HopperGroupBox1.Controls.Add(this.HopperLabel1);
            this.HopperGroupBox1.Name = "HopperGroupBox1";
            // 
            // HopperNeeded1
            // 
            resources.ApplyResources(this.HopperNeeded1, "HopperNeeded1");
            this.HopperNeeded1.Checked = global::Emulator.Properties.Settings.Default.HopperNeeded1;
            this.HopperNeeded1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperNeeded1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperNeeded1.Name = "HopperNeeded1";
            this.HopperNeeded1.UseVisualStyleBackColor = true;
            // 
            // HopperWork1
            // 
            resources.ApplyResources(this.HopperWork1, "HopperWork1");
            this.HopperWork1.Checked = global::Emulator.Properties.Settings.Default.HopperWork1;
            this.HopperWork1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.HopperWork1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperWork1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperWork1.Name = "HopperWork1";
            this.HopperWork1.UseVisualStyleBackColor = true;
            // 
            // HopperVirt1
            // 
            resources.ApplyResources(this.HopperVirt1, "HopperVirt1");
            this.HopperVirt1.Checked = global::Emulator.Properties.Settings.Default.HopperVirt1;
            this.HopperVirt1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "HopperVirt1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HopperVirt1.Name = "HopperVirt1";
            this.HopperVirt1.UseVisualStyleBackColor = true;
            this.HopperVirt1.CheckedChanged += new System.EventHandler(this.HopperVirt1_CheckedChanged);
            // 
            // HopperLabel1
            // 
            resources.ApplyResources(this.HopperLabel1, "HopperLabel1");
            this.HopperLabel1.Name = "HopperLabel1";
            // 
            // CoinAcceptorEditsGroupBox2
            // 
            resources.ApplyResources(this.CoinAcceptorEditsGroupBox2, "CoinAcceptorEditsGroupBox2");
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.label64);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.CoinAcceptorMax);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.label65);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.CoinAcceptorMaxCount);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.label66);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.CoinAcceptorCurrentRead);
            this.CoinAcceptorEditsGroupBox2.Controls.Add(this.CoinAcceptorCurrent);
            this.CoinAcceptorEditsGroupBox2.Name = "CoinAcceptorEditsGroupBox2";
            // 
            // label64
            // 
            resources.ApplyResources(this.label64, "label64");
            this.label64.Name = "label64";
            // 
            // CoinAcceptorMax
            // 
            resources.ApplyResources(this.CoinAcceptorMax, "CoinAcceptorMax");
            this.CoinAcceptorMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CoinAcceptorMax.Name = "CoinAcceptorMax";
            this.CoinAcceptorMax.ReadOnly = true;
            // 
            // label65
            // 
            resources.ApplyResources(this.label65, "label65");
            this.label65.Name = "label65";
            // 
            // CoinAcceptorMaxCount
            // 
            resources.ApplyResources(this.CoinAcceptorMaxCount, "CoinAcceptorMaxCount");
            this.CoinAcceptorMaxCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CoinAcceptorMaxCount.Name = "CoinAcceptorMaxCount";
            this.CoinAcceptorMaxCount.ReadOnly = true;
            // 
            // label66
            // 
            resources.ApplyResources(this.label66, "label66");
            this.label66.Name = "label66";
            // 
            // CoinAcceptorCurrentRead
            // 
            resources.ApplyResources(this.CoinAcceptorCurrentRead, "CoinAcceptorCurrentRead");
            this.CoinAcceptorCurrentRead.Name = "CoinAcceptorCurrentRead";
            this.CoinAcceptorCurrentRead.UseVisualStyleBackColor = true;
            this.CoinAcceptorCurrentRead.Click += new System.EventHandler(this.CoinAcceptorCurrentRead_Click);
            // 
            // CoinAcceptorCurrent
            // 
            resources.ApplyResources(this.CoinAcceptorCurrent, "CoinAcceptorCurrent");
            this.CoinAcceptorCurrent.Name = "CoinAcceptorCurrent";
            // 
            // CoinAcceptorEditsGroupBox1
            // 
            resources.ApplyResources(this.CoinAcceptorEditsGroupBox1, "CoinAcceptorEditsGroupBox1");
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m6);
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m5);
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m4);
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m3);
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m2);
            this.CoinAcceptorEditsGroupBox1.Controls.Add(this.m1);
            this.CoinAcceptorEditsGroupBox1.Name = "CoinAcceptorEditsGroupBox1";
            // 
            // m6
            // 
            resources.ApplyResources(this.m6, "m6");
            this.m6.Name = "m6";
            this.m6.UseVisualStyleBackColor = true;
            // 
            // m5
            // 
            resources.ApplyResources(this.m5, "m5");
            this.m5.Name = "m5";
            this.m5.UseVisualStyleBackColor = true;
            // 
            // m4
            // 
            resources.ApplyResources(this.m4, "m4");
            this.m4.Name = "m4";
            this.m4.UseVisualStyleBackColor = true;
            this.m4.Click += new System.EventHandler(this.m10_Click);
            // 
            // m3
            // 
            resources.ApplyResources(this.m3, "m3");
            this.m3.Name = "m3";
            this.m3.UseVisualStyleBackColor = true;
            this.m3.Click += new System.EventHandler(this.m5_Click);
            // 
            // m2
            // 
            resources.ApplyResources(this.m2, "m2");
            this.m2.Name = "m2";
            this.m2.UseVisualStyleBackColor = true;
            this.m2.Click += new System.EventHandler(this.m2_Click);
            // 
            // m1
            // 
            resources.ApplyResources(this.m1, "m1");
            this.m1.Name = "m1";
            this.m1.UseVisualStyleBackColor = true;
            this.m1.Click += new System.EventHandler(this.m1_Click);
            // 
            // CoinAcceptorGroupBox
            // 
            resources.ApplyResources(this.CoinAcceptorGroupBox, "CoinAcceptorGroupBox");
            this.CoinAcceptorGroupBox.Controls.Add(this.CoinAcceptorNeeded);
            this.CoinAcceptorGroupBox.Controls.Add(this.CoinAcceptorWork);
            this.CoinAcceptorGroupBox.Controls.Add(this.CoinAcceptorVirt);
            this.CoinAcceptorGroupBox.Controls.Add(this.CoinAcceptorLabel);
            this.CoinAcceptorGroupBox.Name = "CoinAcceptorGroupBox";
            // 
            // CoinAcceptorNeeded
            // 
            resources.ApplyResources(this.CoinAcceptorNeeded, "CoinAcceptorNeeded");
            this.CoinAcceptorNeeded.Checked = global::Emulator.Properties.Settings.Default.CoinAcceptorNeeded;
            this.CoinAcceptorNeeded.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CoinAcceptorNeeded", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CoinAcceptorNeeded.Name = "CoinAcceptorNeeded";
            this.CoinAcceptorNeeded.UseVisualStyleBackColor = true;
            // 
            // CoinAcceptorWork
            // 
            resources.ApplyResources(this.CoinAcceptorWork, "CoinAcceptorWork");
            this.CoinAcceptorWork.Checked = global::Emulator.Properties.Settings.Default.CoinAcceptorWork;
            this.CoinAcceptorWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CoinAcceptorWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CoinAcceptorWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CoinAcceptorWork.Name = "CoinAcceptorWork";
            this.CoinAcceptorWork.UseVisualStyleBackColor = true;
            // 
            // CoinAcceptorVirt
            // 
            resources.ApplyResources(this.CoinAcceptorVirt, "CoinAcceptorVirt");
            this.CoinAcceptorVirt.Checked = global::Emulator.Properties.Settings.Default.CoinAcceptorVirt;
            this.CoinAcceptorVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CoinAcceptorVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CoinAcceptorVirt.Name = "CoinAcceptorVirt";
            this.CoinAcceptorVirt.UseVisualStyleBackColor = true;
            this.CoinAcceptorVirt.CheckedChanged += new System.EventHandler(this.CoinAcceptorVirt_CheckedChanged);
            // 
            // CoinAcceptorLabel
            // 
            resources.ApplyResources(this.CoinAcceptorLabel, "CoinAcceptorLabel");
            this.CoinAcceptorLabel.Name = "CoinAcceptorLabel";
            // 
            // CashDispenserGroupBox
            // 
            resources.ApplyResources(this.CashDispenserGroupBox, "CashDispenserGroupBox");
            this.CashDispenserGroupBox.Controls.Add(this.CashDispenserNeeded);
            this.CashDispenserGroupBox.Controls.Add(this.CashDispenserWork);
            this.CashDispenserGroupBox.Controls.Add(this.CashDispenserVirt);
            this.CashDispenserGroupBox.Controls.Add(this.DispenserLabel);
            this.CashDispenserGroupBox.Name = "CashDispenserGroupBox";
            // 
            // CashDispenserNeeded
            // 
            resources.ApplyResources(this.CashDispenserNeeded, "CashDispenserNeeded");
            this.CashDispenserNeeded.Checked = global::Emulator.Properties.Settings.Default.CashDispenserNeeded;
            this.CashDispenserNeeded.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashDispenserNeeded", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashDispenserNeeded.Name = "CashDispenserNeeded";
            this.CashDispenserNeeded.UseVisualStyleBackColor = true;
            // 
            // CashDispenserWork
            // 
            resources.ApplyResources(this.CashDispenserWork, "CashDispenserWork");
            this.CashDispenserWork.Checked = global::Emulator.Properties.Settings.Default.CashDispenserWork;
            this.CashDispenserWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CashDispenserWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashDispenserWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashDispenserWork.Name = "CashDispenserWork";
            this.CashDispenserWork.UseVisualStyleBackColor = true;
            // 
            // CashDispenserVirt
            // 
            resources.ApplyResources(this.CashDispenserVirt, "CashDispenserVirt");
            this.CashDispenserVirt.Checked = global::Emulator.Properties.Settings.Default.CashDispenserVirt;
            this.CashDispenserVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashDispenserVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashDispenserVirt.Name = "CashDispenserVirt";
            this.CashDispenserVirt.UseVisualStyleBackColor = true;
            this.CashDispenserVirt.CheckedChanged += new System.EventHandler(this.DispenserVirt_CheckedChanged);
            // 
            // DispenserLabel
            // 
            resources.ApplyResources(this.DispenserLabel, "DispenserLabel");
            this.DispenserLabel.Name = "DispenserLabel";
            // 
            // CashAcceptorEditsGroupBox2
            // 
            resources.ApplyResources(this.CashAcceptorEditsGroupBox2, "CashAcceptorEditsGroupBox2");
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.label78);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.CashAcceptorMax);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.label79);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.CashAcceptorMaxCount);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.label81);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.CashAcceptorCurrentRead);
            this.CashAcceptorEditsGroupBox2.Controls.Add(this.CashAcceptorCurrent);
            this.CashAcceptorEditsGroupBox2.Name = "CashAcceptorEditsGroupBox2";
            // 
            // label78
            // 
            resources.ApplyResources(this.label78, "label78");
            this.label78.Name = "label78";
            // 
            // CashAcceptorMax
            // 
            resources.ApplyResources(this.CashAcceptorMax, "CashAcceptorMax");
            this.CashAcceptorMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CashAcceptorMax.Name = "CashAcceptorMax";
            this.CashAcceptorMax.ReadOnly = true;
            // 
            // label79
            // 
            resources.ApplyResources(this.label79, "label79");
            this.label79.Name = "label79";
            // 
            // CashAcceptorMaxCount
            // 
            resources.ApplyResources(this.CashAcceptorMaxCount, "CashAcceptorMaxCount");
            this.CashAcceptorMaxCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CashAcceptorMaxCount.Name = "CashAcceptorMaxCount";
            this.CashAcceptorMaxCount.ReadOnly = true;
            // 
            // label81
            // 
            resources.ApplyResources(this.label81, "label81");
            this.label81.Name = "label81";
            // 
            // CashAcceptorCurrentRead
            // 
            resources.ApplyResources(this.CashAcceptorCurrentRead, "CashAcceptorCurrentRead");
            this.CashAcceptorCurrentRead.Name = "CashAcceptorCurrentRead";
            this.CashAcceptorCurrentRead.UseVisualStyleBackColor = true;
            this.CashAcceptorCurrentRead.Click += new System.EventHandler(this.CashAcceptorCurrentRead_Click);
            // 
            // CashAcceptorCurrent
            // 
            resources.ApplyResources(this.CashAcceptorCurrent, "CashAcceptorCurrent");
            this.CashAcceptorCurrent.Name = "CashAcceptorCurrent";
            // 
            // CashAcceptorEditsGroupBox1
            // 
            resources.ApplyResources(this.CashAcceptorEditsGroupBox1, "CashAcceptorEditsGroupBox1");
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b5000);
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b1000);
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b500);
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b100);
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b50);
            this.CashAcceptorEditsGroupBox1.Controls.Add(this.b10);
            this.CashAcceptorEditsGroupBox1.Name = "CashAcceptorEditsGroupBox1";
            // 
            // b5000
            // 
            resources.ApplyResources(this.b5000, "b5000");
            this.b5000.Name = "b5000";
            this.b5000.UseVisualStyleBackColor = true;
            this.b5000.Click += new System.EventHandler(this.b5000_Click);
            // 
            // b1000
            // 
            resources.ApplyResources(this.b1000, "b1000");
            this.b1000.Name = "b1000";
            this.b1000.UseVisualStyleBackColor = true;
            this.b1000.Click += new System.EventHandler(this.b1000_Click);
            // 
            // b500
            // 
            resources.ApplyResources(this.b500, "b500");
            this.b500.Name = "b500";
            this.b500.UseVisualStyleBackColor = true;
            this.b500.Click += new System.EventHandler(this.b500_Click);
            // 
            // b100
            // 
            resources.ApplyResources(this.b100, "b100");
            this.b100.Name = "b100";
            this.b100.UseVisualStyleBackColor = true;
            this.b100.Click += new System.EventHandler(this.b100_Click);
            // 
            // b50
            // 
            resources.ApplyResources(this.b50, "b50");
            this.b50.Name = "b50";
            this.b50.UseVisualStyleBackColor = true;
            this.b50.Click += new System.EventHandler(this.b50_Click);
            // 
            // b10
            // 
            resources.ApplyResources(this.b10, "b10");
            this.b10.BackColor = System.Drawing.SystemColors.Control;
            this.b10.Name = "b10";
            this.b10.UseVisualStyleBackColor = false;
            this.b10.Click += new System.EventHandler(this.b10_Click);
            // 
            // CashAcceptorGroupBox
            // 
            resources.ApplyResources(this.CashAcceptorGroupBox, "CashAcceptorGroupBox");
            this.CashAcceptorGroupBox.Controls.Add(this.CashAcceptorNeeded);
            this.CashAcceptorGroupBox.Controls.Add(this.CashAcceptorWork);
            this.CashAcceptorGroupBox.Controls.Add(this.CashAcceptorVirt);
            this.CashAcceptorGroupBox.Controls.Add(this.CashAcceptorLabel);
            this.CashAcceptorGroupBox.Name = "CashAcceptorGroupBox";
            // 
            // CashAcceptorNeeded
            // 
            resources.ApplyResources(this.CashAcceptorNeeded, "CashAcceptorNeeded");
            this.CashAcceptorNeeded.Checked = global::Emulator.Properties.Settings.Default.CashAcceptorNeeded;
            this.CashAcceptorNeeded.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashAcceptorNeeded", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashAcceptorNeeded.Name = "CashAcceptorNeeded";
            this.CashAcceptorNeeded.UseVisualStyleBackColor = true;
            // 
            // CashAcceptorWork
            // 
            resources.ApplyResources(this.CashAcceptorWork, "CashAcceptorWork");
            this.CashAcceptorWork.Checked = global::Emulator.Properties.Settings.Default.CashAcceptorWork;
            this.CashAcceptorWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CashAcceptorWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashAcceptorWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashAcceptorWork.Name = "CashAcceptorWork";
            this.CashAcceptorWork.UseVisualStyleBackColor = true;
            // 
            // CashAcceptorVirt
            // 
            resources.ApplyResources(this.CashAcceptorVirt, "CashAcceptorVirt");
            this.CashAcceptorVirt.Checked = global::Emulator.Properties.Settings.Default.CashAcceptorVirt;
            this.CashAcceptorVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CashAcceptorVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CashAcceptorVirt.Name = "CashAcceptorVirt";
            this.CashAcceptorVirt.UseVisualStyleBackColor = true;
            this.CashAcceptorVirt.CheckedChanged += new System.EventHandler(this.CashAcceptorVirt_CheckedChanged);
            // 
            // CashAcceptorLabel
            // 
            resources.ApplyResources(this.CashAcceptorLabel, "CashAcceptorLabel");
            this.CashAcceptorLabel.Name = "CashAcceptorLabel";
            // 
            // KKMGroupBox
            // 
            resources.ApplyResources(this.KKMGroupBox, "KKMGroupBox");
            this.KKMGroupBox.Controls.Add(this.KKMWork);
            this.KKMGroupBox.Controls.Add(this.KKMVirt);
            this.KKMGroupBox.Controls.Add(this.label63);
            this.KKMGroupBox.Name = "KKMGroupBox";
            // 
            // KKMWork
            // 
            resources.ApplyResources(this.KKMWork, "KKMWork");
            this.KKMWork.Checked = global::Emulator.Properties.Settings.Default.KKMWork;
            this.KKMWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.KKMWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "KKMWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.KKMWork.Name = "KKMWork";
            this.KKMWork.UseVisualStyleBackColor = true;
            // 
            // KKMVirt
            // 
            resources.ApplyResources(this.KKMVirt, "KKMVirt");
            this.KKMVirt.Checked = global::Emulator.Properties.Settings.Default.KKMVirt;
            this.KKMVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "KKMVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.KKMVirt.Name = "KKMVirt";
            this.KKMVirt.UseVisualStyleBackColor = true;
            this.KKMVirt.CheckedChanged += new System.EventHandler(this.KKMVirt_CheckedChanged);
            // 
            // label63
            // 
            resources.ApplyResources(this.label63, "label63");
            this.label63.Name = "label63";
            // 
            // CarNumberPanel
            // 
            resources.ApplyResources(this.CarNumberPanel, "CarNumberPanel");
            this.CarNumberPanel.Controls.Add(this.ReadCarNumber);
            this.CarNumberPanel.Controls.Add(this.CarNumber);
            this.CarNumberPanel.Controls.Add(this.label2);
            this.CarNumberPanel.Name = "CarNumberPanel";
            // 
            // ReadCarNumber
            // 
            resources.ApplyResources(this.ReadCarNumber, "ReadCarNumber");
            this.ReadCarNumber.Name = "ReadCarNumber";
            this.ReadCarNumber.UseVisualStyleBackColor = true;
            this.ReadCarNumber.Click += new System.EventHandler(this.ReadCarNumber_Click);
            // 
            // CarNumber
            // 
            resources.ApplyResources(this.CarNumber, "CarNumber");
            this.CarNumber.Name = "CarNumber";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // CardReaderEditsGroupBox
            // 
            resources.ApplyResources(this.CardReaderEditsGroupBox, "CardReaderEditsGroupBox");
            this.CardReaderEditsGroupBox.Controls.Add(this.ReaderSheduleID);
            this.CardReaderEditsGroupBox.Controls.Add(this.label3);
            this.CardReaderEditsGroupBox.Controls.Add(this.ClientType);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReaderTariffID);
            this.CardReaderEditsGroupBox.Controls.Add(this.CardSum);
            this.CardReaderEditsGroupBox.Controls.Add(this.label1);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReadDataError);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReaderEntranceTime);
            this.CardReaderEditsGroupBox.Controls.Add(this.AmountSum);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReadData);
            this.CardReaderEditsGroupBox.Controls.Add(this.label60);
            this.CardReaderEditsGroupBox.Controls.Add(this.label59);
            this.CardReaderEditsGroupBox.Controls.Add(this.label58);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReaderCasseKey);
            this.CardReaderEditsGroupBox.Controls.Add(this.label57);
            this.CardReaderEditsGroupBox.Controls.Add(this.ReaderIDCard);
            this.CardReaderEditsGroupBox.Controls.Add(this.label56);
            this.CardReaderEditsGroupBox.Name = "CardReaderEditsGroupBox";
            // 
            // ReaderSheduleID
            // 
            resources.ApplyResources(this.ReaderSheduleID, "ReaderSheduleID");
            this.ReaderSheduleID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ReaderSheduleID.FormattingEnabled = true;
            this.ReaderSheduleID.Name = "ReaderSheduleID";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // ClientType
            // 
            resources.ApplyResources(this.ClientType, "ClientType");
            this.ClientType.FormattingEnabled = true;
            this.ClientType.Items.AddRange(new object[] {
            resources.GetString("ClientType.Items"),
            resources.GetString("ClientType.Items1"),
            resources.GetString("ClientType.Items2")});
            this.ClientType.Name = "ClientType";
            // 
            // ReaderTariffID
            // 
            resources.ApplyResources(this.ReaderTariffID, "ReaderTariffID");
            this.ReaderTariffID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ReaderTariffID.FormattingEnabled = true;
            this.ReaderTariffID.Name = "ReaderTariffID";
            // 
            // CardSum
            // 
            resources.ApplyResources(this.CardSum, "CardSum");
            this.CardSum.Name = "CardSum";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // ReadDataError
            // 
            resources.ApplyResources(this.ReadDataError, "ReadDataError");
            this.ReadDataError.Name = "ReadDataError";
            this.ReadDataError.UseVisualStyleBackColor = true;
            this.ReadDataError.Click += new System.EventHandler(this.ReadDataError_Click);
            // 
            // ReaderEntranceTime
            // 
            resources.ApplyResources(this.ReaderEntranceTime, "ReaderEntranceTime");
            this.ReaderEntranceTime.Name = "ReaderEntranceTime";
            this.ReaderEntranceTime.ValidatingType = typeof(System.DateTime);
            // 
            // AmountSum
            // 
            resources.ApplyResources(this.AmountSum, "AmountSum");
            this.AmountSum.Name = "AmountSum";
            // 
            // ReadData
            // 
            resources.ApplyResources(this.ReadData, "ReadData");
            this.ReadData.Name = "ReadData";
            this.ReadData.UseVisualStyleBackColor = true;
            this.ReadData.Click += new System.EventHandler(this.ReadData_Click);
            // 
            // label60
            // 
            resources.ApplyResources(this.label60, "label60");
            this.label60.Name = "label60";
            // 
            // label59
            // 
            resources.ApplyResources(this.label59, "label59");
            this.label59.Name = "label59";
            // 
            // label58
            // 
            resources.ApplyResources(this.label58, "label58");
            this.label58.Name = "label58";
            // 
            // ReaderCasseKey
            // 
            resources.ApplyResources(this.ReaderCasseKey, "ReaderCasseKey");
            this.ReaderCasseKey.Name = "ReaderCasseKey";
            // 
            // label57
            // 
            resources.ApplyResources(this.label57, "label57");
            this.label57.Name = "label57";
            // 
            // ReaderIDCard
            // 
            resources.ApplyResources(this.ReaderIDCard, "ReaderIDCard");
            this.ReaderIDCard.Name = "ReaderIDCard";
            // 
            // label56
            // 
            resources.ApplyResources(this.label56, "label56");
            this.label56.Name = "label56";
            // 
            // CardReaderGroupBox
            // 
            resources.ApplyResources(this.CardReaderGroupBox, "CardReaderGroupBox");
            this.CardReaderGroupBox.Controls.Add(this.CardReaderWork);
            this.CardReaderGroupBox.Controls.Add(this.CardReaderVirt);
            this.CardReaderGroupBox.Controls.Add(this.label53);
            this.CardReaderGroupBox.Name = "CardReaderGroupBox";
            // 
            // CardReaderWork
            // 
            resources.ApplyResources(this.CardReaderWork, "CardReaderWork");
            this.CardReaderWork.Checked = global::Emulator.Properties.Settings.Default.CardReaderWork;
            this.CardReaderWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CardReaderWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CardReaderWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CardReaderWork.Name = "CardReaderWork";
            this.CardReaderWork.UseVisualStyleBackColor = true;
            // 
            // CardReaderVirt
            // 
            resources.ApplyResources(this.CardReaderVirt, "CardReaderVirt");
            this.CardReaderVirt.Checked = global::Emulator.Properties.Settings.Default.CardReaderVirt;
            this.CardReaderVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CardReaderVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CardReaderVirt.Name = "CardReaderVirt";
            this.CardReaderVirt.UseVisualStyleBackColor = true;
            this.CardReaderVirt.CheckedChanged += new System.EventHandler(this.CardReaderVirt_CheckedChanged);
            // 
            // label53
            // 
            resources.ApplyResources(this.label53, "label53");
            this.label53.Name = "label53";
            // 
            // CardDispenserEditsGroupBox
            // 
            resources.ApplyResources(this.CardDispenserEditsGroupBox, "CardDispenserEditsGroupBox");
            this.CardDispenserEditsGroupBox.Controls.Add(this.CardGotStuck);
            this.CardDispenserEditsGroupBox.Controls.Add(this.ReleaseCard);
            this.CardDispenserEditsGroupBox.Controls.Add(this.TakeCard);
            this.CardDispenserEditsGroupBox.Controls.Add(this.InsertCard);
            this.CardDispenserEditsGroupBox.Name = "CardDispenserEditsGroupBox";
            // 
            // CardGotStuck
            // 
            resources.ApplyResources(this.CardGotStuck, "CardGotStuck");
            this.CardGotStuck.Name = "CardGotStuck";
            this.CardGotStuck.UseVisualStyleBackColor = true;
            this.CardGotStuck.CheckedChanged += new System.EventHandler(this.CardGotStuck_CheckedChanged);
            // 
            // ReleaseCard
            // 
            resources.ApplyResources(this.ReleaseCard, "ReleaseCard");
            this.ReleaseCard.Name = "ReleaseCard";
            this.ReleaseCard.UseVisualStyleBackColor = true;
            this.ReleaseCard.Click += new System.EventHandler(this.CardGotStuck_Click);
            // 
            // TakeCard
            // 
            resources.ApplyResources(this.TakeCard, "TakeCard");
            this.TakeCard.Name = "TakeCard";
            this.TakeCard.UseVisualStyleBackColor = true;
            this.TakeCard.Click += new System.EventHandler(this.TakeCard_Click);
            // 
            // InsertCard
            // 
            resources.ApplyResources(this.InsertCard, "InsertCard");
            this.InsertCard.Name = "InsertCard";
            this.InsertCard.UseVisualStyleBackColor = true;
            this.InsertCard.Click += new System.EventHandler(this.InsertCard_Click);
            // 
            // CardDispenserGroupBox
            // 
            resources.ApplyResources(this.CardDispenserGroupBox, "CardDispenserGroupBox");
            this.CardDispenserGroupBox.Controls.Add(this.CardDispenserWork);
            this.CardDispenserGroupBox.Controls.Add(this.CardDispenserVirt);
            this.CardDispenserGroupBox.Controls.Add(this.label52);
            this.CardDispenserGroupBox.Name = "CardDispenserGroupBox";
            // 
            // CardDispenserWork
            // 
            resources.ApplyResources(this.CardDispenserWork, "CardDispenserWork");
            this.CardDispenserWork.Checked = global::Emulator.Properties.Settings.Default.CardDispenserWork;
            this.CardDispenserWork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CardDispenserWork.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CardDispenserWork", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CardDispenserWork.Name = "CardDispenserWork";
            this.CardDispenserWork.UseVisualStyleBackColor = true;
            // 
            // CardDispenserVirt
            // 
            resources.ApplyResources(this.CardDispenserVirt, "CardDispenserVirt");
            this.CardDispenserVirt.Checked = global::Emulator.Properties.Settings.Default.CardDispenserVirt;
            this.CardDispenserVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "CardDispenserVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CardDispenserVirt.Name = "CardDispenserVirt";
            this.CardDispenserVirt.UseVisualStyleBackColor = true;
            this.CardDispenserVirt.CheckedChanged += new System.EventHandler(this.CardDispenserVirt_CheckedChanged);
            // 
            // label52
            // 
            resources.ApplyResources(this.label52, "label52");
            this.label52.Name = "label52";
            // 
            // InternalDoorGroupBox
            // 
            resources.ApplyResources(this.InternalDoorGroupBox, "InternalDoorGroupBox");
            this.InternalDoorGroupBox.Controls.Add(this.InternalDoorOpen);
            this.InternalDoorGroupBox.Controls.Add(this.InternalDoorVirt);
            this.InternalDoorGroupBox.Controls.Add(this.label55);
            this.InternalDoorGroupBox.Name = "InternalDoorGroupBox";
            // 
            // InternalDoorOpen
            // 
            resources.ApplyResources(this.InternalDoorOpen, "InternalDoorOpen");
            this.InternalDoorOpen.Checked = global::Emulator.Properties.Settings.Default.InternalDoorOpen;
            this.InternalDoorOpen.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "InternalDoorOpen", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.InternalDoorOpen.Name = "InternalDoorOpen";
            this.InternalDoorOpen.UseVisualStyleBackColor = true;
            // 
            // InternalDoorVirt
            // 
            resources.ApplyResources(this.InternalDoorVirt, "InternalDoorVirt");
            this.InternalDoorVirt.Checked = global::Emulator.Properties.Settings.Default.InternalDoorVirt;
            this.InternalDoorVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "InternalDoorVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.InternalDoorVirt.Name = "InternalDoorVirt";
            this.InternalDoorVirt.UseVisualStyleBackColor = true;
            this.InternalDoorVirt.CheckedChanged += new System.EventHandler(this.InternalDoorVirt_CheckedChanged);
            // 
            // label55
            // 
            resources.ApplyResources(this.label55, "label55");
            this.label55.Name = "label55";
            // 
            // ExternalDoorGroupBox
            // 
            resources.ApplyResources(this.ExternalDoorGroupBox, "ExternalDoorGroupBox");
            this.ExternalDoorGroupBox.Controls.Add(this.ExternalDoorOpen);
            this.ExternalDoorGroupBox.Controls.Add(this.ExternalDoorVirt);
            this.ExternalDoorGroupBox.Controls.Add(this.label54);
            this.ExternalDoorGroupBox.Name = "ExternalDoorGroupBox";
            // 
            // ExternalDoorOpen
            // 
            resources.ApplyResources(this.ExternalDoorOpen, "ExternalDoorOpen");
            this.ExternalDoorOpen.Checked = global::Emulator.Properties.Settings.Default.ExternalDoorOpen;
            this.ExternalDoorOpen.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "ExternalDoorOpen", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExternalDoorOpen.Name = "ExternalDoorOpen";
            this.ExternalDoorOpen.UseVisualStyleBackColor = true;
            this.ExternalDoorOpen.CheckedChanged += new System.EventHandler(this.ExternalDoorOpen_CheckedChanged);
            // 
            // ExternalDoorVirt
            // 
            resources.ApplyResources(this.ExternalDoorVirt, "ExternalDoorVirt");
            this.ExternalDoorVirt.Checked = global::Emulator.Properties.Settings.Default.ExternalDoorVirt;
            this.ExternalDoorVirt.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::Emulator.Properties.Settings.Default, "ExternalDoorVirt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExternalDoorVirt.Name = "ExternalDoorVirt";
            this.ExternalDoorVirt.UseVisualStyleBackColor = true;
            this.ExternalDoorVirt.CheckedChanged += new System.EventHandler(this.ExternalDoorVirt_CheckedChanged);
            // 
            // label54
            // 
            resources.ApplyResources(this.label54, "label54");
            this.label54.Name = "label54";
            // 
            // ShiftStatusPanel
            // 
            resources.ApplyResources(this.ShiftStatusPanel, "ShiftStatusPanel");
            this.ShiftStatusPanel.Controls.Add(this.ShiftStatusLabel1);
            this.ShiftStatusPanel.Name = "ShiftStatusPanel";
            // 
            // ShiftStatusLabel1
            // 
            resources.ApplyResources(this.ShiftStatusLabel1, "ShiftStatusLabel1");
            this.ShiftStatusLabel1.Name = "ShiftStatusLabel1";
            // 
            // CasseStatusLabelPanel
            // 
            resources.ApplyResources(this.CasseStatusLabelPanel, "CasseStatusLabelPanel");
            this.CasseStatusLabelPanel.Controls.Add(this.CasseStatusLabel1);
            this.CasseStatusLabelPanel.Name = "CasseStatusLabelPanel";
            // 
            // CasseStatusLabel1
            // 
            resources.ApplyResources(this.CasseStatusLabel1, "CasseStatusLabel1");
            this.CasseStatusLabel1.Name = "CasseStatusLabel1";
            // 
            // CEmulator
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "CEmulator";
            this.panel1.ResumeLayout(false);
            this.PrinterGroupBox.ResumeLayout(false);
            this.PrinterGroupBox.PerformLayout();
            this.BankModuleEditsGroupBox.ResumeLayout(false);
            this.BankModuleGroupBox.ResumeLayout(false);
            this.BankModuleGroupBox.PerformLayout();
            this.HopperGroupBox2.ResumeLayout(false);
            this.HopperGroupBox2.PerformLayout();
            this.HopperGroupBox1.ResumeLayout(false);
            this.HopperGroupBox1.PerformLayout();
            this.CoinAcceptorEditsGroupBox2.ResumeLayout(false);
            this.CoinAcceptorEditsGroupBox2.PerformLayout();
            this.CoinAcceptorEditsGroupBox1.ResumeLayout(false);
            this.CoinAcceptorGroupBox.ResumeLayout(false);
            this.CoinAcceptorGroupBox.PerformLayout();
            this.CashDispenserGroupBox.ResumeLayout(false);
            this.CashDispenserGroupBox.PerformLayout();
            this.CashAcceptorEditsGroupBox2.ResumeLayout(false);
            this.CashAcceptorEditsGroupBox2.PerformLayout();
            this.CashAcceptorEditsGroupBox1.ResumeLayout(false);
            this.CashAcceptorGroupBox.ResumeLayout(false);
            this.CashAcceptorGroupBox.PerformLayout();
            this.KKMGroupBox.ResumeLayout(false);
            this.KKMGroupBox.PerformLayout();
            this.CarNumberPanel.ResumeLayout(false);
            this.CarNumberPanel.PerformLayout();
            this.CardReaderEditsGroupBox.ResumeLayout(false);
            this.CardReaderEditsGroupBox.PerformLayout();
            this.CardReaderGroupBox.ResumeLayout(false);
            this.CardReaderGroupBox.PerformLayout();
            this.CardDispenserEditsGroupBox.ResumeLayout(false);
            this.CardDispenserEditsGroupBox.PerformLayout();
            this.CardDispenserGroupBox.ResumeLayout(false);
            this.CardDispenserGroupBox.PerformLayout();
            this.InternalDoorGroupBox.ResumeLayout(false);
            this.InternalDoorGroupBox.PerformLayout();
            this.ExternalDoorGroupBox.ResumeLayout(false);
            this.ExternalDoorGroupBox.PerformLayout();
            this.ShiftStatusPanel.ResumeLayout(false);
            this.CasseStatusLabelPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel BankModuleEditsGroupBox;
        public System.Windows.Forms.Button BankCardTake;
        public System.Windows.Forms.Button BankCardNotSuccess;
        public System.Windows.Forms.Button BankCardSuccess;
        public System.Windows.Forms.Button BankCardInsert;
        public System.Windows.Forms.Panel BankModuleGroupBox;
        public System.Windows.Forms.CheckBox BankModuleNeeded;
        public System.Windows.Forms.CheckBox BankModuleWork;
        public System.Windows.Forms.CheckBox BankModuleVirt;
        public System.Windows.Forms.Label BankModuleLabel;
        public System.Windows.Forms.Panel HopperGroupBox2;
        public System.Windows.Forms.CheckBox HopperNeeded2;
        public System.Windows.Forms.CheckBox HopperWork2;
        public System.Windows.Forms.CheckBox HopperVirt2;
        public System.Windows.Forms.Label HopperLabel2;
        public System.Windows.Forms.Panel HopperGroupBox1;
        public System.Windows.Forms.CheckBox HopperNeeded1;
        public System.Windows.Forms.CheckBox HopperWork1;
        public System.Windows.Forms.CheckBox HopperVirt1;
        public System.Windows.Forms.Panel CoinAcceptorEditsGroupBox2;
        public System.Windows.Forms.Label label64;
        public System.Windows.Forms.Label label65;
        public System.Windows.Forms.Label label66;
        public System.Windows.Forms.MaskedTextBox CoinAcceptorCurrent;
        public System.Windows.Forms.Panel CoinAcceptorEditsGroupBox1;
        public System.Windows.Forms.Button m4;
        public System.Windows.Forms.Button m3;
        public System.Windows.Forms.Button m2;
        public System.Windows.Forms.Button m1;
        public System.Windows.Forms.Panel CoinAcceptorGroupBox;
        public System.Windows.Forms.CheckBox CoinAcceptorNeeded;
        public System.Windows.Forms.CheckBox CoinAcceptorWork;
        public System.Windows.Forms.CheckBox CoinAcceptorVirt;
        public System.Windows.Forms.Label CoinAcceptorLabel;
        public System.Windows.Forms.Panel CashDispenserGroupBox;
        public System.Windows.Forms.CheckBox CashDispenserNeeded;
        public System.Windows.Forms.CheckBox CashDispenserWork;
        public System.Windows.Forms.CheckBox CashDispenserVirt;
        public System.Windows.Forms.Label DispenserLabel;
        public System.Windows.Forms.Panel CashAcceptorEditsGroupBox2;
        public System.Windows.Forms.Label label78;
        public System.Windows.Forms.Label label79;
        public System.Windows.Forms.MaskedTextBox CashAcceptorMaxCount;
        public System.Windows.Forms.Label label81;
        public System.Windows.Forms.MaskedTextBox CashAcceptorCurrent;
        public System.Windows.Forms.Panel CashAcceptorEditsGroupBox1;
        public System.Windows.Forms.Button b5000;
        public System.Windows.Forms.Button b1000;
        public System.Windows.Forms.Button b500;
        public System.Windows.Forms.Button b100;
        public System.Windows.Forms.Button b50;
        public System.Windows.Forms.Button b10;
        public System.Windows.Forms.Panel CashAcceptorGroupBox;
        public System.Windows.Forms.CheckBox CashAcceptorNeeded;
        public System.Windows.Forms.CheckBox CashAcceptorWork;
        public System.Windows.Forms.CheckBox CashAcceptorVirt;
        public System.Windows.Forms.Label CashAcceptorLabel;
        public System.Windows.Forms.Panel KKMGroupBox;
        public System.Windows.Forms.CheckBox KKMWork;
        public System.Windows.Forms.CheckBox KKMVirt;
        public System.Windows.Forms.Label label63;
        public System.Windows.Forms.Panel CardReaderEditsGroupBox;
        public System.Windows.Forms.MaskedTextBox ReaderEntranceTime;
        public System.Windows.Forms.MaskedTextBox AmountSum;
        public System.Windows.Forms.Button ReadData;
        public System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label label58;
        public System.Windows.Forms.MaskedTextBox ReaderCasseKey;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.MaskedTextBox ReaderIDCard;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.Panel CardReaderGroupBox;
        public System.Windows.Forms.CheckBox CardReaderWork;
        public System.Windows.Forms.CheckBox CardReaderVirt;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Panel CardDispenserEditsGroupBox;
        public System.Windows.Forms.CheckBox CardGotStuck;
        public System.Windows.Forms.Button ReleaseCard;
        public System.Windows.Forms.Button TakeCard;
        public System.Windows.Forms.Button InsertCard;
        public System.Windows.Forms.CheckBox CardDispenserWork;
        public System.Windows.Forms.CheckBox CardDispenserVirt;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.CheckBox InternalDoorOpen;
        public System.Windows.Forms.CheckBox InternalDoorVirt;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Panel ExternalDoorGroupBox;
        public System.Windows.Forms.CheckBox ExternalDoorOpen;
        public System.Windows.Forms.CheckBox ExternalDoorVirt;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Panel ShiftStatusPanel;
        public System.Windows.Forms.Label ShiftStatusLabel1;
        public System.Windows.Forms.Button ReadDataError;
        public System.Windows.Forms.TextBox CashAcceptorMax;
        public System.Windows.Forms.TextBox CoinAcceptorMax;
        public System.Windows.Forms.TextBox CoinAcceptorMaxCount;
        public System.Windows.Forms.Button CoinAcceptorCurrentRead;
        public System.Windows.Forms.Button CashAcceptorCurrentRead;
        public System.Windows.Forms.MaskedTextBox CardSum;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel InternalDoorGroupBox;
        public System.Windows.Forms.Panel CasseStatusLabelPanel;
        public System.Windows.Forms.Label CasseStatusLabel1;
        public System.Windows.Forms.Label HopperLabel1;
        public System.Windows.Forms.Panel CardDispenserGroupBox;
        public System.Windows.Forms.Panel CarNumberPanel;
        public System.Windows.Forms.TextBox CarNumber;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button ReadCarNumber;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ComboBox ReaderTariffID;
        public System.Windows.Forms.ComboBox ClientType;
        public System.Windows.Forms.ComboBox ReaderSheduleID;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Panel PrinterGroupBox;
        public System.Windows.Forms.CheckBox PrinterWork;
        public System.Windows.Forms.CheckBox PrinterVirt;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button m6;
        public System.Windows.Forms.Button m5;
    }
}
