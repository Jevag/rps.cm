﻿using System;
using System.Windows.Controls;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для Calc_Back.xaml
    /// </summary>
    public partial class Calc_Back : Button
	{
        public event EventHandler OnButtonClick;
        public Calc_Back()
		{
			this.InitializeComponent();
		}
        private void ButtonClicked(object sender, EventArgs e)
        {
            OnButtonClick?.Invoke(this, e);
        }
    }
}