﻿using System.Windows;
using System.Windows.Controls;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для ButtonBar.xaml
    /// </summary>
    public partial class ButtonBar : UserControl
    {
        public delegate void RoutedEvent(object sender, RoutedEventArgs e);
        public event RoutedEvent OnButtonClick;

        public ButtonBar()
        {
            this.InitializeComponent();
        }

        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            FrameworkElement feSource = e.Source as FrameworkElement;
            switch (feSource.Name)
            {
                case "OperatorControl":
                    // do something here ...
                    break;
                case "LangControl":
                    // do something ...
                    break;
                case "CancelControl":
                    // do something ...
                    break;
            }
            if (OnButtonClick != null)
                OnButtonClick(sender, e);
            e.Handled = true;
        }
    }
}