﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CashierWpfControlLibrary.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CashierWpfControlLibrary.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I want to transfer bigger sum from my credit card.
        /// </summary>
        internal static string AddingSumRequest {
            get {
                return ResourceManager.GetString("AddingSumRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string AvailableLanguages {
            get {
                return ResourceManager.GetString("AvailableLanguages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string CancelButton {
            get {
                return ResourceManager.GetString("CancelButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cashier work.
        /// </summary>
        internal static string CashierWork {
            get {
                return ResourceManager.GetString("CashierWork", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EN.
        /// </summary>
        internal static string CurLang {
            get {
                return ResourceManager.GetString("CurLang", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string DeafultLanguage {
            get {
                return ResourceManager.GetString("DeafultLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Language.
        /// </summary>
        internal static string LangLabel {
            get {
                return ResourceManager.GetString("LangLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Call the operator.
        /// </summary>
        internal static string OperatorTalk {
            get {
                return ResourceManager.GetString("OperatorTalk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to pay for penalty card.
        /// </summary>
        internal static string PenalButton {
            get {
                return ResourceManager.GetString("PenalButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lost your paring card?.
        /// </summary>
        internal static string PenalPanelLabel {
            get {
                return ResourceManager.GetString("PenalPanelLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string PopUpCancel {
            get {
                return ResourceManager.GetString("PopUpCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Complete.
        /// </summary>
        internal static string PopUpComplete {
            get {
                return ResourceManager.GetString("PopUpComplete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select COM-Port.
        /// </summary>
        internal static string PopUpComPortsLabel {
            get {
                return ResourceManager.GetString("PopUpComPortsLabel", resourceCulture);
            }
        }
    }
}
