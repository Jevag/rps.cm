﻿using System;
using System.Windows.Controls;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для Calc_OK.xaml
    /// </summary>
    public partial class Calc_OK : Button
	{
        public event EventHandler OnButtonClick;
        public Calc_OK()
		{
			this.InitializeComponent();
		}
        private void ButtonClicked(object sender, EventArgs e)
        {
            OnButtonClick?.Invoke(this, e);
        }
    }
}