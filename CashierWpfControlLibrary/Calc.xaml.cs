﻿using System;
using System.Windows;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для Calc.xaml
    /// </summary>
    public partial class Calc : Window
    {
        //public delegate void EventHandler(object sender, EventArgs args);
        public string Sum;
        public int Len
        {
            set
            {
                SummText.MaxLength = value + 1;
            }
        }
        public Calc()
        {
            this.InitializeComponent();

            // Вставьте ниже код, необходимый для создания объекта.
        }

        private void CalcBody_Loaded(object sender, RoutedEventArgs e)
        {
            SummText.Text = "_";
        }

        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement feSource = e.Source as FrameworkElement;
                switch (feSource.Name)
                {
                    case "btn_OK":
                        //ToDo: Вызов оператора
                        Sum = SummText.Text.Substring(0, SummText.Text.Length - 1);
                        DialogResult = true;
                        Close();
                        break;
                    case "btn_Back":
                        if (SummText.Text.Length > 1)
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 2) + "_";
                        break;
                    case "btn_1":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "1_";
                        break;
                    case "btn_2":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "2_";
                        break;
                    case "btn_3":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "3_";
                        break;
                    case "btn_4":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "4_";
                        break;
                    case "btn_5":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "5_";
                        break;
                    case "btn_6":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "6_";
                        break;
                    case "btn_7":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "7_";
                        break;
                    case "btn_8":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "8_";
                        break;
                    case "btn_9":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "9_";
                        break;
                    case "btn_0":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "0_";
                        break;
                    case "btn_Dot":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "._";
                        break;
                    case "btn_Minus":
                        if ((SummText.MaxLength > 0) && (SummText.Text.Length < SummText.MaxLength))
                            SummText.Text = SummText.Text.Substring(0, SummText.Text.Length - 1) + "-_";
                        break;
                }
            }
            catch
            {

            }
        }
    }
}