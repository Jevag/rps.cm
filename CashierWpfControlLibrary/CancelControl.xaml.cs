﻿using System.Windows.Controls;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для CancelControl.xaml
    /// </summary>
    public partial class CancelControl : Button
    {

        public CancelControl()
        {
            this.InitializeComponent();
        }
    }
}