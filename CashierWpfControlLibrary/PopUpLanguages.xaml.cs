﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для PopUpLanguages.xaml
    /// </summary>
    public partial class PopUpLanguages : Window
    {
        public int SelectedIndex;
        public PopUpLanguages()
        {
            InitializeComponent();

        }
        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            StackPanel sp;
            RadioButton rb;
            try
            {
                FrameworkElement feSource = e.Source as FrameworkElement;
                switch (feSource.Name)
                {
                    case "PopUpComplete":
                        for (int i = 0; i < ItemsList.Items.Count; i++)
                        {
                            sp = (StackPanel)ItemsList.Items[i];
                            rb = (RadioButton)(sp.Children[1]);
                            if ((bool)rb.IsChecked)
                                SelectedIndex = i;
                        }
                        DialogResult = true;
                        Close();
                        break;
                    case "PopUpCancel":
                        DialogResult = false;
                        Close();
                        break;
                }
            }
            catch
            {

            }
        }

        public void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < ItemsList.Items.Count; i++)
                {
                    RadioButton rb;
                    StackPanel sp;
                    sp = (StackPanel)ItemsList.Items[i];
                    rb = (RadioButton)sp.Children[1];
                    switch (sender.GetType().ToString())
                    {
                        case "System.Windows.Controls.RadioButton":
                            if (((RadioButton)sender).Parent == sp)
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = true;
                                SelectedIndex = i;

                            }
                            else
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = false;
                            }
                            break;
                        case "System.Windows.Controls.Image":
                            if (((Image)sender).Parent == sp)
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = true;
                                SelectedIndex = i;

                            }
                            else
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = false;
                            }
                            break;
                        case "System.Windows.Controls.StackPanel":
                            if (sender == sp)
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = true;
                                SelectedIndex = i;

                            }
                            else
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = false;
                            }
                            break;
                        case "System.Windows.Controls.ListBox":
                            if (((ListBox)sender).SelectedIndex == i)
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = true;
                                SelectedIndex = i;
                            }
                            else
                            {
                                ((RadioButton)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = false;
                            }
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                //cashierForm.ToLog(0, exc.ToString());
            }

        }

    }
}
