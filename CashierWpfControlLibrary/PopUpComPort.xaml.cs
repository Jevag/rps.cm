﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для PopUpComPort.xaml
    /// </summary>
    public partial class PopUpComPort : Window
    {
        public int SelectedIndex;
        public PopUpComPort()
        {
            InitializeComponent();
        }
        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement feSource = e.Source as FrameworkElement;
                switch (feSource.Name)
                {
                    case "PopUpComplete":
                        for (int i = 0; i < ItemsList.Items.Count; i++)
                            if ((bool)((RadioButton)ItemsList.Items[i]).IsChecked)
                                SelectedIndex = i;
                        DialogResult = true;
                        Close();
                        break;
                    case "PopUpCancel":
                        DialogResult = false;
                        Close();
                        break;
                }
            }
            catch
            {

            }
        }
        public void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < ItemsList.Items.Count; i++)
                {
                    RadioButton rb;
                    rb = (RadioButton)ItemsList.Items[i];
                    switch (sender.GetType().ToString())
                    {
                        case "System.Windows.Controls.RadioButton":
                            if (((RadioButton)sender) == rb)
                            {
                                ((RadioButton)ItemsList.Items[i]).IsChecked = true;
                                SelectedIndex = i;
                                ItemsList.SelectedIndex = i;
                            }
                            else
                            {
                                ((RadioButton)ItemsList.Items[i]).IsChecked = false;
                            }
                            break;
                        case "System.Windows.Controls.ListBox":
                            if (((ListBox)sender).SelectedIndex == i)
                            {
                                ((RadioButton)ItemsList.Items[i]).IsChecked = true;
                                SelectedIndex = i;
                            }
                            else
                            {
                                ((RadioButton)ItemsList.Items[i]).IsChecked = false;
                            }
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                //cashierForm.ToLog(0, exc.ToString());
            }

        }

    }
}
