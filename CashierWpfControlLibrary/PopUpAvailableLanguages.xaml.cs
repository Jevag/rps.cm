﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для PopUpLanguages.xaml
    /// </summary>
    public partial class PopUpAvailableLanguages : Window
    {
        public PopUpAvailableLanguages()
        {
            InitializeComponent();
        }
        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                FrameworkElement feSource = e.Source as FrameworkElement;
                switch (feSource.Name)
                {
                    case "PopUpComplete":
                        DialogResult = true;
                        Close();
                        break;
                    case "PopUpCancel":
                        DialogResult = false;
                        Close();
                        break;
                }
            }
            catch
            {

            }
        }

        public void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < ItemsList.Items.Count; i++)
                {
                    CheckBox rb;
                    StackPanel sp;
                    sp = (StackPanel)ItemsList.Items[i];
                    rb = (CheckBox)sp.Children[1];
                    switch (sender.GetType().ToString())
                    {
                        case "System.Windows.Controls.CheckBox":
                            if (((CheckBox)sender).Parent == sp)
                            {
                                if (ItemsList.SelectedIndex != i)
                                {
                                    ((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = !((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked;
                                    ItemsList.SelectedIndex = i;
                                }
                            }
                            break;
                        case "System.Windows.Controls.Image":
                            if (((Image)sender).Parent == sp)
                            {
                                if (ItemsList.SelectedIndex != i)
                                {
                                    ((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = !((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked;
                                    ItemsList.SelectedIndex = i;
                                }
                            }
                            break;
                        case "System.Windows.Controls.StackPanel":
                            //if (sender == sp)
                            //{
                            //    ((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = !((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked;
                            //}
                            break;
                        case "System.Windows.Controls.ListBox":
                            if (((ListBox)sender).SelectedIndex == i)
                            {
                                ((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked = !((CheckBox)((StackPanel)ItemsList.Items[i]).Children[1]).IsChecked;
                                ItemsList.SelectedIndex = -1;
                            }
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                //cashierForm.ToLog(0, exc.ToString());
            }
        }
    }
}
