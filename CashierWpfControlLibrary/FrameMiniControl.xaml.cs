﻿using System;
using System.Windows.Controls;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для FrameMini.xaml
    /// </summary>
    public partial class FrameMini : Button
	{
        //public delegate void EventHandler(object sender);
        public event EventHandler OnButtonClick;
        public FrameMini()
		{
			this.InitializeComponent();
		}
        private void ButtonClicked(object sender, EventArgs e)
        {
            OnButtonClick?.Invoke(this, e);
        }
    }
}