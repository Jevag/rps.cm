﻿using System.Windows.Controls;
using System.Windows.Media;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для CEndPayment.xaml
    /// </summary>
    public partial class CButton : Button
    {
        public CButton()
        {
            InitializeComponent();
        }

        private void Button_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (IsEnabled) Rec.Opacity = 1;
            else Rec.Opacity = 0.5;
        }
    }
}
