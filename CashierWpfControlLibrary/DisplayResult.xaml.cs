﻿using System;
using System.Windows;

namespace CashierWpfControlLibrary
{
    /// <summary>
    /// Логика взаимодействия для DisplayResult.xaml
    /// </summary>
    public partial class DisplayResultT : Window
	{
        //public delegate void EventHandler(object sender, EventArgs args);
        public string TextBox
        {
            set
            {
                textBox.Text = value;
            }
        }
        public DisplayResultT()
		{
			this.InitializeComponent();
			
			// Вставьте ниже код, необходимый для создания объекта.
		}

        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
            FrameworkElement feSource = e.Source as FrameworkElement;
            switch (feSource.Name)
            {
                case "btn_OK":
                Close();
                    break;
            }
            }
            catch 
            {
                
            }
        }
    }
}