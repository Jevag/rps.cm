﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CashierWpfControlLibrary;
using CommonClassLibrary;
using Infralution.Localization.Wpf;
using Rps.ShareBase;
using SqlBaseImport.Model;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class TechFormWPF : Window
    {
        #region Variables
        MainWindow cashierForm;
        public System.Windows.Forms.Timer ExternalDeviceTimer = new System.Windows.Forms.Timer();

        public int CashAcceptorModelSelectedIndex = 0;
        int CashAcceptorPortSelectedIndex = 0;
        public int CashDispenserModelSelectedIndex = 0;
        int CashDispenserPortSelectedIndex = 0;
        int CoinHopperModelSelectedIndex = 0;
        int CoinHopper1PortSelectedIndex = 0;
        //int CoinHopper2PortSelectedIndex = 0;
        int BankModuleModelSelectedIndex = 0;
        int BankModulePortSelectedIndex = 0;
        int CoinAcceptorModelSelectedIndex = 0;
        int CoinAcceptorPortSelectedIndex = 0;
        int KKMModelSelectedIndex = 0;
        int KKMPortSelectedIndex = 0;
        int CashierTypeSelectedIndex = 0;
        int ChequeTypesSelectedIndex = 0;
        int CardDispenserModelSelectedIndex = 0;
        int CardDispenserPortSelectedIndex = 0;
        int CardReaderModelSelectedIndex = 0;
        int CardReaderPortSelectedIndex = 0;
        int SlaveModelSelectedIndex = 0;
        int SlavePortSelectedIndex = 0;
        int PrinterModelSelectedIndex = 0;
        int PrinterPortSelectedIndex = 0;
        public int PenalCardTPSelectedIndex = 0;
        public int PenalCardTSSelectedIndex = 0;
        public int ServerExchangeProtocolSelectedIndex = 0;
        public int DefaultLanguageSelectedIndex = 0;
        public int ZoneComboBoxSelectedIndex = 0;
        public int GroupComboBoxSelectedIndex = 0;
        int VATIndex = 0;
        public bool WindowClosing = false;

        public List<string> AvailableLanguagesList = new List<string>();
        #endregion Variables

        public TechFormWPF(MainWindow MainForm)
        {
            InitializeComponent();
            cashierForm = MainForm;
            TechPanel.Visibility = Visibility.Visible;
            TechButtonsPanel.Visibility = Visibility.Hidden;
            ShiftPanel.Visibility = Visibility.Hidden;
            DeliveryPanel.Visibility = Visibility.Hidden;
            CashAcceptorSettings.Visibility = Visibility.Hidden;
            CoinAcceptorSettings.Visibility = Visibility.Hidden;
            CashDispenserSettings.Visibility = Visibility.Hidden;
            CoinHopperSettings.Visibility = Visibility.Hidden;
            BankModuleSettings.Visibility = Visibility.Hidden;
            KKMSettings.Visibility = Visibility.Hidden;
            OtherSettings.Visibility = Visibility.Hidden;
            FinanceGroup.Visibility = Visibility.Hidden;
            TimeOutsGroup.Visibility = Visibility.Hidden;
            InterfacesGroup.Visibility = Visibility.Hidden;
            ButtonsGroup.Visibility = Visibility.Hidden;
            CardDispenserGroup.Visibility = Visibility.Visible;
            CardReaderGroup.Visibility = Visibility.Hidden;
            SlaveGroup.Visibility = Visibility.Hidden;
            PrinterGroup.Visibility = Visibility.Hidden;
            PopUpShadow.Visibility = Visibility.Hidden;
            MainGroup.Visibility = Visibility.Visible;

            ExternalDeviceTimer.Enabled = false;
            ExternalDeviceTimer.Interval = 100;
            ExternalDeviceTimer.Tick += ExternalDeviceTimer_Tick;
        }
        public void ExternalDeviceTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                lock (cashierForm.ConsoleLog)
                {
                    if (cashierForm != null && cashierForm.ConsoleLogChanged /*&& !ConsoleLog.IsFocused*/)
                    {
                        string text = "";
                        foreach (string s in cashierForm.ConsoleLog)
                            text += s + Environment.NewLine;
                        ConsoleLog.Text = text;
                        cashierForm.ConsoleLogChanged = false;
                    }
                }

                lock (cashierForm.ErrorsTextLast)
                {
                    if (cashierForm != null && Visibility == Visibility.Visible && cashierForm.ErrorsTextChanged)
                    {
                        ErrorsText.Text = cashierForm.ErrorsTextLast;
                        cashierForm.ErrorsTextChanged = false;
                    }
                }

                ShiftModeButton.IsChecked = cashierForm.ShiftOpened;
                ShiftButton.IsChecked = cashierForm.ShiftOpened;

                if (cashierForm.ShiftOpened)
                {
                    if (cashierForm.DefaultCulture != null)
                    {
                        if (ShiftState.Text != cashierForm.ResourceManager.GetString("ShiftOpened", cashierForm.DefaultCulture))
                            ShiftState.Text = cashierForm.ResourceManager.GetString("ShiftOpened", cashierForm.DefaultCulture);
                        if ((string)ShiftModeButton.Content != cashierForm.ResourceManager.GetString("CloseShift", cashierForm.DefaultCulture))
                            ShiftModeButton.Content = cashierForm.ResourceManager.GetString("CloseShift", cashierForm.DefaultCulture);
                    }
                    else
                    {
                        ShiftState.Text = Properties.Resources.ShiftOpened;
                        ShiftModeButton.Content = Properties.Resources.CloseShift;
                    }
                    if (ShiftStateRed.Visibility != Visibility.Hidden)
                        ShiftStateRed.Visibility = Visibility.Hidden;
                    if (ShiftStateGreen.Visibility != Visibility.Visible)
                        ShiftStateGreen.Visibility = Visibility.Visible;
                    if (Block_windows.Visibility != Visibility.Visible)
                        Block_windows.Visibility = Visibility.Visible;
                    if (cashierForm.KKMModel == "PrimFAKKM")
                    {
                        if (Block_windows1.Visibility != Visibility.Hidden)
                            Block_windows1.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (Block_windows1.Visibility != Visibility.Visible)
                            Block_windows1.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    if (cashierForm.DefaultCulture != null)
                    {
                        if (ShiftState.Text != cashierForm.ResourceManager.GetString("ShiftClosed", cashierForm.DefaultCulture))
                            ShiftState.Text = cashierForm.ResourceManager.GetString("ShiftClosed", cashierForm.DefaultCulture);
                        if ((string)ShiftModeButton.Content != cashierForm.ResourceManager.GetString("OpenShift", cashierForm.DefaultCulture))
                            ShiftModeButton.Content = cashierForm.ResourceManager.GetString("OpenShift", cashierForm.DefaultCulture);
                    }
                    else
                    {
                        if (ShiftState.Text != Properties.Resources.ShiftClosed)
                            ShiftState.Text = Properties.Resources.ShiftClosed;
                        if ((string)ShiftModeButton.Content != Properties.Resources.OpenShift)
                            ShiftModeButton.Content = Properties.Resources.OpenShift;
                    }
                    if (ShiftStateRed.Visibility != Visibility.Visible)
                        ShiftStateRed.Visibility = Visibility.Visible;
                    if (ShiftStateGreen.Visibility != Visibility.Hidden)
                        ShiftStateGreen.Visibility = Visibility.Hidden;
                    if (Block_windows.Visibility != Visibility.Hidden)
                        Block_windows.Visibility = Visibility.Hidden;
                    if (cashierForm.KKMModel == "PrimFAKKM")
                    {
                        if (Block_windows1.Visibility != Visibility.Visible)
                            Block_windows1.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (Block_windows1.Visibility != Visibility.Hidden)
                            Block_windows1.Visibility = Visibility.Hidden;

                    }
                }
                if (ShiftModeButtonDisableTime != DateTime.MinValue && ShiftModeButtonDisableTime.AddSeconds(10) >= DateTime.Now)
                {
                    ShiftModeButtonDisableTime = DateTime.MinValue;
                    if (!ShiftModeButton.IsEnabled)
                        ShiftModeButton.IsEnabled = true;
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: Window_Closing");
            WindowClosing = true;
            base.OnClosed(e);
            if (cashierForm != null && !cashierForm.WindowClosing)
            {
                cashierForm.Close();
            }
        }

        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            /*if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: CommonClickHandler");*/
            try
            {
                //Calc AddingSummForm;
                int v;
                FrameworkElement feSource = e.Source as FrameworkElement;

                switch (feSource.Name)
                {
                    case "DetailLog":
                        if (cashierForm != null)
                            cashierForm.DetailLog = (bool)DetailLog.IsChecked;
                            //DetailLog.IsChecked = !DetailLog.IsChecked;
                        break;

                    case "ShiftButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogShiftButton);
                        ShiftButton_Click(sender, e);
                        break;
                    case "DeliveryButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogDeliveryButton);
                        DeliveryButton_Click(sender, e);
                        break;
                    case "SettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogSettingsButton);
                        SettingsButton_Click(sender, e);
                        break;
                    case "TestingButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogTestingButton);
                        TestingButton_Click(sender, e);
                        break;
                    case "CashAcceptorButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashAcceptorButton);
                        CashAcceptorButton_Click(sender, e);
                        break;
                    case "CashDispenserButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogDispenserButton);
                        CashDispenserButton_Click(sender, e);
                        break;
                    case "BankModuleButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogBankModuleButton);
                        BankModuleButton_Click(sender, e);
                        break;
                    case "OtherDevicesButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherDevicesButton);
                        OtherDevicesButton_Click(sender, e);
                        break;
                    case "OtherSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherSettingsButton);
                        OtherSettingsButton_Click(sender, e);
                        break;
                    case "CoinAcceptorButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCoinAcceptorButton);
                        CoinAcceptorButton_Click(sender, e);
                        break;
                    case "CoinHopperButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogHopperButton);
                        CoinHopperButton_Click(sender, e);
                        break;
                    case "KKMButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogKKMButton);
                        KKMButton_Click(sender, e);
                        break;
                    case "SettingsPanelReturnButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogSettingsReturn);
                        TechReturnButtons();
                        break;
                    case "ShiftModeButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogShiftModeButton);
                        if (!cashierForm.ShiftOpened)
                        {
                            Block_windows.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            Block_windows.Visibility = Visibility.Hidden;
                        }
                        ShiftModeButton_Click();
                        ShiftModeButton.IsChecked = cashierForm.ShiftOpened;
                        break;
                    case "ShiftPanelReturnButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogShiftReturn);
                        ShiftButton.IsChecked = cashierForm.ShiftOpened;
                        TechReturnButtons();
                        break;
                    case "DeliveryPanelReturnButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogDeliveryReturn);
                        DeliveryReturnButton_Click(sender, e);
                        TechReturnButtons();
                        break;
                    case "CashAcceptorSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashAcceptorCancel);
                        CashAcceptorCancelButton_Click(sender, e);
                        break;
                    case "CashAcceptorSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashAcceptorSave);
                        CashAcceptorSaveButton_Click(sender, e);
                        break;
                    case "CoinAcceptorSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCoinAcceptorCancel);
                        CoinAcceptorCancelButton_Click(sender, e);
                        break;
                    case "CoinAcceptorSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCoinAcceptorSave);
                        CoinAcceptorSaveButton_Click(sender, e);
                        break;
                    case "CashDispenserReset":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashDispenserReset);
                        CashDispenserReset_Click(sender, e);
                        break;
                    case "CashAcceptorReset":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashAcceptorReset);
                        CashAcceptorReset_Click(sender, e);
                        break;
                    case "CashDispenserSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashDispenserCancel);
                        CashDispenserCancelButton_Click(sender, e);
                        break;
                    case "CashDispenserSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashDispenserSave);
                        CashDispenserSaveButton_Click(sender, e);
                        break;
                    case "CoinHopperSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogHopperCancel);
                        CoinHopperCancelButton_Click(sender, e);
                        break;
                    case "CoinHopperSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogHopperSave);
                        CoinHopperSaveButton_Click(sender, e);
                        break;
                    case "BankModuleSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogBMCancel);
                        BankModuleCancelButton_Click(sender, e);
                        break;
                    case "BankModuleSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogBMSave);
                        BankModuleSaveButton_Click(sender, e);
                        break;
                    case "KKMSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogKKMCancel);
                        KKMCancelButton_Click(sender, e);
                        break;
                    case "KKMSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogKKMSave);
                        KKMSaveButton_Click(sender, e);
                        break;
                    case "OtherDevicesSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherDevicesCancel);
                        OtherDevicesCancelButton_Click(sender, e);
                        break;
                    case "OtherDevicesSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherDevicesSave);
                        OtherDevicesSaveButton_Click(sender, e);
                        break;
                    case "OtherSettingsReturn":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherSettingsCancel);
                        OtherSettingsCancelButton_Click(sender, e);
                        break;
                    case "OtherSettingsSave":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogOtherSettingsSave);
                        OtherSettingsSaveButton_Click(sender, e);
                        break;
                    case "MainSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: MainSettingsButton_Click");
                        MainSettingsButton_Click(sender, e);
                        break;
                    case "FinanceSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: FinanceSettingsButton_Click");
                        FinanceSettingsButton_Click(sender, e);
                        break;
                    case "TimeOutSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: TimeOutSettingsButton_Click");
                        TimeOutSettingsButton_Click(sender, e);
                        break;
                    case "InterfaceSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: InterfaceSettingsButton_Click");
                        InterfaceSettingsButton_Click(sender, e);
                        break;
                    case "ButtonsSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: ButtonsSettingsButton_Click");
                        ButtonsSettingsButton_Click(sender, e);
                        break;
                    case "ReSync":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: ReSync_Click");
                        ReSync_Click(sender, e);
                        break;
                    case "hourUp":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: HourUp_Click");
                        try
                        {
                            v = Convert.ToInt16(AutoShiftTimeHour.Text);
                        }
                        catch { v = 0; }
                        if (++v > 23)
                            v = 0;
                        AutoShiftTimeHour.Text = v.ToString("D2");
                        KKMSettingsSave.IsEnabled = true;
                        break;
                    case "hourDown":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: HourDown_Click");
                        try
                        {
                            v = Convert.ToInt16(AutoShiftTimeHour.Text);
                        }
                        catch { v = 0; }
                        if (--v < 0)
                            v = 23;
                        AutoShiftTimeHour.Text = v.ToString("D2");
                        KKMSettingsSave.IsEnabled = true;
                        break;
                    case "minUp":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: MinUp_Click");
                        try
                        {
                            v = Convert.ToInt16(AutoShiftTimeMin.Text);
                        }
                        catch { v = 0; }
                        if (++v > 59)
                            v = 0;
                        AutoShiftTimeMin.Text = v.ToString("D2");
                        KKMSettingsSave.IsEnabled = true;
                        break;
                    case "minDown":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: MinDown_Click");
                        try
                        {
                            v = Convert.ToInt16(AutoShiftTimeMin.Text);
                        }
                        catch { v = 0; }
                        if (--v < 0)
                            v = 59;
                        AutoShiftTimeMin.Text = v.ToString("D2");
                        KKMSettingsSave.IsEnabled = true;
                        break;
                    case "AddUpperButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: UpAdd_button_Click");
                        UpAdd_button_Click(sender, e);
                        break;
                    case "AddLowerButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: DownAdd_button_Click");
                        DownAdd_button_Click(sender, e);
                        break;
                    case "AddLeftButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: LeftAdd_button_Click");
                        LeftAdd_button_Click(sender, e);
                        break;
                    case "AddRightButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: RightAdd_button_Click");
                        RightAdd_button_Click(sender, e);
                        break;
                    case "RejectBoxClearButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: ClearRejectBox_Click");
                        ClearRejectBox_Click(sender, e);
                        break;
                    case "CardDispenserSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: CardDispenserSettingsButton_Click");
                        CardDispenserSettingsButton_Click(sender, e);
                        break;
                    case "CardReaderSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: CardReaderSettingsButton_Click");
                        CardReaderSettingsButton_Click(sender, e);
                        break;
                    case "SlaveSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: SlaveSettingsButton_Click");
                        SlaveSettingsButton_Click(sender, e);
                        break;
                    case "PrinterSettingsButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: PrinterSettingsButton_Click");
                        PrinterSettingsButton_Click(sender, e);
                        break;
                    case "TestingPanelReturnButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(1, Properties.Resources.ToLogTestingsReturn);
                        TechReturnButtons();
                        break;
                    case "PrintXReport":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: PrintXReport_Click");
                        PrintXReport_Click(sender, e);
                        break;
                    case "PrintZReport":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: PrintZReport_Click");
                        PrintZReport_Click(sender, e);
                        break;
                    case "CashAcceptorCleanButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: CashAcceptorCleanButton_Click");
                        Inkassation_Click(sender, e);
                        break;
                    case "CoinAcceptorCleanButton":
                        if (cashierForm != null)
                            cashierForm.ToLog(2, "TechForm: CommonClickHandler: CoinAcceptorCleanButton_Click");
                        Inkassation_Click(sender, e);
                        break;
                    case "BMLoadSW":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMLoadSW);
                            cashierForm.BMLoadSW();
                        }
                        break;
                    case "BMLoadParams":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMLoadParams);
                            cashierForm.BMLoadParams();
                        }
                        break;
                    case "BMTerminalInfo":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMTerminalInfo);
                            cashierForm.BMTerminalInfo();
                        }
                        break;
                    case "BMWorkKey":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMWorkKey);
                            cashierForm.BMWorkKey(true);
                        }
                        break;
                    case "BMEchoTest":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMEchoTest);
                            cashierForm.BMEchoTest();
                        }
                        break;
                    case "BMVerification":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMVerification);
                            cashierForm.BMVerification(true);
                        }
                        break;
                    case "BMCleanCache":
                        if (cashierForm != null)
                        {
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMVerification);
                            cashierForm.BMCleanCache();
                        }
                        break;
                    case "BMCancell":
                        if (cashierForm != null)
                        {
                            int ChequeNumber = 0;
                            decimal AmountDue = 0;
                            try
                            {
                                Calc AddingSummForm;
                                AddingSummForm = new Calc();
                                AddingSummForm.LayoutRoot.RowDefinitions[0].Height = new GridLength(40);
                                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                                AddingSummForm.Header.Visibility = Visibility.Visible;
                                AddingSummForm.Header.Text = Properties.Resources.AddingSummChequeNumber;

                                AddingSummForm.Len = 6;
                                AddingSummForm.Topmost = true;
                                AddingSummForm.ShowDialog();
                                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                                {
                                    try
                                    {
                                        ChequeNumber = Convert.ToInt32(AddingSummForm.Sum);
                                    }
                                    catch
                                    {
                                        ChequeNumber = 0;
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                            }

                            try
                            {
                                Calc AddingSummForm;
                                AddingSummForm = new Calc();
                                AddingSummForm.LayoutRoot.RowDefinitions[0].Height = new GridLength(40);
                                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                                AddingSummForm.btn_Dot.Visibility = Visibility.Visible;
                                AddingSummForm.btn_DotT.Visibility = Visibility.Visible;
                                AddingSummForm.Header.Visibility = Visibility.Visible;
                                AddingSummForm.Header.Text = Properties.Resources.AddingSummChequeSumm;
                                AddingSummForm.Len = 15;
                                AddingSummForm.Topmost = true;
                                AddingSummForm.ShowDialog();
                                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                                {
                                    try
                                    {
                                        AmountDue = Convert.ToDecimal(AddingSummForm.Sum);
                                    }
                                    catch
                                    {
                                        AmountDue = 0;
                                    }
                                }
                                AddingSummForm.Header.Visibility = Visibility.Hidden;
                                AddingSummForm.btn_Dot.Visibility = Visibility.Hidden;
                                AddingSummForm.btn_DotT.Visibility = Visibility.Hidden;
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                            }
                            //cashierForm.ToLog(1, Properties.Resources.ToLogBMVerification);
                            if (ChequeNumber != 0)
                            {
                                cashierForm.BMCancell(ChequeNumber, AmountDue);
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void PrintXReport_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: PrintXReport_Click");
            PrintXReport.IsEnabled = false;
            ShiftPanelReturnButton.Focus();
            try
            {
                if (cashierForm != null)
                {
                    if (((!cashierForm.EmulatorEnabled) || (!cashierForm.EmulatorForm.Emulator.KKMVirt.Checked)) && (cashierForm.KKM != null))
                    {
                        if ((cashierForm.KKM.DeviceEnabled) && (!cashierForm.KKM.HardStatus.PaperEnd))
                        {
                            cashierForm.KKM.SetPrezenter(0, 0, 0);
                            //cashierForm.KKM.SetPrezenter(0, 1, 0); //??????????
                            cashierForm.ToLog(2, "TechForm: PrintXReport_Click: cashierForm.PrintXReport");
                            cashierForm.PrintXReport();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
            PrintXReport.IsEnabled = true;
        }

        private void PrintZReport_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: PrintZReport_Click");
            PrintZReport.IsEnabled = false;
            ShiftPanelReturnButton.Focus();
            try
            {
                {
                    if (((!cashierForm.EmulatorEnabled) || (!cashierForm.EmulatorForm.Emulator.KKMVirt.Checked)) && (cashierForm.KKM != null))
                    {
                        int zReportFrom = Convert.ToInt32(ZReportFrom.Text);
                        int zReportTo = Convert.ToInt32(ZReportTo.Text);
                        if ((cashierForm.KKM.DeviceEnabled) && (!cashierForm.KKM.HardStatus.PaperEnd))
                        {
                            cashierForm.KKM.SetPrezenter(0, 0, 0); //????????
                            if (zReportFrom <= zReportTo)
                            {
                                for (int i = zReportFrom; i <= zReportTo; i++)
                                {
                                    /*DataRow BankDocumets = null;
                                    string req = "select * from BankDocumets where ShiftNumber=" + i.ToString() + " and OperationCode=4";
                                    lock (cashierForm.db_lock)
                                    {
                                        try
                                        {
                                            BankDocumets = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                BankDocumets = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }

                                    if (BankDocumets != null)
                                    {
                                        cashierForm.ToLog(2, "TechForm: PrintZReport_Click: cashierForm.PrintBankVerification");
                                        List<string> Ver = new List<string>();
                                        Ver.AddRange(BankDocumets.GetString("Receipt").Split(new Char[] { '\n' }));
                                        cashierForm.KKM.PrintNotCheck(Ver);
                                    }*/

                                    cashierForm.ToLog(2, "TechForm: PrintZReport_Click: cashierForm.KKM.ZReportByNum");
                                    cashierForm.KKM.ZReportByNum(i);

                                    if (cashierForm.KKM.HardStatus.PaperEnd)
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
            PrintZReport.IsEnabled = true;
        }
        private void Inkassation_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: Inkassaion");
            try
            {
                var result = System.Windows.Forms.MessageBox.Show(Properties.Resources.InkassationMessage, Properties.Resources.CashAcceptorCleanCaption,
                                             System.Windows.Forms.MessageBoxButtons.YesNo,
                                             System.Windows.Forms.MessageBoxIcon.Question);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    if (cashierForm != null)
                    {
                        cashierForm.ToLog(2, "TechForm: InkassationClick: cashierForm.KKM.Inkasstion");
                        if (cashierForm.KKM != null)
                            cashierForm.KKM.Inkasstion();
                        cashierForm.CashAcceptorCurrent = 0;
                        CashAcceptorCurrent.Text = "0";
                        cashierForm.CoinAcceptorCurrent = 0;
                        CoinAcceptorCurrent.Text = "0";

                        cashierForm.deviceCMModel.CashAcceptorCurrent = cashierForm.CashAcceptorCurrent;
                        cashierForm.deviceCMModel.CoinAcceptorCurrent = cashierForm.CoinAcceptorCurrent;
                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CashAcceptorCurrent", cashierForm.deviceCMModel.CashAcceptorCurrent, "CoinAcceptorCurrent", cashierForm.deviceCMModel.CoinAcceptorCurrent }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CashAcceptorCurrent", cashierForm.deviceCMModel.CashAcceptorCurrent, "CoinAcceptorCurrent", cashierForm.deviceCMModel.CoinAcceptorCurrent }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (cashierForm.CashAcceptor != null)
                            cashierForm.CashAcceptor.Reset();
                        if (cashierForm.EmulatorEnabled)
                        {
                            cashierForm.EmulatorForm.Emulator.CashAcceptorCurrent.Text = "0";
                            cashierForm.EmulatorForm.Emulator.PaidBanknoteCurrent = 0;
                            cashierForm.EmulatorForm.Emulator.CoinAcceptorCurrent.Text = "0";
                            cashierForm.EmulatorForm.Emulator.PaidCoinCurrent = 0;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        #region Shift
        protected void ShiftButton_Click(object sender, EventArgs e)
        {
            if (!ShiftModeButton.IsEnabled)
                ShiftModeButton.IsEnabled = true;
            if (cashierForm != null)
            {
                cashierForm.ToLog(2, "TechForm: ShiftButton_Click");
                try
                {
                    LastZReport.Text = cashierForm.LastZReport.ToString();
                    if ((cashierForm.deviceCMModel.CashAcceptorExist != null) && (bool)cashierForm.deviceCMModel.CashAcceptorExist)
                    {
                        if (CashAcceptorCleanButton.Visibility != Visibility.Visible)
                            CashAcceptorCleanButton.Visibility = Visibility.Visible;
                        if (CashAcceptorCurrent.Visibility != Visibility.Visible)
                            CashAcceptorCurrent.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (CashAcceptorCleanButton.Visibility != Visibility.Hidden)
                            CashAcceptorCleanButton.Visibility = Visibility.Hidden;
                        if (CashAcceptorCurrent.Visibility != Visibility.Hidden)
                            CashAcceptorCurrent.Visibility = Visibility.Hidden;
                    }
                    if (cashierForm.CashierType == 0)
                    {
                        if ((cashierForm.deviceCMModel.CoinAcceptorExist != null) && (bool)cashierForm.deviceCMModel.CoinAcceptorExist)
                        {
                            if (CoinAcceptorCleanButton.Visibility != Visibility.Visible)
                                CoinAcceptorCleanButton.Visibility = Visibility.Visible;
                            if (CoinAcceptorCurrent.Visibility != Visibility.Visible)
                                CoinAcceptorCurrent.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            if (CoinAcceptorCleanButton.Visibility != Visibility.Hidden)
                                CoinAcceptorCleanButton.Visibility = Visibility.Hidden;
                            if (CoinAcceptorCurrent.Visibility != Visibility.Hidden)
                                CoinAcceptorCurrent.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        if (CoinAcceptorCleanButton.Visibility != Visibility.Hidden)
                            CoinAcceptorCleanButton.Visibility = Visibility.Hidden;
                        if (CoinAcceptorCurrent.Visibility != Visibility.Hidden)
                            CoinAcceptorCurrent.Visibility = Visibility.Hidden;
                    }

                    if (cashierForm.ShiftOpened)
                    {
                        if (cashierForm.DefaultCulture != null)
                        {
                            if (ShiftState.Text != cashierForm.ResourceManager.GetString("ShiftOpened", cashierForm.DefaultCulture))
                                ShiftState.Text = cashierForm.ResourceManager.GetString("ShiftOpened", cashierForm.DefaultCulture);
                            if ((string)ShiftModeButton.Content != cashierForm.ResourceManager.GetString("CloseShift", cashierForm.DefaultCulture))
                                ShiftModeButton.Content = cashierForm.ResourceManager.GetString("CloseShift", cashierForm.DefaultCulture);
                        }
                        else
                        {
                            ShiftState.Text = Properties.Resources.ShiftOpened;
                            ShiftModeButton.Content = Properties.Resources.CloseShift;
                        }
                        if (ShiftStateRed.Visibility != Visibility.Hidden)
                            ShiftStateRed.Visibility = Visibility.Hidden;
                        if (ShiftStateGreen.Visibility != Visibility.Visible)
                            ShiftStateGreen.Visibility = Visibility.Visible;
                        if (Block_windows.Visibility != Visibility.Visible)
                            Block_windows.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (cashierForm.DefaultCulture != null)
                        {
                            if (ShiftState.Text != cashierForm.ResourceManager.GetString("ShiftClosed", cashierForm.DefaultCulture))
                                ShiftState.Text = cashierForm.ResourceManager.GetString("ShiftClosed", cashierForm.DefaultCulture);
                            if ((string)ShiftModeButton.Content != cashierForm.ResourceManager.GetString("OpenShift", cashierForm.DefaultCulture))
                                ShiftModeButton.Content = cashierForm.ResourceManager.GetString("OpenShift", cashierForm.DefaultCulture);
                        }
                        else
                        {
                            if (ShiftState.Text != Properties.Resources.ShiftClosed)
                                ShiftState.Text = Properties.Resources.ShiftClosed;
                            if ((string)ShiftModeButton.Content != Properties.Resources.OpenShift)
                                ShiftModeButton.Content = Properties.Resources.OpenShift;
                        }
                        if (ShiftStateRed.Visibility != Visibility.Visible)
                            ShiftStateRed.Visibility = Visibility.Visible;
                        if (ShiftStateGreen.Visibility != Visibility.Hidden)
                            ShiftStateGreen.Visibility = Visibility.Hidden;
                        if (Block_windows.Visibility != Visibility.Hidden)
                            Block_windows.Visibility = Visibility.Hidden;
                    }

                    if ((ZReportFrom.Text != "") && (ZReportTo.Text != "") && (ZReportFrom.Text != "0") && (ZReportTo.Text != "0"))
                        PrintZReport.IsEnabled = true;
                    else
                        PrintZReport.IsEnabled = false;

                    ShiftPanel.Visibility = Visibility.Visible;
                    TechButtonsPanel.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    if (cashierForm != null)
                        cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        DateTime ShiftModeButtonDisableTime;
        public void ShiftModeButton_Click()
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: ShiftModeButton_Click");

            ShiftModeButton.IsEnabled = false;

            if (cashierForm != null)
            {
                try
                {
                    cashierForm.ShiftOpened = !cashierForm.ShiftOpened;
                    if (cashierForm.ShiftOpened)
                    {
                        cashierForm.ToLog(2, "TechForm: ShiftModeButton_Click: cashierForm.ShiftOpen");
                        cashierForm.ShiftOpen(false);
                        LastZReport.Text = cashierForm.LastZReport.ToString();
                    }
                    else
                    {
                        cashierForm.ToLog(2, "TechForm: ShiftModeButton_Click: cashierForm.ShiftClose");
                        cashierForm.ShiftClose(false);
                        LastZReport.Text = cashierForm.LastZReport.ToString();
                    }
                }
                catch (Exception exc)
                {
                    if (cashierForm != null)
                        cashierForm.ToLog(0, exc.ToString());
                }
            }
            ShiftModeButtonDisableTime = DateTime.Now;
        }
        private void ZReportFrom_GotFocus(object sender, RoutedEventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: ZReportFrom_GotFocus");
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                if (cashierForm != null)
                    cashierForm.ToLog(2, "TechForm: ZReportFrom_GotFocus: AddingSummForm.ShowDialog");
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        ZReportFrom.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        if ((ZReportTo.Text == "") || (Convert.ToInt32(ZReportTo.Text) < Convert.ToInt32(ZReportFrom.Text)))
                            ZReportTo.Text = ZReportFrom.Text;
                    }
                    catch
                    {
                    }
                }
                if ((ZReportFrom.Text != "") && (ZReportTo.Text != "") && (ZReportFrom.Text != "0") && (ZReportTo.Text != "0"))
                    PrintZReport.IsEnabled = true;
                else
                    PrintZReport.IsEnabled = false;
                ShiftPanelReturnButton.Focus();
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void ZReportTo_GotFocus(object sender, RoutedEventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: ZReportTo_GotFocus");
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                if (cashierForm != null)
                    cashierForm.ToLog(2, "TechForm: ZReportTo_GotFocus: AddingSummForm.ShowDialog");
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        ZReportTo.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        if ((ZReportFrom.Text == "") || (Convert.ToInt32(ZReportTo.Text) < Convert.ToInt32(ZReportFrom.Text)))
                            ZReportFrom.Text = ZReportTo.Text;
                    }
                    catch
                    {
                    }
                }
                if ((ZReportFrom.Text != "") && (ZReportTo.Text != "") && (ZReportFrom.Text != "0") && (ZReportTo.Text != "0"))
                    PrintZReport.IsEnabled = true;
                else
                    PrintZReport.IsEnabled = false;
                ShiftPanelReturnButton.Focus();
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        #endregion Shift

        #region Settings
        protected void SettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: SettingsButton_Click");
            try
            {
                if (SettingsButtonPanel.Visibility != Visibility.Visible)
                    SettingsButtonPanel.Visibility = Visibility.Visible;
                if (TechButtonsPanel.Visibility != Visibility.Hidden)
                    TechButtonsPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }

        #region CashAcceptor
        protected void CashAcceptorButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: CashAcceptorButton_Click");
            try
            {
                if (cashierForm != null)
                    cashierForm.ToLog(2, "TechForm: CashAcceptorButton_Click: CashAcceptorGetSettings");
                CashAcceptorGetSettings();
                if (SettingsPanel.Visibility != Visibility.Visible)
                    SettingsPanel.Visibility = Visibility.Visible;
                if (CashAcceptorSettings.Visibility != Visibility.Visible)
                    CashAcceptorSettings.Visibility = Visibility.Visible;
                if (SettingsButtonPanel.Visibility != Visibility.Hidden)
                    SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void CashAcceptorSaveButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: CashAcceptorSaveButton_Click");
            bool changed = false;
            if (cashierForm != null)
            {
                int CashAcceptorTypeId = (cashierForm.deviceCMModel.CashAcceptorTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.CashAcceptorTypeId;
                int CashDispenserTypeId = (cashierForm.deviceCMModel.CashDispenserTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.CashDispenserTypeId;
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        int i;
                        // Сохранение параметров приемника банкнот
                        // Наличие устройства
                        cashierForm.deviceCMModel.CashAcceptorExist = (bool)CashAcceptorExists.IsChecked;
                        // Текущая модель
                        if ((CashAcceptorModelSelectedIndex >= 0) &&
                            (cashierForm.CashAcceptorExecutableDevices.Count - 1 >= CashAcceptorModelSelectedIndex))
                        {
                            cashierForm.deviceCMModel.CashAcceptorTypeId = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id;
                            cashierForm.CashAcceptorExecutableDeviceType = (ExecutableDeviceType)cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].ExecutableDeviceTypeId;
                            if (cashierForm.CashAcceptorExecutableDeviceType == ExecutableDeviceType.enCashRecycler)
                            {
                                /*for (i = 0; i < cashierForm.CashDispenserExecutableDevices.Count; i++)
                                    if (cashierForm.CashDispenserExecutableDevices[i].Id == cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id)
                                    {
                                        cashierForm.deviceCMModel.CashDispenserTypeId = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id;
                                        CashDispenserModelSelectedIndex = i;
                                        break;
                                    }
                                */
                                /*if (CashDispenserModelLine.IsEnabled)
                                    CashDispenserModelLine.IsEnabled = false;*/
                                if (CashDispenserModelLine.Visibility != Visibility.Hidden)
                                    CashDispenserModelLine.Visibility = Visibility.Hidden;
                                if (CashDispenserPortLine.Visibility != Visibility.Hidden)
                                    CashDispenserPortLine.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                /*if (!CashDispenserModelLine.IsEnabled)
                                    CashDispenserModelLine.IsEnabled = true;*/
                                if (CashDispenserModelLine.Visibility != Visibility.Visible)
                                    CashDispenserModelLine.Visibility = Visibility.Visible;
                                /*if ((cashierForm.CashDispenserExecutableDevices.Count - 1 < CashDispenserModelSelectedIndex) ||
                                    (cashierForm.CashDispenserExecutableDevices[CashDispenserModelSelectedIndex].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler))
                                {
                                    cashierForm.deviceCMModel.CashDispenserTypeId = null;
                                    CashDispenserModelSelectedIndex = -1;
                                }*/
                                if (CashDispenserPortLine.Visibility != Visibility.Visible)
                                    CashDispenserPortLine.Visibility = Visibility.Visible;
                            }

                            if (cashierForm.EmulatorEnabled && (CashAcceptorModelSelectedIndex >= 0))
                                cashierForm.EmulatorForm.Emulator.CashAcceptorLabel.Text = Properties.Resources.CashAcceptor + " " + cashierForm.CashDispenserExecutableDevices[CashAcceptorModelSelectedIndex].Name;
                        }

                        // Список номиналов банкнот и выбор принимаемых номиналов
                        string req = "select CashAcceptorAllowModel.* from CashAcceptorAllowModel, BanknoteModel where DeviceId='" + cashierForm.DeviceId.ToString() +
                            "' and CashAcceptorAllowModel.BanknoteId=BanknoteModel.Id and BanknoteModel.Code='" + cashierForm.Valute +
                            "' order by CashAcceptorAllowModel.BanknoteId ";
                        DataTable can = null;
                        lock (cashierForm.db_lock)
                        {
                            try
                            {
                                can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        /*if (can != null && can.Rows.Count > 0)
                        {
                            for (i = 0; i < can.Rows.Count; i++)
                                cashierForm.CashAcceptorAllows.Add(SetCashAcceptorAllowModel(can.Rows[i]));
                        }*/
                        //global::Cashier.Properties.Settings.Default.CashAcceptorCheckedListBox_Index.Clear();
                        for (i = 0; i < CashAcceptorNominals1.Children.Count; i++)
                        {
                            CashAcceptorAllowModelT caa;
                            if (i < cashierForm.CashAcceptorAllows.Count)
                            {
                                caa = cashierForm.CashAcceptorAllows[i];
                                caa.DeviceId = cashierForm.DeviceId;
                                caa.BanknoteId = cashierForm.Banknotes[i].Id;
                                caa.Allow = (bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked;
                                cashierForm.CashAcceptorAllows[i] = caa;
                                lock (cashierForm.db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.UpdateCommand("CashAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.UpdateCommand("CashAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                caa = new CashAcceptorAllowModelT();
                                caa.Id = Guid.NewGuid();
                                caa.DeviceId = cashierForm.DeviceId;
                                caa.BanknoteId = cashierForm.Banknotes[i].Id;
                                caa.Allow = (bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked;
                                cashierForm.CashAcceptorAllows.Add(caa);
                                lock (cashierForm.db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.InsertCommand("CashAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "BanknoteId", caa.BanknoteId });
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("CashAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "BanknoteId", caa.BanknoteId });
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                        }
                        for (i = 0; i < CashAcceptorNominals2.Children.Count; i++)
                        {
                            CashAcceptorAllowModelT caa;
                            if ((i + 3) < cashierForm.CashAcceptorAllows.Count)
                            {
                                caa = cashierForm.CashAcceptorAllows[i + 3];
                                caa.DeviceId = cashierForm.DeviceId;
                                caa.BanknoteId = cashierForm.Banknotes[i + 3].Id;
                                caa.Allow = (bool)((CheckBox)CashAcceptorNominals2.Children[i]).IsChecked;
                                cashierForm.CashAcceptorAllows[i + 3] = caa;
                                lock (cashierForm.db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.UpdateCommand("CashAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.UpdateCommand("CashAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                caa = new CashAcceptorAllowModelT();
                                caa.Id = Guid.NewGuid();
                                caa.DeviceId = cashierForm.DeviceId;
                                caa.BanknoteId = cashierForm.Banknotes[i + 3].Id;
                                caa.Allow = (bool)((CheckBox)CashAcceptorNominals2.Children[i]).IsChecked;
                                cashierForm.CashAcceptorAllows.Add(caa);

                                lock (cashierForm.db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.InsertCommand("CashAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "BanknoteId", caa.BanknoteId });
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("CashAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "BanknoteId", caa.BanknoteId });
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                        }
                        // Порог заполнения
                        try
                        {
                            cashierForm.deviceCMModel.CashAcceptorAlarm = Convert.ToInt16(CashAcceptorBoxEvent.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CashAcceptorAlarm = 0;
                        }
                        try
                        {
                            cashierForm.deviceCMModel.CashAcceptorLimit = Convert.ToInt16(CashAcceptorBoxCapacity.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CashAcceptorLimit = 0;
                        }
                        // CashAcceptorPort
                        if (CashAcceptorPortSelectedIndex > 0)
                        {
                            if (cashierForm.deviceCMModel.CashAcceptorComPortId != CashAcceptorPortSelectedIndex)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.CashAcceptorComPortId = CashAcceptorPortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.CashAcceptorComPortId != 1)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.CashAcceptorComPortId = 1;
                            }
                        }
                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.CashAcceptorExist != null)
                        {
                            lst.Add("CashAcceptorExist");
                            lst.Add(cashierForm.deviceCMModel.CashAcceptorExist);
                        }
                        if (cashierForm.deviceCMModel.CashAcceptorTypeId != null)
                        {
                            lst.Add("CashAcceptorTypeId");
                            lst.Add(cashierForm.deviceCMModel.CashAcceptorTypeId);
                        }
                        if (cashierForm.deviceCMModel.CashDispenserTypeId != null)
                        {
                            lst.Add("CashDispenserTypeId");
                            lst.Add(cashierForm.deviceCMModel.CashDispenserTypeId);
                        }
                        lst.Add("CashAcceptorAlarm");
                        lst.Add(cashierForm.deviceCMModel.CashAcceptorAlarm);
                        lst.Add("CashAcceptorLimit");
                        lst.Add(cashierForm.deviceCMModel.CashAcceptorLimit);
                        if (cashierForm.deviceCMModel.CashAcceptorComPortId != null)
                        {
                            lst.Add("CashAcceptorComPortId");
                            lst.Add(cashierForm.deviceCMModel.CashAcceptorComPortId);
                        }
                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }
                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    cashierForm.ToLog(2, "TechForm: CashAcceptorSaveButton_Click: CashAcceptorGetSettings");

                    if (changed ||
                        ((CashAcceptorTypeId >= 0) &&
                        (cashierForm.deviceCMModel.CashAcceptorTypeId != null) &&
                        cashierForm.deviceCMModel.CashAcceptorTypeId != CashAcceptorTypeId))
                    {
                        if (cashierForm.CashAcceptor != null)
                        {
                            cashierForm.CashAcceptor.Close();
                            cashierForm.CashAcceptor = null;
                        }
                    }

                    if ((CashDispenserTypeId >= 0) &&
                        (cashierForm.deviceCMModel.CashDispenserTypeId != null) &&
                        cashierForm.deviceCMModel.CashDispenserTypeId != CashDispenserTypeId)
                    {
                        if (cashierForm.CashDispenser != null)
                        {
                            cashierForm.CashDispenser.Close();
                            cashierForm.CashDispenser = null;
                        }
                    }

                    CashAcceptorGetSettings();
                    cashierForm.ToLog(2, "TechForm: CashAcceptorSaveButton_Click: SettingsReturnButton");
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void CashAcceptorCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                cashierForm.ToLog(2, "TechForm: CashAcceptorCancelButton_Click");
                try
                {
                    cashierForm.ToLog(2, "TechForm: CashAcceptorCancelButton_Click: CashAcceptorGetSettings");
                    CashAcceptorGetSettings();
                    cashierForm.ToLog(2, "TechForm: CashAcceptorCancelButton_Click: SettingsReturnButton");
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
            SettingsReturnButton();
        }
        public void CashAcceptorGetSettings()
        {
            int i;
            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    lock (cashierForm.db_lock)
                    {
                        //            bool b;
                        //CashAcceptorComboBox.Items.Clear();
                        if (cashierForm.EmulatorEnabled)
                        {
                            cashierForm.EmulatorForm.Emulator.b10.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.b50.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.b100.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.b500.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.b1000.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.b5000.BackColor = System.Drawing.Color.Red;
                        }
                        // Чтение сохраненных параметров банкнотоприемника
                        // Наличие устройства
                        try
                        {
                            CashAcceptorExists.IsChecked = (bool)cashierForm.deviceCMModel.CashAcceptorExist;
                            exists = (bool)cashierForm.deviceCMModel.CashAcceptorExist;
                        }
                        catch
                        {
                            CashAcceptorExists.IsChecked = false;
                            exists = false;
                        }
                        CashAcceptorModelLine.IsEnabled = exists;
                        CashAcceptorPortLine.IsEnabled = exists;
                        CashAcceptorNominalsLine.IsEnabled = exists;
                        CashAcceptorCapacityLine.IsEnabled = exists;
                        CashAcceptorLimitLine.IsEnabled = exists;
                        CashAcceptorResetLine.IsEnabled = exists;

                        // Текущая модель

                        try
                        {
                            CashAcceptorModelSelectedIndex = -1;
                            CashAcceptorModel.Text = Properties.Resources.NotSelected0;
                            for (i = 0; i < cashierForm.CashAcceptorExecutableDevices.Count; i++)
                                if (cashierForm.CashAcceptorExecutableDevices[i].Id == cashierForm.deviceCMModel.CashAcceptorTypeId)
                                {
                                    CashAcceptorModelSelectedIndex = i;
                                    CashAcceptorModel.Text = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            CashAcceptorModelSelectedIndex = -1;
                        }
                        cashierForm.CashAcceptorModel = CashAcceptorModel.Text;
                        if (CashAcceptorModelSelectedIndex >= 0 && cashierForm.CashAcceptorExecutableDevices.Count - 1 >= CashAcceptorModelSelectedIndex)
                        {
                            if (cashierForm.EmulatorEnabled)
                                cashierForm.EmulatorForm.Emulator.CashAcceptorLabel.Text = Properties.Resources.CashAcceptor + " " + cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Name;
                            cashierForm.CashAcceptorExecutableDeviceType = (ExecutableDeviceType)cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].ExecutableDeviceTypeId;
                            if (cashierForm.CashAcceptorExecutableDeviceType == ExecutableDeviceType.enCashRecycler)
                            {
                                for (i = 0; i < cashierForm.CashDispenserExecutableDevices.Count; i++)
                                    if (cashierForm.CashDispenserExecutableDevices[i].Id == cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id)
                                    {
                                        cashierForm.deviceCMModel.CashDispenserTypeId = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id;
                                        CashDispenserModelSelectedIndex = i;
                                        break;
                                    }
                                if (CashDispenserModelLine.IsEnabled)
                                    CashDispenserModelLine.IsEnabled = false;
                                if (CashDispenserPortLine.Visibility != Visibility.Hidden)
                                    CashDispenserPortLine.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                if (!CashDispenserModelLine.IsEnabled)
                                    CashDispenserModelLine.IsEnabled = true;
                                if (CashDispenserPortLine.Visibility != Visibility.Visible)
                                    CashDispenserPortLine.Visibility = Visibility.Visible;
                            }
                        }

                        SetCashAcceptorNominals();
                    }
                    // Порог заполнения
                    try
                    {
                        cashierForm.CashAcceptorMax = cashierForm.deviceCMModel.CashAcceptorAlarm;
                    }
                    catch
                    {
                        cashierForm.CashAcceptorMax = 0;
                    }
                    try
                    {
                        cashierForm.CashAcceptorMaxCount = cashierForm.deviceCMModel.CashAcceptorLimit;
                    }
                    catch
                    {
                        cashierForm.CashAcceptorMaxCount = 0;
                    }
                    CashAcceptorBoxEvent.Text = cashierForm.CashAcceptorMax.ToString();
                    CashAcceptorBoxCapacity.Text = cashierForm.CashAcceptorMaxCount.ToString();
                    // Список портов
                    try
                    {
                        CashAcceptorPortSelectedIndex = (int)cashierForm.deviceCMModel.CashAcceptorComPortId;
                    }
                    catch
                    {
                        CashAcceptorPortSelectedIndex = 1;
                    }
                    CashAcceptorPort.Text = "COM" + (CashAcceptorPortSelectedIndex).ToString();
                    cashierForm.CashAcceptorPort = CashAcceptorPort.Text;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
            CashAcceptorSettingsSave.IsEnabled = false;
            CashAcceptorCheck();
            //            }
        }
        public void SetCashAcceptorNominals()
        {
            try
            {
                string req = "select CashAcceptorAllowModel.* from CashAcceptorAllowModel, BanknoteModel where DeviceId='" + cashierForm.DeviceId.ToString() +
                    "' and CashAcceptorAllowModel.BanknoteId=BanknoteModel.Id and BanknoteModel.Code='" + cashierForm.Valute +
                    "' order by CashAcceptorAllowModel.BanknoteId";
                DataTable can = null;
                lock (cashierForm.db_lock)
                {
                    try
                    {
                        can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                        try
                        {
                            can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            cashierForm.ToLog(0, exc1.ToString());
                        }
                    }
                }
                cashierForm.CashAcceptorAllows.Clear();
                if (can != null && can.Rows.Count > 0)
                {
                    for (int i = 0; i < can.Rows.Count; i++)
                        cashierForm.CashAcceptorAllows.Add(SetCashAcceptorAllowModel(can.Rows[i]));
                }
                for (int i = 0; i < cashierForm.Banknotes.Count; i++)
                {
                    if (i < 6)
                    {
                        if ((cashierForm.CashAcceptorAllows.Count > i) && cashierForm.CashAcceptorAllows[i].Allow)
                        {
                            if (i < 3)
                            {
                                ((CheckBox)CashAcceptorNominals1.Children[i]).Visibility = Visibility.Visible;
                                ((CheckBox)CashAcceptorNominals1.Children[i]).Content = cashierForm.Banknotes[i].Value;
                                ((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked = true;
                            }
                            else
                            {
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Visibility = Visibility.Visible;
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Content = cashierForm.Banknotes[i].Value;
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked = true;
                            }
                        }
                        else
                        {
                            if (i < 3)
                            {
                                ((CheckBox)CashAcceptorNominals1.Children[i]).Visibility = Visibility.Visible;
                                ((CheckBox)CashAcceptorNominals1.Children[i]).Content = cashierForm.Banknotes[i].Value;
                                ((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked = false;
                            }
                            else
                            {
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Visibility = Visibility.Visible;
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Content = cashierForm.Banknotes[i].Value;
                                ((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked = false;
                            }
                        }
                    }
                }
                for (int i = cashierForm.Banknotes.Count; i < 6; i++)
                {
                    if (i < 3)
                    {
                        ((CheckBox)CashAcceptorNominals1.Children[i]).Visibility = Visibility.Hidden;
                        ((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked = false;
                    }
                    else
                    {
                        ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Visibility = Visibility.Hidden;
                        ((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked = false;
                    }
                }

                for (int i = 0; i < cashierForm.Banknotes.Count; i++)
                {
                    if (cashierForm.EmulatorEnabled)
                    {
                        switch (i)
                        {
                            case 0:
                                if ((bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b10.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b10.BackColor = System.Drawing.Color.Red;
                                break;
                            case 1:
                                if ((bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b50.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b50.BackColor = System.Drawing.Color.Red;
                                break;
                            case 2:
                                if ((bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b100.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b100.BackColor = System.Drawing.Color.Red;
                                break;
                            case 3:
                                if ((bool)((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b500.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b500.BackColor = System.Drawing.Color.Red;
                                break;
                            case 4:
                                if ((bool)((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b1000.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b1000.BackColor = System.Drawing.Color.Red;
                                break;
                            case 5:
                                if ((bool)((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.b5000.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.b5000.BackColor = System.Drawing.Color.Red;
                                break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
        }
        public void CashAcceptorCheck()
        {
            if (cashierForm != null)
            {
                try
                {
                    bool First;
                    int i;

                    if (cashierForm.EmulatorEnabled)
                    {
                        if (cashierForm.EmulatorForm.Emulator.CashAcceptorWork.Checked && (bool)CashAcceptorExists.IsChecked)
                        {
                            First = true;
                            cashierForm.CashAcceptorStatusLabel = Properties.Resources.CashAcceptorStatusLabel;
                            for (i = 0; i < cashierForm.Banknotes.Count; i++)
                            {
                                if (i < 3)
                                {
                                    if ((bool)((CheckBox)CashAcceptorNominals1.Children[i]).IsChecked)
                                        if (First)
                                        {
                                            cashierForm.CashAcceptorStatusLabel += " ";
                                            cashierForm.CashAcceptorStatusLabel += ((CheckBox)CashAcceptorNominals1.Children[i]).Content;
                                            First = false;
                                        }
                                        else
                                        {
                                            cashierForm.CashAcceptorStatusLabel += ", ";
                                            cashierForm.CashAcceptorStatusLabel += ((CheckBox)CashAcceptorNominals1.Children[i]).Content;
                                        }
                                }
                                else
                                {
                                    if ((bool)((CheckBox)CashAcceptorNominals2.Children[i - 3]).IsChecked)
                                        if (First)
                                        {
                                            cashierForm.CashAcceptorStatusLabel += " ";
                                            cashierForm.CashAcceptorStatusLabel += ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Content;
                                            First = false;
                                        }
                                        else
                                        {
                                            cashierForm.CashAcceptorStatusLabel += ", ";
                                            cashierForm.CashAcceptorStatusLabel += ((CheckBox)CashAcceptorNominals2.Children[i - 3]).Content;
                                        }
                                }
                            }
                            if (cashierForm.CashAcceptorStatusLabel != Properties.Resources.CashAcceptorStatusLabel)
                                cashierForm.CashAcceptorStatusLabel += " " + Properties.Resources.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                            else
                                cashierForm.CashAcceptorStatusLabel = "";
                        }
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CashAcceptorMax_Enter(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: CashAcceptorMax_Enter");
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                if (cashierForm != null)
                    cashierForm.ToLog(2, "TechForm: CashAcceptorMax_Enter: AddingSummForm.ShowDialog");
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CashAcceptorBoxEvent.Text != AddingSummForm.Sum)
                            CashAcceptorSettingsSave.IsEnabled = true;
                        CashAcceptorBoxEvent.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
            CashAcceptorSettingsReturn.Focus();
        }
        private void CashAcceptorMaxCount_Enter(object sender, EventArgs e)
        {
            if (cashierForm != null)
                cashierForm.ToLog(2, "TechForm: CashAcceptorMaxCount_Enter");
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                if (cashierForm != null)
                    cashierForm.ToLog(2, "TechForm: CashAcceptorMaxCount_Enter: AddingSummForm.ShowDialog");
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CashAcceptorBoxCapacity.Text != AddingSummForm.Sum)
                            CashAcceptorSettingsSave.IsEnabled = true;
                        CashAcceptorBoxCapacity.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
            CashAcceptorSettingsReturn.Focus();
        }
        private void CashAcceptorPort_SelectedIndexChanged(object sender = null, EventArgs e = null)
        {
            if (cashierForm != null)
            {
                cashierForm.ToLog(2, "TechForm: CashAcceptorPort_SelectedIndexChanged");
                try
                {
                    if (cashierForm.CashAcceptor != null)
                    {
                        cashierForm.ToLog(2, "TechForm: CashAcceptorPort_SelectedIndexChanged: cashierForm.CashAcceptor.Close");
                        cashierForm.CashAcceptor.Close();
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CashAcceptorModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CashAcceptorModel;
                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CashAcceptorExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CashAcceptorModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.CashAcceptorExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CashAcceptorModelSelectedIndex != p.SelectedIndex)
                            CashAcceptorSettingsSave.IsEnabled = true;
                        CashAcceptorModelSelectedIndex = p.SelectedIndex;
                        CashAcceptorModel.Text = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                if (cashierForm != null)
                    cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CashAcceptorSettingsReturn.Focus();
        }
        private void CashAcceptorPort_GotFocus(object sender, RoutedEventArgs e)
        {
            int port = CashAcceptorPortSelectedIndex;
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CashAcceptorPort;
                ((RadioButton)p.ItemsList.Items[CashAcceptorPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CashAcceptorPortSelectedIndex != p.SelectedIndex + 1)
                            CashAcceptorSettingsSave.IsEnabled = true;

                        CashAcceptorPortSelectedIndex = p.SelectedIndex + 1;
                        CashAcceptorPort.Text = (string)((RadioButton)p.ItemsList.Items[CashAcceptorPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
                if (port != CashAcceptorPortSelectedIndex)
                    CashAcceptorPort_SelectedIndexChanged();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CashAcceptorSettingsReturn.Focus();
        }
        private void CashAcceptorCheckBox_Click(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            switch (cb.Name)
            {
                case "CashAcceptorExists":
                    CashAcceptorModelLine.IsEnabled = (bool)cb.IsChecked;
                    CashAcceptorPortLine.IsEnabled = (bool)cb.IsChecked;
                    CashAcceptorNominalsLine.IsEnabled = (bool)cb.IsChecked;
                    CashAcceptorCapacityLine.IsEnabled = (bool)cb.IsChecked;
                    CashAcceptorLimitLine.IsEnabled = (bool)cb.IsChecked;
                    CashAcceptorResetLine.IsEnabled = (bool)cb.IsChecked;
                    break;
            }
            CashAcceptorSettingsSave.IsEnabled = true;
        }
        private void CashAcceptorReset_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!cashierForm.EmulatorEnabled) || (!cashierForm.EmulatorForm.Emulator.CashAcceptorVirt.Checked))
                    cashierForm.CashAcceptor.Reset();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }

        }
        #endregion CashAcceptor

        #region CoinAcceptor
        protected void CoinAcceptorButton_Click(object sender, EventArgs e)
        {
            try
            {
                CoinAcceptorGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                CoinAcceptorSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void CoinAcceptorSaveButton_Click(object sender, EventArgs e)
        {
            bool changed = false;
            if (cashierForm != null)
            {
                int CoinAcceptorTypeId = (cashierForm.deviceCMModel.CoinAcceptorTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.CoinAcceptorTypeId;
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        int i;
                        // Сохранение параметров приемника монет
                        // Наличие устройства
                        cashierForm.deviceCMModel.CoinAcceptorExist = CoinAcceptorExists.IsChecked;
                        // Текущая модель
                        if (CoinAcceptorModelSelectedIndex >= 0)
                            cashierForm.deviceCMModel.CoinAcceptorTypeId = cashierForm.CoinAcceptorExecutableDevices[CoinAcceptorModelSelectedIndex].Id;

                        if (cashierForm.EmulatorEnabled)
                            cashierForm.EmulatorForm.Emulator.CoinAcceptorLabel.Text = Properties.Resources.CoinAcceptor + " " + cashierForm.CoinAcceptorExecutableDevices[CoinAcceptorModelSelectedIndex].Name;

                        // Список номиналов монет и выбор принимаемых номиналов
                        string req = "select CoinAcceptorAllowModel.* from CoinAcceptorAllowModel, CoinModel where DeviceId='" + cashierForm.DeviceId.ToString() +
                            "' and CoinAcceptorAllowModel.CoinId=CoinModel.Id and CoinModel.Code='" + cashierForm.Valute +
                            "' order by CoinAcceptorAllowModel.CoinId";
                        DataTable can = null;
                        lock (cashierForm.db_lock)
                        {
                            try
                            {
                                can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        /*if (can != null && can.Rows.Count > 0)
                        {
                            for (i = 0; i < can.Rows.Count; i++)
                                cashierForm.CoinAcceptorAllows.Add(SetCoinAcceptorAllowModel(can.Rows[i]));
                        }*/
                        //global::Cashier.Properties.Settings.Default.CoinAcceptorCheckedListox_Index.Clear();
                        for (i = 0; i < CoinAcceptorNominals1.Children.Count; i++)
                        {
                            CoinAcceptorAllowModelT caa;
                            if (i < cashierForm.CoinAcceptorAllows.Count)
                            {
                                if (((CheckBox)CoinAcceptorNominals1.Children[i]).IsVisible)
                                {
                                    caa = cashierForm.CoinAcceptorAllows[i];
                                    caa.DeviceId = cashierForm.DeviceId;
                                    caa.CoinId = cashierForm.Coins[i].Id;
                                    caa.Allow = (bool)((CheckBox)CoinAcceptorNominals1.Children[i]).IsChecked;
                                    cashierForm.CoinAcceptorAllows[i] = caa;
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.UpdateCommand("CoinAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.UpdateCommand("CoinAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (((CheckBox)CoinAcceptorNominals1.Children[i]).IsVisible)
                                {
                                    caa = new CoinAcceptorAllowModelT();
                                    caa.Id = Guid.NewGuid();
                                    caa.DeviceId = cashierForm.DeviceId;
                                    caa.CoinId = cashierForm.Coins[i].Id;
                                    caa.Allow = (bool)((CheckBox)CoinAcceptorNominals1.Children[i]).IsChecked;
                                    cashierForm.CoinAcceptorAllows.Add(caa);
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("CoinAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "CoinId", caa.CoinId });
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("CoinAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "CoinId", caa.CoinId });
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        for (i = 0; i < CoinAcceptorNominals2.Children.Count; i++)
                        {
                            CoinAcceptorAllowModelT caa;
                            if ((i + 3) < cashierForm.CoinAcceptorAllows.Count)
                            {
                                if (((CheckBox)CoinAcceptorNominals2.Children[i]).IsVisible)
                                {
                                    caa = cashierForm.CoinAcceptorAllows[i + 3];
                                    caa.DeviceId = cashierForm.DeviceId;
                                    caa.CoinId = cashierForm.Coins[i + 3].Id;
                                    caa.Allow = (bool)((CheckBox)CoinAcceptorNominals2.Children[i]).IsChecked;
                                    cashierForm.CoinAcceptorAllows[i + 3] = caa;
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.UpdateCommand("CoinAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.UpdateCommand("CoinAcceptorAllowModel", new object[] { "Allow", caa.Allow }, "Id=@Key", new object[] { "@Key", caa.Id }, cashierForm.db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (((CheckBox)CoinAcceptorNominals2.Children[i]).IsVisible)
                                {
                                    caa = new CoinAcceptorAllowModelT();
                                    caa.Id = Guid.NewGuid();
                                    caa.DeviceId = cashierForm.DeviceId;
                                    caa.CoinId = cashierForm.Coins[i + 3].Id;
                                    caa.Allow = (bool)((CheckBox)CoinAcceptorNominals2.Children[i]).IsChecked;
                                    cashierForm.CoinAcceptorAllows.Add(caa);

                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("CoinAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "CoinId", caa.CoinId });
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("CoinAcceptorAllowModel", cashierForm.db, null, new object[] { "Id", caa.Id, "DeviceId", caa.DeviceId, "Allow", caa.Allow, "CoinId", caa.CoinId });
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Порог заполнения
                        try
                        {
                            cashierForm.deviceCMModel.CoinAcceptorAlarm = Convert.ToInt16(CoinAcceptorBoxEvent.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CoinAcceptorAlarm = 0;
                        }
                        try
                        {
                            cashierForm.deviceCMModel.CoinAcceptorLimit = Convert.ToInt16(CoinAcceptorBoxCapacity.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CoinAcceptorLimit = 0;
                        }

                        // CoinAcceptorPort
                        if (CoinAcceptorPortSelectedIndex != 0)
                        {
                            if (cashierForm.deviceCMModel.CoinAcceptorComPortId != CoinAcceptorPortSelectedIndex)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.CoinAcceptorComPortId = CoinAcceptorPortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.CoinAcceptorComPortId != 1)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.CoinAcceptorComPortId = 1;
                            }
                        }
                        cashierForm.deviceCMModel.CoinAcceptorAddress = byte.Parse(CoinAcceptorAddress.Text);
                        byte[] encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(CoinAcceptorPIN.Password);

                        cashierForm.deviceCMModel.CoinAcceptorPinCode = cashierForm.cryp.ByteAToString(encrypted);

                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.CoinAcceptorExist != null)
                        {
                            lst.Add("CoinAcceptorExist");
                            lst.Add(cashierForm.deviceCMModel.CoinAcceptorExist);
                        }
                        if (cashierForm.deviceCMModel.CoinAcceptorTypeId != null)
                        {
                            lst.Add("CoinAcceptorTypeId");
                            lst.Add(cashierForm.deviceCMModel.CoinAcceptorTypeId);
                        }
                        lst.Add("CoinAcceptorAlarm");
                        lst.Add(cashierForm.deviceCMModel.CoinAcceptorAlarm);
                        lst.Add("CoinAcceptorLimit");
                        lst.Add(cashierForm.deviceCMModel.CoinAcceptorLimit);
                        if (cashierForm.deviceCMModel.CoinAcceptorComPortId != null)
                        {
                            lst.Add("CoinAcceptorComPortId");
                            lst.Add(cashierForm.deviceCMModel.CoinAcceptorComPortId);
                        }
                        if (cashierForm.deviceCMModel.CoinAcceptorAddress != null)
                        {
                            lst.Add("CoinAcceptorAddress");
                            lst.Add(cashierForm.deviceCMModel.CoinAcceptorAddress);
                        }
                        if (cashierForm.deviceCMModel.CoinAcceptorPinCode != null)
                        {
                            lst.Add("CoinAcceptorPinCode");
                            lst.Add(cashierForm.deviceCMModel.CoinAcceptorPinCode);
                        }

                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }

                    if (changed ||
                        ((CoinAcceptorTypeId >= 0) &&
                        (cashierForm.deviceCMModel.CoinAcceptorTypeId != null) &&
                        cashierForm.deviceCMModel.CoinAcceptorTypeId != CoinAcceptorTypeId))
                    {
                        if (cashierForm.CoinAcceptor != null)
                        {
                            cashierForm.CoinAcceptor.Close();
                            cashierForm.CoinAcceptor = null;
                        }
                    }

                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    CoinAcceptorGetSettings();

                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void CoinAcceptorCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CoinAcceptorGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void CoinAcceptorGetSettings()
        {
            int i;
            byte[] encrypted;
            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    lock (cashierForm.db_lock)
                    {
                        if (cashierForm.EmulatorEnabled)
                        {
                            cashierForm.EmulatorForm.Emulator.m1.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.m2.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.m3.BackColor = System.Drawing.Color.Red;
                            cashierForm.EmulatorForm.Emulator.m4.BackColor = System.Drawing.Color.Red;
                        }
                        // Чтение сохраненных параметров приемника монет
                        // Наличие устройства
                        try
                        {
                            CoinAcceptorExists.IsChecked = (bool)cashierForm.deviceCMModel.CoinAcceptorExist;
                            exists = (bool)cashierForm.deviceCMModel.CoinAcceptorExist;
                        }
                        catch
                        {
                            CoinAcceptorExists.IsChecked = false;
                            exists = false;
                        }
                        CoinAcceptorModelLine.IsEnabled = exists;
                        CoinAcceptorPortLine.IsEnabled = exists;
                        CoinAcceptorNominalsLine.IsEnabled = exists;
                        CoinAcceptorCapacityLine.IsEnabled = exists;
                        CoinAcceptorLimitLine.IsEnabled = exists;
                        CoinAcceptorAddressLine.IsEnabled = exists;
                        CoinAcceptorPinLine.IsEnabled = exists;
                        // Текущая модель
                        try
                        {
                            for (i = 0; i < cashierForm.CoinAcceptorExecutableDevices.Count; i++)
                                if (cashierForm.CoinAcceptorExecutableDevices[i].Id == cashierForm.deviceCMModel.CoinAcceptorTypeId)
                                {
                                    CoinAcceptorModelSelectedIndex = i;
                                    CoinAcceptorModel.Text = cashierForm.CoinAcceptorExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            CoinAcceptorModelSelectedIndex = 0;
                        }
                        cashierForm.CoinAcceptorModel = CoinAcceptorModel.Text;
                        if (cashierForm.EmulatorEnabled && (CoinAcceptorModelSelectedIndex >= 0))
                            cashierForm.EmulatorForm.Emulator.CoinAcceptorLabel.Text = Properties.Resources.CoinAcceptor + " " + cashierForm.CoinAcceptorExecutableDevices[CoinAcceptorModelSelectedIndex].Name;

                        // Список номиналов монет и выбор принимаемых номиналов
                        SetCoinAcceptorNominals();
                        // Порог заполнения
                        try
                        {
                            cashierForm.CoinAcceptorMax = cashierForm.deviceCMModel.CoinAcceptorAlarm;
                        }
                        catch
                        {
                            cashierForm.CoinAcceptorMax = 0;
                        }
                        try
                        {
                            cashierForm.CoinAcceptorMaxCount = cashierForm.deviceCMModel.CoinAcceptorLimit;
                        }
                        catch
                        {
                            cashierForm.CoinAcceptorMaxCount = 0;
                        }
                        CoinAcceptorBoxEvent.Text = cashierForm.CoinAcceptorMax.ToString();
                        CoinAcceptorBoxCapacity.Text = cashierForm.CoinAcceptorMaxCount.ToString();

                        try
                        {
                            CoinAcceptorPortSelectedIndex = (int)cashierForm.deviceCMModel.CoinAcceptorComPortId;
                        }
                        catch
                        {
                            CoinAcceptorPortSelectedIndex = 1;
                        }
                        CoinAcceptorPort.Text = "COM" + (CoinAcceptorPortSelectedIndex).ToString();
                        cashierForm.CoinAcceptorPort = CoinAcceptorPort.Text;
                        try
                        {
                            CoinAcceptorAddress.Text = ((byte)cashierForm.deviceCMModel.CoinAcceptorAddress).ToString();
                        }
                        catch
                        {
                            CoinAcceptorAddress.Text = "2";
                        }
                        if ((cashierForm.deviceCMModel.CoinAcceptorPinCode == null) || (cashierForm.deviceCMModel.CoinAcceptorPinCode == ""))
                        {
                            encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("010203040506");
                            cashierForm.deviceCMModel.CoinAcceptorPinCode = cashierForm.cryp.ByteAToString(encrypted);
                        }

                        encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.CoinAcceptorPinCode);
                        CoinAcceptorPIN.Password = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);
                    }
                    cashierForm.CoinAcceptorPIN = CoinAcceptorPIN.Password;
                    CoinAcceptorCheck();
                    CoinAcceptorSettingsSave.IsEnabled = false;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void CoinAcceptorCheck()
        {
            bool First;
            int i;
            if (cashierForm != null)
            {
                try
                {
                    if (cashierForm.EmulatorEnabled)
                    {
                        if (cashierForm.EmulatorForm.Emulator.CoinAcceptorWork.Checked && (bool)CoinAcceptorExists.IsChecked)
                        {
                            First = true;
                            cashierForm.CoinAcceptorStatusLabel = Properties.Resources.CashAcceptorStatusLabel;
                            int ic = 0;
                            string Value = "";
                            for (i = 0; i < cashierForm.Coins.Count; i++)
                            {
                                if (Value != cashierForm.Coins[i].Value)
                                {
                                    Value = cashierForm.Coins[i].Value;
                                    if (ic < 3)
                                    {
                                        if ((bool)((CheckBox)CoinAcceptorNominals1.Children[ic]).IsChecked)
                                            if (First)
                                            {
                                                cashierForm.CoinAcceptorStatusLabel += " ";
                                                cashierForm.CoinAcceptorStatusLabel += ((CheckBox)CoinAcceptorNominals1.Children[ic]).Content;
                                                First = false;
                                            }
                                            else
                                            {
                                                cashierForm.CoinAcceptorStatusLabel += ", ";
                                                cashierForm.CoinAcceptorStatusLabel += ((CheckBox)CoinAcceptorNominals1.Children[ic]).Content;
                                            }
                                    }
                                    else
                                    {
                                        if ((bool)((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).IsChecked)
                                            if (First)
                                            {
                                                cashierForm.CoinAcceptorStatusLabel += " ";
                                                cashierForm.CoinAcceptorStatusLabel += ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Content;
                                                First = false;
                                            }
                                            else
                                            {
                                                cashierForm.CoinAcceptorStatusLabel += ", ";
                                                cashierForm.CoinAcceptorStatusLabel += ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Content;
                                            }
                                    }
                                    ic++;
                                }
                            }
                            if (cashierForm.CoinAcceptorStatusLabel == Properties.Resources.CashAcceptorStatusLabel)
                            {
                                cashierForm.CoinAcceptorStatusLabel = "";
                            }
                            else
                                cashierForm.CoinAcceptorStatusLabel += " " + Properties.Resources.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                        }
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CoinAcceptorMax_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CoinAcceptorBoxEvent.Text != AddingSummForm.Sum)
                            CoinAcceptorSettingsSave.IsEnabled = true;
                        CoinAcceptorBoxEvent.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinAcceptorSettingsReturn.Focus();
        }
        private void CoinAcceptorMaxCount_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CoinAcceptorBoxCapacity.Text != AddingSummForm.Sum)
                            CoinAcceptorSettingsSave.IsEnabled = true;
                        CoinAcceptorBoxCapacity.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinAcceptorSettingsReturn.Focus();
        }
        private void Address_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CoinAcceptorAddress.Text != AddingSummForm.Sum)
                            CoinAcceptorSettingsSave.IsEnabled = true;
                        CoinAcceptorAddress.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinAcceptorSettingsReturn.Focus();
        }
        private void PinCode_Enter(object sender, EventArgs e)
        {
            try
            {
                CalcHex AddingSummForm;
                AddingSummForm = new CalcHex();
                AddingSummForm.Len = 12;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    if (AddingSummForm.Sum != "")
                    {
                        try
                        {
                            if (CoinAcceptorPIN.Password != AddingSummForm.Sum)
                                CoinAcceptorSettingsSave.IsEnabled = true;
                            CoinAcceptorPIN.Password = AddingSummForm.Sum;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinAcceptorSettingsReturn.Focus();
        }
        public void SetCoinAcceptorNominals()
        {
            try
            {
                int i = 0;
                string req = "select CoinAcceptorAllowModel.* from CoinAcceptorAllowModel, CoinModel where DeviceId='" + cashierForm.DeviceId.ToString() +
                    "' and CoinAcceptorAllowModel.CoinId=CoinModel.Id and CoinModel.Code='" + cashierForm.Valute +
                    "' order by CoinAcceptorAllowModel.CoinId";
                DataTable can = null;
                for (i = 0; i < 3; i++)
                {
                    ((CheckBox)CoinAcceptorNominals1.Children[i]).Visibility = Visibility.Hidden;
                    ((CheckBox)CoinAcceptorNominals2.Children[i]).Visibility = Visibility.Hidden;
                }

                lock (cashierForm.db_lock)
                {
                    try
                    {
                        can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                        try
                        {
                            can = UtilsBase.FillTable(req, cashierForm.db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            cashierForm.ToLog(0, exc1.ToString());
                        }
                    }
                }
                cashierForm.CoinAcceptorAllows.Clear();
                if (can != null && can.Rows.Count > 0)
                {
                    for (i = 0; i < can.Rows.Count; i++)
                        cashierForm.CoinAcceptorAllows.Add(SetCoinAcceptorAllowModel(can.Rows[i]));
                }
                int ic = 0;
                string Value = "";
                for (i = 0; i < cashierForm.Coins.Count; i++)
                {
                    if (ic < 6)
                    {
                        if (Value != cashierForm.Coins[i].Value)
                        {
                            Value = cashierForm.Coins[i].Value;

                            if ((cashierForm.CoinAcceptorAllows.Count > ic) && cashierForm.CoinAcceptorAllows[ic].Allow)
                            {
                                if (ic < 3)
                                {
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).Visibility = Visibility.Visible;
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).Content = cashierForm.Coins[i].Value;
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).IsChecked = true;
                                }
                                else
                                {
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Visibility = Visibility.Visible;
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Content = cashierForm.Coins[i].Value;
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).IsChecked = true;
                                }
                            }
                            else
                            {
                                if (ic < 3)
                                {
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).Visibility = Visibility.Visible;
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).Content = cashierForm.Coins[i].Value;
                                    ((CheckBox)CoinAcceptorNominals1.Children[ic]).IsChecked = false;
                                }
                                else
                                {
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Visibility = Visibility.Visible;
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).Content = cashierForm.Coins[i].Value;
                                    ((CheckBox)CoinAcceptorNominals2.Children[ic - 3]).IsChecked = false;
                                }
                            }
                            ic++;
                        }
                    }
                }

                for (i = 0; i < 6; i++)
                    if (cashierForm.EmulatorEnabled)
                    {
                        switch (i)
                        {
                            case 0:
                                if ((bool)((CheckBox)CoinAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.m1.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.m1.BackColor = System.Drawing.Color.Red;

                                break;
                            case 1:
                                if ((bool)((CheckBox)CoinAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.m2.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.m2.BackColor = System.Drawing.Color.Red;
                                break;
                            case 2:
                                if ((bool)((CheckBox)CoinAcceptorNominals1.Children[i]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.m3.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.m3.BackColor = System.Drawing.Color.Red;
                                break;
                            case 3:
                                if ((bool)((CheckBox)CoinAcceptorNominals2.Children[i - 3]).IsChecked)
                                    cashierForm.EmulatorForm.Emulator.m4.BackColor = System.Drawing.SystemColors.Control;
                                else
                                    cashierForm.EmulatorForm.Emulator.m4.BackColor = System.Drawing.Color.Red;
                                break;
                        }
                    }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CoinAcceptorModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CoinAcceptorModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CoinAcceptorExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CoinAcceptorModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.CoinAcceptorExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CoinAcceptorModelSelectedIndex != p.SelectedIndex)
                            CoinAcceptorSettingsSave.IsEnabled = true;
                        CoinAcceptorModelSelectedIndex = p.SelectedIndex;
                        CoinAcceptorModel.Text = cashierForm.CoinAcceptorExecutableDevices[CoinAcceptorModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CoinAcceptorSettingsReturn.Focus();
        }
        private void CoinAcceptorPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CoinAcceptorPort;
                ((RadioButton)p.ItemsList.Items[CoinAcceptorPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CoinAcceptorPortSelectedIndex != p.SelectedIndex + 1)
                            CoinAcceptorSettingsSave.IsEnabled = true;
                        CoinAcceptorPortSelectedIndex = p.SelectedIndex + 1;
                        CoinAcceptorPort.Text = (string)((RadioButton)p.ItemsList.Items[CoinAcceptorPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CoinAcceptorSettingsReturn.Focus();
        }
        private void CoinAcceptorCheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "CoinAcceptorExists":
                        CoinAcceptorModelLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorPortLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorNominalsLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorCapacityLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorLimitLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorAddressLine.IsEnabled = (bool)cb.IsChecked;
                        CoinAcceptorPinLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
                CoinAcceptorSettingsSave.IsEnabled = true;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        #endregion CoinAcceptor

        #region CashDispenser
        protected void CashDispenserButton_Click(object sender, EventArgs e)
        {
            try
            {
                CashDispenserGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                CashDispenserSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void CashDispenserSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool changed = false;
                if (cashierForm != null)
                {
                    int CashDispenserTypeId = (cashierForm.deviceCMModel.CashDispenserTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.CashDispenserTypeId;
                    int CashDispenserTopBoxChecked = 0, CashDispenserBottomBoxChecked = 0;
                    for (int i = 0; i < cashierForm.Banknotes.Count; i++)
                    {
                        if ((bool)((RadioButton)CashDispenserTopBoxNominal.Children[i]).IsChecked)
                            CashDispenserTopBoxChecked = i;
                        if ((bool)((RadioButton)CashDispenserBottomBoxNominal.Children[i]).IsChecked)
                            CashDispenserBottomBoxChecked = i;
                    }
                    if (CashDispenserModel.Text == "UBA RC")
                        if (CashDispenserTopBoxChecked == CashDispenserBottomBoxChecked)
                        {
                            MessageBox.Show(Properties.Resources.ErrorNominalSettings, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                    try
                    {
                        lock (cashierForm.db_lock)
                        {
                            cashierForm.ToLog(1, Properties.Resources.ToLogCashDispenserSave);
                            // Cохранение параметров диспенсера банкнот
                            // Наличие устройства
                            cashierForm.deviceCMModel.CashDispenserExist = CashDispenserExists.IsChecked;
                            // Текущая модель
                            if (CashDispenserModelSelectedIndex >= 0)
                                cashierForm.deviceCMModel.CashDispenserTypeId = cashierForm.CashDispenserExecutableDevices[CashDispenserModelSelectedIndex].Id;
                            if (cashierForm.EmulatorEnabled)
                                cashierForm.EmulatorForm.Emulator.DispenserLabel.Text = Properties.Resources.CashDispenser + " " + CashDispenserModel.Text;
                            for (int i = 0; i < cashierForm.Banknotes.Count; i++)
                            {
                                BanknoteModelT banknote = cashierForm.Banknotes[i];

                                for (int j = 0; j < CashDispenserTopBoxNominal.Children.Count; j++)
                                {
                                    if ((bool)((RadioButton)CashDispenserTopBoxNominal.Children[i]).IsChecked)
                                    {
                                        if (banknote.Value == (string)((RadioButton)CashDispenserTopBoxNominal.Children[i]).Content)
                                        {
                                            cashierForm.deviceCMModel.NominalUpId = banknote.Id;
                                            TopBoxNominal.Text = banknote.Value;
                                            cashierForm.UpNominal = Convert.ToDecimal(banknote.Value);
                                        }
                                    }
                                }

                                for (int j = 0; j < CashDispenserBottomBoxNominal.Children.Count; j++)
                                {
                                    if ((bool)((RadioButton)CashDispenserBottomBoxNominal.Children[i]).IsChecked)
                                    {
                                        if (banknote.Value == (string)((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Content)
                                        {
                                            cashierForm.deviceCMModel.NominalDownId = banknote.Id;
                                            BottomBoxNominal.Text = banknote.Value;
                                            cashierForm.DownNominal = Convert.ToDecimal(banknote.Value);
                                        }
                                    }
                                }
                            }

                            try
                            {
                                cashierForm.deviceCMModel.MaxCountUp = Convert.ToInt16(CashDispenserTopBoxCapacity.Text);
                            }
                            catch
                            {
                                cashierForm.deviceCMModel.MaxCountUp = 0;
                            }
                            try
                            {
                                cashierForm.deviceCMModel.MaxCountDown = Convert.ToInt16(CashDispenserBottomBoxCapacity.Text);
                            }
                            catch
                            {
                                cashierForm.deviceCMModel.MaxCountDown = 0;
                            }

                            // DispenserPort
                            if (CashDispenserPortSelectedIndex >= 0)
                            {
                                if (cashierForm.deviceCMModel.CashDispenserComPortId != CashDispenserPortSelectedIndex)
                                {
                                    changed = true;
                                    cashierForm.deviceCMModel.CashDispenserComPortId = CashDispenserPortSelectedIndex;
                                }
                            }
                            else
                            {
                                if (cashierForm.deviceCMModel.CashDispenserComPortId != 1)
                                {
                                    changed = true;
                                    cashierForm.deviceCMModel.CashDispenserComPortId = 1;
                                }
                            }

                            List<object> lst = new List<object>();
                            if (cashierForm.deviceCMModel.CashDispenserExist != null)
                            {
                                lst.Add("CashDispenserExist");
                                lst.Add(cashierForm.deviceCMModel.CashDispenserExist);
                            }
                            if (cashierForm.deviceCMModel.CashDispenserTypeId != null)
                            {
                                lst.Add("CashDispenserTypeId");
                                lst.Add(cashierForm.deviceCMModel.CashDispenserTypeId);
                            }
                            if (cashierForm.deviceCMModel.NominalUpId != null)
                            {
                                lst.Add("NominalUpId");
                                lst.Add(cashierForm.deviceCMModel.NominalUpId);
                            }
                            if (cashierForm.deviceCMModel.NominalDownId != null)
                            {
                                lst.Add("NominalDownId");
                                lst.Add(cashierForm.deviceCMModel.NominalDownId);
                            }
                            if (cashierForm.deviceCMModel.MaxCountUp != null)
                            {
                                lst.Add("MaxCountUp");
                                lst.Add(cashierForm.deviceCMModel.MaxCountUp);
                            }
                            if (cashierForm.deviceCMModel.MaxCountDown != null)
                            {
                                lst.Add("MaxCountDown");
                                lst.Add(cashierForm.deviceCMModel.MaxCountDown);
                            }
                            if (cashierForm.deviceCMModel.CashDispenserComPortId != null)
                            {
                                lst.Add("CashDispenserComPortId");
                                lst.Add(cashierForm.deviceCMModel.CashDispenserComPortId);
                            }
                            lock (cashierForm.db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }

                        if (changed || ((CashDispenserTypeId >= 0) && (cashierForm.deviceCMModel.CashDispenserTypeId != null) && cashierForm.deviceCMModel.CashDispenserTypeId != CashDispenserTypeId))
                        {
                            if (cashierForm.CashDispenser != null)
                            {
                                cashierForm.CashDispenser.Close();
                                cashierForm.CashDispenser = null;
                                cashierForm.GetCashDispenserStatus();
                            }
                        }

                        cashierForm.SetNotesNominal();

                        cashierForm.SettingsLastUpdate = DateTime.Now;
                        CashDispenserGetSettings();

                        SettingsReturnButton();
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void CashDispenserCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CashDispenserGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void CashDispenserGetSettings()
        {
            int i;

            if (cashierForm != null)
            {
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        bool exists;
                        // Чтение сохраненных параметров диспенсера банкнот
                        // Наличие устройства
                        try
                        {
                            CashDispenserExists.IsChecked = (bool)cashierForm.deviceCMModel.CashDispenserExist;
                            exists = (bool)cashierForm.deviceCMModel.CashDispenserExist;
                        }
                        catch
                        {
                            CashDispenserExists.IsChecked = false;
                            exists = false;
                        }
                        CashDelivery.IsEnabled = exists;
                        lock (cashierForm.DeliveryNominals)
                            if (cashierForm.CashDeliveryEnabled != exists)
                                cashierForm.CashDeliveryEnabled = exists;

                        CashDispenserModelLine.IsEnabled = exists;
                        CashDispenserPortLine.IsEnabled = exists;
                        CashDispenserUpNominalLine.IsEnabled = exists;
                        CashDispenserDownNominalLine.IsEnabled = exists;
                        CashDispenserUpCapacityLine.IsEnabled = exists;
                        CashDispenserDownCapacityLine.IsEnabled = exists;
                        CashDispenserResetLine.IsEnabled = exists;

                        if ((CashAcceptorModelSelectedIndex >= 0) &&
                            (cashierForm.CashAcceptorExecutableDevices.Count - 1 >= CashAcceptorModelSelectedIndex) &&
                            (cashierForm.CashAcceptorExecutableDeviceType == ExecutableDeviceType.enCashRecycler))
                        {
                            cashierForm.CashDispenserModel = CashAcceptorModel.Text;
                            /*for (i = 0; i < cashierForm.CashDispenserExecutableDevices.Count; i++)
                                if (cashierForm.CashDispenserExecutableDevices[i].Id == cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id)
                                {
                                    cashierForm.deviceCMModel.CashDispenserTypeId = cashierForm.CashAcceptorExecutableDevices[CashAcceptorModelSelectedIndex].Id;
                                    CashDispenserModelSelectedIndex = i;
                                    break;
                                }
                            */
                            /*if (CashDispenserModelLine.IsEnabled)
                                CashDispenserModelLine.IsEnabled = false;*/
                            if (CashDispenserModelLine.Visibility != Visibility.Hidden)
                                CashDispenserModelLine.Visibility = Visibility.Hidden;
                            if (CashDispenserPortLine.Visibility != Visibility.Hidden)
                                CashDispenserPortLine.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            // Текущая модель
                            try
                            {
                                CashDispenserModelSelectedIndex = -1;
                                CashDispenserModel.Text = Properties.Resources.NotSelected0;
                                for (i = 0; i < cashierForm.CashDispenserExecutableDevices.Count; i++)
                                    if (cashierForm.CashDispenserExecutableDevices[i].Id == cashierForm.deviceCMModel.CashDispenserTypeId)
                                    {
                                        CashDispenserModelSelectedIndex = i;
                                        CashDispenserModel.Text = cashierForm.CashDispenserExecutableDevices[i].Name;
                                        break;
                                    }
                            }
                            catch
                            {
                                CashDispenserModelSelectedIndex = -1;
                            }
                            cashierForm.CashDispenserModel = CashDispenserModel.Text;
                            if (cashierForm.EmulatorEnabled)
                                if (CashDispenserModelSelectedIndex >= 0 && cashierForm.CashDispenserExecutableDevices.Count < CashDispenserModelSelectedIndex)
                                    cashierForm.EmulatorForm.Emulator.DispenserLabel.Text = Properties.Resources.CashDispenser + " " + cashierForm.CashDispenserExecutableDevices[CashDispenserModelSelectedIndex].Name;

                            /*if (!CashDispenserModelLine.IsEnabled)
                                CashDispenserModelLine.IsEnabled = true;*/
                            if (CashDispenserModelLine.Visibility != Visibility.Visible)
                                CashDispenserModelLine.Visibility = Visibility.Visible;
                            /*if ((cashierForm.CashDispenserExecutableDevices.Count - 1 < CashDispenserModelSelectedIndex) ||
                                (cashierForm.CashDispenserExecutableDevices[CashDispenserModelSelectedIndex].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler))
                            {
                                cashierForm.deviceCMModel.CashDispenserTypeId = null;
                                CashDispenserModelSelectedIndex = -1;
                            }*/
                            if (CashDispenserPortLine.Visibility != Visibility.Visible)
                                CashDispenserPortLine.Visibility = Visibility.Visible;
                        }


                        //if (CashDispenserTopBoxNominal.Children.Count == 0)
                        {
                            for (i = 0; i < cashierForm.Banknotes.Count; i++)
                            {
                                BanknoteModelT banknote = cashierForm.Banknotes[i];
                                if (CashDispenserModel.Text == "UBA RC")
                                {
                                    if ((banknote.Value == "10") || (banknote.Value == "50") || (banknote.Value == "100") || (banknote.Value == "500"))
                                    {
                                        ((RadioButton)CashDispenserTopBoxNominal.Children[i]).Content = banknote.Value;
                                        ((RadioButton)CashDispenserTopBoxNominal.Children[i]).Visibility = Visibility.Visible;
                                        ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Content = banknote.Value;
                                        ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Visibility = Visibility.Visible;
                                    }
                                    else
                                    {
                                        ((RadioButton)CashDispenserTopBoxNominal.Children[i]).Visibility = Visibility.Hidden;
                                        ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Visibility = Visibility.Hidden;
                                    }
                                }
                                else
                                {
                                    ((RadioButton)CashDispenserTopBoxNominal.Children[i]).Content = banknote.Value;
                                    ((RadioButton)CashDispenserTopBoxNominal.Children[i]).Visibility = Visibility.Visible;
                                    ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Content = banknote.Value;
                                    ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).Visibility = Visibility.Visible;
                                }

                                // Выбранный номинал верхнего ящика
                                if ((cashierForm.deviceCMModel.NominalUpId != null) && (banknote.Id == cashierForm.deviceCMModel.NominalUpId))
                                {
                                    ((RadioButton)CashDispenserTopBoxNominal.Children[i]).IsChecked = true;
                                    cashierForm.UpNominal = Convert.ToDecimal(banknote.Value);
                                    TopBoxNominal.Text = banknote.Value;
                                }

                                // Выбранный номинал нижнего ящика
                                if ((cashierForm.deviceCMModel.NominalDownId != null) && (banknote.Id == cashierForm.deviceCMModel.NominalDownId))
                                {
                                    ((RadioButton)CashDispenserBottomBoxNominal.Children[i]).IsChecked = true;
                                    cashierForm.DownNominal = Convert.ToDecimal(banknote.Value);
                                    BottomBoxNominal.Text = banknote.Value;
                                }
                            }
                        }

                        try
                        {
                            CashDispenserTopBoxCapacity.Text = cashierForm.deviceCMModel.MaxCountUp.ToString();
                        }
                        catch
                        {
                            CashDispenserTopBoxCapacity.Text = "0";
                        }

                        try
                        {
                            CashDispenserBottomBoxCapacity.Text = cashierForm.deviceCMModel.MaxCountDown.ToString();
                        }
                        catch
                        {
                            CashDispenserBottomBoxCapacity.Text = "0";
                        }

                        TopBoxCountMaxLabel.Text = CashDispenserTopBoxCapacity.Text;
                        BottomBoxCountMaxLabel.Text = CashDispenserBottomBoxCapacity.Text;

                        try
                        {
                            CashDispenserPortSelectedIndex = (int)cashierForm.deviceCMModel.CashDispenserComPortId;
                        }
                        catch
                        {
                            CashDispenserPortSelectedIndex = 1;
                        }
                        CashDispenserPort.Text = "COM" + (CashDispenserPortSelectedIndex).ToString();
                        cashierForm.CashDispenserPort = CashDispenserPort.Text;
                    }
                    CashDispenserSettingsSave.IsEnabled = false;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void MaxCountUp_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CashDispenserTopBoxCapacity.Text != AddingSummForm.Sum)
                            CashDispenserSettingsSave.IsEnabled = true;
                        CashDispenserTopBoxCapacity.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CashDispenserSettingsReturn.Focus();
        }
        private void MaxCountDown_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CashDispenserBottomBoxCapacity.Text != AddingSummForm.Sum)
                            CashDispenserSettingsSave.IsEnabled = true;
                        CashDispenserBottomBoxCapacity.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CashDispenserSettingsReturn.Focus();
        }
        private void CashDispenserReset_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!cashierForm.EmulatorEnabled) || (!cashierForm.EmulatorForm.Emulator.CashDispenserVirt.Checked))
                    if (cashierForm.CashDispenser != null)
                        cashierForm.CashDispenser.Reset();
                if (!cashierForm.CashAcceptorInhibit)
                    cashierForm.CashAcceptorInhibit = true;
                cashierForm.MoveForwardResult = null;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CashDispenserCheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "CashDispenserExists":
                        CashDispenserModelLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserPortLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserUpNominalLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserDownNominalLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserUpCapacityLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserDownCapacityLine.IsEnabled = (bool)cb.IsChecked;
                        CashDispenserResetLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
                CashDispenserSettingsSave.IsEnabled = true;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CashDispenserModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CashDispenserModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CashDispenserExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CashDispenserModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.CashDispenserExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CashDispenserModelSelectedIndex != p.SelectedIndex)
                            CashDispenserSettingsSave.IsEnabled = true;
                        CashDispenserModelSelectedIndex = p.SelectedIndex;
                        CashDispenserModel.Text = cashierForm.CashDispenserExecutableDevices[CashDispenserModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CashDispenserSettingsReturn.Focus();
        }
        private void CashDispenserPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CashDispenserPort;
                ((RadioButton)p.ItemsList.Items[CashDispenserPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CashDispenserPortSelectedIndex != p.SelectedIndex + 1)
                            CashDispenserSettingsSave.IsEnabled = true;

                        CashDispenserPortSelectedIndex = p.SelectedIndex + 1;
                        CashDispenserPort.Text = (string)((RadioButton)p.ItemsList.Items[CashDispenserPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CashDispenserSettingsReturn.Focus();
        }
        private void CashDispenserNominal_Click(object sender, RoutedEventArgs e)
        {
            CashDispenserSettingsSave.IsEnabled = true;
        }
        #endregion CashDispenser

        #region CoinHopper
        protected void CoinHopperButton_Click(object sender, EventArgs e)
        {
            try
            {
                CoinHopperGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                CoinHopperSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void CoinHopperSaveButton_Click(object sender, EventArgs e)
        {
            bool changed1 = false, changed2 = false;
            if (cashierForm != null)
            {
                int HopperTypeId = (cashierForm.deviceCMModel.HopperTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.HopperTypeId;
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        // Cохранение параметров хоппера монет
                        // Наличие устройства
                        cashierForm.deviceCMModel.HopperExist = CoinHopperExists.IsChecked;
                        // Текущая модель
                        if (CoinHopperModelSelectedIndex >= 0)
                            cashierForm.deviceCMModel.HopperTypeId = cashierForm.HopperExecutableDevices[CoinHopperModelSelectedIndex].Id;
                        // Выбранный номинал верхнего ящика
                        string Value = "";
                        for (int i = 0; i < cashierForm.Coins.Count; i++)
                        {
                            CoinModelT coin = cashierForm.Coins[i];
                            if (Value != cashierForm.Coins[i].Value)
                            {
                                Value = cashierForm.Coins[i].Value;

                                // Выбранный номинал левого ящика
                                for (int j = 0; j < CoinHopperLeftBoxNominal.Children.Count; j++)
                                {
                                    if ((bool)((RadioButton)CoinHopperLeftBoxNominal.Children[j]).IsChecked)
                                    {
                                        if (Value == (string)((RadioButton)CoinHopperLeftBoxNominal.Children[j]).Content)
                                        {
                                            cashierForm.deviceCMModel.NominalLeftId = coin.Id;
                                            LeftBoxNominal.Text = Value;
                                            cashierForm.Nominal1 = Convert.ToDecimal(Value);
                                        }
                                    }
                                }

                                // Выбранный номинал правого ящика
                                for (int j = 0; j < CoinHopperRightBoxNominal.Children.Count; j++)
                                {
                                    if ((bool)((RadioButton)CoinHopperRightBoxNominal.Children[j]).IsChecked)
                                    {
                                        if (coin.Value == (string)((RadioButton)CoinHopperRightBoxNominal.Children[j]).Content)
                                        {
                                            cashierForm.deviceCMModel.NominalRightId = coin.Id;
                                            RightBoxNominal.Text = Value;
                                            cashierForm.Nominal2 = Convert.ToDecimal(Value);
                                        }
                                    }
                                }
                            }
                        }
                        try
                        {
                            cashierForm.deviceCMModel.MaxCount1 = Convert.ToInt16(CoinHopperLeftBoxCapacity.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.MaxCount1 = 0;
                        }
                        try
                        {
                            cashierForm.deviceCMModel.MaxCount2 = Convert.ToInt16(CoinHopperRightBoxCapacity.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.MaxCount2 = 0;
                        }

                        // Hopper1Port
                        if (CoinHopper1PortSelectedIndex > 0)
                        {
                            if (cashierForm.deviceCMModel.Hopper1ComPortId != CoinHopper1PortSelectedIndex)
                            {
                                changed1 = true;
                                cashierForm.deviceCMModel.Hopper1ComPortId = CoinHopper1PortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.Hopper1ComPortId != 1)
                            {
                                changed1 = true;
                                cashierForm.deviceCMModel.Hopper1ComPortId = 1;
                            }
                        }
                        // Hopper2Port
                        /*if (CoinHopper2PortSelectedIndex > 0)
                        {
                            if (cashierForm.deviceCMModel.Hopper2ComPortId != CoinHopper2PortSelectedIndex)
                            {
                                changed2 = true;
                                cashierForm.deviceCMModel.Hopper2ComPortId = CoinHopper2PortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.Hopper2ComPortId != 1)
                            {
                                changed2 = true;
                                cashierForm.deviceCMModel.Hopper2ComPortId = 1;
                            }
                        }*/

                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.HopperExist != null)
                        {
                            lst.Add("HopperExist");
                            lst.Add(cashierForm.deviceCMModel.HopperExist);
                        }
                        if (cashierForm.deviceCMModel.HopperTypeId != null)
                        {
                            lst.Add("HopperTypeId");
                            lst.Add(cashierForm.deviceCMModel.HopperTypeId);
                        }
                        if (cashierForm.deviceCMModel.NominalRightId != null)
                        {
                            lst.Add("NominalRightId");
                            lst.Add(cashierForm.deviceCMModel.NominalRightId);
                        }
                        if (cashierForm.deviceCMModel.NominalLeftId != null)
                        {
                            lst.Add("NominalLeftId");
                            lst.Add(cashierForm.deviceCMModel.NominalLeftId);
                        }
                        if (cashierForm.deviceCMModel.MaxCount1 != null)
                        {
                            lst.Add("MaxCount1");
                            lst.Add(cashierForm.deviceCMModel.MaxCount1);
                        }
                        if (cashierForm.deviceCMModel.MaxCount2 != null)
                        {
                            lst.Add("MaxCount2");
                            lst.Add(cashierForm.deviceCMModel.MaxCount2);
                        }
                        if (cashierForm.deviceCMModel.Hopper1ComPortId != null)
                        {
                            lst.Add("Hopper1ComPortId");
                            lst.Add(cashierForm.deviceCMModel.Hopper1ComPortId);
                        }
                        /*if (cashierForm.deviceCMModel.Hopper2ComPortId != null)
                        {
                            lst.Add("Hopper2ComPortId");
                            lst.Add(cashierForm.deviceCMModel.Hopper2ComPortId);
                        }*/
                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }
                    if ((HopperTypeId >= 0) &&
                        (cashierForm.deviceCMModel.HopperTypeId != null) &&
                        cashierForm.deviceCMModel.HopperTypeId != HopperTypeId)
                    {
                        if (cashierForm.CoinHopper != null)
                            cashierForm.CoinHopper.Close();
                    }
                    else
                    {
                        if (changed1 && cashierForm.CoinHopper != null)
                            cashierForm.CoinHopper.Close();
                    }

                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    CoinHopperGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void CoinHopperCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CoinHopperGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void CoinHopperGetSettings()
        {
            int i;

            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    lock (cashierForm.db_lock)
                    {
                        // Чтение сохраненных параметров хопера монет
                        // Наличие устройства
                        try
                        {
                            CoinHopperExists.IsChecked = (bool)cashierForm.deviceCMModel.HopperExist;
                            exists = (bool)cashierForm.deviceCMModel.HopperExist;
                        }
                        catch
                        {
                            CoinHopperExists.IsChecked = false;
                            exists = false;
                        }
                        //CoinDelivery.IsEnabled = exists;
                        if (cashierForm.deviceCMModel.CashierType == 0)
                        {
                            if (exists)
                            {
                                if (CoinDelivery.Visibility != Visibility.Visible)
                                    CoinDelivery.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                if (CoinDelivery.Visibility != Visibility.Hidden)
                                    CoinDelivery.Visibility = Visibility.Hidden;
                                CoinDelivery.Visibility = Visibility.Hidden;
                            }
                        }

                        lock (cashierForm.DeliveryNominals)
                            cashierForm.CoinDeliveryEnabled = exists;

                        CoinHopperModelLine.IsEnabled = exists;
                        CoinHopperLeftPortLine.IsEnabled = exists;
                        //CoinHopperRightPortLine.IsEnabled = exists;
                        CoinHopperCleanLine.IsEnabled = exists;
                        CoinHopperLeftNominalLine.IsEnabled = exists;
                        CoinHopperRightNominalLine.IsEnabled = exists;
                        CoinHopperLeftCapacityLine.IsEnabled = exists;
                        CoinHopperRightCapacityLine.IsEnabled = exists;
                        // Текущая модель
                        try
                        {
                            for (i = 0; i < cashierForm.HopperExecutableDevices.Count; i++)
                                if (cashierForm.HopperExecutableDevices[i].Id == cashierForm.deviceCMModel.HopperTypeId)
                                {
                                    CoinHopperModelSelectedIndex = i;
                                    CoinHopperModel.Text = cashierForm.HopperExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            CoinHopperModelSelectedIndex = 0;
                        }
                        cashierForm.CoinHopperModel = CoinHopperModel.Text;
                        if (cashierForm.EmulatorEnabled && (CoinHopperModelSelectedIndex >= 0))
                        {
                            cashierForm.EmulatorForm.Emulator.HopperLabel1.Text = "Hopper " + cashierForm.HopperExecutableDevices[CoinHopperModelSelectedIndex].Name + " - 1";
                            cashierForm.EmulatorForm.Emulator.HopperLabel2.Text = "Hopper " + cashierForm.HopperExecutableDevices[CoinHopperModelSelectedIndex].Name + " - 2";
                        }

                        for (i = 0; i < 6; i++)
                        {
                            ((RadioButton)CoinHopperLeftBoxNominal.Children[i]).Visibility = Visibility.Hidden;
                            ((RadioButton)CoinHopperRightBoxNominal.Children[i]).Visibility = Visibility.Hidden;
                        }

                        int ic = 0;
                        string Value = "";
                        for (i = 0; i < cashierForm.Coins.Count; i++)
                        {
                            if (ic < 6)
                            {
                                if (Value != cashierForm.Coins[i].Value)
                                {
                                    Value = cashierForm.Coins[i].Value;

                                    CoinModelT coin = cashierForm.Coins[i];
                                    ((RadioButton)CoinHopperLeftBoxNominal.Children[ic]).Content = Value;
                                    ((RadioButton)CoinHopperLeftBoxNominal.Children[ic]).Visibility = Visibility.Visible;
                                    ((RadioButton)CoinHopperRightBoxNominal.Children[ic]).Content = Value;
                                    ((RadioButton)CoinHopperRightBoxNominal.Children[ic]).Visibility = Visibility.Visible;
                                    // Выбранный номинал левого ящика
                                    if ((cashierForm.deviceCMModel.NominalLeftId != null) && (coin.Id == cashierForm.deviceCMModel.NominalLeftId))
                                    {
                                        ((RadioButton)CoinHopperLeftBoxNominal.Children[ic]).IsChecked = true;
                                        cashierForm.Nominal1 = Convert.ToDecimal(Value);
                                        LeftBoxNominal.Text = Value;
                                    }

                                    // Выбранный номинал нижнего ящика
                                    if ((cashierForm.deviceCMModel.NominalRightId != null) && (coin.Id == cashierForm.deviceCMModel.NominalRightId))
                                    {
                                        ((RadioButton)CoinHopperRightBoxNominal.Children[ic]).IsChecked = true;
                                        cashierForm.Nominal2 = Convert.ToDecimal(Value);
                                        RightBoxNominal.Text = Value;
                                    }
                                    ic++;
                                }
                            }
                        }

                        try
                        {
                            CoinHopperLeftBoxCapacity.Text = cashierForm.deviceCMModel.MaxCount1.ToString();
                        }
                        catch
                        {
                            CoinHopperLeftBoxCapacity.Text = "0";
                        }

                        LeftBoxCountMaxLabel.Text = CoinHopperLeftBoxCapacity.Text;

                        try
                        {
                            CoinHopperRightBoxCapacity.Text = cashierForm.deviceCMModel.MaxCount2.ToString();
                        }
                        catch
                        {
                            CoinHopperRightBoxCapacity.Text = "0";
                        }

                        RightBoxCountMaxLabel.Text = CoinHopperRightBoxCapacity.Text;
                        try
                        {
                            CoinHopper1PortSelectedIndex = (int)cashierForm.deviceCMModel.Hopper1ComPortId;
                        }
                        catch
                        {
                            CoinHopper1PortSelectedIndex = 1;
                        }
                        CoinHopper1Port.Text = "COM" + (CoinHopper1PortSelectedIndex).ToString();
                        cashierForm.CoinHopperPort = CoinHopper1Port.Text;
                        /*try
                        {
                            CoinHopper2PortSelectedIndex = (int)cashierForm.deviceCMModel.Hopper2ComPortId;
                        }
                        catch
                        {
                            CoinHopper2PortSelectedIndex = 1;
                        }
                        CoinHopper2Port.Text = "COM" + (CoinHopper2PortSelectedIndex).ToString();
                        cashierForm.CoinHopper2Port = CoinHopper2Port.Text;*/
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
                CoinHopperSettingsSave.IsEnabled = false;
            }
        }
        private void MaxCountLeft_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CoinHopperLeftBoxCapacity.Text != AddingSummForm.Sum)
                            CoinHopperSettingsSave.IsEnabled = true;
                        CoinHopperLeftBoxCapacity.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinHopperSettingsReturn.Focus();
        }
        private void MaxCountRight_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CoinHopperRightBoxCapacity.Text != AddingSummForm.Sum)
                            CoinHopperSettingsSave.IsEnabled = true;
                        CoinHopperRightBoxCapacity.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinHopperSettingsReturn.Focus();
        }
        private void CoinHopperCheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "CoinHopperExists":
                        CoinHopperModelLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperLeftPortLine.IsEnabled = (bool)cb.IsChecked;
                        //CoinHopperRightPortLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperCleanLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperLeftNominalLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperRightNominalLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperLeftCapacityLine.IsEnabled = (bool)cb.IsChecked;
                        CoinHopperRightCapacityLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            CoinHopperSettingsSave.IsEnabled = true;
        }
        private void CoinHopperModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CoinHopperModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.HopperExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CoinHopperModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.HopperExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CoinHopperModelSelectedIndex != p.SelectedIndex)
                            CoinHopperSettingsSave.IsEnabled = true;
                        CoinHopperModelSelectedIndex = p.SelectedIndex;
                        CoinHopperModel.Text = cashierForm.HopperExecutableDevices[CoinHopperModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CoinHopperSettingsReturn.Focus();
        }
        private void CoinHopper1Port_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CoinHopperPort;
                ((RadioButton)p.ItemsList.Items[CoinHopper1PortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CoinHopper1PortSelectedIndex != p.SelectedIndex + 1)
                            CoinHopperSettingsSave.IsEnabled = true;

                        CoinHopper1PortSelectedIndex = p.SelectedIndex + 1;
                        CoinHopper1Port.Text = (string)((RadioButton)p.ItemsList.Items[CoinHopper1PortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CoinHopperSettingsReturn.Focus();
        }
        /*private void CoinHopper2Port_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CoinHopper2Port;
                ((RadioButton)p.ItemsList.Items[CoinHopper2PortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CoinHopper2PortSelectedIndex != p.SelectedIndex + 1)
                            CoinHopperSettingsSave.IsEnabled = true;

                        CoinHopper2PortSelectedIndex = p.SelectedIndex + 1;
                        CoinHopper2Port.Text = (string)((RadioButton)p.ItemsList.Items[CoinHopper2PortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            CoinHopperSettingsReturn.Focus();
        }*/
        private void coinHopperNominal_Click(object sender, RoutedEventArgs e)
        {
            CoinHopperSettingsSave.IsEnabled = true;
        }
        #endregion CoinHopper

        #region BankModule
        protected void BankModuleButton_Click(object sender, EventArgs e)
        {
            try
            {
                BankModuleGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                BankModuleSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        public void BankModuleGetSettings()
        {
            int i;
            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    // Чтение сохраненных параметров банковского модуля
                    // Наличие устройства
                    try
                    {
                        exists = (bool)cashierForm.deviceCMModel.BankModuleExist;
                        BankModuleExists.IsChecked = exists;
                    }
                    catch
                    {
                        exists = false;
                        BankModuleExists.IsChecked = false;
                    }
                    BankModuleModelLine.IsEnabled = exists;
                    BankModuleCardReaderLine.IsEnabled = exists;
                    BankModulePayPassLine.IsEnabled = exists;

                    // Текущая модель
                    try
                    {
                        for (i = 0; i < cashierForm.BankModuleExecutableDevices.Count; i++)
                            if (cashierForm.BankModuleExecutableDevices[i].Id == cashierForm.deviceCMModel.BankModuleTypeId)
                            {
                                BankModuleModelSelectedIndex = i;
                                BankModuleModel.Text = cashierForm.BankModuleExecutableDevices[i].Name;
                                break;
                            }
                    }
                    catch
                    {
                        BankModuleModelSelectedIndex = 0;
                    }
                    cashierForm.BankModuleModel = BankModuleModel.Text;
                    /*try
                    {
                        BankModulePortSelectedIndex = (int)cashierForm.deviceCMModel.BankModuleComPortId;
                    }
                    catch
                    {
                        BankModulePortSelectedIndex = 1;
                    }*/
                    //            cashierForm.BankModuleLabel.Text = "Банковский модуль " + BankModuleСomboBox.Items[BankModuleСomboBox.SelectedIndex].ToString();
                    // Наличие считываещего устройства
                    try
                    {
                        BankModuleReaderExists.IsChecked = (bool)cashierForm.deviceCMModel.Reader;
                    }
                    catch
                    {
                        BankModuleReaderExists.IsChecked = false;
                    }
                    // Наличие устройства PaPas
                    try
                    {
                        BankModulePayPassExists.IsChecked = (bool)cashierForm.deviceCMModel.PayPass;
                    }
                    catch
                    {
                        BankModulePayPassExists.IsChecked = false;
                    }
                    BankModuleSettingsSave.IsEnabled = false;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void BankModuleSaveButton_Click(object sender, EventArgs e)
        {
            bool changed = false;
            if (cashierForm != null)
            {
                int BankModuleTypeId = (cashierForm.deviceCMModel.BankModuleTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.BankModuleTypeId;
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        // Сохранение параметров банковского модуля
                        // Наличие устройства
                        cashierForm.deviceCMModel.BankModuleExist = BankModuleExists.IsChecked;
                        // Текущая модель
                        if (BankModuleModelSelectedIndex >= 0)
                            cashierForm.deviceCMModel.BankModuleTypeId = cashierForm.BankModuleExecutableDevices[BankModuleModelSelectedIndex].Id;
                        /*try
                        {
                            if (BankModulePortSelectedIndex > 0)
                                cashierForm.deviceCMModel.BankModuleComPortId = BankModulePortSelectedIndex;
                            else
                                cashierForm.deviceCMModel.BankModuleComPortId = 1;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.ban BankModuleComPortId = 1;
                        }*/
                        // Наличие считываещего устройства
                        cashierForm.deviceCMModel.Reader = BankModuleReaderExists.IsChecked;
                        // Наличие устройства PaPas
                        cashierForm.deviceCMModel.PayPass = BankModulePayPassExists.IsChecked;
                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.BankModuleExist != null)
                        {
                            lst.Add("BankModuleExist");
                            lst.Add(cashierForm.deviceCMModel.BankModuleExist);
                        }
                        if (cashierForm.deviceCMModel.BankModuleTypeId != null)
                        {
                            lst.Add("BankModuleTypeId");
                            lst.Add(cashierForm.deviceCMModel.BankModuleTypeId);
                        }
                        if (cashierForm.deviceCMModel.Reader != null)
                        {
                            lst.Add("Reader");
                            lst.Add(cashierForm.deviceCMModel.Reader);
                        }
                        if (cashierForm.deviceCMModel.PayPass != null)
                        {
                            lst.Add("PayPass");
                            lst.Add(cashierForm.deviceCMModel.PayPass);
                        }

                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }
                    if (changed || ((BankModuleTypeId >= 0) && (cashierForm.deviceCMModel.BankModuleTypeId != null) && cashierForm.deviceCMModel.BankModuleTypeId != BankModuleTypeId))
                    {
                        if (cashierForm.BankModule != null)
                        {
                            //cashierForm.BankModule.Close();
                            cashierForm.BankModule = null;
                        }
                    }
                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    BankModuleGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void BankModuleCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    BankModuleGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void BankModuleCheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "BankModuleExists":
                        BankModuleModelLine.IsEnabled = (bool)cb.IsChecked;
                        BankModuleCardReaderLine.IsEnabled = (bool)cb.IsChecked;
                        BankModulePayPassLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            BankModuleSettingsSave.IsEnabled = true;
        }
        private void BankModuleModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.BankModuleModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.BankModuleExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (BankModuleModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.BankModuleExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (BankModuleModelSelectedIndex != p.SelectedIndex)
                            BankModuleSettingsSave.IsEnabled = true;
                        BankModuleModelSelectedIndex = p.SelectedIndex;
                        BankModuleModel.Text = cashierForm.BankModuleExecutableDevices[BankModuleModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            BankModuleSettingsReturn.Focus();
        }
        private void BankModulePort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.BankModulePort;
                ((RadioButton)p.ItemsList.Items[BankModulePortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (BankModulePortSelectedIndex != p.SelectedIndex + 1)
                            BankModuleSettingsSave.IsEnabled = true;

                        BankModulePortSelectedIndex = p.SelectedIndex + 1;
                        BankModulePort.Text = (string)((RadioButton)p.ItemsList.Items[BankModulePortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            BankModuleSettingsReturn.Focus();
        }
        #endregion BankModule

        #region KKM
        protected void KKMButton_Click(object sender, EventArgs e)
        {
            try
            {
                KKMGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                KKMSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void KKMSaveButton_Click(object sender, EventArgs e)
        {
            bool changed = false;
            if (cashierForm != null)
            {
                try
                {
                    int KKMTypeId = (cashierForm.deviceCMModel.KKMTypeId == null) ? -1 : (int)cashierForm.deviceCMModel.KKMTypeId;
                    lock (cashierForm.db_lock)
                    {
                        // Сохранение параметров приемника банкнот
                        // Наличие устройства
                        cashierForm.deviceCMModel.KKMExists = KKMExists.IsChecked;

                        // Текущая модель
                        if (KKMModelSelectedIndex >= 0)
                            cashierForm.deviceCMModel.KKMTypeId = cashierForm.KKMExecutableDevices[KKMModelSelectedIndex].Id;

                        // KKMPort
                        if (KKMPortSelectedIndex > 0)
                        {
                            if (cashierForm.deviceCMModel.KKMComPortId != KKMPortSelectedIndex)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.KKMComPortId = KKMPortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.KKMComPortId != 1)
                            {
                                changed = true;
                                cashierForm.deviceCMModel.KKMComPortId = 1;
                            }
                        }

                        try
                        {
                            cashierForm.deviceCMModel.ChequeTimeOut = Convert.ToByte(KKMChequeTime.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.ChequeTimeOut = 30;
                        }

                        cashierForm.deviceCMModel.ShiftAutoClose = ShiftAutoSwitch.IsChecked;
                        cashierForm.deviceCMModel.ShiftNewShiftTime = Convert.ToDateTime(AutoShiftTimeHour.Text + ":" + AutoShiftTimeMin.Text);
                        byte[] encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(KKMPassword.Password);
                        cashierForm.deviceCMModel.CommandPassword = cashierForm.cryp.ByteAToString(encrypted);
                        encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(KKMModePassword.Password);
                        cashierForm.deviceCMModel.RegistartionPass = cashierForm.cryp.ByteAToString(encrypted);
                        /*encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(WithOutCleaningPass.Text);
                        cashierForm.deviceCMModel.WithOutCleaningPass = cashierForm.cryp.ByteAToString(encrypted);
                        encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(WithCleaningPass.Text);
                        cashierForm.deviceCMModel.WithCleaningPass = cashierForm.cryp.ByteAToString(encrypted);*/
                        try
                        {
                            cashierForm.deviceCMModel.ShiftNumber = Convert.ToInt32(KKMShiftNumber.Text);
                        }
                        catch { cashierForm.deviceCMModel.ShiftNumber = 1; }
                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.KKMExists != null)
                        {
                            lst.Add("KKMExists");
                            lst.Add(cashierForm.deviceCMModel.KKMExists);
                        }
                        if (cashierForm.deviceCMModel.KKMTypeId != null)
                        {
                            lst.Add("KKMTypeId");
                            lst.Add(cashierForm.deviceCMModel.KKMTypeId);
                        }
                        if (cashierForm.deviceCMModel.ChequeTimeOut != null)
                        {
                            lst.Add("ChequeTimeOut");
                            lst.Add(cashierForm.deviceCMModel.ChequeTimeOut);
                        }
                        if (cashierForm.deviceCMModel.ShiftAutoClose != null)
                        {
                            lst.Add("ShiftAutoClose");
                            lst.Add(cashierForm.deviceCMModel.ShiftAutoClose);
                        }
                        if (cashierForm.deviceCMModel.ShiftNewShiftTime != null)
                        {
                            lst.Add("ShiftNewShiftTime");
                            lst.Add(cashierForm.deviceCMModel.ShiftNewShiftTime);
                        }
                        if (cashierForm.deviceCMModel.CommandPassword != null)
                        {
                            lst.Add("CommandPassword");
                            lst.Add(cashierForm.deviceCMModel.CommandPassword);
                        }
                        if (cashierForm.deviceCMModel.RegistartionPass != null)
                        {
                            lst.Add("RegistartionPass");
                            lst.Add(cashierForm.deviceCMModel.RegistartionPass);
                        }
                        if (cashierForm.deviceCMModel.KKMComPortId != null)
                        {
                            lst.Add("KKMComPortId");
                            lst.Add(cashierForm.deviceCMModel.KKMComPortId);
                        }
                        if (cashierForm.deviceCMModel.ShiftNumber != null)
                        {
                            lst.Add("ShiftNumber");
                            lst.Add(cashierForm.deviceCMModel.ShiftNumber);
                        }
                        if (cashierForm.deviceCMModel.ChequeWork != null)
                        {
                            lst.Add("ChequeWork");
                            lst.Add(cashierForm.deviceCMModel.ChequeWork);
                        }

                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }
                    if (changed ||
                        ((KKMTypeId >= 0) &&
                        (cashierForm.deviceCMModel.KKMTypeId != null) &&
                        cashierForm.deviceCMModel.KKMTypeId != KKMTypeId))
                    {
                        if (cashierForm.KKM != null)
                        {
                            cashierForm.KKM.FR_Disconnect();
                            cashierForm.KKM = null;
                        }
                    }

                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    KKMGetSettings();

                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void KKMCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    KKMGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void KKMGetSettings()
        {
            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    int i;
                    byte[] encrypted;
                    lock (cashierForm.db_lock)
                    {
                        //            bool b;
                        // Чтение сохраненных параметров ККМ
                        // Наличие устройства
                        try
                        {
                            KKMExists.IsChecked = (bool)cashierForm.deviceCMModel.KKMExists;
                            exists = true;
                        }
                        catch
                        {
                            KKMExists.IsChecked = false;
                            exists = false;
                        }

                        KKMModelLine.IsEnabled = exists;
                        KKMPortLine.IsEnabled = exists;
                        KKMCommandPasswordLine.IsEnabled = exists;
                        KKMModePasswordLine.IsEnabled = exists;

                        if (exists)
                        {
                            if (cashierForm.KKM != null)
                            {
                                if (cashierForm.KKM.DeferedZReportEnabled)
                                {
                                    KKMShiftAutoSwithLine.Visibility = Visibility.Visible;
                                    KKMShiftAutoSwithTimeLine.Visibility = Visibility.Visible;
                                }
                                KKMChequeTimeLine.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                KKMShiftAutoSwithLine.Visibility = Visibility.Hidden;
                                KKMShiftAutoSwithTimeLine.Visibility = Visibility.Hidden;
                                KKMChequeTimeLine.Visibility = Visibility.Hidden;
                                //KKMChequeTime1Line.Visibility = Visibility.Hidden;
                            }

                            cashierForm.deviceCMModel.PrinterExists = false;
                            if ((bool)PrinterExists.IsChecked)
                                PrinterExists.IsChecked = false;
                            if (PrinterGroup.IsEnabled)
                                PrinterGroup.IsEnabled = false;
                        }
                        else
                        {
                            if (!PrinterGroup.IsEnabled)
                                PrinterGroup.IsEnabled = true;
                        }
                        cashierForm.ShiftWork = (cashierForm.deviceCMModel.PrinterExists == null) ? true : !(bool)cashierForm.deviceCMModel.PrinterExists;

                        // Текущая модель
                        KKMModel.Text = Properties.Resources.NotSelected0;
                        KKMModelSelectedIndex = -1;
                        try
                        {
                            for (i = 0; i < cashierForm.KKMExecutableDevices.Count; i++)
                                if (cashierForm.KKMExecutableDevices[i].Id == cashierForm.deviceCMModel.KKMTypeId)
                                {
                                    //if ((cashierForm.CashierType != 2) && (cashierForm.KKMExecutableDevices[i].Name == "IskraKKM")) {
                                    KKMModelSelectedIndex = i;
                                    KKMModel.Text = cashierForm.KKMExecutableDevices[i].Name;
                                    //}
                                    /*if ((cashierForm.CashierType == 2) && (cashierForm.KKMExecutableDevices[i].Name == "AtolKKM"))
                                    {
                                        KKMModelSelectedIndex = i;
                                        KKMModel.Text = cashierForm.KKMExecutableDevices[i].Name;
                                    }*/
                                }
                        }
                        catch
                        {
                            KKMModelSelectedIndex = -1;
                            KKMModel.Text = Properties.Resources.NotSelected0;
                        }
                        cashierForm.KKMModel = KKMModel.Text;

                        if (KKMModel != null)
                        {
                            if (KKMModel.Text == "AtolKKM")
                            {
                                KKMModePasswordLine.Visibility = Visibility.Visible;
                            }
                            else if (KKMModel.Text == "IskraKKM")
                            {
                                KKMModePasswordLine.Visibility = Visibility.Hidden;
                            }
                        }

                        try
                        {
                            KKMPortSelectedIndex = (int)cashierForm.deviceCMModel.KKMComPortId;
                        }
                        catch
                        {
                            KKMPortSelectedIndex = 1;
                        }
                        KKMPort.Text = "COM" + (KKMPortSelectedIndex).ToString();
                        cashierForm.KKMPort = KKMPort.Text;

                        try
                        {
                            ShiftAutoSwitch.IsChecked = (bool)cashierForm.deviceCMModel.ShiftAutoClose;
                        }
                        catch
                        {
                            ShiftAutoSwitch.IsChecked = true;
                        }
                        switch (cashierForm.deviceCMModel.ChequeWork)
                        {
                            case 0:
                                if (KKMChequeTimeLine.Visibility != Visibility.Hidden)
                                    KKMChequeTimeLine.Visibility = Visibility.Hidden;
                                break;
                            case 1:
                                KKMChequeTimeLabel.Text = Properties.Resources.KKMChequeTimeToBox;
                                if (KKMChequeTimeLine.Visibility != Visibility.Visible)
                                    KKMChequeTimeLine.Visibility = Visibility.Visible;
                                break;
                            case 2:
                                KKMChequeTimeLabel.Text = Properties.Resources.KKMChequeTimeDispense;
                                if (KKMChequeTimeLine.Visibility != Visibility.Visible)
                                    KKMChequeTimeLine.Visibility = Visibility.Visible;
                                break;
                        }
                        try
                        {
                            KKMChequeTime.Text = ((byte)cashierForm.deviceCMModel.ChequeTimeOut).ToString();
                        }
                        catch
                        {
                            KKMChequeTime.Text = "30";
                        }

                        /*if (exists)
                        {
                            if (cashierForm.KKM != null)
                                KKMChequeTimeLine.Visibility = Visibility.Visible;
                            else
                                KKMChequeTimeLine.Visibility = Visibility.Hidden;
                        }*/

                        try
                        {
                            KKMChequeTime.Text = ((byte)cashierForm.deviceCMModel.ChequeTimeOut).ToString();
                        }
                        catch
                        {
                            KKMChequeTime.Text = "30";
                        }

                        if (cashierForm.deviceCMModel.ChequeWork != null)
                        {
                            ChequeTypes.Text = cashierForm.ChequeTypes[(int)cashierForm.deviceCMModel.ChequeWork];
                            ChequeTypesSelectedIndex = (int)cashierForm.deviceCMModel.ChequeWork;
                        }
                        else
                        {
                            ChequeTypes.Text = cashierForm.ChequeTypes[0];
                            cashierForm.deviceCMModel.ChequeWork = 0;
                            ChequeTypesSelectedIndex = 0;
                        }

                        if ((bool)ShiftAutoSwitch.IsChecked && (cashierForm.deviceCMModel.ShiftNewShiftTime != null))
                        {
                            AutoShiftTimeHour.Text = ((DateTime)cashierForm.deviceCMModel.ShiftNewShiftTime).ToString("HH");
                            AutoShiftTimeMin.Text = ((DateTime)cashierForm.deviceCMModel.ShiftNewShiftTime).ToString("mm");
                        }

                        if (cashierForm.deviceCMModel != null)
                        {
                            if ((cashierForm.deviceCMModel.CommandPassword == null) || (cashierForm.deviceCMModel.CommandPassword == ""))
                            {
                                encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("AERF");
                                cashierForm.deviceCMModel.CommandPassword = cashierForm.cryp.ByteAToString(encrypted);
                            }

                            encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.CommandPassword);
                            KKMPassword.Password = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);
                            cashierForm.KKMPassword = KKMPassword.Password;

                            if ((cashierForm.deviceCMModel.RegistartionPass == null) || (cashierForm.deviceCMModel.RegistartionPass == ""))
                            {
                                encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("0000");
                                cashierForm.deviceCMModel.RegistartionPass = cashierForm.cryp.ByteAToString(encrypted);
                            }
                            encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.RegistartionPass);
                            KKMModePassword.Password = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);
                            cashierForm.KKMModePassword = KKMModePassword.Password;

                            /*if ((cashierForm.deviceCMModel.WithOutCleaningPass == null) || (cashierForm.deviceCMModel.WithOutCleaningPass == ""))
                            {
                                encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("0000");
                                cashierForm.deviceCMModel.WithOutCleaningPass = cashierForm.cryp.ByteAToString(encrypted);
                            }
                            encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.WithOutCleaningPass);
                            WithOutCleaningPass.Text = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);

                            if ((cashierForm.deviceCMModel.WithCleaningPass == null) || (cashierForm.deviceCMModel.WithCleaningPass == ""))
                            {
                                encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("0000");
                                cashierForm.deviceCMModel.WithCleaningPass = cashierForm.cryp.ByteAToString(encrypted);
                            }
                            encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.WithCleaningPass);
                            WithCleaningPass.Text = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);*/
                            if (cashierForm.deviceCMModel.ShiftNumber == null)
                                cashierForm.deviceCMModel.ShiftNumber = 1;
                            KKMShiftNumber.Text = cashierForm.deviceCMModel.ShiftNumber.ToString();
                        }
                    }
                    /*if (KKMModel.Text == "IskraKKM")
                    {
                        ShiftPanel.label48.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.LastZReport.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.label49.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.ZReportFrom.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.label50.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.ZReportTo.Visible = true;
                        cashierForm.TechForm.TechPanel.ShiftPanel.PrintZReport.Visible = true;
                        cashierForm.TechForm.TechPanel.SettingsPanel.KKMPanel.label3.Visible = false;
                        cashierForm.TechForm.TechPanel.SettingsPanel.KKMPanel.RegistrationPass.Visible = false;
                    }
                    else
                    {
                        cashierForm.TechForm.TechPanel.ShiftPanel.label48.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.LastZReport.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.label49.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.ZReportFrom.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.label50.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.ZReportTo.Visible = false;
                        cashierForm.TechForm.TechPanel.ShiftPanel.PrintZReport.Visible = false;
                    }*/

                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
                KKMSettingsSave.IsEnabled = false;
            }
        }
        private void KKMCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (PrinterExists != null)
                {
                    PrinterExists.IsChecked = !KKMExists.IsChecked;
                    PrinterExists.IsEnabled = !(bool)KKMExists.IsChecked;
                    PrinterModel.IsEnabled = !(bool)KKMExists.IsChecked;
                    PrinterPort.IsEnabled = !(bool)KKMExists.IsChecked;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CommandPassword_Enter(object sender, EventArgs e)
        {
            try
            {
                CalcHex AddingSummForm;
                AddingSummForm = new CalcHex();
                AddingSummForm.Len = 4;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (KKMPassword.Password != AddingSummForm.Sum)
                            KKMSettingsSave.IsEnabled = true;
                        KKMPassword.Password = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            KKMSettingsReturn.Focus();
        }
        private void RegistartionPass_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 4;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (KKMModePassword.Password != AddingSummForm.Sum)
                            KKMSettingsSave.IsEnabled = true;
                        KKMModePassword.Password = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            KKMSettingsReturn.Focus();
        }
        private void KKMModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (KKMModel != null)
                {
                    if (KKMModel.Text == "AtolKKM")
                    {
                        KKMModePasswordLine.Visibility = Visibility.Visible;
                    }
                    else if (KKMModel.Text == "IskraKKM")
                    {
                        KKMModePasswordLine.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void KKMCheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "KKMExists":
                        KKMModelLine.IsEnabled = (bool)cb.IsChecked;
                        KKMPortLine.IsEnabled = (bool)cb.IsChecked;
                        KKMCommandPasswordLine.IsEnabled = (bool)cb.IsChecked;
                        KKMModePasswordLine.IsEnabled = (bool)cb.IsChecked;
                        KKMShiftAutoSwithLine.IsEnabled = (bool)cb.IsChecked;
                        KKMShiftAutoSwithTimeLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            KKMSettingsSave.IsEnabled = true;
        }
        private void KKMModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.KKMModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.KKMExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (KKMModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.KKMExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (KKMModelSelectedIndex != p.SelectedIndex)
                            KKMSettingsSave.IsEnabled = true;
                        KKMModelSelectedIndex = p.SelectedIndex;
                        KKMModel.Text = cashierForm.KKMExecutableDevices[KKMModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            KKMSettingsReturn.Focus();
        }
        private void KKMPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.KKMPort;
                ((RadioButton)p.ItemsList.Items[KKMPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (KKMPortSelectedIndex != p.SelectedIndex + 1)
                            KKMSettingsSave.IsEnabled = true;

                        KKMPortSelectedIndex = p.SelectedIndex + 1;
                        KKMPort.Text = (string)((RadioButton)p.ItemsList.Items[KKMPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            KKMSettingsReturn.Focus();
        }
        private void KKMChequeTime_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        KKMChequeTime.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        KKMSettingsSave.IsEnabled = true;
                        //KKMChequeTime1.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        OtherSettingsSave.IsEnabled = true;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherDevicesSettingsReturn.Focus();
        }
        private void KKMShiftNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        KKMShiftNumber.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        KKMSettingsSave.IsEnabled = true;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherDevicesSettingsReturn.Focus();
        }
        private void ChequeWork_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.ChequeWork;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.ChequeTypes.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if (ChequeTypesSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.ChequeTypes[i];
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        ChequeTypesSelectedIndex = p.SelectedIndex;
                        if (cashierForm.deviceCMModel.ChequeWork != ChequeTypesSelectedIndex)
                        {
                            cashierForm.deviceCMModel.ChequeWork = (byte)ChequeTypesSelectedIndex;
                            switch (cashierForm.deviceCMModel.ChequeWork)
                            {
                                case 0:
                                    if (KKMChequeTimeLine.Visibility != Visibility.Hidden)
                                        KKMChequeTimeLine.Visibility = Visibility.Hidden;
                                    break;
                                case 1:
                                    KKMChequeTimeLabel.Text = Properties.Resources.KKMChequeTimeToBox;
                                    if (KKMChequeTimeLine.Visibility != Visibility.Visible)
                                        KKMChequeTimeLine.Visibility = Visibility.Visible;
                                    break;
                                case 2:
                                    KKMChequeTimeLabel.Text = Properties.Resources.KKMChequeTimeDispense;
                                    if (KKMChequeTimeLine.Visibility != Visibility.Visible)
                                        KKMChequeTimeLine.Visibility = Visibility.Visible;
                                    break;
                            }
                            KKMSettingsSave.IsEnabled = true;
                        }
                        ChequeTypes.Text = cashierForm.ChequeTypes[(byte)ChequeTypesSelectedIndex];
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            KKMSettingsReturn.Focus();

        }
        #endregion KKM

        #region OtherDevices
        protected void OtherDevicesButton_Click(object sender, EventArgs e)
        {
            try
            {
                OtherDevicesGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                OtherDevicesSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void OtherDevicesSaveButton_Click(object sender, EventArgs e)
        {
            bool changed1 = false, changed2 = false, changed3 = false, changed4 = false;
            if (cashierForm != null)
            {
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        // Сохранение других настроек
                        try
                        {
                            cashierForm.deviceCMModel.CardDispenserExists = CardDispenserExists.IsChecked;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CardDispenserExists = false;
                        }
                        // Текущая модель
                        if (CardDispenserModelSelectedIndex >= 0)
                        {
                            if (cashierForm.deviceCMModel.CardDispenserTypeId != cashierForm.CardDispenserExecutableDevices[CardDispenserModelSelectedIndex].Id)
                                changed1 = true;
                            cashierForm.deviceCMModel.CardDispenserTypeId = cashierForm.CardDispenserExecutableDevices[CardDispenserModelSelectedIndex].Id;
                        }
                        // CardDispenserPort
                        try
                        {
                            if (CardDispenserPortSelectedIndex > 0)
                            {
                                if (cashierForm.deviceCMModel.CardDispenserComPortId != CardDispenserPortSelectedIndex)
                                {
                                    changed1 = true;
                                    cashierForm.deviceCMModel.CardDispenserComPortId = CardDispenserPortSelectedIndex;
                                }
                            }
                            else
                            {
                                if (cashierForm.deviceCMModel.CardDispenserComPortId != 1)
                                {
                                    changed1 = true;
                                    cashierForm.deviceCMModel.CardDispenserComPortId = 1;
                                }
                            }
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CardDispenserComPortId = 1;
                        }

                        try
                        {
                            cashierForm.deviceCMModel.CardReaderExist = CardReaderExists.IsChecked;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CardReaderExist = false;
                        }
                        if (CardReaderModelSelectedIndex >= 0)
                        {
                            if (cashierForm.deviceCMModel.CardReaderTypeId != cashierForm.CardReaderExecutableDevices[CardReaderModelSelectedIndex].Id)
                                changed2 = true;
                            cashierForm.deviceCMModel.CardReaderTypeId = cashierForm.CardReaderExecutableDevices[CardReaderModelSelectedIndex].Id;
                        }
                        // CardReaderPort
                        try
                        {
                            if (CardReaderPortSelectedIndex > 0)
                            {
                                if (cashierForm.deviceCMModel.CardReaderComPortId != CardReaderPortSelectedIndex)
                                {
                                    changed2 = true;
                                    cashierForm.deviceCMModel.CardReaderComPortId = CardReaderPortSelectedIndex;
                                }
                            }
                            else
                            {
                                if (cashierForm.deviceCMModel.CardReaderComPortId != 1)
                                {
                                    changed2 = true;
                                    cashierForm.deviceCMModel.CardReaderComPortId = CardReaderPortSelectedIndex;
                                }
                            }
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CardReaderComPortId = 1;
                        }

                        try
                        {
                            cashierForm.deviceCMModel.SectorNumber = Convert.ToByte(CardReaderSector.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.SectorNumber = 1;
                        }
                        try
                        {
                            cashierForm.deviceCMModel.SlaveExist = SlaveExists.IsChecked;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.SlaveExist = false;
                        }

                        if (SlaveModelSelectedIndex >= 0)
                        {
                            if (cashierForm.deviceCMModel.SlaveTypeId != cashierForm.SlaveControllerExecutableDevices[SlaveModelSelectedIndex].Id)
                                changed3 = true;
                            cashierForm.deviceCMModel.SlaveTypeId = cashierForm.SlaveControllerExecutableDevices[SlaveModelSelectedIndex].Id;
                        }
                        try
                        {
                            if (SlavePortSelectedIndex > 0)
                            {
                                if (cashierForm.deviceCMModel.SlaveComPortId != SlavePortSelectedIndex)
                                {
                                    changed3 = true;
                                    cashierForm.deviceCMModel.SlaveComPortId = SlavePortSelectedIndex;
                                }
                            }
                            else
                            {
                                if (cashierForm.deviceCMModel.SlaveComPortId != 1)
                                {
                                    changed3 = true;
                                    cashierForm.deviceCMModel.SlaveComPortId = 1;
                                }
                            }
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.SlaveComPortId = 1;
                        }
                        // PrinterComPortId
                        try
                        {
                            cashierForm.deviceCMModel.PrinterExists = PrinterExists.IsChecked;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.PrinterExists = false;
                        }
                        if (PrinterModelSelectedIndex >= 0)
                        {
                            if (cashierForm.deviceCMModel.PrinterTypeId != cashierForm.PrinterExecutableDevices[PrinterModelSelectedIndex].Id)
                                changed4 = true;
                            cashierForm.deviceCMModel.PrinterTypeId = cashierForm.PrinterExecutableDevices[PrinterModelSelectedIndex].Id;
                        }
                        if (PrinterPortSelectedIndex > 0)
                        {
                            if (cashierForm.deviceCMModel.PrinterComPortId != PrinterPortSelectedIndex)
                            {
                                changed4 = true;
                                cashierForm.deviceCMModel.PrinterComPortId = PrinterPortSelectedIndex;
                            }
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.PrinterComPortId != 1)
                            {
                                changed4 = true;
                                cashierForm.deviceCMModel.PrinterComPortId = 1;
                            }
                        }

                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.CardDispenserExists != null)
                        {
                            lst.Add("CardDispenserExists");
                            lst.Add(cashierForm.deviceCMModel.CardDispenserExists);
                        }
                        if (cashierForm.deviceCMModel.CardDispenserTypeId != null)
                        {
                            lst.Add("CardDispenserTypeId");
                            lst.Add(cashierForm.deviceCMModel.CardDispenserTypeId);
                        }
                        if (cashierForm.deviceCMModel.CardDispenserComPortId != null)
                        {
                            lst.Add("CardDispenserComPortId");
                            lst.Add(cashierForm.deviceCMModel.CardDispenserComPortId);
                        }
                        if (cashierForm.deviceCMModel.CardReaderExist != null)
                        {
                            lst.Add("CardReaderExist");
                            lst.Add(cashierForm.deviceCMModel.CardReaderExist);
                        }
                        if (cashierForm.deviceCMModel.CardReaderTypeId != null)
                        {
                            lst.Add("CardReaderTypeId");
                            lst.Add(cashierForm.deviceCMModel.CardReaderTypeId);
                        }
                        if (cashierForm.deviceCMModel.CardReaderComPortId != null)
                        {
                            lst.Add("CardReaderComPortId");
                            lst.Add(cashierForm.deviceCMModel.CardReaderComPortId);
                        }
                        if (cashierForm.deviceCMModel.SectorNumber != null)
                        {
                            lst.Add("SectorNumber");
                            lst.Add(cashierForm.deviceCMModel.SectorNumber);
                        }
                        if (cashierForm.deviceCMModel.SlaveExist != null)
                        {
                            lst.Add("SlaveExist");
                            lst.Add(cashierForm.deviceCMModel.SlaveExist);
                        }
                        if (cashierForm.deviceCMModel.SlaveTypeId != null)
                        {
                            lst.Add("SlaveTypeId");
                            lst.Add(cashierForm.deviceCMModel.SlaveTypeId);
                        }
                        if (cashierForm.deviceCMModel.SlaveComPortId != null)
                        {
                            lst.Add("SlaveComPortId");
                            lst.Add(cashierForm.deviceCMModel.SlaveComPortId);
                        }
                        if (cashierForm.deviceCMModel.PrinterExists != null)
                        {
                            lst.Add("PrinterExists");
                            lst.Add(cashierForm.deviceCMModel.PrinterExists);
                        }
                        if (cashierForm.deviceCMModel.PrinterTypeId != null)
                        {
                            lst.Add("PrinterTypeId");
                            lst.Add(cashierForm.deviceCMModel.PrinterTypeId);
                        }
                        if (cashierForm.deviceCMModel.PrinterComPortId != null)
                        {
                            lst.Add("PrinterComPortId");
                            lst.Add(cashierForm.deviceCMModel.PrinterComPortId);
                        }

                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }

                    if (changed1 && cashierForm.CardDispenser != null)
                    {
                        cashierForm.CardDispenser.Close();
                        cashierForm.CardDispenser = null;
                    }
                    if (changed2 && cashierForm.CardReader != null)
                    {
                        cashierForm.CardReader.ClosePool();
                        cashierForm.CardReader.Close();
                        cashierForm.CardReader = null;
                    }
                    if (changed3 && cashierForm.SlaveController != null)
                    {
                        cashierForm.SlaveController.Close();
                        cashierForm.SlaveController = null;
                    }
                    if (changed4 && cashierForm.Printer != null)
                    {
                        cashierForm.Printer.Close();
                        cashierForm.Printer = null;
                    }

                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    OtherDevicesGetSettings();

                    cashierForm.ShiftWork = (cashierForm.deviceCMModel.PrinterExists == null) ? true : !(bool)cashierForm.deviceCMModel.PrinterExists;

                    SettingsReturnButton();
                    if (!(bool)CardDispenserExists.IsChecked)
                        cashierForm.CardTimeOutToBasket = 30;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void OtherDevicesCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    OtherDevicesGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void OtherDevicesGetSettings()
        {
            int i;
            if (cashierForm != null)
            {
                try
                {
                    bool exists;
                    lock (cashierForm.db_lock)
                    {
                        // Чтение других настроек
                        try
                        {
                            exists = (bool)cashierForm.deviceCMModel.CardDispenserExists;
                            CardDispenserExists.IsChecked = exists;
                        }
                        catch
                        {
                            exists = false;
                            CardDispenserExists.IsChecked = false;
                        }

                        // Текущая модель Диспенсера карт
                        try
                        {
                            for (i = 0; i < cashierForm.CardDispenserExecutableDevices.Count; i++)
                                if (cashierForm.CardDispenserExecutableDevices[i].Id == cashierForm.deviceCMModel.CardDispenserTypeId)
                                {
                                    CardDispenserModelSelectedIndex = i;
                                    CardDispenserModel.Text = cashierForm.CardDispenserExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            CardDispenserModelSelectedIndex = 0;
                        }
                        cashierForm.CardDispenserModel = CardDispenserModel.Text;

                        CardDispenserModelLine.IsEnabled = exists;
                        CardDispenserPortLine.IsEnabled = exists;

                        try
                        {
                            CardDispenserPortSelectedIndex = (int)cashierForm.deviceCMModel.CardDispenserComPortId;
                        }
                        catch
                        {
                            CardDispenserPortSelectedIndex = 1;
                        }
                        CardDispenserPort.Text = "COM" + (CardDispenserPortSelectedIndex).ToString();
                        cashierForm.CardDispenserPort = CardDispenserPort.Text;

                        try
                        {
                            exists = (bool)cashierForm.deviceCMModel.CardReaderExist;
                            CardReaderExists.IsChecked = exists;
                        }
                        catch
                        {
                            exists = false;
                            CardReaderExists.IsChecked = false;
                        }
                        CardReaderModelLine.IsEnabled = exists;
                        // Текущая модель Ридера карт
                        try
                        {
                            for (i = 0; i < cashierForm.CardReaderExecutableDevices.Count; i++)
                                if (cashierForm.CardReaderExecutableDevices[i].Id == cashierForm.deviceCMModel.CardReaderTypeId)
                                {
                                    CardReaderModelSelectedIndex = i;
                                    CardReaderModel.Text = cashierForm.CardReaderExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            CardReaderModelSelectedIndex = 0;
                        }
                        cashierForm.CardReaderModel = CardReaderModel.Text;
                        if (cashierForm.CardReaderModel == "HID OmniKey")
                            CardReaderPortLine.Visibility = Visibility.Hidden;
                        else
                        {
                            CardReaderPortLine.Visibility = Visibility.Visible;
                            CardReaderPortLine.IsEnabled = exists;
                        }

                        try
                        {
                            CardReaderPortSelectedIndex = (int)cashierForm.deviceCMModel.CardReaderComPortId;
                        }
                        catch
                        {
                            CardReaderPortSelectedIndex = 1;
                        }
                        CardReaderPort.Text = "COM" + (CardReaderPortSelectedIndex).ToString();
                        cashierForm.CardReaderPort = CardReaderPort.Text;
                        try
                        {
                            CardReaderSector.Text = ((byte)cashierForm.deviceCMModel.SectorNumber).ToString();
                            cashierForm.SectorNumber = Convert.ToByte(CardReaderSector.Text);

                        }
                        catch
                        {
                            CardReaderSector.Text = "1";
                            cashierForm.SectorNumber = 1;
                        }
                        if (cashierForm.CardReader != null)
                            cashierForm.CardReader.SectorNumber = cashierForm.SectorNumber;
                        try
                        {
                            exists = (bool)cashierForm.deviceCMModel.PrinterExists;
                            PrinterExists.IsChecked = exists;
                        }
                        catch
                        {
                            exists = false;
                            PrinterExists.IsChecked = false;
                        }
                        PrinterModelLine.IsEnabled = exists;
                        PrinterPortLine.IsEnabled = exists;
                        // Текущая модель Принтера
                        try
                        {
                            for (i = 0; i < cashierForm.PrinterExecutableDevices.Count; i++)
                                if (cashierForm.PrinterExecutableDevices[i].Id == cashierForm.deviceCMModel.PrinterTypeId)
                                {
                                    PrinterModelSelectedIndex = i;
                                    PrinterModel.Text = cashierForm.PrinterExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            PrinterModelSelectedIndex = 0;
                        }
                        cashierForm.PrinterModel = PrinterModel.Text;
                        try
                        {
                            PrinterPortSelectedIndex = (int)cashierForm.deviceCMModel.PrinterComPortId;
                        }
                        catch
                        {
                            PrinterPortSelectedIndex = 1;
                        }
                        PrinterPort.Text = "COM" + (PrinterPortSelectedIndex).ToString();
                        cashierForm.PrinterPort = PrinterPort.Text;
                        try
                        {
                            exists = (bool)cashierForm.deviceCMModel.SlaveExist;
                            SlaveExists.IsChecked = exists;
                        }
                        catch
                        {
                            exists = true;
                            SlaveExists.IsChecked = true;
                        }
                        SlaveControllerModelLine.IsEnabled = exists;
                        SlaveControllerPortLine.IsEnabled = exists;
                        // Текущая модель Слейв
                        try
                        {
                            for (i = 0; i < cashierForm.SlaveControllerExecutableDevices.Count; i++)
                                if (cashierForm.SlaveControllerExecutableDevices[i].Id == cashierForm.deviceCMModel.SlaveTypeId)
                                {
                                    SlaveModelSelectedIndex = i;
                                    SlaveModel.Text = cashierForm.SlaveControllerExecutableDevices[i].Name;
                                    break;
                                }
                        }
                        catch
                        {
                            SlaveModelSelectedIndex = 0;
                        }
                        cashierForm.SlaveControllerModel = SlaveModel.Text;
                        try
                        {
                            SlavePortSelectedIndex = (int)cashierForm.deviceCMModel.SlaveComPortId;
                        }
                        catch
                        {
                            SlavePortSelectedIndex = 1;
                        }
                        SlavePort.Text = "COM" + (SlavePortSelectedIndex).ToString();
                        cashierForm.SlaveControllerPort = SlavePort.Text;
                        if (cashierForm.SlaveController != null)
                            cashierForm.SlaveController.ComPort = SlavePort.Text;
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
            OtherDevicesSettingsSave.IsEnabled = false;
        }
        private void OtherDevicesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                switch (cb.Name)
                {
                    case "PrinterExists":
                        PrinterModelLine.IsEnabled = (bool)cb.IsChecked;
                        PrinterPortLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                    case "CardDispenserExists":
                        CardDispenserModelLine.IsEnabled = (bool)cb.IsChecked;
                        CardDispenserPortLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                    case "CardReaderExists":
                        CardReaderModelLine.IsEnabled = (bool)cb.IsChecked;
                        CardReaderPortLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                    case "SlaveExists":
                        SlaveControllerModelLine.IsEnabled = (bool)cb.IsChecked;
                        SlaveControllerPortLine.IsEnabled = (bool)cb.IsChecked;
                        break;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherDevicesSettingsSave.IsEnabled = true;
        }

        #region CardDispenser
        protected void CardDispenserSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CardDispenserSettingsButton.IsChecked = true;
                    CardReaderSettingsButton.IsChecked = false;
                    SlaveSettingsButton.IsChecked = false;
                    PrinterSettingsButton.IsChecked = false;
                    CardDispenserGroup.Visibility = Visibility.Visible;
                    CardReaderGroup.Visibility = Visibility.Hidden;
                    SlaveGroup.Visibility = Visibility.Hidden;
                    PrinterGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CardDispenserExists_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (CardDispenserExistsLine != null)
                {
                    CardDispenserModelLine.IsEnabled = (bool)CardDispenserExists.IsChecked;
                    CardDispenserPortLine.IsEnabled = (bool)CardDispenserExists.IsChecked;

                    PenalCardTP.IsEnabled = (bool)CardDispenserExists.IsChecked;
                    PenalCardTS.IsEnabled = (bool)CardDispenserExists.IsChecked;
                    CardTimeOutLine.IsEnabled = (bool)CardDispenserExists.IsChecked;
                    PenalCardTimeOut.IsEnabled = (bool)CardDispenserExists.IsChecked;
                    if (!(bool)CardDispenserExists.IsChecked)
                        if (cashierForm.CardDispenser != null)
                            cashierForm.CardDispenser.Close();
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardDispenserСomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.CardDispenser != null)
                    cashierForm.CardDispenser.Close();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardDispenserPort_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.CardDispenser != null)
                    cashierForm.CardDispenser.Close();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardDispenserModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CardDispenserModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CardDispenserExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CardDispenserModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.CardDispenserExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CardDispenserModelSelectedIndex != p.SelectedIndex)
                            OtherDevicesSettingsSave.IsEnabled = true;
                        CardDispenserModelSelectedIndex = p.SelectedIndex;
                        CardDispenserModel.Text = cashierForm.CardDispenserExecutableDevices[CardDispenserModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        private void CardDispenserPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CardDispenserPort;
                ((RadioButton)p.ItemsList.Items[CardDispenserPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CardDispenserPortSelectedIndex != p.SelectedIndex + 1)
                            OtherDevicesSettingsSave.IsEnabled = true;

                        CardDispenserPortSelectedIndex = p.SelectedIndex + 1;
                        CardDispenserPort.Text = (string)((RadioButton)p.ItemsList.Items[CardDispenserPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        #endregion CardDispenser

        #region CardReader
        protected void CardReaderSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CardDispenserSettingsButton.IsChecked = false;
                    CardReaderSettingsButton.IsChecked = true;
                    SlaveSettingsButton.IsChecked = false;
                    PrinterSettingsButton.IsChecked = false;
                    CardDispenserGroup.Visibility = Visibility.Hidden;
                    CardReaderGroup.Visibility = Visibility.Visible;
                    SlaveGroup.Visibility = Visibility.Hidden;
                    PrinterGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CardReaderExists_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (CardReaderExistsLine != null)
                {
                    CardReaderModelLine.IsEnabled = (bool)CardReaderExists.IsChecked;
                    CardReaderPortLine.IsEnabled = (bool)CardReaderExists.IsChecked;
                    if (cashierForm.CardReader != null)
                    {
                        cashierForm.CardReader.ClosePool();
                        cashierForm.CardReader.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardReaderСomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.CardReader != null)
                {
                    cashierForm.CardReader.ClosePool();
                    cashierForm.CardReader.Close();
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardReaderPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.CardReader != null)
                {
                    cashierForm.CardReader.ClosePool();
                    cashierForm.CardReader.Close();
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CardReaderModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CardReaderModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CardReaderExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (CardReaderModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.CardReaderExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CardReaderModelSelectedIndex != p.SelectedIndex)
                            OtherDevicesSettingsSave.IsEnabled = true;
                        CardReaderModelSelectedIndex = p.SelectedIndex;
                        CardReaderModel.Text = cashierForm.CardReaderExecutableDevices[CardReaderModelSelectedIndex].Name;
                        if (CardReaderModel.Text != "HID OmniKey")
                            CardReaderPortLine.IsEnabled = true;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        private void CardReaderPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.CardReaderPort;
                ((RadioButton)p.ItemsList.Items[CardReaderPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (CardReaderPortSelectedIndex != p.SelectedIndex + 1)
                            OtherDevicesSettingsSave.IsEnabled = true;

                        CardReaderPortSelectedIndex = p.SelectedIndex + 1;
                        CardReaderPort.Text = (string)((RadioButton)p.ItemsList.Items[CardReaderPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        private void CardReaderSector_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 1;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        CardReaderSector.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        OtherDevicesSettingsSave.IsEnabled = true;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherDevicesSettingsReturn.Focus();
        }
        #endregion CardReader

        #region Slave
        protected void SlaveSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CardDispenserSettingsButton.IsChecked = false;
                    CardReaderSettingsButton.IsChecked = false;
                    SlaveSettingsButton.IsChecked = true;
                    PrinterSettingsButton.IsChecked = false;
                    CardDispenserGroup.Visibility = Visibility.Hidden;
                    CardReaderGroup.Visibility = Visibility.Hidden;
                    SlaveGroup.Visibility = Visibility.Visible;
                    PrinterGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void SlaveExists_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.SlaveController != null)
                    cashierForm.SlaveController.CloseThread();
                if (SlaveControllerExistsLine != null)
                {
                    SlaveControllerModelLine.IsEnabled = (bool)SlaveExists.IsChecked;
                    SlaveControllerPortLine.IsEnabled = (bool)SlaveExists.IsChecked;
                    /*SlaveControllerPortLabel.Enabled = SlaveExists.Checked;
                    SlavePort.Enabled = SlaveExists.Checked;*/
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        /*private void SlavePort_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.SlaveController != null)
                    cashierForm.SlaveController.CloseThread();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }*/
        private void SlaveModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.SlaveModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.SlaveControllerExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (SlaveModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.SlaveControllerExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (SlaveModelSelectedIndex != p.SelectedIndex)
                            OtherDevicesSettingsSave.IsEnabled = true;
                        SlaveModelSelectedIndex = p.SelectedIndex;
                        SlaveModel.Text = cashierForm.SlaveControllerExecutableDevices[SlaveModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        private void SlavePort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.SlavePort;
                ((RadioButton)p.ItemsList.Items[SlavePortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (SlavePortSelectedIndex != p.SelectedIndex + 1)
                        {
                            OtherDevicesSettingsSave.IsEnabled = true;

                            SlavePortSelectedIndex = p.SelectedIndex + 1;
                            SlavePort.Text = (string)((RadioButton)p.ItemsList.Items[SlavePortSelectedIndex - 1]).Content;
                            if (cashierForm.SlaveController != null)
                                cashierForm.SlaveController.CloseThread();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        #endregion Slave

        #region Printer
        protected void PrinterSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    CardDispenserSettingsButton.IsChecked = false;
                    CardReaderSettingsButton.IsChecked = false;
                    SlaveSettingsButton.IsChecked = false;
                    PrinterSettingsButton.IsChecked = true;
                    CardDispenserGroup.Visibility = Visibility.Hidden;
                    CardReaderGroup.Visibility = Visibility.Hidden;
                    SlaveGroup.Visibility = Visibility.Hidden;
                    PrinterGroup.Visibility = Visibility.Visible;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void PrinterExists_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (PrinterExistsLine != null)
                {
                    PrinterModelLine.IsEnabled = (bool)PrinterExists.IsChecked;
                    PrinterPortLine.IsEnabled = (bool)PrinterExists.IsChecked;
                }
                if (cashierForm.Printer != null)
                    cashierForm.Printer.Close();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void PrinterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.Printer != null)
                    cashierForm.Printer.Close();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void PrinterPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.Printer != null)
                    cashierForm.Printer.Close();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void PrinterModel_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.PrinterModel;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.PrinterExecutableDevices.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if (PrinterModelSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.PrinterExecutableDevices[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (PrinterModelSelectedIndex != p.SelectedIndex)
                            OtherDevicesSettingsSave.IsEnabled = true;
                        PrinterModelSelectedIndex = p.SelectedIndex;
                        PrinterModel.Text = cashierForm.PrinterExecutableDevices[PrinterModelSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        private void PrinterPort_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpComPort p = new PopUpComPort();

                p.PopUpComPortsHeader.Text = Properties.Resources.PrinterPort;
                ((RadioButton)p.ItemsList.Items[PrinterPortSelectedIndex - 1]).IsChecked = true;
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (PrinterPortSelectedIndex != p.SelectedIndex + 1)
                            OtherDevicesSettingsSave.IsEnabled = true;

                        PrinterPortSelectedIndex = p.SelectedIndex + 1;
                        PrinterPort.Text = (string)((RadioButton)p.ItemsList.Items[PrinterPortSelectedIndex - 1]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherDevicesSettingsReturn.Focus();
        }
        #endregion Printer
        #endregion OtherDevices

        #region OtherSettings
        protected void OtherSettingsButton_Click(object sender, EventArgs e)
        {
            try
            {
                OtherSettingsGetSettings();
                SettingsPanel.Visibility = Visibility.Visible;
                OtherSettings.Visibility = Visibility.Visible;
                SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void OtherSettingsSaveButton_Click(object sender, EventArgs e)
        {
            bool changed = false;
            if (cashierForm != null)
            {
                try
                {
                    bool ChangeDefaultLaguage = false;
                    lock (cashierForm.db_lock)
                    {
                        // Сохранение других настроек
                        // ID Кассы
                        // Encrypt the string to an array of bytes.
                        byte[] encrypted = cashierForm.cryp.EncryptStringToBytes_Aes(CashierKey.Password);

                        cashierForm.deviceCMModel.CardKey = cashierForm.cryp.ByteAToString(encrypted);
                        // Номер кассы
                        try
                        {
                            cashierForm.deviceCMModel.CashierNumber = Convert.ToInt32(CashierNumber.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.CashierNumber = 0;
                        }
                        // Уровень логирования
                        try
                        {
                            cashierForm.deviceCMModel.LogLevel = Convert.ToByte(LogLevel.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.LogLevel = 2;
                        }
                        // НДС
                        try
                        {
                            cashierForm.deviceCMModel.VAT = Convert.ToInt16(VAT.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.VAT = 0;
                        }
                        // Максимальная сумма на сдачу
                        try
                        {
                            cashierForm.deviceCMModel.MaximalChange = Convert.ToInt32(MaxSummDelivery.Text);
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.MaximalChange = 0;
                        }
                        cashierForm.deviceCMModel.CancelRefund = CancelRefund.IsChecked;
                        cashierForm.deviceCMModel.SyncNominals = SyncNominals.IsChecked;
                        cashierForm.deviceCMModel.SingleClient0 = OneTimeClient0.IsChecked;

                        try
                        {
                            if (PenalCardTP.IsEnabled)
                            {
                                if (PenalCardTPSelectedIndex >= 0)
                                {
                                    cashierForm.deviceCMModel.PenaltyCardTPId = (int)cashierForm.tariffs[PenalCardTPSelectedIndex].IdFC;
                                }
                                if (PenalCardTSSelectedIndex >= 0)
                                {
                                    cashierForm.deviceCMModel.PenaltyCardTSId = (int)cashierForm.TariffSchedule[PenalCardTSSelectedIndex].IdFC;
                                }
                            }
                            else
                            {
                                cashierForm.PenalCardTP = 0;
                                cashierForm.PenalCardTS = 0;
                            }
                        }
                        catch
                        {
                            //cashierForm.PenalCardNominal = 0;
                            cashierForm.deviceCMModel.PenaltyCardTPId = 0;
                            cashierForm.deviceCMModel.PenaltyCardTSId = 0;
                        }
                        if (ZoneComboBoxSelectedIndex >= 0)
                        {
                            cashierForm.deviceCMModel.ZoneId = cashierForm.zones[ZoneComboBoxSelectedIndex].Id;
                            CurrentZone.Text = cashierForm.zones[ZoneComboBoxSelectedIndex].Name;
                        }
                        if (GroupComboBoxSelectedIndex >= 0)
                        {
                            cashierForm.deviceCMModel.GroupId = cashierForm.groups[GroupComboBoxSelectedIndex].Id;
                            CurrentGroup.Text = cashierForm.groups[GroupComboBoxSelectedIndex].Name;
                        }

                        // Время ожидания штрафной карты
                        try
                        {
                            cashierForm.PenalCardTimeOut = Convert.ToInt16(PenalCardTimeOut.Text);
                            cashierForm.deviceCMModel.PenalCardTimeOut = cashierForm.PenalCardTimeOut;
                        }
                        catch
                        {
                            cashierForm.PenalCardTimeOut = 60;
                            cashierForm.deviceCMModel.PenalCardTimeOut = 60;
                        }
                        // Время ожидания парковочной карты
                        try
                        {
                            cashierForm.CardTimeOutToBasket = Convert.ToInt16(CardTimeOut.Text);
                            cashierForm.deviceCMModel.CardTimeOutToBasket = cashierForm.CardTimeOutToBasket;
                        }
                        catch
                        {
                            cashierForm.CardTimeOutToBasket = 60;
                            cashierForm.deviceCMModel.CardTimeOutToBasket = 60;
                        }

                        int dlc = 0;
                        foreach (DevicesLanguagesT d in cashierForm.DevicesLanguages)
                            if (d.Checked)
                                dlc++;
                        if (dlc > 6)
                        {
                            var result = MessageBox.Show(Properties.Resources.CheckAvailableLanguages, Properties.Resources.TooManyAvailableLanguages,
                                         MessageBoxButton.OK,
                                         MessageBoxImage.Error);
                            return;
                        }

                        foreach (DevicesLanguagesT d in cashierForm.DevicesLanguages)
                        {
                            lock (cashierForm.db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.UpdateCommand("DevicesLanguages", new object[] { "Checked", d.Checked }, "Id=@Key", new object[] { "@Key", d.Id }, cashierForm.db);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.UpdateCommand("DevicesLanguages", new object[] { "Checked", d.Checked }, "Id=@Key", new object[] { "@Key", d.Id }, cashierForm.db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }

                        // ServerExchageProtocolId
                        if (ServerExchangeProtocolSelectedIndex >= 0)
                            try
                            {
                                cashierForm.deviceCMModel.ServerExchangeProtocolId = ServerExchangeProtocolSelectedIndex + 1;
                            }
                            catch
                            {
                                cashierForm.deviceCMModel.ServerExchangeProtocolId = null;
                            }

                        // ServerUrl
                        try
                        {
                            cashierForm.deviceCMModel.ServerURL = ServerUrl.Text;
                        }
                        catch
                        {
                            cashierForm.deviceCMModel.ServerURL = "";
                        }

                        // Язык по умолчанию
                        if (DefaultLanguageSelectedIndex >= 0)
                        {
                            if (cashierForm.deviceCMModel.DefaultLanguage != cashierForm.Languages[DefaultLanguageSelectedIndex][0])
                                ChangeDefaultLaguage = true;
                            cashierForm.deviceCMModel.DefaultLanguage = cashierForm.Languages[DefaultLanguageSelectedIndex][0];
                        }
                        else
                        {
                            if (cashierForm.deviceCMModel.DefaultLanguage != "EN")
                                ChangeDefaultLaguage = true;
                            cashierForm.deviceCMModel.DefaultLanguage = "EN";
                        }

                        // Таймаут восстановления языка
                        try
                        {
                            cashierForm.LanguageTimeOut = Convert.ToInt16(LanguageTimeOut.Text);
                            cashierForm.deviceCMModel.LanguageTimeOut = cashierForm.LanguageTimeOut;
                        }
                        catch
                        {
                            cashierForm.LanguageTimeOut = 60;
                            cashierForm.deviceCMModel.LanguageTimeOut = 60;
                        }

                        if (cashierForm.deviceCMModel.CashierType != CashierTypeSelectedIndex)
                            changed = true;
                        cashierForm.deviceCMModel.CashierType = (byte)CashierTypeSelectedIndex;

                        List<object> lst = new List<object>();
                        if (cashierForm.deviceCMModel.CardKey != null)
                        {
                            lst.Add("CardKey");
                            lst.Add(cashierForm.deviceCMModel.CardKey);
                        }
                        if (cashierForm.deviceCMModel.CashierNumber != null)
                        {
                            lst.Add("CashierNumber");
                            lst.Add(cashierForm.deviceCMModel.CashierNumber);
                        }
                        lst.Add("LogLevel");
                        lst.Add(cashierForm.deviceCMModel.LogLevel);
                        if (cashierForm.deviceCMModel.VAT != null)
                        {
                            lst.Add("VAT");
                            lst.Add(cashierForm.deviceCMModel.VAT);
                        }
                        if (cashierForm.deviceCMModel.MaximalChange != null)
                        {
                            lst.Add("MaximalChange");
                            lst.Add(cashierForm.deviceCMModel.MaximalChange);
                        }
                        if (cashierForm.deviceCMModel.CancelRefund != null)
                        {
                            lst.Add("CancelRefund");
                            lst.Add(cashierForm.deviceCMModel.CancelRefund);
                        }
                        if (cashierForm.deviceCMModel.SyncNominals != null)
                        {
                            lst.Add("SyncNominals");
                            lst.Add(cashierForm.deviceCMModel.SyncNominals);
                        }
                        if (cashierForm.deviceCMModel.SingleClient0 != null)
                        {
                            lst.Add("SingleClient0");
                            lst.Add(cashierForm.deviceCMModel.SingleClient0);
                        }
                        if (cashierForm.deviceCMModel.PenaltyCardTPId != null)
                        {
                            lst.Add("PenaltyCardTPId");
                            lst.Add(cashierForm.deviceCMModel.PenaltyCardTPId);
                        }
                        if (cashierForm.deviceCMModel.PenaltyCardTSId != null)
                        {
                            lst.Add("PenaltyCardTSId");
                            lst.Add(cashierForm.deviceCMModel.PenaltyCardTSId);
                        }
                        if (cashierForm.deviceCMModel.ZoneId != null)
                        {
                            lst.Add("ZoneId");
                            lst.Add(cashierForm.deviceCMModel.ZoneId);
                        }
                        if (cashierForm.deviceCMModel.GroupId != null)
                        {
                            lst.Add("GroupId");
                            lst.Add(cashierForm.deviceCMModel.GroupId);
                        }
                        if (cashierForm.deviceCMModel.PenalCardTimeOut != null)
                        {
                            lst.Add("PenalCardTimeOut");
                            lst.Add(cashierForm.deviceCMModel.PenalCardTimeOut);
                        }
                        if (cashierForm.deviceCMModel.CardTimeOutToBasket != null)
                        {
                            lst.Add("CardTimeOutToBasket");
                            lst.Add(cashierForm.deviceCMModel.CardTimeOutToBasket);
                        }
                        if (cashierForm.deviceCMModel.ServerExchangeProtocolId != null)
                        {
                            lst.Add("ServerExchangeProtocolId");
                            lst.Add(cashierForm.deviceCMModel.ServerExchangeProtocolId);
                        }
                        if (cashierForm.deviceCMModel.ServerURL != null)
                        {
                            lst.Add("ServerURL");
                            lst.Add(cashierForm.deviceCMModel.ServerURL);
                        }
                        if (cashierForm.deviceCMModel.DefaultLanguage != null)
                        {
                            lst.Add("DefaultLanguage");
                            lst.Add(cashierForm.deviceCMModel.DefaultLanguage);
                        }
                        if (cashierForm.deviceCMModel.LanguageTimeOut != null)
                        {
                            lst.Add("LanguageTimeOut");
                            lst.Add(cashierForm.deviceCMModel.LanguageTimeOut);
                        }
                        if (cashierForm.deviceCMModel.ChequeTimeOut != null)
                        {
                            lst.Add("ChequeTimeOut");
                            lst.Add(cashierForm.deviceCMModel.ChequeTimeOut);
                        }
                        if (cashierForm.deviceCMModel.CashierType != null)
                        {
                            lst.Add("CashierType");
                            lst.Add(cashierForm.deviceCMModel.CashierType);
                        }

                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        cashierForm.SettingsLastUpdate = DateTime.Now;
                    }
                    if (changed && cashierForm.CardReader != null)
                    {
                        cashierForm.CardReader.ClosePool();
                        cashierForm.CardReader.Close();
                        cashierForm.CardReader = null;
                    }


                    OtherSettingsGetSettings();
                    SettingsReturnButton();
                    if (ChangeDefaultLaguage)
                    {
                        CultureInfo lang;
                        if (DefaultLanguage.Text != "CN")
                            lang = new CultureInfo(DefaultLanguage.Text);
                        else
                            lang = new CultureInfo("zh-Hans");
                        if (lang != null)
                        {
                            CultureManager.UICulture = lang;
                        }
                        cashierForm.GetSettings(false);
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        protected void OtherSettingsCancelButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    OtherSettingsGetSettings();
                    SettingsReturnButton();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        public void OtherSettingsGetSettings()
        {
            string req = "";
            try
            {
                if (cashierForm != null)
                {
                    lock (cashierForm.db_lock)
                    {
                        byte[] encrypted;

                        try
                        {
                            cashierForm.DefaultLanguage = cashierForm.deviceCMModel.DefaultLanguage;
                        }
                        catch
                        {
                            cashierForm.DefaultLanguage = "EN";
                        }
                        if (cashierForm.DefaultLanguage == null)
                            cashierForm.DefaultLanguage = "EN";
                        if (cashierForm.deviceCMModel.DefaultLanguage == null)
                        {
                            cashierForm.DefaultCulture = Properties.Settings.Default.DefaultLanguage;
                        }
                        else
                        if (cashierForm.deviceCMModel.DefaultLanguage != "CN")
                        {
                            cashierForm.DefaultCulture = new CultureInfo(cashierForm.DefaultLanguage);
                        }
                        else
                        {
                            cashierForm.DefaultCulture = new CultureInfo("zh-Hans");
                        }
                        CultureManager.UICulture = cashierForm.DefaultCulture;
                        DefaultLanguage.Text = cashierForm.DefaultLanguage;

                        cashierForm.ToLogCardReaded = cashierForm.ResourceManager.GetString("ToLogCardReaded", cashierForm.DefaultCulture);
                        cashierForm.ToLogWriteCard = cashierForm.ResourceManager.GetString("ToLogCardWrited", cashierForm.DefaultCulture);
                        cashierForm.ToLogCardsOnline = cashierForm.ResourceManager.GetString("ToLogCardsOnline", cashierForm.DefaultCulture);
                        cashierForm.ToLogToCardsOnline = cashierForm.ResourceManager.GetString("ToLogToCardsOnline", cashierForm.DefaultCulture);
                        cashierForm.ToLogCardCalculated = cashierForm.ResourceManager.GetString("ToLogCardCalculated", cashierForm.DefaultCulture);

                        cashierForm.ToLogEntranceTime = cashierForm.ResourceManager.GetString("ToLogEntranceTime", cashierForm.DefaultCulture);
                        cashierForm.ToLogLastPaymentTime = cashierForm.ResourceManager.GetString("ToLogLastPaymentTime", cashierForm.DefaultCulture);
                        cashierForm.ToLogRecountTime = cashierForm.ResourceManager.GetString("ToLogRecountTime", cashierForm.DefaultCulture);
                        cashierForm.ToLogTariffID = cashierForm.ResourceManager.GetString("ToLogTariffID", cashierForm.DefaultCulture);
                        cashierForm.ToLogRegular = cashierForm.ResourceManager.GetString("ToLogRegular", cashierForm.DefaultCulture);
                        cashierForm.ToLogTariffSheduleID = cashierForm.ResourceManager.GetString("ToLogTariffSheduleID", cashierForm.DefaultCulture);
                        cashierForm.ToLogZoneid = cashierForm.ResourceManager.GetString("ToLogZoneid", cashierForm.DefaultCulture);
                        cashierForm.ToLogClientGroupid = cashierForm.ResourceManager.GetString("ToLogClientGroupid", cashierForm.DefaultCulture);
                        cashierForm.ToLogSumCard = cashierForm.ResourceManager.GetString("ToLogSumCard", cashierForm.DefaultCulture);
                        cashierForm.ToLogNulltime1Time = cashierForm.ResourceManager.GetString("ToLogNulltime1Time", cashierForm.DefaultCulture);
                        cashierForm.ToLogNulltime2Time = cashierForm.ResourceManager.GetString("ToLogNulltime2Time", cashierForm.DefaultCulture);
                        cashierForm.ToLogNulltime3Time = cashierForm.ResourceManager.GetString("ToLogNulltime3Time", cashierForm.DefaultCulture);
                        cashierForm.ToLogTVP = cashierForm.ResourceManager.GetString("ToLogTVP", cashierForm.DefaultCulture);
                        cashierForm.ToLogDateSaveCard = cashierForm.ResourceManager.GetString("ToLogDateSaveCard", cashierForm.DefaultCulture);
                        cashierForm.ToLogTKVP = cashierForm.ResourceManager.GetString("ToLogTKVP", cashierForm.DefaultCulture);
                        cashierForm.ToLogAmountSumCard = cashierForm.ResourceManager.GetString("ToLogAmountSumCard", cashierForm.DefaultCulture);
                        cashierForm.ToLogCardDebt = cashierForm.ResourceManager.GetString("ToLogCardDebt", cashierForm.DefaultCulture);
                        cashierForm.ToLogCardBalance = cashierForm.ResourceManager.GetString("ToLogCardBalance", cashierForm.DefaultCulture);
                        cashierForm.sValuteLabel = cashierForm.ResourceManager.GetString("ValuteLabel" + cashierForm.Country, cashierForm.DefaultCulture);
                        cashierForm.sCardDispenserWork = cashierForm.ResourceManager.GetString("CardDispenserWork", cashierForm.DefaultCulture);
                        cashierForm.ToLogBankRequest = cashierForm.ResourceManager.GetString("ToLogBankRequest", cashierForm.DefaultCulture);
                        cashierForm.ToLogIDCard = cashierForm.ResourceManager.GetString("ToLogIDCard", cashierForm.DefaultCulture);
                        cashierForm.ToLogCardInStopList = cashierForm.ResourceManager.GetString("ToLogCardInStopList", cashierForm.DefaultCulture);



                        //Список тарифов
                        {
                            cashierForm.tariffs.Clear();
                            if (cashierForm.EmulatorEnabled)
                            {
                                cashierForm.EmulatorForm.Emulator.ReaderTariffID.Items.Clear();
                                cashierForm.EmulatorForm.Emulator.ReaderSheduleID.Items.Clear();
                            }

                            req = "select count(*) as C from Tariffs where (_IsDeleted is null or _IsDeleted = 0)";
                            DataRow tt = null;
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }


                            int tpc = 0;
                            if (tt != null)
                                tpc = (int)tt.GetInt("C");

                            if (tpc > 0)
                            {
                                req = "select *  from Tariffs where (_IsDeleted is null or _IsDeleted = 0)  order by IdFC";
                                DataTable t = null;
                                lock (cashierForm.db_lock)
                                {
                                    try
                                    {
                                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            t = UtilsBase.FillTable(req, cashierForm.db, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                if (t != null && t.Rows.Count > 0)
                                    for (int i = 0; i < t.Rows.Count; i++)
                                    {
                                        TariffsT tp = SetTariffs(t.Rows[i]);
                                        cashierForm.tariffs.Add(tp);
                                        if (cashierForm.EmulatorEnabled)
                                            cashierForm.EmulatorForm.Emulator.ReaderTariffID.Items.Add(tp.Name);
                                    }
                            }
                        }
                        //Список тарифных сеток
                        {
                            cashierForm.TariffSchedule.Clear();
                            req = "select count(*) as C from TariffScheduleModel where (_IsDeleted is null or _IsDeleted = 0)";
                            DataRow tt = null;
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            int tps = 0;
                            if (tt != null)
                                tps = (int)tt.GetInt("C");

                            if (tps > 0)
                            {
                                req = "select *  from TariffScheduleModel where (_IsDeleted is null or _IsDeleted = 0)   order by IdFC";
                                DataTable t = null;
                                lock (cashierForm.db_lock)
                                {
                                    try
                                    {
                                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            t = UtilsBase.FillTable(req, cashierForm.db, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                if (t != null && t.Rows.Count > 0)
                                    for (int i = 0; i < t.Rows.Count; i++)
                                    {
                                        TariffScheduleModelT tp = cashierForm.SetTarifiTS(t.Rows[i]);
                                        cashierForm.TariffSchedule.Add(tp);
                                        if (cashierForm.EmulatorEnabled)
                                            cashierForm.EmulatorForm.Emulator.ReaderSheduleID.Items.Add(tp.Name);
                                    }
                            }
                        }

                        //Список групп
                        {
                            cashierForm.groups.Clear();
                            req = "select count(*) as C from [Group]";
                            DataRow tt = null;
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            int tpc = 0;
                            if (tt != null)
                                tpc = (int)tt.GetInt("C");

                            if (tpc > 0)
                            {
                                req = "select *  from [Group] order by IdFC";
                                DataTable t = null;
                                lock (cashierForm.db_lock)
                                {
                                    try
                                    {
                                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            t = UtilsBase.FillTable(req, cashierForm.db, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                if (t != null && t.Rows.Count > 0)
                                    for (int i = 0; i < t.Rows.Count; i++)
                                    {
                                        GroupT tp = new GroupT();
                                        tp.Id = (Guid)t.Rows[i].GetGuid("Id");
                                        tp.Sync = t.Rows[i].GetDateTime("Sync");
                                        tp.idFC = (byte?)t.Rows[i].GetInt("idFC");
                                        tp.Name = t.Rows[i].GetString("Name");
                                        cashierForm.groups.Add(tp);
                                        /*if (cashierForm.EmulatorEnabled)
                                            cashierForm.EmulatorForm.Emulator.ReaderTariffID.Items.Add(tp.Name);*/
                                    }
                            }
                        }
                        //Список зон
                        {
                            cashierForm.zones.Clear();
                            /*if (cashierForm.EmulatorEnabled)
                            {
                                cashierForm.EmulatorForm.Emulator.ReaderTariffID.Items.Clear();
                                cashierForm.EmulatorForm.Emulator.ReaderSheduleID.Items.Clear();
                            }*/
                            req = "select count(*) as C from ZoneModel where (_IsDeleted is null or _IsDeleted = 0)";
                            DataRow tt = null;
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        tt = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            int tpc = 0;
                            if (tt != null)
                                tpc = (int)tt.GetInt("C");

                            if (tpc > 0)
                            {
                                req = "select *  from ZoneModel where (_IsDeleted is null or _IsDeleted = 0) order by IdFC";
                                DataTable t = null;
                                lock (cashierForm.db_lock)
                                {
                                    try
                                    {
                                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            t = UtilsBase.FillTable(req, cashierForm.db, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                if (t != null && t.Rows.Count > 0)
                                    for (int i = 0; i < t.Rows.Count; i++)
                                    {
                                        ZoneModelT tp = new ZoneModelT();
                                        tp.Id = (Guid)t.Rows[i].GetGuid("Id");
                                        tp.Sync = t.Rows[i].GetDateTime("Sync");
                                        tp.IdFC = (byte?)t.Rows[i].GetInt("idFC");
                                        tp.Name = t.Rows[i].GetString("Name");
                                        tp.Capacity = t.Rows[i].GetInt("Capacity");
                                        tp.Reserved = t.Rows[i].GetInt("Reserved");
                                        tp.FreeSpace = t.Rows[i].GetInt("FreeSpace");
                                        tp.OccupId = t.Rows[i].GetInt("OccupId");
                                        tp._IsDeleted = t.Rows[i].GetBool("_IsDeleted");
                                        cashierForm.zones.Add(tp);
                                        /*if (cashierForm.EmulatorEnabled)
                                            cashierForm.EmulatorForm.Emulator.ReaderTariffID.Items.Add(tp.Name);*/
                                    }
                            }
                        }
                        // Чтение других настроек
                        if ((cashierForm.deviceCMModel.CardKey == null) || (cashierForm.deviceCMModel.CardKey == "FFFFFFFFFFFF") || (cashierForm.deviceCMModel.CardKey == ""))
                        {
                            encrypted = cashierForm.cryp.EncryptStringToBytes_Aes("FFFFFFFFFFFF");
                            cashierForm.deviceCMModel.CardKey = cashierForm.cryp.ByteAToString(encrypted);
                            lock (cashierForm.db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CardKey", cashierForm.deviceCMModel.CardKey }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CardKey", cashierForm.deviceCMModel.CardKey }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            cashierForm.SettingsLastUpdate = DateTime.Now;
                        }

                        encrypted = cashierForm.cryp.StringToByteA(cashierForm.deviceCMModel.CardKey);
                        CashierKey.Password = cashierForm.cryp.DecryptStringFromBytes_Aes(encrypted);
                        if (cashierForm.CardReader != null)
                            cashierForm.CardReader.KeyA = CashierKey.Password;
                        cashierForm.keyA = CashierKey.Password;
                        // Номер кассы
                        CashierNumber.Text = cashierForm.deviceCMModel.CashierNumber.ToString();
                        if (cashierForm.deviceCMModel.CashierType != null)
                        {
                            CashierType.Text = cashierForm.CashierTypes[(int)cashierForm.deviceCMModel.CashierType].Name;
                            cashierForm.CashierType = (byte)cashierForm.deviceCMModel.CashierType;
                            CashierTypeSelectedIndex = (int)cashierForm.deviceCMModel.CashierType;
                        }
                        else
                        {
                            CashierType.Text = cashierForm.CashierTypes[0].Name;
                            cashierForm.CashierType = 0;
                            cashierForm.deviceCMModel.CashierType = 0;
                            CashierTypeSelectedIndex = 0;
                        }
                        if (cashierForm.deviceCMModel.CashierType == 1)
                            CoinDelivery.Visibility = Visibility.Hidden;

                        cashierForm.CashAcceptorExecutableDevices.Clear();
                        cashierForm.CashDispenserExecutableDevices.Clear();
                        cashierForm.CoinAcceptorExecutableDevices.Clear();
                        cashierForm.HopperExecutableDevices.Clear();
                        // Список моделей устройств
                        for (int i = 0; i < cashierForm.ExecutableDevices.Count(); i++)
                        {
                            switch (cashierForm.CashierType)
                            {
                                case 0:
                                    if ((cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashAcceptor) ||
                                        (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler))
                                    {
                                        cashierForm.CashAcceptorExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }

                                    if ((cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashDispenser) ||
                                        (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler))
                                    {
                                        cashierForm.CashDispenserExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }
                                    if (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCoinAcceptor)
                                    {
                                        cashierForm.CoinAcceptorExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }

                                    if (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enHopper)
                                    {
                                        cashierForm.HopperExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }

                                    break;
                                case 1:
                                    if (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler)
                                    {
                                        cashierForm.CashAcceptorExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }

                                    if (cashierForm.ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCashRecycler)
                                    {
                                        cashierForm.CashDispenserExecutableDevices.Add(cashierForm.ExecutableDevices[i]);
                                    }
                                    break;
                            }
                        }

                        switch (cashierForm.CashierType)
                        {
                            case 0:
                                CoinAcceptorButton.IsEnabled = true;
                                CoinHopperButton.IsEnabled = true;
                                CashAcceptorButton.IsEnabled = true;
                                CashDispenserButton.IsEnabled = true;
                                CardDispenserGroup.IsEnabled = true;
                                DeliveryButton.IsEnabled = true;
                                SlaveGroup.IsEnabled = true;
                                break;
                            case 1:
                                CoinAcceptorButton.IsEnabled = false;
                                CoinAcceptorExists.IsChecked = false;
                                cashierForm.deviceCMModel.CoinAcceptorExist = false;
                                CoinHopperButton.IsEnabled = false;
                                CoinHopperExists.IsChecked = false;
                                cashierForm.deviceCMModel.HopperExist = false;
                                CashAcceptorButton.IsEnabled = true;
                                CashDispenserButton.IsEnabled = true;
                                CardDispenserGroup.IsEnabled = true;
                                DeliveryButton.IsEnabled = true;
                                SlaveGroup.IsEnabled = true;
                                break;
                            case 2:
                                CoinAcceptorButton.IsEnabled = false;
                                CoinAcceptorExists.IsChecked = false;
                                cashierForm.deviceCMModel.CoinAcceptorExist = false;
                                CoinHopperButton.IsEnabled = false;
                                CoinHopperExists.IsChecked = false;
                                cashierForm.deviceCMModel.HopperExist = false;
                                CashAcceptorButton.IsEnabled = false;
                                CashAcceptorExists.IsChecked = false;
                                cashierForm.deviceCMModel.CashAcceptorExist = false;
                                CashDispenserButton.IsEnabled = false;
                                CashDispenserExists.IsChecked = false;
                                cashierForm.deviceCMModel.CashDispenserExist = false;
                                CardDispenserGroup.IsEnabled = false;
                                CardDispenserExists.IsChecked = false;
                                cashierForm.deviceCMModel.CardDispenserExists = false;
                                SlaveGroup.IsEnabled = false;
                                SlaveExists.IsChecked = false;
                                DeliveryButton.IsEnabled = false;
                                cashierForm.deviceCMModel.SlaveExist = false;
                                break;
                        }

                        // Уровень логоирования
                        try
                        {
                            LogLevel.Text = cashierForm.deviceCMModel.LogLevel.ToString();
                            cashierForm.log.Level = cashierForm.deviceCMModel.LogLevel;
                        }
                        catch
                        {
                            LogLevel.Text = "2";
                            cashierForm.log.Level = 2;
                        }
                        // НДС
                        if (cashierForm.deviceCMModel.VAT != null)
                            VATIndex = (int)cashierForm.deviceCMModel.VAT;
                        switch (VATIndex)
                        {
                            case 0:
                                if (VAT.Text != cashierForm.ResourceManager.GetString("VAT0", cashierForm.DefaultCulture))
                                    VAT.Text = cashierForm.ResourceManager.GetString("VAT0", cashierForm.DefaultCulture);
                                //VAT.Text = "Без НДС";
                                break;
                            case 1:
                                if (VAT.Text != cashierForm.ResourceManager.GetString("VAT1", cashierForm.DefaultCulture))
                                    VAT.Text = cashierForm.ResourceManager.GetString("VAT1", cashierForm.DefaultCulture);
                                //VAT.Text = "НДС - 0%";
                                break;
                            case 2:
                                if (VAT.Text != cashierForm.ResourceManager.GetString("VAT2", cashierForm.DefaultCulture))
                                    VAT.Text = cashierForm.ResourceManager.GetString("VAT2", cashierForm.DefaultCulture);
                                //VAT.Text = "НДС - 10%";
                                break;
                            case 3:
                                if (VAT.Text != cashierForm.ResourceManager.GetString("VAT3", cashierForm.DefaultCulture))
                                    VAT.Text = cashierForm.ResourceManager.GetString("VAT3", cashierForm.DefaultCulture);
                                //VAT.Text = "НДС - 18%";
                                break;
                        }
                        try
                        {
                            CancelRefund.IsChecked = (bool)cashierForm.deviceCMModel.CancelRefund;
                        }
                        catch
                        {
                            CancelRefund.IsChecked = false;
                        }
                        try
                        {
                            SyncNominals.IsChecked = (bool)cashierForm.deviceCMModel.SyncNominals;
                        }
                        catch
                        {
                            SyncNominals.IsChecked = false;
                        }
                        try
                        {
                            OneTimeClient0.IsChecked = (bool)cashierForm.deviceCMModel.SingleClient0;
                        }
                        catch
                        {
                            OneTimeClient0.IsChecked = true;
                        }

                        // Максимальная сумма на сдачу
                        MaxSummDelivery.Text = cashierForm.deviceCMModel.MaximalChange.ToString();
                        if (MaxSummDelivery.Text == "")
                            MaxSummDelivery.Text = "0";
                        if (cashierForm.deviceCMModel.PenaltyCardTPId != null)
                            for (int i = 0; i < cashierForm.tariffs.Count; i++)
                            {
                                if (cashierForm.deviceCMModel.PenaltyCardTPId == cashierForm.tariffs[i].IdFC)
                                {
                                    PenalCardTPSelectedIndex = i;
                                    PenalCardTP.Text = cashierForm.tariffs[i].Name;
                                    cashierForm.PenalCardTP = (int)cashierForm.tariffs[PenalCardTPSelectedIndex].IdFC;
                                    break;
                                }
                            }
                        else
                            PenalCardTPSelectedIndex = -1;
                        if (cashierForm.deviceCMModel.PenaltyCardTSId != null)
                            for (int i = 0; i < cashierForm.TariffSchedule.Count; i++)
                            {
                                if (cashierForm.deviceCMModel.PenaltyCardTSId == cashierForm.TariffSchedule[i].IdFC)
                                {
                                    PenalCardTSSelectedIndex = i;
                                    PenalCardTS.Text = cashierForm.TariffSchedule[i].Name;
                                    cashierForm.PenalCardTS = (int)cashierForm.TariffSchedule[PenalCardTSSelectedIndex].IdFC;
                                    break;
                                }
                            }
                        else
                            PenalCardTSSelectedIndex = -1;
                        if (cashierForm.deviceCMModel.ZoneId != null)
                            for (int i = 0; i < cashierForm.zones.Count; i++)
                            {
                                if (cashierForm.deviceCMModel.ZoneId == cashierForm.zones[i].Id)
                                {
                                    ZoneComboBoxSelectedIndex = i;
                                    CurrentZone.Text = cashierForm.zones[i].Name;
                                    break;
                                }
                            }
                        else
                            ZoneComboBoxSelectedIndex = -1;
                        if (cashierForm.deviceCMModel.GroupId != null)
                            for (int i = 0; i < cashierForm.groups.Count; i++)
                            {
                                if (cashierForm.deviceCMModel.GroupId == cashierForm.groups[i].Id)
                                {
                                    GroupComboBoxSelectedIndex = i;
                                    CurrentGroup.Text = cashierForm.groups[i].Name;
                                    break;
                                }
                            }
                        else
                        {
                            GroupComboBoxSelectedIndex = -1;
                        }
                        // Время ожидания штрафной карты
                        PenalCardTimeOut.Text = cashierForm.deviceCMModel.PenalCardTimeOut.ToString();
                        if (PenalCardTimeOut.Text == "")
                            PenalCardTimeOut.Text = "60";
                        cashierForm.PenalCardTimeOut = Convert.ToInt16(CardTimeOut.Text);
                        CardTimeOut.Text = cashierForm.deviceCMModel.CardTimeOutToBasket.ToString();
                        if (CardTimeOut.Text == "")
                            CardTimeOut.Text = "60";
                        cashierForm.CardTimeOutToBasket = Convert.ToInt16(CardTimeOut.Text);
                        // Чтение других настроек
                        try
                        {
                            ServerExchangeProtocolSelectedIndex = (int)cashierForm.deviceCMModel.ServerExchangeProtocolId - 1;
                            req = "select * from ServerExchangeProtocolModel order by Id";
                            DataTable t = null;
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    t = UtilsBase.FillTable(req, cashierForm.db, null);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            ServerExchangeProtocol.Text = t.Rows[(int)cashierForm.deviceCMModel.ServerExchangeProtocolId - 1].GetString("Name");
                        }
                        catch
                        {
                            ServerExchangeProtocolSelectedIndex = -1;
                        }
                        ServerUrl.Text = cashierForm.deviceCMModel.ServerURL;
                        if ((cashierForm.Talk != null) && (cashierForm.Talk.ServerURL != ServerUrl.Text))
                            cashierForm.Talk.ServerURL = ServerUrl.Text;

                        req = "select * from DevicesLanguages where DeviceId='" + cashierForm.DeviceId.ToString() +
                            "' order by Language";
                        DataTable dl = null;
                        lock (cashierForm.db_lock)
                        {
                            try
                            {
                                dl = UtilsBase.FillTable(req, cashierForm.db, null);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    dl = UtilsBase.FillTable(req, cashierForm.db, null);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        cashierForm.DevicesLanguages.Clear();
                        if (dl != null && dl.Rows.Count > 0)
                        {
                            for (int i = 0; i < dl.Rows.Count; i++)
                                cashierForm.DevicesLanguages.Add(SetDevicesLanguages(dl.Rows[i]));
                        }

                        bool changed = false;
                        foreach (string[] l in cashierForm.Languages)
                        {
                            DevicesLanguagesT d = cashierForm.DevicesLanguages.Find(x => x.Language == l[0]);
                            if (d == null)
                            {
                                changed = true;
                                d = new DevicesLanguagesT();
                                d.Id = Guid.NewGuid();
                                d.DeviceId = cashierForm.DeviceId;
                                d.Language = l[0];
                                d.Checked = false;
                                lock (cashierForm.db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", d.Id, "DeviceId", d.DeviceId, "Language", d.Language, "Checked", d.Checked });
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", d.Id, "DeviceId", d.DeviceId, "Language", d.Language, "Checked", d.Checked });
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                cashierForm.DevicesLanguages.Add(d);
                            }
                        }

                        for (int i = 0; i < cashierForm.Languages.Count; i++)
                        {
                            if (cashierForm.Languages[i][0] == cashierForm.deviceCMModel.DefaultLanguage)
                            {
                                DefaultLanguageSelectedIndex = i;

                                DevicesLanguagesT d = cashierForm.DevicesLanguages.Find(x => x.Language == cashierForm.Languages[i][0]);
                                if (d != null)
                                {
                                    d.Checked = true;
                                }
                                else
                                {
                                    d = new DevicesLanguagesT();
                                    d.Id = Guid.NewGuid();
                                    d.DeviceId = cashierForm.DeviceId;
                                    d.Language = cashierForm.Languages[i][0];
                                    d.Checked = true;
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", d.Id, "DeviceId", d.DeviceId, "Language", d.Language, "Checked", d.Checked });
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", d.Id, "DeviceId", d.DeviceId, "Language", d.Language, "Checked", d.Checked });
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                    cashierForm.DevicesLanguages.Add(d);
                                }
                            }
                        }
                        DefaultLanguage.Text = cashierForm.Languages[DefaultLanguageSelectedIndex][0];

                        AvailableLanguages.Text = "";
                        try
                        {
                            bool First = true;
                            foreach (DevicesLanguagesT d in cashierForm.DevicesLanguages)
                            {
                                if (cashierForm.Languages[DefaultLanguageSelectedIndex][0] == d.Language)
                                    d.Checked = true;
                                if (d.Checked)
                                {
                                    bool c = false;
                                    foreach (string[] l in cashierForm.Languages)
                                    {
                                        if (d.Language == l[0])
                                        {
                                            c = true;
                                            break;
                                        }
                                    }
                                    if (c)
                                    {
                                        if (First)
                                        {
                                            First = false;
                                            AvailableLanguages.Text = d.Language;
                                        }
                                        else
                                        {
                                            AvailableLanguages.Text += ", " + d.Language;
                                        }
                                    }
                                    else
                                        d.Checked = false;
                                }
                            }

                        }
                        catch
                        {
                        }

                        try
                        {
                            cashierForm.LanguageTimeOut = (int)cashierForm.deviceCMModel.LanguageTimeOut;
                        }
                        catch
                        {
                            cashierForm.LanguageTimeOut = 60;
                        }
                        LanguageTimeOut.Text = cashierForm.LanguageTimeOut.ToString();

                    }
                }
                cashierForm.SetLanguagesVisibility();
                OtherSettingsSave.IsEnabled = false;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void OtherSettingsChanged(object sender, EventArgs e)
        {
            if (OtherSettingsSave != null)
                OtherSettingsSave.IsEnabled = true;
        }

        #region MainSettings
        protected void MainSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    MainSettingsButton.IsChecked = true;
                    FinanceSettingsButton.IsChecked = false;
                    TimeOutSettingsButton.IsChecked = false;
                    InterfaceSettingsButton.IsChecked = false;
                    ButtonsSettingsButton.IsChecked = false;
                    FinanceGroup.Visibility = Visibility.Hidden;
                    TimeOutsGroup.Visibility = Visibility.Hidden;
                    InterfacesGroup.Visibility = Visibility.Hidden;
                    ButtonsGroup.Visibility = Visibility.Hidden;
                    MainGroup.Visibility = Visibility.Visible;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void CasseKey_Enter(object sender, EventArgs e)
        {
            try
            {
                CalcHex AddingSummForm;
                AddingSummForm = new CalcHex();
                AddingSummForm.Len = 12;

                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    if (CashierKey.Password != AddingSummForm.Sum)
                        OtherSettingsSave.IsEnabled = true;
                    CashierKey.Password = AddingSummForm.Sum;
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        private void CashierNumber_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 4;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CashierNumber.Text != AddingSummForm.Sum)
                            OtherSettingsSave.IsEnabled = true;
                        CashierNumber.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        private void LogLevel_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 1;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        int l = Convert.ToInt32(AddingSummForm.Sum);
                        if ((l >= 0) && (l <= 2))
                        {
                            if (LogLevel.Text != AddingSummForm.Sum)
                                OtherSettingsSave.IsEnabled = true;
                            LogLevel.Text = l.ToString();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        private void CashierType_GotFocus(object sender, EventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CashierType;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.CashierTypes.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if (CashierTypeSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.CashierTypes[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        CashierTypeSelectedIndex = p.SelectedIndex;
                        if (cashierForm.deviceCMModel.CashierType != cashierForm.CashierTypes[CashierTypeSelectedIndex].Code)
                            OtherSettingsSave.IsEnabled = true;
                        CashierType.Text = cashierForm.CashierTypes[(byte)CashierTypeSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();

        }
        private void PenalCardTP_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.PenalCardTP;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.tariffs.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if ((PenalCardTPSelectedIndex) == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.tariffs[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (cashierForm.deviceCMModel.PenaltyCardTPId != p.SelectedIndex)
                            OtherSettingsSave.IsEnabled = true;
                        PenalCardTPSelectedIndex = p.SelectedIndex;
                        PenalCardTP.Text = cashierForm.tariffs[PenalCardTPSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();

        }
        private void PenalCardTS_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.PenalCardTS;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.TariffSchedule.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if ((PenalCardTSSelectedIndex) == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.TariffSchedule[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (cashierForm.deviceCMModel.PenaltyCardTSId != (int)cashierForm.TariffSchedule[p.SelectedIndex].IdFC)
                            OtherSettingsSave.IsEnabled = true;
                        PenalCardTSSelectedIndex = p.SelectedIndex;
                        PenalCardTS.Text = cashierForm.TariffSchedule[PenalCardTSSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        private void CurrentZone_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CurrentZone;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.zones.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if ((ZoneComboBoxSelectedIndex) == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.zones[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (cashierForm.deviceCMModel.ZoneId != cashierForm.zones[p.SelectedIndex].Id)
                            OtherSettingsSave.IsEnabled = true;
                        ZoneComboBoxSelectedIndex = p.SelectedIndex;
                        CurrentZone.Text = cashierForm.zones[ZoneComboBoxSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        private void CurrentGroup_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.CurrentGroup;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.groups.Count(); i++)
                {
                    RadioButton r = new RadioButton();
                    if ((GroupComboBoxSelectedIndex) == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = cashierForm.groups[i].Name;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);

                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (cashierForm.deviceCMModel.GroupId != cashierForm.groups[p.SelectedIndex].Id)
                            OtherSettingsSave.IsEnabled = true;
                        GroupComboBoxSelectedIndex = p.SelectedIndex;
                        CurrentGroup.Text = cashierForm.groups[GroupComboBoxSelectedIndex].Name;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        #endregion MainSettings

        #region Finance
        protected void FinanceSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    MainSettingsButton.IsChecked = false;
                    FinanceSettingsButton.IsChecked = true;
                    TimeOutSettingsButton.IsChecked = false;
                    InterfaceSettingsButton.IsChecked = false;
                    ButtonsSettingsButton.IsChecked = false;
                    FinanceGroup.Visibility = Visibility.Visible;
                    TimeOutsGroup.Visibility = Visibility.Hidden;
                    InterfacesGroup.Visibility = Visibility.Hidden;
                    ButtonsGroup.Visibility = Visibility.Hidden;
                    MainGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void NDS_Enter(object sender, EventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.VAT;

                p.ItemsList.Items.Clear();
                for (int i = 0; i < 4; i++)
                {
                    RadioButton r = new RadioButton();
                    switch (i)
                    {
                        case 0:
                            r.Content = "Без НДС";
                            break;
                        case 1:
                            r.Content = "НДС - 0%";
                            break;
                        case 2:
                            r.Content = "НДС - 10%";
                            break;
                        case 3:
                            r.Content = "НДС - 18%";
                            break;
                    }

                    if (VATIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);
                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (cashierForm.deviceCMModel.VAT != p.SelectedIndex)
                            OtherSettingsSave.IsEnabled = true;
                        VATIndex = p.SelectedIndex;
                        switch (VATIndex)
                        {
                            case 0:
                                VAT.Text = "Без НДС";
                                break;
                            case 1:
                                VAT.Text = "НДС - 0%";
                                break;
                            case 2:
                                VAT.Text = "НДС - 10%";
                                break;
                            case 3:
                                VAT.Text = "НДС - 18%";
                                break;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        private void MaxSummDelivery_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 5;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (MaxSummDelivery.Text != AddingSummForm.Sum)
                            OtherSettingsSave.IsEnabled = true;
                        MaxSummDelivery.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        #endregion Finance

        #region TimeOut
        protected void TimeOutSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    MainSettingsButton.IsChecked = false;
                    FinanceSettingsButton.IsChecked = false;
                    TimeOutSettingsButton.IsChecked = true;
                    InterfaceSettingsButton.IsChecked = false;
                    ButtonsSettingsButton.IsChecked = false;
                    FinanceGroup.Visibility = Visibility.Hidden;
                    TimeOutsGroup.Visibility = Visibility.Visible;
                    InterfacesGroup.Visibility = Visibility.Hidden;
                    ButtonsGroup.Visibility = Visibility.Hidden;
                    MainGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void PenalCardTimeOut_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (PenalCardTimeOut.Text != AddingSummForm.Sum)
                            OtherSettingsSave.IsEnabled = true;
                        PenalCardTimeOut.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        private void TimeOutToBasket_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (CardTimeOut.Text != AddingSummForm.Sum)
                            OtherSettingsSave.IsEnabled = true;
                        CardTimeOut.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        private void LanguageTimeOut_Enter(object sender, EventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        if (LanguageTimeOut.Text != AddingSummForm.Sum)
                            OtherSettingsSave.IsEnabled = true;
                        LanguageTimeOut.Text = AddingSummForm.Sum;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }
        /*private void KKMChequeTime1_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                AddingSummForm.Len = 3;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value && (AddingSummForm.Sum != ""))
                {
                    try
                    {
                        KKMChequeTime.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        //KKMChequeTime1.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                        OtherSettingsSave.IsEnabled = true;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            OtherSettingsReturn.Focus();
        }*/
        #endregion TimeOut

        #region Interface
        protected void InterfaceSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    MainSettingsButton.IsChecked = false;
                    FinanceSettingsButton.IsChecked = false;
                    TimeOutSettingsButton.IsChecked = false;
                    InterfaceSettingsButton.IsChecked = true;
                    ButtonsSettingsButton.IsChecked = false;
                    FinanceGroup.Visibility = Visibility.Hidden;
                    TimeOutsGroup.Visibility = Visibility.Hidden;
                    InterfacesGroup.Visibility = Visibility.Visible;
                    ButtonsGroup.Visibility = Visibility.Hidden;
                    MainGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void ServerExchageProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((ServerExchangeProtocolSelectedIndex + 1) == 1)
                {
                    string strhas = "select Value from Setting where Name='ServerDomain'";
                    try
                    {
                        using (SqlCommand comm = new SqlCommand(strhas, cashierForm.db))
                        {
                            object res = comm.ExecuteScalar();
                            if (res != null && res != System.DBNull.Value)
                            {
                                try
                                {
                                    ServerUrl.Text = Convert.ToString(res);
                                }
                                catch { }
                            }
                        }
                    }
                    catch { }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void ServerExchangeProtocol_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpDynamic p = new PopUpDynamic();
                p.PopUpDynamicHeader.Text = Properties.Resources.ServerExchageProtocol;

                p.ItemsList.Items.Clear();
                string req = "select * from ServerExchangeProtocolModel order by Id";
                DataTable t = null;
                lock (cashierForm.db_lock)
                {
                    try
                    {
                        t = UtilsBase.FillTable(req, cashierForm.db, null);
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                        try
                        {
                            t = UtilsBase.FillTable(req, cashierForm.db, null);
                        }
                        catch (Exception exc1)
                        {
                            cashierForm.ToLog(0, exc1.ToString());
                        }
                    }
                }
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    RadioButton r = new RadioButton();
                    if ((ServerExchangeProtocolSelectedIndex) == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;
                    r.Content = t.Rows[i].GetString("Name");
                    r.Height = 30;
                    r.VerticalAlignment = VerticalAlignment.Top;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(0, 10, 0, 0);
                    r.Click += new RoutedEventHandler(p.RadioButton_Click);
                    p.ItemsList.Items.Add(r);
                }
                p.Left = 332;
                p.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                p.Topmost = true;
                p.ShowDialog();
                if (p.DialogResult.HasValue && p.DialogResult.Value)
                {
                    try
                    {
                        if (ServerExchangeProtocolSelectedIndex != p.SelectedIndex)
                            OtherSettingsSave.IsEnabled = true;
                        ServerExchangeProtocolSelectedIndex = p.SelectedIndex;
                        ServerExchangeProtocol.Text = (string)((RadioButton)p.ItemsList.Items[ServerExchangeProtocolSelectedIndex]).Content;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        private void DefaultLanguage_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpLanguages l = new PopUpLanguages();

                //l.ItemsList.SelectionChanged += new SelectionChangedEventHandler(l.RadioButton_Click);
                l.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.Languages.Count; i++)
                {
                    StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 10, 0, 10) };
                    RadioButton r = new RadioButton();
                    Image im = new Image() { Width = 47, VerticalAlignment = VerticalAlignment.Center, Stretch = Stretch.Fill, Height = 42, HorizontalAlignment = HorizontalAlignment.Left };
                    sp.Children.Add(im);
                    //sp.TouchDown += new EventHandler<TouchEventArgs>(l.RadioButton_Click);
                    //sp.MouseDown += new MouseButtonEventHandler(l.RadioButton_Click);

                    //Создадим экземпляр класса BitmapImage, пропишем ему путь к ресурсу с картинкой, и установим режим создания BitmapImage.
                    im.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Flags/" + cashierForm.Languages[i][0] + ".png")) { CreateOptions = BitmapCreateOptions.IgnoreImageCache };
                    im.TouchDown += new EventHandler<TouchEventArgs>(l.RadioButton_Click);
                    im.MouseDown += new MouseButtonEventHandler(l.RadioButton_Click);
                    if (DefaultLanguageSelectedIndex == i)
                        r.IsChecked = true;
                    else
                        r.IsChecked = false;

                    r.Content = cashierForm.Languages[i][1];
                    r.Height = 30;
                    r.HorizontalAlignment = HorizontalAlignment.Center;
                    r.VerticalAlignment = VerticalAlignment.Center;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(20, 0, 0, 0);
                    r.Click += new RoutedEventHandler(l.RadioButton_Click);
                    sp.Children.Add(r);

                    l.ItemsList.Items.Add(sp);
                }
                l.Left = 332;
                l.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                l.Topmost = true;
                l.ShowDialog();
                if (l.DialogResult.HasValue && l.DialogResult.Value)
                {
                    try
                    {
                        if (DefaultLanguageSelectedIndex != l.SelectedIndex)
                            OtherSettingsSave.IsEnabled = true;
                        DefaultLanguageSelectedIndex = l.SelectedIndex;
                        DefaultLanguage.Text = cashierForm.Languages[DefaultLanguageSelectedIndex][0];
                        string req = "select * from DevicesLanguages where DeviceId='" + cashierForm.DeviceId.ToString() +
                            "' and Language='" + DefaultLanguage.Text + "'";
                        DataRow dl1t = null;
                        lock (cashierForm.db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                dl1t = UtilsBase.FillRow(req, cashierForm.db, null, null);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    dl1t = UtilsBase.FillRow(req, cashierForm.db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (dl1t != null)
                        {
                            lock (cashierForm.db_lock)
                            {
                                try
                                {
                                    UtilsBase.SetOne("DevicesLanguages", "Id", new object[] { "Checked", true }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'" +
                                    "' and Language='" + DefaultLanguage.Text + "'", null, cashierForm.db);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DevicesLanguages", "Id", new object[] { "Checked", true }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'" +
                                        "' and Language='" + DefaultLanguage.Text + "'", null, cashierForm.db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            DevicesLanguagesT dl1 = new DevicesLanguagesT();
                            dl1.Id = Guid.NewGuid();
                            dl1.DeviceId = cashierForm.DeviceId;
                            dl1.Language = DefaultLanguage.Text;
                            dl1.Checked = true;
                            lock (cashierForm.db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl1.Id, "DeviceId", dl1.DeviceId, "Language", dl1.Language, "Checked", true });
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl1.Id, "DeviceId", dl1.DeviceId, "Language", dl1.Language, "Checked", true });
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }
                        req = "select * from DevicesLanguages where DeviceId='" + cashierForm.DeviceId.ToString() +
                            "' order by Language";
                        DataTable dlt = null;
                        lock (cashierForm.db_lock)
                        {
                            try
                            {
                                dlt = UtilsBase.FillTable(req, cashierForm.db, null);
                            }
                            catch (Exception exc)
                            {
                                cashierForm.ToLog(0, exc.ToString());
                                try
                                {
                                    dlt = UtilsBase.FillTable(req, cashierForm.db, null);
                                }
                                catch (Exception exc1)
                                {
                                    cashierForm.ToLog(0, exc1.ToString());
                                }
                            }
                        }

                        bool First = true;
                        for (int i = 0; i < dlt.Rows.Count; i++)
                        {
                            DevicesLanguagesT d = SetDevicesLanguages(dlt.Rows[i]);
                            if (cashierForm.Languages[DefaultLanguageSelectedIndex][0] == d.Language)
                                d.Checked = true;

                            if (d.Checked)
                            {
                                if (First)
                                {
                                    First = false;
                                    AvailableLanguages.Text = d.Language;
                                }
                                else
                                {
                                    AvailableLanguages.Text += ", " + d.Language;
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        private void AvailableLanguages_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                PopUpAvailableLanguages l = new PopUpAvailableLanguages();
                for (int i = 0; i < cashierForm.DevicesLanguages.Count; i++)
                {
                    if (i == DefaultLanguageSelectedIndex)
                        cashierForm.DevicesLanguages[i].Checked = true;
                }

                l.ItemsList.Items.Clear();
                for (int i = 0; i < cashierForm.Languages.Count; i++)
                {
                    StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 10, 0, 10) };
                    CheckBox r = new CheckBox();
                    Image im = new Image() { Width = 47, VerticalAlignment = VerticalAlignment.Center, Stretch = Stretch.Fill, Height = 42, HorizontalAlignment = HorizontalAlignment.Left };
                    sp.Children.Add(im);
                    sp.TouchDown += new EventHandler<TouchEventArgs>(l.CheckBox_Click);
                    sp.MouseDown += new MouseButtonEventHandler(l.CheckBox_Click);
                    //Создадим экземпляр класса BitmapImage, пропишем ему путь к ресурсу с картинкой, и установим режим создания BitmapImage.
                    im.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Flags/" + cashierForm.Languages[i][0] + ".png")) { CreateOptions = BitmapCreateOptions.IgnoreImageCache };
                    im.TouchDown += new EventHandler<TouchEventArgs>(l.CheckBox_Click);
                    im.MouseDown += new MouseButtonEventHandler(l.CheckBox_Click);
                    for (int j = 0; j < cashierForm.DevicesLanguages.Count; j++)
                    {
                        if (cashierForm.DevicesLanguages[j].Language == cashierForm.Languages[i][0])
                        {
                            if (cashierForm.DevicesLanguages[j].Checked)
                                r.IsChecked = true;
                            else
                                r.IsChecked = false;
                        }
                    }

                    r.Content = cashierForm.Languages[i][1];
                    r.Height = 30;
                    r.HorizontalAlignment = HorizontalAlignment.Center;
                    r.VerticalAlignment = VerticalAlignment.Center;
                    r.FontSize = 16;
                    r.FontWeight = FontWeights.Bold;
                    r.Margin = new Thickness(20, 0, 0, 0);
                    r.Click += new RoutedEventHandler(l.CheckBox_Click);
                    sp.Children.Add(r);

                    l.ItemsList.Items.Add(sp);
                }
                l.Left = 332;
                l.Top = 80;
                PopUpShadow.Visibility = Visibility.Visible;
                l.Topmost = true;
                l.ShowDialog();
                if (l.DialogResult.HasValue && l.DialogResult.Value)
                {
                    try
                    {
                        bool First = true;
                        foreach (DevicesLanguagesT d in cashierForm.DevicesLanguages)
                            d.Checked = false;

                        for (int i = 0; i < l.ItemsList.Items.Count; i++)
                        {
                            if (i == DefaultLanguageSelectedIndex)
                            {
                                DevicesLanguagesT dl = cashierForm.DevicesLanguages.Find(x => x.Language == cashierForm.Languages[i][0]);
                                if (dl != null)
                                {
                                    dl.Checked = true;
                                }
                                else
                                {
                                    dl = new DevicesLanguagesT();
                                    dl.Id = Guid.NewGuid();
                                    dl.DeviceId = cashierForm.DeviceId;
                                    dl.Language = cashierForm.Languages[i][0];
                                    dl.Checked = true;
                                    cashierForm.DevicesLanguages.Add(dl);
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl.Id, "DeviceId", dl.DeviceId, "Language", dl.Language, "Checked", dl.Checked });
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl.Id, "DeviceId", dl.DeviceId, "Language", dl.Language, "Checked", dl.Checked });
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                                if (First)
                                {
                                    First = false;
                                    AvailableLanguages.Text = cashierForm.Languages[i][0];
                                }
                                else
                                {
                                    AvailableLanguages.Text += ", " + cashierForm.Languages[i][0];
                                }
                            }
                            else
                            {
                                CheckBox cb;
                                StackPanel sp;
                                sp = (StackPanel)l.ItemsList.Items[i];
                                cb = (CheckBox)sp.Children[1];


                                if ((bool)cb.IsChecked)
                                {
                                    if (First)
                                    {
                                        First = false;
                                        AvailableLanguages.Text = cashierForm.Languages[i][0];
                                    }
                                    else
                                    {
                                        AvailableLanguages.Text += ", " + cashierForm.Languages[i][0];
                                    }
                                }

                                DevicesLanguagesT dl = cashierForm.DevicesLanguages.Find(x => x.Language == cashierForm.Languages[i][0]);
                                if (dl != null)
                                {
                                    dl.Checked = (bool)cb.IsChecked;
                                }
                                else
                                {
                                    dl = new DevicesLanguagesT();
                                    dl.Id = Guid.NewGuid();
                                    dl.DeviceId = cashierForm.DeviceId;
                                    dl.Language = cashierForm.Languages[i][0];
                                    dl.Checked = (bool)cb.IsChecked;
                                    cashierForm.DevicesLanguages.Add(dl);
                                    lock (cashierForm.db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl.Id, "DeviceId", dl.DeviceId, "Language", dl.Language, "Checked", dl.Checked });
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("DevicesLanguages", cashierForm.db, null, new object[] { "Id", dl.Id, "DeviceId", dl.DeviceId, "Language", dl.Language, "Checked", dl.Checked });
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            PopUpShadow.Visibility = Visibility.Hidden;
            OtherSettingsReturn.Focus();
        }
        #endregion Interface

        #region Buttons
        protected void ButtonsSettingsButton_Click(object sender, EventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    MainSettingsButton.IsChecked = false;
                    FinanceSettingsButton.IsChecked = false;
                    TimeOutSettingsButton.IsChecked = false;
                    InterfaceSettingsButton.IsChecked = false;
                    ButtonsSettingsButton.IsChecked = true;
                    FinanceGroup.Visibility = Visibility.Hidden;
                    TimeOutsGroup.Visibility = Visibility.Hidden;
                    InterfacesGroup.Visibility = Visibility.Hidden;
                    ButtonsGroup.Visibility = Visibility.Visible;
                    MainGroup.Visibility = Visibility.Hidden;
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void ReSync_Click(object sender, EventArgs e)
        {
            try
            {
                cashierForm.ReSync();
                //cashierForm.ReSyncReq = true;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        /*private void UnProtect_Click(object sender, EventArgs e)
        {
            try
            {
                cashierForm.UnProtectConfiguration();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }*/
        #endregion Buttons
        #endregion OtherSettings
        #endregion Settings

        #region Delivery
        private void DeliveryButton_Click(object sender, EventArgs e)
        {
            try
            {
                TopBoxCurrency.Text = cashierForm.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                BottomBoxCurrency.Text = cashierForm.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                LeftBoxCurrency.Text = cashierForm.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                RightBoxCurrency.Text = cashierForm.ResourceManager.GetString("ValuteLabel" + cashierForm.Country);
                DeliveryPanel.Visibility = Visibility.Visible;
                TechButtonsPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void UpAdd_button_Click(object sender, EventArgs e)
        {
            try
            {
                //TopBoxAdd.Select();
                bool Current = AddUpperButton.IsEnabled;
                AddUpperButton.IsEnabled = false;
                int Count;
                int Diff;
                if (cashierForm != null)
                {
                    try
                    {
                        Count = Convert.ToInt32(TopBoxCount.Text);
                    }
                    catch
                    {
                        Count = 0;
                    }
                    try
                    {
                        Diff = Convert.ToInt32(TopBoxAdd.Text);
                    }
                    catch
                    {
                        Diff = 0;
                    }
                    int Res = Count + Diff;
                    if (Res < 0)
                        Res = 0;
                    if (Res > cashierForm.deviceCMModel.MaxCountUp)
                        Res = (int)cashierForm.deviceCMModel.MaxCountUp;
                    TopBoxCount.Text = Res.ToString();
                    cashierForm.DeliveryUpCount = Convert.ToInt32(TopBoxCount.Text);
                    cashierForm.DeliveryUpCountChanged = true;

                    TopBoxAdd.Text = "0";
                }
                AddUpperButton.IsEnabled = Current;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void DownAdd_button_Click(object sender, EventArgs e)
        {
            try
            {
                //DownCount.Select();
                bool Current = AddLowerButton.IsEnabled;
                AddLowerButton.IsEnabled = false;
                int Count;
                int Diff;
                if (cashierForm != null)
                {
                    try
                    {
                        Count = Convert.ToInt32(BottomBoxCount.Text);
                    }
                    catch
                    {
                        Count = 0;
                    }
                    try
                    {
                        Diff = Convert.ToInt32(BottomBoxAdd.Text);
                    }
                    catch
                    {
                        Diff = 0;
                    }
                    int Res = Count + Diff;
                    if (Res < 0)
                        Res = 0;
                    if (Res > cashierForm.deviceCMModel.MaxCountDown)
                        Res = (int)cashierForm.deviceCMModel.MaxCountDown;
                    BottomBoxCount.Text = Res.ToString();
                    cashierForm.DeliveryDownCount = Convert.ToInt32(BottomBoxCount.Text);
                    cashierForm.DeliveryDownCountChanged = true;
                    BottomBoxAdd.Text = "0";
                }
                AddLowerButton.IsEnabled = Current;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void LeftAdd_button_Click(object sender, EventArgs e)
        {
            try
            {
                //LeftCount.Select();
                bool Current = AddLeftButton.IsEnabled;
                AddLeftButton.IsEnabled = false;
                int Count;
                int Diff;
                if (cashierForm != null)
                {
                    try
                    {
                        Count = Convert.ToInt32(LeftBoxCount.Text);
                    }
                    catch
                    {
                        Count = 0;
                    }
                    try
                    {
                        Diff = Convert.ToInt32(LeftBoxAdd.Text);
                    }
                    catch
                    {
                        Diff = 0;
                    }
                    int Res = Count + Diff;
                    if (Res < 0)
                        Res = 0;
                    if (Res > cashierForm.deviceCMModel.MaxCount1)
                        Res = (int)cashierForm.deviceCMModel.MaxCount1;
                    LeftBoxCount.Text = Res.ToString();
                    cashierForm.Delivery1Count = Convert.ToInt32(LeftBoxCount.Text);
                    cashierForm.DeliveryLeftCountChanged = true;
                    LeftBoxAdd.Text = "0";
                }
                if (cashierForm.Delivery1Count > 0)
                {
                    if (CoinHopperLeftClean.Visibility != Visibility.Visible)
                        CoinHopperLeftClean.Visibility = Visibility.Visible;
                }
                else
                {
                    if (CoinHopperLeftClean.Visibility != Visibility.Hidden)
                        CoinHopperLeftClean.Visibility = Visibility.Hidden;
                }
                AddLeftButton.IsEnabled = Current;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void RightAdd_button_Click(object sender, EventArgs e)
        {
            try
            {
                //RightCount.Select();
                bool Current = AddRightButton.IsEnabled;
                AddRightButton.IsEnabled = false;
                int Count;
                int Diff;
                if (cashierForm != null)
                {
                    try
                    {
                        Count = Convert.ToInt32(RightBoxCount.Text);
                    }
                    catch
                    {
                        Count = 0;
                    }
                    try
                    {
                        Diff = Convert.ToInt32(RightBoxAdd.Text);
                    }
                    catch
                    {
                        Diff = 0;
                    }
                    int Res = Count + Diff;
                    if (Res < 0)
                        Res = 0;
                    if (Res > cashierForm.deviceCMModel.MaxCount2)
                        Res = (int)cashierForm.deviceCMModel.MaxCount2;
                    RightBoxCount.Text = Res.ToString();
                    cashierForm.Delivery2Count = Convert.ToInt32(RightBoxCount.Text);
                    cashierForm.DeliveryRightCountChanged = true;
                    RightBoxAdd.Text = "0";
                }
                if (cashierForm.Delivery2Count > 0)
                {
                    if (CoinHopperRightClean.Visibility != Visibility.Visible)
                        CoinHopperRightClean.Visibility = Visibility.Visible;
                }
                else
                {
                    if (CoinHopperRightClean.Visibility != Visibility.Hidden)
                        CoinHopperRightClean.Visibility = Visibility.Hidden;
                }
                AddRightButton.IsEnabled = Current;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void ClearRejectBox_Click(object sender, EventArgs e)
        {
            try
            {
                cashierForm.RejectCount = 0;
                if (cashierForm.CashDispenser != null)
                {
                    cashierForm.CashDispenser.CloseCassette();
                    cashierForm.CashDispenser.OpenCassette();
                    cashierForm.CashDispenser.ReadCassetteID();
                }
                RejectBoxCount.Text = cashierForm.RejectCount.ToString();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void CoinHopperLeftClean_Click(object sender, RoutedEventArgs e)
        {
            if (cashierForm.deviceCMModel.MaxCount1 != null && ((int)cashierForm.deviceCMModel.MaxCount1) != 0)
            {
                int c = (int)Math.Round((decimal)(int)cashierForm.deviceCMModel.MaxCount1 / 255) + 1;
                for (int i = 0; i < c; i++)
                {
                    if (cashierForm.CoinHopper.MoveForward(255, 0))
                    {
                        Thread.Sleep(100);
                        while (cashierForm.CoinHopper.CommandStatus != CommandStatusT.Completed)
                            Thread.Sleep(100);
                        Thread.Sleep(100);
                        if (cashierForm.CoinHopper.HoppertStatus[0].Unpaid > 0) break;
                    }
                    else break;
                }
                cashierForm.Delivery1Count = 0;
                LeftBoxCount.Text = "0";
                cashierForm.DeliveryLeftCountChanged = true;
                Thread.Sleep(100);
                cashierForm.CoinHopper.Reset(false);
                Thread.Sleep(100);
            }
        }

        private void CoinHopperRightClean_Click(object sender, RoutedEventArgs e)
        {
            if (cashierForm.deviceCMModel.MaxCount2 != null && ((int)cashierForm.deviceCMModel.MaxCount2) != 0)
            {
                int c = (int)Math.Round((decimal)(int)cashierForm.deviceCMModel.MaxCount2 / 100) + 1;
                for (int i = 0; i < c; i++)
                {
                    if (cashierForm.CoinHopper.MoveForward(0, 100))
                    {
                        Thread.Sleep(100);
                        while (cashierForm.CoinHopper.CommandStatus != CommandStatusT.Completed)
                            Thread.Sleep(200);
                        Thread.Sleep(100);
                        if (cashierForm.CoinHopper.HoppertStatus[1].Unpaid > 0) break;
                    }
                    else break;
                }
                cashierForm.Delivery2Count = 0;
                RightBoxCount.Text = "0";
                cashierForm.DeliveryRightCountChanged = true;
                Thread.Sleep(100);
                cashierForm.CoinHopper.Reset(true);
                Thread.Sleep(100);
            }
        }

        private void DeliveryReturnButton_Click(object sender, EventArgs e)
        {
            bool DeliveryUpCountChanged = false, DeliveryDownCountChanged = false;
            bool DeliveryFrontCountChanged = false, DeliveryBackCountChanged = false;
            if (cashierForm != null)
            {
                try
                {
                    lock (cashierForm.db_lock)
                    {
                        if (cashierForm.DeliveryUpCount != cashierForm.deviceCMModel.CountUp)
                            DeliveryUpCountChanged = true;
                        cashierForm.deviceCMModel.CountUp = Convert.ToInt32(TopBoxCount.Text);
                        cashierForm.DeliveryUpCount = (int)cashierForm.deviceCMModel.CountUp;

                        if (cashierForm.deviceCMModel.CountDown != Convert.ToInt32(BottomBoxCount.Text))
                            DeliveryDownCountChanged = true;
                        cashierForm.deviceCMModel.CountDown = Convert.ToInt32(BottomBoxCount.Text);
                        cashierForm.DeliveryDownCount = (int)cashierForm.deviceCMModel.CountDown;

                        if (cashierForm.Delivery1Count != cashierForm.deviceCMModel.Count1)
                            DeliveryFrontCountChanged = true;
                        cashierForm.deviceCMModel.Count1 = Convert.ToInt32(LeftBoxCount.Text);
                        cashierForm.Delivery1Count = (int)cashierForm.deviceCMModel.Count1;

                        if (cashierForm.Delivery2Count != cashierForm.deviceCMModel.Count2)
                            DeliveryBackCountChanged = true;
                        cashierForm.deviceCMModel.Count2 = Convert.ToInt32(RightBoxCount.Text);
                        cashierForm.Delivery2Count = (int)cashierForm.deviceCMModel.Count2;

                        cashierForm.deviceCMModel.RejectCount = Convert.ToInt32(RejectBoxCount.Text);
                        cashierForm.RejectCount = (int)cashierForm.deviceCMModel.RejectCount;
                        //while (Utils.Running)
                        //    Thread.Sleep(50);
                        try
                        {
                            UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CountUp", cashierForm.deviceCMModel.CountUp, "CountDown", cashierForm.deviceCMModel.CountDown, "Count1", cashierForm.deviceCMModel.Count1, "Count2", cashierForm.deviceCMModel.Count2, "RejectCount", cashierForm.deviceCMModel.RejectCount }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                        }
                        catch (Exception exc)
                        {
                            cashierForm.ToLog(0, exc.ToString());
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CountUp", cashierForm.deviceCMModel.CountUp, "CountDown", cashierForm.deviceCMModel.CountDown, "Count1", cashierForm.deviceCMModel.Count1, "Count2", cashierForm.deviceCMModel.Count2, "RejectCount", cashierForm.deviceCMModel.RejectCount }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                            }
                            catch (Exception exc1)
                            {
                                cashierForm.ToLog(0, exc1.ToString());
                            }
                        }
                    }

                    try
                    {
                        if (DeliveryUpCountChanged || DeliveryDownCountChanged || DeliveryFrontCountChanged || DeliveryBackCountChanged)
                        {
                            if (DeliveryUpCountChanged)
                            {
                                if ((cashierForm.deviceCMModel.CashDispenserExist != null && (bool)cashierForm.deviceCMModel.CashDispenserExist) && (!cashierForm.EmulatorEnabled || !cashierForm.EmulatorForm.Emulator.CashDispenserVirt.Checked))
                                {
                                    if (cashierForm.CashDispenser != null)
                                        cashierForm.CashDispenser.SetCount(1, cashierForm.DeliveryUpCount);
                                }
                            }
                            if (DeliveryDownCountChanged)
                            {
                                if ((cashierForm.deviceCMModel.CashDispenserExist != null && (bool)cashierForm.deviceCMModel.CashDispenserExist) && (!cashierForm.EmulatorEnabled || !cashierForm.EmulatorForm.Emulator.CashDispenserVirt.Checked))
                                {
                                    if (cashierForm.CashDispenser != null)
                                        cashierForm.CashDispenser.SetCount(2, cashierForm.DeliveryDownCount);
                                }
                            }
                            if ((cashierForm.deviceCMModel.CashDispenserExist != null && (bool)cashierForm.deviceCMModel.CashDispenserExist) && (!cashierForm.EmulatorEnabled || !cashierForm.EmulatorForm.Emulator.CashDispenserVirt.Checked) &&
                                (cashierForm.CashDispenserModel == "UBA RC") &&
                                (/*(cashierForm.CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette) &&*/
                                    (cashierForm.DeliveryDownCount > 0) ||
                                    /*(cashierForm.CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette) &&*/
                                    (cashierForm.DeliveryUpCount > 0))
                                  )
                            {
                                if (cashierForm.CashDispenser != null)
                                {
                                    cashierForm.CashDispenser.Reset();
                                    if (!cashierForm.CashAcceptorInhibit)
                                        cashierForm.CashAcceptorInhibit = true;
                                    Thread.Sleep(500);
                                    while (cashierForm.CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Initialize)
                                        Thread.Sleep(100);
                                    Thread.Sleep(500);
                                    if (cashierForm.CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                    {
                                        cashierForm.DeliveryUpCount = 0;
                                        TopBoxCount.Text = cashierForm.DeliveryUpCount.ToString();
                                        cashierForm.CashDispenser.SetCount(1, 0);
                                        DeliveryUpCountChanged = true;
                                    }
                                    if (cashierForm.CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                    {
                                        cashierForm.DeliveryDownCount = 0;
                                        BottomBoxCount.Text = cashierForm.DeliveryDownCount.ToString();
                                        cashierForm.CashDispenser.SetCount(2, 0);
                                        DeliveryDownCountChanged = true;
                                    }
                                }
                            }
                            if (cashierForm.deviceCMModel.HopperExist != null && (bool)cashierForm.deviceCMModel.HopperExist && cashierForm.CoinHopper != null)
                            {
                                if (DeliveryFrontCountChanged)
                                {
                                    cashierForm.CoinHopper.Reset(false);
                                }
                                if (DeliveryBackCountChanged)
                                {
                                    cashierForm.CoinHopper.Reset(true);
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                    }
                    try
                    {
                        if (DeliveryUpCountChanged || DeliveryDownCountChanged)
                        {
                            lock (cashierForm.db_lock)
                            {
                                cashierForm.deviceCMModel.CountUp = cashierForm.DeliveryUpCount;
                                cashierForm.deviceCMModel.CountDown = cashierForm.DeliveryDownCount;
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CountUp", cashierForm.deviceCMModel.CountUp, "CountDown", cashierForm.deviceCMModel.CountDown }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                }
                                catch (Exception exc)
                                {
                                    cashierForm.ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", cashierForm.DeviceId, "CountUp", cashierForm.deviceCMModel.CountUp, "CountDown", cashierForm.deviceCMModel.CountDown }, "DeviceId='" + cashierForm.DeviceId.ToString() + "'", null, cashierForm.db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        cashierForm.ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            cashierForm.ToLog(1, "TechForm: DeliveryReturnButton_Click DeliveryUpCount=" + cashierForm.DeliveryUpCount.ToString());
                            cashierForm.ToLog(1, "TechForm: DeliveryReturnButton_Click DeliveryDownCount=" + cashierForm.DeliveryDownCount.ToString());

                            DeliveryUpCountChanged = false;
                            DeliveryDownCountChanged = false;
                        }
                    }
                    catch (Exception exc)
                    {
                        cashierForm.ToLog(0, exc.ToString());
                    }
                    cashierForm.SettingsLastUpdate = DateTime.Now;
                    TechReturnButtons();
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }
        }
        private void DeliveryPanel_VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (cashierForm != null)
            {
                try
                {
                    Grid g = (Grid)sender;
                    if (g.Visibility == Visibility.Visible)
                    {
                        TopBoxCount.Text = cashierForm.DeliveryUpCount.ToString();
                        BottomBoxCount.Text = cashierForm.DeliveryDownCount.ToString();
                        LeftBoxCount.Text = cashierForm.Delivery1Count.ToString();
                        RightBoxCount.Text = cashierForm.Delivery2Count.ToString();
                        RejectBoxCount.Text = cashierForm.RejectCount.ToString();
                        if (cashierForm.Delivery1Count > 0)
                        {
                            if (CoinHopperLeftClean.Visibility != Visibility.Visible)
                                CoinHopperLeftClean.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            if (CoinHopperLeftClean.Visibility != Visibility.Hidden)
                                CoinHopperLeftClean.Visibility = Visibility.Hidden;
                        }
                        if (cashierForm.Delivery2Count > 0)
                        {
                            if (CoinHopperRightClean.Visibility != Visibility.Visible)
                                CoinHopperRightClean.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            if (CoinHopperRightClean.Visibility != Visibility.Hidden)
                                CoinHopperRightClean.Visibility = Visibility.Hidden;
                        }
                    }
                }
                catch (Exception exc)
                {
                    cashierForm.ToLog(0, exc.ToString());
                }
            }

        }
        private void TopBoxAdd_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Visible;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                {
                    try
                    {
                        TopBoxAdd.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                };
                AddUpperButton.Focus();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void LeftBoxAdd_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Visible;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                {
                    try
                    {
                        LeftBoxAdd.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                };
                AddLeftButton.Focus();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        private void BottomBoxAdd_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Visible;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                {
                    try
                    {
                        BottomBoxAdd.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                };
                AddLowerButton.Focus();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }

        }
        private void RightBoxAdd_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                AddingSummForm = new Calc();
                AddingSummForm.btn_Minus.Visibility = Visibility.Visible;
                AddingSummForm.Len = 6;
                AddingSummForm.Topmost = true;
                AddingSummForm.ShowDialog();
                if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                {
                    try
                    {
                        RightBoxAdd.Text = (Convert.ToInt32(AddingSummForm.Sum)).ToString();
                    }
                    catch
                    {
                    }
                };
                AddRightButton.Focus();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        #endregion Delivery

        public void TechReturnButtons()
        {
            try
            {
                SettingsButtonPanel.Visibility = Visibility.Hidden;
                ShiftPanel.Visibility = Visibility.Hidden;
                DeliveryPanel.Visibility = Visibility.Hidden;
                TestingPanel.Visibility = Visibility.Hidden;
                TechButtonsPanel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        public void SettingsReturnButton()
        {
            if (CashAcceptorSettings.Visibility != Visibility.Hidden)
                CashAcceptorSettings.Visibility = Visibility.Hidden;
            if (CoinAcceptorSettings.Visibility != Visibility.Hidden)
                CoinAcceptorSettings.Visibility = Visibility.Hidden;
            if (CashDispenserSettings.Visibility != Visibility.Hidden)
                CashDispenserSettings.Visibility = Visibility.Hidden;
            if (CoinHopperSettings.Visibility != Visibility.Hidden)
                CoinHopperSettings.Visibility = Visibility.Hidden;
            if (BankModuleSettings.Visibility != Visibility.Hidden)
                BankModuleSettings.Visibility = Visibility.Hidden;
            if (OtherSettings.Visibility != Visibility.Hidden)
                OtherSettings.Visibility = Visibility.Hidden;
            if (OtherDevicesSettings.Visibility != Visibility.Hidden)
                OtherDevicesSettings.Visibility = Visibility.Hidden;
            if (KKMSettings.Visibility != Visibility.Hidden)
                KKMSettings.Visibility = Visibility.Hidden;
            if (SettingsPanel.Visibility != Visibility.Hidden)
                SettingsPanel.Visibility = Visibility.Hidden;
            if (SettingsButtonPanel.Visibility != Visibility.Visible)
                SettingsButtonPanel.Visibility = Visibility.Visible;
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (cashierForm.ShiftOpened)
                {
                    if (Block_windows.Visibility != Visibility.Visible)
                        Block_windows.Visibility = Visibility.Visible;
                    if (ShiftStateRed.Visibility != Visibility.Hidden)
                        ShiftStateRed.Visibility = Visibility.Hidden;
                    if (ShiftStateGreen.Visibility != Visibility.Visible)
                        ShiftStateGreen.Visibility = Visibility.Visible;
                    if (ShiftState.Text != Properties.Resources.ShiftOpened)
                        ShiftState.Text = Properties.Resources.ShiftOpened;
                    if ((string)ShiftModeButton.Content != Properties.Resources.CloseShift)
                        ShiftModeButton.Content = Properties.Resources.CloseShift;
                }
                else
                {
                    if (Block_windows.Visibility != Visibility.Hidden)
                        Block_windows.Visibility = Visibility.Hidden;
                    if (ShiftStateGreen.Visibility != Visibility.Hidden)
                        ShiftStateGreen.Visibility = Visibility.Hidden;
                    if (ShiftStateRed.Visibility != Visibility.Visible)
                        ShiftStateRed.Visibility = Visibility.Visible;
                    if (ShiftState.Text != Properties.Resources.ShiftClosed)
                        ShiftState.Text = Properties.Resources.ShiftClosed;
                    if ((string)ShiftModeButton.Content != Properties.Resources.OpenShift)
                        ShiftModeButton.Content = Properties.Resources.OpenShift;
                }
                ShiftModeButton.IsChecked = cashierForm.ShiftOpened;
                ShiftButton.IsChecked = cashierForm.ShiftOpened;
                if (this.Visibility == Visibility.Visible)
                    ExternalDeviceTimer.Enabled = true;
                else
                    ExternalDeviceTimer.Enabled = false;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        protected void TestingButton_Click(object sender, EventArgs e)
        {
            try
            {
                TestingPanel.Visibility = Visibility.Visible;
                TechButtonsPanel.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }
        CashAcceptorAllowModelT SetCashAcceptorAllowModel(DataRow row)
        {
            CashAcceptorAllowModelT caa = new CashAcceptorAllowModelT();
            try
            {
                caa.Allow = (bool)row.GetBool("Allow");
                caa.BanknoteId = (int)row.GetInt("BanknoteId");
                caa.DeviceId = (Guid)row.GetGuid("DeviceId");
                caa.Id = (Guid)row.GetGuid("Id");
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            return caa;
        }
        CoinAcceptorAllowModelT SetCoinAcceptorAllowModel(DataRow row)
        {
            CoinAcceptorAllowModelT caa = new CoinAcceptorAllowModelT();
            try
            {
                caa.Allow = (bool)row.GetBool("Allow");
                caa.CoinId = (int)row.GetInt("CoinId");
                caa.DeviceId = (Guid)row.GetGuid("DeviceId");
                caa.Id = (Guid)row.GetGuid("Id");
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
            return caa;
        }

        DevicesLanguagesT SetDevicesLanguages(DataRow row)
        {
            try
            {
                DevicesLanguagesT dl = new DevicesLanguagesT();
                dl.Checked = (bool)row.GetBool("Checked");
                dl.Language = row.GetString("Language");
                dl.DeviceId = (Guid)row.GetGuid("DeviceId");
                dl.Id = (Guid)row.GetGuid("Id");
                return dl;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
                return null;
            }
        }
        TariffsT SetTariffs(DataRow row)
        {
            try
            {
                TariffsT t = new TariffsT();
                t.Id = (Guid)row.GetGuid("Id");
                t.Name = row.GetString("Name");
                t.ChangingTypeId = row.GetInt("ChangingTypeId");
                t.TypeId = (int)row.GetInt("TypeId");
                t.Initial = (int)row.GetInt("Initial");
                t.InitialTimeTypeId = row.GetInt("InitialTimeTypeId");
                t.InitialAmount = (int)row.GetInt("InitialAmount");
                t.ProtectedInterval = (int)row.GetInt("ProtectedInterval");
                t.ProtectedIntervalTimeTypeId = row.GetInt("ProtectedIntervalTimeTypeId");
                t.FreeTime = (int)row.GetInt("FreeTime");
                t.FreeTimeTypeId = row.GetInt("FreeTimeTypeId");
                t._IsDeleted = (bool)row.GetBool("_IsDeleted");
                t.Sync = row.GetDateTime("Sync");
                t.IdFC = (byte?)row.GetInt("IdFC");
                return t;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
                return null;
            }
        }

    }
}
