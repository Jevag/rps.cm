using CommonClassLibrary;
using Infralution.Localization.Wpf;
using rps.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using CalculatorTS;
using Rps.ShareBase;
using SqlBaseImport.Model;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private void DoSwitchTerminalStatusEmpty()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                TransactionClear();
                if ((PayTransactionExported == null) || (bool)PayTransactionExported)
                    PayTransactionExported = false;
                CashAcceptorPaid.Clear();
                CoinAcceptorPaid.Clear();
                CleanPaymentErrorInfo();
                CleanCardInfo();
                if (AddingSumm != 0)
                    AddingSumm = 0;
                if (BankQuerySumm != 0)
                    BankQuerySumm = 0;
                if (CardQuery != 0)
                    CardQuery = 0;
                if (Paid != 0)
                    Paid = 0;
                if (BankPaid != 0)
                    BankPaid = 0;
                if (PaidBanknote != 0)
                    PaidBanknote = 0;
                if (PaidCoin != 0)
                    PaidCoin = 0;

                if (CrossPay != crossPay.No)
                    CrossPay = crossPay.No;
                if (CurrentSumm != 0)
                    CurrentSumm = 0;
                if (PaidBanknote != 0)
                    PaidBanknote = 0;
                if (PaidCoin != 0)
                    PaidCoin = 0;
                if (DownDiff != 0)
                    DownDiff = 0;
                if (BankSlip.Count > 0)
                    BankSlip.Clear();
                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                    BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                if (RegularGrid.Visibility != Visibility.Hidden)
                    RegularGrid.Visibility = Visibility.Hidden;
                if (EmulatorEnabled)
                {
                    EmulatorForm.Emulator.PaidBanknote = 0;
                    EmulatorForm.Emulator.PaidCoin = 0;
                }

                //DeliveryReturned = false;
                if (EndPaymentEnabled)
                    EndPaymentEnabled = false;
                if (IsPenalCard)
                    IsPenalCard = false;
                if (CardEjectTime != DateTimeNull)
                    CardEjectTime = DateTimeNull;
                if (DevicePaiments != PaimentMethodT.PaimentsAll)
                    DevicePaiments = PaimentMethodT.PaimentsAll;
                if (EndPaymentEnabled)
                    if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsAll)
                        EmulatorForm.Emulator.Paiments = PaimentMethodT.PaimentsAll;
                if (Paiments != PaimentMethodT.PaimentsAll)
                {
                    ToLog(2, "Cashier: DoSwitchTerminalStatusEmpty: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                    Paiments = PaimentMethodT.PaimentsAll;
                }
                if (TSExists)
                    TSExists = false;

                if (CashierType != 2)
                {
                    DisplayCardReaderEmpty();
                    DisplayStatusChanged = true;
                }

                PaymentOff();
                if (PaymentGrid.Visibility != Visibility.Visible)
                    PaymentGrid.Visibility = Visibility.Visible;

                if (EmulatorEnabled && !ExternalDoorOpen)
                {
                    EmulatorForm.Emulator.Paiments = PaimentMethodT.PaimentsAll;
                    if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                    {
                        if (!EmulatorForm.Emulator.InsertCard.Enabled)
                            EmulatorForm.Emulator.InsertCard.Enabled = true;
                        if (EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = false;
                        if (EmulatorForm.Emulator.ReleaseCard.Enabled)
                            EmulatorForm.Emulator.ReleaseCard.Enabled = false;
                        if (!EmulatorForm.Emulator.BankCardInsert.Enabled)
                            EmulatorForm.Emulator.BankCardInsert.Enabled = true;
                        if (EmulatorForm.Emulator.BankCardSuccess.Enabled)
                            EmulatorForm.Emulator.BankCardSuccess.Enabled = false;
                        if (EmulatorForm.Emulator.BankCardNotSuccess.Enabled)
                            EmulatorForm.Emulator.BankCardNotSuccess.Enabled = false;
                        if (EmulatorForm.Emulator.BankCardTake.Enabled)
                            EmulatorForm.Emulator.BankCardTake.Enabled = false;
                        if (EmulatorForm.Emulator.ClientType.SelectedIndex > 0)
                            EmulatorForm.Emulator.ClientType.SelectedIndex = -1;

                        if (EmulatorForm.Emulator.CardDispenserVirt.Checked)
                        {
                            if (!EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardDispenserEditsGroupBox.Show();
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                        }

                        if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                        {
                            if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                        }
                    }

                    if ((deviceCMModel.CardDispenserExists != null &&
                        !(bool)deviceCMModel.CardDispenserExists))
                    {
                        if
                        ((deviceCMModel.CardReaderExist != null &&
                        !(bool)deviceCMModel.CardReaderExist)
                        )
                        {
                            if (!EmulatorForm.Emulator.CarNumberPanel.Visible)
                                EmulatorForm.Emulator.CarNumberPanel.Show();
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CardReaderVirt.Checked)
                            {
                                if (!EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardReaderEditsGroupBox.Show();
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                            }
                        }
                    }

                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                }
                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null && !ExternalDoorOpen)
                {
                    if (!CardDispenserCardInsertEnabled)
                    {
                        CardDispenser.SetInsert();
                        CardDispenserCardInsertEnabled = true;
                    }
                }
                if ((LanguageTimeOut > 0) && (LanguageChangeTime != null))
                {
                    DateTime t = (DateTime)LanguageChangeTime;
                    t = t.AddSeconds(LanguageTimeOut);
                    DateTime t1 = DateTime.Now;
                    if (t1 > t)
                    {
                        ToLog(2, "Cashier: DoSwitchTerminalStatusEmpty: Change to Default language");
                        if (DefaultLanguage != "CN")
                            CultureManager.UICulture = new CultureInfo(DefaultLanguage);
                        else
                            CultureManager.UICulture = new CultureInfo("zh-Hans");

                        LanguageChangeTime = null;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusEmpty: " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardInserted()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                {
                    PaymentOff();
                    if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                    {
                        if (CardDispenserCardInsertEnabled)
                            CardDispenserCardInsertEnabled = false;
                    }

                    if (PenalCardGrid.Visibility == Visibility.Visible)
                        PenalCardGrid.Visibility = Visibility.Hidden;
                    if (EmulatorEnabled)
                    {
                        if (EmulatorForm.Emulator.InsertCard.Enabled)
                            EmulatorForm.Emulator.InsertCard.Enabled = false;
                        if (EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = false;
                        if (EmulatorForm.Emulator.CardReaderVirt.Checked && EmulatorForm.Emulator.CardReaderWork.Checked)
                        {
                            if (!EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Show();
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardInserted: exception" + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardReadedPlus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                {
                    PaymentOn();
                }
                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }

                if (EmulatorEnabled)
                {
                    if (EmulatorForm.Emulator.InsertCard.Enabled)
                        EmulatorForm.Emulator.InsertCard.Enabled = false;
                    if (EmulatorForm.Emulator.TakeCard.Enabled)
                        EmulatorForm.Emulator.TakeCard.Enabled = false;
                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled && CashAcceptorWork)
                    {
                        if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            if (EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked)
                            {
                                if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Show();
                                if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Show();
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                        }
                    }
                    else
                    {
                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    }
                    if (EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled && CoinAcceptorWork)
                    {
                        if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            if (EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                            {
                                if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Show();
                                if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Show();
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                        }
                    }
                    else
                    {
                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    }
                    if (EmulatorForm.Emulator.BankModuleGroupBox.Enabled)
                    {
                        if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsCash)
                        {
                            if (BankQuerySummRequested)
                            {
                                if (EmulatorForm.Emulator.BankModuleVirt.Checked && EmulatorForm.Emulator.BankModuleWork.Checked)
                                {
                                    if (!EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Show();
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                }

                            }
                            else
                            {
                                if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                        }
                    }
                }
                if (CashierType != 2)
                {
                    if (DisplayStatusChanged)
                    {
                        DisplayCardReaderPlus();
                        DisplayStatusChanged = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardReadedPlus exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardReadedMinus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if ((CashAcceptorWork || CoinAcceptorWork) && (Paiments != PaimentMethodT.PaimentsBankModule))
                {
                    CheckNominals();
                }
                {
                    if (CashierType != 2)
                    {
                        DisplayCardReaderMinus();
                        DisplayStatusChanged = false;
                    }
                    PaymentOn();
                    if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                    {
                        if (CardDispenserCardInsertEnabled)
                            CardDispenserCardInsertEnabled = false;
                    }

                    if (EmulatorEnabled)
                    {
                        if (EmulatorForm.Emulator.InsertCard.Enabled)
                            EmulatorForm.Emulator.InsertCard.Enabled = false;
                        if (EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = false;
                        if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                            EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                        if (EmulatorForm.Emulator.CarNumberPanel.Visible)
                            EmulatorForm.Emulator.CarNumberPanel.Hide();
                        if (EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled && CashAcceptorWork)
                        {
                            if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                            {
                                if (EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked)
                                {
                                    if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Show();
                                    if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Show();
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                        }
                        if (EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled && CoinAcceptorWork)
                        {
                            if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                                {
                                    if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Show();
                                    if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Show();
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                        }
                        if (EmulatorForm.Emulator.BankModuleGroupBox.Enabled)
                        {
                            if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsCash)
                            {
                                if (BankQuerySummRequested)
                                {
                                    if (EmulatorForm.Emulator.BankModuleVirt.Checked && EmulatorForm.Emulator.BankModuleWork.Checked)
                                    {
                                        if (!EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                            EmulatorForm.Emulator.BankModuleEditsGroupBox.Show();
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                            EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardReadedMinus: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusPenalCardPrePaiment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if ((CashAcceptorWork || CoinAcceptorWork) && (Paiments != PaimentMethodT.PaimentsBankModule))
                {
                    CheckNominals();
                }

                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    {
                        if (CardDispenser != null)
                        {
                            if (CardDispenserCardInsertEnabled)
                                CardDispenserCardInsertEnabled = false;
                        }
                        if (CashierType != 2)
                        {
                            DisplayCardReaderPenalCardRequest();
                            DisplayStatusChanged = false;
                        }

                        PaymentOn();

                        if (EmulatorEnabled)
                        {
                            if (EmulatorForm.Emulator.InsertCard.Enabled)
                                EmulatorForm.Emulator.InsertCard.Enabled = false;
                            if (EmulatorForm.Emulator.TakeCard.Enabled)
                                EmulatorForm.Emulator.TakeCard.Enabled = false;
                            if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled && CashAcceptorWork)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                                {
                                    if (EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked)
                                    {
                                        if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Show();
                                        if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Show();
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            }
                            if (EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled && CoinAcceptorWork)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                                {
                                    if (EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                                    {
                                        if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Show();
                                        if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Show();
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                    }

                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            }
                            if (EmulatorForm.Emulator.BankModuleGroupBox.Enabled)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsCash)
                                {
                                    if (BankQuerySummRequested)
                                    {
                                        if (EmulatorForm.Emulator.BankModuleVirt.Checked && EmulatorForm.Emulator.BankModuleWork.Checked)
                                        {
                                            if (!EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Show();
                                        }
                                        else
                                        {
                                            if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                        }

                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                            EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusPenalCardPrePaiment: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusPenalCardPaiment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if ((CashAcceptorWork || CoinAcceptorWork) && (Paiments != PaimentMethodT.PaimentsBankModule))
                {
                    CheckNominals();
                }

                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    {
                        if (CardDispenser != null)
                        {
                            if (CardDispenserCardInsertEnabled)
                                CardDispenserCardInsertEnabled = false;
                        }
                        if (CashierType != 2 && Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            DisplayCardReaderPenalCardRequest();
                            DisplayStatusChanged = false;
                        }

                        PaymentOn();

                        if (EmulatorEnabled)
                        {
                            if (EmulatorForm.Emulator.InsertCard.Enabled)
                                EmulatorForm.Emulator.InsertCard.Enabled = false;
                            if (EmulatorForm.Emulator.TakeCard.Enabled)
                                EmulatorForm.Emulator.TakeCard.Enabled = false;
                            if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled && CashAcceptorWork)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                                {
                                    if (EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked)
                                    {
                                        if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Show();
                                        if (!EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Show();
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            }
                            if (EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled && CoinAcceptorWork)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsBankModule)
                                {
                                    if (EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                                    {
                                        if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Show();
                                        if (!EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Show();
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                    }

                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            }
                            if (EmulatorForm.Emulator.BankModuleGroupBox.Enabled)
                            {
                                if (EmulatorForm.Emulator.Paiments != PaimentMethodT.PaimentsCash)
                                {
                                    if (BankQuerySummRequested)
                                    {
                                        if (EmulatorForm.Emulator.BankModuleVirt.Checked && EmulatorForm.Emulator.BankModuleWork.Checked)
                                        {
                                            if (!EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Show();
                                        }
                                        else
                                        {
                                            if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                        }
                                    }
                                    else
                                    {
                                        if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                            EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusPenalCardPaiment: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardPrePaid()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {

                PaymentOff();

                string s, toLog;

                ResDiff = 0;
                DownDiff = Paid - CardQuery;

                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }

                if (CrossPay == crossPay.Last)
                {
                    deliveryStatus = DeliveryStatusT.NotDelivery;
                }

                if ((Paiments != PaimentMethodT.PaimentsBankModule) && (deliveryStatus != DeliveryStatusT.NotDelivery) && (DownDiff > 0))
                {
                    if (DownDiff > 0)
                    {
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCardPrePaid: DownDiff: " + DownDiff.ToString());
                        s = string.Format(ResourceManager.GetString("ToLogDeliverySum", DefaultCulture), DownDiff.ToString(), sValuteLabel);
                        toLog = s;
                        if (CheckReturnPossible(DownDiff, ref toLog))
                            deliveryStatus = DeliveryStatusT.Delivery;
                        else
                            deliveryStatus = DeliveryStatusT.NotDelivery;
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCardPrePaid: CheckReturnPossible result: " + deliveryStatus.ToString());
                    }
                }

                if (EmulatorEnabled)
                {
                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();

                    if (EmulatorForm.Emulator.InsertCard.Enabled)
                        EmulatorForm.Emulator.InsertCard.Enabled = false;
                    if (Paiments == PaimentMethodT.PaimentsCash)
                        if (!EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = true;
                }

                DisplayCardReaderPaid();

                if (Paiments == PaimentMethodT.PaimentsBankModule)
                {
                    if (EmulatorEnabled && EmulatorForm.Emulator.BankModuleVirt.Checked)
                    {
                        string Receipt = "           �������������\n" +
                                         "               iUP250\n" +
                                         "                RPS\n" +
                                         "��������: 00600523           ��� 187\n" +
                                         "�������: 700000\n" +
                                         "               ������\n" +
                                         "              ��������\n" +
                                         "�����: 2000.00 ���\n" +
                                         "�������� ����� ��� (��):    0.00 ���\n" +
                                         "�����:                   2000.00 ���\n" +
                                         "AID: A0000000031010 VISA\n" +
                                         "�����: VISA\n" +
                                         "          * ***********0005\n" +
                                         "            IVANOV IVAN\n" +
                                         "������: 002517746017\n" +
                                         "��� �������.: 746017\n" +
                                         "��� ������: 000\n" +
                                         "����: 08 / 09 / 16\n" +
                                         "�����: 17:55:24\n" +
                                         "���� ��: 08 / 09 / 16\n" +
                                         "����� ��: 17:56:18\n" +
                                         "������ OFFLINE-PIN\n" +
                                         "====================================\n";

                        BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                    }
                    else
                    {
                        if (BankModule != null && BankModule.IsCancelled)
                        {
                            if (BankModule.CommandStatus == CommandStatusT.Completed)
                            {
                                if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                                {
                                    ToLog(1, "Cashier: DoSwitchTerminalStatusCardPrePaid: BankModule.TechnicalCancelation3");
                                    BankModule.TechnicalCancelation();
                                    if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess && (BankModule.AuthResult != "" || BankModule.AuthReceipt != ""))
                                    {
                                        BankSlip.Clear();
                                        BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                                        string Result = BankModule.AuthResult;
                                        BankModule.AuthResult = "";
                                        string Receipt = BankModule.AuthReceipt;
                                        BankModule.AuthReceipt = "";
                                        BankModule.CommandStatus = CommandStatusT.Undeifned;
                                        Guid Id = Guid.NewGuid();
                                        lock (db_lock)
                                        {
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPrePaid: BankModule.TechnicalCancelation Utils.Running Enter");
                                            //while (Utils.Running)
                                            //    Thread.Sleep(50);
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPrePaid: BankModule.TechnicalCancelation Utils.Running Leave");
                                            try
                                            {
                                                UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }
                                        BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                                    }
                                    BankPaid = 0;
                                }
                            }
                            else return;
                        }
                    }
                }
                terminalStatus = TerminalStatusT.CardPaid;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPrePaid1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
                terminalStatusChangeInternal = true;
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardPrePaid: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardPreCancelled()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                PaymentOff();
                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }
                string s, toLog;
                if (IsPenalCard)
                {
                    deliveryStatus = DeliveryStatusT.NotDelivery;
                    if (Paid == 0)
                    {
                        log.EndContinues(299, DateTime.Now, null);
                        if (terminalStatus != TerminalStatusT.Empty)
                            terminalStatus = TerminalStatusT.Empty;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        terminalStatusChangeInternal = true;
                        return;
                    }
                }
                else
                {
                    ResDiff = 0;
                    DownDiff = Paid - CardQuery;

                    if (CrossPay == crossPay.Last)
                    {
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled: CrossPay: NotDelivery");
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                    }

                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled: deliveryStatus: " + deliveryStatus.ToString());
                    if ((Paiments != PaimentMethodT.PaimentsBankModule) && (deliveryStatus != DeliveryStatusT.NotDelivery) && (DownDiff > 0))
                    {
                        if (DownDiff > 0)
                        {
                            s = string.Format(ResourceManager.GetString("ToLogDeliverySum", DefaultCulture), DownDiff.ToString(), sValuteLabel);
                            toLog = s;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled: CheckReturnPossible: DownDiff " + DownDiff.ToString());
                            if (CheckReturnPossible(DownDiff, ref toLog))
                                deliveryStatus = DeliveryStatusT.Delivery;
                            else
                                deliveryStatus = DeliveryStatusT.NotDelivery;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled: CheckReturnPossible result: " + deliveryStatus.ToString());
                        }
                    }
                }

                if (EmulatorEnabled)
                {
                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();

                    if (EmulatorForm.Emulator.InsertCard.Enabled)
                        EmulatorForm.Emulator.InsertCard.Enabled = false;
                    if (Paiments == PaimentMethodT.PaimentsCash)
                        if (!EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = true;
                }

                ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled: DisplayCardReaderPreCancelled");
                DisplayCardReaderPreCancelled();
                terminalStatus = TerminalStatusT.Cancelled;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPreCancelled2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
                terminalStatusChangeInternal = true;
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardPreCancelled: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardPaid()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) && CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }
                if (Paiments != PaimentMethodT.PaimentsBankModule)
                    Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                else
                    Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentBankingCard;
                PaymentOff();
                string s;
                CardInfo.LastPaymentTime = (int)(DateTime.Now - datetime0).TotalSeconds;
                transactions.TimeOplat = DateTime.Now;
                string toLog;
                ResDiff = 0;
                switch (FreeTimeTypeId)
                {
                    case 2:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromHours(FreeTime);
                        break;
                    case 3:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromDays(FreeTime);
                        break;
                    default:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromMinutes(FreeTime);
                        break;
                }
                if ((DateExitEstimated == null) || (DateExitEstimated1 > DateExitEstimated))
                    DateExitEstimated = DateExitEstimated1;

                DownDiff = Paid - CardQuery;

                if (IsPenalCard)
                    CardInfo.ClientTypidFC = 0;

                if (CrossPay == crossPay.Last)
                {
                    deliveryStatus = DeliveryStatusT.NotDelivery;
                }

                CardInfo.SumOnCard = (int)(CardSum + Paid);
                if (ToAbonement != null)
                {
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid:  ToAbonement " + ToAbonement.ToString());
                    CardInfo = CardToAbonement(CardInfo, ToAbonement);
                    if (ToAbonement != null)
                    {
                        DownDiff = 0;
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                    }
                    transactions.SumOnCardAfter = CardInfo.SumOnCard;
                    transactions.TariffScheduleId = ToAbonement;
                    transactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                    Transaction.PaymentTransactionType = PaymentTransactionTypeT.ToAbonement;

                    s = string.Format(ResourceManager.GetString("ToLogTariffIDNew", DefaultCulture), (CardInfo.TPidFC.ToString()));
                    ToLog(1, s);
                    s = string.Format(ResourceManager.GetString("ToLogTariffEnd", DefaultCulture), (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString());
                    ToLog(1, s);

                    ToAbonement = null;
                    CardInfo.ClientTypidFC = 1;
                }

                ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: deliveryStatus: " + deliveryStatus.ToString());
                if ((Paiments != PaimentMethodT.PaimentsBankModule) && (deliveryStatus != DeliveryStatusT.NotDelivery) && (DownDiff > 0))
                { // �� ������
                    s = string.Format(ResourceManager.GetString("ToLogDeliverySum", DefaultCulture), DownDiff.ToString(), sValuteLabel);
                    toLog = s;
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: CheckReturnPossible: DownDiff " + DownDiff.ToString());
                    if (CheckReturnPossible(DownDiff, ref toLog))
                        deliveryStatus = DeliveryStatusT.Delivery;
                    else
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: CheckReturnPossible result: " + deliveryStatus.ToString());

                    if (deliveryStatus == DeliveryStatusT.Delivery)
                    { // ������ ����� ��������
                        PaymentErrorInfo.Delivery = true;
                        CardInfo.SumOnCard = CardInfo.SumOnCard - (int)DownDiff;
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: GiveDelivery: DownDiff " + DownDiff.ToString());
                        IsCancelled = false;
                        GiveDelivery(DownDiff, toLog);
                        if (Paid > 0)
                        {
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: GiveCheque " + Paid.ToString() + " " + CardQuery.ToString() + " " + Paiments.ToString());
                            GiveCheque(0, Paid, CardQuery, Paiments, 0, null, BankSlip);
                        }
                    }
                    else
                    {
                        // ������ ����� ����������
                        DownDiff = 0;
                        ResDiff = 0;

                        PaymentErrorInfo.Delivery = false;
                        PaymentErrorInfo.Paid = Paid;
                        PaymentErrorInfo.CardQuery = Paid;

                        s = string.Format(ResourceManager.GetString("ToLogEjectingCheque", DefaultCulture), Paid.ToString("F0"), sValuteLabel);
                        ToLog(1, s);
                        if (Paid > 0)
                        {
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: Delivery imposible GiveCheque " + Paid.ToString() + " " + Paid.ToString() + " " + Paiments.ToString());
                            GiveCheque(0, Paid, Paid, Paiments, 0, null, BankSlip);
                        }


                        terminalStatusChangeInternal = false;
                        TerminalStatusLabel += Properties.Resources.GetParkingCard;

                    }
                }
                else
                { // ��� �����
                    DownDiff = 0;
                    ResDiff = 0;
                    PaymentErrorInfo.Paid = Paid;
                    PaymentErrorInfo.CardQuery = Paid;
                    deliveryStatus = DeliveryStatusT.NotDelivery;

                    TerminalStatusLabel += Properties.Resources.GetParkingCard;
                    s = string.Format(ResourceManager.GetString("ToLogEjectingCheque", DefaultCulture), Paid.ToString("F0"), sValuteLabel);
                    ToLog(1, s);
                    if (Paid > 0)
                    {
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: NotDelivery GiveCheque " + Paid.ToString() + " " + Paid.ToString() + " " + Paiments.ToString());
                        GiveCheque(0, Paid, Paid, Paiments, 0, null, BankSlip);
                    }
                }

                ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: DisplayCardReaderPaid");
                DisplayCardReaderPaid();
                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                    ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));


                s = string.Format(ResourceManager.GetString("ToLogSumWriting", DefaultCulture), CardInfo.SumOnCard.ToString(), sValuteLabel); //ResDiff = 0
                ToLog(1, s);
                DateTime dt = DateTime.Now;
                ToLog(1, ResourceManager.GetString("ToLogTimeWriting", DefaultCulture) + " " + dt.ToString(DefaultCulture));
                ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: WriteCard ");
                CardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // ����� ���������� ������
                LogWriteCard();
                if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                    WriteCard();
                else
                    SendCardTransaction();

                ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: WriteCard - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                {
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                        WriteCard();

                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid: WriteCard double - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                    if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                    {
                        terminalStatus = TerminalStatusT.CardWriteError;
                        PaymentErrorInfo.CardWriteError = true;
                        PaymentErrorInfo.Error = true;
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                        transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.UnclosedPayment;
                        CurrentTransactionType = TransactionTypeT.Payment_aprowed_with_error;
                    }
                    else
                    {
                        if (Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                            transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentCash;
                        }
                        else
                        {
                            transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentBankingCard;
                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentBankingCard;
                        }
                    }
                }
                else
                {
                    if (Paiments != PaimentMethodT.PaimentsBankModule)
                    {
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                        transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentCash;
                    }
                    else
                    {
                        transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentBankingCard;
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentBankingCard;
                    }
                }

                terminalStatus = TerminalStatusT.LongExitOperation;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardPaid1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;

                if (EmulatorEnabled)
                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                terminalStatusChangeInternal = false;

            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardPaid: exception " + exc.ToString());
            }

            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardEjected()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                PaymentOff();
                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    if (CardDispenser != null)
                    {
                        if (CardDispenserCardInsertEnabled)
                            CardDispenserCardInsertEnabled = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCardEjected: exception" + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardFree()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if ((CardReader == null) || !CardReader.CardExists)
            {
                terminalStatus = TerminalStatusT.Empty;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardFree1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;

                if (EmulatorEnabled)
                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                terminalStatusChangeInternal = false;
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusCardError()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (terminalStatus == TerminalStatusT.PaymentError)
                    return;
                PaymentOff();
                if (CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }
                switch (terminalStatus)
                {
                    case TerminalStatusT.CardJam:
                        if (TerminalStatusLabel != Properties.Resources.CardJam)
                        {
                            TerminalStatusLabel = Properties.Resources.CardJam;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardError: TerminalStatusLabel: " + TerminalStatusLabel);
                        }
                        break;
                    case TerminalStatusT.CardReadedError:
                        if (TerminalStatusLabel != Properties.Resources.CardReadedError)
                        {
                            TerminalStatusLabel = Properties.Resources.CardReadedError;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardError: TerminalStatusLabel: " + TerminalStatusLabel);
                        }
                        break;
                    case TerminalStatusT.CardWriteError:
                        if (TerminalStatusLabel != Properties.Resources.CardWriteError)
                        {
                            TerminalStatusLabel = Properties.Resources.CardWriteError;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardError: TerminalStatusLabel: " + TerminalStatusLabel);
                        }
                        break;
                    case TerminalStatusT.CardInStopList:
                        if (TerminalStatusLabel != Properties.Resources.CardInStopList)
                        {
                            TerminalStatusLabel = Properties.Resources.CardInStopList;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardError: TerminalStatusLabel: " + TerminalStatusLabel);
                        }
                        break;
                    case TerminalStatusT.CardWrongCasse:
                        if (TerminalStatusLabel != Properties.Resources.CardWrongCasse)
                        {
                            TerminalStatusLabel = Properties.Resources.CardWrongCasse;
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCardError: TerminalStatusLabel: " + TerminalStatusLabel);
                        }
                        break;
                    case TerminalStatusT.CardError:
                        break;
                }
                DisplayCardReaderError();
                if (terminalStatusOld != terminalStatus)
                {
                    ToLog(0, "Cashier: DoSwitchTerminalStatusCardError: " + ResourceManager.GetString("ToLogCardReadError", DefaultCulture) + "3: " + CardReader.SoftStatus.ToString());
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardError1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                }

                if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                {
                    if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                    {
                        ToLog(1, "Cashier: DoSwitchTerminalStatusCardError: SendCardToBezelAndHold ");
                        CardDispenser.SendCardToBezelAndHold();
                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                            Thread.Sleep(100);
                        //CardDispensed = true;
                        if (CardTimeOutToBasket > 0)
                            CardEjectTime = DateTime.Now;
                    }
                }
                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    if ((CardDispenser != null) && CardDispenser.IsOpen)
                    {
                        if (!CardDispenser.CardDispenserStatus.CardPreSendPos &&
                            !CardDispenser.CardDispenserStatus.CardDespensePos &&
                            !CardDispenser.CardDispenserStatus.CardAccepting &&
                            !CardDispenser.CardDispenserStatus.CardDispensing)
                        {
                            if (terminalStatus != TerminalStatusT.Empty)
                                terminalStatus = TerminalStatusT.Empty;
                        }
                        else
                        {
                            if (terminalStatus != TerminalStatusT.CardEjected)
                                terminalStatus = TerminalStatusT.CardEjected;
                        }
                    }
                    else
                    {
                        if (terminalStatus != TerminalStatusT.CardEjected)
                            terminalStatus = TerminalStatusT.CardEjected;
                    }
                }
                else
                {
                    if (terminalStatus != TerminalStatusT.CardFree)
                        terminalStatus = TerminalStatusT.CardFree;
                }
                if (terminalStatusOld != terminalStatus)
                {
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCardError2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                }

                if (EmulatorEnabled)
                {
                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                    {
                        if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                            EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    }
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                    if (terminalStatus != TerminalStatusT.CardJam)
                        if (!EmulatorForm.Emulator.TakeCard.Enabled)
                            EmulatorForm.Emulator.TakeCard.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatusPaymentError()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                PaymentOff();
                if (CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }
                if (EmulatorEnabled)
                {
                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                    {
                        if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                            EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    }
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                }
                if (PaymentErrorLevel != AlarmLevelT.Red)
                {
                    PaymentErrorLevel = AlarmLevelT.Red;
                    AlarmT Alarm = new AlarmT();
                    Alarm.AlarmLevel = AlarmLevelT.Red;
                    Alarm.AlarmSource = AlarmSourceT.PaymentError;
                    if (PaymentErrorInfo.ChequeError)
                        Alarm.ErrorCode = AlarmCode.KKMPaymentError;
                    else if (PaymentErrorInfo.CardWriteError)
                        Alarm.ErrorCode = AlarmCode.CardReaderPaymentError;
                    else if (PaymentErrorInfo.DeliveryError)
                        Alarm.ErrorCode = AlarmCode.CashDispenserPaymentError;
                    else if (PaymentErrorInfo.CashAcceptorError)
                        Alarm.ErrorCode = AlarmCode.CashAcceptorPaymentError;
                    else if (PaymentErrorInfo.CardDispenserError)
                        Alarm.ErrorCode = AlarmCode.CardDispenserPaymentError;

                    SendAlarm(Alarm);
                }

                DisplayPaymentError();
                terminalStatus = TerminalStatusT.PaymentError;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: DoSwitchTerminalStatusPaymentError3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        private void DoSwitchTerminalStatusExitWithoutPayment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                //ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: PaymentOff");
                PaymentOff();
                if (CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }
                DisplayCardReaderExitWithoutPayment();

                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    if (Paiments != PaimentMethodT.PaimentsAll)
                    {
                        ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                    }

                    Paiments = PaimentMethodT.PaimentsAll;

                    if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                    {
                        {
                            DateTime dt = DateTime.Now;
                            ToLog(1, ResourceManager.GetString("ToLogTimeWriting", DefaultCulture) + " " + dt.ToString(DefaultCulture));
                            ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: WriteCard ");
                            CardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // ����� ���������� ������
                            LogWriteCard();
                            if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                                WriteCard();
                            else
                                SendCardTransaction();

                            ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: WriteCard - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                            if (terminalStatus == TerminalStatusT.CardWriteError)
                            {
                                if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                                    WriteCard();

                                ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: WriteCard double - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                                if (terminalStatus == TerminalStatusT.CardWriteError)
                                {
                                    PaymentErrorInfo.CardWriteError = true;
                                    PaymentErrorInfo.Error = true;
                                    Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                                    transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.UnclosedPayment;
                                    CurrentTransactionType = TransactionTypeT.Payment_aprowed_with_error;
                                }
                                else
                                {
                                    Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                                    transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentCash;
                                }
                            }
                            else
                            {
                                Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                                transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.SuccessPaymentCash;
                            }

                            ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: CardDispenser.SendCardToBezelAndHold");
                            CardDispenser.SendCardToBezelAndHold();
                            while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                Thread.Sleep(100);
                            //CardDispensed = true;
                            if (CardTimeOutToBasket > 0)
                                CardEjectTime = DateTime.Now;
                        }
                    }

                    if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                    {
                        if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                        !CardDispenser.CardDispenserStatus.CardAccepting &&
                        !CardDispenser.CardDispenserStatus.CardDispensing)
                        {
                            if (terminalStatus != TerminalStatusT.Empty)
                                terminalStatus = TerminalStatusT.Empty;
                        }
                        else
                        {
                            if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                                ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));

                            terminalStatus = TerminalStatusT.CardEjected;
                        }
                    }
                    else
                    {
                        if (terminalStatus != TerminalStatusT.CardFree)
                            terminalStatus = TerminalStatusT.CardFree;
                    }
                    if (terminalStatusOld != terminalStatus)
                        ToLog(2, "Cashier: DoSwitchTerminalStatusExitWithoutPayment1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                    if (EmulatorEnabled)
                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    terminalStatusChangeInternal = false;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusExitWithoutPayment: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        private void DoSwitchTerminalStatusCancelled()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                PaymentOff();
                if (CardDispenser != null)
                {
                    if (CardDispenserCardInsertEnabled)
                        CardDispenserCardInsertEnabled = false;
                }

                DateExitEstimated = null;
                string s;
                if (Paid == 0)
                #region Paid == 0
                {
                    if (IsPenalCard) // ������ - �������� ����� � ������ 0
                    {
                        log.EndContinues(299, DateTime.Now, null);
                        ToLog(1, ResourceManager.GetString("ToLogCancelOperation", DefaultCulture));
                        if (terminalStatus != TerminalStatusT.Empty)
                            terminalStatus = TerminalStatusT.Empty;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;

                        ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: IsPenalCard: Paid = 0 DoSwitchTerminalStatus");
                        DoSwitchTerminalStatus();
                        return;
                    }
                    else // ������ - ������� ����� � ������ 0
                    {
                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                        {
                            ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));
                        }

                        if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                        {
                            //if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                            {
                                ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: NotPenalCard Paid = 0 CardDispenser.SendCardToBezelAndHold");
                                CardDispenser.SendCardToBezelAndHold();
                                while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                    Thread.Sleep(100);
                                //CardDispensed = true;
                                if (CardTimeOutToBasket > 0)
                                    CardEjectTime = DateTime.Now;
                            }
                        }

                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentBeforeIncome;
                        CurrentTransactionType = TransactionTypeT.Payment_is_canceled_by_the_client;
                        SendPayTransaction(CurrentTransactionType);

                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                        {
                            if ((CardDispenser != null) && CardDispenser.IsOpen)
                            {
                                if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                                !CardDispenser.CardDispenserStatus.CardAccepting &&
                                !CardDispenser.CardDispenserStatus.CardDispensing)
                                {
                                    if (terminalStatus != TerminalStatusT.Empty)
                                        terminalStatus = TerminalStatusT.Empty;
                                }
                                else
                                {
                                    terminalStatus = TerminalStatusT.CardEjected;
                                }
                            }
                            else
                            {
                                if (terminalStatus != TerminalStatusT.Empty)
                                    terminalStatus = TerminalStatusT.Empty;
                            }
                        }
                        else
                        {
                            if (terminalStatus != TerminalStatusT.CardFree)
                                terminalStatus = TerminalStatusT.CardFree;
                        }
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;

                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                    }
                }
                #endregion Paid == 0
                else
                #region Paid != 0
                {
                    if (IsPenalCard) // ������ - �������� ����� � ������ �� 0 (����� ������������� �� �����)
                    #region PenalCard && Paid != 0
                    {
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                        //CardInfo.ClientTypidFC = 0;
                        //CardQuery = 0;


                        s = string.Format(ResourceManager.GetString("ToLogEjectingCheque", DefaultCulture), Paid.ToString("F0"), sValuteLabel);
                        ToLog(1, s);
                        if (Paid > 0)
                        {
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: IsPenalCard: Paid > 0 Not Delivery  GiveCheque " + Paid.ToString() + " " + Paid.ToString() + " " + Paiments.ToString());
                            GiveCheque(0, Paid, Paid, Paiments, 0, new List<string> { ResourceManager.GetString("ToLogCardWriteError", DefaultCulture) }, BankSlip);
                        }

                        CardInfo.SumOnCard = /*CardInfo.SumOnCard +*/ (int)Paid;

                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentAfterIncome;
                    }
                    #endregion PenalCard && Paid != 0
                    else // ������ - ������� ����� � ������ �� 0 (����� ������������ ��� �����������)
                    #region RegularCard && Paid != 0
                    {
                        string toLog = string.Format(ResourceManager.GetString("ToLogSummEjected", DefaultCulture), Paid.ToString("F0"), sValuteLabel);
                        DownDiff = Paid;
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: Paid > 0 CheckReturnPossible ");
                        if (
                            (bool)TechForm.CancelRefund.IsChecked &&
                            CheckReturnPossible(DownDiff, ref toLog)
                           )
                        {
                            #region ReturnPossible && RegularCard && Paid != 0
                            ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: Paid > 0 GiveReturn (GiveDelivery) " + DownDiff.ToString());
                            IsCancelled = true;
                            GiveDelivery(DownDiff, toLog);
                        }
                        #endregion ReturnPossible && RegularCard && Paid != 0
                        else
                        #region ReturnImpossible && RegularCard && Paid != 0
                        {// ������ - ������� ����� � ������ �� 0 � ������� ����� ����������
                            PaidBanknote = 0;
                            PaidCoin = 0;
                            DownDiff = 0;
                            if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                            {
                                if (!(bool)TechForm.CancelRefund.IsChecked)
                                    ToLog(1, ResourceManager.GetString("ToLogDeliveryDisabled", DefaultCulture));
                                else
                                    ToLog(1, ResourceManager.GetString("ToLogDeliveryUnavailable", DefaultCulture));
                                CardInfo.SumOnCard = (int)(CardSum + Paid);

                                s = string.Format(ResourceManager.GetString("ToLogEjectingCheque", DefaultCulture), Paid.ToString("F0"), sValuteLabel);
                                ToLog(1, s);
                                ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: Paid > 0 Delivery impossible GiveCheque " + Paid.ToString() + " " + Paid.ToString() + " " + Paiments.ToString());
                                GiveCheque(0, Paid, Paid, Paiments, 0, null, BankSlip);
                                Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentAfterIncome;
                            }
                        }
                        #endregion ReturnImpossible && RegularCard && Paid != 0
                    }
                    #endregion RegularCard && Paid != 0

                    terminalStatus = TerminalStatusT.LongExitOperation;
                    if (terminalStatusOld != terminalStatus)
                        ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                    if (EmulatorEnabled)
                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    terminalStatusChangeInternal = false;
                }
                #endregion Paid != 0

                if (Paid != 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogSumWriting", DefaultCulture), CardInfo.SumOnCard.ToString(), sValuteLabel); //ResDiff = 0
                    ToLog(1, s);
                    DateTime dt = DateTime.Now;
                    ToLog(1, ResourceManager.GetString("ToLogTimeWriting", DefaultCulture) + " " + dt.ToString(DefaultCulture));
                    ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: WriteCard ");
                    CardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // ����� ���������� ������
                    LogWriteCard();
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                        WriteCard();
                    else
                        SendCardTransaction();

                    ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                    if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                    {
                        if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                            WriteCard();

                        ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: WriteCard double - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                        if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                        {
                            PaymentErrorInfo.CardWriteError = true;
                            PaymentErrorInfo.Error = true;
                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                        }
                        else
                        {
                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentAfterIncome;
                            transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.CancellPaymentAfterIncome;
                        }
                    }
                    else
                    {
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentAfterIncome;
                        transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.CancellPaymentAfterIncome;
                    }
                }

                if (EmulatorEnabled)
                {
                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                    if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                    if (!EmulatorForm.Emulator.TakeCard.Enabled)
                        EmulatorForm.Emulator.TakeCard.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: DoSwitchTerminalStatusCancelled:  Exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void DoSwitchTerminalStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (terminalStatusOld != terminalStatus)
                ToLog(2, "Cashier: DoSwitchTerminalStatus: terminalStatus=" + terminalStatus.ToString());
            terminalStatusOld = terminalStatus;
            try
            {
                switch (terminalStatus)
                {
                    case TerminalStatusT.Undeifned:
                        PaymentOff();
                        break;
                    case TerminalStatusT.Empty:
                        DoSwitchTerminalStatusEmpty();
                        break;
                    case TerminalStatusT.CardInserted:
                        DoSwitchTerminalStatusCardInserted();
                        break;
                    //case terminalStatusT.CardReaded:
                    case TerminalStatusT.CardReadedPlus:
                        DoSwitchTerminalStatusCardReadedPlus();
                        break;
                    case TerminalStatusT.ExitWithoutPayment:
                        DoSwitchTerminalStatusExitWithoutPayment();
                        break;
                    case TerminalStatusT.CardReadedMinus:
                        DoSwitchTerminalStatusCardReadedMinus();
                        break;
                    case TerminalStatusT.PenalCardRequest:
                        if (CashierType != 2)
                        {
                            DisplayCardReaderPenalCardRequest();
                            DisplayStatusChanged = false;
                        }
                        break;
                    case TerminalStatusT.PenalCardPrePaiment:
                        DoSwitchTerminalStatusPenalCardPrePaiment();
                        break;
                    case TerminalStatusT.PenalCardPaiment:
                        DoSwitchTerminalStatusPenalCardPaiment();
                        break;
                    case TerminalStatusT.PenalCardPaid:
                    case TerminalStatusT.CardPrePaid:
                        DoSwitchTerminalStatusCardPrePaid();
                        break;
                    case TerminalStatusT.CardPaid:
                        DoSwitchTerminalStatusCardPaid();
                        break;
                    case TerminalStatusT.CardEjected:
                        if (terminalStatusOld != TerminalStatusT.Empty)
                            DoSwitchTerminalStatusCardEjected();
                        break;
                    case TerminalStatusT.PreCancelled:
                        DoSwitchTerminalStatusCardPreCancelled();
                        break;
                    case TerminalStatusT.Cancelled:
                        DoSwitchTerminalStatusCancelled();
                        break;
                    case TerminalStatusT.CardJam:
                    case TerminalStatusT.CardReadedError:
                    case TerminalStatusT.CardInStopList:
                    case TerminalStatusT.CardWrongCasse:
                    case TerminalStatusT.CardError:
                        DoSwitchTerminalStatusCardError();
                        break;
                    case TerminalStatusT.LongExitOperation:
                        DoCheckGiveStatus();
                        break;
                    case TerminalStatusT.CardFree:
                        DoSwitchTerminalStatusCardFree();
                        break;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
    }
}