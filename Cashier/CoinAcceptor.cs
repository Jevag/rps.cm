﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Globalization;
using System.Windows.Media;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime CoinAcceptorAlarmTime;
        void HardCoinAcceptor()
        {
            try
            {
                if (!CoinAcceptor.IsOpen)
                {
                    if ((CoinAcceptorStatusCheckDT == null) || ((DateTime)CoinAcceptorStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                    {
                        if (CoinAcceptorPIN.Length != 12)
                        {
                            ErrorsText += ResourceManager.GetString("CoinAcceptorCommunicationError", DefaultCulture) + " PinCode incorrect length." + Environment.NewLine;
                            if (CoinAcceptorLevel != AlarmLevelT.Brown)
                            {
                                if (CoinAcceptorAlarmTime == DateTime.MinValue)
                                {
                                    CoinAcceptorAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CoinAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CoinAcceptorLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CoinAcceptor;
                                        Alarm.ErrorCode = AlarmCode.CoinAcceptorNotConnect;
                                        SendAlarm(Alarm);
                                        CoinAcceptorAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                            return;
                        }
                        byte[] pin = new byte[6];
                        {
                            pin[0] = byte.Parse(CoinAcceptorPIN.Substring(0, 2), NumberStyles.HexNumber);
                            pin[1] = byte.Parse(CoinAcceptorPIN.Substring(2, 2), NumberStyles.HexNumber);
                            pin[2] = byte.Parse(CoinAcceptorPIN.Substring(4, 2), NumberStyles.HexNumber);
                            pin[3] = byte.Parse(CoinAcceptorPIN.Substring(6, 2), NumberStyles.HexNumber);
                            pin[4] = byte.Parse(CoinAcceptorPIN.Substring(8, 2), NumberStyles.HexNumber);
                            pin[5] = byte.Parse(CoinAcceptorPIN.Substring(10, 2), NumberStyles.HexNumber);
                        }
                        ToLog(2, "Cashier: HardCoinAcceptor: CoinAcceptor.Open");
                        if (CoinAcceptor.Open(pin, Convert.ToInt16(CoinAcceptorPort.Substring(3)),
                            Convert.ToByte(deviceCMModel.CoinAcceptorAddress)) != 0)
                        {
                            ToLog(2, "Cashier: HardCoinAcceptor: CoinAcceptor.Close");
                            CoinAcceptor.Close();
                        }
                        CoinAcceptorStatusCheckDT = DateTime.Now;
                    }
                }
                if (!CoinAcceptor.IsOpen)
                {
                    if (TechForm.CoinAcceptorPortLabel.Foreground != Brushes.Red)
                        TechForm.CoinAcceptorPortLabel.Foreground = Brushes.Red;
                    if (CoinAcceptorWork)
                        CoinAcceptorWorkChanged = true;
                    CoinAcceptorWork = false;
                    if (CoinAcceptorLevel != AlarmLevelT.Brown)
                    {
                        if (CoinAcceptorAlarmTime == DateTime.MinValue)
                        {
                            CoinAcceptorAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CoinAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                CoinAcceptorLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.CoinAcceptor;
                                Alarm.ErrorCode = AlarmCode.CoinAcceptorNotConnect;
                                SendAlarm(Alarm);
                                CoinAcceptorAlarmTime = DateTime.MinValue;
                            }
                        }
                    }
                }
                else
                {
                    if (TechForm.CoinAcceptorPortLabel.Foreground != Brushes.Green)
                        TechForm.CoinAcceptorPortLabel.Foreground = Brushes.Green;
                    if (!CoinAcceptorWork)
                    {
                        CoinAcceptorWorkChanged = true;
                        CoinAcceptorWork = true;
                    }
                    if (CoinAcceptorAlarmTime != DateTime.MinValue)
                        CoinAcceptorAlarmTime = DateTime.MinValue;
                    if (CoinAcceptorLevel != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.CoinAcceptor;
                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                        Alarm.Status = ResourceManager.GetString("CoinAcceptorWork", DefaultCulture);
                        SendAlarm(Alarm);
                        CoinAcceptorLevel = AlarmLevelT.Green;
                    }
                    if (!CoinAcceptorWork)
                        CoinAcceptorWorkChanged = true;
                    CoinAcceptorWork = true;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void GetCoinAcceptorStatus()
        {
            try
            {
                if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
                {
                    if (CoinAcceptor == null)
                    {
                        CoinAcceptor = new CoinAcceptor(log);
                    }
                    if (EmulatorEnabled && EmulatorForm.Emulator.CoinAcceptorVirt.Checked)
                    {
                        if (CoinAcceptor.IsOpen)
                            CoinAcceptor.Close();
                        EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled = true;
                        if (EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                        {
                            if (!CoinAcceptorWork)
                            {
                                CoinAcceptorWorkChanged = true;
                                CoinAcceptorWork = true;
                            }
                            if (CoinAcceptorAlarmTime != DateTime.MinValue)
                                CoinAcceptorAlarmTime = DateTime.MinValue;
                            if (CoinAcceptorLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.CoinAcceptor;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("CoinAcceptorWork", DefaultCulture);
                                SendAlarm(Alarm);
                                CoinAcceptorLevel = AlarmLevelT.Green;
                                CoinAcceptorAlarmTime = DateTime.MinValue;
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CoinAcceptorNeeded.Checked)
                            {
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("TechCoinAcceptorCheckReq", DefaultCulture) + Environment.NewLine;
                                if (CoinAcceptorWork)
                                    CoinAcceptorWorkChanged = true;
                                CoinAcceptorWork = false;
                            }
                            else
                            {
                                if (CoinAcceptorWork)
                                    CoinAcceptorWorkChanged = true;
                                CoinAcceptorWork = false;
                            }
                            if (CoinAcceptorLevel != AlarmLevelT.Brown)
                            {
                                if (CoinAcceptorAlarmTime == DateTime.MinValue)
                                {
                                    CoinAcceptorAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CoinAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        CoinAcceptorLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CoinAcceptor;
                                        Alarm.ErrorCode = AlarmCode.CoinAcceptorNotConnect;
                                        SendAlarm(Alarm);
                                        CoinAcceptorAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        HardCoinAcceptor();
                    }
                    if ((terminalStatus != TerminalStatusT.CardReaded) &&
                        (terminalStatus != TerminalStatusT.CardReadedPlus) &&
                        (terminalStatus != TerminalStatusT.CardReadedMinus) &&
                        (terminalStatus != TerminalStatusT.CardPaid) &&
                        (terminalStatus != TerminalStatusT.CardPrePaid)
                    )
                    {
                        if (CoinAcceptorCurrent > (CoinAcceptorMaxCount - CoinAcceptorMax))
                        {
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            TerminalStatusChanged = true;
                            if (TerminalWork_old != TerminalWork)
                                ToLog(0, ResourceManager.GetString("TechCoinAcceptorIncasseReq", DefaultCulture));
                        }
                    }

                }
                else
                {
                    if (CoinAcceptorWork)
                        CoinAcceptorWorkChanged = true;
                    CoinAcceptorWork = false;
                    if (EmulatorEnabled)
                    {
                        EmulatorForm.Emulator.CoinAcceptorGroupBox.Enabled = false;
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                        EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}