﻿using CommonClassLibrary;
using EventWeb;
using System;
using System.Windows;
using System.Linq;
using System.Data.SqlClient;
using Rps.ShareBase;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using SqlBaseImport.Model;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public void SendPayTransaction(TransactionTypeT TransactionType)
        {
            try
            {
                if (PayTransactionExported == false)
                {
                    if (DetailLog)
                        ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
                    //TransactionClear();
                    Transaction.TransactionTime = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                    Transaction.TransactionNumber = CurrentTransactionNumber++;
                    deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                    Transaction.TransactionType = TransactionType;
                    if (Paiments != PaimentMethodT.PaimentsBankModule)
                    {
                        for (int c = 0; c < CashAcceptorPaid.Count; c++)
                        {
                            for (int i = 0; i < Transaction.BanknotesAccepted.GetLength(0); i++)
                                if (CashAcceptorPaid[c] == Transaction.BanknotesAccepted[i].Nominal)
                                    Transaction.BanknotesAccepted[i].Count = Transaction.BanknotesAccepted[i].Count + 1;
                        }
                        for (int c = 0; c < CoinAcceptorPaid.Count; c++)
                        {
                            for (int i = 0; i < Transaction.CoinsAccepted.GetLength(0); i++)
                                if (CoinAcceptorPaid[c] == Transaction.CoinsAccepted[i].Nominal)
                                    Transaction.CoinsAccepted[i].Count = Transaction.CoinsAccepted[i].Count + 1;
                        }
                        Transaction.WayOfPayment = WayOfPaymentT.Cash;
                    }
                    else
                        Transaction.WayOfPayment = WayOfPaymentT.Banking_card;
                    Transaction.ToPay = CardQuery - CardSum;
                    if (Transaction.ToPay < 0)
                        Transaction.ToPay = 0;
                    Transaction.Paid = Paid;
                    if (ShiftWork)
                    {
                        if (LastShiftOpenTime != null)
                            Transaction.LastShiftOpenTime = ((DateTime)LastShiftOpenTime).ToString("yyyy\\/MM\\/dd HH:mm:ss");
                    }
                    if (DownDiff < 0)
                        DownDiff = 0;
                    Transaction.Issued = DownDiff;
                    Transaction.PaymentTime = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                    ;
                    if (DownDiff > 0)
                        Transaction.BillIsued = true;
                    else
                        Transaction.BillIsued = false;
                    switch (deviceCMModel.ServerExchangeProtocolId)
                    {
                        case 1:
                            {

                                transactions.Time = DateTime.Now;


                                if (Paiments == PaimentMethodT.PaimentsBankModule)
                                {
                                    transactions.BankCardAccepted = Transaction.Paid;
                                }
                                else
                                {
                                    int sum = 0;
                                    for (int i = 0; i < Transaction.BanknotesAccepted.GetLength(0); i++)
                                        sum += Transaction.BanknotesAccepted[i].Count;

                                    transactions.BanknotesAccepted = sum; // Принято банкнот
                                    transactions.CachDispenerDown = Transaction.DeliveredFromLower; // Выдано из нижнего ящика
                                    transactions.CachDispenerUp = Transaction.DeliveredFromUpper; // Выдано из верхнего ящика

                                    sum = 0;
                                    for (int i = 0; i < Transaction.CoinsAccepted.GetLength(0); i++)
                                        sum += Transaction.CoinsAccepted[i].Count;
                                    transactions.CoinsAccepted = sum; // Принято монет
                                    transactions.Hopper1 = Transaction.DeliveredFrom1Hopper; // Выдано из первого хоппера
                                    transactions.Hopper2 = Transaction.DeliveredFrom2Hopper; // Выдано из второго хоппера
                                }
                                transactions.SumAccepted = Transaction.Paid; // Принятая сумма
                                transactions.ChangeIssued = Transaction.Issued; // Сумма выданнной сдачи
                                transactions.Paid = transactions.SumAccepted - transactions.ChangeIssued; // Сумма внесенная на карту
                                transactions.SumOnCardAfter = CardInfo.SumOnCard;
                                transactions.Balance = transactions.SumOnCardAfter - ((transactions.Amount == null) ? 0 : transactions.Amount);
                                transactions.PaymentTransactionTypeId = (int)Transaction.PaymentTransactionType;
                                transactions.Id = Guid.NewGuid();
                                List<object> lst = new List<object>();
                                if (transactions.Id != null)
                                {
                                    lst.Add("Id");
                                    lst.Add(transactions.Id);
                                }
                                if (transactions.DeviceId != null)
                                {
                                    lst.Add("DeviceId");
                                    lst.Add(transactions.DeviceId);
                                }
                                if (transactions.Time != null)
                                {
                                    lst.Add("Time");
                                    lst.Add(transactions.Time);
                                }
                                if (transactions.TimeEntry != null)
                                {
                                    lst.Add("TimeEntry");
                                    lst.Add(transactions.TimeEntry);
                                }
                                lst.Add("_IsDeleted");
                                lst.Add(false);
                                if (transactions.DeviceTypeID != null)
                                {
                                    lst.Add("DeviceTypeID");
                                    lst.Add(transactions.DeviceTypeID);
                                }
                                if (transactions.Cardnumber != null)
                                {
                                    lst.Add("Cardnumber");
                                    lst.Add(transactions.Cardnumber);
                                }
                                if (transactions.ClientID != null)
                                {
                                    lst.Add("ClientID");
                                    lst.Add(transactions.ClientID);
                                }
                                if (transactions.TarifPlanId != null)
                                {
                                    lst.Add("TarifPlanId");
                                    lst.Add(transactions.TarifPlanId);
                                }
                                if (transactions.TariffScheduleId != null)
                                {
                                    lst.Add("TariffScheduleId");
                                    lst.Add(transactions.TariffScheduleId);
                                }
                                if (transactions.Nulltime1 != null)
                                {
                                    lst.Add("Nulltime1");
                                    lst.Add(transactions.Nulltime1);
                                }
                                if (transactions.Nulltime2 != null)
                                {
                                    lst.Add("Nulltime2");
                                    lst.Add(transactions.Nulltime2);
                                }
                                if (transactions.Nulltime3 != null)
                                {
                                    lst.Add("Nulltime3");
                                    lst.Add(transactions.Nulltime3);
                                }
                                if (transactions.TVP != null)
                                {
                                    lst.Add("TVP");
                                    lst.Add(transactions.TVP);
                                }
                                if (transactions.TKVP != null)
                                {
                                    lst.Add("TKVP");
                                    lst.Add(transactions.TKVP);
                                }
                                if (transactions.SumOnCardBefore != null)
                                {
                                    lst.Add("SumOnCardBefore");
                                    lst.Add(transactions.SumOnCardBefore);
                                }
                                if (transactions.Amount != null)
                                {
                                    lst.Add("Amount");
                                    lst.Add(transactions.Amount);
                                }
                                if (transactions.Balance != null)
                                {
                                    lst.Add("Balance");
                                    lst.Add(transactions.Balance);
                                }
                                if (transactions.Paid != null)
                                {
                                    lst.Add("Paid");
                                    lst.Add(transactions.Paid);
                                }
                                if (transactions.SumOnCardAfter != null)
                                {
                                    lst.Add("SumOnCardAfter");
                                    lst.Add(transactions.SumOnCardAfter);
                                }
                                if (transactions.SumAccepted != null)
                                {
                                    lst.Add("SumAccepted");
                                    lst.Add(transactions.SumAccepted);
                                }
                                if (transactions.ClientTypidFC != null)
                                {
                                    lst.Add("ClientTypidFC");
                                    lst.Add(transactions.ClientTypidFC);
                                }
                                if (transactions.ZoneId != null)
                                {
                                    lst.Add("ZoneBeforeId");
                                    lst.Add(TechForm.ZoneComboBoxSelectedIndex);
                                    lst.Add("ZoneId");
                                    lst.Add(transactions.ZoneId);
                                    lst.Add("ZoneAfterId");
                                    lst.Add(TechForm.ZoneComboBoxSelectedIndex);
                                }
                                if (transactions.LastRecountTime != null)
                                {
                                    lst.Add("LastRecountTime");
                                    lst.Add(transactions.LastRecountTime);
                                }
                                if (transactions.TimeOplat != null)
                                {
                                    lst.Add("TimeOplat");
                                    lst.Add(transactions.TimeOplat);
                                }
                                if (transactions.PaymentTransactionTypeId != null)
                                {
                                    lst.Add("PaymentTransactionTypeId");
                                    lst.Add(transactions.PaymentTransactionTypeId);
                                }
                                if (transactions.ChangeIssued != null)
                                {
                                    lst.Add("ChangeIssued");
                                    lst.Add(transactions.ChangeIssued);
                                }
                                if (transactions.CachDispenerDown != null)
                                {
                                    lst.Add("CachDispenerDown");
                                    lst.Add(transactions.CachDispenerDown);
                                }
                                if (transactions.CachDispenerUp != null)
                                {
                                    lst.Add("CachDispenerUp");
                                    lst.Add(transactions.CachDispenerUp);
                                }
                                if (transactions.Hopper1 != null)
                                {
                                    lst.Add("Hopper1");
                                    lst.Add(transactions.Hopper1);
                                }
                                if (transactions.Hopper2 != null)
                                {
                                    lst.Add("Hopper2");
                                    lst.Add(transactions.Hopper2);
                                }
                                if (transactions.BanknotesAccepted != null)
                                {
                                    lst.Add("BanknotesAccepted");
                                    lst.Add(transactions.BanknotesAccepted);
                                }
                                if (transactions.CoinsAccepted != null)
                                {
                                    lst.Add("CoinsAccepted");
                                    lst.Add(transactions.CoinsAccepted);
                                }
                                if (transactions.BankCardAccepted != null)
                                {
                                    lst.Add("BankCardAccepted");
                                    lst.Add(transactions.BankCardAccepted);
                                }

                                lock (db_lock)
                                {
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: SendPayTransaction Utils.Running Enter");
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: SendPayTransaction Utils.Running Leave");
                                    try
                                    {
                                        UtilsBase.InsertCommand("Transactions", db, null, lst.ToArray());
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("Transactions", db, null, lst.ToArray());
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                SettingsLastUpdate = DateTime.Now;
                            }
                            break;
                        case 2:
                            if (deviceCMModel.ServerURL != null)
                            {
                                EventWebClient eventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);
                                string StrTransaction = "Transaction?CashierNumber=" + deviceCMModel.CashierNumber.ToString();
                                StrTransaction += "&Now=" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                                StrTransaction += "&TransactionNumber=" + Transaction.TransactionNumber.ToString();
                                StrTransaction += "&TransactionTime=" + Transaction.TransactionTime;
                                if (ShiftWork)
                                {
                                    if (Transaction.LastShiftOpenTime != null)
                                        StrTransaction += "&LastShiftOpenTime=" + Transaction.LastShiftOpenTime;
                                }
                                if (Transaction.PaymentTime != null)
                                    StrTransaction += "&PaymentTime=" + Transaction.PaymentTime;
                                if (Transaction.ExitBefore != null)
                                    StrTransaction += "&ExitBefore=" + Transaction.ExitBefore;
                                if (Transaction.TransactionType != TransactionTypeT.Undefined)
                                    StrTransaction += "&TransactionType=" + Transaction.TransactionType.ToString();
                                if (Transaction.WayOfPayment != WayOfPaymentT.Undefined)
                                    StrTransaction += "&WayOfPayment=" + Transaction.WayOfPayment.ToString();
                                if (Transaction.PurchaseSum != 0)
                                    StrTransaction += "&PurchaseSum=" + Transaction.PurchaseSum.ToString();
                                if (Transaction.ConclusionCode != ConclusionCodeT.Undefined)
                                    StrTransaction += "&ConclusionCode=" + Transaction.ConclusionCode.ToString();
                                if (Transaction.PrepaymentSum != 0)
                                    StrTransaction += "&PrepaymentSum=" + Transaction.PrepaymentSum.ToString("F0");
                                if (Transaction.ToPay != 0)
                                    StrTransaction += "&ToPay=" + Transaction.ToPay.ToString("F0");
                                if (Transaction.Paid != 0)
                                    StrTransaction += "&Paid=" + Transaction.Paid.ToString("F0");
                                if (Transaction.Issued != 0)
                                    StrTransaction += "&Issued=" + Transaction.Issued.ToString("F0");


                                if (Transaction.SumStayedInCM != 0)
                                    StrTransaction += "&SumStayedInCM=" + Transaction.SumStayedInCM.ToString("F0");
                                if (Transaction.BillIsued)
                                    StrTransaction += "&BillIsued=" + Transaction.BillIsued.ToString();
                                if (Transaction.ErrorCodeTransactiontop != 0)
                                    StrTransaction += "&ErrorCodeTransactiontop=" + Transaction.ErrorCodeTransactiontop.ToString();
                                for (int i = 0; i < Transaction.BanknotesAccepted.GetLength(0); i++)
                                {
                                    if (Transaction.BanknotesAccepted[i].Count > 0)
                                    {
                                        if (Transaction.BanknotesAccepted[i].Nominal == Transaction.BanknotesAccepted[i].Nominal)
                                            StrTransaction += "&BanknotesAccepted" + Transaction.BanknotesAccepted[i].Nominal.ToString("F0") + "=" + Transaction.BanknotesAccepted[i].Count.ToString();
                                        else
                                            StrTransaction += "&BanknotesAccepted" + Transaction.BanknotesAccepted[i].Nominal.ToString("F0") + "=" + Transaction.BanknotesAccepted[i].Count.ToString();
                                    }
                                }
                                for (int i = 0; i < Transaction.CoinsAccepted.GetLength(0); i++)
                                {
                                    if (Transaction.CoinsAccepted[i].Count > 0)
                                    {
                                        if (Transaction.CoinsAccepted[i].Nominal == Transaction.CoinsAccepted[i].Nominal)
                                            StrTransaction += "&CoinsAccepted" + Transaction.CoinsAccepted[i].Nominal.ToString("F0") + "=" + Transaction.CoinsAccepted[i].Count.ToString();
                                        else
                                            StrTransaction += "&CoinsAccepted" + Transaction.CoinsAccepted[i].Nominal.ToString("F0") + "=" + Transaction.CoinsAccepted[i].Count.ToString();
                                    }
                                }

                                if (Transaction.IssuedNotesCount != 0)
                                    StrTransaction += "&IssuedNotesCount=" + Transaction.IssuedNotesCount.ToString();
                                if (Transaction.IssuedFromUpper != 0)
                                    StrTransaction += "&IssuedFromUpper=" + Transaction.IssuedFromUpper.ToString();
                                if (Transaction.IssuedFromLower != 0)
                                    StrTransaction += "&IssuedFromLower=" + Transaction.IssuedFromLower.ToString();
                                if (Transaction.IssuedFrom1Hopper != 0)
                                    StrTransaction += "&IssuedFrom1Hopper=" + Transaction.IssuedFrom1Hopper.ToString();
                                if (Transaction.IssuedFrom2Hopper != 0)
                                    StrTransaction += "&IssuedFrom2Hopper=" + Transaction.IssuedFrom2Hopper.ToString();

                                if (Transaction.DeliveredNotesCount != 0)
                                    StrTransaction += "&DeliveredNotesCount=" + Transaction.DeliveredNotesCount.ToString();
                                if (Transaction.DeliveredFromUpper != 0)
                                    StrTransaction += "&DeliveredFromUpper=" + Transaction.DeliveredFromUpper.ToString();
                                if (Transaction.DeliveredNotesCount != 0)
                                    StrTransaction += "&DeliveredFromLower=" + Transaction.DeliveredFromLower.ToString();
                                if (Transaction.Rejected != 0)
                                    StrTransaction += "&Rejected=" + Transaction.Rejected.ToString();

                                eventWebClient.Process(StrTransaction);
                            }
                            break;
                    }
                    log.EndContinues((int)transactions.PaymentTransactionTypeId + 200, DateTime.Now, transactions.Id);
                    PayTransactionExported = true;
                    transactions = null;
                    if (DetailLog)
                        ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void SendAlarm(AlarmT Alarm)
        {
            try
            {
                ToLog(2, "SendAlarm Alarm.AlarmLevel=" + Alarm.AlarmLevel.ToString()+ " Alarm.AlarmLevel="+Alarm.AlarmSource.ToString() + " AlarmSourceT.CardDispenser=" + AlarmSourceT.CardDispenser.ToString() + " Alarm.ErrorCode=" + Alarm.ErrorCode.ToString());

                Guid Id = Guid.NewGuid();
                string req = "select top 1 * from MessageTypeModel where Id=" + ((int)Alarm.ErrorCode).ToString();
                DataRow mr = null;
                lock (db_lock)
                {
                    try
                    {
                        mr = UtilsBase.FillRow(req, db, null, null);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            mr = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }

                MessageTypeModelT m = null;
                if (mr != null)
                {
                    m = new MessageTypeModelT();
                    m.Color = mr.GetInt("Color");
                    m.Message = mr.GetString("Message");
                }
                if (m == null)
                {
                    Alarm.Status = "Unknown error";
                    Alarm.AlarmLevel = AlarmLevelT.Red;
                }
                else
                {
                    if (Alarm.Status == null)
                        Alarm.Status = m.Message;
                    Alarm.AlarmLevel = (AlarmLevelT)m.Color;
                }
                req = "";
                switch (deviceCMModel.ServerExchangeProtocolId)
                {
                    case 1:
                        {
                            DataTable alarmModel = null;
                            switch (Alarm.AlarmSource)
                            {
                                case AlarmSourceT.License:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId = 120 and [End] is null";
                                    break;
                                /*case AlarmSourceT.Terminal:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and (TypeId = 199 or TypeId = 297 or TypeId = 306) and [End] is null";
                                    break;*/
                                case AlarmSourceT.CashAcceptor:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and ((TypeId >= 200 and TypeId <= 205) or (TypeId >= 311 and TypeId <= 325)) and [End] is null";
                                    break;
                                case AlarmSourceT.CashDispenserDown:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 206 and TypeId <= 208 and [End] is null";
                                    break;
                                case AlarmSourceT.CashDispenserUp:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 209 and TypeId <= 211 and [End] is null";
                                    break;
                                case AlarmSourceT.CashDispenser:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and ((TypeId >= 213 and TypeId <= 219) or (TypeId >= 326 and TypeId <= 335)) and [End] is null";
                                    break;
                                case AlarmSourceT.CoinHopper1:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 220 and TypeId <= 224 and [End] is null";
                                    break;
                                case AlarmSourceT.CoinHopper2:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 225 and TypeId <= 229 and [End] is null";
                                    break;
                                case AlarmSourceT.CoinAcceptor:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 230 and TypeId <= 234 and [End] is null";
                                    break;
                                case AlarmSourceT.CardDispenser:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 240 and TypeId <= 244 and [End] is null";
                                    break;
                                case AlarmSourceT.CardReader:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 245 and TypeId <= 249 and [End] is null";
                                    break;
                                case AlarmSourceT.BankModule:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 250 and TypeId <= 254 and [End] is null";
                                    break;
                                case AlarmSourceT.SlaveController:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 255 and TypeId <= 259 and [End] is null";
                                    break;
                                case AlarmSourceT.KKM:
                                case AlarmSourceT.Printer:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 280 and TypeId <= 292 and [End] is null";
                                    break;
                                case AlarmSourceT.ExternalDoor:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId = 293 and [End] is null";
                                    break;
                                case AlarmSourceT.InternalDoor:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 294 and [End] is null";
                                    break;
                                case AlarmSourceT.PaymentError:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and TypeId >= 299 and TypeId <= 306 and [End] is null";
                                    break;
                                default:
                                    req = "select * from AlarmModel where DeviceId ='" + DeviceId.ToString() +
                                        "' and ((TypeId >= 295 and TypeId <= 298) or TypeId = 199 or TypeId = 310) and [End] is null";
                                    break;
                            }
                            if (req != "")
                            {
                                lock (db_lock)
                                {
                                    try
                                    {
                                        alarmModel = UtilsBase.FillTable(req, cashierForm.db, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        cashierForm.ToLog(0, exc.ToString());
                                        try
                                        {
                                            alarmModel = UtilsBase.FillTable(req, cashierForm.db, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            cashierForm.ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                            else
                                alarmModel = null;

                            if (alarmModel != null)
                            {
                                for (int i = 0; i < alarmModel.Rows.Count; i++)
                                {
                                    lock (db_lock)
                                    {
                                        if (DetailLog)
                                            ToLog(2, "Cashier: SendAlarm Utils.Running Enter");
                                        while (Utils.Running)
                                            Thread.Sleep(50);
                                        if (DetailLog)
                                            ToLog(2, "Cashier: SendAlarm Utils.Running Leave");
                                        try
                                        {
                                            UtilsBase.UpdateCommand("AlarmModel", new object[] { "[End]", DateTime.Now, "ValueEnd", Alarm.Status }, "Id=@Key", new object[] { "@Key", alarmModel.Rows[i].GetGuid("Id") }, db);
                                        }
                                        catch (Exception exc)
                                        {
                                            cashierForm.ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.UpdateCommand("AlarmModel", new object[] { "[End]", DateTime.Now, "ValueEnd", Alarm.Status }, "Id=@Key", new object[] { "@Key", alarmModel.Rows[i].GetGuid("Id") }, db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                cashierForm.ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                            }

                            if (Alarm.AlarmLevel != AlarmLevelT.Green)
                            {
                                Alarm.Now = DateTime.Now;

                                lock (db_lock)
                                {
                                    if (DetailLog)
                                        ToLog(2, "Cashier: SendAlarm Utils.Running Enter");
                                    while (Utils.Running)
                                        Thread.Sleep(50);
                                    if (DetailLog)
                                        ToLog(2, "Cashier: SendAlarm Utils.Running Leave");
                                    try
                                    {
                                        UtilsBase.InsertCommand("AlarmModel", db, null, new object[] {
                                    "Id", Id,
                                    "DeviceId", DeviceId,
                                    "[Begin]", Alarm.Now,
                                    "[Value]", Alarm.Status,
                                    "AlarmColorId" , (int)Alarm.AlarmLevel,
                                    "_IsDeleted", false,
                                    "TypeId", (int)Alarm.ErrorCode
                                });
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("AlarmModel", db, null, new object[] {
                                    "Id", Id,
                                    "DeviceId", DeviceId,
                                    "[Begin]", Alarm.Now,
                                    "[Value]", Alarm.Status,
                                    "AlarmColorId" , (int)Alarm.AlarmLevel,
                                    "_IsDeleted", false,
                                    "TypeId", (int)Alarm.ErrorCode
                                });
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }

                            SettingsLastUpdate = DateTime.Now;
                        }
                        break;
                    case 2:
                        if (deviceCMModel.ServerURL != null)
                        {
                            EventWebClient eventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);
                            string StrTransaction = "Alarm?CashierNumber=" + deviceCMModel.CashierNumber.ToString();
                            StrTransaction += "&Now=" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            StrTransaction += "&AlarmLevel=" + Alarm.AlarmLevel.ToString();
                            StrTransaction += "&AlarmSource=" + Alarm.AlarmSource.ToString();
                            StrTransaction += "&Status=" + Alarm.Status;
                            if (Alarm.ErrorCode != 0)
                                StrTransaction += "&ErrorCode=" + Alarm.ErrorCode.ToString();
                            eventWebClient.Process(StrTransaction);
                        }
                        break;
                }
                if ((Alarm.Status != null) && (Alarm.Status != ""))
                {
                    ToLog(0, Alarm.Status);
                    if (Alarm.AlarmLevel > 0)
                        log.LogEvent((int)Alarm.AlarmLevel, Id, Alarm.Now);
                }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void SendCardTransaction()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (card != null)
                {
                    CardsTransactionT cardstransactions = new CardsTransactionT { Id = Guid.NewGuid() };

                    if (card.Blocked == null)
                        card.Blocked = 0;

                    cardstransactions.Blocked = (byte)card.Blocked;

                    if (card._IsDeleted == null)
                        cardstransactions._IsDeleted = false;
                    else
                        cardstransactions._IsDeleted = (bool)card._IsDeleted;

                    cardstransactions.CardId = card.CardId;
                    cardstransactions.ClientGroupidFC = CardInfo.ClientGroupidFC;
                    cardstransactions.ClientTypidFC = CardInfo.ClientTypidFC;
                    cardstransactions.LastPaymentTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime);
                    cardstransactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                    cardstransactions.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                    cardstransactions.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                    cardstransactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                    cardstransactions.TVP = datetime0 + TimeSpan.FromSeconds(CardInfo.TVP);
                    cardstransactions.ParkingEnterTime = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                    cardstransactions.SumOnCard = CardInfo.SumOnCard;
                    cardstransactions.TKVP = CardInfo.TKVP;
                    cardstransactions.TPidFC = CardInfo.TPidFC;
                    cardstransactions.TSidFC = CardInfo.TSidFC;
                    cardstransactions.ZoneidFC = CardInfo.ZoneidFC;
                    cardstransactions.DateSaveCard = datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard);



                    List<object> lst = new List<object>();
                    //if (cardstransactions.Id != null)
                    {
                        lst.Add("Id");
                        lst.Add(cardstransactions.Id);
                    }
                    //if (cardstransactions.Blocked != null)
                    {
                        lst.Add("Blocked");
                        lst.Add(cardstransactions.Blocked);
                    }
                    //if (cardstransactions._IsDeleted != null)
                    {
                        lst.Add("_IsDeleted");
                        lst.Add(cardstransactions._IsDeleted);
                    }
                    //if (cardstransactions.CardId != null)
                    {
                        lst.Add("CardId");
                        lst.Add(cardstransactions.CardId);
                    }
                    if (cardstransactions.ClientGroupidFC != null)
                    {
                        lst.Add("ClientGroupidFC");
                        lst.Add(cardstransactions.ClientGroupidFC);
                    }
                    if (cardstransactions.ClientTypidFC != null)
                    {
                        lst.Add("ClientTypidFC");
                        lst.Add(cardstransactions.ClientTypidFC);
                    }
                    if (cardstransactions.LastPaymentTime != null)
                    {
                        lst.Add("LastPaymentTime");
                        lst.Add(cardstransactions.LastPaymentTime);
                    }
                    if (cardstransactions.LastRecountTime != null)
                    {
                        lst.Add("LastRecountTime");
                        lst.Add(cardstransactions.LastRecountTime);
                    }
                    if (cardstransactions.Nulltime1 != null)
                    {
                        lst.Add("Nulltime1");
                        lst.Add(cardstransactions.Nulltime1);
                    }
                    if (cardstransactions.Nulltime2 != null)
                    {
                        lst.Add("Nulltime2");
                        lst.Add(cardstransactions.Nulltime2);
                    }
                    if (cardstransactions.Nulltime3 != null)
                    {
                        lst.Add("Nulltime3");
                        lst.Add(cardstransactions.Nulltime3);
                    }
                    if (cardstransactions.TVP != null)
                    {
                        lst.Add("TVP");
                        lst.Add(cardstransactions.TVP);
                    }
                    if (cardstransactions.ParkingEnterTime != null)
                    {
                        lst.Add("ParkingEnterTime");
                        lst.Add(cardstransactions.ParkingEnterTime);
                    }
                    if (cardstransactions.SumOnCard != null)
                    {
                        lst.Add("SumOnCard");
                        lst.Add(cardstransactions.SumOnCard);
                    }
                    if (cardstransactions.TKVP != null)
                    {
                        lst.Add("TKVP");
                        lst.Add(cardstransactions.TKVP);
                    }
                    if (cardstransactions.TPidFC != null)
                    {
                        lst.Add("TPidFC");
                        lst.Add(cardstransactions.TPidFC);
                    }
                    if (cardstransactions.TSidFC != null)
                    {
                        lst.Add("TSidFC");
                        lst.Add(cardstransactions.TSidFC);
                    }
                    if (cardstransactions.ZoneidFC != null)
                    {
                        lst.Add("ZoneidFC");
                        lst.Add(cardstransactions.ZoneidFC);
                    }
                    if (cardstransactions.DateSaveCard != null)
                    {
                        lst.Add("DateSaveCard");
                        lst.Add(cardstransactions.DateSaveCard);
                    }
                    LogToCardsOnline(cardstransactions);
                    lock (db_lock)
                    {
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: SendCardsTransaction Utils.Running Enter");
                        //while (Utils.Running)
                        //    Thread.Sleep(50);
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: SendCardsTransaction Utils.Running Leave");
                        try
                        {
                            UtilsBase.InsertCommand("CardsTransaction", db, null, lst.ToArray());
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                UtilsBase.InsertCommand("CardsTransaction", db, null, lst.ToArray());
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                }

                SettingsLastUpdate = DateTime.Now;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void ClearAlarm()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                string strhas = "update AlarmModel set [End]=@date where AlarmModel.DeviceId='" + DeviceId.ToString() + "' and [End] is NULL";
                SqlParameter pr = new SqlParameter("@date", DateTime.Now);
                if (DetailLog)
                    ToLog(2, "Cashier: SendCardsTransaction Utils.Running Enter");
                while (Utils.Running)
                    Thread.Sleep(50);
                if (DetailLog)
                    ToLog(2, "Cashier: SendCardsTransaction Utils.Running Leave");
                lock (db_lock)
                {
                    using (SqlCommand comm = new SqlCommand(strhas, db))
                    {
                        comm.Parameters.Add(pr);
                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
    }
}