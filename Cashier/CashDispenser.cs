﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Threading;
using System.Windows.Media;
using Rps.ShareBase;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public void SetNotesNominal()
        {
            bool inh = true;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Start");
            if (CashRecycler != null && CashRecycler.IsOpen)
            {
                inh = CashRecycler.Inhibit;
                if (!CashRecycler.Inhibit)
                {
                    CashRecycler.InhibitOn();
                }
                for (int i = 0; i < Banknotes.Count; i++)
                {
                    if (Banknotes[i].Id == deviceCMModel.NominalUpId)
                    {
                        ToLog(2, "Cashier: CashDispenserSetSettings: CashRecycler.SetNoteNominal");
                        CashDispenser.SetNoteNominal(1, Valute, Convert.ToInt16(Banknotes[i].Value));
                        //ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteSize");
                        //CashDispenser.SetNoteSize(1, Convert.ToInt16(Banknotes[i].Length), Convert.ToInt16(Banknotes[i].Width));
                        UpNominal = Convert.ToDecimal(Banknotes[i].Value);
                    }
                    if (Banknotes[i].Id == deviceCMModel.NominalDownId)
                    {
                        ToLog(2, "Cashier: CashDispenserSetSettings: CashRecycler.SetNoteNominal");
                        CashDispenser.SetNoteNominal(2, Valute, Convert.ToInt16(Banknotes[i].Value));
                        //ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteSize");
                        //CashDispenser.SetNoteSize(1, Convert.ToInt16(Banknotes[i].Length), Convert.ToInt16(Banknotes[i].Width));
                        DownNominal = Convert.ToDecimal(Banknotes[i].Value);
                    }
                }
            }
            else
            if (CashDispenser != null && CashDispenser.IsOpen)
            {
                for (int i = 0; i < Banknotes.Count; i++)
                {
                    if (Banknotes[i].Id == deviceCMModel.NominalUpId)
                    {
                        ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteNominal");
                        CashDispenser.SetNoteNominal(1, Valute, Convert.ToInt32(Banknotes[i].Value));
                        //ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteSize");
                        //CashDispenser.SetNoteSize(1, Convert.ToInt16(Banknotes[i].Length), Convert.ToInt16(Banknotes[i].Width));
                        UpNominal = Convert.ToDecimal(Banknotes[i].Value);
                    }
                    if (Banknotes[i].Id == deviceCMModel.NominalDownId)
                    {
                        ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteNominal");
                        CashDispenser.SetNoteNominal(2, Valute, Convert.ToInt32(Banknotes[i].Value));
                        //ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.SetNoteSize");
                        //CashDispenser.SetNoteSize(1, Convert.ToInt16(Banknotes[i].Length), Convert.ToInt16(Banknotes[i].Width));
                        DownNominal = Convert.ToDecimal(Banknotes[i].Value);
                    }
                }
            }
            if (CashRecycler != null && CashRecycler.IsOpen && !inh)
            {
                CashRecycler.InhibitOff();
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        DateTime CashDispenserAlarmTime, CashDispenserUpAlarmTime, CashDispenserDownAlarmTime;
        void HardCashDispenser()
        {
            bool init = false;
            string text;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (GiveDeliveryThread == null)
                {
                    CashDispenser1Level = AlarmLevelT.Green;
                    if (CashDispenser == null)
                    {
                        if (CashDispenserModel == "Multimech")
                        {
                            ToLog(2, "Cashier: HardCashDispenser: Create CashDispenserMultiMech");
                            CashDispenser = new CashDispenserMultiMech(CashDispenserLock, log);
                        }
                        else
                        {
                            if (CashRecycler == null)
                            {
                                CashRecycler = new CashRecyclerJCM_RC_ID_003(CashDispenserLock, log, DefaultCulture);
                                ToLog(2, "Cashier: HardCashDispenser: Create CashRecyclerJCM_RC_ID_003");
                            }
                            CashDispenser = CashRecycler;
                            init = true;
                            if (!CashDispenser.IsOpen)
                                CashDispenser.Open(CashAcceptorPort);
                        }
                    }
                    if (!CashDispenser.IsOpen || init)
                    {
                        init = false;
                        bool res;
                        if ((CashAcceptorStatusCheckDT == null) || (CashAcceptorStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                        {
                            ToLog(2, "Cashier: HardCashDispenser: CashDispenser.Open");
                            if (CashDispenserModel == "Multimech")
                                res = CashDispenser.Open(CashDispenserPort);
                            else
                                res = CashDispenser.Open(CashAcceptorPort);
                            if (res)
                            {
                                if (CashDispenserModel != "Multimech")
                                {
                                    ToLog(1, "CashRecycler Version: " + CashAcceptor.Version);
                                    ToLog(1, "CashRecycler BootVersion: " + CashAcceptor.BootVersion);
                                }

                                ToLog(2, "Cashier: HardCashDispenser: CashDispenserSetSettings");
                                CashDispenserSetSettings();

                                if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Green)
                                    TechForm.CashDispenserPortLabel.Foreground = Brushes.Green;
                                CashDispenserWorkChanged = true;
                                CashDispenserWork = true;
                                ToLog(2, "Cashier: HardCashDispenser: CashDispenser.CheckStatus");
                                CashDispenser.CheckStatus();
                                if (CashDispenser.CashDispenserResult.S == CashDispenserStatusT.SuccessfulCommand)
                                {
                                    if (
                                        ((CashDispenser.CashDispenserResult.MainMotor != null) && (CashDispenser.CashDispenserResult.MainMotor != "00")) ||
                                        ((CashDispenser.CashDispenserResult.NoteQualifier != null) && (CashDispenser.CashDispenserResult.NoteQualifier != "00")) ||
                                        ((CashDispenser.CashDispenserResult.NoteDiverterer != null) && (CashDispenser.CashDispenserResult.NoteDiverterer != "00")) ||
                                        ((CashDispenser.CashDispenserResult.NoteTransport != null) && (CashDispenser.CashDispenserResult.NoteTransport != "00")) ||
                                        ((CashDispenser.CashDispenserResult.NoteOutput != null) && (CashDispenser.CashDispenserResult.NoteOutput != "00")) ||
                                        ((CashDispenser.CashDispenserResult.DataHandler != null) && (CashDispenser.CashDispenserResult.DataHandler != "00"))
                                    )
                                    {
                                        CashDispenserWork = false;
                                        text = "";
                                        //text = ResourceManager.GetString("CashDispenserError", DefaultCulture);
                                        if (CashDispenser.CashDispenserResult.MainMotor != "00")
                                            text = ResourceManager.GetString("CashDispenserMainMotor", DefaultCulture) + Environment.NewLine;
                                        if (CashDispenser.CashDispenserResult.NoteQualifier != "00")
                                            text = ResourceManager.GetString("CashDispenserNoteQualifier", DefaultCulture) + Environment.NewLine;
                                        if (CashDispenser.CashDispenserResult.NoteDiverterer != "00")
                                            text = ResourceManager.GetString("CashDispenserNoteDiverterer", DefaultCulture) + Environment.NewLine;
                                        if (CashDispenser.CashDispenserResult.NoteTransport != "00")
                                            text = ResourceManager.GetString("CashDispenserNoteTransport", DefaultCulture) + Environment.NewLine;
                                        if (CashDispenser.CashDispenserResult.NoteOutput != "00")
                                            text = ResourceManager.GetString("CashDispenserNoteOutput", DefaultCulture) + Environment.NewLine;
                                        if (CashDispenser.CashDispenserResult.DataHandler != "00")
                                        {
                                            switch (CashDispenser.CashDispenserResult.DataHandler)
                                            {
                                                /*case "01":
                                                    text += " CashDispenser Busy" + Environment.NewLine;
                                                    break;*/
                                                case "02":
                                                    text = ResourceManager.GetString("CashDispenserBoxOpen", DefaultCulture) + Environment.NewLine;
                                                    break;
                                                case "03":
                                                    text = ResourceManager.GetString("CashDispenserFault_0x43", DefaultCulture) + Environment.NewLine;
                                                    break;
                                                case "04":
                                                    text = ResourceManager.GetString("CashDispenserFault_0x4A", DefaultCulture) + Environment.NewLine;
                                                    break;
                                                case "05":
                                                    text = ResourceManager.GetString("CashDispenserNotConnect", DefaultCulture) + Environment.NewLine;
                                                    break;
                                            }
                                        }
                                        ErrorsText += text + Environment.NewLine;
                                        if (CashDispenserLevel != AlarmLevelT.Brown)
                                        {
                                            if (CashDispenserAlarmTime == DateTime.MinValue)
                                            {
                                                CashDispenserAlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    CashDispenserOldLevel = CashDispenserLevel;
                                                    CashDispenserLevel = AlarmLevelT.Brown;
                                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                    Alarm.AlarmSource = AlarmSourceT.CashDispenser;

                                                    if (CashDispenser.CashDispenserResult.MainMotor != "00")
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserMainMotorError;
                                                    else if (CashDispenser.CashDispenserResult.NoteQualifier != "00")
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserNoteQualifierError;
                                                    else if (CashDispenser.CashDispenserResult.NoteDiverterer != "00")
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserNoteDivertererError;
                                                    else if (CashDispenser.CashDispenserResult.NoteTransport != "00")
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserNoteTransportError;
                                                    else if (CashDispenser.CashDispenserResult.NoteOutput != "00")
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserNoteOutputError;
                                                    else if (CashDispenser.CashDispenserResult.DataHandler != "00")
                                                    {
                                                        switch (CashDispenser.CashDispenserResult.DataHandler)
                                                        {
                                                            /*case "01":
                                                                Alarm.ErrorCode = AlarmCode.CashDispenserBusy;
                                                                break;*/
                                                            case "02":
                                                                Alarm.ErrorCode = AlarmCode.CashDispenserDoorOpen;
                                                                break;
                                                            case "03":
                                                                Alarm.ErrorCode = AlarmCode.CashDispenserEEPROMError;
                                                                break;
                                                            case "04":
                                                                Alarm.ErrorCode = AlarmCode.CashDispenserHardwareError;
                                                                break;
                                                            case "05":
                                                                Alarm.ErrorCode = AlarmCode.CashDispenserUnconnected;
                                                                break;
                                                        }
                                                    }

                                                    //Alarm.ErrorCode = AlarmCode.CashDispenserError;

                                                    //Alarm.Status = text;

                                                    SendAlarm(Alarm);
                                                    //text = Alarm.Status;
                                                    CashDispenserAlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }

                                        if (ShiftOpened)
                                        {
                                            ToLog(2, "Cashier: HardCashDispenser: CashDispenser.OpenCassette");
                                            CashDispenser.OpenCassette();
                                            ToLog(2, "Cashier: HardCashDispenser: CashDispenser.ReadCassetteID");
                                            CashDispenser.ReadCassetteID();
                                        }
                                        else
                                        {
                                            ToLog(2, "Cashier: HardCashDispenser: CashDispenser.CloseCassette");
                                            CashDispenser.CloseCassette();
                                        }
                                        if ((CashDispenser.CashDispenserResult.S == CashDispenserStatusT.LowLevel) ||
                                                 (CashDispenser.CashDispenserResult.S == CashDispenserStatusT.RejectVaultAlmostFull))
                                        {
                                            text = "CashDispenser Reject Vault Almost Full";
                                            ErrorsText += text + Environment.NewLine;
                                            CashDispenser1Level = AlarmLevelT.Yellow;
                                            if (CashDispenserLevel != AlarmLevelT.Yellow)
                                            {
                                                if (CashDispenserAlarmTime == DateTime.MinValue)
                                                {
                                                    CashDispenserAlarmTime = DateTime.Now;
                                                }
                                                else
                                                {
                                                    if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                                    {
                                                        AlarmT Alarm = new AlarmT();
                                                        Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                                        CashDispenserOldLevel = CashDispenserLevel;
                                                        CashDispenserLevel = AlarmLevelT.Yellow;
                                                        Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserRejectAlmosFull;
                                                        Alarm.Status = text;
                                                        SendAlarm(Alarm);
                                                        CashDispenserAlarmTime = DateTime.MinValue;
                                                    }
                                                }
                                            }
                                        }
                                        else if ((CashDispenser.CashDispenserResult.S == CashDispenserStatusT.CommunicationsTimeOut1) ||
                                            (CashDispenser.CashDispenserResult.S == CashDispenserStatusT.CommunicationsTimeOut2))
                                        {
                                            text = ResourceManager.GetString("CashDispenserError", DefaultCulture);
                                            if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                                text += " CommunicationsError: " + CashDispenser.CashDispenserResult.CommunicationsErrorStatus;
                                            else
                                                text += " 1 " + CashDispenser.CashDispenserResult.S.ToString();
                                            ErrorsText += text + Environment.NewLine;
                                            if (CashDispenserLevel != AlarmLevelT.Brown)
                                            {
                                                if (CashDispenserAlarmTime == DateTime.MinValue)
                                                {
                                                    CashDispenserAlarmTime = DateTime.Now;
                                                }
                                                else
                                                {
                                                    if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                                    {
                                                        AlarmT Alarm = new AlarmT();
                                                        CashDispenserOldLevel = CashDispenserLevel;
                                                        CashDispenserLevel = AlarmLevelT.Brown;
                                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                        Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                                        Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                                        Alarm.Status = text;
                                                        SendAlarm(Alarm);
                                                        CashDispenserAlarmTime = DateTime.MinValue;
                                                    }
                                                }
                                            }
                                            CashDispenser.Close();
                                        }
                                    }
                                    if (CashDispenser.IsOpen)
                                        CashDispenser.Pool = true;
                                }
                                CashAcceptorStatusCheckDT = DateTime.Now;
                            }
                        }

                        if ((!CashDispenser.IsOpen))
                        {
                            if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Red)
                                TechForm.CashDispenserPortLabel.Foreground = Brushes.Red;
                            ErrorsText += ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture);
                            if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                ErrorsText += " CommunicationsError: " + CashDispenser.CashDispenserResult.CommunicationsErrorStatus;
                            else
                                ErrorsText += " 3 " + CashDispenser.CashDispenserResult.S.ToString();

                            ErrorsText += Environment.NewLine;

                            if (CashDispenserWork)
                            {
                                CashDispenserWorkChanged = true;
                                CashDispenserWork = false;
                            }
                            if (CashDispenserLevel != AlarmLevelT.Brown)
                            {
                                if (CashDispenserAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashDispenserOldLevel = CashDispenserLevel;
                                        CashDispenserLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                        Alarm.Status = ResourceManager.GetString("CashDispenserError", DefaultCulture);
                                        if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                            Alarm.Status += " CommunicationsError";
                                        else
                                            Alarm.Status += " 4 " + CashDispenser.CashDispenserResult.S.ToString();
                                        SendAlarm(Alarm);
                                        CashDispenserAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (CashDispenser.CashDispenserResult.S == CashDispenserStatusT.CommunicationsTimeOut)
                            {
                                if (CashDispenserCommError >= 2)
                                {
                                    if (CashDispenserWork)
                                    {
                                        CashDispenserWorkChanged = true;
                                        CashDispenserWork = false;
                                    }
                                    if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Red)
                                        TechForm.CashDispenserPortLabel.Foreground = Brushes.Red;
                                    ErrorsText += ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture);
                                    if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                        ErrorsText += " CommunicationsError: " + CashDispenser.CashDispenserResult.CommunicationsErrorStatus;
                                    else
                                        ErrorsText += " 5 " + CashDispenser.CashDispenserResult.S.ToString();

                                    ErrorsText += Environment.NewLine;

                                    if (CashDispenserAlarmTime == DateTime.MinValue)
                                    {
                                        CashDispenserAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            if (CashDispenserLevel != AlarmLevelT.Brown)
                                            {
                                                AlarmT Alarm = new AlarmT();
                                                CashDispenserOldLevel = CashDispenserLevel;
                                                CashDispenserLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                Alarm.ErrorCode = AlarmCode.CashDispenserNotConnect;
                                                SendAlarm(Alarm);
                                                CashDispenserAlarmTime = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (CashDispenserCommError < 3)
                                        CashDispenserCommError++;
                                }
                            }
                            else if ((CashDispenser.CashDispenserResult.S == CashDispenserStatusT.SuccessfulCommand) || CashDispenser.CashDispenserResult.S == CashDispenserStatusT.RejectedNotes)
                            {
                                if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Green)
                                    TechForm.CashDispenserPortLabel.Foreground = Brushes.Green;
                                if (
                                    ((CashDispenser.CashDispenserResult.MainMotor != null) && (CashDispenser.CashDispenserResult.MainMotor != "00")) ||
                                    ((CashDispenser.CashDispenserResult.NoteQualifier != null) && (CashDispenser.CashDispenserResult.NoteQualifier != "00")) ||
                                    ((CashDispenser.CashDispenserResult.NoteDiverterer != null) && (CashDispenser.CashDispenserResult.NoteDiverterer != "00")) ||
                                    ((CashDispenser.CashDispenserResult.NoteTransport != null) && (CashDispenser.CashDispenserResult.NoteTransport != "00")) ||
                                    ((CashDispenser.CashDispenserResult.NoteOutput != null) && (CashDispenser.CashDispenserResult.NoteOutput != "00")) ||
                                    ((CashDispenser.CashDispenserResult.DataHandler != null) && (CashDispenser.CashDispenserResult.DataHandler != "00"))
                                )
                                {
                                    CashDispenserWork = false;
                                    text = "";
                                    //text = ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture);
                                    if (CashDispenser.CashDispenserResult.MainMotor != "00")
                                        text = ResourceManager.GetString("CashDispenserMainMotor", DefaultCulture) + Environment.NewLine;
                                    if (CashDispenser.CashDispenserResult.NoteQualifier != "00")
                                        text = ResourceManager.GetString("CashDispenserNoteQualifier", DefaultCulture) + Environment.NewLine;
                                    if (CashDispenser.CashDispenserResult.NoteDiverterer != "00")
                                        text = ResourceManager.GetString("CashDispenserNoteDiverterer", DefaultCulture) + Environment.NewLine;
                                    if (CashDispenser.CashDispenserResult.NoteTransport != "00")
                                        text = ResourceManager.GetString("CashDispenserNoteTransport", DefaultCulture) + Environment.NewLine;
                                    if (CashDispenser.CashDispenserResult.NoteOutput != "00")
                                        text = ResourceManager.GetString("CashDispenserNoteOutput", DefaultCulture) + Environment.NewLine;
                                    if (CashDispenser.CashDispenserResult.DataHandler != "00")
                                    {
                                        switch (CashDispenser.CashDispenserResult.DataHandler)
                                        {
                                            /*case "01":
                                                text = ResourceManager.GetString("CashDispenserBoxOpen", DefaultCulture) + Environment.NewLine;
                                                break;*/
                                            case "02":
                                                text = ResourceManager.GetString("CashDispenserBoxOpen", DefaultCulture) + Environment.NewLine;
                                                break;
                                            case "03":
                                                text = ResourceManager.GetString("CashDispenserFault_0x43", DefaultCulture) + Environment.NewLine;
                                                break;
                                            case "04":
                                                text = ResourceManager.GetString("CashDispenserFault_0x4A", DefaultCulture) + Environment.NewLine;
                                                break;
                                            case "05":
                                                text = ResourceManager.GetString("CashDispenserNotConnect", DefaultCulture) + Environment.NewLine;
                                                break;
                                        }
                                    }
                                    ErrorsText += text;
                                    if (CashDispenserLevel != AlarmLevelT.Brown)
                                    {
                                        if (CashDispenserAlarmTime == DateTime.MinValue)
                                        {
                                            CashDispenserAlarmTime = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                            {
                                                AlarmT Alarm = new AlarmT();
                                                CashDispenserOldLevel = CashDispenserLevel;
                                                CashDispenserLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmSource = AlarmSourceT.CashDispenser;

                                                if (CashDispenser.CashDispenserResult.MainMotor != "00")
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserMainMotorError;
                                                else if (CashDispenser.CashDispenserResult.NoteQualifier != "00")
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserNoteQualifierError;
                                                else if (CashDispenser.CashDispenserResult.NoteDiverterer != "00")
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserNoteDivertererError;
                                                else if (CashDispenser.CashDispenserResult.NoteTransport != "00")
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserNoteTransportError;
                                                else if (CashDispenser.CashDispenserResult.NoteOutput != "00")
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserNoteOutputError;
                                                else if (CashDispenser.CashDispenserResult.DataHandler != "00")
                                                {
                                                    switch (CashDispenser.CashDispenserResult.DataHandler)
                                                    {
                                                        /*case "01":
                                                            Alarm.ErrorCode = AlarmCode.CashDispenserBusy;
                                                            break;*/
                                                        case "02":
                                                            Alarm.ErrorCode = AlarmCode.CashDispenserDoorOpen;
                                                            break;
                                                        case "03":
                                                            Alarm.ErrorCode = AlarmCode.CashDispenserEEPROMError;
                                                            break;
                                                        case "04":
                                                            Alarm.ErrorCode = AlarmCode.CashDispenserHardwareError;
                                                            break;
                                                        case "05":
                                                            Alarm.ErrorCode = AlarmCode.CashDispenserUnconnected;
                                                            break;
                                                    }
                                                }

                                                SendAlarm(Alarm);
                                                CashDispenserAlarmTime = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if ((MoveForwardResult != null) &&
                                        (((CashDispenserResultT)MoveForwardResult).S != CashDispenserStatusT.SuccessfulCommand)
                                        &&
                                        (((CashDispenserResultT)MoveForwardResult).S != CashDispenserStatusT.FailureToFeed)
                                        )
                                    {
                                        text = ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture) + " " + ((CashDispenserResultT)MoveForwardResult).S.ToString();
                                        ErrorsText += text;
                                        if (CashDispenserLevel != AlarmLevelT.Brown)
                                        {
                                            if (CashDispenserAlarmTime == DateTime.MinValue)
                                            {
                                                CashDispenserAlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    CashDispenserOldLevel = CashDispenserLevel;
                                                    CashDispenserLevel = AlarmLevelT.Brown;
                                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                    Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                                    Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                                    Alarm.Status = text;
                                                    SendAlarm(Alarm);
                                                    CashDispenserAlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!CashDispenserWork)
                                        {
                                            CashDispenserWorkChanged = true;
                                            CashDispenserWork = true;
                                        }
                                        if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Green)
                                            TechForm.CashDispenserPortLabel.Foreground = Brushes.Green;

                                        if (CashDispenserAlarmTime != DateTime.MinValue)
                                            CashDispenserAlarmTime = DateTime.MinValue;
                                        if (CashDispenserLevel != AlarmLevelT.Green)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            CashDispenserOldLevel = CashDispenserLevel;
                                            CashDispenserLevel = AlarmLevelT.Green;
                                            Alarm.AlarmLevel = AlarmLevelT.Green;
                                            Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                                            SendAlarm(Alarm);
                                        }
                                    }
                                }

                                if (DeliveryUpCountChanged)
                                {
                                    if ((deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist) && (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked))
                                    {
                                        if (CashDispenser.SetCount(1, DeliveryUpCount))
                                            DeliveryUpCountChanged = false;
                                    }
                                    ToLog(2, "Cashier: HardCashDispenser DeliveryUpCount=" + DeliveryUpCount.ToString());
                                }
                                if (DeliveryDownCountChanged)
                                {
                                    if ((deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist) && (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked))
                                    {
                                        if (CashDispenser.SetCount(2, DeliveryDownCount))
                                            DeliveryDownCountChanged = false;
                                    }
                                    ToLog(2, "Cashier: HardCashDispenser DeliveryDownCount=" + cashierForm.DeliveryDownCount.ToString());
                                }
                            }
                            else if (
                                (
                                    CashDispenser.CashDispenserResult.S == CashDispenserStatusT.LowLevel ||
                                    CashDispenser.CashDispenserResult.S == CashDispenserStatusT.RejectVaultAlmostFull
                                )
                            )
                            {
                                ErrorsText += "CashDispenser Reject Vault Almost Full" + Environment.NewLine;
                                if (CashDispenserLevel != AlarmLevelT.Yellow)
                                {
                                    if (CashDispenserAlarmTime == DateTime.MinValue)
                                    {
                                        CashDispenserAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                            CashDispenserOldLevel = CashDispenserLevel;
                                            CashDispenserLevel = AlarmLevelT.Yellow;
                                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                            Alarm.ErrorCode = AlarmCode.CashDispenserRejectAlmosFull;
                                            Alarm.Status = "CashDispenser Reject Vault Almost Full";
                                            SendAlarm(Alarm);
                                            CashDispenserAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (CashDispenserWork)
                                    CashDispenserWorkChanged = true;
                                CashDispenserWork = false;

                                ErrorsText += ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture);
                                if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                    ErrorsText += " CommunicationsError: " + CashDispenser.CashDispenserResult.CommunicationsErrorStatus;
                                else
                                    ErrorsText += " 6 " + CashDispenser.CashDispenserResult.S.ToString();

                                ErrorsText += Environment.NewLine;

                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                if ((CashDispenser.CashDispenserResult.CommunicationsErrorStatus != null) && (CashDispenser.CashDispenserResult.CommunicationsErrorStatus != ""))
                                {
                                    if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Red)
                                        TechForm.CashDispenserPortLabel.Foreground = Brushes.Red;
                                    if (CashDispenserLevel != AlarmLevelT.Brown)
                                    {
                                        if (CashDispenserAlarmTime == DateTime.MinValue)
                                        {
                                            CashDispenserAlarmTime = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                            {
                                                CashDispenserOldLevel = CashDispenserLevel;
                                                CashDispenserLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                                SendAlarm(Alarm);
                                                CashDispenserAlarmTime = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (CashDispenserLevel != AlarmLevelT.Brown)
                                    {
                                        if (CashDispenserAlarmTime == DateTime.MinValue)
                                        {
                                            CashDispenserAlarmTime = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                            {
                                                CashDispenserOldLevel = CashDispenserLevel;
                                                CashDispenserLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                                Alarm.Status = ResourceManager.GetString("CashDispenserError", DefaultCulture) + " 7 " + CashDispenser.CashDispenserResult.S.ToString();
                                                SendAlarm(Alarm);
                                                CashDispenserAlarmTime = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        public void GetCashDispenserStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist)
                {
                    if (EmulatorEnabled)
                        EmulatorForm.Emulator.CashDispenserGroupBox.Enabled = true;

                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                    {
                        HardCashDispenser();
                    }
                    else
                    {
                        if (CashDispenser != null)
                        {
                            if ((CashDispenserModel == "Multimech") || CashDispenser.IsOpen)
                                CashDispenser.Close();
                        }
                        if (EmulatorForm.Emulator.CashDispenserWork.Checked)
                        {

                            if (!CashDispenserWork)
                            {
                                CashDispenserWorkChanged = true;
                                CashDispenserWork = true;
                            }
                            if (TechForm.CashDispenserPortLabel.Foreground != Brushes.Green)
                                TechForm.CashDispenserPortLabel.Foreground = Brushes.Green;

                            if (CashDispenserAlarmTime != DateTime.MinValue)
                                CashDispenserAlarmTime = DateTime.MinValue;
                            if (CashDispenserLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                CashDispenserOldLevel = CashDispenserLevel;
                                CashDispenserLevel = AlarmLevelT.Green;
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                SendAlarm(Alarm);
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CashDispenserNeeded.Checked)
                            {
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("TechCashDispenserCheckReq", DefaultCulture) + Environment.NewLine;
                                if (CashDispenserWork)
                                {
                                    CashDispenserWorkChanged = true;
                                    CashDispenserWork = false;
                                }
                            }
                            else
                            {
                                if (CashDispenserWork)
                                {
                                    CashDispenserWorkChanged = true;
                                    CashDispenserWork = false;
                                }
                                lock (DeliveryNominals)
                                    if (CashDeliveryEnabled)
                                        CashDeliveryEnabled = false;
                            }
                            if (CashDispenserLevel != AlarmLevelT.Brown)
                            {
                                if (CashDispenserAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        CashDispenserOldLevel = CashDispenserLevel;
                                        CashDispenserLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenser;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserError;
                                        SendAlarm(Alarm);
                                        CashDispenserAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                    }
                    if (CashDispenserWork)
                    {
                        if (DeliveryUpCount > 20)
                        {
                            if (CashDispenserUpAlarmTime != DateTime.MinValue)
                                CashDispenserUpAlarmTime = DateTime.MinValue;

                            if (CashDispenserUpLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.CashDispenserUp;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("CashDispenserUpWork", DefaultCulture);
                                SendAlarm(Alarm);
                                CashDispenserUpLevel = AlarmLevelT.Green;
                            }
                        }
                        else if (DeliveryUpCount > 0)
                        {
                            ErrorsText += ResourceManager.GetString("CashDispenserUpLowLewel", DefaultCulture) + Environment.NewLine;
                            if (CashDispenserUpLevel != AlarmLevelT.Yellow)
                            {
                                if (CashDispenserUpAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserUpAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserUpAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashDispenserUpLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenserUp;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserUpNearEmpty;
                                        SendAlarm(Alarm);
                                        CashDispenserUpAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorsText += ResourceManager.GetString("CashDispenserUpEmpty", DefaultCulture) + Environment.NewLine;
                            if (CashDispenserUpLevel != AlarmLevelT.Red)
                            {
                                if (CashDispenserUpAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserUpAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserUpAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashDispenserUpLevel = AlarmLevelT.Red;
                                        Alarm.AlarmLevel = AlarmLevelT.Red;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenserUp;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserUpEmpty;
                                        SendAlarm(Alarm);
                                        CashDispenserUpAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        if (DeliveryDownCount > 20)
                        {
                            if (CashDispenserDownAlarmTime != DateTime.MinValue)
                                CashDispenserDownAlarmTime = DateTime.MinValue;
                            if (CashDispenserDownLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.CashDispenserDown;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("CashDispenserDownWork", DefaultCulture);
                                SendAlarm(Alarm);
                                CashDispenserDownLevel = AlarmLevelT.Green;
                            }
                        }
                        else
                        if (DeliveryDownCount > 0)
                        {

                            ErrorsText += ResourceManager.GetString("CashDispenserDownLowLewel", DefaultCulture) + Environment.NewLine;
                            if (CashDispenserDownLevel != AlarmLevelT.Yellow)
                            {
                                if (CashDispenserDownAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserDownAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserDownAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashDispenserDownLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenserDown;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserDownNearEmpty;
                                        SendAlarm(Alarm);
                                        CashDispenserDownAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorsText += ResourceManager.GetString("CashDispenserDownEmpty", DefaultCulture) + Environment.NewLine;
                            if (CashDispenserDownLevel != AlarmLevelT.Red)
                            {
                                if (CashDispenserDownAlarmTime == DateTime.MinValue)
                                {
                                    CashDispenserDownAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashDispenserDownAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashDispenserDownLevel = AlarmLevelT.Red;
                                        Alarm.AlarmLevel = AlarmLevelT.Red;
                                        Alarm.AlarmSource = AlarmSourceT.CashDispenserDown;
                                        Alarm.ErrorCode = AlarmCode.CashDispenserDownEmpty;
                                        SendAlarm(Alarm);
                                        CashDispenserDownAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (CashDispenserWork)
                        CashDispenserWorkChanged = true;
                    CashDispenserWork = false;
                    if (EmulatorEnabled)
                        EmulatorForm.Emulator.CashDispenserGroupBox.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        public void CashDispenserSetSettings()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist)
                {
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                    {
                        lock (CashDispenser)
                        {
                            if (CashDispenser == null)
                            {
                                if (CashDispenserModel == "Multimech")
                                {
                                    CashDispenser = new CashDispenserMultiMech(CashDispenserLock, log);
                                    CashDispenser.Open(CashDispenserPort);
                                }
                                else
                                {
                                    if (CashRecycler == null)
                                        CashRecycler = new CashRecyclerJCM_RC_ID_003(CashDispenserLock, log, DefaultCulture);
                                    CashDispenser = CashRecycler;
                                    if (!CashDispenser.IsOpen)
                                        CashDispenser.Open(CashAcceptorPort);
                                }
                            }
                            ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.CheckStatus");
                            CashDispenser.CheckStatus();
                            ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.OpenCassette");
                            CashDispenser.OpenCassette();
                            ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.ReadCassetteID");
                            CashDispenser.ReadCassetteID();
                            if (CashDispenser.CashDispenserResult.S != CashDispenserStatusT.SuccessfulCommand)
                            {
                                CashDispenser.Pool = false;
                                ToLog(2, "Cashier: CashDispenserSetSettings: CashDispenser.Close");
                                CashDispenser.Close();
                            }
                            else
                            {
                                SetNotesNominal();

                                if (CashDispenser != null)
                                {
                                    CashDispenser.SetCount(1, DeliveryUpCount);
                                    CashDispenser.SetCount(2, DeliveryDownCount);
                                }

                                CashDispenser.Pool = true;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
    }
}