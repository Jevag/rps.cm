﻿using System.Windows.Forms.Integration;
using CommonClassLibrary;
using Communications;
using EventWeb;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Security.Cryptography;
using System.Windows;
using SqlBaseImport.Model;
using System.Threading.Tasks;
using CalculatorTS;
using System.Resources;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Win32;
using System.Windows.Forms;
using Rps.Crypto;
using ConnectFromLevel2;
namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Переменные
        bool Initalized = false;
        public WindowsFormsHost host = new WindowsFormsHost();
        public System.Windows.Forms.Timer ExternalDeviceTimer = new System.Windows.Forms.Timer();

        public ResourceManager ResourceManager;
        MainWindow cashierForm;
        //public CEmulator Emulator;
        public bool? smallTable = false;
        Random rnd;
        //public CMasterController MasterControllerr;
        //public CLCD LCD;
        public CardDispenserMT166 CardDispenser;
        public CommonCardDispenser.DispensStatus CardDispenserStatus, CardDispenserStatusOld;
        public CommonCardReader CardReader;
        public CommonPrinter Printer;

        public CommonKKM KKM;
        //public FprnM1C.FprnM45 KKM;

        public CommonCashAcceptor CashAcceptor;
        public CommonCashDispenserI CashDispenser;
        public CommonCashRecycler CashRecycler;
        public CoinAcceptor CoinAcceptor;
        public CoinHopper CoinHopper;

        public Slave SlaveController;

        //private BaseSync bs = new BaseSync();
        Task SyncTask = null;
        public string conlocal;
        string conserver;
        public CEmulatorForm EmulatorForm;
        public List<string[]> Languages = new List<string[]>();

        public TechFormWPF TechForm;

        internal ResourceManager rm = new ResourceManager("Cashier.Properties.Resources", typeof(Cashier.Properties.Resources).Assembly);

        protected internal bool full_screen = false;
        protected internal bool ShiftOpened;
        protected internal bool ShiftStatusChanged = false;
        protected internal bool EmulatorEnabled;
        public TerminalWorkT TerminalWork = TerminalWorkT.TerminalClosed, TerminalWork_old;

        protected internal TerminalStatusT terminalStatus = TerminalStatusT.Empty,
             terminalStatusOld = TerminalStatusT.Undeifned;
        private PaimentMethodT Paiments = PaimentMethodT.PaimentsAll, DevicePaiments = PaimentMethodT.PaimentsAll;
        public BankModuleStatusT BankModuleStatus = BankModuleStatusT.BankCardEmpty;
        private DeliveryStatusT deliveryStatus = DeliveryStatusT.Delivery;
        public struct CashierTypeS { public string Name; public byte Code; };
        public List<CashierTypeS> CashierTypes = new List<CashierTypeS>();
        public List<string> ChequeTypes = new List<string>();

        private enum crossPay { No, First, Last };
        private crossPay CrossPay;
        private decimal CurrentSumm = 0;

        private bool ExternalDoorOpen;
        private bool ExternalDoorOpenChanged = false;
        private bool InternalDoorOpen;
        private bool InternalDoorOpenChanged = false;
        public bool CashAcceptorWork;
        public bool CashAcceptorWorkChanged = false;
        private bool CashDispenserWork;
        private bool CashDispenserWorkChanged = false;
        private bool CoinAcceptorWork;
        private bool CoinAcceptorWorkChanged = false;
        private bool Hopper1Work;
        private bool Hopper2Work;
        private bool HopperWorkChanged = false;
        private bool BankModuleWork;
        private bool BankModuleWorkChanged = false;
        //private bool BankCardReaderWork;
        //private bool BankCardReaderWorkChanged = false;
        //private bool PayPassWork;
        //private bool PayPassWorkChanged = false;
        private bool CardDispenserWork;
        private bool CardDispenserWorkChanged = false;
        private bool CardReaderWork;
        private bool CardReaderWorkChanged = false;
        private bool Delivery = false;
        private bool TerminalStatusChanged = false;
        private bool SlaveControllerWork;

        private bool CashDispenserUpper = true;
        private bool CashDispenserLower = true;
        private bool CoinDispenser1Hopper = true;
        private bool CoinDispenser2Hopper = true;


        private bool KKMWorkChanged = false;
        public bool KKMWork = false;
        private bool PrinterWorkChanged = false;

        protected internal bool CardReaderStatusChanged = false;
        private bool PaimentsStatusChanged = false;
        private bool DisplayStatusChanged = false;
        private bool BankModuleStatusChanged = false;
        protected internal bool AddingSummChanged = false;
        protected internal int PenalCardTP = 0;
        protected internal int PenalCardTS = 0;
        protected internal bool IsPenalCard = false;

        public bool CashAcceptorInhibit = true, CoinAcceptorInhibit = true;
        private bool CardBalancePos;
        private int ClientType;
        protected internal decimal AmountDue, DownDiff, Paid, PaidBanknote, PaidCoin, CardSum, CardQuery, ResDiff, BankPaid;
        protected internal decimal AddingSumm, BankQuerySumm = 0;
        protected internal bool BankQuerySummRequested = false;

        //private bool DeliveryReturned = false;
        public CashDispenserResultT? MoveForwardResult;
        //private string[] StopList;

        private string ReaderEntranceTime, ReaderZoneTime, ReaderCardBalance, ReaderIDCardHex, ReaderIDCardDec, ReaderCasseKey;
        private byte ReaderTariffID, ReaderSheduleID;

        #region данные с карты
        public ToTariffPlan.DateCard CardInfo;
        public string keyA;
        private byte[] wBlock = new byte[48];               // запись сектора карты
        #endregion

        public int CashAcceptorCurrent, CoinAcceptorCurrent;
        public int DeliveryUpCount, DeliveryDownCount, Delivery1Count, Delivery2Count, RejectCount;

        private List<decimal> CashAcceptorNominals = new List<decimal>();
        private List<decimal> CoinAcceptorNominals = new List<decimal>();

        private Collection<decimal> CashAcceptorPaid = new Collection<decimal>();
        private Collection<decimal> CoinAcceptorPaid = new Collection<decimal>();
        public Collection<ExecutableDeviceT> ExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> CoinAcceptorExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> HopperExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> CashAcceptorExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> CashDispenserExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> BankModuleExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> CardDispenserExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> CardReaderExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> SlaveControllerExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> PrinterExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<ExecutableDeviceT> KKMExecutableDevices = new Collection<ExecutableDeviceT>();
        public Collection<TariffsT> tariffs = new Collection<TariffsT>();
        public Collection<TariffScheduleModelT> TariffSchedule = new Collection<TariffScheduleModelT>();
        public Collection<GroupT> groups = new Collection<GroupT>();
        public Collection<ZoneModelT> zones = new Collection<ZoneModelT>();
        public List<CashAcceptorAllowModelT> CashAcceptorAllows = new List<CashAcceptorAllowModelT>();
        public List<CoinAcceptorAllowModelT> CoinAcceptorAllows = new List<CoinAcceptorAllowModelT>();
        public decimal MinBanknote, MinCoin;
        protected internal string DeliveryStatusLabel, CashAcceptorStatusLabel, CoinAcceptorStatusLabel, PaymentStatusLabel, PaymentStatusLabel1, TerminalStatusLabel,
            EntranceTimeStatusLabel, AmountDueStatusLabel, CasseStatusLabel, AddingSummStatusLabel, BankModuleResultDescription;
        protected bool EndPaymentEnabled = false;
        protected internal DateTime PenalCardRequestTime, DateTimeNull;
        protected internal DateTime? CardEjectTime, LightTime;
        protected internal int PenalCardTimeOut, CardTimeOutToBasket;
        //public Entities db;
        public SqlConnection db;
        SqlConnection upddb;
        public DeviceModelT device = new DeviceModelT();
        private static EventWebServer webServer = null;
        public DeviceCMModelT deviceCMModel;
        public bool terminalStatusChangeInternal = false;
        //public Transactions transactions = null;
        public TransactionsT transactions = null;
        public CardsT card = null;
        public GroupT group;
        public ClientModelT client = null;
        private DateTime datetime0 = new DateTime(2000, 1, 1, 0, 0, 0, 0);  // НУЛЕВОЕ время
        private DateTime datetimeFirst = new DateTime(2014, 1, 1, 0, 0, 0, 0);  // Минимальное время въезда (для калькулятора)
        private DateTime DateExitEstimated1, LastCalcTime;
        private DateTime? DateExitEstimated;

        public int CashAcceptorMaxCount, CashAcceptorMax, CoinAcceptorMax, CoinAcceptorMaxCount;
        decimal CurDenom = 0;
        private string CarNumber;

        bool EventWebServer;

        public TransactionT Transaction;
        public int CurrentTransactionNumber;
        public DateTime? LastShiftOpenTime;
        public bool ShiftFromKKMChecked = false;
        private bool? PayTransactionExported;
        public decimal[] DeliveryBoxesNominals = new decimal[4];
        public struct DeliveryBoxesT { public int Code; public int Count; public bool Work; };
        public DeliveryBoxesT[] DeliveryBoxes = new DeliveryBoxesT[4];
        public decimal UpNominal = 0, DownNominal = 0, Nominal1 = 0, Nominal2 = 0;
        public Aes myAes;
        public AlarmLevelT InternalLevel, LicenseLevel, CardDispenserLevel, CardReaderLevel, CashAcceptorLevel, CoinAcceptorLevel, CashDispenserLevel, CashDispenserOldLevel, CashDispenserUpLevel, CashDispenserDownLevel, CoinHopper1Level, CoinHopper2Level, SlaveLevel, ExternalDoorLevel, InternalDoorLevel, TerminalLevel, KKMLevel, PrinterLevel, BankModuleLevel, CashDispenser1Level, PaymentErrorLevel;

        string d_format = "dd.MM.yyyy";
        string dt_format = "dd.MM.yyyy HH:mm";
        string ldt_format = "yyyy'/'MM'/'dd HH:mm:ss.fff";

        public string Country;
        public string DefaultLanguage;
        public int LanguageTimeOut;
        private DateTime? LanguageChangeTime;
        public string Valute;
        public List<BanknoteModelT> Banknotes = new List<BanknoteModelT>();
        public List<CoinModelT> Coins = new List<CoinModelT>();

        public int ZReportCount = 25;
        private byte KKMCommError = 0;
        private byte KKMError = 0;
        private byte CashAcceptorCommError = 0;
        private byte CashDispenserCommError = 0;
        //private byte CoinAcceptorCommError = 0;
        //private byte Hopper1CommError = 0;
        //private byte Hopper2CommError = 0;
        //private byte SlaveCommError = 0;
        private byte CardReaderCommError = 0;
        //private byte CardDispenserCommError = 0;
        private byte BankModuleCommError = 0;

        bool _ExternalDoorOpen0 = false, _InternalDoorOpen0 = false, _ExternalDoorOpen1 = false, _InternalDoorOpen1 = false;
        private decimal SumDelivery;
        private PaymentErrorInfoT PaymentErrorInfo;
        public DateTime SettingsLastUpdate, CheckSettingsUpdate, CardStatusToServerUpdate, CallUpdate, CallStart;
        public bool TSExists = false;
        public int TSRulesCount = 0;
        private ToTariffPlan CalculatorTS;
        private List<TariffPlanTariffScheduleModelT> TariffPlanTariffScheduleList;
        private decimal Balance = 0;
        GridLength GridLength0 = new GridLength(0);
        GridLength GridLength1 = new GridLength(1, GridUnitType.Star);
        GridLength GridLength3 = new GridLength(3, GridUnitType.Star);
        GridLength GridLength5 = new GridLength(5, GridUnitType.Star);
        GridLength GridLength50 = new GridLength(50, GridUnitType.Star);
        string CashLabel = "", BankLabel = "";
        public OperatorTalk Talk;
        private Guid? ToAbonement;
        private Guid? TarifiTP;
        private Guid? TarifiTS;
        private string TarifiTPName;
        private string SignatureSlave = "";          // SignatureSlave текстовый 
        public byte SectorNumber = 1;
        //private DateTime LastSignatureTime = new DateTime(2000, 1, 1, 0, 0, 0, 0);     //  время последнего чтения SignatureSlave;
        //private int SignatureTime = 2 * 60 * 60;          // период опроса SignatureSlave в секундах
        private string versionSlave = "";
        private int comDiskret = 0;
        private bool diskret_check0 = true;
        private bool LicenseWork = false/*, DebugMode = false*/;
        private int FreeTime, FreeTimeTypeId;
        private DateTime? LastTopMost;
        private bool ChequePrinted = false, Cheque1Printed = false, Cheque2Printed = false;
        private DateTime? ChequeDT;
        public object db_lock = new object();
        public object CashAcceptorLock = new object();
        public object CashDispenserLock = new object();
        public object DeliveryNominals = new object();
        public bool CoinDeliveryEnabled = false, FrontBoxEnabled = false, BackBoxEnabled = false;
        public bool CashDeliveryEnabled = false;
        public object loglock = new object();

        public AllPayParams? Params;
        public List<string> Items = new List<string>();
        public CommonClassLibrary.Log log;
        DateTime? KKMStatusCheckDT = null, CardDispenserStatusCheckDT = null, CardReaderStatusCheckDT = null, CashDispenserStatusCheckDT = null,
            CashAcceptorStatusCheckDT = null, CoinAcceptorStatusCheckDT = null, CoinHopperStatusCheckDT = null, PrinterStatusCheckDT = null;
        int DeviceType = 2;
        public Guid DeviceId;
        CommonKKM.ErrorCode KKMResultCode = CommonKKM.ErrorCode.Sucsess;
        CommonPrinter.ErrorCode PrinterResultCode = CommonPrinter.ErrorCode.Sucsess;
        public bool DeliveryUpCountChanged = false;
        public bool DeliveryDownCountChanged = false;
        public bool DeliveryLeftCountChanged = false;
        public bool DeliveryRightCountChanged = false;
        public bool CashAcceptorCountChanged = false;
        string KKMResultDescription = "", PrinterResultDescription = "";
        //bool CardDispensed = false;
        bool CashNominalsChanged = true, CoinNominalsChanged = true;
        public bool ShiftWork = true;
        int CardReadCount = 0;
        public CultureInfo DefaultCulture;
        public string KKMPassword;
        public string KKMModePassword;
        public string CoinAcceptorPIN;
        public bool WindowClosing = false;
        public string CashAcceptorPort, CashDispenserPort, CoinAcceptorPort, CoinHopperPort,
            CardReaderPort, CardDispenserPort, KKMPort, PrinterPort, SlaveControllerPort;
        public string CashAcceptorModel, CashDispenserModel, CoinAcceptorModel, CoinHopperModel, BankModuleModel,
            CardReaderModel, CardDispenserModel, KKMModel, PrinterModel, SlaveControllerModel;
        bool[] CoinsAccepted = new bool[] { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
        //bool[] CoinsAccepted = new bool[] { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };
        //bool[] CashAccepted = new bool[] { true, true, true, true, true, true };
        //                                 10    50    100   500   1000  5000
        int CashAcceptedCode = 0;
        bool GiveChequeStatus = false, GiveDeliveryStatus = false;
        Thread GiveDeliveryThread, GiveChequeThread;
        decimal ThreadDeliveryDiff = 0;
        string ThreadDeliveryToLog = "";
        decimal ThreadDeliveryResDiff = 0;
        bool IsCancelled = false;
        private string EntitiesCS;
        bool UpdateNeeded = false;
        //Task Updater;
        public Queue<string> ConsoleLog = new Queue<string>();
        public bool ConsoleLogChanged = false;
        public bool ToggleTechForm = false;
        public byte CashierType = 0; //0 - Обычная касса, 1 - Касса Лайт, 2 - Ручная Касса.
        public List<DevicesLanguagesT> DevicesLanguages = new List<DevicesLanguagesT>();
        DataRow deviceCM;

        TransactionTypeT CurrentTransactionType = TransactionTypeT.Undefined;
        public string ToLogCardReaded = "", ToLogCardCalculated = "", ToLogEntranceTime = "", ToLogRecountTime = "", ToLogTariffID = "", ToLogRegular = "",
            ToLogTariffSheduleID = "", ToLogZoneid = "", ToLogClientGroupid = "", ToLogSumCard = "", ToLogNulltime1Time = "", ToLogNulltime2Time = "",
            ToLogNulltime3Time = "", ToLogTVP = "", ToLogTKVP = "", ToLogAmountSumCard = "", ToLogCardDebt = "", ToLogCardBalance = "", sValuteLabel = "",
            sCardDispenserWork = "", ToLogBankRequest = "", ToLogIDCard = "", ToLogCardInStopList = "", ToLogDateSaveCard = "", ToLogCardsOnline = "", ToLogLastPaymentTime = "", ToLogWriteCard="",
            ToLogToCardsOnline = "";

        private string conlocal0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";
        private string conserver0 = "Data Source=; Initial Catalog=rpsMDBG; User Id=sa; Password=";


        RegistryKey currRegistryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE\\RPS");

        NotifyIcon CashierIcon;
        ContextMenuStrip NotifyIconСontextMenu = new ContextMenuStrip();
        ToolStripMenuItem menuItemExit = new ToolStripMenuItem("E&xit");
        ToolStripMenuItem menuTechForm = new ToolStripMenuItem("&TechForm");
        //public bool ReSyncReq;
        public int LastZReport = 0;
        public CommonBankModule BankModule;
        object ExternalDeviceTimerLock = new object();
        List<string> BankSlip = new List<string>();
        bool WorkKeyLoaded = false;
        DateTime BankCardErrorDateTime, WorkKeyCheckTime;
        string ErrorsText = "";
        public string ErrorsTextLast = "";
        public bool ErrorsTextChanged = false;
        SoftReaderStatusT CardReaderSoftStatus = SoftReaderStatusT.Unknown;
        CommonCashAcceptorStatusT CashAcceptorStatus = CommonCashAcceptorStatusT.Unknown;
        public CryptoDecrypto cryp;
        UDPListener ConnectFromLevel2 = new UDPListener();
        NumberFormatInfo nfi;
        public bool CashDispenserResetNeeded = false;
        string BanqQueryStatusLabel = "";
        GridLength GL0 = new GridLength(0);
        GridLength GL20S = new GridLength(20, GridUnitType.Star);
        GridLength GL50S = new GridLength(50, GridUnitType.Star);
        GridLength GL100S = new GridLength(100, GridUnitType.Star);
        GridLength GL1P = new GridLength(1, GridUnitType.Pixel);
        GridLength GL10P = new GridLength(10, GridUnitType.Pixel);
        GridLength GL15P = new GridLength(15, GridUnitType.Pixel);
        GridLength GL30P = new GridLength(30, GridUnitType.Pixel);
        GridLength GL50P = new GridLength(50, GridUnitType.Pixel);
        GridLength GL20P = new GridLength(20, GridUnitType.Pixel);
        GridLength GL27P = new GridLength(27, GridUnitType.Pixel);
        GridLength GL29P = new GridLength(29, GridUnitType.Pixel);
        GridLength GL35P = new GridLength(35, GridUnitType.Pixel);
        GridLength GL40P = new GridLength(40, GridUnitType.Pixel);
        GridLength GL140P = new GridLength(140, GridUnitType.Pixel);
        GridLength GL280P = new GridLength(280, GridUnitType.Pixel);
        GridLength GL1Auto = new GridLength(1, GridUnitType.Auto);
        bool RegularGridVisibble = false;
        public ExecutableDeviceType CashAcceptorExecutableDeviceType;
        bool CardDispenserCardInsertEnabled = false;
        int CurrentVersion;
        public bool DetailLog = false;
        #endregion Переменные
        //const int time_diskret = 20;        // время таймера обмена с SlaveController * 50 мс 
    }
}
