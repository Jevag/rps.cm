using CommonClassLibrary;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using Rps.ShareBase;
using SqlBaseImport.Model;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //bool vPaymentCashOn = false;
        bool vPaymentBankOn = false;
        void SetCashPayment()
        {
            ToLog(2, "Cashier: SetCashPayment Enter DevicePaiments=" + DevicePaiments.ToString());
            if (vPaymentBankOn)
                PaymentBankCardOff();
            if (DevicePaiments != PaimentMethodT.PaimentsBankModule)
            {
                if (DevicePaiments != PaimentMethodT.PaimentsCash)
                    DevicePaiments = PaimentMethodT.PaimentsCash;
                //if (!vPaymentCashOn)
                PaymentCashOn();
            }
            else
            {
                //if (vPaymentCashOn)
                PaymentCashOff();
            }
            if (Paiments != DevicePaiments)
            {
                ToLog(2, "Cashier: SetCashPayment: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                Paiments = DevicePaiments;
                if (EmulatorEnabled)
                    EmulatorForm.Emulator.Paiments = DevicePaiments;
            }
            PaimentsStatusChanged = true;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void SetBankCardPayment()
        {
            ToLog(2, "Cashier: SetBankCardPayment Enter DevicePaiments=" + DevicePaiments.ToString());
            //if (vPaymentCashOn)
            PaymentCashOff();
            if (DevicePaiments != PaimentMethodT.PaimentsBankModule)
                DevicePaiments = PaimentMethodT.PaimentsBankModule;
            if (Paiments != DevicePaiments)
            {
                ToLog(2, "Cashier: SetBankCardPayment: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                Paiments = DevicePaiments;
                if (EmulatorEnabled)
                    EmulatorForm.Emulator.Paiments = DevicePaiments;
            }
            PaimentsStatusChanged = true;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void PaymentCashOn()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked))
            {
                if (CashAcceptorWork)
                {
                    CashAcceptor.InhibitStatusReq();
                    CashAcceptorInhibit = CashAcceptor.Inhibit;
                    if (CashAcceptorInhibit && (CashAcceptor != null) && CashAcceptor.IsOpen /*&& (CashAcceptor.InhibitData == 1)*/)
                    {
                        ToLog(2, "Cashier: PaymentCashOn: CashAcceptor.InhibitOff");
                        CashAcceptor.InhibitOff();
                        CashAcceptorInhibit = CashAcceptor.Inhibit;
                    }
                }
            }
            if (CashierType == 0)
            {
                if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CoinAcceptorVirt.Checked))
                {
                    if (CoinAcceptorWork)
                    {
                        var res = CoinAcceptor.GetStatus();
                        if (res != 0) return;

                        CoinAcceptorInhibit = CoinAcceptor.Inhibit;
                        if (CoinAcceptorInhibit && (CoinAcceptor != null))
                        {
                            ToLog(2, "Cashier: PaymentCashOn: CoinAcceptor.StartPoll");
                            CoinAcceptor.StartPoll();
                            CoinAcceptorInhibit = CoinAcceptor.Inhibit;
                        }
                    }
                }
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void PaymentCashOff()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked))
            {
                if (CashAcceptorWork)
                {
                    CashAcceptor.InhibitStatusReq();
                    CashAcceptorInhibit = CashAcceptor.Inhibit;

                    if (!CashAcceptorInhibit && (CashAcceptor != null) && CashAcceptor.IsOpen /*&& (CashAcceptor.InhibitData == 0)*/)
                    {
                        ToLog(2, "Cashier: PaymentCashOff: CashAcceptor.InhibitOn");
                        CashAcceptor.InhibitOn();
                        CashAcceptorInhibit = CashAcceptor.Inhibit;
                    }
                }
            }
            if (CashierType == 0)
            {
                if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CoinAcceptorVirt.Checked))
                {
                    if (CoinAcceptorWork)
                    {
                        CoinAcceptorInhibit = CoinAcceptor.Inhibit;
                        if (!CoinAcceptorInhibit && (CoinAcceptor != null))
                        {
                            ToLog(2, "Cashier: PaymentCashOff: CoinAcceptor.StopPoll");
                            CoinAcceptor.StopPoll();
                            CoinAcceptorInhibit = CoinAcceptor.Inhibit;
                        }
                    }
                }
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        void PaymentBankCardOn()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            /*if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist && (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked))
            {
            }*/
            vPaymentBankOn = true;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        void PaymentBankCardOff()
        {
            string status = "";
            ToLog(2, "Cashier: PaymentBankCardOff DevicePaiments=" + DevicePaiments.ToString());
            BankQuerySummRequested = false;
            if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist && (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked))
            {
                if (BankModule != null && BankModuleWork && /*!BankModule.IsCancell &&*/ (BankModule.CommandStatus == CommandStatusT.Run || BankModule.CommandStatus == CommandStatusT.Running))
                {
                    ToLog(2, "Cashier: PaymentBankCardOff call BankModule.GetOpStatus(true)");
                    status = BankModule.GetOpStatus(true);
                    ToLog(2, "Cashier:  PaymentBankCardOff BankModule.GetOpStatus(true) result= " + status);
                    if (BankModule.CardInserted || BankModule.CardWasInserted || BankModule.AuthorizationStarted)
                    {
                        DevicePaiments = PaimentMethodT.PaimentsBankModule;
                    }
                    else
                    {
                        if (DevicePaiments == PaimentMethodT.PaimentsBankModule)
                            DevicePaiments = PaimentMethodT.PaimentsAll;
                        vPaymentBankOn = false;
                    }
                    if (Paiments != DevicePaiments)
                    {
                        ToLog(2, "Cashier: PaymentBankCardOff: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                        Paiments = DevicePaiments;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.Paiments = DevicePaiments;
                    }
                    ToLog(2, "Cashier:  PaymentBankCardOff BankModule.GetOpStatus() result= " + status + " DevicePaiments=" + DevicePaiments.ToString());
                }
                else
                {
                    BankQuerySummRequested = false;
                    vPaymentBankOn = false;
                }
            }
            else
            {
                BankQuerySummRequested = false;
                vPaymentBankOn = false;
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void PaymentOff()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            PaymentCashOff();
            if (vPaymentBankOn)
                PaymentBankCardOff();
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void PaymentOn()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (Paiments == PaimentMethodT.PaimentsBankModule)
            {
                if (!vPaymentBankOn)
                    PaymentBankCardOn();
            }
            else if (Paiments == PaimentMethodT.PaimentsCash)
            {
                //if (!vPaymentCashOn)
                PaymentCashOn();
            }
            else
            {
                //if (!vPaymentCashOn)
                PaymentCashOn();
                if (!vPaymentBankOn)
                    PaymentBankCardOn();
            }

            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void SetDeliveryNominals()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                DeliveryBoxes[0].Code = 0; //UpNominal ��������
                DeliveryBoxes[1].Code = 1; //DownNominal ��������
                lock (DeliveryNominals)
                    if (CashDeliveryEnabled)
                    {
                        DeliveryBoxes[0].Count = DeliveryUpCount;
                        DeliveryBoxes[0].Work = CashDispenserUpper;
                        DeliveryBoxes[1].Count = DeliveryDownCount;
                        DeliveryBoxes[1].Work = CashDispenserLower;
                        DeliveryBoxesNominals[0] = UpNominal;
                        DeliveryBoxesNominals[1] = DownNominal;
                    }
                    else
                    {
                        DeliveryBoxes[0].Count = 0;
                        DeliveryBoxesNominals[0] = 9999999999;
                        DeliveryBoxes[1].Count = 0;
                        DeliveryBoxesNominals[1] = 9999999999;
                        DeliveryBoxes[0].Work = false;
                        DeliveryBoxes[1].Work = false;
                    }
                DeliveryBoxes[2].Code = 2; //LeftNominal ������
                lock (DeliveryNominals)
                    if (CoinDeliveryEnabled)
                    {
                        DeliveryBoxes[2].Count = Delivery1Count;
                        DeliveryBoxesNominals[2] = Nominal1;
                        DeliveryBoxes[2].Work = CoinDispenser1Hopper;
                    }
                    else
                    {
                        DeliveryBoxes[2].Count = 0;
                        DeliveryBoxes[2].Work = false;
                        DeliveryBoxesNominals[2] = 9999999999;
                    }
                DeliveryBoxes[3].Code = 3; //RightNominal ������
                lock (DeliveryNominals)
                    if (CoinDeliveryEnabled)
                    {
                        DeliveryBoxes[3].Count = Delivery2Count;
                        DeliveryBoxesNominals[3] = Nominal2;
                        DeliveryBoxes[3].Work = CoinDispenser2Hopper;
                    }
                    else
                    {
                        DeliveryBoxes[3].Count = 0;
                        DeliveryBoxes[3].Work = false;
                        DeliveryBoxesNominals[3] = 9999999999;
                    }
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        private void GiveDelivery(decimal Diff, string toLog)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            GiveDeliveryStatus = true;
            ThreadDeliveryDiff = Diff;
            ThreadDeliveryToLog = toLog;
            GiveDeliveryThread = new Thread(GiveDelivery);
            GiveDeliveryThread.Name = "GiveDeliveryThread";
            GiveDeliveryThread.IsBackground = true;
            GiveDeliveryThread.Start();
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }


        private void GiveDelivery()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            bool _CashDispenserUpper = CashDispenserUpper;
            bool _CashDispenserLower = CashDispenserLower;
            bool _CoinDispenser1Hopper = CoinDispenser1Hopper;
            bool _CoinDispenser2Hopper = CoinDispenser2Hopper;
            if (CashDispenser != null)
                CashDispenser.Pool = false;
            try
            {
                CashDispenserResultT CheckDeliveredNotesResult;
                string s = "";

                int Quotient; // ��������� �������������� �������
                ThreadDeliveryResDiff = ThreadDeliveryDiff;
                decimal NeededDelivery = ThreadDeliveryDiff;
                int Boxes = 4;

                decimal Delivered = 0;

                int CurrentBox;
                int DeliveryUp = 0, DeliveryDown = 0, DeliveryLeft = 0, DeliveryRight = 0;



                //TransactionClear();

                //ToLog(2, "Cashier: GiveDelivery(): SetDeliveryNominals");
                SetDeliveryNominals();
                Array.Sort(DeliveryBoxesNominals, DeliveryBoxes);

                for (int d = 0; d < 4; d++)
                {
                    if (!DeliveryBoxes[d].Work || (DeliveryBoxesNominals[d] == 0) || (DeliveryBoxes[d].Count == 0))
                        Boxes--;
                }

                for (int d = 0; d < Boxes; d++)
                {
                    NeededDelivery = ThreadDeliveryResDiff;

                    //ToLog(2, "Cashier: GiveDelivery(): SetDeliveryNominals");
                    SetDeliveryNominals();
                    Array.Sort(DeliveryBoxesNominals, DeliveryBoxes);

                    do
                    {
                        Delivered = 0;
                        DeliveryUp = 0;
                        DeliveryDown = 0;
                        DeliveryLeft = 0;
                        DeliveryRight = 0;

                        NeededDelivery = ThreadDeliveryResDiff;

                        for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                        {
                            if ((DeliveryBoxesNominals[CurrentBox] != 0) && DeliveryBoxes[CurrentBox].Work)
                            {
                                Quotient = (int)(NeededDelivery / DeliveryBoxesNominals[CurrentBox]);
                                if (Quotient > DeliveryBoxes[CurrentBox].Count)
                                    Quotient = DeliveryBoxes[CurrentBox].Count;
                                Delivered = Quotient * DeliveryBoxesNominals[CurrentBox];
                                switch (DeliveryBoxes[CurrentBox].Code)
                                {
                                    case 0:
                                        if (_CashDispenserUpper)
                                        {
                                            if (Quotient > 0)
                                            {
                                                s = string.Format(ResourceManager.GetString("ToLogDeliveryUp", DefaultCulture), DeliveryBoxesNominals[CurrentBox].ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), Quotient.ToString(), ResourceManager.GetString("Pieces", DefaultCulture));
                                                //                                DeliveryUpCount = DeliveryBoxes[CurrentBox].DeliveryBoxesCount;
                                                DeliveryUp = Quotient;
                                            }
                                        }
                                        break;
                                    case 1:
                                        if (_CashDispenserLower)
                                        {
                                            if (Quotient > 0)
                                            {
                                                s = string.Format(ResourceManager.GetString("ToLogDeliveryDown", DefaultCulture), DeliveryBoxesNominals[CurrentBox].ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), Quotient.ToString(), ResourceManager.GetString("Pieces", DefaultCulture));
                                                //                                DeliveryDownCount = DeliveryBoxes[CurrentBox].DeliveryBoxesCount;
                                                DeliveryDown = Quotient;
                                            }
                                        }
                                        break;
                                    case 2:
                                        if (_CoinDispenser1Hopper)
                                        {
                                            if (Quotient > 0)
                                            {
                                                s = string.Format(ResourceManager.GetString("ToLogDeliveryFront", DefaultCulture), DeliveryBoxesNominals[CurrentBox].ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), Quotient.ToString(), ResourceManager.GetString("Pieces", DefaultCulture));
                                                //                                DeliveryLeftCount = DeliveryBoxes[CurrentBox].DeliveryBoxesCount;
                                                DeliveryLeft = Quotient;
                                            }
                                        }
                                        break;
                                    case 3:
                                        if (_CoinDispenser2Hopper)
                                        {
                                            if (Quotient > 0)
                                            {
                                                s = string.Format(ResourceManager.GetString("ToLogDeliveryBack", DefaultCulture), DeliveryBoxesNominals[CurrentBox].ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), Quotient.ToString(), ResourceManager.GetString("Pieces", DefaultCulture));
                                                //                                DeliveryRightCount = DeliveryBoxes[CurrentBox].DeliveryBoxesCount;
                                                DeliveryRight = Quotient;
                                            }
                                        }
                                        break;
                                }
                                if (s != "")
                                    ThreadDeliveryToLog += Environment.NewLine + s;
                                s = "";
                            }
                            NeededDelivery = NeededDelivery - Delivered;
                            if (NeededDelivery == 0)
                                break;
                        }
                        //            ToLog(ThreadDeliveryToLog);
                        if ((DeliveryUp + DeliveryDown) > 0)
                        {
                            if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                            { //Hard Dispenser
                              //                int DeliveredUp = 0, DeliveredDown = 0;
                              //lock (CashDispenserLock)
                                {
                                    if (DeliveryUp > 50)
                                        DeliveryUp = 50;
                                    if (DeliveryDown > 50)
                                        DeliveryDown = 50;
                                    ToLog(2, "Cashier: GiveDelivery(): CashDispenser.MoveForward");
                                    CashDispenser.MoveForward(DeliveryUp, DeliveryDown);

                                    MoveForwardResult = CashDispenser.CashDispenserResult;
                                    if (((CashDispenserResultT)MoveForwardResult).Cassettes != null)
                                    {
                                        for (int i = 0; i < ((CashDispenserResultT)MoveForwardResult).Cassettes.Length; i++)
                                        {
                                            switch (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperNumber)
                                            {
                                                case "0":
                                                    Transaction.IssuedNotesCount += ((CashDispenserResultT)MoveForwardResult).Cassettes[i].Count;
                                                    break;
                                                case "1":
                                                    Transaction.IssuedFromUpper += ((CashDispenserResultT)MoveForwardResult).Cassettes[i].Count;
                                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                        if (DeliveryBoxes[CurrentBox].Code == 0) // Upper
                                                        {
                                                            DeliveryBoxes[CurrentBox].Count -= ((CashDispenserResultT)MoveForwardResult).Cassettes[i].Count;
                                                            break;
                                                        }
                                                    ToLog(1, string.Format("Cashier: GiveDelivery: MoveForwardResult.Cassettes[{0}].HopperStatus={1}", i.ToString(), ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus.ToString()));
                                                    if (
                                                        (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                        (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                        )
                                                    {
                                                        if (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus == CashDispenserStatusT.EmptyCassette /*||
                                                            ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus == CashDispenserStatusT.FailureToFeed*/)
                                                        {
                                                            ToLog(1, "Cashier: GiveDelivery: DeliveryUpCount Empty");
                                                            //DeliveryUpCount = 0;
                                                            DeliveryBoxes[CurrentBox].Count = 0;
                                                            CashDispenser.SetCount(1, 0);
                                                        }
                                                        if (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.Full /*&&
                                                            ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.EmptyCassette*/)
                                                        {
                                                            DeliveryBoxes[CurrentBox].Work = false;
                                                            _CashDispenserUpper = false;
                                                        }
                                                    }
                                                    break;
                                                case "2":
                                                    Transaction.IssuedFromLower += ((CashDispenserResultT)MoveForwardResult).Cassettes[i].Count;
                                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                        if (DeliveryBoxes[CurrentBox].Code == 1) //Lower
                                                        {
                                                            DeliveryBoxes[CurrentBox].Count -= ((CashDispenserResultT)MoveForwardResult).Cassettes[i].Count;
                                                            break;
                                                        }
                                                    ToLog(1, string.Format("Cashier: GiveDelivery: MoveForwardResult.Cassettes[{0}].HopperStatus={1}", i.ToString(), ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus.ToString()));
                                                    if (
                                                        (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                        (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                        )
                                                    {
                                                        if (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus == CashDispenserStatusT.EmptyCassette /*||
                                                            ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus == CashDispenserStatusT.FailureToFeed*/)
                                                        {
                                                            ToLog(1, "Cashier: GiveDelivery: DeliveryDownCount Empty");
                                                            //DeliveryDownCount = 0;
                                                            DeliveryBoxes[CurrentBox].Count = 0;
                                                            CashDispenser.SetCount(2, 0);
                                                        }
                                                        if (((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.Full /*&&
                                                            ((CashDispenserResultT)MoveForwardResult).Cassettes[i].HopperStatus != CashDispenserStatusT.EmptyCassette*/)
                                                        {
                                                            DeliveryBoxes[CurrentBox].Work = false;
                                                            _CashDispenserLower = false;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    ToLog(2, "Cashier: GiveDelivery(): CashDispenser.CheckDeliveredNotes");
                                    CashDispenser.CheckDeliveredNotes();
                                    CheckDeliveredNotesResult = CashDispenser.CashDispenserResult;
                                    if (CheckDeliveredNotesResult.Cassettes != null)
                                    {
                                        for (int i = 0; i < CheckDeliveredNotesResult.Cassettes.Length; i++)
                                        {
                                            switch (CheckDeliveredNotesResult.Cassettes[i].HopperNumber)
                                            {
                                                case "0":
                                                    Transaction.DeliveredNotesCount += CheckDeliveredNotesResult.Cassettes[i].Count;
                                                    break;
                                                case "1":
                                                    Transaction.DeliveredFromUpper += CheckDeliveredNotesResult.Cassettes[i].Count;
                                                    if (DeliveryUp < CheckDeliveredNotesResult.Cassettes[i].Count)
                                                        _CashDispenserUpper = false;
                                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                        if (DeliveryBoxes[CurrentBox].Code == 0)
                                                        {
                                                            ThreadDeliveryResDiff -= CheckDeliveredNotesResult.Cassettes[i].Count * DeliveryBoxesNominals[CurrentBox];
                                                            break;
                                                        }
                                                    if (
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.Full) &&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.SuccessfulCommand) /*&&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.EmptyCassette)*/
                                                        )
                                                        _CashDispenserUpper = false;
                                                    break;
                                                case "2":
                                                    Transaction.DeliveredFromLower += CheckDeliveredNotesResult.Cassettes[i].Count;
                                                    if (DeliveryDown < CheckDeliveredNotesResult.Cassettes[i].Count)
                                                        CashDispenserLower = false;
                                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                        if (DeliveryBoxes[CurrentBox].Code == 1)
                                                        {
                                                            ThreadDeliveryResDiff -= CheckDeliveredNotesResult.Cassettes[i].Count * DeliveryBoxesNominals[CurrentBox];
                                                            break;
                                                        }
                                                    if (
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.Full) &&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.SuccessfulCommand) /*&&
                                                        (CheckDeliveredNotesResult.Cassettes[i].HopperStatus != CashDispenserStatusT.EmptyCassette)*/
                                                        )
                                                        _CashDispenserLower = false;
                                                    break;
                                            }
                                        }
                                    }
                                    if (((CashDispenserResultT)MoveForwardResult).S != CashDispenserStatusT.SuccessfulCommand)
                                    {
                                        DeliveryUp = 0;
                                        DeliveryDown = 0;
                                    }

                                    if (
                                        (((CashDispenserResultT)MoveForwardResult).S == CashDispenserStatusT.FailureToFeed) ||
                                        (((CashDispenserResultT)MoveForwardResult).S == CashDispenserStatusT.SuccessfulCommand)
                                       )
                                        MoveForwardResult = null;
                                    //s = String.Format("Delivered All: {0} Delivered Upper: {1} Delivered Down: {2}", Transaction.DeliveredNotesCount, Transaction.DeliveredFromUpper, Transaction.DeliveredFromLower);
                                    //ThreadDeliveryToLog += Environment.NewLine + s;
                                }
                            }
                            else
                            { // Soft Dispenser
                                Transaction.IssuedFromUpper = DeliveryUp;
                                Transaction.IssuedFromLower = DeliveryDown;
                                Transaction.DeliveredNotesCount = DeliveryUp + DeliveryDown;
                                Transaction.DeliveredFromUpper = DeliveryUp;
                                Transaction.DeliveredFromLower = DeliveryDown;
                                for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                {
                                    switch (DeliveryBoxes[CurrentBox].Code)
                                    {
                                        case 0:
                                            DeliveryBoxes[CurrentBox].Count -= DeliveryUp;
                                            ThreadDeliveryResDiff -= DeliveryUp * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                        case 1:
                                            DeliveryBoxes[CurrentBox].Count -= DeliveryDown;
                                            ThreadDeliveryResDiff -= DeliveryDown * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                    }
                                }
                            }
                        }
                        if (DeliveryLeft > 0)
                        {
                            if (!EmulatorEnabled || !EmulatorForm.Emulator.HopperVirt1.Checked)
                            { // Hard 1 Hopper
                              // ToDo: ������ ������ �� ������� �������
                                if (DeliveryLeft > 255)
                                    DeliveryLeft = 255;
                                ToLog(2, "Cashier: GiveDelivery(): CoinHopper.MoveForward(" + DeliveryLeft.ToString() + ", 0)");
                                if (CoinHopper.MoveForward(DeliveryLeft, 0))
                                {

                                    Thread.Sleep(100);

                                    while (CoinHopper.CommandStatus != CommandStatusT.Completed)
                                        Thread.Sleep(200);
                                    Thread.Sleep(100);

                                    Transaction.IssuedFrom1Hopper += CoinHopper.HoppertStatus[0].LastPaidout;
                                    Transaction.DeliveredFrom1Hopper += CoinHopper.HoppertStatus[0].LastPaidout;
                                    Transaction.DeliveredCoinsCount += CoinHopper.HoppertStatus[0].LastPaidout;
                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                        if (DeliveryBoxes[CurrentBox].Code == 2) // Front
                                        {
                                            DeliveryBoxes[CurrentBox].Count -= CoinHopper.HoppertStatus[0].LastPaidout;
                                            ThreadDeliveryResDiff -= CoinHopper.HoppertStatus[0].LastPaidout * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                        }

                                    if (CoinHopper.HoppertStatus[0].Unpaid > 0)
                                    {
                                        _CoinDispenser1Hopper = false;
                                        if (CoinHopper.HoppertStatus[0].PayoutTimeout)
                                        {
                                            for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                if (DeliveryBoxes[CurrentBox].Code == 2) // Front
                                                {
                                                    DeliveryBoxes[CurrentBox].Count = 0;
                                                    break;
                                                }
                                            Thread.Sleep(100);
                                            CoinHopper.Reset(false);
                                            Thread.Sleep(100);
                                        }
                                    }
                                }
                                else
                                {
                                    _CoinDispenser1Hopper = false;
                                }
                            }
                            else
                            { //Soft 1 Hopper
                                Transaction.IssuedFrom1Hopper += DeliveryLeft;
                                Transaction.DeliveredFrom1Hopper += DeliveryLeft;
                                Transaction.DeliveredCoinsCount += DeliveryLeft;
                                for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                {
                                    switch (DeliveryBoxes[CurrentBox].Code)
                                    {
                                        case 2:
                                            DeliveryBoxes[CurrentBox].Count -= Transaction.DeliveredFrom1Hopper;
                                            ThreadDeliveryResDiff -= Transaction.DeliveredFrom1Hopper * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                    }
                                }
                            }
                        }
                        if (DeliveryRight > 0)
                        {
                            if (!EmulatorEnabled || !EmulatorForm.Emulator.HopperVirt2.Checked)
                            {// Hard 2 Hopper
                                if (DeliveryRight > 255)
                                    DeliveryRight = 255;
                                ToLog(2, "Cashier: GiveDelivery(): CoinHopper.MoveForward(0, " + DeliveryRight.ToString() + ")");
                                if (CoinHopper.MoveForward(0, DeliveryRight))
                                {
                                    Thread.Sleep(100);

                                    while (cashierForm.CoinHopper.CommandStatus != CommandStatusT.Completed)
                                        Thread.Sleep(200);
                                    Thread.Sleep(100);
                                    Transaction.IssuedFrom2Hopper += CoinHopper.HoppertStatus[1].LastPaidout;
                                    Transaction.DeliveredFrom2Hopper += CoinHopper.HoppertStatus[1].LastPaidout;
                                    Transaction.DeliveredCoinsCount += CoinHopper.HoppertStatus[1].LastPaidout;
                                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                        if (DeliveryBoxes[CurrentBox].Code == 3) // Back
                                        {
                                            DeliveryBoxes[CurrentBox].Count -= CoinHopper.HoppertStatus[1].LastPaidout;
                                            ThreadDeliveryResDiff -= CoinHopper.HoppertStatus[1].LastPaidout * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                        }
                                    if (CoinHopper.HoppertStatus[1].Unpaid > 0)
                                    {
                                        _CoinDispenser2Hopper = false;
                                        if (CoinHopper.HoppertStatus[1].PayoutTimeout)
                                        {
                                            for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                                if (DeliveryBoxes[CurrentBox].Code == 3) // Back
                                                {
                                                    DeliveryBoxes[CurrentBox].Count = 0;
                                                    break;
                                                }
                                            Thread.Sleep(100);
                                            CoinHopper.Reset(true);
                                            Thread.Sleep(100);
                                        }
                                    }
                                }
                                else
                                {
                                    _CoinDispenser2Hopper = false;
                                }
                            }
                            else
                            {// Soft 2 Hopper
                                Transaction.IssuedFrom2Hopper = DeliveryRight;
                                Transaction.DeliveredFrom2Hopper = DeliveryRight;
                                Transaction.DeliveredCoinsCount += DeliveryRight;
                                for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                                {
                                    switch (DeliveryBoxes[CurrentBox].Code)
                                    {
                                        case 3:
                                            DeliveryBoxes[CurrentBox].Count -= Transaction.DeliveredFrom2Hopper;
                                            ThreadDeliveryResDiff -= Transaction.DeliveredFrom2Hopper * DeliveryBoxesNominals[CurrentBox];
                                            break;
                                    }
                                }
                            }
                        }

                        if ((DeliveryUp > 0) && (DeliveryUpCount <= 0))
                            break;
                        if ((DeliveryDown > 0) && (DeliveryDownCount <= 0))
                            break;
                        if ((DeliveryLeft > 0) && (Delivery1Count <= 0))
                            break;
                        if ((DeliveryRight > 0) && (Delivery2Count <= 0))
                            break;
                    } while ((DeliveryUp + DeliveryDown + DeliveryLeft + DeliveryRight) > 0);

                    Transaction.Rejected = Transaction.IssuedNotesCount - Transaction.DeliveredNotesCount;
                    RejectCount += (Transaction.IssuedNotesCount - Transaction.DeliveredNotesCount);

                    try
                    {
                        if (CashAcceptorExecutableDeviceType == ExecutableDeviceType.enCashRecycler)
                        {
                            if (RejectCount > 0)
                            {
                                int CashAcceptorCurrent_ = CashAcceptorCurrent;
                                CashAcceptorCurrent += RejectCount;
                                ToLog(1, "Cashier: GiveDelivery: (Rejected) CashAcceptorCurrent changed from " + CashAcceptorCurrent_.ToString() + " to " + CashAcceptorCurrent.ToString());
                                RejectCount = 0;
                            }
                        }
                    }
                    catch
                    {
                    }

                    for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                    {
                        switch (DeliveryBoxes[CurrentBox].Code)
                        {
                            case 0:
                                if (DeliveryUpCount != DeliveryBoxes[CurrentBox].Count)
                                {
                                    DeliveryUpCountChanged = true;
                                    ToLog(1, "Cashier: GiveDelivery: DeliveryUpCount changed from " + DeliveryUpCount.ToString() + " to " + DeliveryBoxes[CurrentBox].Count.ToString());
                                }
                                lock (DeliveryNominals)
                                    DeliveryUpCount = DeliveryBoxes[CurrentBox].Count;
                                break;
                            case 1:
                                if (DeliveryDownCount != DeliveryBoxes[CurrentBox].Count)
                                {
                                    DeliveryDownCountChanged = true;
                                    ToLog(1, "Cashier: GiveDelivery: DeliveryDownCount changed from " + DeliveryDownCount.ToString() + " to " + DeliveryBoxes[CurrentBox].Count.ToString());
                                }
                                lock (DeliveryNominals)
                                    DeliveryDownCount = DeliveryBoxes[CurrentBox].Count;
                                break;
                            case 2:
                                if (Delivery1Count != DeliveryBoxes[CurrentBox].Count)
                                {
                                    DeliveryLeftCountChanged = true;
                                    ToLog(1, "Cashier: GiveDelivery: Delivery1Count changed from " + Delivery1Count.ToString() + " to " + DeliveryBoxes[CurrentBox].Count.ToString());
                                }
                                lock (DeliveryNominals)
                                    Delivery1Count = DeliveryBoxes[CurrentBox].Count;
                                break;
                            case 3:
                                if (Delivery2Count != DeliveryBoxes[CurrentBox].Count)
                                {
                                    DeliveryRightCountChanged = true;
                                    ToLog(1, "Cashier: GiveDelivery: Delivery2Count changed from " + Delivery2Count.ToString() + " to " + DeliveryBoxes[CurrentBox].Count.ToString());
                                }
                                lock (DeliveryNominals)
                                    Delivery2Count = DeliveryBoxes[CurrentBox].Count;
                                break;
                        }
                    }
                    try
                    {
                        if (DeliveryUpCountChanged || DeliveryDownCountChanged || DeliveryLeftCountChanged || DeliveryRightCountChanged)
                        {
                            lock (db_lock)
                            {

                                deviceCMModel.CountUp = DeliveryUpCount;
                                deviceCMModel.CountDown = DeliveryDownCount;
                                deviceCMModel.Count1 = Delivery1Count;
                                deviceCMModel.Count2 = Delivery2Count;
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: GiveDelivery Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: GiveDelivery Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2 }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2 }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            DeliveryUpCountChanged = false;
                            DeliveryDownCountChanged = false;
                            DeliveryLeftCountChanged = false;
                            DeliveryRightCountChanged = false;
                        }
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                    }
                    /*CashDispenserUpper = _CashDispenserUpper;
                    CashDispenserLower = _CashDispenserLower;
                    CoinDispenser1Hopper = _CoinDispenser1Hopper;
                    CoinDispenser2Hopper = _CoinDispenser2Hopper;*/

                    if (ThreadDeliveryResDiff != 0)
                    {
                        ThreadDeliveryToLog += Environment.NewLine + ResourceManager.GetString("ToLogDeliveryError", DefaultCulture);
                        Transaction.ConclusionCode = ConclusionCodeT.Payment_is_finished_but_change_had_not_been_given_or_given_not_fully;
                    }
                    else
                    {
                        ThreadDeliveryToLog += Environment.NewLine + ResourceManager.GetString("ToLogDeliveryDone", DefaultCulture);
                        Transaction.ConclusionCode = ConclusionCodeT.Done;
                    }

                    if (ThreadDeliveryResDiff == 0)
                        break;
                }

                if ((Transaction.DeliveredFromUpper + Transaction.DeliveredFromLower + Transaction.DeliveredFrom1Hopper + Transaction.DeliveredFrom2Hopper) > 0)
                    ThreadDeliveryToLog += Environment.NewLine + ResourceManager.GetString("ToLogDelivered", DefaultCulture);
                if ((Transaction.IssuedFromUpper + Transaction.IssuedFromLower + Transaction.IssuedFrom1Hopper + Transaction.IssuedFrom2Hopper) > 0)
                    ThreadDeliveryToLog += Environment.NewLine + ResourceManager.GetString("ToLogDelivered1", DefaultCulture);
                if (Transaction.IssuedFromUpper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromUp", DefaultCulture), Transaction.IssuedFromUpper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.IssuedFromLower > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromDown", DefaultCulture), Transaction.IssuedFromLower.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.IssuedFrom1Hopper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromFront", DefaultCulture), Transaction.IssuedFrom1Hopper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.IssuedFrom2Hopper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromBack", DefaultCulture), Transaction.IssuedFrom2Hopper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.Rejected > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogBanknotesRejected", DefaultCulture), Transaction.Rejected.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredNotesCount > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogDeliveredAll", DefaultCulture), Transaction.DeliveredNotesCount.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredFromUpper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromUp", DefaultCulture), Transaction.DeliveredFromUpper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredFromLower > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromDown", DefaultCulture), Transaction.DeliveredFromLower.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredCoinsCount > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogDeliveredAll1", DefaultCulture), Transaction.DeliveredCoinsCount.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredFrom1Hopper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromFront", DefaultCulture), Transaction.DeliveredFrom1Hopper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }
                if (Transaction.DeliveredFrom2Hopper > 0)
                {
                    s = string.Format(ResourceManager.GetString("ToLogFromBack", DefaultCulture), Transaction.DeliveredFrom2Hopper.ToString());
                    ThreadDeliveryToLog += Environment.NewLine + s;
                }

                CashDispenserUpper = _CashDispenserUpper;
                CashDispenserLower = _CashDispenserLower;
                CoinDispenser1Hopper = _CoinDispenser1Hopper;
                CoinDispenser2Hopper = _CoinDispenser2Hopper;
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
                CashDispenserUpper = _CashDispenserUpper;
                CashDispenserLower = _CashDispenserLower;
                CoinDispenser1Hopper = _CoinDispenser1Hopper;
                CoinDispenser2Hopper = _CoinDispenser2Hopper;
            }
            GiveDeliveryStatus = false;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private bool DeliveryPossibleCheck(decimal Diff, decimal[] DeliveryBoxesNominals, DeliveryBoxesT[] DeliveryBoxes, ref string toLog)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            //Array.Sort(DeliveryBoxesNominals, DeliveryBoxes);
            try
            {
                int CurrentBox, Quotient, Delivered;
                int Summ = 0;
                for (CurrentBox = 3; CurrentBox >= 0; CurrentBox--)
                {
                    if ((DeliveryBoxesNominals[CurrentBox] != 0) && DeliveryBoxes[CurrentBox].Work)
                    {
                        Quotient = (int)(Diff / DeliveryBoxesNominals[CurrentBox]);
                        if (Quotient > DeliveryBoxes[CurrentBox].Count)
                            Quotient = DeliveryBoxes[CurrentBox].Count;
                        Delivered = (int)(Quotient * DeliveryBoxesNominals[CurrentBox]);
                        Summ += (int)DeliveryBoxesNominals[CurrentBox] * DeliveryBoxes[CurrentBox].Count;
                        Diff = Diff - Delivered;
                    }
                }
                if (Diff != 0 || Summ == 0)
                    toLog = ResourceManager.GetString("ToLogDeliveryImpossible", DefaultCulture);
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                return ((Diff == 0) && (Summ != 0));
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
                return false;
            }
        }

        private bool CheckReturnPossible(decimal Summ, ref string toLog)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            {
                decimal MaxDelivery = 0;
                try
                {
                    if (deviceCMModel.MaximalChange != null)
                        MaxDelivery = (int)deviceCMModel.MaximalChange;
                }
                catch { }
                if (Summ >= MaxDelivery)
                    return false;
                try
                {
                    string[] Result = new string[5];

                    //ToLog(2, "Cashier: CheckReturnPossible: SetDeliveryNominals");
                    SetDeliveryNominals();

                    Array.Sort(DeliveryBoxesNominals, DeliveryBoxes);

                    if (DetailLog)
                        ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                    return DeliveryPossibleCheck(Summ, DeliveryBoxesNominals, DeliveryBoxes, ref toLog);
                }
                catch (Exception e)
                {
                    ToLog(0, e.ToString());
                    return false;
                }
            }
        }
        decimal DeliveryPossibleOld = 0;
        private void CheckNominals()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {

                decimal MaxDelivery = 0, DeliveryPossible = 0,
                    CoinAcceptorReminder = 0, CashAcceptorReminder = 0;
                SumDelivery = 0;

                decimal DeltaPaid = (CardQuery > 0) ? CardQuery - Paid : 0;
                decimal Diff;
                decimal Nominal;
                //decimal MaxCoin;

                string[] Result = new string[5];
                string toLog = "";

                //ToLog(2, "Cashier: CheckNominals: SetDeliveryNominals");
                SetDeliveryNominals();

                MinBanknote = Math.Min(DeliveryBoxesNominals[0], DeliveryBoxesNominals[1]);
                if (MinBanknote == 9999999999)
                    MinBanknote = 0;
                MinCoin = Math.Min(DeliveryBoxesNominals[2], DeliveryBoxesNominals[3]);
                if (MinCoin == 9999999999)
                    MinCoin = 0;

                #region FillCash
                CashAcceptorNominals.Clear();
                if (PaymentGrid.Visibility == Visibility.Visible || terminalStatus == TerminalStatusT.Empty)
                {
                    if (CashAcceptorWork)
                    {
                        CashAcceptorReminder = CashAcceptorMaxCount - CashAcceptorCurrent;
                        for (int i = 0; i < Banknotes.Count; i++)
                        {
                            if (i < CashAcceptorAllows.Count)
                            {
                                if (CashAcceptorAllows[i].Allow)
                                    CashAcceptorNominals.Add(Convert.ToDecimal(Banknotes[i].Value));
                            }
                        }
                    }
                }
                #endregion FillCash
                #region FillCoin
                CoinAcceptorNominals.Clear();
                if (PaymentGrid.Visibility == Visibility.Visible || terminalStatus == TerminalStatusT.Empty)
                {
                    if (CoinAcceptorWork)
                    {
                        CoinAcceptorReminder = CoinAcceptorMaxCount - CoinAcceptorCurrent;
                        int ic = 0;
                        string Value = "";
                        for (int i = 0; i < Coins.Count; i++)
                        {
                            if (Value != cashierForm.Coins[i].Value)
                            {
                                Value = cashierForm.Coins[i].Value;
                                if (ic < CoinAcceptorAllows.Count)
                                {
                                    if (CoinAcceptorAllows[ic].Allow)
                                        CoinAcceptorNominals.Add(Convert.ToDecimal(Value));
                                }
                                ic++;
                            }
                        }
                    }
                }
                #endregion FillCoin
                #region CheckDelivery
                if (CashAcceptorCurrent >= CashAcceptorMaxCount)
                    CashAcceptorWork = false;
                if (CoinAcceptorCurrent >= CoinAcceptorMaxCount)
                    CoinAcceptorWork = false;

                if (CashDispenserWork)
                    SumDelivery += DeliveryUpCount * UpNominal +
                        DeliveryDownCount * DownNominal;
                if (Hopper1Work)
                    SumDelivery += Delivery1Count * Nominal1;
                if (Hopper2Work)
                    SumDelivery += Delivery2Count * Nominal2;

                /*if ((SumDelivery == 0) && (deviceCMModel.SingleClient0 != null) && !(bool)deviceCMModel.SingleClient0)
                {
                    Delivery = false;
                    deliveryStatus = DeliveryStatusT.NotDelivery;
                }*/
                if (deviceCMModel.MaximalChange != null)
                    MaxDelivery = Convert.ToDecimal(deviceCMModel.MaximalChange);

                DeliveryPossible = Math.Min(MaxDelivery, SumDelivery);

                if (DeliveryPossibleOld != DeliveryPossible)
                {
                    DeliveryPossibleOld = DeliveryPossible;
                    ToLog(2, "CheckNominals: DeliveryPossible=" + DeliveryPossible);
                }
                if (deliveryStatus == DeliveryStatusT.Delivery)
                {
                    if (!Delivery || (!CashDispenserWork && !Hopper1Work && !Hopper2Work))
                    {
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                    }
                }
                #endregion CheckDelivery

                #region CleanEmulatorNominals
                /*if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist && EmulatorEnabled && EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked)
                {
                    if (EmulatorForm.Emulator.b10.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b10.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.b50.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b50.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.b100.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b100.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.b500.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b500.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.b1000.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b1000.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.b5000.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.b5000.BackColor = System.Drawing.SystemColors.Control;
                }
                if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist && EmulatorEnabled && EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                {
                    if (EmulatorForm.Emulator.m1.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.m1.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.m2.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.m2.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.m5.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.m5.BackColor = System.Drawing.SystemColors.Control;
                    if (EmulatorForm.Emulator.m10.BackColor != System.Drawing.Color.Red)
                        EmulatorForm.Emulator.m10.BackColor = System.Drawing.SystemColors.Control;
                }*/
                #endregion CleanEmulatorNominals

                if (
                    (
                     (terminalStatus == TerminalStatusT.CardReadedPlus) ||
                     (terminalStatus == TerminalStatusT.CardReadedMinus) ||
                     (terminalStatus == TerminalStatusT.PenalCardRequest) ||
                     (terminalStatus == TerminalStatusT.PenalCardPrePaiment) ||
                     (terminalStatus == TerminalStatusT.PenalCardPaiment)
                    ) /*&&
                    (deliveryStatus != DeliveryStatusT.NotDelivery)
                    */
                   )
                {
                    /*DeliveryBoxes[0].Count = DeliveryUpCount;
                    DeliveryBoxes[1].Count = DeliveryDownCount;
                    DeliveryBoxes[2].Count = Delivery1Count;
                    DeliveryBoxes[3].Count = Delivery2Count;*/

                    Array.Sort(DeliveryBoxesNominals, DeliveryBoxes);

                    #region CheckNominals
                    if (!CardBalancePos && ((CardQuery - Paid) > 0))
                    {
                        if (CashAcceptorWork)
                        {
                            if (CashAcceptorCurrent >= CashAcceptorMaxCount)
                                CashAcceptorWork = false;
                            else
                            {
                                if (terminalStatus != TerminalStatusT.Empty)
                                {
                                    if (deliveryStatus != DeliveryStatusT.NotDelivery || ClientType == 0)
                                    {
                                        for (int i = CashAcceptorNominals.Count - 1; i >= 0; i--)
                                        {
                                            Nominal = CashAcceptorNominals[i];

                                            if ((Nominal - DeltaPaid) > DeliveryPossible)
                                            {
                                                CashAcceptorNominals.RemoveAt(i);
                                                continue;
                                            }

                                            if ((Nominal * CashAcceptorReminder) < DeltaPaid)
                                            {
                                                CashAcceptorNominals.RemoveAt(i);
                                                continue;
                                            }

                                            if (deviceCMModel.SyncNominals != null && (bool)deviceCMModel.SyncNominals)
                                            {
                                                if ((deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist) && (Hopper1Work || Hopper2Work))
                                                {
                                                    if ((Nominal < MinBanknote) && (Nominal < MinCoin))
                                                    {
                                                        CashAcceptorNominals.RemoveAt(i);
                                                        continue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Nominal < MinBanknote)
                                                    {
                                                        CashAcceptorNominals.RemoveAt(i);
                                                        continue;
                                                    }
                                                }
                                            }

                                            Diff = Paid + Nominal - CardQuery;
                                            if (Diff > 0)
                                                if (!DeliveryPossibleCheck(Diff, DeliveryBoxesNominals, DeliveryBoxes, ref toLog))
                                                {
                                                    CashAcceptorNominals.RemoveAt(i);
                                                    continue;
                                                }
                                        }
                                    }
                                }
                            }
                        }

                        if (CoinAcceptorWork)
                        {
                            if (CoinAcceptorCurrent >= CoinAcceptorMaxCount)
                                CoinAcceptorWork = false;
                            else
                            {
                                if (terminalStatus != TerminalStatusT.Empty)
                                {
                                    //if (deliveryStatus != DeliveryStatusT.NotDelivery)
                                    {

                                        for (int i = CoinAcceptorNominals.Count - 1; i >= 0; i--)
                                        {
                                            Nominal = CoinAcceptorNominals[i];
                                            if ((Nominal - DeltaPaid) > DeliveryPossible)
                                            {
                                                CoinAcceptorNominals.RemoveAt(i);
                                                continue;
                                            }

                                            if ((Nominal * CoinAcceptorReminder) < DeltaPaid)
                                            {
                                                CoinAcceptorNominals.RemoveAt(i);
                                                continue;
                                            }
                                            if (deviceCMModel.SyncNominals != null && (bool)deviceCMModel.SyncNominals)
                                            {
                                                if ((deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist) && (Hopper1Work || Hopper2Work))
                                                {
                                                    if (Nominal < MinCoin)
                                                    {
                                                        CoinAcceptorNominals.RemoveAt(i);
                                                        continue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Nominal < MinBanknote)
                                                    {
                                                        CoinAcceptorNominals.RemoveAt(i);
                                                        continue;
                                                    }
                                                }
                                            }
                                            Diff = Paid + Nominal - CardQuery;
                                            if (Diff > 0)
                                                if (!DeliveryPossibleCheck(Diff, DeliveryBoxesNominals, DeliveryBoxes, ref toLog))
                                                {
                                                    CoinAcceptorNominals.RemoveAt(i);
                                                    continue;
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion CheckNominals
                }
                #region SetAcceptedCash
                if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist && EmulatorEnabled && EmulatorForm.Emulator.CashAcceptorVirt.Checked && EmulatorForm.Emulator.CashAcceptorWork.Checked && CashAcceptorWork)
                {
                    if (CashAcceptorNominals.Contains(10))
                    {
                        if (EmulatorForm.Emulator.b10.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b10.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b10.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b10.BackColor = System.Drawing.Color.Yellow;

                    if (CashAcceptorNominals.Contains(50))
                    {
                        if (EmulatorForm.Emulator.b50.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b50.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b50.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b50.BackColor = System.Drawing.Color.Yellow;
                    if (CashAcceptorNominals.Contains(100))
                    {
                        if (EmulatorForm.Emulator.b100.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b100.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b100.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b100.BackColor = System.Drawing.Color.Yellow;
                    if (CashAcceptorNominals.Contains(500))
                    {
                        if (EmulatorForm.Emulator.b500.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b500.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b500.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b500.BackColor = System.Drawing.Color.Yellow;
                    if (
                        CashAcceptorNominals.Contains(1000))
                    {
                        if (EmulatorForm.Emulator.b1000.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b1000.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b1000.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b1000.BackColor = System.Drawing.Color.Yellow;

                    if (CashAcceptorNominals.Contains(5000))
                    {
                        if (EmulatorForm.Emulator.b5000.BackColor != System.Drawing.Color.Red)
                            EmulatorForm.Emulator.b5000.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                        if (EmulatorForm.Emulator.b5000.BackColor == System.Drawing.SystemColors.Control)
                        EmulatorForm.Emulator.b5000.BackColor = System.Drawing.Color.Yellow;

                }
                int CashAcceptedCodeOld = CashAcceptedCode;
                CashAcceptedCode = 0;
                //if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked))
                {
                    for (int i = 0; i < Banknotes.Count; i++)
                    {
                        decimal Val = Convert.ToDecimal(Banknotes[i].Value);
                        if (!CashAcceptorNominals.Contains(Val))
                        {
                            CashAcceptedCode += (int)Banknotes[i].DisableData;
                        }
                    }
                    if (CashAcceptedCodeOld != CashAcceptedCode)
                        CashNominalsChanged = true;
                }
                #endregion SetAcceptedCash
                #region SetAcceptedCoin
                if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist && EmulatorEnabled && EmulatorForm.Emulator.CoinAcceptorVirt.Checked && EmulatorForm.Emulator.CoinAcceptorWork.Checked && CoinAcceptorWork)
                {
                    int ic = 0;
                    string Value = "";
                    for (int i = 0; i < Coins.Count; i++)
                    {
                        if (Value != Coins[i].Value)
                        {
                            Value = Coins[i].Value;
                            decimal Val = Convert.ToDecimal(Value);
                            if (CoinAcceptorNominals.Contains(Val))
                            {
                                switch (ic)
                                {
                                    case 0:
                                        if (EmulatorForm.Emulator.m1.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m1.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                    case 1:
                                        if (EmulatorForm.Emulator.m2.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m2.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                    case 2:
                                        if (EmulatorForm.Emulator.m3.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m3.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                    case 3:
                                        if (EmulatorForm.Emulator.m4.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m4.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                    case 4:
                                        if (EmulatorForm.Emulator.m3.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m3.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                    case 5:
                                        if (EmulatorForm.Emulator.m6.BackColor != System.Drawing.Color.Red)
                                            EmulatorForm.Emulator.m6.BackColor = System.Drawing.SystemColors.Control;
                                        break;
                                }
                            }
                            else
                            {
                                switch (ic)
                                {
                                    case 0:
                                        if (EmulatorForm.Emulator.m1.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m1.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                    case 1:
                                        if (EmulatorForm.Emulator.m2.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m2.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                    case 2:
                                        if (EmulatorForm.Emulator.m3.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m3.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                    case 3:
                                        if (EmulatorForm.Emulator.m4.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m4.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                    case 4:
                                        if (EmulatorForm.Emulator.m3.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m3.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                    case 5:
                                        if (EmulatorForm.Emulator.m6.BackColor == System.Drawing.SystemColors.Control)
                                            EmulatorForm.Emulator.m6.BackColor = System.Drawing.Color.Yellow;
                                        break;
                                }
                            }
                            ic++;
                        }
                    }
                }
                //if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist && (!EmulatorEnabled || !EmulatorForm.Emulator.CoinAcceptorVirt.Checked))
                {
                    for (int i = 0; i < Coins.Count; i++)
                    {
                        decimal Val = Convert.ToDecimal(Coins[i].Value);
                        if (!CoinAcceptorNominals.Contains(Val))
                        {
                            if (Coins[i].CIndex - 1 <= CoinsAccepted.Length)
                            {
                                if (CoinsAccepted[Coins[i].CIndex - 1])
                                {
                                    CoinNominalsChanged = true;
                                    CoinsAccepted[Coins[i].CIndex - 1] = false;
                                }
                            }
                        }
                        else
                        {
                            if (Coins[i].CIndex - 1 <= CoinsAccepted.Length)
                            {
                                if (!CoinsAccepted[Coins[i].CIndex - 1])
                                {
                                    CoinNominalsChanged = true;
                                    //CoinsAccepted[Coins[i].CIndex] = false;
                                    CoinsAccepted[Coins[i].CIndex - 1] = true;
                                }
                            }
                        }
                    }

                }
                #endregion SetAcceptedCoin
                string s = "";
                if (CashNominalsChanged)
                {
                    for (int i = 0; i < CashAcceptorNominals.Count; i++)
                    {
                        s += CashAcceptorNominals[i].ToString();
                        if (i < CashAcceptorNominals.Count - 1)
                            s += ", ";
                    }
                    ToLog(2, "Cashier: CheckNominals: CashAccepted Nominals - " + s);
                }
                if ((CashAcceptor != null) && (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked) && CashNominalsChanged)
                {
                    if (CashAcceptor.OnOffSetting(CashAcceptedCode))
                        CashNominalsChanged = false;

                }
                else
                    CashNominalsChanged = false;

                s = "";
                if (CoinNominalsChanged)
                {
                    for (int i = 0; i < CoinAcceptorNominals.Count; i++)
                    {
                        s += CoinAcceptorNominals[i].ToString();
                        if (i < CoinAcceptorNominals.Count - 1)
                            s += ", ";
                    }
                    ToLog(2, "Cashier: CheckNominals: CoinAccepted Nominals - " + s);
                }
                if ((CoinAcceptor != null) && (!EmulatorEnabled || !EmulatorForm.Emulator.CoinAcceptorVirt.Checked) && CoinNominalsChanged)
                {
                    if (CoinAcceptor.SetInhibit(CoinsAccepted) == 0)
                        CoinNominalsChanged = false;
                }
                else
                    CoinNominalsChanged = false;

                bool First;
                #region DisplayAcceptedCash
                CashAcceptorStatusLabel = "";
                if (CashAcceptorWork)
                {
                    First = true;
                    if (CashAcceptorNominals.Count > 0)
                    {
                        CashAcceptorStatusLabel = Properties.Resources.CashAcceptorStatusLabel;
                        for (int i = 0; i < CashAcceptorNominals.Count; i++)
                            if (First)
                            {
                                CashAcceptorStatusLabel += " ";
                                if (CashAcceptorNominals[i] == Math.Round(CashAcceptorNominals[i]))
                                    CashAcceptorStatusLabel += (int)CashAcceptorNominals[i];
                                else
                                    CashAcceptorStatusLabel += CashAcceptorNominals[i];

                                First = false;
                            }
                            else
                            {
                                CashAcceptorStatusLabel += ", ";
                                if (CashAcceptorNominals[i] == Math.Round(CashAcceptorNominals[i]))
                                    CashAcceptorStatusLabel += (int)CashAcceptorNominals[i];
                                else
                                    CashAcceptorStatusLabel += CashAcceptorNominals[i];
                            }
                        CashAcceptorStatusLabel += " " + Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country);
                    }
                }
                #endregion DisplayAcceptedCash
                #region DisplayAcceptedCoin
                CoinAcceptorStatusLabel = "";
                if (CoinAcceptorWork)
                {
                    First = true;
                    if (CoinAcceptorNominals.Count > 0)
                    {
                        CoinAcceptorStatusLabel = Properties.Resources.CoinAcceptorStatusLabel;
                        for (int i = 0; i < CoinAcceptorNominals.Count; i++)
                            if (First)
                            {
                                CoinAcceptorStatusLabel += " ";
                                if (CoinAcceptorNominals[i] == Math.Round(CoinAcceptorNominals[i]))
                                    CoinAcceptorStatusLabel += (int)CoinAcceptorNominals[i];
                                else
                                    CoinAcceptorStatusLabel += CoinAcceptorNominals[i];
                                First = false;
                            }
                            else
                            {
                                CoinAcceptorStatusLabel += ", ";
                                if (CoinAcceptorNominals[i] == Math.Round(CoinAcceptorNominals[i]))
                                    CoinAcceptorStatusLabel += (int)CoinAcceptorNominals[i];
                                else
                                    CoinAcceptorStatusLabel += CoinAcceptorNominals[i];
                            }
                        CoinAcceptorStatusLabel += " " + Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country);
                    }
                }
                #endregion DisplayAcceptedCoin
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void CashAcceptorPaiment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            bool CashAcceptorStatusCahnged = false;
            try
            {
                string s;
                int Nominal;
                if (EmulatorEnabled && EmulatorForm.Emulator.CashAcceptorVirt.Checked)
                {
                    #region CashAcceptorVirt
                    if (EmulatorForm.Emulator.PaidBanknoteCurrent != CashAcceptorCurrent)
                    {
                        foreach (int Banknote in EmulatorForm.Emulator.PaidBanknoteList)
                        {
                            if (!CashAcceptorNominals.Contains(Banknote))
                            {
                                Nominal = Banknote;
                                EmulatorForm.Emulator.PaidBanknote -= Nominal;
                                decimal pdc = --EmulatorForm.Emulator.PaidBanknoteCurrent;
                                EmulatorForm.Emulator.CashAcceptorCurrent.Text = pdc.ToString();
                                s = string.Format(ResourceManager.GetString("ToLogCashRevocation", DefaultCulture), Nominal.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country));
                                ToLog(1, s);
                            }
                            else
                                CashAcceptorPaid.Add(Banknote);
                        }
                        EmulatorForm.Emulator.PaidBanknoteList.Clear();
                        CashAcceptorCurrent = EmulatorForm.Emulator.PaidBanknoteCurrent;
                        PaidBanknote = EmulatorForm.Emulator.PaidBanknote;
                        SetCashPayment();
                        PaimentsStatusChanged = true;
                    }
                    #endregion CashAcceptorVirt
                }
                else
                {
                    #region CashAcceptorHard
                    if ((CashAcceptor != null) && CashAcceptorWork)
                    {
                        if (CashAcceptorStatus != CashAcceptor.CashAcceptorStatus.Status)
                        {
                            ToLog(2, "Cahsier: CashAcceptorPaiment: " + string.Format("CashAcceptor status changed from {0}  to {1}", CashAcceptorStatus, CashAcceptor.CashAcceptorStatus.Status));
                            CashAcceptorStatusCahnged = true;
                            CashAcceptorStatus = CashAcceptor.CashAcceptorStatus.Status;
                        }
                        if (
                            CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Accepting
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Escrow
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Stacking
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.VendValid
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Stacked
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Rejecting
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Returning
                            || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Holding
                            )
                        {
                            if (DevicePaiments == PaimentMethodT.PaimentsAll || Paiments == PaimentMethodT.PaimentsAll)
                            {
                                SetCashPayment();
                            }
                        }
                        if (CashAcceptorStatus != CashAcceptor.CashAcceptorStatus.Status)
                        {
                            ToLog(2, "Cahsier: CashAcceptorPaiment: " + string.Format("CashAcceptor status changed from {0}  to {1}", CashAcceptorStatus, CashAcceptor.CashAcceptorStatus.Status));
                            CashAcceptorStatusCahnged = true;
                            CashAcceptorStatus = CashAcceptor.CashAcceptorStatus.Status;
                        }

                        if (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Escrow && CashAcceptorStatusCahnged)
                        {
                            if (BankModule != null)
                            {
                                if (DevicePaiments != PaimentMethodT.PaimentsBankModule)
                                {
                                    //if (!BankModule.AuthorizationStarted)
                                    {
                                        int NominalCode = CashAcceptor.GetNominalCode();
                                        for (int i = 0; i < Banknotes.Count; i++)
                                        {
                                            if (Banknotes[i].EscrowData == NominalCode)
                                            {
                                                CurDenom = Convert.ToDecimal(Banknotes[i].Value);
                                                break;
                                            }
                                        }
                                        if (CashAcceptorNominals.Contains(CurDenom))
                                        {
                                            if (DetailLog)
                                                ToLog(2, "Cashier: CashAcceptor CashAcceptor.Stack_1_Operation");
                                            CashAcceptor.Stack_1_Operation();
                                            //Thread.Sleep(200);
                                        }
                                        else
                                        {
                                            if (DetailLog)
                                                ToLog(2, "Cashier: CashAcceptor CashAcceptor.Return_Operation");
                                            CashAcceptor.Return_Operation();
                                            //Thread.Sleep(200);
                                        }
                                    }
                                }
                                else
                                {
                                    if (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Accepting || CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Escrow)
                                    {
                                        if (DetailLog)
                                            ToLog(2, "Cashier: CashAcceptor CashAcceptor.Return_Operation");
                                        CashAcceptor.Return_Operation();
                                        //Thread.Sleep(200);
                                    }
                                }
                            }
                            else
                            {
                                int NominalCode = CashAcceptor.GetNominalCode();
                                for (int i = 0; i < Banknotes.Count; i++)
                                {
                                    if (Banknotes[i].EscrowData == NominalCode)
                                    {
                                        CurDenom = Convert.ToDecimal(Banknotes[i].Value);
                                        break;
                                    }
                                }
                                if (CashAcceptorNominals.Contains(CurDenom))
                                {
                                    if (DetailLog)
                                        ToLog(2, "Cashier: CashAcceptor CashAcceptor.Return_Operation");
                                    CashAcceptor.Stack_1_Operation();
                                    //Thread.Sleep(200);
                                }
                                else
                                {
                                    if (DetailLog)
                                        ToLog(2, "Cashier: CashAcceptor CashAcceptor.Return_Operation");
                                    CashAcceptor.Return_Operation();
                                    //Thread.Sleep(200);
                                }
                            }
                        }
                        if (CashAcceptor.NewNotesInserted)
                        {
                            if (DetailLog)
                                ToLog(2, "Cashier: CashAcceptor.NewNotesInserted Enter");
                            int NominalCode = CashAcceptor.GetNominalCode();
                            for (int i = 0; i < Banknotes.Count; i++)
                            {
                                if (Banknotes[i].EscrowData == NominalCode)
                                {
                                    CurDenom = Convert.ToDecimal(Banknotes[i].Value);
                                    break;
                                }
                            }
                            PaidBanknote += CurDenom;
                            SetCashPayment();
                            CashAcceptorPaid.Add(CurDenom);
                            s = string.Format(ResourceManager.GetString("ToLogCashAddition", DefaultCulture), CurDenom.ToString("F0"), ResourceManager.GetString("ValuteLabel", DefaultCulture));
                            ToLog(1, s);

                            try
                            {
                                if (CashAcceptorExecutableDeviceType == ExecutableDeviceType.enCashRecycler)
                                {
                                    if (CashRecycler.NotesCountChanged)
                                    {
                                        ToLog(1, string.Format("Cashier: CashAcceptorPaiment: CashRecycler.CashDispenserResultStatus.Cassettes[1].HopperStatus={0}", CashRecycler.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                        if (CashRecycler.NotesCount[0] != DeliveryUpCount)
                                        {
                                            int DeliveryUpCount_ = DeliveryUpCount;
                                            DeliveryUpCount = CashRecycler.NotesCount[0];
                                            ToLog(1, "Cashier: CashAcceptorPaiment: DeliveryUpCount changed from " + DeliveryUpCount_.ToString() + " to " + DeliveryUpCount.ToString());
                                            if (!CashDispenserUpper)
                                                CashDispenserUpper = true;
                                            DeliveryUpCountChanged = true;
                                        }
                                        ToLog(1, string.Format("Cashier: CashAcceptorPaiment: CashRecycler.CashDispenserResultStatus.Cassettes[2].HopperStatus={0}", CashRecycler.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                        if (CashRecycler.NotesCount[1] != DeliveryDownCount)
                                        {
                                            int DeliveryDownCount_ = DeliveryDownCount;
                                            DeliveryDownCount = CashRecycler.NotesCount[1];
                                            ToLog(1, "Cashier: CashAcceptorPaiment: DeliveryDownCount changed from " + DeliveryDownCount_.ToString() + " to " + DeliveryDownCount.ToString());
                                            if (!CashDispenserLower)
                                                CashDispenserLower = true;
                                            DeliveryDownCountChanged = true;
                                        }
                                        CashRecycler.NotesCountChanged = false;
                                    }
                                    else
                                    {
                                        int CashAcceptorCurrent_ = CashAcceptorCurrent;
                                        CashAcceptorCurrent++;
                                        CashAcceptorCountChanged = true;
                                        ToLog(1, "Cashier: CashAcceptorPaiment: CashAcceptorCurrent changed from " + CashAcceptorCurrent_.ToString() + " to " + CashAcceptorCurrent.ToString());
                                    }
                                }
                                else
                                {
                                    int CashAcceptorCurrent_ = CashAcceptorCurrent;
                                    CashAcceptorCurrent++;
                                    CashAcceptorCountChanged = true;
                                    ToLog(1, "Cashier: CashAcceptorPaiment: CashAcceptorCurrent changed from " + CashAcceptorCurrent_.ToString() + " to " + CashAcceptorCurrent.ToString());
                                }
                            }
                            catch
                            {
                                int CashAcceptorCurrent_ = CashAcceptorCurrent;
                                CashAcceptorCurrent++;
                                CashAcceptorCountChanged = true;
                                ToLog(1, "Cashier: CashAcceptorPaiment: CashAcceptorCurrent changed from " + CashAcceptorCurrent_.ToString() + " to " + CashAcceptorCurrent.ToString());
                            }
                            CurDenom = 0;
                            PaimentsStatusChanged = true;
                            try
                            {
                                if (DeliveryUpCountChanged || DeliveryDownCountChanged || CashAcceptorCountChanged)
                                {
                                    if (DetailLog)
                                        ToLog(2, "Cashier: CashAcceptor CountChanged dbSave Enter");
                                    lock (db_lock)
                                    {
                                        deviceCMModel.CountUp = DeliveryUpCount;
                                        deviceCMModel.CountDown = DeliveryDownCount;
                                        deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                                        //if (DetailLog)
                                        //    ToLog(2, "Cashier: CashAcceptor CountChanged Utils.Running Enter");
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        //if (DetailLog)
                                        //    ToLog(2, "Cashier: CashAcceptor CountChanged Utils.Running Leave");
                                        try
                                        {
                                            UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                        if (DetailLog)
                                            ToLog(2, "Cashier: CashAcceptor CountChanged dbSave db_lock Leave");
                                    }
                                    if (CashAcceptorCountChanged)
                                    {
                                        TechForm.CashAcceptorCurrent.Text = CashAcceptorCurrent.ToString();
                                    }
                                    DeliveryUpCountChanged = false;
                                    DeliveryDownCountChanged = false;
                                    CashAcceptorCountChanged = false;
                                    if (DetailLog)
                                        ToLog(2, "Cashier: CashAcceptor CountChanged dbSave Leave");
                                }
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                            }
                            if (DetailLog)
                                ToLog(2, "Cashier: CashAcceptor.NewNotesInserted Leave");
                        }
                    };
                    #endregion CashAcceptorHard                
                };
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void CoinAcceptorPaiment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                string s;
                int Nominal;
                if (EmulatorEnabled && EmulatorForm.Emulator.CoinAcceptorVirt.Checked)
                {
                    #region CoinAcceptorVirt
                    if (EmulatorForm.Emulator.PaidCoinCurrent != CoinAcceptorCurrent)
                    {
                        foreach (int Coin in EmulatorForm.Emulator.PaidCoinList)
                        {
                            if (!CoinAcceptorNominals.Contains(Coin))
                            {
                                Nominal = Coin;
                                EmulatorForm.Emulator.PaidCoin -= Nominal;
                                decimal pcc = --EmulatorForm.Emulator.PaidCoinCurrent;
                                EmulatorForm.Emulator.CoinAcceptorCurrent.Text = pcc.ToString("F0");
                                s = string.Format(ResourceManager.GetString("ToLogCoinRevocation", DefaultCulture), Nominal.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country));
                                ToLog(1, s);
                            }
                            else
                                CoinAcceptorPaid.Add(Coin);
                        }
                        EmulatorForm.Emulator.PaidCoinList.Clear();
                        CoinAcceptorCurrent = EmulatorForm.Emulator.PaidCoinCurrent;
                        PaidCoin = EmulatorForm.Emulator.PaidCoin;
                        SetCashPayment();
                        PaimentsStatusChanged = true;
                    }
                    #endregion CoinAcceptorVirt                
                }
                else
                {
                    #region CoinAcceptorHard
                    if ((CoinAcceptor != null) && CoinAcceptorWork && CoinAcceptor.IsOpen)
                    {
                        var res = CoinAcceptor.GetStatus();
                        if (res != 0)
                        {
                            CurDenom = (decimal)CoinAcceptor.GetNominalCoin();
                            if (CurDenom > 0)
                            {
                                if (DevicePaiments == PaimentMethodT.PaimentsAll)
                                {
                                    SetCashPayment();
                                }
                                CoinAcceptorInhibit = true;
                                PaidCoin += CurDenom;
                                CoinAcceptorPaid.Add(CurDenom);
                                s = string.Format(ResourceManager.GetString("ToLogCoinAddition", DefaultCulture), CurDenom.ToString("F0"), ResourceManager.GetString("ValuteLabel", DefaultCulture));
                                ToLog(1, s);
                                CoinAcceptorCurrent++;
                                CurDenom = 0;
                                PaimentsStatusChanged = true;
                            }
                        }
                    };
                    #endregion CoinAcceptorHard
                };
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void BankModulePaiment()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                string s;
                if (EmulatorEnabled && EmulatorForm.Emulator.BankModuleVirt.Checked)
                {
                    #region BankModuleSoft
                    if (BankModuleStatus != EmulatorForm.Emulator.BankModuleStatus)
                    {
                        if (EmulatorForm.Emulator.BankModuleStatus == BankModuleStatusT.BankCardInserted)
                        {
                            if (BankModuleStatus != BankModuleStatusT.BankCardInserted)
                            {
                                BankCardErrorDateTime = datetime0;
                                BankModuleStatus = BankModuleStatusT.BankCardInserted;
                                BankModuleStatusChanged = true;
                                if (TerminalStatusLabel != "������ ���������� ������")
                                {
                                    TerminalStatusLabel = "������ ���������� ������";
                                }
                                SetBankCardPayment();
                            }
                        }

                        BankModuleStatusChanged = true;
                        BankModuleStatus = EmulatorForm.Emulator.BankModuleStatus;
                        if (BankQuerySummRequested &&
                            BankQuerySumm > 0)
                        {
                            s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                            ToLog(1, s);
                            BankModuleResultDescription = "";
                            //BankModule.Purchase(BankQuerySumm);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region BankModuleHard
                    if ((BankModule != null) && BankModuleWork && BankModule.DeviceEnabled)
                    {
                        if (BankModuleStatus != BankModuleStatusT.BankCardNotSuccess && !BankModule.IsCancelled)
                        {
                            if (BankModule.CardInserted)
                            {
                                if (BankModuleStatus != BankModuleStatusT.BankCardInserted)
                                {
                                    BankCardErrorDateTime = datetime0;
                                    BankModuleStatus = BankModuleStatusT.BankCardInserted;
                                    BankModuleStatusChanged = true;
                                    if (TerminalStatusLabel != BankModule.AuthRequest && BankModule.AuthRequest != "" && BankModule.AuthRequest != "THE TRANSACTION IS COMPLETED!")
                                    {
                                        TerminalStatusLabel = BankModule.AuthRequest;
                                    }
                                    SetBankCardPayment();
                                }
                            }
                            else
                            {
                                if (BankModule.AuthRequest != FormBankCardLabel.Text)
                                    FormBankCardLabel.Text = BankModule.AuthRequest;
                            }
                        }

                        if (Paiments == PaimentMethodT.PaimentsBankModule)
                        {
                            if (BankModule.CommandStatus == CommandStatusT.Running)
                            {
                                if (TerminalStatusLabel != BankModule.AuthRequest && BankModule.AuthRequest != "" && BankModule.AuthRequest != "THE TRANSACTION IS COMPLETED!")
                                {
                                    BankModuleStatusChanged = true;
                                    TerminalStatusLabel = BankModule.AuthRequest;
                                    if (BankModule.TransactionStarted)
                                    {
                                        if (BankPaymentCancel.Visibility != Visibility.Hidden)
                                            BankPaymentCancel.Visibility = Visibility.Hidden;
                                    }
                                    else
                                    {
                                        if (BankPaymentCancel.Visibility != Visibility.Visible)
                                            BankPaymentCancel.Visibility = Visibility.Visible;
                                    }
                                }
                            }
                            else if (BankModule.CommandStatus == CommandStatusT.Completed)
                            {
                                if (BankPaymentCancel.Visibility != Visibility.Hidden)
                                    BankPaymentCancel.Visibility = Visibility.Hidden;
                                if (BankModule.CardWasInserted)
                                {
                                    BankModuleStatusChanged = true;
                                    if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess && (BankModule.AuthResult != "" || BankModule.AuthReceipt != ""))
                                    {
                                        BankModuleStatus = BankModuleStatusT.BankCardSuccess;
                                        string Result = BankModule.AuthResult;
                                        BankModule.AuthResult = "";
                                        string Receipt = BankModule.AuthReceipt;
                                        BankModule.AuthReceipt = "";
                                        BankModule.CommandStatus = CommandStatusT.Undeifned;
                                        Guid Id = Guid.NewGuid();
                                        lock (db_lock)
                                        {
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: BankModulePayment Utils.Running Enter");
                                            //while (Utils.Running)
                                            //    Thread.Sleep(50);
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: BankModulePayment Utils.Running Leave");
                                            try
                                            {
                                                UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 1, "Result", Result, "Receipt", Receipt });
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 1, "Result", Result, "Receipt", Receipt });
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }

                                        BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                                    }
                                    else if (BankModule.AuthResult != "" || BankModule.AuthReceipt != "" || BankModule.ResultCode != CommonBankModule.ErrorCode.Sucsess)
                                    {
                                        string Result = BankModule.AuthResult;
                                        BankModule.AuthResult = "";
                                        string Receipt = BankModule.AuthReceipt;
                                        BankModule.AuthReceipt = "";
                                        BankModule.CommandStatus = CommandStatusT.Undeifned;
                                        Guid Id = Guid.NewGuid();
                                        lock (db_lock)
                                        {
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: BankModulePayment Utils.Running Enter");
                                            //while (Utils.Running)
                                            //    Thread.Sleep(50);
                                            //if (DetailLog)
                                            //    ToLog(2, "Cashier: BankModulePayment Utils.Running Leave");
                                            try
                                            {
                                                UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 1, "Result", Result, "Receipt", Receipt });
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 1, "Result", Result, "Receipt", Receipt });
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }

                                        BankCardErrorDateTime = DateTime.Now;

                                        BankModuleResultDescription = BankModule.ResultDescription;
                                        BankModuleStatus = BankModuleStatusT.BankCardNotSuccess;
                                        BankModule.CommandStatus = CommandStatusT.Undeifned;
                                        if (((PaidBanknote + PaidCoin) > 0))
                                        {
                                            DevicePaiments = PaimentMethodT.PaimentsCash;
                                            if (Paiments != PaimentMethodT.PaimentsCash)
                                            {
                                                ToLog(2, "Cashier: BankModulePaiment1: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsCash.ToString());
                                                Paiments = PaimentMethodT.PaimentsCash;
                                                if (EmulatorEnabled)
                                                    EmulatorForm.Emulator.Paiments = DevicePaiments;
                                            }
                                        }
                                        else
                                        {
                                            DevicePaiments = PaimentMethodT.PaimentsAll;
                                            if (Paiments != PaimentMethodT.PaimentsAll)
                                            {
                                                ToLog(2, "Cashier: BankModulePaiment1: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                                                Paiments = PaimentMethodT.PaimentsAll;
                                                if (EmulatorEnabled)
                                                    EmulatorForm.Emulator.Paiments = DevicePaiments;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (BankQuerySummRequested &&
                            BankQuerySumm > 0 &&
                            BankModuleStatus != BankModuleStatusT.BankCardNotSuccess &&
                            BankModuleStatus != BankModuleStatusT.BankCardSuccess &&
                            BankModule.CommandStatus != CommandStatusT.Run &&
                            BankModule.CommandStatus != CommandStatusT.Running)
                        {
                            s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                            ToLog(1, s);
                            BankModuleResultDescription = "";
                            BankModule.Purchase(BankQuerySumm);
                        }
                    }
                    #endregion
                };

                if (BankModuleStatusChanged)
                {
                    BankModuleStatusChanged = false;
                    PaimentsStatusChanged = true;
                    switch (BankModuleStatus)
                    {
                        case BankModuleStatusT.BankCardInserted:
                            if (WorkCancel.Visibility != Visibility.Hidden)
                                WorkCancel.Visibility = Visibility.Hidden;
                            if (smallTableWorkPanel1.Visibility != Visibility.Hidden)
                                smallTableWorkPanel1.Visibility = Visibility.Hidden;
                            if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                                FormAlarmTextLabel.Visibility = Visibility.Visible;
                            //SetBankCardPayment();
                            break;
                        case BankModuleStatusT.BankCardSuccess:
                            if (BankQuerySumm > 0)
                            {
                                s = string.Format(ResourceManager.GetString("ToLogBankAdition", DefaultCulture), BankQuerySumm.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country)) + " " + ResourceManager.GetString("ToLogBankChequeSuccess", DefaultCulture);
                                ToLog(1, s);

                                BankPaid = BankQuerySumm;
                                BankQuerySummRequested = false;
                                PaimentsStatusChanged = true;

                                //WorkCancel.Visibility = Visibility.Visible;
                                if (EmulatorEnabled)
                                {
                                    if (terminalStatus != EmulatorForm.Emulator.terminalStatus)
                                    {
                                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    }
                                    EmulatorForm.Emulator.BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                                }
                                BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                                Delivery = false;
                            }
                            break;
                    }
                }
                if (BankModuleStatus == BankModuleStatusT.BankCardNotSuccess)
                {
                    if (BankCardErrorDateTime != datetime0 && BankCardErrorDateTime < DateTime.Now - TimeSpan.FromSeconds(5))
                    {
                        if (BankQuerySumm > 0)
                        {
                            s = string.Format(ResourceManager.GetString("ToLogBankAdition", DefaultCulture), BankQuerySumm.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country)) + " " + ResourceManager.GetString("ToLogBankNotSuccess", DefaultCulture);
                            ToLog(1, s);
                        }
                        //AddingSumm = 0;
                        //BankQuerySumm = CardQuery;
                        PaimentsStatusChanged = true;
                        if (BankQuerySumm > 0)
                            BankQuerySummRequested = true;
                        if (Paiments != PaimentMethodT.PaimentsAll)
                        {
                            ToLog(2, "Cashier: BankModulePaiment2: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                        }
                        Paiments = PaimentMethodT.PaimentsAll;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.Paiments = DevicePaiments;
                        DevicePaiments = PaimentMethodT.PaimentsAll;
                        ToLog(0, ResourceManager.GetString("ToLogBankChequeNotSuccess", DefaultCulture));
                        switch (terminalStatus)
                        {
                            case TerminalStatusT.PenalCardPrePaiment:
                            case TerminalStatusT.PenalCardPaiment:
                            case TerminalStatusT.PenalCardRequest:
                                PaymentOff();
                                PenalCardRequestTime = DateTimeNull;
                                if (Paiments != PaimentMethodT.PaimentsAll)
                                {
                                    ToLog(2, "Cashier: BankModulePaiment3: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                                }
                                Paiments = PaimentMethodT.PaimentsAll;
                                if (EmulatorEnabled)
                                    EmulatorForm.Emulator.Paiments = DevicePaiments;
                                log.EndContinues(299, DateTime.Now, null);
                                if (terminalStatus != TerminalStatusT.Empty)
                                    terminalStatus = TerminalStatusT.Empty;
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(2, "Cashier: BankModulePaiment1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                terminalStatusOld = terminalStatus;
                                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                terminalStatusChangeInternal = false;
                                return;
                        }


                        if (EmulatorEnabled)
                        {
                            if (terminalStatus != EmulatorForm.Emulator.terminalStatus)
                            {
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            }
                            EmulatorForm.Emulator.BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                        }
                        BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                        GenerateLabels();
                        if (Delivery)
                            deliveryStatus = DeliveryStatusT.Delivery;
                        else
                            deliveryStatus = DeliveryStatusT.NotDelivery;
                    }
                }
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void PaimentsStatusCheck()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                //ToLog(2, "Cashier: PaimentsStatusCheck: CashAcceptorPaiment");
                CashAcceptorPaiment();
                //ToLog(2, "Cashier: PaimentsStatusCheck: CoinAcceptorPaiment");
                CoinAcceptorPaiment();
                //ToLog(2, "Cashier: PaimentsStatusCheck: BankModulePaiment");
                BankModulePaiment();

                if ((((PaidBanknote + PaidCoin) > 0) || (Paiments == PaimentMethodT.PaimentsBankModule && BankPaid > 0)))
                {
                    if (Paiments == PaimentMethodT.PaimentsBankModule && BankPaid > 0 && ((PaidBanknote > 0) || (PaidCoin > 0)))
                    {
                        if (BankModule != null)
                        {
                            ToLog(1, "Cashier: PaimentsStatusCheck: BankModule.TechnicalCancelation1");
                            BankModule.TechnicalCancelation();
                            if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess && (BankModule.AuthResult != "" || BankModule.AuthReceipt != ""))
                            {
                                BankSlip.Clear();
                                BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                                string Result = BankModule.AuthResult;
                                BankModule.AuthResult = "";
                                string Receipt = BankModule.AuthReceipt;
                                BankModule.AuthReceipt = "";
                                BankModule.CommandStatus = CommandStatusT.Undeifned;
                                Guid Id = Guid.NewGuid();
                                lock (db_lock)
                                {
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: PaymentStatusCheck Utils.Running Enter");
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: PaymentStatusCheck Utils.Running Leave");
                                    try
                                    {
                                        UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                            }
                        }
                        BankSlip.Clear();
                        BankPaid = 0;
                        DevicePaiments = PaimentMethodT.PaimentsCash;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.Paiments = PaimentMethodT.PaimentsCash;
                    }

                    if (BankModule != null && BankModule.IsCancelled && BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                    {
                        ToLog(1, "Cashier: PaimentsStatusCheck: BankModule.TechnicalCancelation2");
                        BankModule.TechnicalCancelation();
                        if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess && (BankModule.AuthResult != "" || BankModule.AuthReceipt != ""))
                        {
                            BankSlip.Clear();
                            BankModuleStatus = BankModuleStatusT.BankCardEmpty;
                            string Result = BankModule.AuthResult;
                            BankModule.AuthResult = "";
                            string Receipt = BankModule.AuthReceipt;
                            BankModule.AuthReceipt = "";
                            BankModule.CommandStatus = CommandStatusT.Undeifned;
                            Guid Id = Guid.NewGuid();
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: PaymentStatusCheck Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: PaymentStatusCheck Utils.Running Leave");
                                try
                                {
                                    UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 27, "Result", Result, "Receipt", Receipt });
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                        }
                        BankPaid = 0;
                    }
                    if ((terminalStatus == TerminalStatusT.PenalCardPrePaiment))
                    {
                        ToLog(1, ResourceManager.GetString("ToLogPenalCardInserting", DefaultCulture));
                        if ((CardDispenser != null) && CardDispenser.IsOpen)
                        {
                            ToLog(2, "Cashier: PaimentsStatusCheck: CardDispenser.SendFromBoxToReadPosition");
                            CardDispenser.SendFromBoxToReadPosition();
                            while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                Thread.Sleep(100);
                            CardReader.SoftStatus = SoftReaderStatusT.Unknown;
                        }
                        CardReader.CardInserted = true;
                        //CardDispensed = false;

                        terminalStatus = TerminalStatusT.PenalCardPaiment;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: PaimentsStatusCheck1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        CardReaderStatusChanged = true;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                    }
                    else if (terminalStatus == TerminalStatusT.PenalCardPaiment)
                    {
                        if (Paiments != PaimentMethodT.PaimentsBankModule) // Cash ��� �����
                        {
                            if (Paid != (PaidBanknote + PaidCoin))
                            {
                                PaimentsStatusChanged = true;
                                DisplayStatusChanged = true;
                                Paid = PaidBanknote + PaidCoin;
                                if (!CardBalancePos && (Paid >= CardQuery))
                                {
                                    terminalStatus = TerminalStatusT.PenalCardPaid;
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: PaimentsStatusCheck2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    terminalStatusChangeInternal = false;
                                }
                            }
                        }
                        else if (Paiments == PaimentMethodT.PaimentsBankModule && BankPaid > 0)
                        {
                            Paid = BankPaid;
                            PaimentsStatusChanged = true;
                            DisplayStatusChanged = true;

                            terminalStatus = TerminalStatusT.PenalCardPaid;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: PaimentsStatusCheck3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            terminalStatusChangeInternal = false;
                        }
                    }
                    else if (terminalStatus == TerminalStatusT.CardReadedMinus ||
                            terminalStatus == TerminalStatusT.CardReadedPlus)
                    {
                        if (Paiments != PaimentMethodT.PaimentsBankModule) // Cash ��� �����
                        {
                            if (Paid != (PaidBanknote + PaidCoin))
                            {
                                PaimentsStatusChanged = true;
                                DisplayStatusChanged = true;
                                Paid = PaidBanknote + PaidCoin;
                                if (ClientType == 0)
                                {
                                    if (!CardBalancePos && (Paid >= CardQuery))
                                    {
                                        terminalStatus = TerminalStatusT.CardPrePaid;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: PaimentsStatusCheck4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                        if (EmulatorEnabled)
                                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                        terminalStatusChangeInternal = false;
                                    }
                                }
                                else
                                {
                                    if (!CardBalancePos && (Paid >= CardQuery))
                                    {
                                        EndPaymentEnabled = true;
                                    }
                                }
                            }
                        }
                        else if (Paiments == PaimentMethodT.PaimentsBankModule && BankPaid > 0)
                        {
                            Paid = BankPaid;
                            PaimentsStatusChanged = true;
                            DisplayStatusChanged = true;
                            terminalStatus = TerminalStatusT.CardPrePaid;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: PaimentsStatusCheck5: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            terminalStatusChangeInternal = false;
                        }
                    }
                }

                if (EmulatorEnabled)
                {
                    if (Paiments != EmulatorForm.Emulator.Paiments)
                    {
                        Paiments = EmulatorForm.Emulator.Paiments;
                        PaimentsStatusChanged = true;

                        if (EmulatorForm.Emulator.Paiments == PaimentMethodT.PaimentsCash)
                        {
                            if (vPaymentBankOn)
                                PaymentBankCardOff();
                            if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                            if (AddingSumm > 0)
                                AddingSumm = 0;
                            if (BankQuerySumm > 0)
                                BankQuerySumm = 0;

                        }
                        else if (EmulatorForm.Emulator.Paiments == PaimentMethodT.PaimentsBankModule)
                        {
                            //if (vPaymentCashOn)
                            PaymentCashOff();
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                        }
                    }
                    if (Paiments != DevicePaiments)
                    {
                        ToLog(2, "Cashier: PaimentsStatusCheck1: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                        Paiments = DevicePaiments;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.Paiments = DevicePaiments;
                        PaimentsStatusChanged = true;

                        if (DevicePaiments == PaimentMethodT.PaimentsCash)
                        {
                            if (vPaymentBankOn)
                                PaymentBankCardOff();
                        }
                        else if (DevicePaiments == PaimentMethodT.PaimentsBankModule)
                        {
                            //if (vPaymentCashOn)
                            PaymentCashOff();
                        }
                    }
                }
                else
                {
                    if (Paiments != DevicePaiments)
                    {
                        ToLog(2, "Cashier: PaimentsStatusCheck2: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                        Paiments = DevicePaiments;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.Paiments = DevicePaiments;
                        PaimentsStatusChanged = true;

                        if (DevicePaiments == PaimentMethodT.PaimentsCash)
                        {
                            if (vPaymentBankOn)
                                PaymentBankCardOff();
                        }
                        else if (DevicePaiments == PaimentMethodT.PaimentsBankModule)
                        {
                            //if (vPaymentCashOn)
                            PaymentCashOff();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ToLog(0, e.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

    }
}