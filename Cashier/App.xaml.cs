﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Threading; // DispatcherUnhandledExceptionEventArgs


namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application, ISingleInstanceApp
    {
        private const string Unique = "Cashier";

    [STAThread]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                var application = new App();

                application.InitializeComponent();
                application.Run();

                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }
        /*public App()
        {

        }*/
        #region ISingleInstanceApp Members

        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            // handle command line arguments of second instance
            // …

            return true;
        }

        #endregion
        //Запуск одной копии приложения
        Mutex mut;
        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool createdNew;
            string mutName = "Cashier";
            mut = new Mutex(true, mutName, out createdNew);
            if (!createdNew)
            {
                Shutdown();
            }
        }
        //Евент для оповещения всех окон приложения
        //public static event EventHandler LanguageChanged;

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs exc)
        {
            string text;
            text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + ": " + exc.ToString();
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"LogCashier.txt"), true))
            {
                file.WriteLine(text);
            }
            exc.Handled = true;
        }
        public static CultureInfo Language
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture;
            }
            /*set
            {
                if (value == null)
                    throw new ArgumentNullException("value");
                if (value == System.Threading.Thread.CurrentThread.CurrentUICulture)
                    return;

                //1. Меняем язык приложения:
                System.Threading.Thread.CurrentThread.CurrentUICulture = value;
                //4. Вызываем евент для оповещения всех окон.
                LanguageChanged(Application.Current, new EventArgs());
            }*/
        }

        private void Application_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //Language = Cashier.Properties.Settings.Default.DefaultLanguage;
        }

        /*private void App_LanguageChanged(Object sender, EventArgs e)
        {
            //Cashier.Properties.Settings.Default.DefaultLanguage = Language;
            Cashier.Properties.Settings.Default.Save();
        }*/
    }

}
