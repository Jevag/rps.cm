using CommonClassLibrary;
using System;
using System.Windows;
using System.Globalization;

namespace Cashier
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void GetExternalServerStatus()
        {
            try
            {
                if (ShiftWork)
                {
                    if (ShiftOpened && (deviceCMModel.ServerExchangeProtocolId != null && deviceCMModel.ServerExchangeProtocolId == 2))
                    {
                        if (!EventWebClientBackgroundWorkerInitialised)
                        {
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftWork InitializeCheckEventWebClientBackgroundWorker");
                            InitializeCheckEventWebClientBackgroundWorker();
                        }
                        if (!CheckEventServerBackgroundWorker.IsBusy)
                        {
                            // Start the asynchronous operation.
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftWork CheckEventServerBackgroundWorker.RunWorkerAsync");
                            CheckEventServerBackgroundWorker.RunWorkerAsync();
                        }


                        if (!EventWebServer)
                        {
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftWork EventWebServer error");
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("TechEventServerCheckReq", DefaultCulture) + Environment.NewLine;
                        }
                    }
                    else
                    {
                        if (EventWebClientBackgroundWorkerInitialised)
                        {
                            if (CheckEventServerBackgroundWorker.IsBusy)
                            {
                                ToLog(2, "Cashier: GetExternalServerStatus: ShiftWork CancelCheckEventWebClient");
                                CancelCheckEventWebClient(null, null);
                            }
                        }

                    }
                }
                else
                {
                    if (deviceCMModel.ServerExchangeProtocolId != null && deviceCMModel.ServerExchangeProtocolId == 2)
                    {
                        if (!EventWebClientBackgroundWorkerInitialised)
                        {
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftNotWork InitializeCheckEventWebClientBackgroundWorker");
                            InitializeCheckEventWebClientBackgroundWorker();
                        }
                        if (!CheckEventServerBackgroundWorker.IsBusy)
                        {
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftNotWork CheckEventServerBackgroundWorker.RunWorkerAsync");
                            // Start the asynchronous operation.
                            CheckEventServerBackgroundWorker.RunWorkerAsync();
                        }


                        if (!EventWebServer)
                        {
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("TechEventServerCheckReq", DefaultCulture) + Environment.NewLine;
                            ToLog(2, "Cashier: GetExternalServerStatus: ShiftNotWork EventWebServer Error");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: GetExternalServerStatus:  exception " + exc.ToString());
            }
        }
        private bool TerminalCheckStatus()
        {
            string s;
            bool res = true;
            try
            {
                Delivery = true;
                TerminalWork_old = TerminalWork;
                TerminalWork = TerminalWorkT.TerminalWork;

                if (CashierType == 0)
                {
                    if ((comDiskret != 0) && (DateTime.Now - LightTime) > new TimeSpan(0, 0, 30))
                    {
                        comDiskret = 0;
                        if (SlaveController != null)
                            SlaveController.ComDiskret = comDiskret;
                        ToLog(2, "Cashier: TerminalCheckStatus: Disable light");
                    }
                }

                if (CashierType != 2)
                {
                    GetDoorStatus();

                    if ((deviceCMModel.SlaveExist != null) && (bool)deviceCMModel.SlaveExist)
                    #region TechForm.SlaveControllerStatus
                    {
                        if (!SlaveControllerWork)
                        #region TechForm.SlaveControllerStatus1
                        {
                            ErrorsText += ResourceManager.GetString("TechSlaveCheckReq", DefaultCulture) + Environment.NewLine;
                            if (TechForm.SlaveControllerRed.Visibility != Visibility.Visible)
                                TechForm.SlaveControllerRed.Visibility = Visibility.Visible;
                            if (TechForm.SlaveControllerGreen.Visibility != Visibility.Hidden)
                                TechForm.SlaveControllerGreen.Visibility = Visibility.Hidden;
                            if (TechForm.SlaveControllerGray.Visibility != Visibility.Hidden)
                                TechForm.SlaveControllerGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.SlaveControllerRed.Visibility != Visibility.Hidden)
                                TechForm.SlaveControllerRed.Visibility = Visibility.Hidden;
                            if (TechForm.SlaveControllerGreen.Visibility != Visibility.Visible)
                                TechForm.SlaveControllerGreen.Visibility = Visibility.Visible;
                            if (TechForm.SlaveControllerGray.Visibility != Visibility.Hidden)
                                TechForm.SlaveControllerGray.Visibility = Visibility.Hidden;
                        }
                        #endregion
                    }
                    else
                    {
                        if (TechForm.SlaveControllerRed.Visibility != Visibility.Hidden)
                            TechForm.SlaveControllerRed.Visibility = Visibility.Hidden;
                        if (TechForm.SlaveControllerGreen.Visibility != Visibility.Hidden)
                            TechForm.SlaveControllerGreen.Visibility = Visibility.Hidden;
                        if (TechForm.SlaveControllerGray.Visibility != Visibility.Visible)
                            TechForm.SlaveControllerGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (deviceCMModel.SlaveExist != null && (bool)deviceCMModel.SlaveExist)
                    #region TechForm.LicenseWorkStatus
                    {
                        if (!LicenseWork)
                        #region TechForm.LicenseWorkStatus1
                        {
                            if (LicenseLevel != AlarmLevelT.Red)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Red;
                                LicenseLevel = AlarmLevelT.Red;
                                Alarm.AlarmSource = AlarmSourceT.License;
                                Alarm.ErrorCode = AlarmCode.LicenseIncorrect;
                                //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                SendAlarm(Alarm);
                            }

                            if (TechForm.LicenseRed.Visibility != Visibility.Visible)
                                TechForm.LicenseRed.Visibility = Visibility.Visible;
                            if (TechForm.LicenseGreen.Visibility != Visibility.Hidden)
                                TechForm.LicenseGreen.Visibility = Visibility.Hidden;

                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("LicenseIncorrect", DefaultCulture) + Environment.NewLine;
                        }
                        else
                        {
                            if (LicenseLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.License;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("LicenseWork", DefaultCulture);
                                SendAlarm(Alarm);
                                LicenseLevel = AlarmLevelT.Green;
                            }
                            if (TechForm.LicenseRed.Visibility != Visibility.Hidden)
                                TechForm.LicenseRed.Visibility = Visibility.Hidden;
                            if (TechForm.LicenseGreen.Visibility != Visibility.Visible)
                                TechForm.LicenseGreen.Visibility = Visibility.Visible;
                        }
                        #endregion
                    }
                    else
                    {
                        if (LicenseLevel != AlarmLevelT.Green)
                        {
                            AlarmT Alarm = new AlarmT();
                            //Alarm.AlarmLevel = AlarmLevelT.Red;
                            LicenseLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.License;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = ResourceManager.GetString("LicenseWork", DefaultCulture);
                            SendAlarm(Alarm);
                        }
                        if (TechForm.LicenseRed.Visibility != Visibility.Hidden)
                            TechForm.LicenseRed.Visibility = Visibility.Hidden;
                        if (TechForm.LicenseGreen.Visibility != Visibility.Visible)
                            TechForm.LicenseGreen.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (ExternalDoorOpen)
                    #region ExternalDoorStatus
                    {
                        if (TechForm.ExternalDoorGray.Visibility != Visibility.Hidden)
                            TechForm.ExternalDoorGray.Visibility = Visibility.Hidden;
                        if (TechForm.ExternalDoorRed.Visibility != Visibility.Visible)
                            TechForm.ExternalDoorRed.Visibility = Visibility.Visible;
                        if (TechForm.ExternalDoorGreen.Visibility != Visibility.Hidden)
                            TechForm.ExternalDoorGreen.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (TechForm.ExternalDoorGray.Visibility != Visibility.Hidden)
                            TechForm.ExternalDoorGray.Visibility = Visibility.Hidden;
                        if (TechForm.ExternalDoorRed.Visibility != Visibility.Hidden)
                            TechForm.ExternalDoorRed.Visibility = Visibility.Hidden;
                        if (TechForm.ExternalDoorGreen.Visibility != Visibility.Visible)
                            TechForm.ExternalDoorGreen.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (CashierType == 0)
                    #region InternalDoorStatus
                    {
                        if (InternalDoorOpen)
                        {
                            if (TechForm.InternalDoorGray.Visibility != Visibility.Hidden)
                                TechForm.InternalDoorGray.Visibility = Visibility.Hidden;
                            if (TechForm.InternalDoorRed.Visibility != Visibility.Visible)
                                TechForm.InternalDoorRed.Visibility = Visibility.Visible;
                            if (TechForm.InternalDoorGreen.Visibility != Visibility.Hidden)
                                TechForm.InternalDoorGreen.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.InternalDoorGray.Visibility != Visibility.Hidden)
                                TechForm.InternalDoorGray.Visibility = Visibility.Hidden;
                            if (TechForm.InternalDoorRed.Visibility != Visibility.Hidden)
                                TechForm.InternalDoorRed.Visibility = Visibility.Hidden;
                            if (TechForm.InternalDoorGreen.Visibility != Visibility.Visible)
                                TechForm.InternalDoorGreen.Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        if (TechForm.InternalDoorGray.Visibility != Visibility.Visible)
                            TechForm.InternalDoorGray.Visibility = Visibility.Visible;
                        if (TechForm.InternalDoorRed.Visibility != Visibility.Hidden)
                            TechForm.InternalDoorRed.Visibility = Visibility.Hidden;
                        if (TechForm.InternalDoorGreen.Visibility != Visibility.Hidden)
                            TechForm.InternalDoorGreen.Visibility = Visibility.Hidden;
                    }
                    #endregion
                    if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                    #region CardDispenserStatus
                    {
                        GetCardDispenserStatus();
                        if (!CardDispenserWork)
                        {
                            if (TechForm.CardDispenserRed.Visibility != Visibility.Visible)
                                TechForm.CardDispenserRed.Visibility = Visibility.Visible;
                            if (TechForm.CardDispenserGreen.Visibility != Visibility.Hidden)
                                TechForm.CardDispenserGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CardDispenserGray.Visibility != Visibility.Hidden)
                                TechForm.CardDispenserGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CardDispenserRed.Visibility != Visibility.Hidden)
                                TechForm.CardDispenserRed.Visibility = Visibility.Hidden;
                            if (TechForm.CardDispenserGreen.Visibility != Visibility.Visible)
                                TechForm.CardDispenserGreen.Visibility = Visibility.Visible;
                            if (TechForm.CardDispenserGray.Visibility != Visibility.Hidden)
                                TechForm.CardDispenserGray.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        CardDispenserWork = false;
                        if (TechForm.CardDispenserRed.Visibility != Visibility.Hidden)
                            TechForm.CardDispenserRed.Visibility = Visibility.Hidden;
                        if (TechForm.CardDispenserGreen.Visibility != Visibility.Hidden)
                            TechForm.CardDispenserGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CardDispenserGray.Visibility != Visibility.Visible)
                            TechForm.CardDispenserGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                }
                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                #region CardReaderStatus
                {
                    GetCardReaderStatus();
                    if (!CardReaderWork)
                    {
                        if (TechForm.CardReaderRed.Visibility != Visibility.Visible)
                            TechForm.CardReaderRed.Visibility = Visibility.Visible;
                        if (TechForm.CardReaderGreen.Visibility != Visibility.Hidden)
                            TechForm.CardReaderGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CardReaderGray.Visibility != Visibility.Hidden)
                            TechForm.CardReaderGray.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (TechForm.CardReaderRed.Visibility != Visibility.Hidden)
                            TechForm.CardReaderRed.Visibility = Visibility.Hidden;
                        if (TechForm.CardReaderGreen.Visibility != Visibility.Visible)
                            TechForm.CardReaderGreen.Visibility = Visibility.Visible;
                        if (TechForm.CardReaderGray.Visibility != Visibility.Hidden)
                            TechForm.CardReaderGray.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    CardReaderWork = false;
                    if (TechForm.CardReaderRed.Visibility != Visibility.Hidden)
                        TechForm.CardReaderRed.Visibility = Visibility.Hidden;
                    if (TechForm.CardReaderGreen.Visibility != Visibility.Hidden)
                        TechForm.CardReaderGreen.Visibility = Visibility.Hidden;
                    if (TechForm.CardReaderGray.Visibility != Visibility.Visible)
                        TechForm.CardReaderGray.Visibility = Visibility.Visible;
                }
                #endregion
                if (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists)
                #region KKMStatus
                {
                    GetKKMStatus();
                    if (!KKMWork)
                    {
                        if (TechForm.KKMRed.Visibility != Visibility.Visible)
                            TechForm.KKMRed.Visibility = Visibility.Visible;
                        if (TechForm.KKMGreen.Visibility != Visibility.Hidden)
                            TechForm.KKMGreen.Visibility = Visibility.Hidden;
                        if (TechForm.KKMGray.Visibility != Visibility.Hidden)
                            TechForm.KKMGray.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (TechForm.KKMRed.Visibility != Visibility.Hidden)
                            TechForm.KKMRed.Visibility = Visibility.Hidden;
                        if (TechForm.KKMGreen.Visibility != Visibility.Visible)
                            TechForm.KKMGreen.Visibility = Visibility.Visible;
                        if (TechForm.KKMGray.Visibility != Visibility.Hidden)
                            TechForm.KKMGray.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    KKMWork = false;
                    if (TechForm.KKMRed.Visibility != Visibility.Hidden)
                        TechForm.KKMRed.Visibility = Visibility.Hidden;
                    if (TechForm.KKMGreen.Visibility != Visibility.Hidden)
                        TechForm.KKMGreen.Visibility = Visibility.Hidden;
                    if (TechForm.KKMGray.Visibility != Visibility.Visible)
                        TechForm.KKMGray.Visibility = Visibility.Visible;
                }
                #endregion
                if (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists)
                #region PrinterStatus
                {
                    GetPrinterStatus();
                }
                #endregion
                if (CashierType != 2)
                {
                    if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist)
                    #region CashAcceptorStatus
                    {
                        GetCashAcceptorStatus();
                        if (!CashAcceptorWork)
                        {
                            if (TechForm.CashAcceptorRed.Visibility != Visibility.Visible)
                                TechForm.CashAcceptorRed.Visibility = Visibility.Visible;
                            if (TechForm.CashAcceptorGreen.Visibility != Visibility.Hidden)
                                TechForm.CashAcceptorGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CashAcceptorGray.Visibility != Visibility.Hidden)
                                TechForm.CashAcceptorGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CashAcceptorRed.Visibility != Visibility.Hidden)
                                TechForm.CashAcceptorRed.Visibility = Visibility.Hidden;
                            if (TechForm.CashAcceptorGreen.Visibility != Visibility.Visible)
                                TechForm.CashAcceptorGreen.Visibility = Visibility.Visible;
                            if (TechForm.CashAcceptorGray.Visibility != Visibility.Hidden)
                                TechForm.CashAcceptorGray.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        CashAcceptorWork = false;
                        if (TechForm.CashAcceptorRed.Visibility != Visibility.Hidden)
                            TechForm.CashAcceptorRed.Visibility = Visibility.Hidden;
                        if (TechForm.CashAcceptorGreen.Visibility != Visibility.Hidden)
                            TechForm.CashAcceptorGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CashAcceptorGray.Visibility != Visibility.Visible)
                            TechForm.CashAcceptorGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist)
                    #region CashDispenserStatus
                    {
                        GetCashDispenserStatus();
                        if (!CashDispenserWork)
                        {
                            if (TechForm.CashDispenserRed.Visibility != Visibility.Visible)
                                TechForm.CashDispenserRed.Visibility = Visibility.Visible;
                            if (TechForm.CashDispenserGreen.Visibility != Visibility.Hidden)
                                TechForm.CashDispenserGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CashDispenserGray.Visibility != Visibility.Hidden)
                                TechForm.CashDispenserGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CashDispenserRed.Visibility != Visibility.Hidden)
                                TechForm.CashDispenserRed.Visibility = Visibility.Hidden;
                            if (TechForm.CashDispenserGreen.Visibility != Visibility.Visible)
                                TechForm.CashDispenserGreen.Visibility = Visibility.Visible;
                            if (TechForm.CashDispenserGray.Visibility != Visibility.Hidden)
                                TechForm.CashDispenserGray.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        CashDispenserWork = false;
                        if (TechForm.CashDispenserRed.Visibility != Visibility.Hidden)
                            TechForm.CashDispenserRed.Visibility = Visibility.Hidden;
                        if (TechForm.CashDispenserGreen.Visibility != Visibility.Hidden)
                            TechForm.CashDispenserGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CashDispenserGray.Visibility != Visibility.Visible)
                            TechForm.CashDispenserGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
                    #region CoinAcceptorStatus
                    {
                        GetCoinAcceptorStatus();
                        if (!CoinAcceptorWork)
                        {
                            if (TechForm.CoinAcceptorRed.Visibility != Visibility.Visible)
                                TechForm.CoinAcceptorRed.Visibility = Visibility.Visible;
                            if (TechForm.CoinAcceptorGreen.Visibility != Visibility.Hidden)
                                TechForm.CoinAcceptorGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CoinAcceptorGray.Visibility != Visibility.Hidden)
                                TechForm.CoinAcceptorGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CoinAcceptorRed.Visibility != Visibility.Hidden)
                                TechForm.CoinAcceptorRed.Visibility = Visibility.Hidden;
                            if (TechForm.CoinAcceptorGreen.Visibility != Visibility.Visible)
                                TechForm.CoinAcceptorGreen.Visibility = Visibility.Visible;
                            if (TechForm.CoinAcceptorGray.Visibility != Visibility.Hidden)
                                TechForm.CoinAcceptorGray.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        CoinAcceptorWork = false;
                        if (TechForm.CoinAcceptorRed.Visibility != Visibility.Hidden)
                            TechForm.CoinAcceptorRed.Visibility = Visibility.Hidden;
                        if (TechForm.CoinAcceptorGreen.Visibility != Visibility.Hidden)
                            TechForm.CoinAcceptorGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CoinAcceptorGray.Visibility != Visibility.Visible)
                            TechForm.CoinAcceptorGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                    if (deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist)
                    #region HopperStatus
                    {
                        GetHopperStatus();
                        if (!Hopper1Work)
                        {
                            if (TechForm.CoinHopperLeftRed.Visibility != Visibility.Visible)
                                TechForm.CoinHopperLeftRed.Visibility = Visibility.Visible;
                            if (TechForm.CoinHopperLeftGreen.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperLeftGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CoinHopperLeftGray.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperLeftGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CoinHopperLeftRed.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperLeftRed.Visibility = Visibility.Hidden;
                            if (TechForm.CoinHopperLeftGreen.Visibility != Visibility.Visible)
                                TechForm.CoinHopperLeftGreen.Visibility = Visibility.Visible;
                            if (TechForm.CoinHopperLeftGray.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperLeftGray.Visibility = Visibility.Hidden;
                        }
                        if (!Hopper2Work)
                        {
                            if (TechForm.CoinHopperRightRed.Visibility != Visibility.Visible)
                                TechForm.CoinHopperRightRed.Visibility = Visibility.Visible;
                            if (TechForm.CoinHopperRightGreen.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperRightGreen.Visibility = Visibility.Hidden;
                            if (TechForm.CoinHopperRightGray.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperRightGray.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            if (TechForm.CoinHopperRightRed.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperRightRed.Visibility = Visibility.Hidden;
                            if (TechForm.CoinHopperRightGreen.Visibility != Visibility.Visible)
                                TechForm.CoinHopperRightGreen.Visibility = Visibility.Visible;
                            if (TechForm.CoinHopperRightGray.Visibility != Visibility.Hidden)
                                TechForm.CoinHopperRightGray.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        Hopper1Work = false;
                        Hopper2Work = false;
                        if (TechForm.CoinHopperLeftRed.Visibility != Visibility.Hidden)
                            TechForm.CoinHopperLeftRed.Visibility = Visibility.Hidden;
                        if (TechForm.CoinHopperLeftGreen.Visibility != Visibility.Hidden)
                            TechForm.CoinHopperLeftGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CoinHopperLeftGray.Visibility != Visibility.Visible)
                            TechForm.CoinHopperLeftGray.Visibility = Visibility.Visible;
                        if (TechForm.CoinHopperRightRed.Visibility != Visibility.Hidden)
                            TechForm.CoinHopperRightRed.Visibility = Visibility.Hidden;
                        if (TechForm.CoinHopperRightGreen.Visibility != Visibility.Hidden)
                            TechForm.CoinHopperRightGreen.Visibility = Visibility.Hidden;
                        if (TechForm.CoinHopperRightGray.Visibility != Visibility.Visible)
                            TechForm.CoinHopperRightGray.Visibility = Visibility.Visible;
                    }
                    #endregion
                }
                if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist)
                {
                    #region BankModuleStatus
                    GetBankModuleStatus();
                    if (!BankModuleWork)
                    {
                        if (TechForm.BankModuleRed.Visibility != Visibility.Visible)
                            TechForm.BankModuleRed.Visibility = Visibility.Visible;
                        if (TechForm.BankModuleGreen.Visibility != Visibility.Hidden)
                            TechForm.BankModuleGreen.Visibility = Visibility.Hidden;
                        if (TechForm.BankModuleGray.Visibility != Visibility.Hidden)
                            TechForm.BankModuleGray.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (TechForm.BankModuleRed.Visibility != Visibility.Hidden)
                            TechForm.BankModuleRed.Visibility = Visibility.Hidden;
                        if (TechForm.BankModuleGreen.Visibility != Visibility.Visible)
                            TechForm.BankModuleGreen.Visibility = Visibility.Visible;
                        if (TechForm.BankModuleGray.Visibility != Visibility.Hidden)
                            TechForm.BankModuleGray.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    BankModuleWork = false;
                    if (TechForm.BankModuleRed.Visibility != Visibility.Hidden)
                        TechForm.BankModuleRed.Visibility = Visibility.Hidden;
                    if (TechForm.BankModuleGreen.Visibility != Visibility.Hidden)
                        TechForm.BankModuleGreen.Visibility = Visibility.Hidden;
                    if (TechForm.BankModuleGray.Visibility != Visibility.Visible)
                        TechForm.BankModuleGray.Visibility = Visibility.Visible;
                }
                #endregion
                GetExternalServerStatus();

                if (TerminalWork == TerminalWorkT.TerminalWork)
                {

                    if (CashierType != 2)
                    {
                        if (
                        (terminalStatus == TerminalStatusT.CardReadedPlus) ||
                        (terminalStatus == TerminalStatusT.CardReadedMinus) ||
                        //(terminalStatus == TerminalStatusT.PenalCardRequest) ||
                        (terminalStatus == TerminalStatusT.PenalCardPrePaiment) ||
                        (terminalStatus == TerminalStatusT.PenalCardPaiment)
                        )
                        {
                            PaymentOn();
                        }
                        else
                        {
                            PaymentOff();
                        }
                    }
                }
                if (ShiftWork)
                {
                    if (!ShiftOpened)
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                        ErrorsText += ResourceManager.GetString("ShiftClosed", DefaultCulture) + Environment.NewLine;
                        if (CashierType == 0)
                        {
                            if (InternalDoorOpen)
                            {
                                TechForm.ShiftPanelHeading.IsEnabled = true;
                                TechForm.PrintZReportPanel.IsEnabled = true;
                                TechForm.InkassationPanel.IsEnabled = true;
                            }
                            else
                            {
                                TechForm.ShiftPanelHeading.IsEnabled = false;
                                TechForm.PrintZReportPanel.IsEnabled = false;
                                TechForm.InkassationPanel.IsEnabled = false;
                            }
                        }
                        else //deviceCMModel.CashierLight==true
                        {
                            TechForm.ShiftPanelHeading.IsEnabled = true;
                            TechForm.PrintZReportPanel.IsEnabled = true;
                            TechForm.InkassationPanel.IsEnabled = true;
                        }

                    }
                }

                if (ShiftWork)
                {
                    if (TechForm.ShiftButton.Visibility != Visibility.Visible)
                        TechForm.ShiftButton.Visibility = Visibility.Visible;
                }
                else
                {
                    if (TechForm.ShiftButton.Visibility == Visibility.Visible)
                        TechForm.ShiftButton.Visibility = Visibility.Hidden;
                }

                if (CashierType == 1)
                {
                    if (!TechForm.ShiftButton.IsEnabled)
                        TechForm.ShiftButton.IsEnabled = true;
                    if (!TechForm.DeliveryButton.IsEnabled)
                        TechForm.DeliveryButton.IsEnabled = true;
                }
                else if (CashierType == 2)
                {
                    if (!TechForm.ShiftButton.IsEnabled)
                        TechForm.ShiftButton.IsEnabled = true;
                    if (TechForm.DeliveryButton.IsEnabled)
                        TechForm.DeliveryButton.IsEnabled = false;
                }
                else
                {
                    if (InternalDoorOpen)
                    {
                        if (!TechForm.ShiftButton.IsEnabled)
                            TechForm.ShiftButton.IsEnabled = true;
                        if (!TechForm.DeliveryButton.IsEnabled)
                            TechForm.DeliveryButton.IsEnabled = true;
                    }
                    else
                    {
                        if (TechForm.ShiftButton.IsEnabled)
                            TechForm.ShiftButton.IsEnabled = false;
                        if (TechForm.DeliveryButton.IsEnabled)
                            TechForm.DeliveryButton.IsEnabled = false;
                    }
                }
                if (!KKMWork)
                {
                    if (terminalStatus != TerminalStatusT.PaymentError)
                        TerminalWork = TerminalWorkT.TerminalDontWork;
                }
                if (CashierType != 2)
                {
                    if (!CashAcceptorWork &&
                     (
                       (Paid > 0) &&
                         (
                           (terminalStatus == TerminalStatusT.CardReadedPlus) ||
                           (terminalStatus == TerminalStatusT.CardReadedMinus) ||
                           (terminalStatus == TerminalStatusT.PenalCardPaiment)
                         )
                     )
                   )
                    {
                        if (!CashAcceptorInhibit)
                            CashAcceptorInhibit = true;
                        if (vPaymentBankOn)
                            PaymentBankCardOff();

                        PaymentErrorInfo.CashAcceptorError = true;
                        PaymentErrorInfo.Error = true;
                        if ((Transaction.ToPay - Paid) > 0)
                        {
                            CardInfo.LastPaymentTime = (int)(DateTime.Now - datetime0).TotalSeconds;
                            //transactions.TimeOplat = DateTime.Now;
                        }

                        switch (FreeTimeTypeId)
                        {
                            case 2:
                                DateExitEstimated1 = DateTime.Now + TimeSpan.FromHours(FreeTime);
                                break;
                            case 3:
                                DateExitEstimated1 = DateTime.Now + TimeSpan.FromDays(FreeTime);
                                break;
                            default:
                                DateExitEstimated1 = DateTime.Now + TimeSpan.FromMinutes(FreeTime);
                                break;
                        }

                        if ((DateExitEstimated != null) && (DateExitEstimated1 > DateExitEstimated))
                            DateExitEstimated = DateExitEstimated1;

                        CardInfo.SumOnCard += (int)Paid;

                        if (IsPenalCard)
                            CardInfo.ClientTypidFC = 0;

                        {
                            s = string.Format(ResourceManager.GetString("ToLogSumWriting", DefaultCulture), CardInfo.SumOnCard.ToString(), sValuteLabel); //ResDiff = 0
                            ToLog(1, s);
                            DateTime dt = DateTime.Now;
                            ToLog(1, ResourceManager.GetString("ToLogTimeWriting", DefaultCulture) + " " + dt.ToString(DefaultCulture));
                            ToLog(2, "Cashier: TerminalCheckStatus CashAcceptorError: WriteCard ");
                            CardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // ����� ���������� ������
                            LogWriteCard();
                            if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                                WriteCard();
                            else
                                SendCardTransaction();

                            ToLog(2, "Cashier: DoSwitchTerminalStatusCancelled: - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                            if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                            {
                                if (!EmulatorEnabled || !EmulatorForm.Emulator.CardReaderVirt.Checked)
                                    WriteCard();

                                ToLog(2, "Cashier: TerminalCheckStatus CashAcceptorError: WriteCard double - CardReader.SoftStatus " + CardReader.SoftStatus.ToString());
                                if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                                {
                                    PaymentErrorInfo.CardWriteError = true;
                                }
                            }
                        }

                        s = string.Format(ResourceManager.GetString("ToLogEjectingChequeDelivery", DefaultCulture), Paid.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), DownDiff.ToString("F0"));
                        ToLog(1, s);

                        ToLog(2, "Cashier: TerminalCheckStatus: Paid > 0 CashAcceptor Not Work GiveCheque " + Paid.ToString() + " " + Paid.ToString() + " " + Paiments.ToString());
                        GiveCheque(0, Paid, Paid, Paiments, 0, null, BankSlip);
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                        transactions.PaymentTransactionTypeId = (int)PaymentTransactionTypeT.UnclosedPayment;

                        terminalStatus = TerminalStatusT.LongExitOperation;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: TerminalCheckStatus1: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        terminalStatusOld = terminalStatus;
                    }
                }
                if (CashierType != 2)
                {
                    if (!CashAcceptorWork && !CoinAcceptorWork && !BankModuleWork)
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError &&
                            terminalStatus != TerminalStatusT.LongExitOperation)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                        ErrorsText += ResourceManager.GetString("PaimentDevicesNotExists", DefaultCulture) + Environment.NewLine;
                    }
                }
                if (CashierType != 2)
                {
                    if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists && !CardDispenserWork)
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                    }
                    if (!CardReaderWork)
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                }
                else
                {
                    if (!CardReaderWork)
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                }

                if (CashierType != 2)
                {
                    if (TerminalWork != TerminalWorkT.TerminalDontWork)
                    {
                        if (terminalStatus == TerminalStatusT.Empty)
                        {
                            if ((CashAcceptorWork && CashAcceptorStatusLabel != "") || (CoinAcceptorWork && CoinAcceptorStatusLabel != ""))
                            {
                                if (BankModuleWork)
                                {
                                    PaymentStatusLabel = Properties.Resources.CashAndBank;
                                    //PaymentStatusLabel1 = Properties.Resources.CashAndBank1;
                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.paiments_main = PaimentMethodT.PaimentsAll;
                                }
                                else
                                {
                                    PaymentStatusLabel = Properties.Resources.Cash;
                                    //PaymentStatusLabel1 = Properties.Resources.Cash1;
                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.paiments_main = PaimentMethodT.PaimentsCash;
                                }
                            }
                            else
                            {
                                if (BankModuleWork)
                                {
                                    PaymentStatusLabel = Properties.Resources.Bank;
                                    //PaymentStatusLabel1 = Properties.Resources.Bank1;
                                    //deliveryStatus = DeliveryStatusT.Undefined;
                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.paiments_main = PaimentMethodT.PaimentsBankModule;
                                    Delivery = false;
                                }
                            }
                        }
                        if (TerminalWork != TerminalWorkT.TerminalWork)
                            TerminalWork = TerminalWorkT.TerminalWork;
                    }
                }

                if (TerminalWork_old != TerminalWork)
                    TerminalStatusChanged = true;


                return TerminalStatusChanged;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return false;
            }
        }
    }
}