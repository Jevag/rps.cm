﻿
using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime CardDispenserAlarmTime;
        DateTime CardDispenserCardShortageDT;
        void HardCardDispenser()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if ((CardDispenser != null) && !CardDispenser.IsOpen)
                {
                    if ((CardDispenserStatusCheckDT == null) || (CardDispenserStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                    {
                        CardDispenser.Open(CardDispenserPort);
                        CardDispenserStatusCheckDT = DateTime.Now;
                    }
                    if (CardDispenser.IsOpen)
                        CardDispenser.RecycleToReadPosition();
                }
                if (CardDispenser.IsOpen)
                {
                    if (CardDispenser.CardDispenserStatus.CommunicationsError)
                    {
                        if (CardDispenserWork)
                        {
                            CardDispenserWorkChanged = true;
                            CardDispenserWork = false;
                        }

                        if (TechForm.CardDispenserPortLabel.Foreground != Brushes.Red)
                            TechForm.CardDispenserPortLabel.Foreground = Brushes.Red;
                        ErrorsText += ResourceManager.GetString("CardDispenserCommunicationError", DefaultCulture) + Environment.NewLine;
                        if (CardDispenserLevel != AlarmLevelT.Brown)
                        {
                            if (CardDispenserAlarmTime == DateTime.MinValue)
                            {
                                CardDispenserAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (CardDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    CardDispenserLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                    Alarm.ErrorCode = AlarmCode.CardDispenserNotConnect;
                                    SendAlarm(Alarm);
                                    CardDispenserAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                        else
                        {
                            if (TechForm.CardDispenserPortLabel.Foreground != Brushes.Green)
                                TechForm.CardDispenserPortLabel.Foreground = Brushes.Green;
                        }
                    }
                    else if (CardDispenser.CardDispenserStatus.CardError)
                    {
                        if (CardDispenserWork)
                        {
                            CardDispenserWorkChanged = true;
                            CardDispenserWork = false;
                        }
                        ErrorsText += ResourceManager.GetString("ToLogCardJam", DefaultCulture) + Environment.NewLine;
                        if (CardDispenserLevel != AlarmLevelT.Brown)
                        {
                            if (CardDispenserAlarmTime == DateTime.MinValue)
                            {
                                CardDispenserAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (CardDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    CardDispenserLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                    Alarm.ErrorCode = AlarmCode.CardDispenserJam;
                                    SendAlarm(Alarm);
                                    CardDispenserAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                        if (CardDispenserCardInsertEnabled)
                            CardDispenserCardInsertEnabled = false;
                    }
                    else
                    /* Пока не поправили (нет карт в тубусе, а не вообще в диспенсере) - не выдаем аларм*/
                    /*if (CardDispenser.CardDispenserStatus.CardEmpty)
                        {
                            if (CardDispenserLevel != AlarmLevelT.Brown)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                CardDispenserLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                Alarm.ErrorCode = 241;
                                Alarm.Status = ResourceManager.GetString("CardDispenserStatusCardEmpty", DefaultCulture);
                                SendAlarm(Alarm);
                            }
                        }
                        else */
                    if (CardDispenser.CardDispenserStatus.CardShortage)
                    {
                        if (!CardDispenserWork)
                        {
                            CardDispenserWorkChanged = true;
                            CardDispenserWork = true;
                        }
                        if (CardDispenserLevel != AlarmLevelT.Yellow)
                        {
                            if (CardDispenserCardShortageDT == DateTime.MinValue)
                            {
                                CardDispenserCardShortageDT = DateTime.Now;
                            }
                            else
                            {
                                if (CardDispenserCardShortageDT.AddMinutes(3) < DateTime.Now)
                                {
                                    ErrorsText += ResourceManager.GetString("CardDispenserStatusCardShortage", DefaultCulture) + Environment.NewLine;
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                    CardDispenserLevel = AlarmLevelT.Yellow;
                                    Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                    Alarm.ErrorCode = AlarmCode.CardDispenserNearEmpty;
                                    SendAlarm(Alarm);
                                    CardDispenserCardShortageDT = DateTime.MinValue;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!CardDispenserWork)
                        {
                            CardDispenserWorkChanged = true;
                            CardDispenserWork = true;
                        }
                        if (CardDispenserCardShortageDT != DateTime.MinValue)
                            CardDispenserCardShortageDT = DateTime.MinValue;
                        if (CardDispenserAlarmTime != DateTime.MinValue)
                            CardDispenserAlarmTime = DateTime.MinValue;
                        if (CardDispenserLevel != AlarmLevelT.Green)
                        {

                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = ResourceManager.GetString("CardDispenserWork", DefaultCulture);
                            SendAlarm(Alarm);
                            CardDispenserLevel = AlarmLevelT.Green;
                        }
                    }
                    CardDispenserStatus = CardDispenser.CardDispenserStatus;
                }
                else
                {
                    if (CardDispenserWork)
                    {
                        CardDispenserWorkChanged = true;
                        CardDispenserWork = false;
                    }
                    if (TechForm.CardDispenserPortLabel.Foreground != Brushes.Red)
                        TechForm.CardDispenserPortLabel.Foreground = Brushes.Red;
                    ErrorsText += ResourceManager.GetString("CardDispenserCommunicationError", DefaultCulture) + Environment.NewLine;
                    if (CardDispenserLevel != AlarmLevelT.Brown)
                    {
                        if (CardDispenserAlarmTime == DateTime.MinValue)
                        {
                            CardDispenserAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CardDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                AlarmT Alarm = new AlarmT();
                                CardDispenserLevel = AlarmLevelT.Brown;
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                Alarm.ErrorCode = AlarmCode.CardDispenserNotConnect;
                                SendAlarm(Alarm);
                                CardDispenserAlarmTime = DateTime.MinValue;
                            }
                        }
                    }
                    ErrorsText += ResourceManager.GetString("CardDispenserCommunicationError", DefaultCulture) + Environment.NewLine;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void GetCardDispenserStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    if (CardDispenser == null)
                    {
                        ToLog(2, "Cashier: GetCardDispenserStatus Creating CardDispenserMT166");
                        CardDispenser = new CardDispenserMT166(log);
                    }

                    #region OtherDevicesPanel.CardDispenserExists.Checked
                    if (EmulatorEnabled)
                    {
                        if (!EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                            EmulatorForm.Emulator.CardDispenserGroupBox.Show();
                        if (EmulatorForm.Emulator.CarNumberPanel.Visible)
                            EmulatorForm.Emulator.CarNumberPanel.Hide();
                        if (EmulatorEnabled && EmulatorForm.Emulator.CardDispenserVirt.Checked)
                        {
                            if (EmulatorForm.Emulator.CardDispenserWork.Checked)
                            {
                                if (EmulatorForm.Emulator.CardDispenserWork.Checked != CardDispenserWork)
                                {
                                    CardDispenserWork = EmulatorForm.Emulator.CardDispenserWork.Checked;
                                    TerminalStatusChanged = true;
                                }
                                if ((CardDispenser != null) && CardDispenser.IsOpen)
                                {
                                    TerminalStatusChanged = true;
                                    CardDispenser.Close();
                                }
                                if (!EmulatorForm.Emulator.CardDispenserWork.Checked)
                                {
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    ErrorsText += ResourceManager.GetString("TechCardDispenserCheckReq", DefaultCulture) + Environment.NewLine;
                                    TerminalStatusChanged = true;
                                    if (CardDispenserWork)
                                    {
                                        CardDispenserWorkChanged = true;
                                        CardDispenserWork = false;
                                    }
                                    if (CardDispenserLevel != AlarmLevelT.Brown)
                                    {
                                        if (CardDispenserAlarmTime == DateTime.MinValue)
                                        {
                                            CardDispenserAlarmTime = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (CardDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                                            {
                                                AlarmT Alarm = new AlarmT();
                                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                CardDispenserLevel = AlarmLevelT.Brown;
                                                Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                                Alarm.ErrorCode = AlarmCode.CardDispenserNotConnect;
                                                SendAlarm(Alarm);
                                                CardDispenserAlarmTime = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (CardDispenserAlarmTime != DateTime.MinValue)
                                        CardDispenserAlarmTime = DateTime.MinValue;
                                    if (CardDispenserLevel != AlarmLevelT.Green)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Green;
                                        Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                                        Alarm.Status = ResourceManager.GetString("CardDispenserWork", DefaultCulture);
                                        SendAlarm(Alarm);
                                        CardDispenserLevel = AlarmLevelT.Green;
                                    }
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                                if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                                if (CardDispenserWork)
                                {
                                    CardDispenserWorkChanged = true;
                                    CardDispenserWork = false;
                                }
                            }
                        }
                        else
                        {
                            HardCardDispenser();
                        }
                    }
                    else
                    {
                        HardCardDispenser();
                    }
                    #endregion OtherDevicesPanel.CardDispenserExists.Checked
                }
                else
                {
                    #region OtherDevicesPanel.CardDispenserExists.NotChecked
                    if (CardDispenserWork)
                    {
                        CardDispenserWorkChanged = true;
                        CardDispenserWork = false;
                    }
                    if (EmulatorEnabled)
                    {
                        EmulatorForm.Emulator.CardDispenserGroupBox.Hide();
                        EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                    }
                    #endregion OtherDevicesPanel.CardDispenserExists.NotChecked
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void SoftCardDispenserWork()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (EmulatorForm.Emulator.terminalStatus != terminalStatus)
                {
                    TerminalStatusChanged = true;
                    if (terminalStatusChangeInternal)
                    {
                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    }
                    else
                    {
                        terminalStatus = EmulatorForm.Emulator.terminalStatus;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: SoftCardDispenserWork1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                    }
                    terminalStatusChangeInternal = false;
                    if (terminalStatus == TerminalStatusT.CardPaid)
                    {
                        PaimentsStatusChanged = true;
                    }
                }
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: SoftCardDispenserWork2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                if (terminalStatus == TerminalStatusT.CardEjected)
                {
                    #region Карта выдана в губы
                    if (CardEjectTime == null)
                    {
                        TerminalStatusChanged = true;
                        ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                        if (terminalStatus != TerminalStatusT.Empty)
                            terminalStatus = TerminalStatusT.Empty;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: SoftCardDispenserWork3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        DoSwitchTerminalStatus();
                    }
                    else if ((DateTime)CardEjectTime != DateTimeNull)
                    {
                        TimeSpan CardWait = DateTime.Now - (DateTime)CardEjectTime;
                        TimeSpan TimeOutToBasket = new TimeSpan(0, 0, CardTimeOutToBasket);
                        if (CardWait > TimeOutToBasket)
                        {
                            CardEjectTime = DateTimeNull;
                            TerminalStatusChanged = true;
                            ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                            if (terminalStatus != TerminalStatusT.Empty)
                                terminalStatus = TerminalStatusT.Empty;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: SoftCardDispenserWork4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            terminalStatusChangeInternal = false;
                            DoSwitchTerminalStatus();
                        }
                    }
                    #endregion Карта выдана в губы
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        bool CardDispenserStatusCardPreSendPos = false;
        bool CardDispenserStatusCardDespensePos = false;
        void HardCardDispenserWork()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (CardDispenserWork)
                {
                    CardDispenserGetStatus();
                    if (CardDispenserStatus.CommunicationsError)
                    {
                        ToLog(0, ResourceManager.GetString("ToLogCardDispenserError", DefaultCulture));
                        return;
                    }
                    else

                    if (CardDispenserStatusCardPreSendPos != CardDispenserStatus.CardPreSendPos)
                    {
                        ToLog(2, "Cashier: HardCardDispenserWork: Status: CardPreSendPos changed from " + CardDispenserStatusCardPreSendPos.ToString() + " to " + CardDispenserStatus.CardPreSendPos.ToString());
                        CardDispenserStatusCardPreSendPos = CardDispenserStatus.CardPreSendPos;
                    }
                    if (CardDispenserStatusCardDespensePos != CardDispenserStatus.CardDespensePos)
                    {
                        ToLog(2, "Cashier: HardCardDispenserWork: Status: CardDespensePos changed from " + CardDispenserStatusCardDespensePos.ToString() + " to " + CardDispenserStatus.CardDespensePos.ToString());
                        CardDispenserStatusCardDespensePos = CardDispenserStatus.CardDespensePos;
                    }

                    if (!CardDispenserStatus.CardAccepting && //В движении
                        !CardDispenserStatus.CardDispensing) //В движении
                    {
                        if (CardDispenserStatus.CardPreSendPos) // Под ридером
                        {
                            #region Карта под ридером
                            if (TerminalWork == TerminalWorkT.TerminalDontWork)
                            {
                                ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));
                                if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                                {
                                    if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                                    {
                                        ToLog(2, "Cashier: HardCardDispenserWork: TerminalDontWork CardDispenser.SendCardToBezelAndHold");
                                        CardDispenser.SendCardToBezelAndHold();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                        if (CardTimeOutToBasket > 0)
                                            CardEjectTime = DateTime.Now;
                                    }
                                }
                                return;
                            }
                            if (!CardReader.CardInserted)
                            {
                                CardReader.CardInserted = true;
                            }
                            if ((terminalStatus == TerminalStatusT.Empty) || (terminalStatus == TerminalStatusT.CardEjected))
                            {
                                TerminalStatusChanged = true;
                                terminalStatus = TerminalStatusT.CardInserted;
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(2, "Cashier: HardCardDispenserWork1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                terminalStatusOld = terminalStatus;
                                if (EmulatorEnabled)
                                {
                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    if (EmulatorForm.Emulator.CardReaderVirt.Checked)
                                    {
                                        DateTime dt = DateTime.Now;
                                        int hours = rnd.Next(1, 24);
                                        TimeSpan ts = new TimeSpan(hours, 0, 0);
                                        DateTime dt1 = dt.Subtract(ts);
                                        EmulatorForm.Emulator.ReaderEntranceTime.Text = dt1.ToString(dt_format);
                                    }
                                    EmulatorForm.Emulator.Paiments = EmulatorForm.Emulator.paiments_main;
                                }
                                terminalStatusChangeInternal = false;
                                if (CardReaderWork)
                                    ToLog(1, ResourceManager.GetString("ToLogCardInserted", DefaultCulture));
                            }
                            else
                                if (
                                (terminalStatus == TerminalStatusT.PenalCardRequest)
                                    ||
                                (terminalStatus == TerminalStatusT.PenalCardPrePaiment)
                                    )
                            {
                                terminalStatus = TerminalStatusT.PenalCardPaiment;
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(2, "Cashier: HardCardDispenserWork2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                TerminalStatusChanged = true;
                                terminalStatusOld = terminalStatus;
                                terminalStatusChangeInternal = false;
                                if (EmulatorEnabled)
                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                ToLog(1, ResourceManager.GetString("ToLogPenalCardInserted", DefaultCulture));
                            }
                            #endregion Карта под ридером
                        }
                        else if (CardDispenserStatus.CardDespensePos) // В губах
                        {
                            #region Карта в губах
                            if (terminalStatus != TerminalStatusT.Empty && terminalStatus != TerminalStatusT.CardInserted
                                && terminalStatus != TerminalStatusT.LongExitOperation && terminalStatus != TerminalStatusT.PaymentError)
                            {
                                if (CardReader.CardInserted)
                                    CardReader.CardInserted = false;
                                TerminalStatusChanged = true;
                                if (terminalStatus != TerminalStatusT.CardEjected)
                                {
                                    terminalStatus = TerminalStatusT.CardEjected;
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: HardCardDispenserWork3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                }
                                if (EmulatorEnabled)
                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                terminalStatusChangeInternal = false;
                                if (CardEjectTime == null)
                                {
                                    if ((CardDispenser != null) && CardDispenser.IsOpen)
                                    {
                                        ToLog(2, "Cashier: HardCardDispenserWork: CardEjectTime is null CardDispenser.SendCardToRecycleBox");
                                        CardDispenser.SendCardToRecycleBox();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                    }
                                    ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                                    if (terminalStatus != TerminalStatusT.Empty)
                                        terminalStatus = TerminalStatusT.Empty;
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: HardCardDispenserWork4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                    if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    terminalStatusChangeInternal = false;
                                    //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: DoSwitchTerminalStatus");
                                    DoSwitchTerminalStatus();
                                }
                                else if ((DateTime)CardEjectTime != DateTimeNull)
                                {
                                    TimeSpan CardWait = DateTime.Now - (DateTime)CardEjectTime;
                                    TimeSpan TimeOutToBasket = new TimeSpan(0, 0, CardTimeOutToBasket);
                                    if (CardWait > TimeOutToBasket)
                                    {
                                        CardEjectTime = DateTimeNull;
                                        TerminalStatusChanged = true;
                                        if ((CardDispenser != null) && CardDispenser.IsOpen)
                                        {
                                            ToLog(2, "Cashier: HardCardDispenserWork: CardTimeout CardDispenser.SendCardToRecycleBox");
                                            CardDispenser.SendCardToRecycleBox();
                                            while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                                Thread.Sleep(100);
                                        }
                                        ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                                        if (terminalStatus != TerminalStatusT.Empty)
                                            terminalStatus = TerminalStatusT.Empty;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardDispenserWork5: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                        terminalStatusChangeInternal = false;
                                        if (EmulatorEnabled)
                                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                        DoSwitchTerminalStatus();
                                    }
                                }
                            }
                            #endregion Карта в губах
                        }
                        else if (!CardDispenserStatus.CardDespensePos &&
                            !CardDispenserStatus.CardPreSendPos &&
                            terminalStatus != TerminalStatusT.PenalCardRequest &&
                            terminalStatus != TerminalStatusT.PenalCardPrePaiment)
                        {
                            #region Карта извлечена
                            if (!CardReader.CardExists)
                            {
                                if (
                                    (terminalStatus != TerminalStatusT.Empty) &&
                                    (terminalStatus != TerminalStatusT.CardReadedError) &&
                                    (CardReader.SoftStatus != SoftReaderStatusT.TimeOut) &&
                                    //(CardReader.SoftStatus != SoftReaderStatusT.Unknown) &&
                                    (CardReader.SoftStatus != SoftReaderStatusT.ReadError)
                                    )
                                {
                                    //CardDispensed = false;
                                    TerminalStatusChanged = true;
                                    if (terminalStatus != TerminalStatusT.Empty)
                                    {
                                        terminalStatus = TerminalStatusT.Empty;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardDispenserWork6: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                    }
                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    terminalStatusChangeInternal = false;
                                    ToLog(1, ResourceManager.GetString("ToLogCardEjected", DefaultCulture));
                                }
                            }
                            #endregion Карта извлечена
                        }

                    }
                    if ((CardDispenserStatusOld.CardAccepting != CardDispenserStatus.CardAccepting) ||
                        (CardDispenserStatusOld.CardDespensePos != CardDispenserStatus.CardDespensePos) ||
                        (CardDispenserStatusOld.CardDispensing != CardDispenserStatus.CardDispensing) ||
                        (CardDispenserStatusOld.CardEmpty != CardDispenserStatus.CardEmpty) ||
                        (CardDispenserStatusOld.CardError != CardDispenserStatus.CardError) ||
                        (CardDispenserStatusOld.CardPreSendPos != CardDispenserStatus.CardPreSendPos) ||
                        (CardDispenserStatusOld.CardShortage != CardDispenserStatus.CardShortage) ||
                        (CardDispenserStatusOld.CardTimeOut != CardDispenserStatus.CardTimeOut) ||
                        (CardDispenserStatusOld.CommunicationsError != CardDispenserStatus.CommunicationsError)
                        )
                    {
                        if (CardDispenserStatusOld.CardAccepting != CardDispenserStatus.CardAccepting)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardAccepting from" + CardDispenserStatusOld.CardAccepting.ToString() + " to " + CardDispenserStatus.CardAccepting.ToString());
                        if (CardDispenserStatusOld.CardDespensePos != CardDispenserStatus.CardDespensePos)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardDespensePos from" + CardDispenserStatusOld.CardDespensePos.ToString() + " to " + CardDispenserStatus.CardDespensePos.ToString());
                        if (CardDispenserStatusOld.CardDispensing != CardDispenserStatus.CardDispensing)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardDispensing from" + CardDispenserStatusOld.CardDispensing.ToString() + " to " + CardDispenserStatus.CardDispensing.ToString());
                        if (CardDispenserStatusOld.CardEmpty != CardDispenserStatus.CardEmpty)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardEmpty from" + CardDispenserStatusOld.CardEmpty.ToString() + " to " + CardDispenserStatus.CardEmpty.ToString());
                        if (CardDispenserStatusOld.CardError != CardDispenserStatus.CardError)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardError from" + CardDispenserStatusOld.CardError.ToString() + " to " + CardDispenserStatus.CardError.ToString());
                        if (CardDispenserStatusOld.CardShortage != CardDispenserStatus.CardShortage)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardShortage from" + CardDispenserStatusOld.CardShortage.ToString() + " to " + CardDispenserStatus.CardShortage.ToString());
                        if (CardDispenserStatusOld.CardTimeOut != CardDispenserStatus.CardTimeOut)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CardTimeOut from" + CardDispenserStatusOld.CardTimeOut.ToString() + " to " + CardDispenserStatus.CardTimeOut.ToString());
                        if (CardDispenserStatusOld.CommunicationsError != CardDispenserStatus.CommunicationsError)
                            ToLog(2, "Cashier: HardCardDispenserWork: Status changed CommunicationsError from" + CardDispenserStatusOld.CommunicationsError.ToString() + " to " + CardDispenserStatus.CommunicationsError.ToString());


                        TerminalStatusChanged = true;
                        CardDispenserStatusOld = CardDispenserStatus;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        private void CardDispenserGetStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                CardDispenserStatus = CardDispenser.CardDispenserStatus;
                if (CardDispenser.CardDispenserStatus.CardError || CardDispenser.CardDispenserStatus.CommunicationsError)
                {
                    string text;
                    if (CardDispenserStatus.CardError)
                    {
                        if (!CardDispenserStatusOld.Equals(CardDispenserStatus))
                            ToLog(2, "Cashier: CardDispenserGetStatus: CardDispenser.CardDispenserStatus.CardError");
                        text = ResourceManager.GetString("CardJam", DefaultCulture);
                    }
                    else
                    {
                        if (!CardDispenserStatusOld.Equals(CardDispenserStatus))
                            ToLog(2, "Cashier: CardDispenserGetStatus: CardDispenser.CardDispenserStatus.CommunicationError");
                        text = ResourceManager.GetString("CardDispenserCommunicationError", DefaultCulture) + CardDispenser.CardDispenserStatus.CommunicationsErrorStatus;
                    }
                    ErrorsText += text + Environment.NewLine;

                    if (CardDispenserLevel != AlarmLevelT.Brown)
                    {
                        if (CardDispenserAlarmTime == DateTime.MinValue)
                        {
                            CardDispenserAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CardDispenserAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                AlarmT Alarm = new AlarmT();
                                CardDispenserLevel = AlarmLevelT.Brown;
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                if (CardDispenserStatus.CardError)
                                    Alarm.ErrorCode = AlarmCode.CardDispenserJam;
                                else
                                    Alarm.ErrorCode = AlarmCode.CardDispenserNotConnect;

                                SendAlarm(Alarm);
                                CardDispenserAlarmTime = DateTime.MinValue;
                            }
                        }
                    }
                    if (!CardDispenserStatusOld.Equals(CardDispenserStatus))
                    {
                        TerminalStatusChanged = true;
                        CardDispenserStatusOld = CardDispenserStatus;
                    }
                }
                else
                {
                    /* Пока не поправили (нет карт в тубусе, а не вообще в диспенсере) - не выдаем аларм
                    if (CardDispenser.CardDispenserStatus.CardEmpty)
                    {
                        if (CardDispenserLevel != AlarmLevelT.Brown)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                            CardDispenserLevel = AlarmLevelT.Brown;
                            Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                            Alarm.ErrorCode = 241;
                            Alarm.Status = ResourceManager.GetString("CardDispenserStatusCardEmpty", DefaultCulture);
                            SendAlarm(Alarm);
                        }
                    }
                    else */
                    if (CardDispenser.CardDispenserStatus.CardShortage)
                    {
                        if (CardDispenserCardShortageDT != DateTime.MinValue)
                        {
                            if (CardDispenserCardShortageDT.AddMinutes(3) > DateTime.Now)
                            {
                                ErrorsText += ResourceManager.GetString("CardDispenserStatusCardShortage", DefaultCulture) + Environment.NewLine;
                                if (CardDispenserLevel != AlarmLevelT.Yellow)
                                {
                                    if (CardDispenserStatusOld != CardDispenserStatus)
                                        ToLog(2, "Cashier: CardDispenserGetStatus: " + ResourceManager.GetString("CardDispenserStatusCardShortage", DefaultCulture));
                                    AlarmT Alarm = new AlarmT();
                                    CardDispenserLevel = AlarmLevelT.Yellow;
                                    Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                    Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                                    Alarm.ErrorCode = AlarmCode.CardDispenserNearEmpty;
                                    SendAlarm(Alarm);
                                }
                            }
                        }
                        else
                        {
                            CardDispenserCardShortageDT = DateTime.Now;
                        }
                    }
                    else
                    {
                        if (CardDispenserAlarmTime != DateTime.MinValue)
                            CardDispenserAlarmTime = DateTime.MinValue;
                        if (CardDispenserCardShortageDT != DateTime.MinValue)
                            CardDispenserCardShortageDT = DateTime.MinValue;
                        if (CardDispenserLevel != AlarmLevelT.Green)
                        {
                            if (CardDispenserStatusOld != CardDispenserStatus)
                                ToLog(2, "Cashier: CardDispenserGetStatus: " + sCardDispenserWork);
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.CardDispenser;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = sCardDispenserWork;
                            SendAlarm(Alarm);
                            CardDispenserLevel = AlarmLevelT.Green;
                        }
                    }
                    if (CardDispenserStatusOld != CardDispenserStatus)
                    {
                        TerminalStatusChanged = true;
                        CardDispenserStatusOld = CardDispenserStatus;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

    }
}