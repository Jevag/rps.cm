﻿namespace Cashier
{
    partial class CEmulatorForm
    {
        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            /*if (disposing && (components != null))
            {
                components.Dispose();
            }*/
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CEmulatorForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ConsoleLog_groupbox = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ConsoleLog = new System.Windows.Forms.TextBox();
            this.Emulator = new Emulator.CEmulator();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ConsoleLog_groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            resources.ApplyResources(this.splitContainer1.Panel1, "splitContainer1.Panel1");
            this.splitContainer1.Panel1.Controls.Add(this.Emulator);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ConsoleLog_groupbox);
            // 
            // ConsoleLog_groupbox
            // 
            this.ConsoleLog_groupbox.Controls.Add(this.button1);
            this.ConsoleLog_groupbox.Controls.Add(this.ConsoleLog);
            resources.ApplyResources(this.ConsoleLog_groupbox, "ConsoleLog_groupbox");
            this.ConsoleLog_groupbox.Name = "ConsoleLog_groupbox";
            this.ConsoleLog_groupbox.TabStop = false;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ConsoleLog
            // 
            resources.ApplyResources(this.ConsoleLog, "ConsoleLog");
            this.ConsoleLog.Name = "ConsoleLog";
            this.ConsoleLog.ReadOnly = true;
            this.ConsoleLog.TextChanged += new System.EventHandler(this.ConsoleLog_TextChanged);
            // 
            // Emulator
            // 
            resources.ApplyResources(this.Emulator, "Emulator");
            this.Emulator.Name = "Emulator";
            // 
            // CEmulatorForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "CEmulatorForm";
            this.Activated += new System.EventHandler(this.CEmulatorForm_Enter);
            this.Deactivate += new System.EventHandler(this.CEmulatorForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CEmulatorForm_FormClosing);
            this.Shown += new System.EventHandler(this.CEmulatorForm_Shown);
            this.Enter += new System.EventHandler(this.CEmulatorForm_Enter);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ConsoleLog_groupbox.ResumeLayout(false);
            this.ConsoleLog_groupbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

 //       private System.ComponentModel.IContainer components;
        public System.Windows.Forms.SplitContainer splitContainer1;
        public System.Windows.Forms.GroupBox ConsoleLog_groupbox;
        public System.Windows.Forms.TextBox ConsoleLog;
        public Emulator.CEmulator Emulator;
        public System.Windows.Forms.Button button1;
    }
}