﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using CalculatorTS;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool CardReaderIsOpen = false;
        DateTime CardReaderAlarmTime;
        void HardCardReader()
        {
            try
            {
                if ((CardReader != null) && !CardReader.IsOpen)
                {
                    if ((CardReaderStatusCheckDT == null) || (CardReaderStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                    {
                        if ((CardReaderIsOpen != CardReader.IsOpen) && !CardReader.IsOpen)
                            ToLog(2, "Cashier: HardCardReader: CardReader.Closed");
                        CardReader.KeyA = keyA;
                        CardReader.SectorNumber = SectorNumber;
                        if (CardReaderModel != "HID OmniKey")
                        {
                            if (CardReader.Open(CardReaderPort)) TerminalStatusChanged = true;
                        }
                        else
                        {
                            if (CardReader.Open()) TerminalStatusChanged = true;
                        }
                        if (CardReader.IsOpen)
                            CardReader.InitPool();
                        CardReaderStatusCheckDT = DateTime.Now;
                    }
                }
                if (!CardReader.IsOpen)
                {
                    if (terminalStatus != TerminalStatusT.PaymentError)
                        TerminalWork = TerminalWorkT.TerminalDontWork;
                    if (CardReaderWork)
                    {
                        CardReaderWork = false;
                    }
                    if (TechForm.CardReaderPortLabel.Foreground != Brushes.Red)
                        TechForm.CardReaderPortLabel.Foreground = Brushes.Red;
                    if (CardReaderLevel != AlarmLevelT.Brown)
                    {
                        if (CardReaderAlarmTime == DateTime.MinValue)
                        {
                            CardReaderAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CardReaderAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                CardReaderLevel = AlarmLevelT.Brown;
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.CardReader;
                                Alarm.ErrorCode = AlarmCode.CardReaderNotConnect;
                                SendAlarm(Alarm);
                                CardReaderAlarmTime = DateTime.MinValue;
                            }
                        }
                    }
                }
                else
                {
                    if (CardReaderSoftStatus != CardReader.SoftStatus && CardReader.SoftStatus != SoftReaderStatusT.Reading)
                    {
                        ToLog(2, "Cashier: HardCardReader: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                        CardReaderSoftStatus = CardReader.SoftStatus;
                    }
                    if ((CardReader.SoftStatus == SoftReaderStatusT.TimeOut) ||
                    //(CardReader.SoftStatus == SoftReaderStatusT.ReadError) ||
                    //(CardReader.SoftStatus == SoftReaderStatusT.WriteError) ||
                    (CardReader.SoftStatus == SoftReaderStatusT.Exception)
                    )
                    {
                        CardReaderStatusChanged = true;
                        if (terminalStatus != TerminalStatusT.Empty)
                            terminalStatus = TerminalStatusT.Empty;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: SoftCardReader1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (CardReaderCommError >= 0)
                        {
                            if (CardReaderWork)
                            {
                                CardReaderWork = false;
                            }
                            CardReader.Close();
                            if (TechForm.CardReaderPortLabel.Foreground != Brushes.Red)
                                TechForm.CardReaderPortLabel.Foreground = Brushes.Red;
                            if (CardReaderLevel != AlarmLevelT.Brown)
                            {
                                if (CardReaderAlarmTime == DateTime.MinValue)
                                {
                                    CardReaderAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CardReaderAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        CardReaderLevel = AlarmLevelT.Brown;
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CardReader;
                                        Alarm.ErrorCode = AlarmCode.CardReaderNotConnect;
                                        SendAlarm(Alarm);
                                        CardReaderAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (CardReaderCommError < 3)
                                CardReaderCommError++;
                        }
                    }
                    else if (CardReader.SoftStatus != SoftReaderStatusT.Unknown)
                    {
                        if (CardReader.CardExists && (terminalStatus == TerminalStatusT.Empty))
                        {
                            terminalStatus = TerminalStatusT.CardInserted;
                        }
                        if (terminalStatusOld != terminalStatus)
                        {
                            ToLog(2, "Cashier: SoftCardReader2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            CardReaderStatusChanged = true;
                            terminalStatusOld = terminalStatus;
                        }

                        CardReaderWork = true;
                        CardReaderCommError = 0;

                        if (TechForm.CardReaderPortLabel.Foreground != Brushes.Green)
                            TechForm.CardReaderPortLabel.Foreground = Brushes.Green;
                        if (CardReaderAlarmTime != DateTime.MinValue)
                            CardReaderAlarmTime = DateTime.MinValue;
                        if (CardReaderLevel != AlarmLevelT.Green)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.CardReader;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = ResourceManager.GetString("CardReaderWork", DefaultCulture);
                            SendAlarm(Alarm);
                            CardReaderLevel = AlarmLevelT.Green;
                        }
                        if ((CardReader.SoftStatus == SoftReaderStatusT.CardReaded) &&
                            (terminalStatus == TerminalStatusT.Empty))
                            terminalStatus = TerminalStatusT.CardInserted;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: HardCardReader1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void GetCardReaderStatus()
        {
            try
            {
                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                {
                    if (CardReader == null)
                    {
                        if (CardReaderModel != "HID OmniKey")
                        {
                            CardReader = new CardReaderMT625(log);
                        }
                        else
                        {
                            CardReader = new CardReaderOmniKey5x2x(log);
                            try
                            {
                                CardReader.ReaderName = currRegistryKey.GetValue("ReaderName").ToString();
                            }
                            catch
                            {
                                CardReader.ReaderName = "OMNIKEY CardMan 5x21-CL 0";
                                currRegistryKey.SetValue("ReaderName", "OMNIKEY CardMan 5x21-CL 0");
                            }
                        }
                    }
                    if (EmulatorEnabled)
                        #region OtherDevicesPanel.CardReaderExists.Checked
                        if (EmulatorEnabled)
                        {
                            if (!EmulatorForm.Emulator.CardReaderGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderGroupBox.Show();
                            if (EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked)
                            {
                                CardReaderWork = EmulatorForm.Emulator.CardReaderWork.Checked;

                                if (EmulatorForm.Emulator.CardReaderWork.Checked)
                                {
                                    if ((CardReader != null) && CardReader.IsOpen)
                                    {
                                        TerminalStatusChanged = true;
                                        CardReader.Close();
                                    }

                                    if (!EmulatorForm.Emulator.CardReaderWork.Checked)
                                    {
                                        if (terminalStatus != TerminalStatusT.PaymentError)
                                            TerminalWork = TerminalWorkT.TerminalDontWork;
                                        if (CardReaderLevel != AlarmLevelT.Brown)
                                        {
                                            if (CardReaderAlarmTime == DateTime.MinValue)
                                            {
                                                CardReaderAlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CardReaderAlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    CardReaderLevel = AlarmLevelT.Brown;
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                    Alarm.AlarmSource = AlarmSourceT.CardReader;
                                                    Alarm.ErrorCode = AlarmCode.CardReaderNotConnect;
                                                    SendAlarm(Alarm);
                                                    CardReaderAlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CardReaderAlarmTime != DateTime.MinValue)
                                            CardReaderAlarmTime = DateTime.MinValue;
                                        if (CardReaderLevel != AlarmLevelT.Green)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Green;
                                            Alarm.AlarmSource = AlarmSourceT.CardReader;
                                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                                            Alarm.Status = ResourceManager.GetString("CardReaderWork", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CardReaderLevel = AlarmLevelT.Green;
                                        }
                                    }
                                }
                                else
                                {
                                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                                    if (CardReaderWork)
                                    {
                                        TerminalStatusChanged = true;
                                        CardReaderWork = false;
                                    }
                                }

                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                                HardCardReader();
                            }
                        }
                        else
                        {
                            HardCardReader();
                        }
                    if (!CardReaderWork)
                        ErrorsText += ResourceManager.GetString("TechCardReaderCheckReq", DefaultCulture) + Environment.NewLine;

                }
                #endregion OtherDevicesPanel.CardReaderExists.Checked
                else
                {
                    #region OtherDevicesPanel.CardReaderExists.NotChecked
                    if (CardReaderWork)
                    {
                        CardReaderWork = false;
                        if ((CardReader != null) && CardReader.IsOpen)
                            CardReader.Close();
                    }
                    CardReaderWork = false;
                    if (EmulatorEnabled)
                    {
                        if (EmulatorForm.Emulator.CardReaderGroupBox.Visible)
                            EmulatorForm.Emulator.CardReaderGroupBox.Hide();
                        if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                            EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                    }
                    #endregion OtherDevicesPanel.CardReaderExists.NotChecked
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void CardStatusCheck()
        {
            try
            {
                if (EmulatorEnabled)
                {
                    if (CardDispenserWork)
                    {
                        if (EmulatorForm.Emulator.CardDispenserVirt.Checked)
                        {
                            if (EmulatorForm.Emulator.CardDispenserWork.Checked)
                            {
                                SoftCardDispenserWork();
                            }
                        }
                        else
                        {
                            HardCardDispenserWork();
                        }
                    }
                    if (TerminalWork == TerminalWorkT.TerminalWork)
                    {
                        if (EmulatorForm.Emulator.CardReaderVirt.Checked)
                        {
                            if (CardReaderWork)
                            {
                                SoftCardReaderWork();
                            }
                        }
                        else
                        {
                            HardCardReaderWork();
                        }
                    }
                    else
                    {
                        if (!EmulatorForm.Emulator.CardDispenserVirt.Checked)
                        {
                            if (CardDispenserStatus.CardPreSendPos) // Под ридером
                            {
                                ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));
                                if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                                {
                                    if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                                    {
                                        ToLog(2, "Cashier: HardCardDispenserWork: TerminalDontWork CardDispenser.SendCardToBezelAndHold");
                                        CardDispenser.SendCardToBezelAndHold();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                        if (CardTimeOutToBasket > 0)
                                            CardEjectTime = DateTime.Now;
                                    }
                                }
                                return;
                            }
                        }
                    }
                }
                else
                {
                    if (CardDispenserWork)
                    {
                        HardCardDispenserWork();
                    }
                    if (TerminalWork == TerminalWorkT.TerminalWork)
                    {
                        if (CardReaderWork)
                        {
                            HardCardReaderWork();
                        }
                    }
                    else
                    {
                        ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));
                        if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                        {
                            if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                            {
                                ToLog(2, "Cashier: HardCardDispenserWork: TerminalDontWork CardDispenser.SendCardToBezelAndHold");
                                CardDispenser.SendCardToBezelAndHold();
                                while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                    Thread.Sleep(100);
                                if (CardTimeOutToBasket > 0)
                                    CardEjectTime = DateTime.Now;
                            }
                        }
                        return;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: CardStatusCheck: exception " + exc.ToString());
            }
        }
        private void WriteCard(ToTariffPlan.DateCard? vCardInfo = null)
        {
            ToTariffPlan.DateCard lCardInfo;
            if (vCardInfo != null)
            {
                lCardInfo = (ToTariffPlan.DateCard)vCardInfo;
            }
            else
            {
                lCardInfo = CardInfo;
            }
            ToLog(2, "Cashier: WriteCard");
            try
            {
                CardReader.CardInfo = lCardInfo;

                ToLog(2, "Cashier: WriteCard: CardReader.WriteData");
                lock (CardReader)
                    CardReader.WriteData();
                while (CardReader.CommandStatus != CommandStatusT.Completed)
                {
                    Thread.Sleep(100);
                }
                if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                {
                    lock (CardReader)
                        CardReader.WriteData();
                    while (CardReader.CommandStatus != CommandStatusT.Completed)
                    {
                        Thread.Sleep(100);
                    }
                }
                if (CardReader.SoftStatus == SoftReaderStatusT.WriteError)
                {
                    terminalStatus = TerminalStatusT.CardWriteError;
                    ToLog(0, "Cashier: WriteCard: CardReader.WriteData: WriteError");
                }
                else
                {
                    ToLog(2, "Cashier: WriteCard: CardReader.ReadData");
                    lock (CardReader)
                        CardReader.ReadData();
                    while (CardReader.CommandStatus != CommandStatusT.Completed)
                    {
                        Thread.Sleep(100);
                    }
                    if ((CardReader.SoftStatus != SoftReaderStatusT.CardReaded) ||
                        (lCardInfo.ClientGroupidFC != CardReader.CardInfo.ClientGroupidFC) ||
                        (lCardInfo.ClientTypidFC != CardReader.CardInfo.ClientTypidFC) ||
                        (lCardInfo.DateSaveCard != CardReader.CardInfo.DateSaveCard) ||
                        (lCardInfo.LastPaymentTime != CardReader.CardInfo.LastPaymentTime) ||
                        (lCardInfo.LastRecountTime != CardReader.CardInfo.LastRecountTime) ||
                        (lCardInfo.Nulltime1 != CardReader.CardInfo.Nulltime1) ||
                        (lCardInfo.Nulltime2 != CardReader.CardInfo.Nulltime2) ||
                        (lCardInfo.Nulltime3 != CardReader.CardInfo.Nulltime3) ||
                        (lCardInfo.ParkingEnterTime != CardReader.CardInfo.ParkingEnterTime) ||
                        (lCardInfo.SumOnCard != CardReader.CardInfo.SumOnCard) ||
                        (lCardInfo.TKVP != CardReader.CardInfo.TKVP) ||
                        (lCardInfo.TPidFC != CardReader.CardInfo.TPidFC) ||
                        (lCardInfo.TSidFC != CardReader.CardInfo.TSidFC) ||
                        (lCardInfo.TVP != CardReader.CardInfo.TVP) ||
                        (lCardInfo.ZoneidFC != CardReader.CardInfo.ZoneidFC)
                        )
                    {
                        //terminalStatus = TerminalStatusT.CardWriteError;
                        CardReader.SoftStatus = SoftReaderStatusT.WriteError;
                        ToLog(0, "Cashier: WriteCard: CardReader.WriteData: IncorrectData");
                    }
                    else
                    {
                        ToLog(2, "Cashier: WriteCard: CardReader.WriteData: Success");
                        CardInfo = lCardInfo;
                        SendCardTransaction();
                    }
                    if (terminalStatusOld != terminalStatus)
                        ToLog(2, "Cashier: WriteCard1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: WriteCard: exception " + exc.ToString());
            }
        }
    }
}