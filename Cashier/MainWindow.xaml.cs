﻿using System.Reflection;
using SqlBaseImport.Model;
using CashierWpfControlLibrary;
using CommonClassLibrary;
using Communications;
using EventWeb;
using Rps.ShareBase;
using RPS_Shared;
using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using CalculatorTS;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;
using Infralution.Localization.Wpf;
using System.Data;
using RPS.ServiceLicencee.Compute;
using System.Windows.Forms;
using System.Drawing;
using System.Threading.Tasks;
using UpdateProg;
using Rps.Crypto;
using System.Security.Principal;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        void ConfToReestr()
        {
            #region // проверка, есть ли данные в конфиге/реестре
            //Берем строки соединения для синхронизатора
            string section = "connectionStrings";
            Configuration config =
            ConfigurationManager.OpenExeConfiguration(
            ConfigurationUserLevel.None);

            // Get the section to unprotect.
            ConfigurationSection Section =
                config.GetSection(section);

            string conlocalReg;
            string conlocalConf = null;
            try
            {
                // строка conlocal
                conlocalConf = ConfigurationManager.ConnectionStrings["DeviceConnect"].ConnectionString;
                if (conlocalConf == conlocal0 | conlocalConf == null)
                {
                    if (currRegistryKey.GetValue("conlocalReg") != null)
                    {
                        conlocalReg = currRegistryKey.GetValue("conlocalReg").ToString();
                        conlocal = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conlocalReg));
                        if (conlocal == null)
                        {
                            SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocalReg);
                            conlocal = conlocalReg;
                            string Password = cb.Password;
                            string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                            if (Password1 == null)
                            {
                                Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                                cb.Password = Password;
                                currRegistryKey.SetValue("conlocalReg", cb.ToString());
                            }
                            else
                            {
                                cb.Password = Password1;
                                conlocal = cb.ToString();
                            }
                        }
                        else
                        {
                            SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocal);
                            string Password = cb.Password;
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                            cb.Password = Password;
                            currRegistryKey.SetValue("conlocalReg", cb.ToString());
                        }
                    }
                }
                else
                {
                    SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conlocalConf);
                    conlocal = conlocalConf;
                    string Password = cb.Password;
                    Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                    cb.Password = Password;
                    currRegistryKey.SetValue("conlocalReg", cb.ToString());
                }
            }
            catch { ToLog(0, "DeviceConnect not found in Configuration"); }

            string conserverReg;
            string conserverConf = null;
            // строка conserver
            try
            {
                conserverConf = ConfigurationManager.ConnectionStrings["ServerConnect"].ConnectionString;
                if (conserverConf == conserver0 | conserverConf == null)
                {
                    if (currRegistryKey.GetValue("conserverReg") != null)
                    {
                        conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                        conserver = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conserverReg));
                        if (conserver == null)
                        {
                            SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserverReg);
                            conserver = conserverReg;
                            string Password = cb.Password;
                            string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                            if (Password1 == null)
                            {
                                Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                                cb.Password = Password;
                                currRegistryKey.SetValue("conserverReg", cb.ToString());
                            }
                            else
                            {
                                cb.Password = Password1;
                                conserver = cb.ToString();
                            }
                        }
                        else
                        {
                            SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserver);
                            string Password = cb.Password;
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                            cb.Password = Password;
                            currRegistryKey.SetValue("conserverReg", cb.ToString());
                        }
                    }
                }
                else
                {
                    SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserverConf);
                    conserver = conserverConf;
                    string Password = cb.Password;
                    Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                    cb.Password = Password;
                    currRegistryKey.SetValue("conserverReg", cb.ToString());
                }
            }
            catch
            {
                if (currRegistryKey.GetValue("conserverReg") != null)
                {
                    conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                    conserver = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(conserverReg));
                    if (conserver == null)
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserverReg);
                        conserver = conserverReg;
                        string Password = cb.Password;
                        string Password1 = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                        if (Password1 == null)
                        {
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                            cb.Password = Password;
                            currRegistryKey.SetValue("conserverReg", cb.ToString());
                        }
                        else
                        {
                            cb.Password = Password1;
                            conserver = cb.ToString();
                        }
                    }
                    else
                    {
                        SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserver);
                        string Password = cb.Password;
                        Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));
                        cb.Password = Password;
                        currRegistryKey.SetValue("conserverReg", cb.ToString());
                    }
                }
            }

            // строка Entities
            try
            {
                EntitiesCS = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
                if (EntitiesCS != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("Entities");
                }
            }
            catch { }

            //EntitiesCS = "metadata=res://*/RPS_Shared.csdl|res://*/RPS_Shared.ssdl|res://*/RPS_Shared.msl;provider=System.Data.SqlClient;provider connection string=\"" + conlocal + ";MultipleActiveResultSets=True;App=EntityFramework\" providerName = \"System.Data.EntityClient\"";
            EntitiesCS = "metadata=res://*/RPS_Shared.csdl|res://*/RPS_Shared.ssdl|res://*/RPS_Shared.msl; provider=System.Data.SqlClient; provider connection string=\"" + conlocal + "\"";
            Entities.EntitiesCS = EntitiesCS;
            // сохраняем новый пустой конфиг
            if (conlocalConf != null && conlocalConf != conlocal0)
            {
                var conlocalA = config.ConnectionStrings.ConnectionStrings["DeviceConnect"];
                conlocalA.ConnectionString = conlocal0;
            }
            if (conserverConf != null && conserverConf != conserver0)
            {
                var conserverA = config.ConnectionStrings.ConnectionStrings["ServerConnect"];
                conserverA.ConnectionString = conserver0;
            }

            ConfigurationManager.RefreshSection("connectionStrings");
            Section.SectionInformation.ForceSave = false;
            config.Save(ConfigurationSaveMode.Modified);

            #endregion
        }

        private void CashierIcon_TechFormSwitch(object Sender, EventArgs e)
        {
            lock (ShiftLock)
            {
                ToLog(2, "Cashier: notifyIcon_DoubleClick: ToggleTechForm");
                ToggleTechForm = !ToggleTechForm;
                TerminalStatusChanged = true;
            }
        }
        private void CashierIcon_Exit(object Sender, EventArgs e)
        {
            CashierIcon.Visible = false;

            // Close the form, which closes the application.
            this.Close();
        }

        internal bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
        public MainWindow()
        {
            try
            {
                if (IsRunAsAdmin())
                {
                    string dn = Environment.UserDomainName;
                    string un = Environment.UserName;

                    string cmd = "netsh http add urlacl url=http://+:3333/ user=" + dn + "\\" + un;
                    ProcessStartInfo psiOpt = new ProcessStartInfo(@"cmd.exe", @"/C " + cmd);
                    psiOpt.WindowStyle = ProcessWindowStyle.Hidden;
                    psiOpt.RedirectStandardOutput = true;
                    psiOpt.UseShellExecute = false;
                    psiOpt.CreateNoWindow = true;
                    Process procCommand = Process.Start(psiOpt);
                    StreamReader srIncoming = procCommand.StandardOutput;
                    procCommand.WaitForExit();
                    string result = srIncoming.ReadToEnd();
                }
                Country = Properties.Settings.Default.Country;
                CultureInfo DefaultCulture = Properties.Settings.Default.DefaultLanguage;
                //if (DefaultLanguage != "CN")
                {
                    ///CultureManager.UICulture = new CultureInfo(DefaultLanguage);
                    CultureManager.UICulture = DefaultCulture;
                }
                /*else
                {
                    CultureManager.UICulture = new CultureInfo("zh-Hans");
                }*/

                log = new CommonClassLibrary.Log(@"LogCashier.txt", null);
                ToLog(0, "Cashier: MainWindow Start");
                InitializeComponent();
                smallTableWorkPanel1.Visibility = Visibility.Hidden;
                bigTableWorkPanel1.Visibility = Visibility.Hidden;
                ButtonsBar.Visibility = Visibility.Hidden;
                LanguagePanel.Visibility = Visibility.Hidden;
                FormCardReaderStatusPanel.Visibility = Visibility.Hidden;
                Left = 0;
                Top = 0;
                cryp = new CryptoDecrypto();

                Process ps = Process.GetCurrentProcess();
                ResourceManager = Properties.Resources.ResourceManager;
                GetLanguages();
                CultureInfo currLang = App.Language;
                nfi = new CultureInfo(currLang.Name, false).NumberFormat;

                int[] mySizes1 = { 3 };
                nfi.NumberGroupSizes = mySizes1;
                nfi.NumberGroupSeparator = " ";
                nfi.NumberDecimalDigits = 0;

                BankCardErrorDateTime = datetime0;
                WorkKeyCheckTime = datetime0;
                ConfToReestr();
                if (conlocal != null)
                    log.ConnectionString = conlocal;

                string strHostName = Dns.GetHostName();
                IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);
                string IP = "";
                foreach (IPAddress ipaddress in iphostentry.AddressList)
                {
                    if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        IP = ipaddress.ToString();
                        break;
                    }
                }


                CashierTypeS s = new CashierTypeS();
                s.Code = 0;
                s.Name = ResourceManager.GetString("CashierStandard", DefaultCulture);// "Обычная касса";
                CashierTypes.Add(s);
                s = new CashierTypeS();
                s.Code = 1;
                s.Name = ResourceManager.GetString("CashierLight", DefaultCulture);// "Касса Лайт";
                CashierTypes.Add(s);
                s = new CashierTypeS();
                s.Code = 2;
                s.Name = ResourceManager.GetString("CashierManual", DefaultCulture);// "Ручная касса";
                CashierTypes.Add(s);

                ChequeTypes.Add(ResourceManager.GetString("NothingCheque", DefaultCulture)); // "Ничего не делать"
                ChequeTypes.Add(ResourceManager.GetString("GetChequeToBox")); // "Забирать чек"
                ChequeTypes.Add(ResourceManager.GetString("DispenceCheque", DefaultCulture)); // "Выбрасывать не улицу"

                InternalLevel = AlarmLevelT.Green;
                CardDispenserLevel = AlarmLevelT.Green;
                CardReaderLevel = AlarmLevelT.Green;
                CashAcceptorLevel = AlarmLevelT.Green;
                CoinAcceptorLevel = AlarmLevelT.Green;
                CashDispenserLevel = AlarmLevelT.Green;
                CoinHopper1Level = AlarmLevelT.Green;
                CoinHopper2Level = AlarmLevelT.Green;
                ExternalDoorLevel = AlarmLevelT.Green;
                InternalDoorLevel = AlarmLevelT.Green;
                TerminalLevel = AlarmLevelT.Green;
                KKMLevel = AlarmLevelT.Green;
                BankModuleLevel = AlarmLevelT.Green;
                LicenseLevel = AlarmLevelT.Green;

                d_format = Properties.Resources.DateFormat;
                dt_format = Properties.Resources.CardDateTimeFormat;
                ldt_format = Properties.Resources.ToLogDateTimeFormat;

                if (conlocal != null && EntitiesCS != null)
                {
                    ToLog(2, "Cashier: MainWindow: Connect to DB");
                    db = new SqlConnection(conlocal);
                    try
                    {
                        db.Open();
                    }
                    catch (Exception exc)
                    {
                        ToLog(2, Properties.Resources.ToLogCreateConnectionException + " " + exc.ToString());
                    }
                    ToLog(2, "Cashier: MainWindow: Init");


                    string Name = "";
                    try
                    {
                        string qry = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DeviceCMModel' AND Column_Name = 'ChequeWork'";
                        lock (db_lock)
                        {
                            try
                            {
                                var res = UtilsBase.ExecuteScalar(qry, db, null, null);
                                if (res == null || res == System.DBNull.Value)
                                {
                                    qry = "alter table DeviceCMModel add ChequeWork tinyint";
                                    res = UtilsBase.ExecuteScalar(qry, db, null, null);
                                }
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    var res = UtilsBase.ExecuteScalar(qry, db, null, null);
                                    if (res == null || res == System.DBNull.Value)
                                    {
                                        qry = "alter table DeviceCMModel add ChequeWork tinyint";
                                        res = UtilsBase.ExecuteScalar(qry, db, null, null);
                                    }
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }

                        string req = "select top 1 * from DeviceTypeModel where Id=" + DeviceType.ToString();
                        DataRow deviceTypeModel = null;
                        lock (db_lock)
                        {
                            try
                            {
                                deviceTypeModel = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    deviceTypeModel = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (deviceTypeModel == null)
                        {
                            System.Windows.MessageBox.Show(Properties.Resources.ToLogReadDeviceTypeFromDBError + Environment.NewLine, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            //Close();
                            return;
                        }
                        else
                        {
                            Name = deviceTypeModel.GetString("Name");
                        }
                        req = "select top 1 * from DeviceModel where (_IsDeleted is null or _IsDeleted = 0) ";
                        DataRow deviceModel = null;
                        lock (db_lock)
                        {
                            try
                            {
                                deviceModel = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    deviceModel = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }

                        if (deviceModel == null)
                        {
                            SqlConnectionStringBuilder cb =
                        new SqlConnectionStringBuilder(conlocal);
                            string Password = cb.Password;
                            Password = cryp.ByteAToString(cryp.EncryptStringToBytes_Aes(Password));

                            lock (db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.InsertCommand("DeviceModel", db, null, new object[] { "Id", Guid.NewGuid(), "[Type]", DeviceType, "[Name]", Name, "[Host]", IP, "[DataBase]", cb.InitialCatalog, "[User]", cb.UserID, "Password", Password });
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.InsertCommand("DeviceModel", db, null, new object[] { "Id", Guid.NewGuid(), "[Type]", DeviceType, "[Name]", Name, "[Host]", IP, "[DataBase]", cb.InitialCatalog, "[User]", cb.UserID, "Password", Password });
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            req = "select top 1 * from DeviceModel where (_IsDeleted is null or _IsDeleted = 0) ";
                            lock (db_lock)
                            {
                                try
                                {
                                    deviceModel = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        deviceModel = UtilsBase.FillRow(req, db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            device = SetDevice(deviceModel);
                        }
                        else
                        {
                            ToLog(2, "Cashier: MainWindow: SetDevice");
                            device = SetDevice(deviceModel);
                            if (!device.Host.Equals(IP))
                            {
                                device.Host = IP;
                                lock (db_lock)
                                {
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceModel", "Id", new object[] { "Id", device.Id, "Host", IP }, "Id='" + device.Id.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.SetOne("DeviceModel", "Id", new object[] { "Id", device.Id, "Host", IP }, "Id='" + device.Id.ToString() + "'", null, db);
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                        }
                        DeviceId = device.Id;
                        log.DeviceId = DeviceId;
                        req = "select top 1 * from DeviceCMModel where DeviceId='" + DeviceId.ToString() + "' and (_IsDeleted is null or _IsDeleted = 0) ";
                        lock (db_lock)
                        {
                            try
                            {
                                deviceCM = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    deviceCM = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (deviceCM == null)
                        {
                            lock (db_lock)
                            {
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                try
                                {
                                    UtilsBase.InsertCommand("DeviceCMModel", db, null, new object[] { "DeviceId", DeviceId, "SlaveExist", 1 });
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.InsertCommand("DeviceCMModel", db, null, new object[] { "DeviceId", DeviceId, "SlaveExist", 1 });
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            SettingsLastUpdate = DateTime.Now;
                            req = "select top 1 * from DeviceCMModel where DeviceId='" + DeviceId.ToString() + "' and (_IsDeleted is null or _IsDeleted = 0) ";
                            lock (db_lock)
                            {
                                try
                                {
                                    deviceCM = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        deviceCM = UtilsBase.FillRow(req, db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, Properties.Resources.ToLogReadSettingsFromDBException);
                        ToLog(0, exc.ToString());
                    }

                    ToLog(2, "Cashier: MainForm: SetDeviceCM");
                    deviceCMModel = SetDeviceCM(deviceCM);

                    cashierForm = this;
                    try
                    {
                        CashierType = (byte)deviceCMModel.CashierType;
                    }
                    catch { CashierType = 0; }
                    // Initialize contextMenu1

                    menuTechForm.Click += new EventHandler(CashierIcon_TechFormSwitch);
                    menuTechForm.CheckOnClick = true;
                    NotifyIconСontextMenu.Items.Add(menuTechForm);

                    menuItemExit.Click += new System.EventHandler(CashierIcon_Exit);
                    NotifyIconСontextMenu.Items.Add(menuItemExit);

                    CashierIcon = new NotifyIcon();
                    CashierIcon.ContextMenuStrip = NotifyIconСontextMenu;
                    //загружаем файл с иконкой 
                    CashierIcon.Icon = new Icon("Cashier.ico");

                    if (CashierType != 2)
                        this.Visibility = Visibility.Visible;
                    else
                    {
                        this.Visibility = Visibility.Hidden;
                    }

                    TechForm = new TechFormWPF(this);

                    // лейбл
                    Assembly thisAssem = typeof(MainWindow).Assembly;
                    AssemblyName thisAssemName = thisAssem.GetName();
                    Version ver = thisAssemName.Version;
                    CurrentVersion = ver.Major;
                    string Ver = string.Format(ResourceManager.GetString("VersionFormat", DefaultCulture), ver.Major.ToString(), ver.Minor.ToString(), ver.Build.ToString());
                    FileInfo f = new FileInfo(System.Windows.Forms.Application.ExecutablePath);
                    string Update = string.Format(ResourceManager.GetString("UpdateFormat", DefaultCulture), f.LastWriteTime.ToString("d"));

                    TechForm.Version.Content = Ver;
                    TechForm.Update.Content = Update;
                    ToLog(2, "Cashier: MainWindow: Call CleanTechPanels");
                    CleanTechPanels();
                    TechForm.Left = 0;
                    TechForm.Top = 0;

                    Initalized = false;

                    TerminalWork = TerminalWorkT.TerminalWork;

                    ExternalDeviceTimer.Interval = 100;
                    ExternalDeviceTimer.Tick += ExternalDeviceTimer_Tick;

                    ExternalDeviceTimer.Enabled = true;

                }
                else
                {
                    ToLog(0, "Cashier doesn't work because connection isn't defined.");
                }
                Activate();
            }
            catch (Exception exc)
            {
                using (StreamWriter file = new StreamWriter(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"LogCashier.txt"), true))
                {
                    file.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + ": " + exc.ToString());
                }
                //показываем иконку в панели
                if (CashierType == 2 && CashierIcon != null)
                    CashierIcon.Visible = true;
            }
        }

        void Initialize()
        {
            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Start");
            try
            {
                try
                {
                    TariffPlanTariffScheduleList = new List<TariffPlanTariffScheduleModelT>();
                    RegionInfo myRI1 = new RegionInfo(Country);

                    Valute = myRI1.ISOCurrencySymbol;


                    ToLog(2, "Cashier: Initialize: Get DeviceAction.WriteTime");
                    try
                    {
                        string strhas = "select max(DeviceAction.WriteTime) from DeviceAction";
                        using (SqlCommand comm = new SqlCommand(strhas, db))
                        {
                            object res = comm.ExecuteScalar();
                            if (res != null && res != System.DBNull.Value)
                            {
                                try
                                {
                                    log.MaxWriteTime = Convert.ToInt16(res);
                                }
                                catch { }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                    }


                    #region ИНИЦИАЛИЗАЦИЯ КАССЫ

                    log.Level = deviceCMModel.LogLevel;
                    try
                    {
                        SettingsLastUpdate = (DateTime)deviceCMModel.Sync;
                    }
                    catch
                    {
                        SettingsLastUpdate = DateTime.Now;
                    }
                    #endregion ИНИЦИАЛИЗАЦИЯ КАССЫ

                    ClearAlarm();

                    CalculatorTS = new ToTariffPlan();



                    rnd = new Random();

                    // Список номиналов банкнот
                    ToLog(2, "Cashier: Initialize: Get Banknote Nominals");
                    string req = "select * from BanknoteModel where Code=@key order by Value, Id";
                    DataTable banknotesQuery = null;
                    lock (db_lock)
                    {
                        try
                        {
                            banknotesQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                banknotesQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }

                    if (banknotesQuery != null && banknotesQuery.Rows.Count > 0)
                    {
                        for (int i = 0; i < banknotesQuery.Rows.Count; i++)
                        {
                            BanknoteModelT banknote = new BanknoteModelT();
                            banknote.Id = (int)banknotesQuery.Rows[i].GetInt("Id");
                            banknote.Code = banknotesQuery.Rows[i].GetString("Code");
                            banknote.Name = banknotesQuery.Rows[i].GetString("Name");
                            banknote.Value = banknotesQuery.Rows[i].GetString("Value");
                            banknote.Length = banknotesQuery.Rows[i].GetString("Length");
                            banknote.Width = banknotesQuery.Rows[i].GetString("Width");
                            banknote.EscrowData = banknotesQuery.Rows[i].GetInt("EscrowData");
                            banknote.DisableData = banknotesQuery.Rows[i].GetInt("DisableData");
                            Banknotes.Add(banknote);
                        }
                    }

                    // Список номиналов монет
                    ToLog(2, "Cashier: Initialize: Get Coin Nominals");
                    req = "select * from CoinModel where Code=@key order by Value, Id";
                    DataTable coinsQuery = null;
                    lock (db_lock)
                    {
                        try
                        {
                            coinsQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                coinsQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }

                    if (coinsQuery != null || coinsQuery.Rows.Count > 0)
                    {
                        for (int i = 0; i < coinsQuery.Rows.Count; i++)
                        {
                            CoinModelT coin = new CoinModelT();
                            coin.Id = (int)coinsQuery.Rows[i].GetInt("Id");
                            coin.Code = coinsQuery.Rows[i].GetString("Code");
                            coin.Name = coinsQuery.Rows[i].GetString("Name");
                            coin.Value = coinsQuery.Rows[i].GetString("Value");
                            coin.CIndex = (int)(coinsQuery.Rows[i].GetInt("DisableData"));
                            Coins.Add(coin);
                        }
                    }
                    //ToLog(2, "Cashier: Initialize: Get Coin Nominals End");

                    // Список устройств
                    ToLog(2, "Cashier: Initialize: Get ExecutableDevices");
                    req = "select * from ExecutableDevice order by Id";
                    DataTable ExecutableDevicesQuery = null;
                    lock (db_lock)
                    {
                        try
                        {
                            ExecutableDevicesQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                ExecutableDevicesQuery = UtilsBase.FillTable(req, db, null, "@key", Valute);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }

                    if (ExecutableDevicesQuery != null || ExecutableDevicesQuery.Rows.Count > 0)
                    {
                        for (int i = 0; i < ExecutableDevicesQuery.Rows.Count; i++)
                        {
                            ExecutableDeviceT ed = new ExecutableDeviceT();
                            ed.Id = (int)ExecutableDevicesQuery.Rows[i].GetInt("Id");
                            ed.Name = ExecutableDevicesQuery.Rows[i].GetString("Name");
                            ed.ExecutableDeviceTypeId = ExecutableDevicesQuery.Rows[i].GetInt("ExecutableDeviceTypeId");
                            ExecutableDevices.Add(ed);
                        }
                    }

                    // Список моделей устройств
                    for (int i = 0; i < ExecutableDevices.Count(); i++)
                    {
                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enBankModule)
                        {
                            BankModuleExecutableDevices.Add(ExecutableDevices[i]);
                        }

                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enKKM)
                        {
                            KKMExecutableDevices.Add(ExecutableDevices[i]);
                        }

                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCardDispenser)
                        {
                            CardDispenserExecutableDevices.Add(ExecutableDevices[i]);
                        }

                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enCardReader)
                        {
                            CardReaderExecutableDevices.Add(ExecutableDevices[i]);
                        }

                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enPrinter)
                        {
                            PrinterExecutableDevices.Add(ExecutableDevices[i]);
                        }
                        if (ExecutableDevices[i].ExecutableDeviceTypeId == (int)ExecutableDeviceType.enSlave)
                        {
                            SlaveControllerExecutableDevices.Add(ExecutableDevices[i]);
                        }
                    }

                    EmulatorEnabled = Properties.Settings.Default.EmulatorEnabled;

                    if (EmulatorEnabled)
                    {
                        ToLog(2, "Cashier: Initialize: EmulatorForm Initialize");
                        EmulatorForm = new CEmulatorForm(this);
                        EmulatorForm.Icon = new Icon("Cashier.ico");
                        EmulatorForm.Emulator.Show();
                        EmulatorForm.Emulator.dt_format = dt_format;
                        EmulatorForm.Show();
                        this.Activate();
                    }
                    try
                    {
                        ShiftOpened = (bool)deviceCMModel.ShiftOpened;
                    }
                    catch { ShiftOpened = false; }

                    if (deviceCMModel.DefaultLanguage == null)
                    {
                        DefaultCulture = Properties.Settings.Default.DefaultLanguage;
                    }
                    else
                    if (deviceCMModel.DefaultLanguage != "CN")
                    {
                        DefaultCulture = new CultureInfo(deviceCMModel.DefaultLanguage);
                    }
                    else
                    {
                        DefaultCulture = new CultureInfo("zh-Hans");
                    }
                    CultureManager.UICulture = DefaultCulture;

                    ToLog(2, "Cashier: Initialize: GetSettings");
                    GetSettings(true);

                    ToLog(2, "Cashier: Initialize: FillBigCalculator");
                    FillBigCalculator();

                    ShiftWork = (deviceCMModel.PrinterExists == null) ? true : !(bool)deviceCMModel.PrinterExists;

                    // Создание объектов исполняющих устройств
                    if (CashierType != 2)
                    {
                        ToLog(2, "Cashier: Initialize: CardDispenser Initialize");
                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                            CardDispenser = new CardDispenserMT166(log);
                    }
                    if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                    {

                        ToLog(2, "Cashier: Initialize: CardReader Initialize");
                        if (CardReaderModel != "HID OmniKey")
                        {
                            CardReader = new CardReaderMT625(log);
                        }
                        else
                        {
                            CardReader = new CardReaderOmniKey5x2x(log);
                            try
                            {
                                CardReader.ReaderName = currRegistryKey.GetValue("ReaderName").ToString();
                            }
                            catch
                            {
                                CardReader.ReaderName = "OMNIKEY CardMan 5x21-CL 0";
                                currRegistryKey.SetValue("ReaderName", "OMNIKEY CardMan 5x21-CL 0");
                            }
                        }
                    }

                    if (CashierType != 2)
                    {
                        if ((deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked))
                        {
                            ToLog(2, "Cashier: Initialize: CashAcceptor Initialize");
                            if (CashAcceptorModel == "iVision")
                                CashAcceptor = new CashAcceptorJCM_ID_003(CashAcceptorLock, log, DefaultCulture);
                            else
                            {
                                CashDispenserLock = CashAcceptorLock;
                                if (CashRecycler == null)
                                    CashRecycler = new CashRecyclerJCM_RC_ID_003(CashAcceptorLock, log, DefaultCulture);
                                CashAcceptor = CashRecycler;
                            }

                            ToLog(2, "Cashier: Initialize: CashAcceptor.Open");
                            if (CashAcceptor.Open(CashAcceptorPort))
                            {
                                if (CashAcceptorModel == "iVision")
                                {
                                    ToLog(1, "CashAcceptor Version: " + CashAcceptor.Version);
                                    ToLog(1, "CashAcceptor BootVersion: " + CashAcceptor.BootVersion);
                                }
                                else
                                {
                                    ToLog(1, "CashRecycler Version: " + CashAcceptor.Version);
                                    ToLog(1, "CashRecycler BootVersion: " + CashAcceptor.BootVersion);
                                }
                                CashAcceptorStatusCheckDT = DateTime.Now;
                            }
                        }

                        if ((deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked))
                        {
                            if (CashDispenserModel == "Multimech")
                                CashDispenser = new CashDispenserMultiMech(CashDispenserLock, log);
                            else
                            {
                                if (CashRecycler != null)
                                {
                                    CashDispenser = CashRecycler;
                                }
                                else
                                {
                                    CashRecycler = new CashRecyclerJCM_RC_ID_003(CashDispenserLock, log, DefaultCulture);
                                    CashDispenser = CashRecycler;
                                }
                            }
                            bool res;
                            if (CashDispenserModel == "Multimech")
                                res = CashDispenser.Open(CashDispenserPort);
                            else
                            {
                                if (!CashDispenser.IsOpen)
                                    res = CashDispenser.Open(CashAcceptorPort);
                            }
                            //ToLog(2, "Cashier: Initialize: CashDispenser Initialize End");
                            CashDispenserStatusCheckDT = DateTime.Now;
                        }
                        if (CashDispenser != null && CashDispenser.IsOpen)
                        {
                            CashDispenser.SetCount(1, DeliveryUpCount);
                            CashDispenser.SetCount(2, DeliveryDownCount);
                        }

                        if (CashRecycler != null)
                        {
                            if (CashRecycler.IsOpen)
                            {
                                DeliveryUpCountChanged = false;
                                DeliveryDownCountChanged = false;
                                CashRecycler.Reset();
                                Thread.Sleep(500);
                                int cou = 0;
                                while (CashRecycler.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Initialize && cou < 120)
                                {
                                    cou++;
                                    Thread.Sleep(500);
                                }
                                ToLog(1, string.Format("Cashier: Initialize: CashRecycler.CashDispenserResult.Cassettes[1].HopperStatus={0}", CashRecycler.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                if (CashRecycler.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                {
                                    ToLog(1, "Cashier: Initialize: DeliveryUpCount Empty");
                                    DeliveryUpCount = 0;
                                    CashRecycler.SetCount(1, 0);
                                    DeliveryUpCountChanged = true;
                                }
                                ToLog(1, string.Format("Cashier: Initialize: CashRecycler.CashDispenserResult.Cassettes[2].HopperStatus={0}", CashRecycler.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                if (CashRecycler.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                {
                                    ToLog(1, "Cashier: Initialize: DeliveryDownCount Empty");
                                    DeliveryDownCount = 0;
                                    CashRecycler.SetCount(2, 0);
                                    DeliveryDownCountChanged = true;
                                }

                                try
                                {
                                    if (DeliveryUpCountChanged || DeliveryDownCountChanged)
                                    {
                                        lock (db_lock)
                                        {
                                            deviceCMModel.CountUp = DeliveryUpCount;
                                            deviceCMModel.CountDown = DeliveryDownCount;
                                            //while (Utils.Running)
                                            //    Thread.Sleep(50);
                                            try
                                            {
                                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }
                                        DeliveryUpCountChanged = false;
                                        DeliveryDownCountChanged = false;
                                    }
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                }
                            }
                        }
                        else
                        {
                            if (CashAcceptor != null && CashAcceptor.IsOpen)
                                CashAcceptor.Reset();
                            if (CashDispenser != null && CashDispenser.IsOpen)
                                CashDispenser.Reset();
                        }
                        if (!CashAcceptorInhibit)
                            CashAcceptorInhibit = true;
                    }

                    if ((deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked))
                    {
                        try
                        {
                            if (KKMModel == "IskraKKM")
                                KKM = new IskraKKM(log);
                            else if (KKMModel == "PrimFAKKM")
                                KKM = new PrimFAKKM(log/*, device.Name*/);
                            else if (KKMModel == "AtolKKM")
                                KKM = new AtolKKM(log);
                            else
                                ToLog(0, KKMModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture));

                        }
                        catch (Exception exc)
                        {
                            ToLog(0, ResourceManager.GetString("KKMDriverError", DefaultCulture) + " " + exc.ToString());
                        }
                    }
                    if ((deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked))
                    {
                        try
                        {
                            Printer = new TKiosk(log);
                            PrinterStatusCheckDT = DateTime.Now;
                        }
                        catch
                        {
                            ToLog(0, ResourceManager.GetString("TechPrinterCheckReq", DefaultCulture));
                        }
                    }

                    if (CashierType != 2)
                    {
                        if (deviceCMModel.SlaveExist != null && (bool)deviceCMModel.SlaveExist)
                        {
                            ToLog(2, "Cashier: Initialize: SlaveController Initialize");
                            SlaveController = new Slave(log);
                            SlaveController.SlavePort1 = "H";
                            SlaveController.SlavePort2 = " ";
                            SlaveController.SlavePort3 = " ";
                            SlaveController.SlavePort4 = " ";
                            if (EmulatorEnabled)
                            {
                                if ((EmulatorForm != null) && (EmulatorForm.Emulator.terminalStatus != terminalStatus))
                                {
                                    EmulatorForm.Emulator.ExternalDoorVirt.Enabled = true;
                                    EmulatorForm.Emulator.InternalDoorVirt.Enabled = true;
                                }
                            }
                        }
                        else
                        {
                            if (EmulatorEnabled)
                            {
                                if ((EmulatorForm != null) && (EmulatorForm.Emulator.terminalStatus != terminalStatus))
                                {
                                    EmulatorForm.Emulator.ExternalDoorVirt.Enabled = false;
                                    EmulatorForm.Emulator.InternalDoorVirt.Enabled = false;
                                }
                            }
                        }
                    }
                    if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist && (!EmulatorEnabled || EmulatorForm == null || !EmulatorForm.Emulator.BankModuleVirt.Checked))
                    {
                        if (BankModuleModel == "GPB")
                        {
                            try
                            {
                                BankModule = new GPB(log);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                            }
                        }
                        else
                        {
                            ToLog(0, BankModuleModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture));
                        }
                        if (BankModule != null)
                        {
                            BankModule.KKMNumber = (int)deviceCMModel.CashierNumber;//Установка номера кассы
                            BankModule.ChequeNumber = CurrentTransactionNumber;
                            BankModule.Open();
                            if (BankModule.DeviceEnabled)
                            {
                                BankModule.WorkKey();
                                if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                                    WorkKeyLoaded = true;
                                else
                                    WorkKeyCheckTime = DateTime.Now + TimeSpan.FromMinutes(5);
                            }
                        }
                    }
                    if (CashierType == 0)
                    {
                        if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
                        {
                            ToLog(2, "Cashier: Initialize: CoinAcceptor Initialize");
                            CoinAcceptor = new CoinAcceptor(log);
                            //ToLog(2, "Cashier: Initialize: CoinAcceptor Initialize End");
                        }
                    }
                    if (CashierType == 0)
                    {
                        if (deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist)
                        {
                            ToLog(2, "Cashier: Initialize: CoinHopper Initialize");
                            CoinHopper = new CoinHopper(log);
                        }
                    }

                    if (ShiftWork)
                    {
                        if (ShiftOpened)
                        {
                            try
                            {
                                CurrentTransactionNumber = (int)deviceCMModel.CurrentTransactionNumber;
                            }
                            catch
                            {
                                CurrentTransactionNumber = 1;
                                deviceCMModel.CurrentTransactionNumber = 1;
                            }
                            LastShiftOpenTime = deviceCMModel.LastShiftOpenTime;
                            if (LastShiftOpenTime == null)
                                LastShiftOpenTime = DateTime.Now;
                            if (TerminalLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.Terminal;
                                Alarm.ErrorCode = AlarmCode.ShiftOpened;
                                //Alarm.Status = ResourceManager.GetString("ShiftOpened", DefaultCulture);
                                SendAlarm(Alarm);
                                TerminalLevel = AlarmLevelT.Green;
                            }
                        }
                        else
                        {
                            CurrentTransactionNumber = 0;
                            LastShiftOpenTime = null;
                            if (TerminalLevel != AlarmLevelT.Red)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Red;
                                TerminalLevel = AlarmLevelT.Red;
                                Alarm.AlarmSource = AlarmSourceT.Terminal;
                                Alarm.ErrorCode = AlarmCode.ShiftClosed;
                                //Alarm.Status = ResourceManager.GetString("ShiftClosed;
                                SendAlarm(Alarm);
                            }
                        }
                        /*
                        if (ShiftOpened)
                        {
                            TechForm.ShiftStateGreen.Visibility = Visibility.Visible;
                            TechForm.ShiftStateRed.Visibility = Visibility.Hidden;
                            if (DefaultCulture != null)
                            {
                                TechForm.ShiftState.Text = ResourceManager.GetString("ShiftOpened", DefaultCulture);
                                TechForm.ShiftModeButton.Content = ResourceManager.GetString("CloseShift", DefaultCulture);
                            }
                            else
                            {
                                TechForm.ShiftState.Text = Properties.Resources.ShiftOpened;
                                TechForm.ShiftModeButton.Content = Properties.Resources.CloseShift;
                            }
                        }
                        else
                        {
                            TechForm.ShiftStateGreen.Visibility = Visibility.Hidden;
                            TechForm.ShiftStateRed.Visibility = Visibility.Visible;
                            if (DefaultCulture != null)
                            {
                                TechForm.ShiftState.Text = ResourceManager.GetString("ShiftClosed", DefaultCulture);
                                TechForm.ShiftModeButton.Content = ResourceManager.GetString("OpenShift", DefaultCulture);
                            }
                            else
                            {
                                TechForm.ShiftState.Text = Properties.Resources.ShiftClosed;
                                TechForm.ShiftModeButton.Content = Properties.Resources.OpenShift;
                            }
                        }
                        */
                    }
                    else
                        CurrentTransactionNumber = (int)deviceCMModel.CurrentTransactionNumber;

                    //            CasseStatusLabel = ResourceManager.GetString("CashierDontStarted", DefaultCulture);
                    if (EmulatorEnabled)
                    {
                        if (ShiftWork)
                        {
                            if (!EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                EmulatorForm.Emulator.ShiftStatusLabel1.Show();
                            EmulatorForm.Emulator.ShiftStatusLabel1.Text = (ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed);
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                EmulatorForm.Emulator.ShiftStatusLabel1.Hide();
                        }
                        EmulatorForm.Emulator.TerminalWork = TerminalWorkT.TerminalWork;
                        EmulatorForm.Emulator.CasseStatusLabel1.Text = Properties.Resources.CashierStarted;
                        EmulatorForm.Emulator.CashAcceptorMaxCount.Text = deviceCMModel.CashAcceptorLimit.ToString();
                        EmulatorForm.Emulator.CashAcceptorMax.Text = deviceCMModel.CashAcceptorAlarm.ToString();
                        EmulatorForm.Emulator.CoinAcceptorMaxCount.Text = deviceCMModel.CoinAcceptorLimit.ToString();
                        EmulatorForm.Emulator.CoinAcceptorMax.Text = deviceCMModel.CoinAcceptorAlarm.ToString();
                        EmulatorForm.Emulator.PaidBanknoteCurrent = CashAcceptorCurrent;
                        EmulatorForm.Emulator.CashAcceptorCurrent.Text = CashAcceptorCurrent.ToString();
                        EmulatorForm.Emulator.PaidCoinCurrent = CoinAcceptorCurrent;
                        EmulatorForm.Emulator.CoinAcceptorCurrent.Text = CoinAcceptorCurrent.ToString();
                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                            EmulatorForm.Emulator.CardDispenserGroupBox.Show();
                        else
                            EmulatorForm.Emulator.CardDispenserGroupBox.Hide();
                        if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                            EmulatorForm.Emulator.CardReaderGroupBox.Show();
                        else
                            EmulatorForm.Emulator.CardReaderGroupBox.Hide();
                        switch (TechForm.ServerExchangeProtocolSelectedIndex)
                        {
                            case 1:
                                EmulatorForm.Emulator.CarNumberPanel.Hide();
                                break;
                            case 2:
                                EmulatorForm.Emulator.CarNumberPanel.Show();
                                break;
                        }
                    }

                    if (CashierType != 2)
                    {
                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                        {
                            if (!EmulatorEnabled ||
                                 ((EmulatorForm != null)
                                   && (EmulatorForm.Emulator != null)
                                   && (EmulatorForm.Emulator.CardDispenserWork != null)
                                   && EmulatorForm.Emulator.CardDispenserWork.Checked
                                   && !EmulatorForm.Emulator.CardDispenserVirt.Checked))
                            {
                                if (CardDispenser != null)
                                {
                                    if (!CardDispenser.IsOpen)
                                    {
                                        ToLog(2, "Cashier: Initialize: CardDispenser.Open");
                                        CardDispenser.Open(CardDispenserPort);
                                        CardDispenserStatus = CardDispenser.CardDispenserStatus;
                                    }

                                    if (CardDispenserStatus.CardDespensePos)
                                    {
                                        ToLog(2, "Cashier: Initialize: CardDispenser.SendCardToRecycleBox");
                                        CardDispenser.SendCardToRecycleBox();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                    }
                                    if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                                    {
                                        CardDispenser.SendCardToBezelAndHold();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                        CardEjectTime = DateTime.Now;
                                    }

                                }
                            }
                        }
                    }

                    if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                    {
                        if (!EmulatorEnabled ||
                             ((EmulatorForm != null)
                               && (EmulatorForm.Emulator != null)
                               && (EmulatorForm.Emulator.CardReaderWork != null)
                               && EmulatorForm.Emulator.CardReaderWork.Checked
                               && !EmulatorForm.Emulator.CardReaderVirt.Checked))
                        {
                            if ((CardReader != null) && !CardReader.IsOpen)
                            {
                                CardReader.SectorNumber = SectorNumber;
                                CardReader.KeyA = keyA;
                                ToLog(2, "Cashier: Initialize: CardReader.Open");
                                if (CardReaderModel != "HID OmniKey")
                                    CardReader.Open(CardReaderPort);
                                else
                                    CardReader.Open();
                                CardReaderIsOpen = CardReader.IsOpen;
                                if (CardReader.IsOpen)
                                    CardReader.InitPool();
                            }
                        }
                    }

                    if (deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist)
                    {
                        if (!EmulatorEnabled ||
                            ((EmulatorForm != null)
                            && (EmulatorForm.Emulator != null)
                            && (EmulatorForm.Emulator.CashDispenserWork != null)
                            && EmulatorForm.Emulator.CashDispenserWork.Checked
                            && !EmulatorForm.Emulator.CashDispenserVirt.Checked))
                        {
                            if (CashDispenser != null)
                            {
                                bool res;
                                ToLog(2, "Cashier: Initialize: CashDispenser.Open");
                                if (CashDispenserModel == "Multimech")
                                    res = CashDispenser.Open(CashDispenserPort);
                                else
                                {
                                    if (!CashDispenser.IsOpen)
                                        res = CashDispenser.Open(CashAcceptorPort);
                                    else
                                        res = true;
                                }
                                if (res)
                                {
                                    TerminalStatusChanged = true;

                                    CashDispenserWorkChanged = true;
                                    CashDispenserWork = true;
                                    ToLog(2, "Cashier: Initialize: CashDispenser.CheckStatus");
                                    CashDispenser.CheckStatus();


                                    if (CashDispenser.CashDispenserResult.S == CashDispenserStatusT.SuccessfulCommand)
                                    {
                                        if (ShiftWork)
                                        {
                                            if (ShiftOpened)
                                            {
                                                CashDispenser.OpenCassette();
                                                CashDispenser.ReadCassetteID();
                                                if (
                                                    (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                    (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                    )
                                                {
                                                    ToLog(1, string.Format("Cashier: Initialize: CashAcceptor.CashAcceptorStatus.Cassettes[1].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                                    if (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                                    {
                                                        ToLog(1, "Cashier: Initialize: DeliveryUpCount Empty");
                                                        DeliveryUpCount = 0;
                                                        CashDispenser.SetCount(1, 0);
                                                        DeliveryUpCountChanged = true;
                                                    }
                                                }
                                                if (
                                                    (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                    (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                    )
                                                {
                                                    ToLog(1, string.Format("Cashier: Initialize: CashAcceptor.CashAcceptorStatus.Cassettes[2].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                                    if (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                                    {
                                                        ToLog(1, "Cashier: Initialize: DeliveryDownCount Empty");
                                                        DeliveryDownCount = 0;
                                                        CashDispenser.SetCount(2, 0);
                                                        DeliveryDownCountChanged = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                CashDispenser.CloseCassette();
                                            }
                                        }
                                        else
                                        {
                                            CashDispenser.CloseCassette();
                                            CashDispenser.OpenCassette();
                                            CashDispenser.ReadCassetteID();
                                            if (
                                                (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                )
                                            {
                                                ToLog(1, string.Format("Cashier: Initialize: CashAcceptor.CashAcceptorStatus.Cassettes[1].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                                if (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                                {
                                                    ToLog(1, "Cashier: Initialize: DeliveryUpCount Empty");
                                                    DeliveryUpCount = 0;
                                                    CashDispenser.SetCount(1, 0);
                                                    DeliveryUpCountChanged = true;
                                                }
                                            }
                                            if (
                                                (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                                (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                                )
                                            {
                                                ToLog(1, string.Format("Cashier: Initialize: CashAcceptor.CashAcceptorStatus.Cassettes[2].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                                if (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                                {
                                                    ToLog(1, "Cashier: Initialize: DeliveryDownCount Empty");
                                                    DeliveryDownCount = 0;
                                                    CashDispenser.SetCount(2, 0);
                                                    DeliveryDownCountChanged = true;
                                                }
                                            }

                                        }
                                        if (CashDispenser.IsOpen)
                                            CashDispenser.Pool = true;
                                    }
                                }
                            }
                        }
                    }

                    if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist)
                    {
                        if (!EmulatorEnabled ||
                            ((EmulatorForm != null)
                            && (EmulatorForm.Emulator != null)
                            && (EmulatorForm.Emulator.CashAcceptorWork != null)
                            && EmulatorForm.Emulator.CashAcceptorWork.Checked
                            && !EmulatorForm.Emulator.CashAcceptorVirt.Checked))
                        {
                            if ((CashAcceptor != null) && !CashAcceptor.IsOpen)
                            {
                                ToLog(2, "Cashier: Initialize: CashAcceptor.Open");
                                if (CashAcceptor.Open(CashAcceptorPort))
                                {
                                    if (CashAcceptorModel == "iVision")
                                    {
                                        ToLog(1, "CashAcceptor Version: " + CashAcceptor.Version);
                                        ToLog(1, "CashAcceptor BootVersion: " + CashAcceptor.BootVersion);
                                    }
                                    else
                                    {
                                        ToLog(1, "CashRecycler Version: " + CashAcceptor.Version);
                                        ToLog(1, "CashRecycler BootVersion: " + CashAcceptor.BootVersion);
                                    }
                                }
                            }
                        }
                    }
                    try
                    {
                        CashAcceptorCurrent = deviceCMModel.CashAcceptorCurrent;
                    }
                    catch
                    {
                        CashAcceptorCurrent = 0;
                    }
                    TechForm.CashAcceptorCurrent.Text = CashAcceptorCurrent.ToString();
                    try
                    {
                        CoinAcceptorCurrent = deviceCMModel.CoinAcceptorCurrent;
                    }
                    catch
                    {
                        CoinAcceptorCurrent = 0;
                    }
                    TechForm.CoinAcceptorCurrent.Text = CoinAcceptorCurrent.ToString();

                    try
                    {
                        DeliveryUpCount = (int)deviceCMModel.CountUp;
                    }
                    catch
                    {
                        DeliveryUpCount = 0;
                    }
                    //TechForm.TopBoxCount.Text = DeliveryUpCount.ToString();
                    try
                    {
                        DeliveryDownCount = (int)deviceCMModel.CountDown;
                    }
                    catch
                    {
                        DeliveryDownCount = 0;
                    }
                    //TechForm.BottomBoxCount.Text = DeliveryDownCount.ToString();
                    try
                    {
                        Delivery1Count = (int)deviceCMModel.Count1;
                    }
                    catch
                    {
                        Delivery1Count = 0;
                    }
                    try
                    {
                        Delivery2Count = (int)deviceCMModel.Count2;
                    }
                    catch
                    {
                        Delivery2Count = 0;
                    }
                    try
                    {
                        RejectCount = (int)deviceCMModel.RejectCount;
                    }
                    catch
                    {
                        RejectCount = 0;
                    }

                    if (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists)
                    {
                        if (!EmulatorEnabled ||
                            ((EmulatorForm != null)
                            && (EmulatorForm.Emulator != null)
                            && (EmulatorForm.Emulator.KKMWork != null)
                            && EmulatorForm.Emulator.KKMWork.Checked
                            && !EmulatorForm.Emulator.KKMVirt.Checked))
                        {
                            if ((KKM != null) && !KKM.DeviceEnabled)
                            {
                                try
                                {
                                    KKM.PortNumber = Convert.ToInt16(KKMPort.Substring(3));
                                }
                                catch { KKM.PortNumber = 1; };
                                /*KKM.AccessPassword = TechForm.CommandPassword.Text;
                                KKM.UseAccessPassword = true;
                                KKM.DefaultPassword = TechForm.RegistrationPass.Text;
                                KKM.OperatorName = deviceCMModel.CashierNumber.ToString();*/
                                ToLog(2, "Cashier: Initialize: KKM.FR_Connect");
                                ShiftFromKKMChecked = false;
                                KKM.FR_Connect(deviceCMModel.CashierNumber.ToString(), KKMPassword, KKMModePassword);
                                KKMStatusCheckDT = DateTime.Now;
                                /*if (KKM.DeviceEnabled)
                                {
                                    //KKM.SetPrezenter(0, 0, 2);

                                    ToLog(2, "Cashier: Initialize: KKM.GetStatus");
                                    KKM.GetStatus();
                                    KKMResultCode = KKM.ResultCode;
                                    KKMResultDescription = KKM.ResultDescription;

                                    if (KKMResultCode == CommonKKM.ErrorCode.Sucsess)
                                    {
                                        ToLog(2, "Cashier: Initialize: ShiftFromKKM");
                                        ShiftFromKKM();
                                    }

                                    if (KKMResultCode == CommonKKM.ErrorCode.Sucsess)
                                    {
                                        ToLog(2, "Cashier: Initialize: KKM.GetResources");
                                        KKM.GetResources();
                                    }
                                    KKMResultCode = KKM.ResultCode;
                                    KKMResultDescription = KKM.ResultDescription;
                                    LastZReport = KKM.Resources.LastZNumber;
                                }*/
                            }
                        }
                        else
                        {
                            LastZReport = 0;
                        }
                    }
                    if (CashierType == 0)
                    {
                        if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
                        {
                            if (!EmulatorEnabled ||
                                ((EmulatorForm != null)
                                && (EmulatorForm.Emulator != null)
                                && (EmulatorForm.Emulator.CoinAcceptorWork != null)
                                && EmulatorForm.Emulator.CoinAcceptorWork.Checked
                                && !EmulatorForm.Emulator.CoinAcceptorVirt.Checked))
                            {
                                if ((CoinAcceptor != null) && !CoinAcceptor.IsOpen)
                                {
                                    byte[] pin = new byte[]
                                    {
                                byte.Parse(CoinAcceptorPIN.Substring(0,2), NumberStyles.HexNumber),
                                byte.Parse(CoinAcceptorPIN.Substring(2,2), NumberStyles.HexNumber),
                                byte.Parse(CoinAcceptorPIN.Substring(4,2), NumberStyles.HexNumber),
                                byte.Parse(CoinAcceptorPIN.Substring(6,2), NumberStyles.HexNumber),
                                byte.Parse(CoinAcceptorPIN.Substring(8,2), NumberStyles.HexNumber),
                                byte.Parse(CoinAcceptorPIN.Substring(10,2), NumberStyles.HexNumber)
                                    };

                                    ToLog(2, "Cashier: Initialize: CoinAcceptor.Open");
                                    if (CoinAcceptor.Open(pin, Convert.ToInt16(CoinAcceptorPort.Substring(3)),
                                        Convert.ToByte(deviceCMModel.CoinAcceptorAddress)) != 0)
                                    {
                                        ToLog(2, "Cashier: Initialize: CoinAcceptor.Close");
                                        CoinAcceptor.Close();
                                    }
                                    CoinAcceptorStatusCheckDT = DateTime.Now;
                                }
                            }
                        }
                    }
                    if (CashierType == 0)
                    {
                        if (deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist)
                        {
                            if (!EmulatorEnabled ||
                                ((EmulatorForm != null)
                                && (EmulatorForm.Emulator != null)
                                && (((EmulatorForm.Emulator.HopperWork1 != null)
                                && EmulatorForm.Emulator.HopperWork1.Checked
                                && !EmulatorForm.Emulator.HopperVirt1.Checked)
                                ||
                                ((EmulatorForm.Emulator.HopperWork2 != null)
                                && EmulatorForm.Emulator.HopperWork2.Checked
                                && !EmulatorForm.Emulator.HopperVirt2.Checked)
                                )))
                            {
                                if ((CoinHopper != null) && !CoinHopper.IsOpen)
                                {
                                    ToLog(2, "Cashier: Initialize: CoinHopper.Open");
                                    CoinHopper.Open(new byte[] { 1, 2, 3, 4 }, Convert.ToInt16(CoinHopperPort.Substring(3)));
                                    if (CoinHopper.IsOpen)
                                    {
                                        Thread.Sleep(100);
                                        CoinHopper.Reset(false);
                                        Thread.Sleep(100);
                                        CoinHopper.Reset(true);
                                    }


                                }
                            }
                        }
                    }

                    CardReaderEventSenderBackgroundWorker = new BackgroundWorker();
                    TalkOperatorEventSenderBackgroundWorker = new BackgroundWorker();
                    CheckEventServerBackgroundWorker = new BackgroundWorker();
                    if (TerminalWork != TerminalWorkT.TerminalClosed)
                    {
                        if (deviceCMModel.ServerExchangeProtocolId != null)
                        {
                            /*if (ShiftWork)
                            {
                                if (ShiftOpened)
                                {
                                    if ((int)deviceCMModel.ServerExchangeProtocolId == 2)
                                        InitializeCheckEventWebClientBackgroundWorker();
                                }
                            }
                            else
                                if ((int)deviceCMModel.ServerExchangeProtocolId == 2)
                                InitializeCheckEventWebClientBackgroundWorker();*/
                            if (deviceCMModel.ServerExchangeProtocolId == 2)
                            {
                                ToLog(2, "Cashier: Initialize: InitializeCheckEventWebClientBackgroundWorker");
                                InitializeCheckEventWebClientBackgroundWorker();
                            }
                        }
                        if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                        {
                            ToLog(2, "Cashier: Initialize: InitializeCardReaderEventSenderBackgroundWorker");
                            InitializeCardReaderEventSenderBackgroundWorker();
                        }
                        ToLog(2, "Cashier: Initialize: InitializeTalkOperatorEventSenderBackgroundWorker");
                        InitializeTalkOperatorEventSenderBackgroundWorker();
                    }

                    CardReaderEventSenderBackgroundWorker.RunWorkerAsync();
                    TalkOperatorEventSenderBackgroundWorker.RunWorkerAsync();

                    Transaction = new TransactionT();
                    Transaction.BanknotesAccepted = new AccetpedT[
                        Banknotes.Count];
                    for (int i = 0; i < Banknotes.Count; i++)
                    {
                        Transaction.BanknotesAccepted[i].Nominal = Convert.ToDecimal(Banknotes[i].Value);
                        Transaction.BanknotesAccepted[i].Count = 0;
                    }

                    Transaction.CoinsAccepted = new AccetpedT[Coins.Count];
                    for (int i = 0; i < Coins.Count; i++)
                    {
                        Transaction.CoinsAccepted[i].Nominal = Convert.ToDecimal(Coins[i].Value);
                        Transaction.CoinsAccepted[i].Count = 0;
                    };
                    if (CashierType != 2)
                    {
                        ToLog(2, "Cashier: Initialize: GenerateLabels");
                        GenerateLabels();
                        ToLog(2, "Cashier: Initialize: CashDispenserSetSettings");
                        CashDispenserSetSettings();
                    }

                }
                catch (Exception exc)
                {
                    ToLog(0, exc.ToString());
                    Initalized = true;
                };

                if (!EmulatorEnabled && CashierType != 2)
                {
                    Topmost = true;
                    TechForm.Topmost = true;
                }

                if (EmulatorEnabled)
                    if (EmulatorForm != null && EmulatorForm.Emulator.ExternalDoorVirt.Checked && EmulatorForm.Emulator.ExternalDoorOpen.Checked)
                        ExternalDoorOpen = true;
                    else
                        ExternalDoorOpen = false;

                if ((ExternalDoorOpen || (!EmulatorEnabled && (deviceCMModel.SlaveExist == null || SlaveController == null || !SlaveControllerWork))) && (CashierType != 2))
                {
                    lock (ConsoleLog)
                    {
                        string text = "";
                        foreach (string s in ConsoleLog)
                            text += s + Environment.NewLine;
                        TechForm.ConsoleLog.Text = text;
                        ConsoleLogChanged = false;
                    }

                    //if (CashierType != 2)
                    TechForm.Visibility = Visibility.Visible;
                    this.Visibility = Visibility.Hidden;
                }
                else
                {
                    if (CashierType != 2)
                        this.Visibility = Visibility.Visible;
                    TechForm.Visibility = Visibility.Hidden;
                }
                CheckSettingsUpdate = DateTime.Now;
                CardStatusToServerUpdate = DateTime.Now;

                Compute comp = new Compute();
                ToLog(2, "Cashier: Initialize: ComputeHash");
                LicenseWork = comp.ComputeHash(db, device.SlaveCode);
                ToLog(2, "Cashier: Initialize: Utils.Run");
                if (conserver != null)
                    SyncTask = Utils.Run(conserver, conlocal, 5000);

                string[] pref = new string[] { "http://+:3333/" };
                webServer = new EventWebServer(pref, SendResponse, log);
                webServer.Run();
                if (deviceCMModel != null && deviceCMModel.ServerURL != null && CashierType != 2)
                {
                    Talk = new OperatorTalk(deviceCMModel.ServerURL, DeviceId.ToString(), log);
                    ToLog(2, "Cashier: Initialize: Talk.Init");
                    Talk.Init();
                    this.Activate();
                }

                UpdateDevice.StartThreadAction(DeviceId, CurrentVersion, TypeDevice.Cashier, null, conlocal);

                if (CashierType == 2 && CashierIcon != null)
                    CashierIcon.Visible = true;

                LastTopMost = DateTime.Now;

                Initalized = true;
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                Initalized = true;
                //this.Close();
                //показываем иконку в панели
                if (CashierType == 2 && CashierIcon != null)
                    CashierIcon.Visible = true;
            }
        }


        private void DoSwitchTerminalWork()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Start");
            try
            {
                //ToLog(2, "Cashier: DoSwitchTerminalWork: TerminalWork: " + TerminalWork.ToString());
                switch (TerminalWork)
                {
                    case TerminalWorkT.TerminalClosed:
                        CasseStatusLabel = Properties.Resources.CashierDontStarted;
                        if (this.Visibility == Visibility.Visible)
                            this.Visibility = Visibility.Hidden;

                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                        {
                            if (EmulatorForm.Emulator.CasseStatusLabel1.Text != Properties.Resources.CashierDontStarted)
                                EmulatorForm.Emulator.CasseStatusLabel1.Text = Properties.Resources.CashierDontStarted;
                            if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                        }
                        break;
                    case TerminalWorkT.TerminalDontWork:
                        CasseStatusLabel = Properties.Resources.TerminalNotWork;
                        //ToLog(2, "Cashier: DoSwitchTerminalWork: OutOfService");
                        OutOfService();

                        if (EmulatorEnabled)
                        {
                            if (ShiftWork)
                            {
                                if (!EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Show();
                                if (EmulatorForm.Emulator.ShiftStatusLabel1.Text != (ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed))
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Text = (ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed);
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Hide();
                            }
                            if (EmulatorForm.Emulator.CasseStatusLabel1.Text != Properties.Resources.TechTerminalNotWork)
                                EmulatorForm.Emulator.CasseStatusLabel1.Text = Properties.Resources.TechTerminalNotWork;
                            if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                        }
                        break;
                    case TerminalWorkT.TerminalWork:
                        //ToLog(2, "Cashier: DoSwitchTerminalWork: InService");
                        InService();
                        if (EmulatorEnabled)
                        {
                            if (EmulatorForm.Emulator.CasseStatusLabel1.Text != Properties.Resources.CashierStarted)
                                EmulatorForm.Emulator.CasseStatusLabel1.Text = Properties.Resources.CashierStarted;
                            if (ShiftWork)
                            {
                                if (!EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Show();
                                if (EmulatorForm.Emulator.ShiftStatusLabel1.Text != (ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed))
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Text = (ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed);
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.ShiftStatusLabel1.Visible)
                                    EmulatorForm.Emulator.ShiftStatusLabel1.Hide();
                            }

                            if (!ExternalDoorOpen && EmulatorForm.Emulator.CardDispenserVirt.Checked && EmulatorForm.Emulator.CardDispenserWork.Checked && deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                            {
                                if (!EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardDispenserEditsGroupBox.Show();
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                            }

                            if (!EmulatorForm.Emulator.CardDispenserVirt.Checked || !EmulatorForm.Emulator.CardDispenserWork.Checked)
                            {
                                if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                            }
                            if (!EmulatorForm.Emulator.CashAcceptorVirt.Checked || !EmulatorForm.Emulator.CashAcceptorWork.Checked)
                            {
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                            }
                            if (!EmulatorForm.Emulator.CoinAcceptorVirt.Checked || !EmulatorForm.Emulator.CoinAcceptorWork.Checked)
                            {
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                            }
                            if (!EmulatorForm.Emulator.BankModuleVirt.Checked || !EmulatorForm.Emulator.BankModuleWork.Checked)
                            {
                                if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                    EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                            }
                        }
                        break;
                }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void CommonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                Calc AddingSummForm;
                FrameworkElement feSource = e.Source as FrameworkElement;

                ToLog(2, "Cashier: CommonClickHandler: feSource.Name:" + feSource.Name);
                switch (feSource.Name)
                {
                    case "OperatorTalkB":
                        CallUpdate = DateTime.Now;
                        CallStart = DateTime.Now;
                        if (Talk != null)
                        {
                            ToLog(2, "Cashier: CommonClickHandler: Talk.Call");
                            Talk.Call();
                        }
                        break;
                    case "ChangeLanguageButton":
                        CasseStatusLabel = "";
                        ToLog(2, "Cashier: CommonClickHandler: OutOfService");
                        OutOfService();
                        if (LanguagePanel.Visibility != Visibility.Visible)
                            LanguagePanel.Visibility = Visibility.Visible;
                        TerminalWork = TerminalWorkT.TerminalChangeLanguage;
                        break;
                    case "WorkCancel":
                        ToLog(2, "Cashier: CommonClickHandler: PaymentOff");
                        PaymentOff();
                        ToLog(1, ResourceManager.GetString("ToLogCancel", DefaultCulture));
                        terminalStatus = TerminalStatusT.PreCancelled;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        terminalStatusChangeInternal = true;

                        break;
                    case "PenalButton":
                        terminalStatus = TerminalStatusT.PenalCardRequest;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        CardSum = 0;
                        AmountDue = 0;
                        Paid = 0;
                        CardReaderStatusChanged = true;
                        terminalStatusChangeInternal = true;
                        if (PenalCardTimeOut > 0)
                            PenalCardRequestTime = DateTime.Now;
                        break;
                    case "EndPayment":
                    case "EndPaymentD":
                    case "EndPaymentDelivery":
                    case "EndPaymentDeliveryB":
                    case "EndPaymentB":

                        ToLog(1, ResourceManager.GetString("ToLogEndPayment", DefaultCulture));

                        ToLog(2, "Cashier: CommonClickHandler: PaymentOff");
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "EndPaymentToCard":
                    case "EndPaymentToCardB":
                        ToLog(1, ResourceManager.GetString("ToLogEndPayment", DefaultCulture));
                        CrossPay = crossPay.Last;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "EndPaymentContinue":
                    case "EndPaymentContinueB":
                        CrossPay = crossPay.Last;
                        break;
                    case "AdditionSumButton":
                        ToLog(2, "Cashier: CommonClickHandler: AddingSummForm.ShowDialog");
                        AddingSummForm = new Calc();
                        AddingSummForm.Left = 296;
                        AddingSummForm.Top = 208;
                        AddingSummForm.LayoutRoot.RowDefinitions[0].Height = new GridLength(40);
                        AddingSummForm.Header.Visibility = Visibility.Visible;
                        AddingSummForm.Header.Text = Properties.Resources.AddingSymm1;
                        AddingSummForm.btn_Minus.Visibility = Visibility.Hidden;
                        AddingSummForm.Len = 10;
                        //AddingSummForm.Owner = (Window)TopLevelControl;
                        AddingSummForm.Topmost = true;
                        AddingSummForm.ShowDialog();
                        if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                        {
                            ToLog(2, "Cashier: CommonClickHandler: AddingSummForm.ShowDialog: Result");
                            try
                            {
                                AddingSumm = Convert.ToInt32(AddingSummForm.Sum);
                                if (AddingSumm > 0)
                                {
                                    if ((AddingSumm > CardQuery) && BankModuleWork)
                                    {
                                        BankQuerySumm = AddingSumm;
                                        AddingSummChanged = true;
                                        BankQuerySummRequested = true;
                                        DisplayStatusChanged = true;
                                        if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                            if (BankModule != null && BankModuleWork)
                                            {
                                                BankModule.GetOpStatus(true);
                                                Thread.Sleep(500);
                                            }
                                    }
                                    else
                                    {
                                        ToLog(0, ResourceManager.GetString("ToLogBankRequestError", DefaultCulture));
                                    }
                                }
                            }
                            catch
                            {
                                AddingSumm = 0;
                            }
                        }
                        else
                        {
                            AddingSumm = 0;
                        };

                        break;
                    case "keyboard_Control":
                        ToLog(2, "Cashier: CommonClickHandler: AddingSummForm.ShowDialog");
                        AddingSummForm = new Calc();
                        AddingSummForm.Left = 296;
                        AddingSummForm.Top = 208;
                        //AddingSummForm.Owner = (Window)TopLevelControl;
                        AddingSummForm.Topmost = true;
                        AddingSummForm.ShowDialog();
                        if (AddingSummForm.DialogResult.HasValue && AddingSummForm.DialogResult.Value)
                        {
                            ToLog(2, "Cashier: CommonClickHandler: AddingSummForm.ShowDialog: Result");
                            CarNumber = AddingSummForm.Sum;
                            if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                EmulatorForm.Emulator.CarNumber.Text = CarNumber;
                            terminalStatus = TerminalStatusT.CardInserted;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: CommonClickHandler5: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                            terminalStatusChangeInternal = true;
                        }
                        break;
                    case "TS1":
                        ToAbonement = TariffPlanTariffScheduleList[0].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler6: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "TS2":
                        ToAbonement = TariffPlanTariffScheduleList[1].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler7: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "TS3":
                        ToAbonement = TariffPlanTariffScheduleList[2].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler8: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "TS4":
                        ToAbonement = TariffPlanTariffScheduleList[3].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler9: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "TS5":
                        ToAbonement = TariffPlanTariffScheduleList[4].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: CommonClickHandler10: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "TS6":
                        ToAbonement = TariffPlanTariffScheduleList[5].Id;
                        terminalStatus = TerminalStatusT.CardPrePaid;
                        if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        break;
                    case "CN":
                        ToLog(2, "Cashier: CommonClickHandler: ChangeLanguage");
                        ChangeLanguage("zh-Hans");
                        TerminalWork = TerminalWorkT.TerminalWork;
                        InService();
                        break;
                    case "BankPaymentCancel":

                        if (BankPaymentCancel.Visibility != Visibility.Hidden)
                            BankPaymentCancel.Visibility = Visibility.Hidden;

                        if (BankModule != null && BankModule.CardInserted)
                        {
                            BankModule.CardInserted = false;
                        }
                        PaymentBankCardOff();
                        Thread.Sleep(1000);
                        //DevicePaiments = PaimentMethodT.PaimentsAll;
                        if (Paiments != DevicePaiments)
                        {
                            ToLog(2, "Cashier: CommonClickHandler: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                            Paiments = DevicePaiments;
                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.Paiments = DevicePaiments;
                        }
                        //BankCardErrorDateTime = DateTime.Now;
                        //BankModuleStatus = BankModuleStatusT.BankCardNotSuccess;
                        //BankModule.CommandStatus = CommandStatusT.Undeifned;
                        //vPaymentBankOn = false;
                        break;
                    default:
                        ToLog(2, "Cashier: CommonClickHandler: ChangeLanguage");
                        ChangeLanguage(feSource.Name);
                        TerminalWork = TerminalWorkT.TerminalWork;
                        InService();
                        break;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }

        public void ExternalDeviceTimer_Tick(object sender, EventArgs e)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            //try
            {
                lock (ExternalDeviceTimerLock)
                {
                    ExternalDeviceTimer.Enabled = false;

                    ErrorsText = "";

                    if (!Initalized)
                    {
                        ToLog(2, "Cashier: ExternalDeviceTimer_Tick: Initialize");
                        Initialize();
                        //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: Initialize End");
                    }
                    if (CashierType == 2)
                    {
                        if (CashierIcon != null && !CashierIcon.Visible)
                            CashierIcon.Visible = true;
                    }
                    else
                    {
                        if (CashierIcon != null && CashierIcon.Visible)
                            CashierIcon.Visible = false;
                    }

                    if (ConnectFromLevel2.ConnectStringUpdated)
                    {
                        lock (ConnectFromLevel2.Locker)
                        {
                            ConnectFromLevel2.ConnectStringUpdated = false;
                        }
                        string conserverReg;
                        string conserver1 = null;
                        if (currRegistryKey.GetValue("conserverReg") != null)
                        {
                            conserverReg = currRegistryKey.GetValue("conserverReg").ToString();
                            SqlConnectionStringBuilder cb = new SqlConnectionStringBuilder(conserverReg);
                            string Password = cb.Password;
                            string Password1 = Password = cryp.DecryptStringFromBytes_Aes(cryp.StringToByteA(Password));
                            cb.Password = Password1;
                            conserver1 = cb.ToString();
                        }

                        if (conserver1 != null && conserver != conserver1)
                        {
                            Utils.Work = false;
                            while (Utils.Running)
                                Thread.Sleep(50);

                            conserver = conserver1;
                            SyncTask = Utils.Run(conserver, conlocal, 5000);
                        }
                    }

                    if (ChequeDT != null &&
                        (
                        ((deviceCMModel.ChequeTimeOut != null) &&
                        ((DateTime.Now - (DateTime)ChequeDT).Seconds > (int)deviceCMModel.ChequeTimeOut))
                        || (terminalStatus == TerminalStatusT.PenalCardRequest)
                        || (terminalStatus == TerminalStatusT.PenalCardDisplay)
                        || (terminalStatus == TerminalStatusT.CardReaded)
                        || (terminalStatus == TerminalStatusT.CardError)
                        || (terminalStatus == TerminalStatusT.CardInStopList)
                        || (terminalStatus == TerminalStatusT.CardJam)
                        || (terminalStatus == TerminalStatusT.CardReadedError)
                        //|| (terminalStatus == TerminalStatusT.CardReadError)
                        || (terminalStatus == TerminalStatusT.CardReadedPlus)
                        || (terminalStatus == TerminalStatusT.CardReadedMinus)
                        || (terminalStatus == TerminalStatusT.CardInserted)
                        )
                       )
                    {
                        ToLog(2, "Cashier: ExternalDeviceTimer_Tick: ChequeTimeOut");
                        ChequeDT = null;
                        if (KKM != null /*&& KKM.Initialised*/)
                        {
                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: KKM.SetPrezenter");
                            if (deviceCMModel.ChequeWork == 1)
                                KKM.SetPrezenter(0, 0, 1);
                            else if (deviceCMModel.ChequeWork == 2)
                                KKM.SetPrezenter(1, 0, 1);
                        }
                    }
                    if (terminalStatus == TerminalStatusT.Empty)
                    {
                        if ((CashierType == 2)
                            ||
                             ((CardDispenser != null) && CardDispenser.IsOpen &&
                             !CardDispenserStatus.CardPreSendPos &&
                             !CardDispenserStatus.CardDispensing &&
                             !CardDispenserStatus.CardDespensePos &&
                             !CardDispenserStatus.CardAccepting))
                        {
                            if (UpdateDevice.Instance != null && UpdateDevice.Instance.NeedRestart)
                            {
                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick: Updater.RunUpdaterKassa");
                                if (UpdateDevice.Instance.UpdateDb())
                                {
                                    UpdateDevice.Instance.RunRestartProgramm();
                                    Close();
                                }
                                else
                                    TerminalWork = TerminalWorkT.TerminalClosed;
                            }
                        }
                    }
                    if (deviceCMModel != null)
                    {
                        #region EmulatorSettings
                        SetEmulator();
                        #endregion EmulatorSettings

                        SystemTime.Content = DateTime.Now.ToString("g");
                        if (Talk == null)
                        {
                            if (deviceCMModel.ServerURL != null && CashierType != 2)
                            {
                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick: Talk.Init");
                                Talk = new OperatorTalk(deviceCMModel.ServerURL, DeviceId.ToString(), log);
                                Talk.Init();
                                this.Activate();
                            }
                        }

                        ErrorsText = "";
                        #region Инкассация
                        if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist)
                            if (CashAcceptorCurrent > (CashAcceptorMaxCount - CashAcceptorMax))
                            {
                                ErrorsText += ResourceManager.GetString("TechCashAcceptorIncasseReq", DefaultCulture) + Environment.NewLine;
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                TerminalStatusChanged = true;
                            }
                        if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
                        {
                            if (CoinAcceptorCurrent > (CoinAcceptorMaxCount - CoinAcceptorMax))
                            {
                                ErrorsText += ResourceManager.GetString("TechCoinAcceptorIncasseReq", DefaultCulture) + Environment.NewLine;
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                TerminalStatusChanged = true;
                            }
                        }
                        #endregion Инкассация

                        #region Синхронизация настроек
                        if (
                        (DateTime.Now > CheckSettingsUpdate + TimeSpan.FromSeconds(1)) &&
                        (terminalStatus == TerminalStatusT.Empty)
                        )// Касса свободна
                        {
                            try
                            {
                                DateTime date1 = new DateTime(2000, 1, 1), date2 = new DateTime(2000, 1, 1), date3 = new DateTime(2000, 1, 1);
                                string strhas = "select DeviceCMModel.Sync from DeviceCMModel where DeviceCMModel.DeviceId='" + DeviceId.ToString() + "'";
                                using (SqlCommand comm = new SqlCommand(strhas, db))
                                {
                                    object res = comm.ExecuteScalar();
                                    if (res != null && res != System.DBNull.Value)
                                    {
                                        try
                                        {
                                            date1 = Convert.ToDateTime(res);
                                        }
                                        catch { }
                                    }
                                }
                                strhas = "select max(CashAcceptorAllowModel.Sync) from CashAcceptorAllowModel where CashAcceptorAllowModel.DeviceId='" + DeviceId.ToString() + "'";
                                using (SqlCommand comm = new SqlCommand(strhas, db))
                                {
                                    object res = comm.ExecuteScalar();
                                    if (res != null && res != System.DBNull.Value)
                                    {
                                        try
                                        {
                                            date2 = Convert.ToDateTime(res);
                                        }
                                        catch { }
                                    }
                                }

                                strhas = "select max(CoinAcceptorAllowModel.Sync) from CoinAcceptorAllowModel where CoinAcceptorAllowModel.DeviceId='" + DeviceId.ToString() + "'";
                                using (SqlCommand comm = new SqlCommand(strhas, db))
                                {
                                    object res = comm.ExecuteScalar();
                                    if (res != null && res != System.DBNull.Value)
                                    {
                                        try
                                        {
                                            date3 = Convert.ToDateTime(res);
                                        }
                                        catch { }
                                    }
                                }

                                strhas = "select max(DeviceAction.WriteTime) from DeviceAction";
                                using (SqlCommand comm = new SqlCommand(strhas, db))
                                {
                                    object res = comm.ExecuteScalar();
                                    if (res != null && res != System.DBNull.Value)
                                    {
                                        try
                                        {
                                            log.MaxWriteTime = Convert.ToInt16(res);
                                        }
                                        catch { }
                                    }
                                }

                                if (!(TechForm.Visibility == Visibility.Visible))
                                {
                                    if (Math.Max(Math.Max(date1.Ticks, date2.Ticks), date3.Ticks) > SettingsLastUpdate.Ticks)
                                    {
                                        string req = "select top 1 * from DeviceCMModel where DeviceId='" + DeviceId.ToString() + "' and (_IsDeleted is null or _IsDeleted = 0) ";
                                        deviceCM = null;
                                        lock (db_lock)
                                        {
                                            try
                                            {
                                                deviceCM = UtilsBase.FillRow(req, db, null, null);
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    deviceCM = UtilsBase.FillRow(req, db, null, null);
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }

                                        deviceCMModel = SetDeviceCM(deviceCM);
                                        GetSettings(false);
                                    }
                                    CheckSettingsUpdate = DateTime.Now;
                                }
                            }
                            catch
                            {

                            }
                        }
                        #endregion Синхронизация настроек

                        #region Таймаут штрафной карты
                        if ((terminalStatus == TerminalStatusT.PenalCardRequest) ||
                        (terminalStatus == TerminalStatusT.PenalCardPrePaiment))
                        {
                            bool Payment = false;
                            if (CashierType != 2)
                            {
                                /*if ((deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist) &&
                                !(EmulatorEnabled && EmulatorForm.Emulator.CashAcceptorVirt.Checked) &&
                                CashAcceptorWork &&
                                (
                                (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Escrow) ||
                                (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Stacking) ||
                                (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.VendValid) ||
                                (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Stacked)
                                )
                                )*/
                                if (Paiments != PaimentMethodT.PaimentsAll || Paid > 0)
                                    Payment = true;

                                if (!Payment)
                                {
                                    if (PenalCardRequestTime != DateTimeNull)
                                    {
                                        TimeSpan PenalCardWait = DateTime.Now - PenalCardRequestTime;
                                        TimeSpan PenalTimeOut = new TimeSpan(0, 0, PenalCardTimeOut);

                                        if (PenalCardWait > PenalTimeOut)
                                        {
                                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: PenalCardTimeOut");
                                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: PaymentOff");
                                            PaymentOff();
                                            PenalCardRequestTime = DateTimeNull;
                                            if (Paiments != PaimentMethodT.PaimentsAll)
                                            {
                                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                                            }
                                            Paiments = PaimentMethodT.PaimentsAll;
                                            ToLog(1, ResourceManager.GetString("ToLogOperationTimeout", DefaultCulture));
                                            if (terminalStatusOld != terminalStatus)
                                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                            terminalStatusOld = terminalStatus;
                                            log.EndContinues(299, DateTime.Now, null);

                                            if (terminalStatus != TerminalStatusT.Empty)
                                                terminalStatus = TerminalStatusT.Empty;
                                            if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                            terminalStatusChangeInternal = false;
                                            if (terminalStatusOld != terminalStatus)
                                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                            terminalStatusOld = terminalStatus;

                                            DoSwitchTerminalStatus();
                                            ExternalDeviceTimer.Enabled = true;
                                            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: PenalCardTimeOut End");
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion Таймаут штрафной карты

                        if (EmulatorEnabled)
                        {
                            DoLogEmulator();
                        }
                        if (!EmulatorEnabled || (EmulatorForm.Emulator.TerminalWork != TerminalWorkT.TerminalClosed))
                        {
                            /* Begin of Main Check's (Module 1)*/
                            if (TerminalWork != TerminalWorkT.TerminalChangeLanguage)
                            {
                                TerminalCheckStatus();
                            }
                            /* End of Main Check's (Module 1)*/

                            if (CashierType != 2)
                            {
                                if (ExternalDoorOpenChanged)
                                {
                                    if (!ExternalDoorOpen)
                                    {
                                        CleanTechPanels();
                                        ChangeLanguage(DefaultLanguage);
                                        if ((CashDispenser != null) && CashDispenser.IsOpen)
                                            lock (CashDispenser)
                                            {
                                                CashDispenser.ReadCassetteID();
                                            }

                                        if (!CashDispenserUpper)
                                            CashDispenserUpper = true;
                                        if (!CashDispenserLower)
                                            CashDispenserLower = true;
                                        if (!CoinDispenser1Hopper)
                                            CoinDispenser1Hopper = true;
                                        if (!CoinDispenser2Hopper)
                                            CoinDispenser2Hopper = true;
                                    }
                                }
                            }
                            if (!(TechForm.Visibility == Visibility.Visible))
                            {
                                if ((deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists) || (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist))
                                {
                                    CardStatusCheck();
                                }
                                else
                                {
                                    /* End of Wait (Module 2)*/
                                    if (EmulatorEnabled)
                                        if ((EmulatorForm != null) && (EmulatorForm.Emulator.terminalStatus != terminalStatus))
                                        {
                                            CardReaderStatusChanged = true;
                                            if (terminalStatusChangeInternal)
                                            {
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                            }
                                            else
                                            {
                                                terminalStatus = EmulatorForm.Emulator.terminalStatus;
                                            }
                                            terminalStatusChangeInternal = false;
                                            if (terminalStatus == TerminalStatusT.CardPaid)
                                            {
                                                PaimentsStatusChanged = true;
                                            }
                                        }
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: ExternalDeviceTimer_Tick3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                }
                                if (CashierType != 2)
                                {
                                    if (terminalStatus == TerminalStatusT.CardEjected)
                                    {
                                        #region Карта выдана в губы
                                        if (CardEjectTime == null)
                                        {
                                            if ((CardDispenser != null) && CardDispenser.IsOpen)
                                            {
                                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick: CardEjectTime == null CardDispenser.SendCardToRecycleBox");
                                                CardDispenser.SendCardToRecycleBox();
                                                while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                                    Thread.Sleep(100);
                                            }
                                            ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                                            if (terminalStatus != TerminalStatusT.Empty)
                                                terminalStatus = TerminalStatusT.Empty;
                                            if (terminalStatusOld != terminalStatus)
                                                ToLog(2, "Cashier: ExternalDeviceTimer_Tick4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                            terminalStatusOld = terminalStatus;
                                            if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                            terminalStatusChangeInternal = false;
                                            DoSwitchTerminalStatus();
                                        }
                                        else if ((DateTime)CardEjectTime != DateTimeNull)
                                        {
                                            TimeSpan CardWait = DateTime.Now - (DateTime)CardEjectTime;
                                            TimeSpan TimeOutToBasket = new TimeSpan(0, 0, CardTimeOutToBasket);
                                            if (CardWait > TimeOutToBasket)
                                            {
                                                CardEjectTime = DateTimeNull;
                                                CardReaderStatusChanged = true;
                                                if ((CardDispenser != null) && CardDispenser.IsOpen)
                                                {
                                                    ToLog(2, "Cashier: ExternalDeviceTimer_Tick: CardTimeout CardDispenser.SendCardToRecycleBox");
                                                    CardDispenser.SendCardToRecycleBox();
                                                    while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                                        Thread.Sleep(100);
                                                }
                                                ToLog(1, ResourceManager.GetString("ToLogCardToBasket", DefaultCulture));
                                                if (terminalStatus != TerminalStatusT.Empty)
                                                    terminalStatus = TerminalStatusT.Empty;
                                                if (terminalStatusOld != terminalStatus)
                                                    ToLog(2, "Cashier: ExternalDeviceTimer_Tick5: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                terminalStatusOld = terminalStatus;
                                                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                                terminalStatusChangeInternal = false;
                                                DoSwitchTerminalStatus();
                                            }
                                        }
                                        #endregion Карта выдана в губы
                                    }
                                }

                                if (CashierType != 2)
                                {
                                    if (terminalStatus == TerminalStatusT.CardReadedMinus
                                        || terminalStatus == TerminalStatusT.CardReadedPlus
                                        || terminalStatus == TerminalStatusT.PenalCardPaiment
                                        || terminalStatus == TerminalStatusT.PenalCardPrePaiment
                                        || terminalStatus == TerminalStatusT.PenalCardRequest
                                       )
                                    {
                                        PaimentsStatusCheck();
                                    }

                                    switch (terminalStatus)
                                    {
                                        case TerminalStatusT.Empty:
                                            deliveryStatus = DeliveryStatusT.Delivery;
                                            break;
                                        //case CardReaderStatusT.CardReaded:
                                        case TerminalStatusT.CardReadedPlus:
                                            deliveryStatus = DeliveryStatusT.NotDelivery;
                                            break;
                                        case TerminalStatusT.CardReadedMinus:
                                            deliveryStatus = DeliveryStatusT.Delivery;
                                            break;
                                        case TerminalStatusT.PenalCardRequest:
                                            deliveryStatus = DeliveryStatusT.Delivery;
                                            break;
                                        case TerminalStatusT.PenalCardPrePaiment:
                                            deliveryStatus = DeliveryStatusT.Delivery;
                                            break;
                                        case TerminalStatusT.PenalCardPaiment:
                                            deliveryStatus = DeliveryStatusT.Delivery;
                                            break;
                                        case TerminalStatusT.CardPrePaid:

                                        case TerminalStatusT.CardPaid:
                                            if (CardQuery == 0)
                                            {
                                                deliveryStatus = DeliveryStatusT.NotDelivery;
                                            }
                                            break;
                                    }
                                    string toLog = "";
                                    if (terminalStatus != TerminalStatusT.LongExitOperation)
                                    {
                                        if (deliveryStatus == DeliveryStatusT.Delivery)
                                        {
                                            if (CheckReturnPossible(DownDiff, ref toLog))
                                                deliveryStatus = DeliveryStatusT.Delivery;
                                            else
                                                deliveryStatus = DeliveryStatusT.NotDelivery;
                                        }
                                    }

                                    /*if (GiveDeliveryThread == null)
                                    {
                                        CheckNominals();

                                        if (SumDelivery == 0)
                                            Delivery = false;
                                    }*/

                                    if (Paiments == PaimentMethodT.PaimentsCash)
                                    {
                                        PaymentStatusLabel = Properties.Resources.Cash;
                                        PaymentStatusLabel1 = Properties.Resources.Cash1;
                                    }
                                    else if (Paiments == PaimentMethodT.PaimentsBankModule)
                                    {
                                        PaymentStatusLabel = Properties.Resources.Bank;
                                        PaymentStatusLabel1 = Properties.Resources.Bank1;
                                        if ((BankModuleStatus != BankModuleStatusT.BankCardNotSuccess) && (BankModuleStatus != BankModuleStatusT.BankCardEjected))
                                        {
                                            deliveryStatus = DeliveryStatusT.NotDelivery;
                                        }
                                    }
                                }

                                if (ShiftWork)
                                {
                                    int delta = rnd.Next(1, 10);
                                    DateTime sht = DateTime.Now;
                                    if (deviceCMModel.ShiftNewShiftTime != null)
                                        sht = deviceCMModel.ShiftNewShiftTime.Value;

                                    DateTime shift_change = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, (sht - TimeSpan.FromMinutes(delta)).Hour, (sht - TimeSpan.FromMinutes(delta)).Minute, 0);

                                    /*int d_min = shift_change.Minute;
                                    int d_hour = shift_change.Hour;*/

                                    int min = rnd.Next(50, 59);
                                    int sec = rnd.Next(0, 59);

                                    if ((terminalStatus == TerminalStatusT.Empty) &&
                                        (CashierType != 2) &&
                                        (deviceCMModel.ShiftAutoClose != null && (bool)deviceCMModel.ShiftAutoClose)
                                        &&
                                       ((LastShiftOpenTime != null) &&
                                       (DateTime.Now > LastShiftOpenTime.Value.Add(new TimeSpan(23, min, sec)))/*
                                       ||((DateTime.Now > LastShiftOpenTime) && (DateTime.Now > shift_change))*/
                                       )
                                       &&
                                       (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists)
                                    )
                                    {
                                        ToLog(2, "Cashier: ExternalDeviceTimer_Tick: ShiftSwitch");
                                        ShiftSwitch();
                                    }
                                }

                            }
                            if (ShiftWork)
                            {
                                if (ZReportCount == 0)
                                {
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    TerminalStatusChanged = true;
                                }
                            }

                            if (TerminalStatusChanged || CardReaderStatusChanged || PaimentsStatusChanged || ExternalDoorOpenChanged || InternalDoorOpenChanged || AddingSummChanged || PrinterWorkChanged ||
                                CardReaderWorkChanged || CashAcceptorWorkChanged || CoinAcceptorWorkChanged || CashDispenserWorkChanged || HopperWorkChanged || BankModuleWorkChanged || CardDispenserWorkChanged || KKMWorkChanged || ShiftStatusChanged)
                            {
                                DisplayStatusChanged = true;


                                DoSwitchTerminalWork();

                                TerminalStatusChanged = false;
                                CardReaderStatusChanged = false;
                                PaimentsStatusChanged = false;
                                ExternalDoorOpenChanged = false;
                                InternalDoorOpenChanged = false;
                                CashAcceptorWorkChanged = false;
                                CoinAcceptorWorkChanged = false;
                                CashDispenserWorkChanged = false;
                                HopperWorkChanged = false;
                                BankModuleWorkChanged = false;
                                CardDispenserWorkChanged = false;
                                AddingSummChanged = false;
                                KKMWorkChanged = false;
                                ShiftStatusChanged = false;

                                CasseStatusLabel = Properties.Resources.CashierDontStarted;
                                if (EmulatorEnabled && (EmulatorForm.Emulator.TerminalWork == TerminalWorkT.TerminalClosed))
                                {
                                    if (EmulatorForm.Emulator.CasseStatusLabel1.Text != Properties.Resources.CashierDontStarted)
                                        EmulatorForm.Emulator.CasseStatusLabel1.Text = Properties.Resources.CashierDontStarted;

                                    if (EmulatorForm.Emulator.CardDispenserEditsGroupBox.Visible)
                                        EmulatorForm.Emulator.CardDispenserEditsGroupBox.Hide();
                                }
                                //}

                                if (TerminalWork == TerminalWorkT.TerminalClosed)
                                {
                                    if (this.Visibility == Visibility.Visible)
                                        this.Visibility = Visibility.Hidden;
                                    if (TechForm.Visibility == Visibility.Visible)
                                        TechForm.Hide();
                                }
                                else
                                {
                                    if (
                                        (ExternalDoorOpen ||
                                         (!EmulatorEnabled && (deviceCMModel.SlaveExist == null || SlaveController == null || !SlaveControllerWork))
                                        ) &&
                                        ((CashierType != 2) || ToggleTechForm)
                                       )
                                    {
                                        if (TechForm.Visibility != Visibility.Visible)
                                        {
                                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: ChangeLanguage(DefaultLanguage)");
                                            ChangeLanguage(DefaultLanguage);
                                            if (DefaultCulture != null)
                                            {
                                                Thread.CurrentThread.CurrentCulture = DefaultCulture;
                                            }
                                            lock (ConsoleLog)
                                            {
                                                string text = "";
                                                foreach (string s in ConsoleLog)
                                                    text += s + Environment.NewLine;
                                                TechForm.ConsoleLog.Text = text;
                                                ConsoleLogChanged = false;
                                            }
                                            TechForm.Visibility = Visibility.Visible;
                                            TechForm.MaxSummDeliveryLabel.Text = sValuteLabel;
                                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: CancellWork");
                                            CancellWork();
                                            ToLog(2, "Cashier: ExternalDeviceTimer_Tick: GetSettings");
                                            GetSettings(false);
                                        }
                                        if (this.Visibility == Visibility.Visible)
                                            this.Visibility = Visibility.Hidden;
                                        if (PaymentErrorInfo.Error || (PaymentErrorLevel != AlarmLevelT.Green))
                                        {
                                            string s;
                                            if (PaymentErrorLevel != AlarmLevelT.Green)
                                            {
                                                AlarmT Alarm = new AlarmT();
                                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                                Alarm.AlarmSource = AlarmSourceT.PaymentError;
                                                Alarm.ErrorCode = 0;
                                                SendAlarm(Alarm);
                                                PaymentErrorLevel = AlarmLevelT.Green;
                                            }
                                            if (EmulatorEnabled &&
                                                ((deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists && !EmulatorForm.Emulator.KKMVirt.Checked) ||
                                                (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists && !EmulatorForm.Emulator.PrinterVirt.Checked)))
                                            {
                                                if (((KKM != null) && !KKM.HardStatus.PaperEnd && !KKM.HardStatus.CoverOpen && !KKM.HardStatus.Error)
                                                    ||
                                                    ((Printer != null) && !Printer.HardStatus.PaperEnd && !Printer.HardStatus.CoverOpen && !Printer.HardStatus.Error))
                                                {
                                                    if (PaymentErrorInfo.ChequeError)
                                                    {
                                                        Thread.Sleep(2000);
                                                        s = string.Format(ResourceManager.GetString("ToLogEjectingChequeDelivery", DefaultCulture), ((AllPayParams)Params).Paid.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), ((AllPayParams)Params).ResDiff.ToString("F0"));
                                                        ToLog(2, "Cashier: ExternalDeviceTimer: ExternalDoorOpen: PaymentError PrintChequeError GiveLastCheque");
                                                        if (GiveCheque())
                                                        {
                                                            ToLog(1, s);
                                                            PaymentErrorInfo.ChequeError = false;
                                                        }
                                                        //ToLog(2, "Cashier: ExternalDeviceTimer: ExternalDoorOpen: PaymentError PrintChequeError GiveLastCheque End");
                                                    }

                                                    if (PaymentErrorInfo.DeliveryError)
                                                    {
                                                        Paid = PaymentErrorInfo.Paid;
                                                        CardQuery = PaymentErrorInfo.CardQuery;
                                                        DownDiff = PaymentErrorInfo.DownDiff;
                                                        ResDiff = PaymentErrorInfo.ResDiff;

                                                        MoveForwardResult = null;
                                                        string ss = string.Format(ResourceManager.GetString("ToLogDeliveryError1", DefaultCulture), DownDiff.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), ResDiff.ToString("F0"));
                                                        ToLog(2, "Cashier: ExternalDeviceTimer: ExternalDoorOpen: PaymentError DeliveryError Give Operator Cheque");
                                                        GiveCheque(1, Paid, CardQuery, Paiments, ResDiff, new List<string> { ss }, BankSlip);
                                                        PaymentErrorInfo.DeliveryError = false;
                                                    }
                                                    if (PaymentErrorInfo.CardWriteError)
                                                    {
                                                        //CardDispenser.SendCardToBezelAndHold();
                                                        //s = string.Format(ResourceManager.GetString("ToLogEjectingChequeDelivery", DefaultCulture), Paid.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), DownDiff.ToString("F0"));
                                                        //GiveCheque();
                                                        //ToLog(s);
                                                        if (deliveryStatus == DeliveryStatusT.Delivery)
                                                        {
                                                            string ss = string.Format(ResourceManager.GetString("ToLogDeliveryError1", DefaultCulture), DownDiff.ToString("F0"), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country), ResDiff.ToString("F0"));
                                                            ToLog(2, "Cashier: ExternalDeviceTimer: ExternalDoorOpen: PaymentError CardWriteError Give Operator Cheque");
                                                            GiveCheque(1, Paid, CardQuery, Paiments, ResDiff, new List<string> { ss }, BankSlip);
                                                        }
                                                        PaymentErrorInfo.CardWriteError = false;
                                                    }

                                                    if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                                                    {
                                                        //if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                                                        {
                                                            ToLog(2, "Cashier: ExternalDeviceTimer: ExternalDoorOpen: PaymentError CardDispenser.SendCardToBezelAndHold");
                                                            CardDispenser.SendCardToBezelAndHold();
                                                            //CardDispensed = true;
                                                            if (CardTimeOutToBasket > 0)
                                                                CardEjectTime = DateTime.Now;
                                                        }
                                                    }
                                                    if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                                                    {
                                                        if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                                                        !CardDispenser.CardDispenserStatus.CardAccepting &&
                                                        !CardDispenser.CardDispenserStatus.CardDispensing)
                                                        {
                                                            if (terminalStatus != TerminalStatusT.Empty)
                                                                terminalStatus = TerminalStatusT.Empty;
                                                        }
                                                        else
                                                        {
                                                            ToLog(1, ResourceManager.GetString("ToLogCardEject", DefaultCulture));

                                                            terminalStatus = TerminalStatusT.CardEjected;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (terminalStatus != TerminalStatusT.CardFree)
                                                            terminalStatus = TerminalStatusT.CardFree;
                                                    }
                                                    if (terminalStatusOld != terminalStatus)
                                                        ToLog(2, "Cashier: ExternalDeviceTimer_Tick6: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                    terminalStatusOld = terminalStatus;

                                                    PaymentErrorInfo.Error = false;
                                                }
                                            }
                                            else
                                            {
                                                //CleanPaymentErrorInfo();
                                                if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                                                {
                                                    //if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                                                    {
                                                        ToLog(2, "Cashier: ExternalDeviceTimer: CardDispenser.SendCardToBezelAndHold");
                                                        CardDispenser.SendCardToBezelAndHold();
                                                        //CardDispensed = true;
                                                        if (CardTimeOutToBasket > 0)
                                                            CardEjectTime = DateTime.Now;
                                                    }
                                                }
                                                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                                                {
                                                    if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                                                        !CardDispenser.CardDispenserStatus.CardAccepting &&
                                                        !CardDispenser.CardDispenserStatus.CardDispensing)
                                                    {
                                                        if (terminalStatus != TerminalStatusT.Empty)
                                                            terminalStatus = TerminalStatusT.Empty;
                                                    }
                                                    else
                                                    {
                                                        ToLog(1, ResourceManager.GetString("ToLogCardEject", DefaultCulture));
                                                        terminalStatus = TerminalStatusT.CardEjected;
                                                    }
                                                }
                                                else
                                                {
                                                    if (terminalStatus != TerminalStatusT.CardFree)
                                                        terminalStatus = TerminalStatusT.CardFree;
                                                }
                                                if (terminalStatusOld != terminalStatus)
                                                    ToLog(2, "Cashier: ExternalDeviceTimer_Tick8: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                terminalStatusOld = terminalStatus;

                                            }

                                            if (EmulatorEnabled)
                                            {
                                                EmulatorForm.Emulator.CardGotStuck.Enabled = false;
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                                EmulatorForm.Emulator.Paiments = PaimentMethodT.PaimentsAll;
                                                if (EmulatorForm.Emulator.TakeCard.Enabled)
                                                    EmulatorForm.Emulator.TakeCard.Enabled = false;
                                                if (!EmulatorForm.Emulator.InsertCard.Enabled)
                                                    EmulatorForm.Emulator.InsertCard.Enabled = true;
                                                if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                                                {
                                                    if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                                        EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                                                }
                                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                                                if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                                                    EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                                                if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                                                    EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                                                if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                                                    EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CashierType != 2)
                                        {
                                            GenerateLabels();

                                            if (this.Visibility == Visibility.Hidden)
                                                this.Visibility = Visibility.Visible;
                                            if (!EmulatorEnabled && CashierType != 2)
                                                this.Topmost = true;
                                        }
                                        if (TechForm.Visibility == Visibility.Visible)
                                            TechForm.Visibility = Visibility.Hidden;
                                    }
                                }
                            }
                        }
                        if (TerminalWork == TerminalWorkT.TerminalWork)
                        {
                            DoSwitchTerminalStatus();
                        }
                        if (Talk != null)
                        {
                            if (Talk.Answered)
                            {

                                if (OperatorTalkB.ButtonGreen.Visibility != Visibility.Visible)
                                {
                                    OperatorTalkB.ButtonGray.Visibility = Visibility.Hidden;
                                    OperatorTalkB.ButtonGreen.Visibility = Visibility.Visible;
                                }
                            }
                            else
                            {
                                if (Talk.Run)
                                {
                                    if (DateTime.Now > CallStart + TimeSpan.FromSeconds(60))
                                    {
                                        ToLog(2, "Cashier: ExternalDeviceTimer: Talk.HangUp");
                                        Talk.HangUp();
                                    }
                                    else
                                    {
                                        if (DateTime.Now > CallUpdate + TimeSpan.FromSeconds(1))
                                        {
                                            if (OperatorTalkB.ButtonGray.Visibility == Visibility.Visible)
                                            {
                                                OperatorTalkB.ButtonGray.Visibility = Visibility.Hidden;
                                                OperatorTalkB.ButtonGreen.Visibility = Visibility.Visible;
                                            }
                                            else
                                            {
                                                OperatorTalkB.ButtonGray.Visibility = Visibility.Visible;
                                                OperatorTalkB.ButtonGreen.Visibility = Visibility.Hidden;
                                            }
                                            CallUpdate = DateTime.Now;
                                        }
                                    }
                                }
                                else
                                {
                                    if (OperatorTalkB.ButtonGray.Visibility != Visibility.Visible)
                                    {
                                        OperatorTalkB.ButtonGreen.Visibility = Visibility.Hidden;
                                        OperatorTalkB.ButtonGray.Visibility = Visibility.Visible;
                                    }
                                }
                            }
                        }
                    }
                    lock (ErrorsTextLast)
                    {
                        if (ErrorsTextLast != ErrorsText)
                        {
                            ErrorsTextLast = ErrorsText;
                            ErrorsTextChanged = true;
                        }
                    }

                    ExternalDeviceTimer.Enabled = true;
                }
            }
            /*catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }*/
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        public void SetEmulator()
        {
            if (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                {
                    if (!EmulatorForm.Emulator.KKMGroupBox.Visible)
                        EmulatorForm.Emulator.KKMGroupBox.Show();
                }
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                {
                    if (EmulatorForm.Emulator.KKMGroupBox.Visible)
                        EmulatorForm.Emulator.KKMGroupBox.Hide();
                }
            }
            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings Printer");
            if (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.PrinterGroupBox.Visible)
                        EmulatorForm.Emulator.PrinterGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.PrinterGroupBox.Visible)
                        EmulatorForm.Emulator.PrinterGroupBox.Hide();
            }
            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings Hopper");
            if (deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                {
                    if (!EmulatorForm.Emulator.HopperGroupBox1.Visible)
                        EmulatorForm.Emulator.HopperGroupBox1.Show();
                    if (!EmulatorForm.Emulator.HopperGroupBox2.Visible)
                        EmulatorForm.Emulator.HopperGroupBox2.Show();
                }
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                {
                    if (EmulatorForm.Emulator.HopperGroupBox1.Visible)
                        EmulatorForm.Emulator.HopperGroupBox1.Hide();
                    if (EmulatorForm.Emulator.HopperGroupBox2.Visible)
                        EmulatorForm.Emulator.HopperGroupBox2.Hide();
                }
            }
            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings CoinAcceptor");
            if (deviceCMModel.CoinAcceptorExist != null && (bool)deviceCMModel.CoinAcceptorExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.CoinAcceptorGroupBox.Visible)
                        EmulatorForm.Emulator.CoinAcceptorGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.CoinAcceptorGroupBox.Visible)
                        EmulatorForm.Emulator.CoinAcceptorGroupBox.Hide();
            }
            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings CashAcceptor");
            if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.CashAcceptorGroupBox.Visible)
                        EmulatorForm.Emulator.CashAcceptorGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.CashAcceptorGroupBox.Visible)
                        EmulatorForm.Emulator.CashAcceptorGroupBox.Hide();
            }

            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings CashDispenser");
            if (deviceCMModel.CashDispenserExist != null && (bool)deviceCMModel.CashDispenserExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.CashDispenserGroupBox.Visible)
                        EmulatorForm.Emulator.CashDispenserGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.CashDispenserGroupBox.Visible)
                        EmulatorForm.Emulator.CashDispenserGroupBox.Hide();
            }

            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings CardDispenser");
            if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                        EmulatorForm.Emulator.CardDispenserGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                        EmulatorForm.Emulator.CardDispenserGroupBox.Hide();
            }

            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings CardReader");
            if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.CardReaderGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.CardReaderGroupBox.Visible)
                        EmulatorForm.Emulator.CardReaderGroupBox.Hide();
            }

            //ToLog(2, "Cashier: ExternalDeviceTimer_Tick: EmulatorSettings BankModule");
            if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist)
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (!EmulatorForm.Emulator.BankModuleGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleGroupBox.Show();
            }
            else
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.BankModuleGroupBox.Visible)
                        EmulatorForm.Emulator.BankModuleGroupBox.Hide();
            }
        }
        public void ToLog(int Level, string What)
        {
            string text;
            text = DateTime.Now.ToString(ldt_format) + ": " + What;
            if (Level < 2)
            {
                lock (ConsoleLog)
                {
                    ConsoleLog.Enqueue(text);
                    ConsoleLogChanged = true;
                }
                if (TechForm != null && TechForm.Visibility != Visibility.Visible)
                {
                    while (ConsoleLog.Count > 500)
                        ConsoleLog.Dequeue();
                }
            }

            if (log != null)
                log.ToLog(Level, text);
        }
        private void DoLogEmulator()
        {
            try
            {
                if (EmulatorEnabled && (EmulatorForm != null) && (EmulatorForm.Emulator != null))
                    if (EmulatorForm.Emulator.sLog.Count > 0)
                    {
                        for (int i = 0; i < EmulatorForm.Emulator.sLog.Count; i++)
                            ToLog(1, EmulatorForm.Emulator.sLog[i]);
                        EmulatorForm.Emulator.sLog.Clear();
                    }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void CleanTechPanels()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (TechForm.CashAcceptorSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.CashAcceptorGetSettings");
                //TechForm.CashAcceptorGetSettings();
                TechForm.CashAcceptorSettings.Visibility = Visibility.Hidden;
            }
            if (CashierType == 0)
            {
                if (TechForm.CoinAcceptorSettings.Visibility == Visibility.Visible)
                {
                    ToLog(2, "Cashier: CleanTechPanels: TechForm.CoinAcceptorGetSettings");
                    //TechForm.CoinAcceptorGetSettings();
                    TechForm.CoinAcceptorSettings.Visibility = Visibility.Hidden;
                }
            }
            if (TechForm.CashDispenserSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.CashDispenserGetSettings");
                //TechForm.CashDispenserGetSettings();
                TechForm.CashDispenserSettings.Visibility = Visibility.Hidden;
            }
            if (CashierType == 0)
            {
                if (TechForm.CoinHopperSettings.Visibility == Visibility.Visible)
                {
                    ToLog(2, "Cashier: CleanTechPanels: TechForm.CoinHopperGetSettings");
                    //TechForm.CoinHopperGetSettings();
                    TechForm.CoinHopperSettings.Visibility = Visibility.Hidden;
                }
            }
            if (TechForm.OtherSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.OtherSettingsGetSettings");
                //TechForm.OtherSettingsGetSettings();
                TechForm.OtherSettings.Visibility = Visibility.Hidden;
            }
            if (TechForm.BankModuleSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.BankModuleGetSettings");
                //TechForm.BankModuleGetSettings();
                TechForm.BankModuleSettings.Visibility = Visibility.Hidden;
            }
            if (TechForm.KKMSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.KKMGetSettings");
                //TechForm.KKMGetSettings();
                TechForm.KKMSettings.Visibility = Visibility.Hidden;
            }
            if (TechForm.OtherDevicesSettings.Visibility == Visibility.Visible)
            {
                ToLog(2, "Cashier: CleanTechPanels: TechForm.OtherDevicesGetSettings");
                //TechForm.OtherDevicesGetSettings();
                TechForm.OtherDevicesSettings.Visibility = Visibility.Hidden;
            }
            if (TechForm.SettingsPanel.Visibility == Visibility.Visible)
            {
                TechForm.SettingsPanel.Visibility = Visibility.Hidden;
            }
            if (TechForm.DeliveryPanel.Visibility == Visibility.Visible)
            {
                TechForm.DeliveryPanel.Visibility = Visibility.Hidden;
            }
            if (TechForm.SettingsButtonPanel.Visibility == Visibility.Visible)
            {
                TechForm.SettingsButtonPanel.Visibility = Visibility.Hidden;
            }
            if (TechForm.ShiftPanel.Visibility == Visibility.Visible)
            {
                TechForm.ShiftPanel.Visibility = Visibility.Hidden;
            }
            if (TechForm.TestingPanel.Visibility == Visibility.Visible)
            {
                TechForm.TestingPanel.Visibility = Visibility.Hidden;
            }
            if (TechForm.TechButtonsPanel.Visibility != Visibility.Visible)
            {
                TechForm.TechButtonsPanel.Visibility = Visibility.Visible;
                TechForm.MasterCardRead.Visibility = Visibility.Hidden;
            }
            TechForm.Visibility = Visibility.Hidden;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        ///Переопределим выход из программы 
        ///Занесем значения элементв с формы в объект 
        ///ties.Settings и сохраним настройки
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Start");
            try
            {
                ExternalDeviceTimer.Enabled = false;
                WindowClosing = true;
                if (Talk != null)
                {
                    ToLog(2, "Cashier: Window_Closing: Talk.Stop");
                    Talk.Stop();
                }

                if (CardDispenser != null && CardDispenser.CardDispenserStatus.CardPreSendPos)
                {
                    CardDispenser.SendCardToBezelAndHold();
                }

                SqlBaseImport.Model.Utils.Work = false;
                ToLog(2, "Cashier: Window_Closing: webServer.Stop");
                if (webServer != null)
                    webServer.Stop();
                try
                {
                    if (deviceCMModel != null)
                    {
                        if (ShiftWork)
                        {
                            deviceCMModel.ShiftOpened = ShiftOpened;
                            if (ShiftOpened)
                            {
                                deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                                deviceCMModel.LastShiftOpenTime = LastShiftOpenTime;
                            }
                            else
                            {
                                deviceCMModel.CurrentTransactionNumber = 0;
                                deviceCMModel.LastShiftOpenTime = null;
                            }
                        }
                        else
                            deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                        deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                        deviceCMModel.CoinAcceptorCurrent = CoinAcceptorCurrent;

                        deviceCMModel.CountUp = DeliveryUpCount;
                        deviceCMModel.CountDown = DeliveryDownCount;
                        deviceCMModel.Count1 = Delivery1Count;
                        deviceCMModel.Count2 = Delivery2Count;
                        deviceCMModel.RejectCount = RejectCount;

                        Properties.Settings.Default.Save();
                        if (EmulatorEnabled)
                            Emulator.Properties.Settings.Default.Save();
                        if (EmulatorForm != null)
                            EmulatorForm.Close();
                        List<object> lst = new List<object>();
                        if (deviceCMModel.DeviceId != null)
                        {
                            lst.Add("DeviceId");
                            lst.Add(deviceCMModel.DeviceId);
                        }
                        if (deviceCMModel.ShiftOpened != null)
                        {
                            lst.Add("ShiftOpened");
                            lst.Add(deviceCMModel.ShiftOpened);
                        }
                        if (deviceCMModel.CurrentTransactionNumber != null)
                        {
                            lst.Add("CurrentTransactionNumber");
                            lst.Add(deviceCMModel.CurrentTransactionNumber);
                        }
                        if (deviceCMModel.LastShiftOpenTime != null)
                        {
                            lst.Add("LastShiftOpenTime");
                            lst.Add(deviceCMModel.LastShiftOpenTime);
                        }
                        lst.Add("CashAcceptorCurrent");
                        lst.Add(deviceCMModel.CashAcceptorCurrent);
                        lst.Add("CoinAcceptorCurrent");
                        lst.Add(deviceCMModel.CoinAcceptorCurrent);
                        if (deviceCMModel.CountUp != null)
                        {
                            lst.Add("CountUp");
                            lst.Add(deviceCMModel.CountUp);
                        }
                        if (deviceCMModel.CountDown != null)
                        {
                            lst.Add("CountDown");
                            lst.Add(deviceCMModel.CountDown);
                        }
                        if (deviceCMModel.Count1 != null)
                        {
                            lst.Add("Count1");
                            lst.Add(deviceCMModel.Count1);
                        }
                        if (deviceCMModel.Count2 != null)
                        {
                            lst.Add("Count2");
                            lst.Add(deviceCMModel.Count2);
                        }
                        if (deviceCMModel.RejectCount != null)
                        {
                            lst.Add("RejectCount");
                            lst.Add(deviceCMModel.RejectCount);
                        }
                        if (deviceCMModel.ShiftNumber != null)
                        {
                            lst.Add("ShiftNumber");
                            lst.Add(deviceCMModel.ShiftNumber);
                        }
                        lock (db_lock)
                        {
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + DeviceId.ToString() + "'", null, db);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", lst.ToArray(), "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    ToLog(0, exc.ToString());
                }
                if ((CardDispenser != null) && CardDispenser.IsOpen)
                    CardDispenser.Close();
                if ((CardReader != null) && CardReader.IsOpen)
                {
                    CardReader.ClosePool();
                    CardReader.Close();
                }
                if ((CashAcceptor != null) && CashAcceptor.IsOpen)
                {
                    CashAcceptor.InhibitOn();
                    CashAcceptor.Close();
                }
                if ((CashDispenser != null) && CashDispenser.IsOpen)
                    CashDispenser.Close();
                if ((KKM != null) /*&& KKM.Initialised*/ && KKM.DeviceEnabled)
                    KKM.FR_Disconnect();
                if ((SlaveController != null) && SlaveController.IsOpen)
                    SlaveController.CloseThread();
                if (EmulatorEnabled && EmulatorForm != null)
                    EmulatorForm.Close();
                if (TechForm != null && !TechForm.WindowClosing)
                    TechForm.Close();
                currRegistryKey.Close();
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                Thread.Sleep(1000);
            }
            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            if (log != null)
                log.Close();
            base.OnClosed(e);
        }
        public void TransactionClear()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (Transaction.BanknotesAccepted != null)
                    for (int i = 0; i < Transaction.BanknotesAccepted.GetLength(0); i++)
                    {
                        Transaction.BanknotesAccepted[i].Count = 0;
                    }
                if (Transaction.CoinsAccepted != null)
                    for (int i = 0; i < Transaction.CoinsAccepted.GetLength(0); i++)
                    {
                        Transaction.CoinsAccepted[i].Count = 0;
                    }
                Transaction.TransactionType = TransactionTypeT.Undefined;
                Transaction.WayOfPayment = WayOfPaymentT.Undefined;
                Transaction.PurchaseSum = 0;
                Transaction.ConclusionCode = ConclusionCodeT.Undefined;
                Transaction.PaymentTime = null;
                Transaction.ExitBefore = null;
                Transaction.PrepaymentSum = 0;
                Transaction.ToPay = 0;
                Transaction.Paid = 0;
                Transaction.Issued = 0;
                Transaction.SumStayedInCM = 0;
                Transaction.BillIsued = false;
                Transaction.ErrorCodeTransactiontop = 0;
                Transaction.IssuedNotesCount = 0;
                Transaction.IssuedFromUpper = 0;
                Transaction.IssuedFromLower = 0;
                Transaction.IssuedFrom1Hopper = 0;
                Transaction.IssuedFrom2Hopper = 0;
                Transaction.Rejected = 0;
                Transaction.DeliveredNotesCount = 0;
                Transaction.DeliveredFromUpper = 0;
                Transaction.DeliveredFromLower = 0;
                Transaction.DeliveredCoinsCount = 0;
                Transaction.DeliveredFrom1Hopper = 0;
                Transaction.DeliveredFrom2Hopper = 0;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        public bool GiveCheque()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                if (!(bool)ChequePrinted)
                {
                    ChequeDT = DateTime.Now;
                    if (
                    (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                   )
                    {

                        if (deviceCMModel.ChequeWork == 0)
                            KKM.SetPrezenter(0, 0, 0);
                        else if (deviceCMModel.ChequeWork == 2)
                            KKM.SetPrezenter(1, 0, 0);
                        ToLog(2, "Cashier: GiveCheque(): KKM.PrintCheck");
                        KKM.PrintCheck((AllPayParams)Params);
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;

                        if ((KKMResultCode != CommonKKM.ErrorCode.Sucsess) && (KKMResultCode != CommonKKM.ErrorCode.NearPaperEnd))
                        {
                            ChequePrinted = false;
                        }
                        else
                        {
                            Params = null;
                            ChequePrinted = true;
                            PaymentErrorInfo.ChequeError = false;
                            PaymentErrorInfo.Error = false;
                        }
                    }
                    else if ((deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked))
                    {
                        ToLog(2, "Cashier: GiveCheque(): Printer.PrintCheck");
                        Printer.PrintCheck((AllPayParams)Params);
                        if (Printer.ResultCode != CommonPrinter.ErrorCode.Sucsess)
                        {
                            ChequePrinted = false;
                        }
                        else
                        {
                            Params = null;
                            ChequePrinted = true;
                            PaymentErrorInfo.ChequeError = false;
                            PaymentErrorInfo.Error = false;
                        }
                    }
                    return ChequePrinted;
                }
                else if (!(bool)Cheque2Printed && (BankSlip.Count > 0))
                {
                    ChequeDT = DateTime.Now;
                    if (
                    (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                   )
                    {
                        ToLog(2, "Cashier: GiveCheque(): KKM.BankSlip");
                        KKM.PrintBankSlip(BankSlip);
                        /*while (KKM.CommandStatus != CommandStatusT.Completed)
                            Thread.Sleep(100);*/
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if ((KKMResultCode == CommonKKM.ErrorCode.Sucsess) || (KKMResultCode == CommonKKM.ErrorCode.NearPaperEnd))
                        {
                            Cheque2Printed = true;
                            BankSlip.Clear();
                        }
                        else
                            Cheque2Printed = false;
                    }
                    else
                    if (
                        (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked)
                       )
                    {
                        ToLog(2, "Cashier: GiveCheque(): Printer.BankSlip");
                        Printer.PrintNotCheck(Items);
                        if (Printer.ResultCode == CommonPrinter.ErrorCode.Sucsess)
                        {
                            Cheque2Printed = true;
                            Items.Clear();
                        }
                        else
                            Cheque2Printed = false;
                    }

                    return Cheque2Printed;
                }
                else if (!(bool)Cheque1Printed && (Items.Count > 0))
                {
                    ChequeDT = DateTime.Now;
                    if (
                    (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                   )
                    {
                        //KKM.SetPrezenter(0, 1, 0);
                        ToLog(2, "Cashier: GiveCheque(): KKM.PrintNotCheck");
                        KKM.PrintNotCheck(Items);
                        /*while (KKM.CommandStatus != CommandStatusT.Completed)
                            Thread.Sleep(100);*/
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if ((KKMResultCode == CommonKKM.ErrorCode.Sucsess) || (KKMResultCode == CommonKKM.ErrorCode.NearPaperEnd))
                        {
                            Cheque1Printed = true;
                            Items.Clear();
                        }
                        else
                            Cheque1Printed = false;
                    }
                    else
                    if (
                        (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                        (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked)
                       )
                    {
                        ToLog(2, "Cashier: GiveCheque(): Printer.PrintNotCheck");
                        Printer.PrintNotCheck(Items);
                        if (Printer.ResultCode == CommonPrinter.ErrorCode.Sucsess)
                        {
                            Cheque1Printed = true;
                            Items.Clear();
                        }
                        else
                            Cheque1Printed = false;
                    }

                    if (DetailLog)
                        ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                    return Cheque1Printed;
                }
                return true;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return false;
            }
        }
        void PrintCheck(object data)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            AllPayParams P = (AllPayParams)data;
            if (
                (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
               )
            {
                ToLog(2, "Cashier: PrintCheck: KKM.PrintCheck");
                KKM.PrintCheck(P);
            }
            else if ((deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                                    (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked))
            {
                ToLog(2, "Cashier: PrintCheck: Printer.PrintCheck");
                Printer.PrintCheck(P);
            }

            GiveChequeStatus = false;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        public bool GiveCheque(int Type, decimal Paid, decimal CardQuery, PaimentMethodT Paiments, decimal NotDelivered = 0, List<string> AddStrings = null, List<string> BankSlip = null)
        {
            ChequeDT = DateTime.Now;
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            AllPayParams P = new AllPayParams();
            P.AddStrings = new List<string>();
            P.BankSlip = new List<string>();
            List<string> I = new List<string>();
            try
            {
                if (Type == 0) // Фискальный чек
                {
                    if ((DateExitEstimated == null) || (DateExitEstimated < (DateTime.Now + TimeSpan.FromMinutes(15))))
                        DateExitEstimated = (DateTime.Now + TimeSpan.FromMinutes(15));

                    P.Paid = Paid;
                    P.CardQuery = CardQuery;
                    P.ResDiff = NotDelivered;
                    P.DocType = DocType.Sale;
                    P.EntryTime = (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime));
                    P.ExitTime = DateExitEstimated;
                    P.CardId = ReaderIDCardDec;
                    P.What = ResourceManager.GetString("ChecqueName", DefaultCulture);
                    P.Pay_Summa = Paid;
                    P.Pay_Query = CardQuery;
                    P.Pay_SummaSdachi = Paid - CardQuery;
                    if (deviceCMModel.VAT != null)
                        P.VAT = (int)deviceCMModel.VAT;
                    else
                        P.VAT = 0;
                    if ((AddStrings != null) && (AddStrings.Count > 0))
                    {
                        P.AddStrings = AddStrings;
                    }
                    P.AddStrings.Add(ResourceManager.GetString("Tariff", DefaultCulture) + TarifiTPName);
                    if ((BankSlip != null) && (BankSlip.Count > 0))
                    {
                        P.BankSlip = BankSlip;
                    }
                    if (Paiments == PaimentMethodT.PaimentsBankModule)
                        P.TypeClose = TypeClose.Card;
                    else
                        P.TypeClose = TypeClose.Cash;
                    if ((deliveryStatus == DeliveryStatusT.NotDelivery) || (Paiments == PaimentMethodT.PaimentsBankModule))
                        P.Delivery = false;
                    else
                        P.Delivery = true;
                }
                else if (Type != 3)
                {
                    I.Add(ResourceManager.GetString("ChecqueCommitment", DefaultCulture));
                    I.Add(ResourceManager.GetString("ChecqueName", DefaultCulture) + Environment.NewLine);
                    I.Add(ResourceManager.GetString("ToLogIDCard", DefaultCulture) + " " + ReaderIDCardDec.ToString());
                    I.Add(ResourceManager.GetString("ToLogEntranceTime", DefaultCulture) + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString("HH:mm dd-MM-yyyy"));
                    I.Add(ResourceManager.GetString("ChecqueIncome", DefaultCulture) + Paid.ToString("F0"));
                    I.Add(ResourceManager.GetString("ChecqueDeliveryNeed", DefaultCulture) + DownDiff.ToString("F0"));
                    I.Add(ResourceManager.GetString("ChecqueDeliveryDiff", DefaultCulture) + ResDiff.ToString("F0"));
                }
                if (
                    (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                   )
                {
                    if (Type == 0) // Фискальный чек
                    {
                        if (deviceCMModel.ChequeWork == 0)
                            KKM.SetPrezenter(0, 0, 0);
                        else if (deviceCMModel.ChequeWork == 2)
                            KKM.SetPrezenter(1, 0, 0);
                        GiveChequeStatus = true;
                        GiveChequeThread = new Thread(PrintCheck);
                        GiveChequeThread.Name = "GiveChequeThread";
                        GiveChequeThread.IsBackground = true;
                        ToLog(2, "Cashier: GiveCheque(int Type): GiveChequeThread.Start");
                        GiveChequeThread.Start(P);
                    }
                    else if (Type == 3) // Банковский слип
                    {
                        ToLog(2, "Cashier: GiveCheque(int Type): KKM.BankSlip");
                        KKM.PrintBankSlip(BankSlip);
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if ((KKMResultCode == CommonKKM.ErrorCode.Sucsess) || (KKMResultCode == CommonKKM.ErrorCode.NearPaperEnd))
                        {
                            Cheque2Printed = true;
                            BankSlip.Clear();
                            ChequeDT = DateTime.Now;
                        }
                        else
                        {
                            Cheque2Printed = false;
                        }

                        if (DetailLog)
                            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                        return Cheque2Printed;
                    }
                    else
                    { // Чек-подтверждение (не фискальный)
                        //KKM.SetPrezenter(0, 1, 0);
                        ToLog(2, "Cashier: GiveCheque(int Type): KKM.PrintNotCheck");
                        KKM.PrintNotCheck(I);
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if ((KKMResultCode == CommonKKM.ErrorCode.Sucsess) || (KKMResultCode == CommonKKM.ErrorCode.NearPaperEnd))
                        {
                            Cheque1Printed = true;
                            Items.Clear();
                            ChequeDT = DateTime.Now;
                        }
                        else
                        {
                            Items = I;
                            Cheque1Printed = false;
                        }

                        if (DetailLog)
                            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                        return Cheque1Printed;
                    }
                }
                else if ((deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                                    (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked))
                {
                    if (Type == 0) // Фискальный чек
                    {
                        if ((DateExitEstimated != null) && (DateExitEstimated < (DateTime.Now + TimeSpan.FromMinutes(15))))
                            DateExitEstimated = (DateTime.Now + TimeSpan.FromMinutes(15));

                        GiveChequeStatus = true;
                        GiveChequeThread = new Thread(PrintCheck);
                        GiveChequeThread.Name = "GiveChequeThread";
                        GiveChequeThread.IsBackground = true;
                        ToLog(2, "Cashier: GiveCheque(int Type): GiveChequeThread.Start");
                        GiveChequeThread.Start(P);

                    }
                    else if (Type == 3) // Банковский слип
                    {
                        ToLog(2, "Cashier: GiveCheque(int Type): Printer.BankSlip");
                        Printer.PrintNotCheck(BankSlip);
                        PrinterResultCode = Printer.ResultCode;
                        PrinterResultDescription = Printer.ResultDescription;
                        if ((PrinterResultCode == CommonPrinter.ErrorCode.Sucsess))
                        {
                            Cheque2Printed = true;
                            BankSlip.Clear();
                            ChequeDT = DateTime.Now;
                        }
                        else
                        {
                            Cheque2Printed = false;
                        }

                        if (DetailLog)
                            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                        return Cheque2Printed;
                    }
                    else
                    { // Чек-подтверждение (не фискальный)
                        ToLog(2, "Cashier: GiveCheque(int Type): Printer.PrintNotCheck");
                        Printer.PrintNotCheck(I);
                        PrinterResultCode = Printer.ResultCode;
                        PrinterResultDescription = Printer.ResultDescription;
                        if ((PrinterResultCode == CommonPrinter.ErrorCode.Sucsess))
                        {
                            Cheque1Printed = true;
                            Items.Clear();
                            ChequeDT = DateTime.Now;
                        }
                        else
                        {
                            Items = I;
                            Cheque1Printed = false;
                        }
                        if (DetailLog)
                            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                        return Cheque1Printed;
                    }
                }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                return true;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return false;
            }
        }
        private void CleanPaymentErrorInfo()
        {
            try
            {
                PaymentErrorInfo.Error = false;
                PaymentErrorInfo.CardWriteError = false;
                PaymentErrorInfo.ChequeError = false;
                PaymentErrorInfo.DeliveryError = false;
                PaymentErrorInfo.CardDispenserError = false;
                PaymentErrorInfo.CashAcceptorError = false;
                PaymentErrorInfo.Delivery = false;
                PaymentErrorInfo.CardQuery = 0;
                PaymentErrorInfo.DownDiff = 0;
                PaymentErrorInfo.Paid = 0;
                PaymentErrorInfo.ResDiff = 0;
                PaymentErrorInfo.SumOnCard = 0;

            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void CleanCardInfo()
        {
            try
            {
                CardInfo.ClientGroupidFC = 0;
                CardInfo.ClientTypidFC = 0;
                CardInfo.LastPaymentTime = 0;
                CardInfo.LastRecountTime = 0;
                CardInfo.Nulltime1 = 0;
                CardInfo.Nulltime2 = 0;
                CardInfo.Nulltime3 = 0;
                CardInfo.ParkingEnterTime = 0;
                CardInfo.SumOnCard = 0;
                CardInfo.TKVP = 0;
                CardInfo.TPidFC = 0;
                CardInfo.TSidFC = 0;
                CardInfo.TVP = 0;
                CardInfo.ZoneidFC = 0;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void GetSettings(bool LogWrite)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                // Чтение других настроек
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadOtherSettings", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.OtherSettingsGetSettings");
                TechForm.OtherSettingsGetSettings();

                // Чтение сохраненных параметров банкнотоприемника
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadCashAcceptorSettings", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.CashAcceptorGetSettings");
                TechForm.CashAcceptorGetSettings();

                // Чтение сохраненных параметров диспенсера банкнот
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadCashDispenserSettings", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.CashDispenserGetSettings");
                TechForm.CashDispenserGetSettings();

                if (CashierType == 0)
                {
                    // Чтение сохраненных параметров приемника монет
                    if (LogWrite)
                        ToLog(1, ResourceManager.GetString("ToLogReadCoinAcceptorSettings", DefaultCulture));
                    //ToLog(2, "Cashier: GetSettings: TechForm.CoinAcceptorGetSettings");
                    TechForm.CoinAcceptorGetSettings();
                }
                if (CashierType == 0)
                {
                    // Чтение сохраненных параметров хопера монет
                    if (LogWrite)
                        ToLog(1, ResourceManager.GetString("ToLogReadHopperSettings", DefaultCulture));
                    //ToLog(2, "Cashier: GetSettings: TechForm.CoinHopperGetSettings");
                    TechForm.CoinHopperGetSettings();
                }
                // Чтение сохраненных параметров банковского модуля
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadBankModuleSettings", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.BankModuleGetSettings");
                TechForm.BankModuleGetSettings();

                // Чтение сохраненных параметров KKM
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadKKMSettings", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.KKMGetSettings");
                TechForm.KKMGetSettings();


                // Чтение других устройств
                if (LogWrite)
                    ToLog(1, ResourceManager.GetString("ToLogReadOtherDevices", DefaultCulture));
                //ToLog(2, "Cashier: GetSettings: TechForm.OtherDevicesGetSettings");
                TechForm.OtherDevicesGetSettings();

                //if ((TechForm.ServerExcahgeProtocolSelectedIndex + 1) == 1)
                {
                    string strhas = "select Value from Setting where Name='ServerDomain'";
                    try
                    {
                        using (SqlCommand comm = new SqlCommand(strhas, db))
                        {
                            object res = comm.ExecuteScalar();
                            if (res != null && res != System.DBNull.Value)
                            {
                                try
                                {
                                    TechForm.ServerUrl.Text = Convert.ToString(res);
                                    deviceCMModel.ServerURL = Convert.ToString(res);
                                    lock (db_lock)
                                    {
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        try
                                        {
                                            UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "ServerURL", deviceCMModel.ServerURL }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "ServerURL", deviceCMModel.ServerURL }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                    catch { }
                }
                keyA = TechForm.CashierKey.Password;

                /*ChangeLanguage(DefaultLanguage);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(DefaultLanguage);*/

                /* Перенесено в чтение Других настроек
                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                {
                    // Чтение стоимости штрафной карты
                    if (deviceCMModel.PenaltyCardTPId != null)
                    {
                        PenalCardTP = (int)deviceCMModel.PenaltyCardTPId;
                    }
                    else
                    {
                        PenalCardTP = 1;
                    }
                    if (deviceCMModel.PenaltyCardTSId != null)
                    {
                        PenalCardTS = (int)deviceCMModel.PenaltyCardTSId;
                    }
                    else
                    {
                        PenalCardTS = 1;
                    }
                }*/
                try
                {
                    CashAcceptorCurrent = deviceCMModel.CashAcceptorCurrent;
                }
                catch
                {
                    CashAcceptorCurrent = 0;
                }
                try
                {
                    CoinAcceptorCurrent = deviceCMModel.CoinAcceptorCurrent;
                }
                catch
                {
                    CoinAcceptorCurrent = 0;
                }

                try
                {
                    DeliveryUpCount = (int)deviceCMModel.CountUp;
                }
                catch
                {
                    DeliveryUpCount = 0;
                }
                try
                {
                    DeliveryDownCount = (int)deviceCMModel.CountDown;
                }
                catch
                {
                    DeliveryDownCount = 0;
                }
                try
                {
                    Delivery1Count = (int)deviceCMModel.Count1;
                }
                catch
                {
                    Delivery1Count = 0;
                }
                try
                {
                    Delivery2Count = (int)deviceCMModel.Count2;
                }
                catch
                {
                    Delivery2Count = 0;
                }
                try
                {
                    RejectCount = (int)deviceCMModel.RejectCount;
                }
                catch
                {
                    RejectCount = 0;
                }
                SettingsLastUpdate = DateTime.Now;
                if (ShiftWork)
                {
                    if ((deviceCMModel.CashAcceptorExist != null) && (bool)deviceCMModel.CashAcceptorExist)
                        TechForm.CashAcceptorCleanButton.Visibility = Visibility.Visible;
                    if ((deviceCMModel.CoinAcceptorExist != null) && (bool)deviceCMModel.CoinAcceptorExist)
                        TechForm.CoinAcceptorCleanButton.Visibility = Visibility.Visible;
                }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leve");
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private string GetPeriodName(int Count, int Type)
        {
            try
            {
                string d = "Period" + Type.ToString() + Count.ToString();
                return rm.GetString(d);
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return null;
            }
        }

        private ToTariffPlan.DateCard CardToAbonement(ToTariffPlan.DateCard CardInfo, Guid? ToAbonement)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                TariffPlanTariffScheduleModelT TarifA = null;
                int ttt = (int)(DateTime.Now - datetime0).TotalSeconds;
                string req = "select top 1 *  from TariffPlanTariffScheduleModel where TariffScheduleId='" + ((Guid)TarifiTS).ToString() +
                    "' and  Id='" + ((Guid)ToAbonement).ToString() +
                    "' and TypeId = 3 and (_IsDeleted is null or _IsDeleted = 0) order by ConditionAbonementPrice";
                DataRow ct = null;
                lock (db_lock)
                {
                    try
                    {
                        ct = UtilsBase.FillRow(req, db, null, null);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            ct = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }
                if (ct != null)
                    TarifA = SetTariffPlanTariffScheduleModel(ct);

                if (TarifA != null && ((CardInfo.SumOnCard - AmountDue) >= TarifA.ConditionAbonementPrice)) // ЕСТЬ ВОЗМОЖНОСТЬ (УСЛОВИЕ) ПЕРЕХОДА НА АБОНЕМЕНТ
                {
                    CardInfo.SumOnCard = CardInfo.SumOnCard - (int)AmountDue - (int)TarifA.ConditionAbonementPrice;

                    ToTariffPlan.TimeType ConditionBackTimeTypeId = (ToTariffPlan.TimeType)TarifA.ConditionBackTimeTypeId;
                    int ConditionBackTime = (int)TarifA.ConditionBackTime;
                    int ts = 0;
                    switch (ConditionBackTimeTypeId)
                    {
                        case ToTariffPlan.TimeType.Minutes:
                            if (CardInfo.Nulltime3 == 0)
                            {
                                DateTime NewDate;
                                NewDate = DateTime.Now.AddMinutes(ConditionBackTime);
                                ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                            }
                            else
                            {
                                DateTime NewDate;
                                NewDate = datetime0.AddSeconds(CardInfo.Nulltime3).AddMinutes(ConditionBackTime);
                                ts = (int)((NewDate - datetime0.AddSeconds(CardInfo.Nulltime3)).TotalSeconds);
                            }
                            break;
                        case ToTariffPlan.TimeType.Hours:
                            if (CardInfo.Nulltime3 == 0)
                            {
                                DateTime NewDate;
                                NewDate = DateTime.Now.AddHours(ConditionBackTime);
                                ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                            }
                            else
                            {
                                DateTime NewDate;
                                NewDate = datetime0.AddSeconds(CardInfo.Nulltime3).AddHours(ConditionBackTime);
                                ts = (int)((NewDate - datetime0.AddSeconds(CardInfo.Nulltime3)).TotalSeconds);
                            }
                            break;
                        case ToTariffPlan.TimeType.Days:
                            if (CardInfo.Nulltime3 == 0)
                            {
                                DateTime NewDate;
                                NewDate = DateTime.Now.AddDays(ConditionBackTime);
                                ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                            }
                            else
                            {
                                DateTime NewDate;
                                NewDate = datetime0.AddSeconds(CardInfo.Nulltime3).AddDays(ConditionBackTime);
                                ts = (int)((NewDate - datetime0.AddSeconds(CardInfo.Nulltime3)).TotalSeconds);
                            }
                            break;
                        case ToTariffPlan.TimeType.Weeeks:
                            if (CardInfo.Nulltime3 == 0)
                            {
                                DateTime NewDate;
                                NewDate = DateTime.Now.AddDays(7 * ConditionBackTime);
                                ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                            }
                            else
                            {
                                DateTime NewDate;
                                NewDate = datetime0.AddSeconds(CardInfo.Nulltime3).AddDays(7 * ConditionBackTime);
                                ts = (int)((NewDate - datetime0.AddSeconds(CardInfo.Nulltime3)).TotalSeconds);
                            }
                            break;
                        case ToTariffPlan.TimeType.Months:
                            if (CardInfo.Nulltime3 == 0)
                            {
                                DateTime NewDate;
                                NewDate = DateTime.Now.AddMonths(ConditionBackTime);
                                ts = (int)((NewDate - DateTime.Now).TotalSeconds);
                            }
                            else
                            {
                                DateTime NewDate;
                                NewDate = datetime0.AddSeconds(CardInfo.Nulltime3).AddMonths(ConditionBackTime);
                                ts = (int)((NewDate - datetime0.AddSeconds(CardInfo.Nulltime3)).TotalSeconds);
                            }
                            break;
                        default:
                            throw new ApplicationException(string.Format(ResourceManager.GetString("TimePeriodIncorrect", DefaultCulture), ConditionBackTimeTypeId));
                            //break;
                    }
                    if (CardInfo.Nulltime3 >= ttt)
                    {
                        CardInfo.Nulltime3 = CardInfo.Nulltime3 + ts;
                    }
                    else
                        CardInfo.Nulltime3 = ttt + ts;

                    CardInfo.LastRecountTime = ttt;

                    //CardInfo.LastPaymentTime = ttt;        //??????????????????

                    //новый ТП на карту
                    Guid? TTP = TarifA.TariffPlanNextId;
                    req = "select * from Tariffs where Id='" + ((Guid)TTP).ToString() + "'";
                    DataRow TarifiTPidN = null;
                    lock (db_lock)
                    {
                        try
                        {
                            TarifiTPidN = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                TarifiTPidN = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                    if (TarifiTPidN != null)
                        CardInfo.TPidFC = (byte)TarifiTPidN.GetInt("IdFC");
                }
                else
                {
                    // НЕТ ВОЗМОЖНОСТИ (УСЛОВИЯ) ПЕРЕХОДА НА АБОНЕМЕНТ
                    ToAbonement = null;
                    //    return false;
                }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                return CardInfo;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return CardInfo;
            }
        }
        public int TariffPlanForwardSchedulesCount(Guid TarifiTS, Guid TarifiTP)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                int c;
                string req = "select count(*)  as C from TariffPlanTariffScheduleModel where TariffScheduleId='" + TarifiTS.ToString() +
                    "' and (TariffPlanId='" + TarifiTP.ToString() +
                       "' or TariffPlanNextId='" + TarifiTP.ToString() +
                       "') and TypeId = 3 and (_IsDeleted is null or _IsDeleted = 0)";
                DataRow ct = null;
                lock (db_lock)
                {
                    try
                    {
                        ct = UtilsBase.FillRow(req, db, null, null);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            ct = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }
                if (ct != null)
                    c = (int)ct.GetInt("C");
                else
                    c = 0;
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                return c;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return 0;
            }
        }

        TariffPlanTariffScheduleModelT SetTariffPlanTariffScheduleModel(DataRow PlanTariffScheduleModel)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            TariffPlanTariffScheduleModelT t1 = new TariffPlanTariffScheduleModelT();
            t1.Id = (Guid)PlanTariffScheduleModel.GetGuid("Id");
            t1.TariffScheduleId = (Guid)PlanTariffScheduleModel.GetGuid("TariffScheduleId");
            t1.TariffPlanId = (Guid)PlanTariffScheduleModel.GetGuid("TariffPlanId");
            t1.TariffPlanNextId = PlanTariffScheduleModel.GetGuid("TariffPlanNextId");
            t1.TypeId = (int)PlanTariffScheduleModel.GetInt("TypeId");
            t1._IsDeleted = (bool)PlanTariffScheduleModel.GetBool("_IsDeleted");
            t1.Sync = PlanTariffScheduleModel.GetDateTime("Sync");
            t1.ConditionZoneAfter = PlanTariffScheduleModel.GetGuid("ConditionZoneAfter");
            t1.ConditionParkingTime = PlanTariffScheduleModel.GetInt("ConditionParkingTime");
            t1.ConditionParkingTimeTypeId = PlanTariffScheduleModel.GetInt("ConditionParkingTimeTypeId");
            t1.ConditionEC = PlanTariffScheduleModel.GetInt("ConditionEC");
            t1.ConditionECProtectInterval = PlanTariffScheduleModel.GetInt("ConditionECProtectInterval");
            t1.ConditionECProtectIntervalTimeTypId = PlanTariffScheduleModel.GetInt("ConditionECProtectIntervalTimeTypId");
            t1.ConditionAbonementPrice = PlanTariffScheduleModel.GetInt("ConditionAbonementPrice");
            t1.ConditionBackTime = PlanTariffScheduleModel.GetInt("ConditionBackTime");
            t1.ConditionBackTimeTypeId = PlanTariffScheduleModel.GetInt("ConditionBackTimeTypeId");
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return t1;
        }
        public List<TariffPlanTariffScheduleModelT> TariffPlanForwardSchedules(Guid TarifiTS, Guid TarifiTP)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                List<TariffPlanTariffScheduleModelT> t = new List<TariffPlanTariffScheduleModelT>();
                string req = "select * from TariffPlanTariffScheduleModel where TariffScheduleId='" + TarifiTS.ToString() +
                    "' and (TariffPlanId='" + TarifiTP.ToString() +
                       "' or TariffPlanNextId='" + TarifiTP.ToString() +
                       "') and TypeId = 3 and (_IsDeleted is null or _IsDeleted = 0)  order by ConditionAbonementPrice ";
                DataTable TariffPlanTariffScheduleModel = null;
                lock (db_lock)
                {
                    try
                    {
                        TariffPlanTariffScheduleModel = UtilsBase.FillTable(req, db, null, "@key", Valute);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            TariffPlanTariffScheduleModel = UtilsBase.FillTable(req, db, null, "@key", Valute);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }
                if (TariffPlanTariffScheduleModel != null || TariffPlanTariffScheduleModel.Rows.Count > 0)
                    for (int i = 0; i < TariffPlanTariffScheduleModel.Rows.Count; i++)
                    {
                        t.Add(SetTariffPlanTariffScheduleModel(TariffPlanTariffScheduleModel.Rows[i]));
                    }
                if (DetailLog)
                    ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
                return t;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return null;
            }
        }
        public void ReSync()
        {
            //ReSyncReq = false;
            ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                ToLog(2, "Cashier: ReSync: Utils.Work stop");
                SqlBaseImport.Model.Utils.Work = false;
                while (Utils.Running)
                    Thread.Sleep(50);
                //ToLog(2, "Cashier: ReSync: Utils.Work stoped");
                string req = "select top 1 * from DeviceCMModel where DeviceId='" + DeviceId.ToString() + "' and (_IsDeleted is null or _IsDeleted = 0) ";
                DataRow deviceCM = null;
                lock (db_lock)
                {
                    try
                    {
                        deviceCM = UtilsBase.FillRow(req, db, null, null);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            deviceCM = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }

                deviceCMModel = SetDeviceCM(deviceCM);
                Guid? ZoneId = null;
                Guid? GroupId = null;
                int? PenaltyCardTPId = null;
                int? PenaltyCardTSId = null;
                if (deviceCMModel != null)
                {
                    ZoneId = deviceCMModel.ZoneId;
                    GroupId = deviceCMModel.GroupId;
                    PenaltyCardTPId = deviceCMModel.PenaltyCardTPId;
                    PenaltyCardTSId = deviceCMModel.PenaltyCardTSId;
                    deviceCMModel.ZoneId = null;
                    deviceCMModel.GroupId = null;
                    deviceCMModel.PenaltyCardTPId = null;
                    deviceCMModel.PenaltyCardTSId = null;
                }
                try
                {
                    ToLog(2, "Cashier: ReSync: BaseSync.ClearDevice");
                    BaseSync.ClearDevice(db);
                    ToLog(2, "Cashier: ReSync: Utils.Sync");
                    SqlBaseImport.Model.Utils.Sync(conserver, conlocal);
                }
                catch (Exception ex)
                {
                    ToLog(0, "Error in ReSync: " + " Exception: " + ex.Message);
                }
                ToLog(2, "Cashier: ReSync: Utils.Work started");
                SqlBaseImport.Model.Utils.Work = true;
                if (deviceCMModel != null)
                {
                    if (ZoneId != null)
                    {
                        req = "select * from ZoneModel where Id='" + ((Guid)ZoneId).ToString() + "'";
                        DataRow Z = null;
                        lock (db_lock)
                        {
                            try
                            {
                                Z = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    Z = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (Z != null)
                            deviceCMModel.ZoneId = ZoneId;
                    }
                    if (GroupId != null)
                    {
                        req = "select * from [Group] where Id='" + ((Guid)GroupId).ToString() + "'";
                        DataRow G = null;
                        lock (db_lock)
                        {
                            try
                            {
                                G = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    G = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (G != null)
                            deviceCMModel.GroupId = GroupId;
                    }
                    if (PenaltyCardTPId != null)
                    {
                        req = "select * from Tariffs where IdFC=" + PenaltyCardTPId.ToString();
                        DataRow TP = null;
                        lock (db_lock)
                        {
                            try
                            {
                                TP = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    TP = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (TP != null)
                            deviceCMModel.PenaltyCardTPId = PenaltyCardTPId;
                    }
                    if (PenaltyCardTSId != null)
                    {
                        req = "select * from TariffScheduleModel where IdFC=" + PenaltyCardTSId.ToString();
                        DataRow TS = null;
                        lock (db_lock)
                        {
                            try
                            {
                                TS = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    TS = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (TS != null)
                            deviceCMModel.PenaltyCardTSId = PenaltyCardTSId;
                    }

                }
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        // Protect the connectionStrings section.
        /*public void ProtectConfiguration(string section = "connectionStrings")
        {
            ToLog(2, "Cashier: ProtectConfiguration");
            try
            {
                // Get the application configuration file.
                Configuration config =
                        ConfigurationManager.OpenExeConfiguration(
                        ConfigurationUserLevel.None);

                // Get the section to protect.
                ConfigurationSection Section =
                    config.GetSection(section);

                if (Section != null)
                {
                    if (!Section.SectionInformation.IsProtected)
                    {
                        if (!Section.ElementInformation.IsLocked)
                        {
                            // Protect the section.
                            Section.SectionInformation.ProtectSection(null);

                            Section.SectionInformation.ForceSave = true;
                            //config.Save(ConfigurationSaveMode.Full, true);
                            config.Save(ConfigurationSaveMode.Modified, true);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }*/

        // Unprotect the connectionStrings section.
        /*public void UnProtectConfiguration(string section = "connectionStrings")
        {
            ToLog(2, "Cashier: UnProtectConfiguration");
            try
            {
                // Get the application configuration file.
                Configuration config =
                        ConfigurationManager.OpenExeConfiguration(
                        ConfigurationUserLevel.None);

                // Get the section to unprotect.
                ConfigurationSection Section =
                    config.GetSection(section);

                if (Section != null)
                {
                    if (Section.SectionInformation.IsProtected)
                    {
                        if (!Section.ElementInformation.IsLocked)
                        {
                            // Unprotect the section.
                            Section.SectionInformation.UnprotectSection();

                            Section.SectionInformation.ForceSave = true;
                            //config.Save(ConfigurationSaveMode.Full, true);
                            config.Save(ConfigurationSaveMode.Modified, true);


                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }*/
        DeviceCMModelT SetDeviceCM(DataRow tab)
        {
            DeviceCMModelT deviceCM = new DeviceCMModelT();
            deviceCM.DeviceId = (Guid)tab.GetGuid("DeviceId");
            deviceCM.ZoneId = tab.GetGuid("ZoneId");
            deviceCM.CashDispenserTypeId = tab.GetInt("CashDispenserTypeId");
            deviceCM.MaxCountUp = tab.GetInt("MaxCountUp");
            deviceCM.MaxCountDown = tab.GetInt("MaxCountDown");
            deviceCM.CountUp = tab.GetInt("CountUp");
            if (deviceCM.CountUp > deviceCM.MaxCountUp)
                deviceCM.CountUp = deviceCM.MaxCountUp;
            deviceCM.CountDown = tab.GetInt("CountDown");
            if (deviceCM.CountDown > deviceCM.MaxCountDown)
                deviceCM.CountDown = deviceCM.MaxCountDown;
            deviceCM.CashDispenserComPortId = tab.GetInt("CashDispenserComPortId");
            deviceCM.CashAcceptorLimit = (int)tab.GetInt("CashAcceptorLimit");
            deviceCM.CashAcceptorAlarm = (int)tab.GetInt("CashAcceptorAlarm");
            deviceCM.CashAcceptorCurrent = (int)tab.GetInt("CashAcceptorCurrent");
            deviceCM.CashAcceptorComPortId = tab.GetInt("CashAcceptorComPortId");
            deviceCM.HopperTypeId = tab.GetInt("HopperTypeId");
            deviceCM.MaxCount2 = tab.GetInt("MaxCount2");
            deviceCM.MaxCount1 = tab.GetInt("MaxCount1");
            deviceCM.Count1 = tab.GetInt("Count1");
            if (deviceCM.Count1 > deviceCM.MaxCount1)
                deviceCM.Count1 = deviceCM.MaxCount1;
            deviceCM.Count2 = tab.GetInt("Count2");
            if (deviceCM.Count2 > deviceCM.MaxCount2)
                deviceCM.Count2 = deviceCM.MaxCount2;
            deviceCM.Hopper1ComPortId = tab.GetInt("Hopper1ComPortId");
            //deviceCM.Hopper2ComPortId = tab.GetInt("Hopper2ComPortId");
            deviceCM.CoinAcceptorLimit = (int)tab.GetInt("CoinAcceptorLimit");
            deviceCM.CoinAcceptorAlarm = (int)tab.GetInt("CoinAcceptorAlarm");
            deviceCM.CoinAcceptorCurrent = (int)tab.GetInt("CoinAcceptorCurrent");
            deviceCM.BankModuleTypeId = tab.GetInt("BankModuleTypeId");
            deviceCM.Reader = tab.GetBool("Reader");
            deviceCM.PayPass = tab.GetBool("PayPass");
            deviceCM.BarcodeTypeId = tab.GetInt("BarcodeTypeId");
            deviceCM.BarcodeComPortId = tab.GetInt("BarcodeComPortId");
            deviceCM.VAT = tab.GetInt("VAT");
            deviceCM.MaximalChange = tab.GetInt("MaximalChange");
            deviceCM.PrinterTypeId = tab.GetInt("PrinterTypeId");
            deviceCM.PrinterComPortId = tab.GetInt("PrinterComPortId");
            deviceCM.CardReaderComPortId = tab.GetInt("CardReaderComPortId");
            deviceCM.CardTimeOutToBasket = tab.GetInt("CardTimeOutToBasket");
            deviceCM.PenalCardTimeOut = tab.GetInt("PenalCardTimeOut");
            deviceCM.CancelRefund = tab.GetBool("CancelRefund");
            deviceCM.ShiftOpened = tab.GetBool("ShiftOpened");
            deviceCM._IsDeleted = tab.GetBool("_IsDeleted");
            deviceCM.CardKey = tab.GetString("CardKey");
            deviceCM.CashierNumber = tab.GetInt("CashierNumber");
            deviceCM.CashDispenserExist = tab.GetBool("CashDispenserExist");
            deviceCM.NominalUpId = tab.GetInt("NominalUpId");
            deviceCM.NominalDownId = tab.GetInt("NominalDownId");
            deviceCM.RejectCount = tab.GetInt("RejectCount");
            deviceCM.CashAcceptorExist = tab.GetBool("CashAcceptorExist");
            deviceCM.HopperExist = tab.GetBool("HopperExist");
            deviceCM.NominalRightId = tab.GetInt("NominalRightId");
            deviceCM.NominalLeftId = tab.GetInt("NominalLeftId");
            deviceCM.CoinAcceptorExist = tab.GetBool("CoinAcceptorExist");
            deviceCM.CoinAcceptorTypeId = tab.GetInt("CoinAcceptorTypeId");
            deviceCM.BankModuleExist = tab.GetBool("BankModuleExist");
            deviceCM.CardDispenserExists = tab.GetBool("CardDispenserExists");
            deviceCM.CardDispenserTypeId = tab.GetInt("CardDispenserTypeId");
            deviceCM.CardDispenserComPortId = tab.GetInt("CardDispenserComPortId");
            deviceCM.BarcodeExist = tab.GetBool("BarcodeExist");
            deviceCM.KKMExists = tab.GetBool("KKMExists");
            deviceCM.KKMTypeId = tab.GetInt("KKMTypeId");
            deviceCM.ShiftAutoClose = tab.GetBool("ShiftAutoClose");
            deviceCM.ShiftNewShiftTime = tab.GetDateTime("ShiftNewShiftTime");
            deviceCM.CommandPassword = tab.GetString("CommandPassword");
            deviceCM.RegistartionPass = tab.GetString("RegistartionPass");
            deviceCM.WithOutCleaningPass = tab.GetString("WithOutCleaningPass");
            deviceCM.WithCleaningPass = tab.GetString("WithCleaningPass");
            deviceCM.KKMComPortId = tab.GetInt("KKMComPortId");
            deviceCM.PrinterExists = tab.GetBool("PrinterExists");
            deviceCM.CardReaderExist = tab.GetBool("CardReaderExist");
            deviceCM.CardReaderTypeId = tab.GetInt("CardReaderTypeId");
            deviceCM.SlaveExist = tab.GetBool("SlaveExist");
            deviceCM.SlaveTypeId = tab.GetInt("SlaveTypeId");
            deviceCM.SlaveComPortId = tab.GetInt("SlaveComPortId");
            deviceCM.PenaltyCardTPId = tab.GetInt("PenaltyCardTPId");
            deviceCM.PenaltyCardTSId = tab.GetInt("PenaltyCardTSId");
            deviceCM.SyncNominals = tab.GetBool("SyncNominals");
            deviceCM.ServerExchangeProtocolId = tab.GetInt("ServerExchangeProtocolId");
            deviceCM.ServerURL = tab.GetString("ServerURL");
            deviceCM.CurrentTransactionNumber = tab.GetInt("CurrentTransactionNumber");
            deviceCM.LanguageTimeOut = tab.GetInt("LanguageTimeOut");
            deviceCM.DefaultLanguage = tab.GetString("DefaultLanguage");
            deviceCM.WebCameraIP = tab.GetString("WebCameraIP");
            deviceCM.WebCameraLogin = tab.GetString("WebCameraLogin");
            deviceCM.WebCameraPassword = tab.GetString("WebCameraPassword");
            deviceCM.LastShiftOpenTime = tab.GetDateTime("LastShiftOpenTime");
            deviceCM.CashAcceptorTypeId = tab.GetInt("CashAcceptorTypeId");
            deviceCM.CoinAcceptorComPortId = tab.GetInt("CoinAcceptorComPortId");
            deviceCM.Sync = tab.GetDateTime("Sync");
            deviceCM.WebCameraUrl = tab.GetString("WebCameraUrl");
            deviceCM.GroupId = tab.GetGuid("GroupId");
            deviceCM.CoinAcceptorPinCode = tab.GetString("CoinAcceptorPinCode");
            deviceCM.CoinAcceptorAddress = (byte?)tab.GetInt("CoinAcceptorAddress");
            deviceCM.SingleClient0 = tab.GetBool("SingleClient0");
            deviceCM.CashierType = (byte?)tab.GetInt("CashierType");
            deviceCM.LogLevel = (byte)tab.GetInt("LogLevel");
            deviceCM.SectorNumber = (byte?)tab.GetInt("SectorNumber");
            deviceCM.ChequeTimeOut = (byte?)tab.GetInt("ChequeTimeOut");
            deviceCM.ShiftNumber = tab.GetInt("ShiftNumber");
            deviceCM.ChequeWork = (byte?)tab.GetInt("ChequeWork");
            return deviceCM;
        }
        DeviceModelT SetDevice(DataRow row)
        {
            DeviceModelT dm = new DeviceModelT();
            dm.Advanced = row.GetString("Advanced");
            dm.DataBase = row.GetString("DataBase");
            dm.Host = row.GetString("Host");
            dm.Id = (Guid)row.GetGuid("Id");
            dm.Name = row.GetString("Name");
            dm.Password = row.GetString("Password");
            dm.SlaveCode = row.GetString("SlaveCode");
            dm.Sync = row.GetDateTime("Sync");
            dm.Type = (int)row.GetInt("Type");
            dm.User = row.GetString("User");

            return dm;
        }
        void FillBigCalculator()
        {

            //ToLog(2, "Cashier: FillBigCalculator");
            /*
            if ((byte)TechForm.ZoneComboBoxSelectedIndex >= 0 && TariffSchedule.Count > 0)
            {
                CardInfo.ClientGroupidFC = 1;
                CardInfo.ClientTypidFC = 0;
                CardInfo.LastRecountTime = (int)TimeSpan.FromMinutes(10).TotalSeconds;
                CardInfo.Nulltime1 = 0;
                CardInfo.Nulltime2 = 0;
                CardInfo.Nulltime3 = 0;
                CardInfo.ParkingEnterTime = (int)TimeSpan.FromMinutes(10).TotalSeconds;
                CardInfo.SumOnCard = 0;
                CardInfo.TKVP = 0;
                CardInfo.TVP = 0;
                CardInfo.ZoneidFC = (byte)TechForm.ZoneComboBoxSelectedIndex;
                CardInfo.SumOnCard = 0;
                CardInfo.TSidFC = (byte)TariffSchedule[0].IdFC;
                for (int i = 0; i < tariffs.Count; i++)
                {
                    CardInfo.TPidFC = (byte)tariffs[i].IdFC;
                    ToLog(2, "Cashier: FillBigCalculator: TSidFC=" + CardInfo.TSidFC.ToString() + " TPidFC=" + CardInfo.TPidFC.ToString() + "");

                    CalculatorTS.DateCards = CardInfo;
                    CalculatorTS.BigCalculator();
                    //ToLog(2, "Cashier: FillBigCalculator: TSidFC=" + CardInfo.TSidFC.ToString() + " TPidFC=" + CardInfo.TPidFC.ToString() + " End");
                }
                for (int i = 1; i < TariffSchedule.Count; i++)
                {
                    CardInfo.TPidFC = (byte)tariffs[0].IdFC;
                    CardInfo.TSidFC = (byte)TariffSchedule[i].IdFC;
                    ToLog(2, "Cashier: FillBigCalculator: TSidFC=" + CardInfo.TSidFC.ToString() + " TPidFC=" + CardInfo.TPidFC.ToString() + "");

                    CalculatorTS.DateCards = CardInfo;
                    CalculatorTS.BigCalculator();
                    ToLog(2, "Cashier: FillBigCalculator: TSidFC=" + CardInfo.TSidFC.ToString() + " TPidFC=" + CardInfo.TPidFC.ToString() + " End");
                }
            }
            */
        }

        void LogCardReaded()
        {
            string s = "";
            ToLog(1, ToLogCardReaded + " " + ToLogIDCard + " " + ReaderIDCardDec);
            ToLog(1, ToLogCardReaded + " " + ToLogEntranceTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogRecountTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogLastPaymentTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogTariffID + " " + CardInfo.TPidFC.ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogRegular + " " + CardInfo.ClientTypidFC.ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogTariffSheduleID + " " + CardInfo.TSidFC.ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogZoneid + " " + CardInfo.ZoneidFC.ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogClientGroupid + " " + CardInfo.ClientGroupidFC.ToString());
            s = string.Format(ToLogCardReaded + " " + ToLogSumCard, CardInfo.SumOnCard.ToString(), sValuteLabel);
            ToLog(1, s);
            ToLog(1, ToLogCardReaded + " " + ToLogNulltime1Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogNulltime2Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogNulltime3Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogTVP + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.TVP).ToString()).ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogTKVP + " " + CardInfo.TKVP.ToString());
            ToLog(1, ToLogCardReaded + " " + ToLogDateSaveCard + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard)).ToString());
        }
        void LogCardsOnline()
        {
            string s = "";
            ToLog(1, ToLogCardsOnline + " " + ToLogIDCard + " " + ReaderIDCardDec);
            ToLog(1, ToLogCardsOnline + " " + ToLogEntranceTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogRecountTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogLastPaymentTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogTariffID + " " + CardInfo.TPidFC.ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogRegular + " " + CardInfo.ClientTypidFC.ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogTariffSheduleID + " " + CardInfo.TSidFC.ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogZoneid + " " + CardInfo.ZoneidFC.ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogClientGroupid + " " + CardInfo.ClientGroupidFC.ToString());
            s = string.Format(ToLogCardsOnline + " " + ToLogSumCard, CardInfo.SumOnCard.ToString(), sValuteLabel);
            ToLog(1, s);
            ToLog(1, ToLogCardsOnline + " " + ToLogNulltime1Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogNulltime2Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogNulltime3Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogTVP + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.TVP).ToString()).ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogTKVP + " " + CardInfo.TKVP.ToString());
            ToLog(1, ToLogCardsOnline + " " + ToLogDateSaveCard + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard)).ToString());
        }

        void LogToCardsOnline(CardsTransactionT cardstransactions)
        {
            string s = "";
            ToLog(1, ToLogToCardsOnline + " " + ToLogIDCard + " " + cardstransactions.CardId.ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogEntranceTime + " " + ((DateTime)cardstransactions.ParkingEnterTime).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogRecountTime + " " + ((DateTime)cardstransactions.LastRecountTime).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogLastPaymentTime + " " + ((DateTime)cardstransactions.LastPaymentTime).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogTariffID + " " + ((byte)cardstransactions.TPidFC).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogRegular + " " + ((byte)cardstransactions.ClientTypidFC).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogTariffSheduleID + " " + ((byte)cardstransactions.TSidFC).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogZoneid + " " + ((byte)cardstransactions.ZoneidFC).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogClientGroupid + " " + ((byte)cardstransactions.ClientGroupidFC).ToString());
            s = string.Format(ToLogToCardsOnline + " " + ToLogSumCard, ((int)cardstransactions.SumOnCard).ToString(), sValuteLabel);
            ToLog(1, s);
            ToLog(1, ToLogToCardsOnline + " " + ToLogNulltime1Time + " " + ((DateTime)cardstransactions.Nulltime1).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogNulltime2Time + " " + ((DateTime)cardstransactions.Nulltime2).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogNulltime3Time + " " + ((DateTime)cardstransactions.Nulltime3).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogTVP + " " + ((DateTime)cardstransactions.TVP).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogTKVP + " " + ((int)cardstransactions.TKVP).ToString());
            ToLog(1, ToLogToCardsOnline + " " + ToLogDateSaveCard + " " + ((DateTime)cardstransactions.DateSaveCard).ToString());
        }

        void LogCardCalculated()
        {
            string s = "";
            ToLog(1, ToLogCardCalculated + " " + ToLogEntranceTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogRecountTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogLastPaymentTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogTariffID + " " + CardInfo.TPidFC.ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogRegular + " " + CardInfo.ClientTypidFC.ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogTariffSheduleID + " " + CardInfo.TSidFC.ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogZoneid + " " + CardInfo.ZoneidFC.ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogClientGroupid + " " + CardInfo.ClientGroupidFC.ToString());
            s = string.Format(ToLogCardCalculated + " " + ToLogSumCard, CardInfo.SumOnCard.ToString(), sValuteLabel);
            ToLog(1, s);
            ToLog(1, ToLogCardCalculated + " " + ToLogNulltime1Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogNulltime2Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogNulltime3Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogTVP + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.TVP).ToString()).ToString());
            ToLog(1, ToLogCardCalculated + " " + ToLogTKVP + " " + CardInfo.TKVP.ToString());
            s = string.Format(ToLogCardCalculated + " " + ToLogAmountSumCard, AmountDue.ToString(), sValuteLabel);
            ToLog(1, s);
            if (CardQuery > 0)
                s = string.Format(ToLogCardCalculated + " " + ToLogCardDebt, CardQuery.ToString(), sValuteLabel);
            else
                s = string.Format(ToLogCardCalculated + " " + ToLogCardBalance, (-CardQuery).ToString(), sValuteLabel);
            ToLog(1, s);
        }
        void LogWriteCard()
        {
            string s = "";
            ToLog(1, ToLogWriteCard + " " + ToLogEntranceTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogRecountTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogLastPaymentTime + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogTariffID + " " + CardInfo.TPidFC.ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogRegular + " " + CardInfo.ClientTypidFC.ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogTariffSheduleID + " " + CardInfo.TSidFC.ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogZoneid + " " + CardInfo.ZoneidFC.ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogClientGroupid + " " + CardInfo.ClientGroupidFC.ToString());
            s = string.Format(ToLogWriteCard + " " + ToLogSumCard, CardInfo.SumOnCard.ToString(), sValuteLabel);
            ToLog(1, s);
            ToLog(1, ToLogWriteCard + " " + ToLogNulltime1Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogNulltime2Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogNulltime3Time + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogTVP + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.TVP).ToString()).ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogTKVP + " " + CardInfo.TKVP.ToString());
            ToLog(1, ToLogWriteCard + " " + ToLogDateSaveCard + " " + (datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard)).ToString());
        }
    }

}
