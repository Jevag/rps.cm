﻿using Newtonsoft.Json;
using System;
using System.Windows;
using System.Threading;
using System.ComponentModel;
using EventWeb;
using CommonClassLibrary;
using System.Net;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using Rps.ShareBase;
using System.Data;
using CalculatorTS;
using System.Diagnostics;

namespace Cashier
{
    public partial class MainWindow : Window
    {
        TariffsT _TarifiTP;
        TariffScheduleModelT _TarifiTS;
        EventWebClient CheckEventWebClient, CardReaderEventWebClient, TalkOperatorEventWebClient;
        BackgroundWorker CheckEventServerBackgroundWorker, CardReaderEventSenderBackgroundWorker, TalkOperatorEventSenderBackgroundWorker;

        object CardDataLock = new object(), PenalCardLock = new object(), ShiftLock = new object(), ReSyncLock = new object(), ReaderLock = new object();
        bool EventWebClientBackgroundWorkerInitialised = false;

        #region TalkOperatorEventSender
        private void CancelTalkOperatorEventSender(object sender, EventArgs e)
        {
            ToLog(2, "Cashier: CancelTalkOperatorEventSender");
            try
            {
                if (TalkOperatorEventSenderBackgroundWorker.WorkerSupportsCancellation == true)
                {
                    // Cancel the asynchronous operation.
                    TalkOperatorEventSenderBackgroundWorker.CancelAsync();
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void InitializeTalkOperatorEventSenderBackgroundWorker()
        {
            ToLog(2, "Cashier: InitializeTalkOperatorEventSenderBackgroundWorker");
            try
            {
                if (deviceCMModel.ServerURL != null)
                {
                    //TalkOperatorEventSenderBackgroundWorkerInitialised = true;
                    TalkOperatorEventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);

                    TalkOperatorEventSenderBackgroundWorker.WorkerReportsProgress = true;
                    TalkOperatorEventSenderBackgroundWorker.WorkerSupportsCancellation = true;

                    TalkOperatorEventSenderBackgroundWorker.DoWork +=
                        new DoWorkEventHandler(
                            TalkOperatorEventSenderBackgroundWorker_DoWork);
                    TalkOperatorEventSenderBackgroundWorker.ProgressChanged +=
                        new ProgressChangedEventHandler(
                    TalkOperatorEventSenderBackgroundWorker_StatusChanged);
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void TalkOperatorEventSenderBackgroundWorker_StatusChanged(object sender, ProgressChangedEventArgs e)
        {
            ToLog(2, "Cashier: TalkOperatorEventSenderBackgroundWorker_StatusChanged");
            try
            {
                if (e.ProgressPercentage == 0)
                    EventWebServer = false;
                else
                    EventWebServer = true;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void TalkOperatorEventSenderBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ToLog(2, "Cashier: TalkOperatorEventSenderBackgroundWorker_DoWork");
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;

                while (true)
                {
                    if (worker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        if ((Talk != null) && Talk.Run && !Talk.Answered)
                        {
                            if ((deviceCMModel.ServerURL != null) && (DeviceId.ToString() != null))
                            {
                                EventWeb.EventWebClient eventWebClient = new EventWeb.EventWebClient("http://" + deviceCMModel.ServerURL, log);
                                string StrCall = "/ws/DeviceCall?T=DeviceCMModel&A=I&DeviceId=" + DeviceId.ToString().ToUpper();
                                //ToLog(2, "Cashier: TalkOperatorEventSenderBackgroundWorker_DoWork: eventWebClient.Process");
                                eventWebClient.Process(StrCall);

                            }
                        }

                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        #endregion TalkOperatorEventSender
        #region CheckEventWebClient
        private void CancelCheckEventWebClient(object sender, EventArgs e)
        {
            ToLog(2, "Cashier: CancelCheckEventWebClient");
            if (CheckEventServerBackgroundWorker.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                CheckEventServerBackgroundWorker.CancelAsync();
            }
        }
        private void InitializeCheckEventWebClientBackgroundWorker()
        {
            ToLog(2, "Cashier: InitializeCheckEventWebClientBackgroundWorker");
            if (deviceCMModel.ServerURL != null)
            {
                EventWebClientBackgroundWorkerInitialised = true;
                CheckEventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);

                CheckEventServerBackgroundWorker.WorkerReportsProgress = true;
                CheckEventServerBackgroundWorker.WorkerSupportsCancellation = true;

                CheckEventServerBackgroundWorker.DoWork +=
                    new DoWorkEventHandler(
                        CheckEventServerBackgroundWorker_DoWork);
                CheckEventServerBackgroundWorker.ProgressChanged +=
                    new ProgressChangedEventHandler(
                CheckEventServerBackgroundWorker_StatusChanged);
            }
        }
        private void CheckEventServerBackgroundWorker_StatusChanged(object sender, ProgressChangedEventArgs e)
        {
            ToLog(2, "Cashier: CheckEventServerBackgroundWorker_StatusChanged");
            if (e.ProgressPercentage == 0)
                EventWebServer = false;
            else
                EventWebServer = true;
        }
        private void CheckEventServerBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ToLog(2, "Cashier: CheckEventServerBackgroundWorker_DoWork");
            BackgroundWorker worker = sender as BackgroundWorker;

            while (true)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {

                    if (deviceCMModel.CashierNumber != null)
                    {
                        //ToLog(2, "Cashier: CheckEventServerBackgroundWorker_DoWork: CheckEventWebClient.Process");
                        CheckEventWebClient.Process("CheckStatus?CashierNumber=" + deviceCMModel.CashierNumber.ToString());
                        if (CheckEventWebClient.response.Responsed)
                        {
                            if (CheckEventWebClient.response.Status == WebAnswerT.Succsess)
                            {
                                if (!EventWebServer)
                                    worker.ReportProgress(1);
                            }
                            else
                            {
                                if (EventWebServer)
                                    worker.ReportProgress(0);
                            }
                        }
                        else
                        {
                            if (EventWebServer)
                                worker.ReportProgress(0);
                        }
                    }
                    else
                    {
                        if (EventWebServer)
                            worker.ReportProgress(0);
                    }
                    Thread.Sleep(1000);
                }
            }
        }
        #endregion CheckEventWebClient
        #region CardReaderEventSender
        private void CancelCardReaderEventSender(object sender, EventArgs e)
        {
            ToLog(2, "Cashier: CancelCardReaderEventSender");
            if (CardReaderEventSenderBackgroundWorker.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                CardReaderEventSenderBackgroundWorker.CancelAsync();
            }
        }
        private void InitializeCardReaderEventSenderBackgroundWorker()
        {
            ToLog(2, "Cashier: InitializeCardReaderEventSenderBackgroundWorker");
            if (deviceCMModel.ServerURL != null)
            {
                //CardReaderEventSenderBackgroundWorkerInitialised = true;
                CardReaderEventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);

                CardReaderEventSenderBackgroundWorker.WorkerSupportsCancellation = true;

                CardReaderEventSenderBackgroundWorker.DoWork +=
                    new DoWorkEventHandler(
                        CardReaderEventSenderBackgroundWorker_DoWork);
            }
        }
        private void CardReaderEventSenderBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ToLog(2, "Cashier: CardReaderEventSenderBackgroundWorker_DoWork");
            BackgroundWorker worker = sender as BackgroundWorker;

            while (true)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    if (deviceCMModel.ServerURL != null)
                    {
                        if (CardDispenserStatus.CardPreSendPos)
                        {
                            //ToLog(2, "Cashier: CardReaderEventSenderBackgroundWorker_DoWork: eventWebClient.Process");
                            EventWebClient eventWebClient = new EventWebClient("http://" + deviceCMModel.ServerURL, log);
                            string StrIDCardSend = "/ws/CardExist?DeviceGUID=" + DeviceId.ToString().ToUpper();
                            StrIDCardSend += "&Now=" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            eventWebClient.Process(StrIDCardSend);
                        }
                    }
                }
                Thread.Sleep(1000);
            }
        }
        #endregion CardReaderEventSender

        CardReaderResponse SetProgrammStatus(CardReaderResponse ProgramStatus, ToTariffPlan.DateCard lCardInfo)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                ProgramStatus.CardId = ReaderIDCardHex;
                ProgramStatus.ClientGroupidFC = lCardInfo.ClientGroupidFC;             // группа клиента на карте
                ProgramStatus.ClientTypidFC = lCardInfo.ClientTypidFC;
                ProgramStatus.LastPaymentTime = (datetime0 + TimeSpan.FromSeconds(lCardInfo.LastPaymentTime)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.LastRecountTime = (datetime0 + TimeSpan.FromSeconds(lCardInfo.LastRecountTime)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.Nulltime1 = (datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime1)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.Nulltime2 = (datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime2)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.Nulltime3 = (datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime3)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.ParkingEnterTime = (datetime0 + TimeSpan.FromSeconds(lCardInfo.ParkingEnterTime)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда
                ProgramStatus.DateSaveCard = (datetime0 + TimeSpan.FromSeconds(lCardInfo.DateSaveCard)).ToString("yyyy-MM-dd HH:mm:ss"); // Время сохранения карты
                ProgramStatus.SumOnCard = lCardInfo.SumOnCard;            // сумма (баланс) на карте
                ProgramStatus.TKVP = lCardInfo.TKVP;
                ProgramStatus.TPidFC = lCardInfo.TPidFC;              // ID ТП на карте
                ProgramStatus.TSidFC = lCardInfo.TSidFC;              // ID ТС на карте
                ProgramStatus.TVP = (datetime0 + TimeSpan.FromSeconds(lCardInfo.TVP)).ToString("yyyy-MM-dd HH:mm:ss"); // Время въезда в зону
                ProgramStatus.ZoneidFC = lCardInfo.ZoneidFC;               // зона на карте

                try
                {
                    for (int i = 0; i < zones.Count; i++)
                        if (lCardInfo.ZoneidFC == zones[i].IdFC)
                            ProgramStatus.Zone = zones[i];
                }
                catch (Exception exc)
                {
                    ToLog(0, exc.ToString());
                }
                ProgramStatus.Tariff = _TarifiTP;
                ProgramStatus.TariffShedule = _TarifiTS;
                ProgramStatus.AmountDue = AmountDue;
                decimal b = CardSum - AmountDue;
                ProgramStatus.Abonements.Clear();
                if (TSExists)
                {
                    List<TariffPlanTariffScheduleModelT> ts = new List<TariffPlanTariffScheduleModelT>();
                    for (int i = 0; i < TSRulesCount; i++)
                        ts.Add(TariffPlanTariffScheduleList[i]);
                    if (ts.Count > 0)
                    {
                        for (int i = 0; i < ts.Count; i++)
                        {
                            string Period = GetPeriodName((int)ts[i].ConditionBackTime, (int)ts[i].ConditionBackTimeTypeId);

                            AbonementT abonement = new AbonementT()
                            {
                                Id = ts[i].Id,
                                ConditionAbonementPrice = (int)ts[i].ConditionAbonementPrice,
                                ConditionTime = ((int)ts[i].ConditionBackTime),
                                ConditionBackTimeTypeId = (int)ts[i].ConditionBackTimeTypeId,
                                ConditionTimePeriod = Period
                            };
                            ProgramStatus.Abonements.Add(abonement);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return ProgramStatus;
        }
        CardReaderResponse CalcCardInfo(CardReaderResponse ProgramStatus, ToTariffPlan.DateCard? vCardInfo = null)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            ToTariffPlan.DateCard lCardInfo;
            if (vCardInfo != null)
                lCardInfo = (ToTariffPlan.DateCard)vCardInfo;
            else
                lCardInfo = CardInfo;
            //ToLog(2, "Cashier: CalcCardInfo");
            try
            {
                {
                    DateTime DateExitEstimated;
                    uint cardid;
                    uint.TryParse(ReaderIDCardHex, NumberStyles.HexNumber, null, out cardid);
                    ReaderIDCardDec = cardid.ToString();

                    string req = "select * from Cards where CardId=" + cardid.ToString();
                    DataRow Cards = null;
                    lock (db_lock)
                    {
                        try
                        {
                            Cards = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                Cards = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                    if (Cards == null)
                        ProgramStatus.IsDababaseExists = false;
                    else
                        ProgramStatus.IsDababaseExists = true;
                    req = "select * from TariffScheduleModel where IdFC=" + lCardInfo.TSidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                    DataRow _TarifiTS_R = null;
                    lock (db_lock)
                    {
                        try
                        {
                            _TarifiTS_R = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                _TarifiTS_R = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                    TarifiTS = null;
                    if (_TarifiTS_R != null)
                    {
                        TarifiTS = _TarifiTS_R.GetGuid("Id");
                        _TarifiTS = SetTarifiTS(_TarifiTS_R);
                    }
                    ProgramStatus.TSidFC = lCardInfo.TSidFC;
                    req = "select * from Tariffs where IdFC=" + lCardInfo.TPidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                    DataRow _TarifiTP_R = null;
                    lock (db_lock)
                    {
                        try
                        {
                            _TarifiTP_R = UtilsBase.FillRow(req, db, null, null);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                _TarifiTP_R = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                    TarifiTP = null;
                    if (_TarifiTP_R != null)
                    {
                        TarifiTP = _TarifiTP_R.GetGuid("Id");
                        _TarifiTP = SetTarifiTP(_TarifiTP_R);
                    }
                    ProgramStatus.TPidFC = lCardInfo.TPidFC;
                    CalculatorTS.DateCards = lCardInfo;
                    lock (TariffPlanTariffScheduleList)
                    {
                        try
                        {
                            TSRulesCount = 0;
                            if (_TarifiTP != null && _TarifiTS != null)
                                TSRulesCount = TariffPlanForwardSchedulesCount((Guid)TarifiTS, (Guid)TarifiTP);

                            TSExists = (TSRulesCount > 0);
                            if (TSExists)
                                TariffPlanTariffScheduleList = TariffPlanForwardSchedules((Guid)TarifiTS, (Guid)TarifiTP);
                        }
                        catch
                        {
                            TSRulesCount = 0;
                            TSExists = (TSRulesCount > 0);
                        }
                    }

                    try
                    {
                        decimal lAmountDue = 0;
                        if (lCardInfo.ZoneidFC != 0)
                        {
                            //ToLog(2, "Cashier: CalcCardInfo: Begin Calculate Success");
                            if (CalculatorTS.BigCalculator())
                            {
                                //ToLog(2, "Cashier: CalcCardInfo: End Calculate Success");
                                lCardInfo = CalculatorTS.DateCards;
                                CardSum = lCardInfo.SumOnCard;
                                lAmountDue = CalculatorTS.Amount;
                                if (transactions != null)
                                {
                                    transactions.SumOnCardAfter = CardSum;
                                    transactions.Amount = lAmountDue;

                                    transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(lCardInfo.LastRecountTime);
                                    transactions.TarifPlanId = CalculatorTS.TarifPlanId;
                                }
                                DateExitEstimated = CalculatorTS.DateExitEstimated;
                                LastCalcTime = DateTime.Now;
                            }
                            else
                            {
                                ToLog(2, "Cashier: CalcCardInfo: End Calculate Not Success");
                                lCardInfo = CalculatorTS.DateCards;
                                CardSum = lCardInfo.SumOnCard;
                                lAmountDue = 0;                          // не смог посчитать, пропущу клиента 
                                LastCalcTime = DateTime.Now;
                            }
                        }
                        else
                        {
                            LastCalcTime = DateTime.Now;
                        }
                        AmountDue = lAmountDue;
                    }
                    catch
                    {
                        AmountDue = 0;
                    }
                    CardQuery = AmountDue - CardSum;
                    if (CardQuery < 0)
                        CardQuery = 0;
                    Transaction.PurchaseSum = -CardQuery;

                    CardBalancePos = (CardQuery <= 0) ? true : false;

                    LogCardCalculated();

                    ProgramStatus = SetProgrammStatus(ProgramStatus, lCardInfo);
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }

            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return ProgramStatus;
        }   

        private string Serialize(ZoneModelT t)
        {
            try
            {
                string s = "{";
                s += "\"Id\":\"" + t.Id.ToString() + "\"";
                s += ", \"IdFC\":" + t.IdFC;
                s += ", \"Name\":\"" + t.Name + "\"";
                s += "}";
                return s;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return "";
            }
        }
        private string Serialize(TariffsT t)
        {
            try
            {
                string s = "{";
                s += "\"Id\":\"" + t.Id.ToString() + "\"";
                s += ", \"IdFC\":" + t.IdFC;
                s += ", \"Name\":\"" + t.Name + "\"";
                s += "}";
                return s;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return "";
            }
        }
        private string Serialize(TariffScheduleModelT t)
        {
            //ToLog(2, "Cashier: Serialize(TariffScheduleModelT t)");
            try
            {
                string s = "{";
                s += "\"Id\":\"" + t.Id.ToString() + "\"";
                s += ", \"IdFC\":" + t.IdFC;
                s += ", \"Name\":\"" + t.Name + "\"";
                s += "}";
                return s;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return "";
            }
        }

        string SetResult(string result, CardReaderResponse ProgramStatus)
        {
            //ToLog(2, "Cashier: SetResult");
            try
            {
                result += "\"Status\" : \"" + ProgramStatus.Status.ToString() + "\",";
                result += "\"Now\" : \"" + ProgramStatus.Now + "\"";

                if (ProgramStatus.CurrentStatus != null)
                    result += ", \"CurrentStatus\" : \"" + ProgramStatus.CurrentStatus.ToString() + "\"";
                if ((ProgramStatus.ErrorText != null) && (ProgramStatus.ErrorText != ""))
                    result += ", \"ErrorText\":\"" + ProgramStatus.ErrorText + "\"";
                if ((ProgramStatus.CardId != null) && (ProgramStatus.CardId != ""))
                {
                    result += ", \"CardId\":\"" + ProgramStatus.CardId + "\"";
                    if (ProgramStatus.IsDababaseExists != null)
                        result += ", \"IsDababaseExists\":" + "\"" + ProgramStatus.IsDababaseExists.ToString() + "\"";
                    if ((ProgramStatus.ParkingEnterTime != null) && (ProgramStatus.ParkingEnterTime != ""))
                        result += ", \"ParkingEnterTime\":\"" + ProgramStatus.ParkingEnterTime + "\"";
                    if ((ProgramStatus.LastRecountTime != null) && (ProgramStatus.LastRecountTime != ""))
                        result += ", \"LastRecountTime\":\"" + ProgramStatus.LastRecountTime + "\"";
                    if (ProgramStatus.SumOnCard != null)
                        result += ", \"SumOnCard\":" + ProgramStatus.SumOnCard.ToString();
                    if (ProgramStatus.TSidFC != null)
                        result += ", \"TSidFC\":" + ProgramStatus.TSidFC.ToString();
                    if (ProgramStatus.TPidFC != null)
                        result += ", \"TPidFC\":" + ProgramStatus.TPidFC.ToString();
                    if (ProgramStatus.ZoneidFC != null)
                        result += ", \"ZoneidFC\":" + ProgramStatus.ZoneidFC.ToString();
                    if (ProgramStatus.ClientGroupidFC != null)
                        result += ", \"ClientGroupidFC\":" + ProgramStatus.ClientGroupidFC.ToString();
                    if ((ProgramStatus.LastPaymentTime != null) && (ProgramStatus.LastPaymentTime != ""))
                        result += ", \"LastPaymentTime\":\"" + ProgramStatus.LastPaymentTime + "\"";
                    if ((ProgramStatus.Nulltime1 != null) && (ProgramStatus.Nulltime1 != ""))
                        result += ", \"Nulltime1\":\"" + ProgramStatus.Nulltime1 + "\"";
                    if ((ProgramStatus.Nulltime2 != null) && (ProgramStatus.Nulltime2 != ""))
                        result += ", \"Nulltime2\":\"" + ProgramStatus.Nulltime2 + "\"";
                    if ((ProgramStatus.Nulltime3 != null) && (ProgramStatus.Nulltime3 != ""))
                        result += ", \"Nulltime3\":\"" + ProgramStatus.Nulltime3 + "\"";
                    if ((ProgramStatus.TVP != null) && (ProgramStatus.TVP != ""))
                        result += ", \"TVP\":\"" + ProgramStatus.TVP + "\"";
                    if (ProgramStatus.TKVP != null)
                        result += ", \"TKVP\":" + ProgramStatus.TKVP.ToString();
                    if (ProgramStatus.ClientTypidFC != null)
                        result += ", \"ClientTypidFC\":" + ProgramStatus.ClientTypidFC.ToString();
                    if (ProgramStatus.AmountDue != null)
                        result += ", \"AmountDue\":" + "\"" + ProgramStatus.AmountDue.ToString() + "\"";
                    if ((ProgramStatus.DateSaveCard != null) && (ProgramStatus.DateSaveCard != ""))
                        result += ", \"DateSaveCard\":\"" + ProgramStatus.DateSaveCard + "\"";
                    if (ProgramStatus.Tariff != null)
                        result += ", \"Tariff\":" + Serialize(ProgramStatus.Tariff);
                    if (ProgramStatus.TariffShedule != null)
                        result += ", \"TariffShedule\":" + Serialize(ProgramStatus.TariffShedule);
                    if (ProgramStatus.Zone != null)
                        result += ", \"Zone\":" + Serialize(ProgramStatus.Zone);

                    if ((ProgramStatus.Abonements != null) && (ProgramStatus.Abonements.Count > 0))
                    {
                        result += ", \"Abonements\":";
                        string ab = JsonConvert.SerializeObject(ProgramStatus.Abonements);
                        result += ab;
                    }
                }
                else
                {
                    if (ProgramStatus.AmountDue != null)
                        result += ", \"AmountDue\":" + "\"" + ProgramStatus.AmountDue.ToString() + "\"";
                }
                if (ProgramStatus.IsAuthenticated != null)
                    result += ", \"IsAuthenticated\" : " + "\"" + ProgramStatus.IsAuthenticated.ToString() + "\"";

                if (ProgramStatus.ReaderList != null)
                {
                    result += ", \"ReaderList\" : [";
                    for (int i = 0; i < ProgramStatus.ReaderList.Count; i++)
                        if (i == 0)
                            result += "{ \"ReaderName\" : \"" + ProgramStatus.ReaderList[i] + "\"}";
                        else
                            result += ", { \"ReaderName\" : \"" + ProgramStatus.ReaderList[i] + "\"}";
                    result += "]";
                }
                if (ProgramStatus.ShiftOpened != null && ProgramStatus.ShiftOpened != "")
                    result += ", \"ShiftOpened\":\"" + ProgramStatus.ShiftOpened + "\"";

                if (ProgramStatus.TerminalStatus != null && ProgramStatus.TerminalStatus != "")
                    result += ", \"TerminalStatus\":\"" + ProgramStatus.TerminalStatus + "\"";

                return result;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return "";
            }
        }

        public EventWebServer.HttpResp SendResponse(HttpListenerRequest request)
        {
            ToTariffPlan.DateCard lCardInfo;
            lCardInfo = CardInfo;


            //ToLog(2, "Cashier: SendResponse");
            EventWebServer.HttpResp Res;
            CardReaderResponse ProgramStatus = new CardReaderResponse() { Abonements = new List<AbonementT>(), Status = WebAnswerT.Succsess },
                ProgramStatus1 = new CardReaderResponse() { Abonements = new List<AbonementT>(), Status = WebAnswerT.Succsess };

            try
            {
                string result = "";
                string T;
                string Req;

                DateTime DT;
                TimeSpan _TimeIn;

                Res.StatusCode = HttpStatusCode.OK;
                string RawUrl = request.RawUrl.Substring(request.RawUrl.IndexOf("/")).Trim(new char[] { '/' });
                int ParamIndex = RawUrl.IndexOf("?");

                if (ParamIndex >= 0)
                    Req = RawUrl.Substring(0, ParamIndex);
                else
                    Req = RawUrl;
                Req = Req.ToLower();

                ProgramStatus1.Now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ProgramStatus.Now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                if ((Req == "GetData".ToLower()) || (Req == "WriteData".ToLower())
                     || (Req == "ToAbonement".ToLower()) || (Req == "Payment".ToLower())
                     || (Req == "CheckCard".ToLower()) || (Req == "CancelCard".ToLower()))
                #region CardReaderExists
                {
                    lock (ExternalDeviceTimerLock)
                    {
                        // Запросы требующие наличия картд-ридера
                        if (CardReader != null)
                        {
                            if (!CardReader.CardExists && !(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked && ReaderIDCardHex != ""))
                            {
                                //ToLog(2, "Cashier: SendResponse: CardNotExists");
                                ProgramStatus.Status = WebAnswerT.TemporaryError;
                                ProgramStatus.ErrorText = "Card not exists";
                                Res.Response = "Card not exists";
                                ProgramStatus.CurrentStatus = HardReaderStatusT.CardNotExists;
                            }
                            else
                            {
                                if ((Req == "CheckCard".ToLower()) || (Req == "CheckReaderStatus".ToLower()))
                                #region CheckCard
                                {
                                    //ToLog(2, "Cashier: SendResponse: CheckCard: CardExists");
                                    ProgramStatus.ErrorText = "Card exists";
                                    Res.Response = "Card exists";
                                    ProgramStatus.CardId = CardReader.CardId;
                                    ProgramStatus.CurrentStatus = HardReaderStatusT.CardExists;
                                }
                                #endregion
                                else if (Req == "GetData".ToLower())
                                #region GetData
                                {
                                    if (CardReader.CardExists && CardReader.CardReaded && (CashierType != 2) || !(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                    {
                                        lock (db_lock)
                                        {
                                            CardInfo = CardReader.CardInfo;
                                            ReaderIDCardHex = CardReader.CardId;
                                            ProgramStatus = CalcCardInfo(ProgramStatus);
                                            ProgramStatus.CurrentStatus = HardReaderStatusT.CardReaded;
                                        }
                                    }
                                    else
                                    {
                                        ProgramStatus.Status = WebAnswerT.TemporaryError;
                                        ProgramStatus.ErrorText = "Card not exists";
                                        Res.Response = "Card not exists";
                                        ProgramStatus.CurrentStatus = HardReaderStatusT.CardNotExists;
                                    }
                                }
                                #endregion
                                else if (Req == "WriteData".ToLower())
                                #region WriteData
                                {
                                    CardReaderStatusChanged = true;
                                    lock (db_lock)
                                    {
                                        if ((CashierType != 2) || !(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                        {
                                            lCardInfo = CardReader.CardInfo;
                                            ReaderIDCardHex = CardReader.CardId;
                                        }
                                        //ToLog(2, "Cashier: SendResponse: WriteData");
                                        ProgramStatus1 = CalcCardInfo(ProgramStatus1, lCardInfo);

                                        if (request.QueryString.AllKeys.Contains("CardId"))
                                        {
                                            if ((CashierType != 2) || !(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                            {

                                                T = request.QueryString["CardId"];
                                                if (CardReader.CardId == T)
                                                {
                                                    if (request.QueryString.AllKeys.Contains("ClientGroupidFC"))
                                                    {
                                                        T = request.QueryString["ClientGroupidFC"];
                                                        lCardInfo.ClientGroupidFC = Convert.ToByte(T); // Группа клиента
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("ClientTypidFC"))
                                                    {
                                                        T = request.QueryString["ClientTypidFC"];
                                                        lCardInfo.ClientTypidFC = Convert.ToByte(T); // Тип клиента
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("LastPaymentTime"))
                                                    {
                                                        T = request.QueryString["LastPaymentTime"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.LastPaymentTime = (int)_TimeIn.TotalSeconds; // Время последней оплаты
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("LastRecountTime"))
                                                    {
                                                        T = request.QueryString["LastRecountTime"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.LastRecountTime = (int)_TimeIn.TotalSeconds; // Время последнего пересчета
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("Nulltime1"))
                                                    {
                                                        T = request.QueryString["Nulltime1"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.Nulltime1 = (int)_TimeIn.TotalSeconds;
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("Nulltime2"))
                                                    {
                                                        T = request.QueryString["Nulltime2"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.Nulltime2 = (int)_TimeIn.TotalSeconds;
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("Nulltime3"))
                                                    {
                                                        T = request.QueryString["Nulltime3"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.Nulltime3 = (int)_TimeIn.TotalSeconds; // Время окончания абонемента
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("ParkingEnterTime"))
                                                    {
                                                        T = request.QueryString["ParkingEnterTime"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.ParkingEnterTime = (int)_TimeIn.TotalSeconds; // Время въезда
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("SumOnCard"))
                                                    {
                                                        T = request.QueryString["SumOnCard"];
                                                        lCardInfo.SumOnCard = Convert.ToInt32(T); // Сумма на карте
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("TKVP"))
                                                    {
                                                        T = request.QueryString["TKVP"];
                                                        lCardInfo.TKVP = Convert.ToByte(T);
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("TPidFC"))
                                                    {
                                                        T = request.QueryString["TPidFC"];
                                                        lCardInfo.TPidFC = Convert.ToByte(T); // Код тарифа
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("TSidFC"))
                                                    {
                                                        T = request.QueryString["TSidFC"];
                                                        lCardInfo.TSidFC = Convert.ToByte(T); // Код тарифной сетки
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("TVP"))
                                                    {
                                                        T = request.QueryString["TVP"];
                                                        DateTime.TryParseExact(T, "yyyy-MM-dd H:mm:ss", null, DateTimeStyles.None, out DT);
                                                        _TimeIn = DT - datetime0; // 4 байта
                                                        lCardInfo.TVP = (int)_TimeIn.TotalSeconds;
                                                    }
                                                    if (request.QueryString.AllKeys.Contains("ZoneidFC"))
                                                    {
                                                        T = request.QueryString["ZoneidFC"];
                                                        lCardInfo.ZoneidFC = Convert.ToByte(T); // Код зоны
                                                    }
                                                    lCardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // Время сохранения данных

                                                    LogWriteCard();
                                                    WriteCard(lCardInfo);

                                                    if (CardReader.SoftStatus == SoftReaderStatusT.CardReaded)
                                                    {
                                                        CardInfo = CardReader.CardInfo;
                                                        lCardInfo = CardReader.CardInfo;
                                                        ReaderIDCardHex = CardReader.CardId;
                                                        ProgramStatus1.CurrentStatus = HardReaderStatusT.CardWrited;
                                                        ProgramStatus = CalcCardInfo(ProgramStatus1);
                                                        terminalStatus = TerminalStatusT.CardInserted;
                                                        if (terminalStatusOld != terminalStatus)
                                                            ToLog(2, "Cashier: SendResponse1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                        terminalStatusOld = terminalStatus;
                                                    }
                                                    else
                                                    {
                                                        ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                        ProgramStatus.ErrorText = "WriteError";
                                                        Res.Response = "WriteError";
                                                        ProgramStatus.CurrentStatus = CardReader.Status;
                                                    }
                                                }
                                                else
                                                {
                                                    ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                    ProgramStatus.ErrorText = "Incorrect CardId";
                                                    Res.Response = "Incorrect CardId";
                                                    Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                    ProgramStatus.CurrentStatus = HardReaderStatusT.SomeoneElsesCard;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ProgramStatus.Status = WebAnswerT.TemporaryError;
                                            ProgramStatus.ErrorText = "Incorrect CardId";
                                            Res.Response = "Incorrect CardId";
                                            Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                            ProgramStatus.CurrentStatus = HardReaderStatusT.SomeoneElsesCard;
                                        }
                                    }
                                }
                                #endregion
                                else if (Req == "ToAbonement".ToLower()) // ManualCashier Only
                                #region ToAbonement
                                {
                                    CardReaderStatusChanged = true;
                                    if (CashierType == 2)
                                    {
                                        lock (db_lock)
                                        {
                                            CardReaderStatusChanged = true;
                                            if (!(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                            {
                                                lCardInfo = CardReader.CardInfo;
                                                ReaderIDCardHex = CardReader.CardId;
                                            }
                                            ProgramStatus1 = CalcCardInfo(ProgramStatus1, lCardInfo);

                                            if (request.QueryString.AllKeys.Contains("CardId"))
                                            {
                                                T = request.QueryString["CardId"];
                                                if ((CardReader.CardId == T) || (CashierType == 2 && EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                                {
                                                    if (request.QueryString.AllKeys.Contains("AbonementId"))
                                                    {
                                                        T = request.QueryString["AbonementId"];
                                                        ToAbonement = new Guid(T); // ID абонемента
                                                    }
                                                    if (ToAbonement != null)
                                                    {
                                                        if (transactions == null)
                                                            transactions = new TransactionsT { DeviceId = DeviceId, DeviceTypeID = device.Type };
                                                        TransactionClear();
                                                        PayTransactionExported = false;
                                                        transactions.Cardnumber = ReaderIDCardDec;
                                                        transactions.TimeEntry = datetime0 + TimeSpan.FromSeconds(lCardInfo.ParkingEnterTime);
                                                        transactions.Nulltime1 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime1);
                                                        transactions.Nulltime2 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime2);
                                                        transactions.TVP = datetime0 + TimeSpan.FromSeconds(lCardInfo.TVP);
                                                        transactions.TKVP = lCardInfo.TKVP;
                                                        transactions.SumOnCardBefore = lCardInfo.SumOnCard;

                                                        lCardInfo = CardToAbonement(lCardInfo, ToAbonement); // Смена тарифа + времени окончания абонемента

                                                        lCardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // Время въезда
                                                        transactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime3);
                                                        transactions.ClientTypidFC = lCardInfo.ClientTypidFC;
                                                        string req = "select * from TariffScheduleModel where IdFC=" + lCardInfo.TSidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                                                        DataRow _TarifiTS_ = null;
                                                        lock (db_lock)
                                                        {
                                                            try
                                                            {
                                                                _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                            }
                                                            catch (Exception exc)
                                                            {
                                                                ToLog(0, exc.ToString());
                                                                try
                                                                {
                                                                    _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                                }
                                                                catch (Exception exc1)
                                                                {
                                                                    ToLog(0, exc1.ToString());
                                                                }
                                                            }
                                                        }
                                                        if (_TarifiTS_ != null)
                                                            TarifiTS = _TarifiTS_.GetGuid("Id");
                                                        transactions.TariffScheduleId = TarifiTS;
                                                        req = "select * from Tariffs where IdFC=" + lCardInfo.TPidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                                                        DataRow _TarifiTP_ = null;
                                                        lock (db_lock)
                                                        {
                                                            try
                                                            {
                                                                _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                            }
                                                            catch (Exception exc)
                                                            {
                                                                ToLog(0, exc.ToString());
                                                                try
                                                                {
                                                                    _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                                }
                                                                catch (Exception exc1)
                                                                {
                                                                    ToLog(0, exc1.ToString());
                                                                }
                                                            }
                                                        }
                                                        if (_TarifiTP_ != null)
                                                            TarifiTP = _TarifiTP_.GetGuid("Id");
                                                        transactions.TarifPlanId = TarifiTP;

                                                        transactions.SumAccepted = 0;
                                                        transactions.ChangeIssued = 0;

                                                        transactions.SumOnCardAfter = lCardInfo.SumOnCard;

                                                        LogWriteCard();
                                                        if (!(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                                        {
                                                            WriteCard(lCardInfo);

                                                            if (CardReader.SoftStatus == SoftReaderStatusT.CardReaded)
                                                            {
                                                                CardInfo = CardReader.CardInfo;
                                                                lCardInfo = CardReader.CardInfo;
                                                                ReaderIDCardHex = CardReader.CardId;
                                                                ProgramStatus = CalcCardInfo(ProgramStatus1);
                                                                SendPayTransaction(TransactionTypeT.ChangeToAbonement);
                                                                //SendCardTransaction выполняется в WriteCard
                                                                CardReader.CardReaded = false;
                                                                terminalStatus = TerminalStatusT.CardInserted;
                                                                if (terminalStatusOld != terminalStatus)
                                                                    ToLog(2, "Cashier: SendResponse2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                                terminalStatusOld = terminalStatus;
                                                                ProgramStatus.CurrentStatus = HardReaderStatusT.CardWrited;
                                                            }
                                                            else
                                                            {
                                                                ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                                ProgramStatus.CurrentStatus = CardReader.Status;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            SendCardTransaction();

                                                            ProgramStatus1.CurrentStatus = HardReaderStatusT.CardWrited;
                                                            ProgramStatus = CalcCardInfo(ProgramStatus1, lCardInfo);
                                                        }

                                                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.ToAbonement;
                                                        CurrentTransactionType = TransactionTypeT.ChangeToAbonement;
                                                        SendPayTransaction(CurrentTransactionType);

                                                        ToAbonement = null;
                                                    }
                                                    else
                                                    {
                                                        ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                        ProgramStatus.ErrorText = "Unable change to Abonement";
                                                        Res.Response = "Unable change to Abonement";
                                                        Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                    }
                                                }
                                                else
                                                {
                                                    ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                    ProgramStatus.ErrorText = "Incorrect CardId";
                                                    Res.Response = "Incorrect CardId";
                                                    ProgramStatus.CurrentStatus = HardReaderStatusT.SomeoneElsesCard;
                                                    Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                }
                                            }
                                            else
                                            {
                                                ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                ProgramStatus.ErrorText = "Incorrect CardId";
                                                Res.Response = "Incorrect CardId";
                                                Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                                        Res.StatusCode = HttpStatusCode.NotImplemented;
                                        Res.Response = "Function not implemented";
                                        return Res;
                                    }
                                }
                                #endregion
                                else if (Req == "Payment".ToLower()) // ManualCashier Only
                                #region Payment
                                {
                                    CardReaderStatusChanged = true;
                                    if (CashierType == 2)
                                    {
                                        if (ShiftOpened && (terminalStatus != TerminalStatusT.ShiftSwitchNeed) && (DateTime.Now < (LastShiftOpenTime + new TimeSpan(23, 59, 50))))
                                        {
                                            lock (db_lock)
                                            {
                                                decimal Paid = 0, Delivery = 0;

                                                if (!(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                                {
                                                    lCardInfo = CardReader.CardInfo;
                                                    ReaderIDCardHex = CardReader.CardId;
                                                }
                                                ProgramStatus1 = CalcCardInfo(ProgramStatus1, lCardInfo);

                                                if (request.QueryString.AllKeys.Contains("CardId"))
                                                {
                                                    T = request.QueryString["CardId"];
                                                    if ((CardReader.CardId == T) || (CashierType == 2 && EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                                    {
                                                        if (request.QueryString.AllKeys.Contains("Paid"))
                                                        {
                                                            T = request.QueryString["Paid"];
                                                            Paid = Convert.ToDecimal(T); // Сумма оплаты
                                                        }
                                                        if (request.QueryString.AllKeys.Contains("Delivery"))
                                                        {
                                                            T = request.QueryString["Delivery"];
                                                            Delivery = Convert.ToDecimal(T); // Сумма сдачи
                                                        }

                                                        if (transactions == null)
                                                            transactions = new TransactionsT { DeviceId = DeviceId, DeviceTypeID = device.Type };
                                                        TransactionClear();
                                                        PayTransactionExported = false;

                                                        if (Paid > 0)
                                                        {
                                                            if (terminalStatus == TerminalStatusT.PenalCardDisplay ||
                                                                terminalStatus == TerminalStatusT.PenalCardPaiment ||
                                                                terminalStatus == TerminalStatusT.PenalCardPrePaiment
                                                               )
                                                            {

                                                                lCardInfo = PenalCardRequest(lCardInfo);
                                                                ReaderIDCardHex = CardReader.CardId;
                                                                ProgramStatus1 = CalcCardInfo(ProgramStatus1, lCardInfo);
                                                            }
                                                            transactions.Cardnumber = ReaderIDCardDec;

                                                            transactions.TimeEntry = datetime0 + TimeSpan.FromSeconds(lCardInfo.ParkingEnterTime);
                                                            transactions.Nulltime1 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime1);
                                                            transactions.Nulltime2 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime2);
                                                            transactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(lCardInfo.Nulltime3);
                                                            transactions.TVP = datetime0 + TimeSpan.FromSeconds(lCardInfo.TVP);
                                                            transactions.TKVP = lCardInfo.TKVP;
                                                            transactions.SumOnCardBefore = lCardInfo.SumOnCard;
                                                            transactions.ClientTypidFC = lCardInfo.ClientTypidFC;
                                                            transactions.TariffScheduleId = TarifiTS;

                                                            lCardInfo.SumOnCard = lCardInfo.SumOnCard + (int)(Paid - Delivery);
                                                            transactions.SumAccepted = Paid;
                                                            transactions.ChangeIssued = Delivery;
                                                            transactions.SumOnCardAfter = lCardInfo.SumOnCard;
                                                            lCardInfo.LastPaymentTime = (int)(DateTime.Now - datetime0).TotalSeconds;
                                                            lCardInfo.DateSaveCard = (int)(DateTime.Now - datetime0).TotalSeconds; // Время въезда

                                                            DevicePaiments = PaimentMethodT.PaimentsCash;

                                                            if (Paiments != DevicePaiments)
                                                                ToLog(2, "Cashier: SendResponse: Paiments changed from " + Paiments.ToString() + " to " + DevicePaiments.ToString());
                                                            Paiments = DevicePaiments;
                                                            GiveCheque(0, Paid, Paid - Delivery, Paiments, 0, null, BankSlip);

                                                            LogWriteCard();
                                                            if (!(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                                                            {
                                                                WriteCard(lCardInfo);

                                                                if (CardReader.SoftStatus == SoftReaderStatusT.CardReaded)
                                                                {
                                                                    CardInfo = CardReader.CardInfo;
                                                                    lCardInfo = CardReader.CardInfo;
                                                                    ReaderIDCardHex = CardReader.CardId;
                                                                    ProgramStatus1.CurrentStatus = HardReaderStatusT.CardWrited;
                                                                    ProgramStatus = CalcCardInfo(ProgramStatus1);
                                                                    CardReader.SoftStatus = SoftReaderStatusT.Unknown;
                                                                    //SendCardTransaction выполняется в WriteCard
                                                                    CardReader.CardReaded = false;
                                                                    terminalStatus = TerminalStatusT.CardInserted;
                                                                    if (terminalStatusOld != terminalStatus)
                                                                        ToLog(2, "Cashier: SendResponse3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                                                    terminalStatusOld = terminalStatus;
                                                                }
                                                                else
                                                                {
                                                                    ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                                    ProgramStatus.CurrentStatus = CardReader.Status;
                                                                }
                                                                lCardInfo = CardReader.CardInfo;
                                                                ReaderIDCardHex = CardReader.CardId;
                                                            }
                                                            else
                                                            {
                                                                SendCardTransaction();
                                                                ProgramStatus1.CurrentStatus = HardReaderStatusT.CardWrited;
                                                                ProgramStatus = CalcCardInfo(ProgramStatus1, lCardInfo);
                                                            }

                                                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.SuccessPaymentCash;
                                                            CurrentTransactionType = TransactionTypeT.Payment_aprowed;

                                                            SendPayTransaction(CurrentTransactionType);
                                                        }
                                                        else
                                                        {
                                                            ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                            ProgramStatus.ErrorText = "Unable Payment. Paid is 0.";
                                                            Res.Response = "Unable Payment. Paid is 0.";
                                                            Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                        ProgramStatus.ErrorText = "Incorrect CardId";
                                                        Res.Response = "Incorrect CardId";
                                                        Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                        ProgramStatus.CurrentStatus = HardReaderStatusT.SomeoneElsesCard;
                                                    }
                                                }
                                                else
                                                {
                                                    ProgramStatus.Status = WebAnswerT.TemporaryError;
                                                    ProgramStatus.ErrorText = "Incorrect CardId";
                                                    Res.Response = "Incorrect CardId";
                                                    Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ProgramStatus.Status = WebAnswerT.TemporaryError;
                                            if (!ShiftOpened)
                                            {
                                                ProgramStatus.ErrorText = "Payment is impossible. Shift closed";
                                                Res.Response = "Payment is impossible. Shift closed";
                                            }
                                            else
                                            {
                                                ProgramStatus.ErrorText = "Payment is impossible. Max shift duration is exceeded.";
                                                Res.Response = "Payment is impossible. Max shift duration is exceeded.";
                                            }
                                            Res.StatusCode = HttpStatusCode.PreconditionFailed;
                                        }
                                    }
                                    else
                                    {
                                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                                        Res.StatusCode = HttpStatusCode.NotImplemented;
                                        Res.Response = "Function not implemented";
                                        return Res;
                                    }
                                }
                                #endregion
                                else if (Req == "CancelCard".ToLower()) // ManualCashier Only
                                #region CancelCard
                                {
                                    CardReaderStatusChanged = true;
                                    if (CashierType == 2)
                                    {
                                        log.EndContinues(299, DateTime.Now, null);
                                        if (terminalStatus != TerminalStatusT.Empty)
                                            terminalStatus = TerminalStatusT.Empty;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: SendResponse4: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                        terminalStatusChangeInternal = true;
                                    }
                                    else
                                    {
                                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                                        Res.StatusCode = HttpStatusCode.NotImplemented;
                                        Res.Response = "Function not implemented";
                                        return Res;
                                    }
                                }
                            }

                            if ((CashierType != 2) && !(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                            {
                                //ToLog(2, "Cashier: SendResponse: CardReader.SoftStatus: " + CardReader.SoftStatus.ToString());
                                switch (CardReader.SoftStatus)
                                {
                                    //case SoftReaderStatusT.ReaderNotExists:
                                    //case SoftReaderStatusT.CardIDNotExists:
                                    case SoftReaderStatusT.Exception:
                                    case SoftReaderStatusT.ReadError:
                                    case SoftReaderStatusT.WriteError:
                                        ProgramStatus.ErrorText = CardReader.StatusStr;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            Res.Response = "Card Reader not exists";
                            Res.StatusCode = HttpStatusCode.PreconditionFailed;
                            ProgramStatus.Status = WebAnswerT.TemporaryError;
                            ProgramStatus.ErrorText = "Card Reader not exists";
                        }
                        #endregion
                    }
                }
                #endregion
                else if (Req == "cancelPenalCardRequest".ToLower())// ManualCashier Only
                #region CancelPenalCardRequest
                {
                    CardReaderStatusChanged = true;
                    if (CashierType == 2)
                    {
                        lock (db_lock)
                        {
                            terminalStatus = TerminalStatusT.Empty;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: SendResponse5: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                            terminalStatusChangeInternal = true;
                            ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                else if (Req == "PenalCardRequest".ToLower())// ManualCashier Only
                #region PenalCardRequest
                {
                    if (CashierType == 2)
                    {
                        lock (db_lock)
                        {
                            lCardInfo = PenalCardRequest(lCardInfo);
                            ProgramStatus = CalcCardInfo(ProgramStatus, lCardInfo);
                            ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                else if (Req == "ShiftStatus".ToLower())
                #region ShiftStatus
                {
                    lock (ShiftLock)
                    {
                        //ToLog(2, "Cashier: SendResponse: ShiftStatus");
                        if (ShiftWork)
                        {
                            ProgramStatus.ShiftOpened = ShiftOpened.ToString();
                        }
                        else
                        {
                            ProgramStatus.ShiftOpened = "Unknown";
                        }
                    }
                }
                #endregion
                else if (Req == "OpenShift".ToLower())// ManualCashier Only
                #region OpenShift
                {
                    if (CashierType == 2)
                    {
                        lock (ShiftLock)
                        {
                            //ToLog(2, "Cashier: SendResponse: OpenShift");
                            if (ShiftWork)
                            {
                                if (!ShiftOpened)
                                {
                                    ShiftOpen(false);
                                }
                                ProgramStatus.ShiftOpened = ShiftOpened.ToString();
                            }
                            else
                            {
                                ProgramStatus.ShiftOpened = "Unknown";
                            }
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                else if (Req == "CloseShift".ToLower())// ManualCashier Only
                #region CloseShift
                {
                    if (CashierType == 2)
                    {
                        lock (ShiftLock)
                        {
                            //ToLog(2, "Cashier: SendResponse: CloseShift");
                            if (ShiftWork)
                            {
                                if (ShiftOpened)
                                {
                                    ShiftClose(false);
                                }
                                ProgramStatus.ShiftOpened = ShiftOpened.ToString();
                            }
                            else
                            {
                                ProgramStatus.ShiftOpened = "Unknown";
                            }
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                else if (Req == "PrintXReport".ToLower())// ManualCashier Only
                #region PrintXReport
                {
                    if (CashierType == 2)
                    {
                        lock (ShiftLock)
                        {
                            //ToLog(2, "Cashier: SendResponse: PrintXReport");
                            if (ShiftWork)
                            {
                                PrintXReport();
                            }
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                //else if (Req == "PrintZReport".ToLower())// ManualCashier Only
                #region PrintZReport
                /*
{
    lock (ShiftLock)
    {
        ToLog(2, "Cashier: SendResponse: PrintZReport");
        if (ShiftWork && !ShiftOpened)
        {
            int Number = 0;
            if (request.QueryString.AllKeys.Contains("Number"))
            {
                T = request.QueryString["Number"];
                Number = Convert.ToUInt16(T);
            }
            PrintZReport(Number);
        }
        else
        {
            ProgramStatus.Status = WebAnswerT.TemporaryError;
            Res.StatusCode = HttpStatusCode.PreconditionFailed;
        }
    }
}*/
                #endregion
                else if (Req == "CallStatus".ToLower())
                #region CallStatus
                {
                    //ToLog(2, "Cashier: SendResponse: CallStatus");
                    if (request.QueryString.AllKeys.Contains("status"))
                    {
                        T = request.QueryString["status"];
                        if (T == "Answer")
                        {
                            if (Talk != null)
                                Talk.Answer();
                        }
                        else if (T == "HangUp")
                        {
                            if (Talk != null)
                                Talk.HangUp();
                        }
                    }
                }
                #endregion
                //else if (Req == "Reset".ToLower())
                #region Reset
                /*
            {
                ToLog(2, "Cashier: SendResponse: Reset");
                if (TechForm.Visibility != Visibility.Visible)
                {
                    if (deviceCMModel != null)
                    {
                        TechForm.SetCashAcceptorNominals();
                        TechForm.SetCoinAcceptorNominals();
                        TerminalStatusChanged = true;
                    }
                }
                else
                {
                    ProgramStatus.Status = WebAnswerT.TemporaryError;
                    Res.StatusCode = HttpStatusCode.PreconditionFailed;
                }
            }*/
                #endregion
                else if (Req == "ReSync".ToLower())
                #region ReSync
                {
                    lock (ReSyncLock)
                    {
                        //ToLog(2, "Cashier: SendResponse: ReSync");
                        if (terminalStatus == TerminalStatusT.Empty)
                        {
                            ReSync();
                            //ReSyncReq = true;
                        }
                        else
                        {
                            ProgramStatus.Status = WebAnswerT.TemporaryError;
                            Res.StatusCode = HttpStatusCode.PreconditionFailed;
                        }
                        ProgramStatus.Now = DateTime.Now.ToString("yyyy\\/MM\\/dd HH:mm:ss");
                        ProgramStatus.TerminalStatus = terminalStatus.ToString();
                    }
                }
                #endregion
                /*else if (Req == "Update".ToLower())
                #region Update
                {
                    ToLog(2, "Cashier: SendResponse: Update");
                    UpdateNeeded = true;
                }
                #endregion*/
                else if (Req == "GetReaderNames".ToLower())
                #region GetReaderNames
                {
                    lock (ReaderLock)
                    {
                        if (!(EmulatorEnabled && EmulatorForm.Emulator.CardReaderVirt.Checked))
                        {
                            if (CardReader.GetReaderNames())
                            {
                                ProgramStatus.ReaderList = CardReader.ReaderList;
                                ProgramStatus.Status = WebAnswerT.Succsess;
                            }
                            else
                            {
                                ProgramStatus.ReaderList = null;
                                ProgramStatus.Status = WebAnswerT.TemporaryError;
                            }
                        }
                        else
                        {
                            ProgramStatus.ReaderList = new List<string> { "Test1", "Test2" };
                            ProgramStatus.Status = WebAnswerT.Succsess;
                        }
                    }
                }
                #endregion
                else if (Req == "SetReaderName".ToLower())
                #region SetReaderName
                {
                    lock (ReaderLock)
                    {
                        if (request.QueryString.AllKeys.Contains("ReaderName"))
                        {
                            CardReader.ReaderName = request.QueryString["ReaderName"];
                            currRegistryKey.SetValue("ReaderName", request.QueryString["ReaderName"]);
                            currRegistryKey.Flush();
                        }
                        CardReader.Close();

                        if (CardReader.IsOpen)
                        {
                            if (CardReader.CardExists && (CardReader.Status == HardReaderStatusT.CardIDExists))
                                ProgramStatus.CardId = CardReader.CardId;
                        }
                        ProgramStatus.Status = WebAnswerT.Succsess;
                    }
                }
                #endregion
                else if (Req == "GetTerminalStatus".ToLower())
                #region GetTerminalStatus
                {
                    lock (ReaderLock)
                    {
                        ProgramStatus.Status = WebAnswerT.Succsess;
                        ProgramStatus.TerminalStatus = terminalStatus.ToString();
                    }
                }
                #endregion
                else if (Req == "ToggleTechForm".ToLower())// ManualCashier Only
                #region ToggleTechForm
                {
                    if (CashierType == 2)
                    {
                        lock (ShiftLock)
                        {
                            //ToLog(2, "Cashier: SendResponse: ToggleTechForm");
                            ToggleTechForm = !ToggleTechForm;
                            TerminalStatusChanged = true;
                        }
                    }
                    else
                    {
                        //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                        Res.StatusCode = HttpStatusCode.NotImplemented;
                        Res.Response = "Function not implemented";
                        return Res;
                    }
                }
                #endregion
                else
                #region NotImplemented
                {
                    //ToLog(2, "Cashier: SendResponse: NotImplemented: " + RawUrl);
                    Res.StatusCode = HttpStatusCode.NotImplemented;
                    Res.Response = "Function not implemented";
                    return Res;
                }
                #endregion

                result = SetResult(result, ProgramStatus);

                result = request.QueryString["callback"] + "({\"Result\": {" + result + "}})";
                Res.Response = result;
                return Res;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                string result = request.QueryString["callback"] + "({\"Result\": {" + exc.ToString() + "}})";
                Res.Response = result;
                Res.StatusCode = HttpStatusCode.OK;
                return Res;
            }
        }
    }
}
