﻿using CommonClassLibrary;
using System;
using System.Windows;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime CoinHopper1AlarmTime, CoinHopper2AlarmTime;
        void HardHopper1()
        {
            try
            {
                if (GiveDeliveryThread == null)
                {
                    if (CoinHopper == null)
                    {
                        CoinHopper = new Communications.CoinHopper(log);
                    }
                    if (CoinHopper != null && !CoinHopper.IsOpen)
                    {
                        CoinHopper.Open(new byte[] { 1, 2, 3, 4 }, Convert.ToInt16(CoinHopperPort.Substring(3)));
                    }
                    if (CoinHopper != null)
                    {
                        if (!CoinHopper.IsOpen)
                        {
                            ErrorsText += ResourceManager.GetString("TechHopperCheckReq", DefaultCulture) + Environment.NewLine;
                        }
                        else
                        {
                            if (!CoinHopper.HoppertStatus[0].Work)
                            {
                                ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + Environment.NewLine;
                                if (Hopper1Work)
                                    HopperWorkChanged = true;
                                Hopper1Work = false;
                                lock (DeliveryNominals)
                                    FrontBoxEnabled = false;
                                if (TechForm.FrontBox.IsEnabled)
                                    TechForm.FrontBox.IsEnabled = false;
                                if (CoinHopper1Level != AlarmLevelT.Brown)
                                {
                                    if (CoinHopper1AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper1AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            CoinHopper1Level = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperLeftNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper1AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (CoinHopper.HoppertStatus[0].OptFraud ||
                                        CoinHopper.HoppertStatus[0].OptoIdleBlocked ||
                                        CoinHopper.HoppertStatus[0].OptoIdleShort ||
                                        CoinHopper.HoppertStatus[0].OptoPayoutBlocked ||
                                        CoinHopper.HoppertStatus[0].OptoPayoutShort||
                                        CoinHopper.HoppertStatus[0].PowerDownPayout ||
                                        CoinHopper.HoppertStatus[0].PowerFail||
                                        CoinHopper.HoppertStatus[0].ReverseLimit||
                                        CoinHopper.HoppertStatus[0].InductiveCoilFault) {
                                    if (Hopper1Work)
                                        HopperWorkChanged = true;
                                    Hopper1Work = false;
                                    lock (DeliveryNominals)
                                        FrontBoxEnabled = false;
                                    if (TechForm.FrontBox.IsEnabled)
                                        TechForm.FrontBox.IsEnabled = false;

                                    if (CoinHopper.HoppertStatus[0].OptFraud ||
                                        CoinHopper.HoppertStatus[0].OptoIdleBlocked ||
                                        CoinHopper.HoppertStatus[0].OptoIdleShort ||
                                        CoinHopper.HoppertStatus[0].OptoPayoutBlocked ||
                                        CoinHopper.HoppertStatus[0].OptoPayoutShort)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperJam", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper1Level != AlarmLevelT.Brown)
                                        {
                                            if (CoinHopper1AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper1AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                    CoinHopper1Level = AlarmLevelT.Brown;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperLeftJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[0].PowerDownPayout ||
                                        CoinHopper.HoppertStatus[0].PowerFail)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperPowerError", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper1Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper1AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper1AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper1Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperLeftJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper1AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[0].ReverseLimit)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperReverseLimit", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper1Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper1AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper1AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper1Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperLeftJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper1AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[0].InductiveCoilFault)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperInductiveCoilFault", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper1Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper1AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper1AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper1Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperLeftJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper1AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {


                                    if (!Hopper1Work)
                                        HopperWorkChanged = true;
                                    Hopper1Work = true;
                                    lock (DeliveryNominals)
                                        FrontBoxEnabled = true;

                                    if (!TechForm.FrontBox.IsEnabled)
                                        TechForm.FrontBox.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        void HardHopper2()
        {
            //ToLog(2, "Cashier: HardHopper2");
            try
            {
                if (GiveDeliveryThread == null)
                {
                    if (CoinHopper == null)
                    {
                        CoinHopper = new Communications.CoinHopper(log);
                    }
                    if (CoinHopper != null && !CoinHopper.IsOpen)
                    {
                        CoinHopper.Open(new byte[] { 1, 2, 3, 4 }, Convert.ToInt16(CoinHopperPort.Substring(3)));
                    }
                    if (CoinHopper != null)
                    {
                        if (!CoinHopper.IsOpen)
                        {
                            ErrorsText += ResourceManager.GetString("TechHopperCheckReq", DefaultCulture) + Environment.NewLine;
                        }
                        else
                        {
                            if (!CoinHopper.HoppertStatus[1].Work)
                            {
                                ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + Environment.NewLine;
                                if (Hopper2Work)
                                    HopperWorkChanged = true;
                                Hopper2Work = false;
                                lock (DeliveryNominals)
                                    BackBoxEnabled = false;
                                if (TechForm.BackBox.IsEnabled)
                                    TechForm.BackBox.IsEnabled = false;
                                if (CoinHopper2Level != AlarmLevelT.Brown)
                                {
                                    if (CoinHopper2AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper2AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            CoinHopper2Level = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperRightNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper2AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (CoinHopper.HoppertStatus[1].OptFraud ||
                                        CoinHopper.HoppertStatus[1].OptoIdleBlocked ||
                                        CoinHopper.HoppertStatus[1].OptoIdleShort ||
                                        CoinHopper.HoppertStatus[1].OptoPayoutBlocked ||
                                        CoinHopper.HoppertStatus[1].OptoPayoutShort ||
                                        CoinHopper.HoppertStatus[1].PowerDownPayout ||
                                        CoinHopper.HoppertStatus[1].PowerFail ||
                                        CoinHopper.HoppertStatus[1].ReverseLimit ||
                                        CoinHopper.HoppertStatus[1].InductiveCoilFault)
                                {
                                    if (Hopper2Work)
                                        HopperWorkChanged = true;
                                    Hopper2Work = false;
                                    lock (DeliveryNominals)
                                        BackBoxEnabled = false;
                                    if (TechForm.BackBox.IsEnabled)
                                        TechForm.BackBox.IsEnabled = false;

                                    if (CoinHopper.HoppertStatus[1].OptFraud ||
                                    CoinHopper.HoppertStatus[1].OptoIdleBlocked ||
                                    CoinHopper.HoppertStatus[1].OptoIdleShort ||
                                    CoinHopper.HoppertStatus[1].OptoPayoutBlocked ||
                                    CoinHopper.HoppertStatus[1].OptoPayoutShort)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperJam", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper2Level != AlarmLevelT.Brown)
                                        {
                                            if (CoinHopper2AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper2AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                                    CoinHopper2Level = AlarmLevelT.Brown;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperRightJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper2AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[1].PowerDownPayout ||
                                        CoinHopper.HoppertStatus[1].PowerFail)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperPowerError", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper2Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper2AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper2AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper2Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperRightJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper2AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[1].ReverseLimit)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperReverseLimit", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper2Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper2AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper2AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper2Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperRightJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper2AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                    else if (CoinHopper.HoppertStatus[1].InductiveCoilFault)
                                    {
                                        ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + " " + ResourceManager.GetString("HopperInductiveCoilFault", DefaultCulture) + Environment.NewLine;

                                        if (CoinHopper2Level != AlarmLevelT.Red)
                                        {
                                            if (CoinHopper2AlarmTime == DateTime.MinValue)
                                            {
                                                CoinHopper2AlarmTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                                {
                                                    AlarmT Alarm = new AlarmT();
                                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                                    CoinHopper2Level = AlarmLevelT.Red;
                                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                                    Alarm.ErrorCode = AlarmCode.CoinHopperRightJam;
                                                    //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                                    SendAlarm(Alarm);
                                                    CoinHopper2AlarmTime = DateTime.MinValue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Hopper2Work)
                                        HopperWorkChanged = true;
                                    Hopper2Work = true;
                                    lock (DeliveryNominals)
                                        BackBoxEnabled = true;

                                    if (!TechForm.BackBox.IsEnabled)
                                        TechForm.BackBox.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void GetHopperStatus()
        {
            //ToLog(2, "Cashier: GetHopperStatus");
            try
            {
                if (deviceCMModel.HopperExist != null && (bool)deviceCMModel.HopperExist)
                {
                    if (EmulatorEnabled)
                    { // Emulator Enabled
                        if (EmulatorForm.Emulator.HopperVirt1.Checked)
                        { // Hopper1 Virt
                            EmulatorForm.Emulator.HopperGroupBox1.Enabled = true;
                            if (EmulatorForm.Emulator.HopperWork1.Checked)
                            {
                                if (!Hopper1Work)
                                {
                                    HopperWorkChanged = true;
                                    Hopper1Work = true;
                                    lock (DeliveryNominals)
                                        if (!TechForm.FrontBox.IsEnabled)
                                            TechForm.FrontBox.IsEnabled = true;
                                    if (CoinHopper1AlarmTime != DateTime.MinValue)
                                        CoinHopper1AlarmTime = DateTime.MinValue;
                                    if (CoinHopper1Level != AlarmLevelT.Green)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Green;
                                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                                        Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                        Alarm.Status = ResourceManager.GetString("CoinHopper1Work", DefaultCulture);
                                        SendAlarm(Alarm);
                                        CoinHopper1Level = AlarmLevelT.Green;
                                    }
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.HopperNeeded1.Checked)
                                {
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    ErrorsText += ResourceManager.GetString("TechHopper1CheckReq", DefaultCulture) + Environment.NewLine;
                                    if (Hopper1Work)
                                    {
                                        HopperWorkChanged = true;
                                        Hopper1Work = false;
                                    }
                                    lock (DeliveryNominals)
                                        if (TechForm.FrontBox.IsEnabled)
                                            TechForm.FrontBox.IsEnabled = false;
                                }
                                else
                                {
                                    //TerminalWork = TerminalWorkT.TerminalWork;
                                    if (Hopper1Work)
                                    {
                                        HopperWorkChanged = true;
                                        Hopper1Work = false;
                                    }
                                    lock (DeliveryNominals)
                                        if (TechForm.FrontBox.IsEnabled)
                                            TechForm.FrontBox.IsEnabled = false;
                                }
                                if (CoinHopper1Level != AlarmLevelT.Brown)
                                {
                                    if (CoinHopper1AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper1AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            CoinHopper1Level = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperRightNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper1CommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper1AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        { // Hopper1 Hard
                            HardHopper1();
                        }
                        if (EmulatorForm.Emulator.HopperVirt2.Checked)
                        {
                            EmulatorForm.Emulator.HopperGroupBox2.Enabled = true;
                            if (EmulatorForm.Emulator.HopperWork2.Checked)
                            {
                                if (!Hopper2Work)
                                {
                                    HopperWorkChanged = true;
                                    Hopper2Work = true;
                                    lock (DeliveryNominals)
                                        if (!TechForm.BackBox.IsEnabled)
                                            TechForm.BackBox.IsEnabled = true;
                                    if (CoinHopper2AlarmTime != DateTime.MinValue)
                                        CoinHopper2AlarmTime = DateTime.MinValue;
                                    if (CoinHopper2Level != AlarmLevelT.Green)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Green;
                                        Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                                        Alarm.Status = ResourceManager.GetString("CoinHopper2Work", DefaultCulture);
                                        SendAlarm(Alarm);
                                        CoinHopper2Level = AlarmLevelT.Green;
                                    }
                                }
                            }
                            else
                            {
                                if (EmulatorForm.Emulator.HopperNeeded2.Checked)
                                {
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    ErrorsText += ResourceManager.GetString("TechHopper2CheckReq", DefaultCulture) + Environment.NewLine;
                                    if (Hopper2Work)
                                    {
                                        HopperWorkChanged = true;
                                        Hopper2Work = false;
                                    }
                                    lock (DeliveryNominals)
                                        if (TechForm.BackBox.IsEnabled)
                                            TechForm.BackBox.IsEnabled = false;
                                }
                                else
                                {
                                    //TerminalWork = TerminalWorkT.TerminalWork;
                                    if (Hopper2Work)
                                    {
                                        HopperWorkChanged = true;
                                        Hopper2Work = false;
                                    }
                                    lock (DeliveryNominals)
                                        if (TechForm.BackBox.IsEnabled)
                                            TechForm.BackBox.IsEnabled = false;
                                }
                                if (CoinHopper2Level != AlarmLevelT.Brown)
                                {
                                    if (CoinHopper2AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper2AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            CoinHopper2Level = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperLeftNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper2CommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper2AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            HardHopper2();
                        }
                    }
                    else
                    {// Emulator Disabled
                        HardHopper1();
                        HardHopper2();
                    }
                    // Количество монет в 1 Хоппере
                    if (Hopper1Work)
                    {
                        if (CoinHopper != null)
                        {
                            if (!CoinHopper.HoppertStatus[0].LowLewel)
                            {
                                if (CoinHopper1AlarmTime != DateTime.MinValue)
                                    CoinHopper1AlarmTime = DateTime.MinValue;
                                if (CoinHopper1Level != AlarmLevelT.Green)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Green;
                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                    Alarm.ErrorCode = AlarmCode.DeviceWork;
                                    Alarm.Status = ResourceManager.GetString("CoinHopper1Work", DefaultCulture);
                                    SendAlarm(Alarm);
                                    CoinHopper1Level = AlarmLevelT.Green;
                                }
                            }
                            else if (Delivery1Count > 0)
                            {
                                ErrorsText += ResourceManager.GetString("CoinHopper1LowLevel", DefaultCulture) + Environment.NewLine;
                                if (CoinHopper1Level != AlarmLevelT.Yellow)
                                {
                                    if (CoinHopper1AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper1AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                            CoinHopper1Level = AlarmLevelT.Yellow;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperLeftNearEmpty;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper1LowLevel", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper1AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ErrorsText += ResourceManager.GetString("CoinHopper1Empty", DefaultCulture) + Environment.NewLine;
                                if (CoinHopper1Level != AlarmLevelT.Red)
                                {
                                    if (CoinHopper1AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper1AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper1AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            CoinHopper1Level = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperLeftEmpty;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper1Empty", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper1AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // Количество монет в 2 Хоппере
                    if (Hopper2Work)
                    {
                        if (CoinHopper != null)
                        {
                            if (!CoinHopper.HoppertStatus[1].LowLewel)
                            {
                                if (CoinHopper2AlarmTime != DateTime.MinValue)
                                    CoinHopper2AlarmTime = DateTime.MinValue;
                                if (CoinHopper2Level != AlarmLevelT.Green)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Green;
                                    Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                    Alarm.ErrorCode = AlarmCode.DeviceWork;
                                    Alarm.Status = ResourceManager.GetString("CoinHopper2Work", DefaultCulture);
                                    SendAlarm(Alarm);
                                    CoinHopper2Level = AlarmLevelT.Green;
                                }
                            }
                            else if (Delivery2Count > 0)
                            {
                                ErrorsText += ResourceManager.GetString("CoinHopper2LowLevel", DefaultCulture) + Environment.NewLine;
                                if (CoinHopper2Level != AlarmLevelT.Yellow)
                                {
                                    if (CoinHopper2AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper2AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                            CoinHopper2Level = AlarmLevelT.Yellow;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperRightNearEmpty;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper2LowLevel", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper2AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ErrorsText += ResourceManager.GetString("CoinHopper2Empty", DefaultCulture) + Environment.NewLine;
                                if (CoinHopper2Level != AlarmLevelT.Red)
                                {
                                    if (CoinHopper2AlarmTime == DateTime.MinValue)
                                    {
                                        CoinHopper2AlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CoinHopper2AlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            CoinHopper2Level = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                                            Alarm.ErrorCode = AlarmCode.CoinHopperRightEmpty;
                                            //Alarm.Status = ResourceManager.GetString("CoinHopper2Empty", DefaultCulture);
                                            SendAlarm(Alarm);
                                            CoinHopper2AlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                /*else
                { // Хопперов нет
                    if (Hopper1Work)
                        HopperWorkChanged = true;
                    Hopper1Work = false;
                    if (Hopper2Work)
                        HopperWorkChanged = true;
                    Hopper2Work = false;
                    lock (DeliveryNominals)
                        if (TechForm.BackBox.IsEnabled)
                            TechForm.BackBox.IsEnabled = false;
                    lock (DeliveryNominals)
                        if (TechForm.FrontBox.IsEnabled)
                            TechForm.FrontBox.IsEnabled = false;
                    if (EmulatorEnabled)
                    {
                        EmulatorForm.Emulator.HopperGroupBox1.Enabled = false;
                        EmulatorForm.Emulator.HopperGroupBox2.Enabled = false;
                    }
                    if (CoinHopper1Level != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.CoinHopper1;
                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                        Alarm.Status = ResourceManager.GetString("CoinHopper1Work", DefaultCulture);
                        SendAlarm(Alarm);
                        CoinHopper1Level = AlarmLevelT.Green;
                    }
                    if (CoinHopper2Level != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.CoinHopper2;
                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                        Alarm.Status = ResourceManager.GetString("CoinHopper2Work", DefaultCulture);
                        SendAlarm(Alarm);
                        CoinHopper2Level = AlarmLevelT.Green;
                    }
                }*/
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}