using CommonClassLibrary;
using Infralution.Localization.Wpf;
using rps.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using CalculatorTS;
using Rps.ShareBase;
using System.Data;
using SqlBaseImport.Model;
using System.Diagnostics;

namespace Cashier
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        void SoftCardReaderWork()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            try
            {
                ToTariffPlan.DateCard _DateCard;
                string s;
                uint IDCard;
                DateTime d;


                if ((terminalStatusOld != terminalStatus) || (EmulatorForm.Emulator.terminalStatus != terminalStatus))
                {

                    TerminalStatusChanged = true;
                    if (terminalStatusChangeInternal)
                    {
                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                    }
                    else
                    {
                        terminalStatus = EmulatorForm.Emulator.terminalStatus;
                    }
                    terminalStatusChangeInternal = false;
                    if (terminalStatus == TerminalStatusT.CardPaid)
                    {
                        PaimentsStatusChanged = true;
                    }
                }
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: SoftCardReaderWork1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
                switch (terminalStatus)
                {
                    case TerminalStatusT.CardReaded:
                        #region terminalStatusT.CardReaded

                        TSRulesCount = 0;

                        CardInfo.ClientGroupidFC = 1;

                        if (transactions == null)
                            transactions = new TransactionsT { DeviceId = DeviceId, DeviceTypeID = device.Type };
                        transactions.SumOnCardAfter = 0;
                        log.BeginContinues(200);
                        ReaderEntranceTime = EmulatorForm.Emulator.ReaderEntranceTime.Text;
                        try
                        {
                            d = DateTime.Parse(ReaderEntranceTime, DefaultCulture);
                        }
                        catch
                        {
                            d = DateTime.Now - TimeSpan.FromHours(rnd.Next(24));
                        }
                        TimeSpan tt = d - datetime0;
                        CardInfo.ParkingEnterTime = (int)tt.TotalSeconds;
                        CardInfo.LastRecountTime = (int)tt.TotalSeconds;
                        CardInfo.DateSaveCard = (int)tt.TotalSeconds;
                        CardInfo.ZoneidFC = 2;
                        ReaderZoneTime = ReaderEntranceTime;
                        ReaderCardBalance = EmulatorForm.Emulator.CardSum.Text;
                        try
                        {
                            CardSum = Convert.ToInt32(ReaderCardBalance);
                        }
                        catch
                        {
                            CardSum = 0;
                        }
                        CardInfo.SumOnCard = (int)CardSum;
                        ReaderIDCardDec = EmulatorForm.Emulator.ReaderIDCard.Text;
                        try
                        {
                            IDCard = Convert.ToUInt32(ReaderIDCardDec);
                        }
                        catch
                        {
                            IDCard = 1;
                        }
                        ReaderIDCardDec = IDCard.ToString();
                        ReaderIDCardHex = IDCard.ToString("X");
                        ReaderCasseKey = EmulatorForm.Emulator.ReaderCasseKey.Text;
                        if (EmulatorForm.Emulator.ReaderTariffID.SelectedIndex < 0)
                            ReaderTariffID = 0;
                        else
                            ReaderTariffID = (byte)(EmulatorForm.Emulator.ReaderTariffID.SelectedIndex);
                        if (EmulatorForm.Emulator.ReaderSheduleID.SelectedIndex < 0)
                            ReaderSheduleID = 0;
                        else
                            ReaderSheduleID = (byte)(EmulatorForm.Emulator.ReaderSheduleID.SelectedIndex);
                        ClientType = EmulatorForm.Emulator.ClientType.SelectedIndex;

                        ReaderIDCardDec = IDCard.ToString();
                        transactions.Cardnumber = ReaderIDCardDec;
                        string req;
                        LogCardReaded();

                        ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Find card from DB");
                        {
                            req = "select * from Cards where CardId=" + IDCard.ToString();
                            DataRow Cards = null;
                            lock (db_lock)
                            {
                                try
                                {
                                    Cards = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        Cards = UtilsBase.FillRow(req, db, null, null);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            if (Cards == null)
                            {
                                ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Card not exists in DB. Inserting.");
                                card = new CardsT();
                                card.Blocked = 0;
                                card.CardId = IDCard;
                                card._IsDeleted = false;
                                lock (db_lock)
                                {
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded Utils.Running Enter");
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded Utils.Running Leave");
                                    try
                                    {
                                        UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                card = SetCard(Cards);
                            }
                        }

                        ToLog(2, "Cashier: SoftCardReaderWork: CardReaded Find ZoneModel in DB - " + CardInfo.ZoneidFC.ToString());
                        req = "select * from ZoneModel where IdFC=" + CardInfo.ZoneidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                        DataRow _Zone = null;
                        lock (db_lock)
                        {
                            try
                            {
                                _Zone = UtilsBase.FillRow(req, db, null, null);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    _Zone = UtilsBase.FillRow(req, db, null, null);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        if (_Zone != null)
                            transactions.ZoneId = _Zone.GetGuid("Id");

                        _DateCard = CardInfo;
                        card.ClientTypidFC = ClientType;
                        transactions.ClientTypidFC = card.ClientTypidFC;
                        card.TPidFC = tariffs[ReaderTariffID].IdFC;
                        CardInfo.TPidFC = (byte)card.TPidFC;
                        card.TSidFC = TariffSchedule[ReaderSheduleID].IdFC;
                        transactions.TariffScheduleId = TariffSchedule[ReaderSheduleID].Id;
                        CardInfo.TSidFC = (byte)card.TSidFC;
                        card.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);

                        TarifiTS = TariffSchedule[ReaderSheduleID].Id;
                        ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Found TS in DB - " + CardInfo.TSidFC.ToString() + "ID=" + TariffSchedule[ReaderSheduleID].Id.ToString());
                        TarifiTP = tariffs[ReaderTariffID].Id;
                        ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Found TP in DB - " + CardInfo.TPidFC.ToString() + "ID=" + TariffSchedule[ReaderSheduleID].Id.ToString());
                        _TarifiTS = TariffSchedule[ReaderSheduleID];
                        _TarifiTP = tariffs[ReaderTariffID];
                        TarifiTPName = tariffs[ReaderTariffID].Name;

                        transactions.TimeEntry = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                        transactions.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                        transactions.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                        transactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                        transactions.TVP = datetime0 + TimeSpan.FromSeconds(CardInfo.TVP);
                        transactions.TKVP = CardInfo.TKVP;
                        transactions.SumOnCardBefore = CardInfo.SumOnCard;
                        transactions.ClientID = card.ClientId;

                        Paid = 0;
                        PaidBanknote = 0;
                        PaidCoin = 0;
                        DownDiff = 0;
                        EndPaymentEnabled = false;
                        IsPenalCard = false;

                        CalculatorTS.DateCards = CardInfo;
                        ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: CardInfo=" + CardInfo.ToString());


                        lock (TariffPlanTariffScheduleList)
                        {
                            try
                            {
                                TSRulesCount = 0;
                                if (_TarifiTP != null && _TarifiTS != null)
                                    TSRulesCount = TariffPlanForwardSchedulesCount((Guid)TarifiTS, (Guid)TarifiTP);
                                ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: TSRulesCount=" + TSRulesCount.ToString());

                                TSExists = (TSRulesCount > 0);
                                if (TSExists)
                                    TariffPlanTariffScheduleList = TariffPlanForwardSchedules((Guid)TarifiTS, (Guid)TarifiTP);
                                foreach (TariffPlanTariffScheduleModelT tpts in TariffPlanTariffScheduleList)
                                    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Abonement=" + tpts.Id.ToString() + " Price=" + tpts.ConditionAbonementPrice.ToString());
                            }
                            catch
                            {
                                TSRulesCount = 0;
                                TSExists = (TSRulesCount > 0);
                            }
                        }
                        if (ReaderTariffID == 255)
                            ReaderTariffID = 1;

                        try
                        {
                            AmountDue = 0;
                            if (CardInfo.ZoneidFC != 0)
                            {
                                if (CalculatorTS.BigCalculator())
                                {
                                    CardInfo = CalculatorTS.DateCards;
                                    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Calculate Success: CardInfo=" + CardInfo.ToString());
                                    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Calculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Calculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                    CardSum = CardInfo.SumOnCard;
                                    AmountDue = CalculatorTS.Amount;
                                    transactions.SumOnCardAfter = CardSum;
                                    transactions.Amount = AmountDue;
                                    Transaction.PrepaymentSum = CardSum;
                                    Transaction.ToPay = AmountDue;

                                    DateExitEstimated = CalculatorTS.DateExitEstimated;
                                    if (DateExitEstimated <= DateTime.Now)
                                        DateExitEstimated = null;
                                    transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                    transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                    LastCalcTime = DateTime.Now;
                                }
                                else
                                {
                                    CardInfo = CalculatorTS.DateCards;
                                    ToLog(2, "Cashier: SoftCardReaderWork: CardReaded: Calculate  Not Success: CardInfo=" + CardInfo.ToString());
                                    CardSum = CardInfo.SumOnCard;
                                    AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                    LastCalcTime = DateTime.Now;
                                }
                            }
                            else
                            {
                                LastCalcTime = DateTime.Now;
                            }
                            PaymentErrorInfo.SumOnCard = CardSum;
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, "Cashier: SoftCardReaderWork: CardReaded: Calculate exception " + exc.ToString());
                            AmountDue = 0;
                        }

                        CardQuery = AmountDue - CardSum;
                        if (CardQuery < 0)
                            CardQuery = 0;

                        Transaction.PurchaseSum = -CardQuery;

                        CardBalancePos = (CardQuery <= 0) ? true : false;

                        LogCardCalculated();

                        if ((CardQuery > 0) && BankModuleWork)
                        {
                            if (Paiments != PaimentMethodT.PaimentsCash)
                            {
                                BankQuerySumm = CardQuery;
                                BankQuerySummRequested = true;
                                BankModuleResultDescription = "";
                            }
                        }

                        if (CardBalancePos)
                            if ((ClientType == 0) && !(bool)TechForm.OneTimeClient0.IsChecked)
                            {
                                terminalStatus = TerminalStatusT.ExitWithoutPayment;
                            }
                            else
                            {
                                terminalStatus = TerminalStatusT.CardReadedPlus;
                            }
                        else
                            terminalStatus = TerminalStatusT.CardReadedMinus;
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: SoftCardReaderWork2: CardReaded: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;

                        terminalStatusChangeInternal = true;
                        break;
                    #endregion terminalStatusT.CardReaded
                    case TerminalStatusT.CardReadedPlus:
                        #region terminalStatusT.CardReadedPlus
                        //ToLog(2, "Cashier: SoftCardReaderWork: CardReadedPlus");
                        if ((DateTime.Now - LastCalcTime) >= new TimeSpan(0, 10, 0))
                        {
                            try
                            {
                                AmountDue = 0;
                                if (CardInfo.ZoneidFC != 0)
                                {
                                    if (CalculatorTS.BigCalculator())
                                    {
                                        CardInfo = CalculatorTS.DateCards;
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedPlus: Recalculate Success: CardInfo=" + CardInfo.ToString());
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedPlus: Recalculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedPlus: Recalculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                        CardSum = CardInfo.SumOnCard;
                                        AmountDue = CalculatorTS.Amount;
                                        transactions.SumOnCardAfter = CardSum;
                                        transactions.Amount = AmountDue;
                                        Transaction.PrepaymentSum = CardSum;
                                        Transaction.ToPay = AmountDue;

                                        DateExitEstimated = CalculatorTS.DateExitEstimated;
                                        if (DateExitEstimated <= DateTime.Now)
                                            DateExitEstimated = null;
                                        transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                        transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                        LastCalcTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        CardInfo = CalculatorTS.DateCards;
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedPlus: Recalculate Not Success: CardInfo=" + CardInfo.ToString());
                                        CardSum = CardInfo.SumOnCard;
                                        AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                        LastCalcTime = DateTime.Now;
                                    }
                                }
                                else
                                {
                                    LastCalcTime = DateTime.Now;
                                }
                                PaymentErrorInfo.SumOnCard = CardSum;
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, "Cashier: SoftCardReaderWork: CardReadedPlus: recalculate exception " + exc.ToString());
                                AmountDue = 0;
                            }


                            CardQuery = AmountDue - CardSum;
                            if (CardQuery < 0)
                                CardQuery = 0;
                            Transaction.PurchaseSum = -CardQuery;

                            CardBalancePos = (CardQuery <= 0) ? true : false;
                            if (CardBalancePos)
                                terminalStatus = TerminalStatusT.CardReadedPlus;
                            else
                                terminalStatus = TerminalStatusT.CardReadedMinus;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: SoftCardReaderWork3: CardReadedPlus: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;

                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            terminalStatusChangeInternal = false;


                            LogCardCalculated();

                            if ((CardQuery > 0) && BankModuleWork)
                            {
                                if (Paiments != PaimentMethodT.PaimentsCash)
                                {
                                    BankQuerySumm = CardQuery;
                                    BankQuerySummRequested = true;
                                    //s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                                    //ToLog(1, s);
                                    BankModuleResultDescription = "";
                                    /*if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                        BankModule.Purchase(BankQuerySumm);*/
                                }
                            }
                        }
                        break;
                    #endregion terminalStatusT.CardReadedPlus
                    case TerminalStatusT.CardReadedMinus:
                        #region terminalStatusT.CardReadedMinus
                        //ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus");

                        if ((DateTime.Now - LastCalcTime) >= new TimeSpan(0, 10, 0))
                        {
                            try
                            {
                                AmountDue = 0;
                                if (CardInfo.ZoneidFC != 0)
                                {
                                    //ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: Begin recalculate ");
                                    if (CalculatorTS.BigCalculator())
                                    {
                                        //ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: End recalculate Success");
                                        CardInfo = CalculatorTS.DateCards;
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: Recalculate Success: CardInfo=" + CardInfo.ToString());
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: Recalculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: Recalculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                        CardSum = CardInfo.SumOnCard;
                                        AmountDue = CalculatorTS.Amount;
                                        transactions.SumOnCardAfter = CardSum;
                                        transactions.Amount = AmountDue;
                                        Transaction.PrepaymentSum = CardSum;
                                        Transaction.ToPay = AmountDue;

                                        DateExitEstimated = CalculatorTS.DateExitEstimated;
                                        if (DateExitEstimated <= DateTime.Now)
                                            DateExitEstimated = null;
                                        transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                        transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                        LastCalcTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        //ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: End recalculate Not Success");
                                        CardInfo = CalculatorTS.DateCards;
                                        ToLog(2, "Cashier: SoftCardReaderWork: CardReadedMinus: Recalculate Not Success: CardInfo=" + CardInfo.ToString());
                                        CardSum = CardInfo.SumOnCard;
                                        AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                        LastCalcTime = DateTime.Now;
                                    }
                                }
                                else
                                {
                                    LastCalcTime = DateTime.Now;
                                }
                                PaymentErrorInfo.SumOnCard = CardSum;
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, "Cashier: SoftCardReaderWork: CardReadedMinus: recalculate exception " + exc.ToString());
                                AmountDue = 0;
                            }


                            CardQuery = AmountDue - CardSum;
                            if (CardQuery < 0)
                                CardQuery = 0;
                            Transaction.PurchaseSum = -CardQuery;

                            CardBalancePos = (CardQuery <= 0) ? true : false;
                            if (CardBalancePos)
                                terminalStatus = TerminalStatusT.CardReadedPlus;
                            else
                                terminalStatus = TerminalStatusT.CardReadedMinus;
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: SoftCardReaderWork4: CardReadedMinus: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;

                            if (EmulatorEnabled)
                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                            terminalStatusChangeInternal = false;


                            LogCardCalculated();

                            if ((CardQuery > 0) && BankModuleWork)
                            {
                                if (Paiments != PaimentMethodT.PaimentsCash)
                                {
                                    BankQuerySumm = CardQuery;
                                    BankQuerySummRequested = true;
                                    //s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                                    //ToLog(1, s);
                                    BankModuleResultDescription = "";
                                    /*if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                        BankModule.Purchase(BankQuerySumm);*/
                                }
                            }
                        }
                        break;
                    #endregion terminalStatusT.CardReadedMinus
                    case TerminalStatusT.CardReadedError:
                        #region terminalStatusT.CardReadedError
                        ToLog(0, ResourceManager.GetString("ToLogCardReadError", DefaultCulture) + "1: " + CardReader.SoftStatus.ToString());
                        TerminalStatusLabel = Properties.Resources.CardReadedError;
                        break;
                    #endregion terminalStatusT.CardReadedError
                    case TerminalStatusT.PenalCardRequest:
                        #region terminalStatusT.PenalCardRequest
                        PenalCardRequest();
                        break;
                    #endregion terminalStatusT.PenalCardRequest
                    case TerminalStatusT.PenalCardPaiment:
                        #region terminalStatusT.PenalCardPaiment
                        if ((ReaderIDCardDec == null)
                             || (ReaderIDCardDec == ""))
                        {
                            ReaderIDCardDec = "1";
                            ReaderIDCardHex = "1";
                            IDCard = 1;
                            transactions.Cardnumber = ReaderIDCardDec;

                            ToLog(2, "Cashier: SoftCardReaderWork: PenalCardPaiment: find card in DB");
                            {
                                req = "select * from Cards where CardId=" + IDCard.ToString();
                                DataRow Cards = null;
                                lock (db_lock)
                                {
                                    try
                                    {
                                        Cards = UtilsBase.FillRow(req, db, null, null);
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                        try
                                        {
                                            Cards = UtilsBase.FillRow(req, db, null, null);
                                        }
                                        catch (Exception exc1)
                                        {
                                            ToLog(0, exc1.ToString());
                                        }
                                    }
                                }
                                if (Cards == null)
                                {
                                    ToLog(2, "Cashier: SoftCardReaderWork: PenalCardPaiment: Card not exists in DB. Inserting.");
                                    card = new CardsT();
                                    card.Blocked = 0;
                                    card.CardId = IDCard;
                                    card._IsDeleted = false;
                                    lock (db_lock)
                                    {
                                        //if (DetailLog)
                                        //    ToLog(2, "Cashier: SoftCardReaderWork: PenalCardPaiment Utils.Running Enter");
                                        //while (Utils.Running)
                                        //    Thread.Sleep(50);
                                        //if (DetailLog)
                                        //    ToLog(2, "Cashier: SoftCardReaderWork: PenalCardPaiment Utils.Running Leave");
                                        try
                                        {
                                            UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                    ToLog(2, "Cashier: SoftCardReaderWork: PenalCardPaiment: Card not exists in DB. Inserted.");
                                }
                                else
                                {
                                    card = SetCard(Cards);
                                }
                            }
                            d = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                            card.Blocked = 0;
                            card.ParkingEnterTime = d;
                            card.LastRecountTime = d;
                            card.TSidFC = CardInfo.TSidFC;
                            card.TPidFC = CardInfo.TPidFC;
                            card.ClientGroupidFC = CardInfo.ClientGroupidFC;
                            card.ZoneidFC = CardInfo.ZoneidFC;
                            card.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                            card.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                            card.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                            card.TVP = datetime0 + TimeSpan.FromSeconds(CardInfo.TVP);
                            card.TKVP = CardInfo.TKVP;
                            card.ClientTypidFC = CardInfo.ClientTypidFC;
                        }
                        #endregion terminalStatusT.PenalCardPaiment
                        break;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        void HardCardReaderWork()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            ToTariffPlan.DateCard _DateCard;
            try
            {
                string s;
                uint cardid;
                bool res;
                //TimeSpan ts;
                DateTime d;

                if (CardReaderWork)
                {
                    if (CashierType == 2 || CardReader.CardExists)
                    {
                        if (CardReader.SoftStatus == SoftReaderStatusT.CardReaded && terminalStatus == TerminalStatusT.Empty)
                        {
                            CardReader.CardInserted = true;
                            terminalStatus = TerminalStatusT.CardInserted;
                            TerminalStatusChanged = true;
                        }
                        else if (!CardReader.CardExists &&
                            terminalStatus != TerminalStatusT.CardEjected &&
                            terminalStatus != TerminalStatusT.PenalCardRequest &&
                            terminalStatus != TerminalStatusT.PenalCardDisplay &&
                            terminalStatus != TerminalStatusT.PenalCardPaiment &&
                            terminalStatus != TerminalStatusT.PenalCardPrePaiment &&
                            terminalStatus != TerminalStatusT.CardInserted &&
                            terminalStatus != TerminalStatusT.ExitWithoutPayment &&
                            terminalStatus != TerminalStatusT.ShiftSwitchNeed
                        )
                        {
                            if (terminalStatus != TerminalStatusT.Empty)
                            {
                                terminalStatus = TerminalStatusT.Empty;
                                TerminalStatusChanged = true;
                            }
                        }
                        else if (CardReader.SoftStatus == SoftReaderStatusT.SomeoneElsesCard)
                        {
                            terminalStatus = TerminalStatusT.CardWrongCasse;
                            TerminalStatusChanged = true;
                        }
                    }
                    else
                    {
                        if (terminalStatus != TerminalStatusT.CardEjected &&
                            terminalStatus != TerminalStatusT.PenalCardRequest &&
                            terminalStatus != TerminalStatusT.PenalCardPrePaiment &&
                            terminalStatus != TerminalStatusT.CardInserted &&
                            terminalStatus != TerminalStatusT.ExitWithoutPayment &&
                            terminalStatus != TerminalStatusT.ShiftSwitchNeed)
                        {
                            if (terminalStatus != TerminalStatusT.Empty)
                            {
                                terminalStatus = TerminalStatusT.Empty;
                                TerminalStatusChanged = true;
                            }
                        }
                    }


                    if (terminalStatusOld != terminalStatus)
                        ToLog(2, "Cashier: HardCardReaderWork1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    if (terminalStatus != TerminalStatusT.PaymentError)
                    {
                        if (CardDispenser != null && CardDispenser.CardDispenserStatus.CardPreSendPos && !CardReader.CardExists)
                        {
                            if (CardReadCount < 10)
                            {
                                CardReadCount++;
                                return;
                            }
                            else
                            {
                                CardReadCount = 0;
                                terminalStatus = TerminalStatusT.CardReadedError;
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(2, "Cashier: HardCardReaderWork2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                terminalStatusOld = terminalStatus;
                            }
                        }
                    }
                    {
                        switch (terminalStatus)
                        {
                            case TerminalStatusT.CardInserted:
                                #region terminalStatusT.CardInserted
                                ReaderEntranceTime = "";
                                ReaderCardBalance = "";
                                ReaderIDCardDec = "";
                                ReaderIDCardHex = "";
                                if (BankQuerySumm != 0)
                                    BankQuerySumm = 0;
                                if (FormBankCardLabel.Text != "")
                                    FormBankCardLabel.Text = "";
                                if (CrossPay != crossPay.No)
                                    CrossPay = crossPay.No;
                                if (CurrentSumm != 0)
                                    CurrentSumm = 0;
                                if (AddingSumm != 0)
                                    AddingSumm = 0;
                                if (BankQuerySumm != 0)
                                    BankQuerySumm = 0;
                                if (CardQuery != 0)
                                    CardQuery = 0;
                                if (Paid != 0)
                                    Paid = 0;
                                if (BankPaid != 0)
                                    BankPaid = 0;
                                if (PaidBanknote != 0)
                                    PaidBanknote = 0;
                                if (PaidCoin != 0)
                                    PaidCoin = 0;

                                if (CrossPay != crossPay.No)
                                    CrossPay = crossPay.No;
                                if (CurrentSumm != 0)
                                    CurrentSumm = 0;
                                if (PaidBanknote != 0)
                                    PaidBanknote = 0;
                                if (PaidCoin != 0)
                                    PaidCoin = 0;
                                if (DownDiff != 0)
                                    DownDiff = 0;
                                if (BankSlip.Count > 0)
                                    BankSlip.Clear();
                                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                                    BankModuleStatus = BankModuleStatusT.BankCardEmpty;

                                Thread.Sleep(100);
                                if (CardReaderSoftStatus != CardReader.SoftStatus)
                                {
                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                    CardReaderSoftStatus = CardReader.SoftStatus;
                                }

                                if (/*(!CardDispenserStatus.CardPreSendPos && !CardDispenser.CardDispenserStatus.CardPreSendPos && (CashierType != 2)) ||*/
                                (CardReader.SoftStatus == SoftReaderStatusT.ReadError) ||
                                (CardReader.SoftStatus == SoftReaderStatusT.SomeoneElsesCard) /*||
                                (CardReader.SoftStatus == SoftReaderStatusT.CardNotExists)*/)
                                {
                                    terminalStatus = TerminalStatusT.CardReadedError;
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: HardCardReaderWork3: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                }
                                else /*if (CardReader.SoftStatus == SoftReaderStatusT.CardReaded)*/
                                    if (CardReader.CardReaded)
                                {
                                    Paid = 0;
                                    PaidBanknote = 0;
                                    PaidCoin = 0;
                                    DownDiff = 0;
                                    EndPaymentEnabled = false;
                                    IsPenalCard = false;

                                    SetCardStatus();

                                    cardid = Convert.ToUInt32(ReaderIDCardDec);

                                    if (transactions == null)
                                        transactions = new TransactionsT { DeviceId = DeviceId, DeviceTypeID = device.Type };
                                    transactions.SumOnCardAfter = 0;
                                    log.BeginContinues(200);

                                    transactions.Cardnumber = ReaderIDCardDec;

                                    LogCardReaded();
                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Find card in DB");
                                    string req = "select * from Cards where CardId=" + cardid.ToString();
                                    DataRow Cards = null;
                                    lock (db_lock)
                                    {
                                        try
                                        {
                                            Cards = UtilsBase.FillRow(req, db, null, null);
                                            if (Cards == null)
                                            {
                                                ToLog(2, "Cashier: HardCardReaderWork4: CardInserted: First find Card whith CardId=" + cardid.ToString() + " - not found");
                                                Thread.Sleep(100);
                                                Cards = UtilsBase.FillRow(req, db, null, null);
                                            }
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                Cards = UtilsBase.FillRow(req, db, null, null);
                                                if (Cards == null)
                                                {
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find Card whith CardId=" + cardid.ToString() + " - not found");
                                                    Thread.Sleep(100);
                                                    Cards = UtilsBase.FillRow(req, db, null, null);
                                                }
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                    if (Cards == null || (Cards.GetInt("Blocked") == 1))
                                    {
                                        ToLog(2, "Cashier: HardCardReaderWork: CardInserted: Second find Card whith CardId=" + cardid.ToString() + " - not found");
                                        ToLog(0, ToLogCardInStopList + ": " + ToLogIDCard + " " + ReaderIDCardDec);
                                        terminalStatus = TerminalStatusT.CardInStopList;
                                        if (EmulatorEnabled)
                                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardReaderWork4: CardInserted terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                        terminalStatusChangeInternal = false;

                                        TerminalStatusLabel = Properties.Resources.ToLogCardInStopList + "." + Environment.NewLine + Properties.Resources.ToLogCallOperator;
                                        DoSwitchTerminalStatus();
                                        return;
                                    }
                                    else
                                    {
                                        card = SetCard(Cards);
                                    }
                                    _DateCard = CardInfo;
                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaded: DateSaveCard from Cards " + card.DateSaveCard.ToString());

                                    CardInfo = CardsOnline(CardInfo, card);
                                    if (!_DateCard.Equals(CardInfo))
                                        LogCardsOnline();

                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Find TS in DB - " + CardInfo.TSidFC.ToString());
                                    {
                                        req = "select * from TariffScheduleModel where IdFC=" + CardInfo.TSidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                                        DataRow _TarifiTS_ = null;
                                        lock (db_lock)
                                        {
                                            try
                                            {
                                                _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                if (_TarifiTS_ == null)
                                                {
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find TariffScheduleModel whith IdFC=" + CardInfo.TSidFC.ToString() + " - not found");
                                                    Thread.Sleep(100);
                                                    _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                }
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                    if (_TarifiTS_ == null)
                                                    {
                                                        ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find TariffScheduleModel whith IdFC=" + CardInfo.TSidFC.ToString() + " - not found");
                                                        Thread.Sleep(100);
                                                        _TarifiTS_ = UtilsBase.FillRow(req, db, null, null);
                                                    }
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }
                                        if (_TarifiTS_ == null)
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: CardInserted: Second find TariffScheduleModel whith IdFC=" + CardInfo.TSidFC.ToString() + " - not found");
                                            ToLog(0, ToLogCardInStopList + ": " + ResourceManager.GetString("ToLogTS", DefaultCulture) + " " + CardInfo.TSidFC);
                                            terminalStatus = TerminalStatusT.CardInStopList;
                                            if (EmulatorEnabled)
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                            if (terminalStatusOld != terminalStatus)
                                                ToLog(2, "Cashier: HardCardReaderWork5: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                            terminalStatusOld = terminalStatus;
                                            terminalStatusChangeInternal = false;

                                            TerminalStatusLabel = Properties.Resources.ToLogCardInStopList + "." + Environment.NewLine + Properties.Resources.ToLogCallOperator;
                                            DoSwitchTerminalStatus();
                                            return;
                                        }
                                        else
                                        {
                                            _TarifiTS = SetTarifiTS(_TarifiTS_);
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Found TS in DB - " + CardInfo.TSidFC.ToString() + "ID=" + _TarifiTS.Id.ToString());
                                        }
                                    }

                                    TarifiTS = _TarifiTS.Id;
                                    transactions.TariffScheduleId = (Guid)TarifiTS;

                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaded Find TP in DB - " + CardInfo.TPidFC.ToString());
                                    {
                                        req = "select * from Tariffs where IdFC=" + CardInfo.TPidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                                        DataRow _TarifiTP_ = null;
                                        lock (db_lock)
                                        {
                                            try
                                            {
                                                _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                if (_TarifiTP_ == null)
                                                {
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find Tariffs whith IdFC=" + CardInfo.TPidFC.ToString() + " - not found");
                                                    Thread.Sleep(100);
                                                    _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                }
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                    if (_TarifiTP_ == null)
                                                    {
                                                        ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find Tariffs whith IdFC=" + CardInfo.TPidFC.ToString() + " - not found");
                                                        Thread.Sleep(100);
                                                        _TarifiTP_ = UtilsBase.FillRow(req, db, null, null);
                                                    }
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }
                                        if (_TarifiTP_ == null)
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: CardInserted: Second find Tariffs whith IdFC=" + CardInfo.TPidFC.ToString() + " - not found");
                                            ToLog(0, ToLogCardInStopList + ": " + ToLogTariffID + " " + CardInfo.TPidFC);
                                            terminalStatus = TerminalStatusT.CardInStopList;
                                            if (terminalStatusOld != terminalStatus)
                                                ToLog(2, "Cashier: HardCardReaderWork6: CardInserted terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                            if (EmulatorEnabled)
                                                EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                            terminalStatusChangeInternal = false;

                                            TerminalStatusLabel = Properties.Resources.ToLogCardInStopList + "." + Environment.NewLine + Properties.Resources.ToLogCallOperator;
                                            DoSwitchTerminalStatus();
                                            return;
                                        }
                                        else
                                        {
                                            _TarifiTP = SetTarifiTP(_TarifiTP_);
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Found TP in DB - " + CardInfo.TPidFC.ToString() + "ID=" + _TarifiTP.Id.ToString());
                                        }

                                    }
                                    TarifiTP = _TarifiTP.Id;

                                    FreeTimeTypeId = (int)_TarifiTP.FreeTimeTypeId;
                                    FreeTime = _TarifiTP.FreeTime;
                                    TarifiTPName = _TarifiTP.Name;
                                    ReaderTariffID = CardInfo.TPidFC;
                                    transactions.TimeEntry = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                                    card.ParkingEnterTime = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                                    card.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                    transactions.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                                    card.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                                    transactions.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                                    card.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                                    transactions.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                                    card.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                                    transactions.TVP = datetime0 + TimeSpan.FromSeconds(CardInfo.TVP);
                                    transactions.TKVP = CardInfo.TKVP;
                                    transactions.SumOnCardBefore = CardInfo.SumOnCard;
                                    transactions.ClientTypidFC = card.ClientTypidFC;
                                    transactions.ClientID = card.ClientId;

                                    ToLog(2, "Cashier: HardCardReaderWork: CardReaded Find ZoneModel in DB - " + CardInfo.ZoneidFC.ToString());
                                    req = "select * from ZoneModel where IdFC=" + CardInfo.ZoneidFC.ToString() + " and (_IsDeleted is null or _IsDeleted = 0) ";
                                    DataRow _Zone = null;
                                    lock (db_lock)
                                    {
                                        try
                                        {
                                            _Zone = UtilsBase.FillRow(req, db, null, null);
                                            if (_Zone == null)
                                            {
                                                ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find ZoneModel whith IdFC=" + CardInfo.ZoneidFC.ToString() + " - not found");
                                                Thread.Sleep(100);
                                                _Zone = UtilsBase.FillRow(req, db, null, null);
                                            }
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                _Zone = UtilsBase.FillRow(req, db, null, null);
                                                if (_Zone == null)
                                                {
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardInserted: First find ZoneModel whith IdFC=" + CardInfo.ZoneidFC.ToString() + " - not found");
                                                    Thread.Sleep(100);
                                                    _Zone = UtilsBase.FillRow(req, db, null, null);
                                                }
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                    if (_Zone != null)
                                        transactions.ZoneId = _Zone.GetGuid("Id");

                                    EndPaymentEnabled = false;
                                    IsPenalCard = false;
                                    PayTransactionExported = false;
                                    CardSum = CardInfo.SumOnCard;
                                    DateTime dt1 = Convert.ToDateTime(ReaderZoneTime);
                                    DateTime dt2 = DateTime.Now;
                                    if (dt1 < datetimeFirst)
                                    {
                                        #region terminalStatusT.CardReadedError
                                        ToLog(0, "Cashier: HardCardReaderWork: CardReaded Error DateTime: " + datetimeFirst.ToString() + " > " + dt1.ToString());
                                        terminalStatus = TerminalStatusT.CardReadedError;
                                        if (CardReaderSoftStatus != CardReader.SoftStatus)
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                            CardReaderSoftStatus = CardReader.SoftStatus;
                                        }
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardReaderWork7: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                        DoSwitchTerminalStatus();
                                        #endregion terminalStatusT.CardReadedError
                                        return;
                                    }
                                    CalculatorTS.DateCards = CardInfo;

                                    lock (TariffPlanTariffScheduleList)
                                    {
                                        try
                                        {
                                            TSRulesCount = 0;
                                            if (_TarifiTP != null && _TarifiTS != null)
                                                TSRulesCount = TariffPlanForwardSchedulesCount((Guid)TarifiTS, (Guid)TarifiTP);
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaded: TSRulesCount=" + TSRulesCount.ToString());
                                            TSExists = (TSRulesCount > 0);
                                            if (TSExists)
                                                TariffPlanTariffScheduleList = TariffPlanForwardSchedules((Guid)TarifiTS, (Guid)TarifiTP);
                                            foreach (TariffPlanTariffScheduleModelT tpts in TariffPlanTariffScheduleList)
                                                ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Abonement=" + tpts.Id.ToString() + " Price=" + tpts.ConditionAbonementPrice.ToString());
                                        }
                                        catch
                                        {
                                            TSRulesCount = 0;
                                            TSExists = (TSRulesCount > 0);
                                        }
                                    }

                                    if (ReaderTariffID == 255)
                                        ReaderTariffID = 1;

                                    try
                                    {
                                        AmountDue = 0;
                                        if (CardInfo.ZoneidFC != 0)
                                        {
                                            //ToLog(2, "Cashier: HardCardReaderWork: CardReaded Begin Calcuate");
                                            if (CalculatorTS.BigCalculator())
                                            {
                                                //ToLog(2, "Cashier: HardCardReaderWork: CardReaded End Calcuate Success");
                                                CardInfo = CalculatorTS.DateCards;
                                                ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Calculate Success: CardInfo: TimeEntry=" + 
                                                    (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString() + 
                                                    " LastRecountTime="+ (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString() +
                                                    " TPidFC=" + CardInfo.TPidFC.ToString()+
                                                    " ZoneidFC=" + CardInfo.ZoneidFC.ToString() +
                                                    " ClientGroupidFC=" + CardInfo.ClientGroupidFC.ToString() +
                                                    " SumOnCard=" + CardInfo.SumOnCard.ToString() +
                                                    " LastPaymentTime=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.LastPaymentTime)).ToString() +
                                                    " Nulltime1=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1)).ToString() +
                                                    " Nulltime2=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2)).ToString() +
                                                    " Nulltime13=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3)).ToString() +
                                                    " TVP=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.TVP)).ToString() +

                                                    " TKVP=" + CardInfo.TKVP.ToString() +
                                                    " ClientTypidFC=" + CardInfo.ClientTypidFC.ToString() +
                                                    " DateSaveCard=" + (datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard)).ToString()
                                                    );
                                                ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Calculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                                ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Calculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                                CardSum = CardInfo.SumOnCard;
                                                AmountDue = CalculatorTS.Amount;
                                                transactions.SumOnCardAfter = CardSum;
                                                transactions.Amount = AmountDue;
                                                Transaction.PrepaymentSum = CardSum;
                                                Transaction.ToPay = AmountDue;

                                                DateExitEstimated = CalculatorTS.DateExitEstimated;
                                                if (DateExitEstimated <= DateTime.Now)
                                                    DateExitEstimated = null;
                                                transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                                transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                                LastCalcTime = DateTime.Now;
                                            }
                                            else
                                            {
                                                CardInfo = CalculatorTS.DateCards;
                                                ToLog(2, "Cashier: HardCardReaderWork: CardReaded: Calculate  Not Success: CardInfo=" + CardInfo.ToString());
                                                CardSum = CardInfo.SumOnCard;
                                                AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                                LastCalcTime = DateTime.Now;
                                            }
                                        }
                                        else
                                        {
                                            LastCalcTime = DateTime.Now;
                                        }
                                        PaymentErrorInfo.SumOnCard = CardSum;
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, "Cashier: HardCardReaderWork: CardReaded Calcuate exception " + exc.ToString());
                                        AmountDue = 0;
                                    }


                                    CardQuery = AmountDue - CardSum;
                                    if (CardQuery < 0)
                                        CardQuery = 0;
                                    Transaction.PurchaseSum = -CardQuery;

                                    CardBalancePos = (CardQuery <= 0) ? true : false;


                                    LogCardCalculated();

                                    if ((CardQuery > 0) && BankModuleWork)
                                    {
                                        if (Paiments != PaimentMethodT.PaimentsCash)
                                        {
                                            BankQuerySumm = CardQuery;
                                            BankQuerySummRequested = true;
                                            //s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                                            //ToLog(1, s);
                                            BankModuleResultDescription = "";
                                            /*if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                                BankModule.Purchase(BankQuerySumm);*/
                                        }
                                    }

                                    if (CardBalancePos)
                                    {
                                        if ((ClientType == 0) && !(bool)TechForm.OneTimeClient0.IsChecked)
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaded CardBalancePos OneTimeClient0=false");
                                            terminalStatus = TerminalStatusT.ExitWithoutPayment;
                                        }
                                        else
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: CardReaded CardBalancePos");
                                            terminalStatus = TerminalStatusT.CardReadedPlus;
                                        }
                                    }
                                    else
                                    {
                                        ToLog(2, "Cashier: HardCardReaderWork: CardReaded CardBalanceNeg");
                                        terminalStatus = TerminalStatusT.CardReadedMinus;
                                    }
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: HardCardReaderWork8: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;

                                    if (EmulatorEnabled)
                                        EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                    terminalStatusChangeInternal = false;
                                }
                                break;
                            #endregion terminalStatusT.CardInserted
                            case TerminalStatusT.CardReadedPlus:
                                #region terminalStatusT.CardReadedPlus
                                //ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus");
                                if (CashierType != 2 && (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists))
                                    /*if (!CardDispenserStatus.CardPreSendPos)
                                    {
                                        terminalStatus = TerminalStatusT.CardReadError;
                                        break;
                                    }*/
                                    if ((DateTime.Now - LastCalcTime) >= new TimeSpan(0, 10, 0))
                                    {
                                        try
                                        {
                                            AmountDue = 0;
                                            if (CardInfo.ZoneidFC != 0)
                                            {
                                                //ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus Begin recalculate");
                                                if (CalculatorTS.BigCalculator())
                                                {
                                                    //ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus End recalculate Success");
                                                    CardInfo = CalculatorTS.DateCards;
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus: Recalculate Success: CardInfo=" + CardInfo.ToString());
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus: Recalculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus: Recalculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                                    CardSum = CardInfo.SumOnCard;
                                                    AmountDue = CalculatorTS.Amount;
                                                    transactions.SumOnCardAfter = CardSum;
                                                    transactions.Amount = AmountDue;
                                                    Transaction.PrepaymentSum = CardSum;
                                                    Transaction.ToPay = AmountDue;

                                                    DateExitEstimated = CalculatorTS.DateExitEstimated;
                                                    if (DateExitEstimated <= DateTime.Now)
                                                        DateExitEstimated = null;
                                                    transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                                    transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                                    LastCalcTime = DateTime.Now;
                                                }
                                                else
                                                {
                                                    //ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus End recalculate Not Success");
                                                    CardInfo = CalculatorTS.DateCards;
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedPlus: Recalculate  Not Success: CardInfo=" + CardInfo.ToString());
                                                    CardSum = CardInfo.SumOnCard;
                                                    AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                                    LastCalcTime = DateTime.Now;
                                                }
                                            }
                                            else
                                            {
                                                LastCalcTime = DateTime.Now;
                                            }
                                            PaymentErrorInfo.SumOnCard = CardSum;
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, "Cashier: HardCardReaderWork: CardReadedPlus recalculate exception " + exc.ToString());
                                            AmountDue = 0;
                                        }


                                        CardQuery = AmountDue - CardSum;
                                        if (CardQuery < 0)
                                            CardQuery = 0;
                                        Transaction.PurchaseSum = -CardQuery;

                                        CardBalancePos = (CardQuery <= 0) ? true : false;
                                        if (CardBalancePos)
                                            terminalStatus = TerminalStatusT.CardReadedPlus;
                                        else
                                            terminalStatus = TerminalStatusT.CardReadedMinus;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardReaderWork9: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;

                                        if (EmulatorEnabled)
                                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                        terminalStatusChangeInternal = false;


                                        LogCardCalculated();

                                        if ((CardQuery > 0) && BankModuleWork)
                                        {
                                            if (Paiments != PaimentMethodT.PaimentsCash)
                                            {
                                                BankQuerySumm = CardQuery;
                                                BankQuerySummRequested = true;
                                                //s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                                                //ToLog(1, s);
                                                BankModuleResultDescription = "";
                                                /*if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                                    BankModule.Purchase(BankQuerySumm);*/
                                            }
                                        }
                                    }
                                break;
                            #endregion terminalStatusT.CardReadedPlus
                            case TerminalStatusT.CardReadedMinus:
                                #region terminalStatusT.CardReadedMinus
                                //ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus");
                                if (CashierType != 2 && (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists))
                                    /*if (!CardDispenserStatus.CardPreSendPos)
                                    {
                                        terminalStatus = TerminalStatusT.CardReadError;
                                        break;
                                    }*/
                                    if ((DateTime.Now - LastCalcTime) >= new TimeSpan(0, 10, 0))
                                    {
                                        try
                                        {
                                            AmountDue = 0;
                                            if (CardInfo.ZoneidFC != 0)
                                            {
                                                //ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus Begin recalculate");
                                                if (CalculatorTS.BigCalculator())
                                                {
                                                    //ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus End recalculate Success");
                                                    CardInfo = CalculatorTS.DateCards;
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus: Recalculate Success: CardInfo=" + CardInfo.ToString());
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus: Recalculate Success: Amount=" + CalculatorTS.Amount.ToString());
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus: Recalculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                                                    CardSum = CardInfo.SumOnCard;
                                                    AmountDue = CalculatorTS.Amount;
                                                    transactions.SumOnCardAfter = CardSum;
                                                    transactions.Amount = AmountDue;
                                                    Transaction.PrepaymentSum = CardSum;
                                                    Transaction.ToPay = AmountDue;

                                                    DateExitEstimated = CalculatorTS.DateExitEstimated;
                                                    if (DateExitEstimated <= DateTime.Now)
                                                        DateExitEstimated = null;
                                                    transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime);
                                                    transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                                                    LastCalcTime = DateTime.Now;
                                                }
                                                else
                                                {
                                                    CardInfo = CalculatorTS.DateCards;
                                                    ToLog(2, "Cashier: HardCardReaderWork: CardReadedMinus: Recalculate  Not Success: CardInfo=" + CardInfo.ToString());
                                                    CardSum = CardInfo.SumOnCard;
                                                    AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                                                    LastCalcTime = DateTime.Now;
                                                }
                                            }
                                            else
                                            {
                                                LastCalcTime = DateTime.Now;
                                            }
                                            PaymentErrorInfo.SumOnCard = CardSum;
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, "Cashier: HardCardReaderWork: CardReadedMinus recalculate " + exc.ToString());
                                            AmountDue = 0;
                                        }


                                        CardQuery = AmountDue - CardSum;
                                        if (CardQuery < 0)
                                            CardQuery = 0;
                                        Transaction.PurchaseSum = -CardQuery;

                                        CardBalancePos = (CardQuery <= 0) ? true : false;
                                        if (CardBalancePos)
                                            terminalStatus = TerminalStatusT.CardReadedPlus;
                                        else
                                            terminalStatus = TerminalStatusT.CardReadedMinus;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardReaderWork10: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;

                                        if (EmulatorEnabled)
                                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                                        terminalStatusChangeInternal = false;

                                        LogCardCalculated();

                                        if ((CardQuery > 0) && BankModuleWork)
                                        {
                                            if (Paiments != PaimentMethodT.PaimentsCash)
                                            {
                                                BankQuerySumm = CardQuery;
                                                BankQuerySummRequested = true;
                                                //s = string.Format(ToLogBankRequest, BankQuerySumm.ToString(), sValuteLabel);
                                                //ToLog(1, s);
                                                BankModuleResultDescription = "";
                                                /*if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                                                    BankModule.Purchase(BankQuerySumm);*/
                                            }
                                        }
                                    }
                                break;
                            #endregion terminalStatusT.CardReadedMinus
                            case TerminalStatusT.CardReadedError:
                                #region terminalStatusT.CardReadedError
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(0, "Cashier: HardCardReaderWork11: CardReadedMinus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString() + " " + ResourceManager.GetString("ToLogCardReadError", DefaultCulture) + CardReader.SoftStatus.ToString());
                                TerminalStatusLabel = Properties.Resources.CardReadedError;
                                #endregion terminalStatusT.CardReadedError
                                break;
                            case TerminalStatusT.PenalCardRequest:
                                #region terminalStatusT.PenalCardRequest
                                PenalCardRequest();
                                break;
                            #endregion terminalStatusT.PenalCardRequest
                            case TerminalStatusT.PenalCardDisplay:
                                #region terminalStatusT.PenalCardDisplay
                                //ToLog(2, "Cashier: HardCardReaderWork: PenalCardDisplay");
                                terminalStatus = TerminalStatusT.PenalCardPrePaiment;
                                if (terminalStatusOld != terminalStatus)
                                    ToLog(2, "Cashier: HardCardReaderWork12: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                terminalStatusOld = terminalStatus;
                                if (EmulatorEnabled)
                                    EmulatorForm.Emulator.terminalStatus = terminalStatus;

                                terminalStatusChangeInternal = false;
                                break;
                            #endregion terminalStatusT.PenalCardDisplay
                            case TerminalStatusT.PenalCardPrePaiment:
                                break;
                            case TerminalStatusT.PenalCardPaiment:
                                #region terminalStatusT.PenalCardPaiment
                                if (ReaderIDCardDec == "")
                                {
                                    if (!CardDispenserStatus.CardPreSendPos)
                                        return;
                                    else
                                    {
                                        if (!CardReader.CardInserted)
                                        {
                                            CardReader.CardInserted = true;
                                            return;
                                        }
                                    }
                                    Thread.Sleep(300);

                                    int i = 0;
                                    if (CardReaderSoftStatus != CardReader.SoftStatus)
                                    {
                                        ToLog(1, "Cashier: HardCardReader: PenalCardPaiment: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                        CardReaderSoftStatus = CardReader.SoftStatus;
                                    }
                                    while (CardDispenserStatus.CardPreSendPos &&
                                           (/*(CardReader.Status == HardReaderStatusT.Waiting) ||*/
                                           (CardReader.SoftStatus != SoftReaderStatusT.CardReaded) &&
                                           (CardReader.SoftStatus != SoftReaderStatusT.ReadError) &&
                                           (CardReader.SoftStatus != SoftReaderStatusT.SomeoneElsesCard) &&
                                           (CardReader.SoftStatus != SoftReaderStatusT.CardNotExists) &&
                                           (CardReader.SoftStatus != SoftReaderStatusT.Exception)) /*&&
                                           (i++ < 10)*/)
                                        Thread.Sleep(100);

                                    if (CardReaderSoftStatus != CardReader.SoftStatus)
                                    {
                                        ToLog(2, "Cashier: HardCardReader: PenalCardPaiment: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                        CardReaderSoftStatus = CardReader.SoftStatus;
                                    }

                                    _DateCard = CardInfo;
                                    SetCardStatus();
                                    if (CardReaderSoftStatus != CardReader.SoftStatus)
                                    {
                                        ToLog(2, "Cashier: HardCardReader: PenalCardPaiment: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                        CardReaderSoftStatus = CardReader.SoftStatus;
                                    }
                                    if (CardReader.SoftStatus != SoftReaderStatusT.CardReaded) // ����� ������
                                    {
                                        ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment - Read Card - ReadError - Change Card");
                                        CardDispenser.SendCardToRecycleBox();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                        ToLog(2, "Cashier: HardCardDispenserWork: CardDispenser.SendFromBoxToReadPosition");
                                        CardDispenser.SendFromBoxToReadPosition();
                                        while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                            Thread.Sleep(100);
                                        CardReader.SoftStatus = SoftReaderStatusT.CardNotExists;
                                        if (CardReaderSoftStatus != CardReader.SoftStatus)
                                        {
                                            ToLog(2, "Cashier: HardCardReader: PenalCardPaiment: CardReaderSoftStatus changed from " + CardReaderSoftStatus.ToString() + " to " + CardReader.SoftStatus.ToString());
                                            CardReaderSoftStatus = CardReader.SoftStatus;
                                        }
                                        return;
                                    }
                                    ReaderIDCardHex = CardReader.CardId;
                                    res = uint.TryParse(ReaderIDCardHex, NumberStyles.HexNumber, null, out cardid);
                                    ReaderIDCardDec = cardid.ToString();
                                    ToLog(1, ToLogCardReaded + " " + ToLogIDCard + " " + ReaderIDCardDec);
                                    CardInfo = _DateCard;

                                    ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment: - Find card in DB");
                                    {
                                        string req = "select * from Cards where CardId=" + cardid.ToString();
                                        DataRow Cards = null;
                                        lock (db_lock)
                                        {
                                            try
                                            {
                                                Cards = UtilsBase.FillRow(req, db, null, null);
                                            }
                                            catch (Exception exc)
                                            {
                                                ToLog(0, exc.ToString());
                                                try
                                                {
                                                    Cards = UtilsBase.FillRow(req, db, null, null);
                                                }
                                                catch (Exception exc1)
                                                {
                                                    ToLog(0, exc1.ToString());
                                                }
                                            }
                                        }
                                        if (Cards == null)
                                        {
                                            ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment: - Insert card to DB");
                                            card = new CardsT();
                                            card.Blocked = 0;
                                            card.CardId = cardid;
                                            card._IsDeleted = false;

                                            lock (db_lock)
                                            {
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment Utils.Running Enter");
                                                //while (Utils.Running)
                                                //    Thread.Sleep(50);
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment Utils.Running Leave");
                                                try
                                                {
                                                    UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                                }
                                                catch (Exception exc)
                                                {
                                                    ToLog(0, exc.ToString());
                                                    try
                                                    {
                                                        UtilsBase.InsertCommand("Cards", db, null, new object[] { "CardId", card.CardId, "Blocked", card.Blocked, "_IsDeleted", card._IsDeleted });
                                                    }
                                                    catch (Exception exc1)
                                                    {
                                                        ToLog(0, exc1.ToString());
                                                    }
                                                }
                                            }
                                            ToLog(2, "Cashier: HardCardReaderWork: PenalCardPaiment: Card not exists in DB. Inserted.");
                                        }
                                        else
                                        {
                                            card = SetCard(Cards);
                                        }
                                    }

                                    d = datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime);
                                    card.Blocked = 0;
                                    card.ParkingEnterTime = d;
                                    card.LastRecountTime = d;
                                    card.TSidFC = CardInfo.TSidFC;
                                    card.TPidFC = CardInfo.TPidFC;
                                    card.ClientGroupidFC = CardInfo.ClientGroupidFC;
                                    card.ZoneidFC = CardInfo.ZoneidFC;
                                    card.Nulltime1 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime1);
                                    card.Nulltime2 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime2);
                                    card.Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                                    card.TVP = datetime0 + TimeSpan.FromSeconds(CardInfo.TVP);
                                    card.TKVP = CardInfo.TKVP;
                                    card.ClientTypidFC = CardInfo.ClientTypidFC;

                                    ReaderEntranceTime = (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString(); // ����� ������

                                    ReaderZoneTime = (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString(); // ����� ������ � ����
                                    ReaderTariffID = CardInfo.TPidFC;

                                    ReaderCardBalance = CardInfo.SumOnCard.ToString(); //������ �����
                                    ClientType = CardInfo.ClientTypidFC;

                                    transactions.Cardnumber = ReaderIDCardDec;
                                    CardReader.CardInfo = CardInfo;

                                }
                                break;
                            #endregion terminalStatusT.PenalCardPaiment
                            case TerminalStatusT.CardEjected:
                                #region TerminalStatusT.CardEjected
                                if (!CardDispenserStatus.CardDespensePos &&
                                    !CardDispenserStatus.CardDispensing &&
                                    !CardDispenserStatus.CardPreSendPos)
                                {
                                    terminalStatus = TerminalStatusT.Empty;
                                    if (terminalStatusOld != terminalStatus)
                                        ToLog(2, "Cashier: HardCardReaderWork13: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    terminalStatusOld = terminalStatus;
                                }
                                break;
                                #endregion TerminalStatusT.CardEjected
                                /*case TerminalStatusT.CardReadError:
                                    #region TerminalStatusT.CardReadError
                                    //ToLog(2, "Cashier: HardCardReaderWork: CardEjected or CardReadError");
                                    if (!CardDispenserStatus.CardDespensePos &&
                                        !CardDispenserStatus.CardDispensing &&
                                        !CardDispenserStatus.CardPreSendPos)
                                    {
                                        terminalStatus = TerminalStatusT.Empty;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardCardReaderWork14: CardReadError terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                    }
                                    break;
                                    #endregion TerminalStatusT.CardReadError
                                  */
                        }
                    }
                    if (terminalStatusOld != terminalStatus)
                        ToLog(2, "Cashier: HardCardReaderWork15: CardReadError terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                    terminalStatusOld = terminalStatus;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: HardCardReaderWork: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }

        bool transactionsTariffScheduleId = false;
        ToTariffPlan.DateCard PenalCardRequest(ToTariffPlan.DateCard? vCardInfo = null)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            ToTariffPlan.DateCard lCardInfo;
            if (vCardInfo != null)
                lCardInfo = (ToTariffPlan.DateCard)vCardInfo;
            else
                lCardInfo = CardInfo;
            try
            {
                string s;
                ClientType = 0;

                if (transactions == null)
                    transactions = new TransactionsT { DeviceId = DeviceId, DeviceTypeID = device.Type };
                transactions.SumOnCardAfter = 0;

                if (TechForm.ZoneComboBoxSelectedIndex >= 0)
                {
                    transactions.ZoneId = zones[TechForm.ZoneComboBoxSelectedIndex].Id;
                }
                else
                    transactions.ZoneId = null;
                log.BeginContinues(200);

                AmountDue = 0;
                Paid = 0;
                PaidBanknote = 0;
                PaidCoin = 0;
                DownDiff = 0;
                CardSum = 0;
                ReaderIDCardHex = "";
                ReaderIDCardDec = "";
                EndPaymentEnabled = false;
                IsPenalCard = true;
                card = null;
                if (TechForm.GroupComboBoxSelectedIndex >= 0)
                    lCardInfo.ClientGroupidFC = (byte)groups[TechForm.GroupComboBoxSelectedIndex].idFC;
                else
                    lCardInfo.ClientGroupidFC = 0;
                lCardInfo.ClientTypidFC = 0;

                DateTime d = DateTime.Now - TimeSpan.FromMinutes(10);
                TimeSpan pts = d - datetime0;
                lCardInfo.LastRecountTime = (int)pts.TotalSeconds;
                transactions.LastRecountTime = d;

                lCardInfo.ParkingEnterTime = (int)pts.TotalSeconds;
                transactions.TimeEntry = d;

                lCardInfo.SumOnCard = 0;
                lCardInfo.TPidFC = (byte)PenalCardTP;
                lCardInfo.TSidFC = (byte)PenalCardTS;
                if (TechForm.ZoneComboBoxSelectedIndex >= 0)
                    lCardInfo.ZoneidFC = (byte)zones[TechForm.ZoneComboBoxSelectedIndex].IdFC;
                else
                    lCardInfo.ZoneidFC = 0;
                ReaderEntranceTime = d.ToString(); // ����� ������
                ReaderTariffID = (byte)PenalCardTP;

                ToLog(1, "Cashier: PenalCardRequest: " + ToLogEntranceTime + " " + ReaderEntranceTime);
                ToLog(1, "Cashier: PenalCardRequest: " + ToLogTariffID + " " + ReaderTariffID);
                ToLog(1, "Cashier: PenalCardRequest: " + ToLogRegular + " " + ClientType.ToString());
                ToLog(1, "Cashier: PenalCardRequest: " + ToLogTariffSheduleID + " " + lCardInfo.TSidFC.ToString());
                ToLog(1, "Cashier: PenalCardRequest: " + ToLogZoneid + " " + lCardInfo.ZoneidFC.ToString());
                ToLog(1, "Cashier: PenalCardRequest: " + ToLogClientGroupid + " " + lCardInfo.ClientGroupidFC.ToString());
                s = string.Format(ToLogSumCard, CardSum.ToString(), sValuteLabel);
                ToLog(1, "Cashier: PenalCardRequest: " + s);

                try
                {
                    transactions.TariffScheduleId = TariffSchedule[TechForm.PenalCardTSSelectedIndex].Id;
                }
                catch (Exception exc)
                {
                    if (!transactionsTariffScheduleId)
                    {
                        transactionsTariffScheduleId = true;
                        ToLog(0, "Cashier: PenalCardRequest: Exceptiion TariffSchedule TechForm.PenalCardTSSelectedIndex=" + TechForm.PenalCardTSSelectedIndex.ToString() + Environment.NewLine + exc.ToString());
                    }
                }
                transactionsTariffScheduleId = false;

                try
                {
                    CalculatorTS.DateCards = lCardInfo;

                    TarifiTPName = tariffs[TechForm.PenalCardTPSelectedIndex].Name;
                    transactions.TarifPlanId = tariffs[TechForm.PenalCardTPSelectedIndex].Id;
                    CultureInfo MyCultureInfo = new CultureInfo(Country);
                    DateTime dt1 = transactions.TimeEntry;
                    DateTime dt2 = DateTime.Now;

                    if (CalculatorTS.BigCalculator())
                    {
                        ToLog(2, "Cashier: PenalCardRequest: Calculate Success: Amount=" + CalculatorTS.Amount.ToString());
                        ToLog(2, "Cashier: PenalCardRequest: CardReaded: Calculate Success: DateExitEstimated=" + CalculatorTS.DateExitEstimated.ToString());
                        AmountDue = CalculatorTS.Amount;
                        DateExitEstimated = CalculatorTS.DateExitEstimated;
                        if (DateExitEstimated <= DateTime.Now)
                            DateExitEstimated = null;
                        LastCalcTime = DateTime.Now;
                        transactions.SumOnCardBefore = CardSum;
                        transactions.Amount = AmountDue;
                        Transaction.PrepaymentSum = CardSum;
                        transactions.TarifPlanId = (Guid)CalculatorTS.TarifPlanId;
                        transactions.LastRecountTime = datetime0 + TimeSpan.FromSeconds(lCardInfo.LastRecountTime);
                        Transaction.ToPay = AmountDue;
                        LastCalcTime = DateTime.Now;
                    }
                    else
                    {
                        ToLog(2, "Cashier: PenalCardRequest: Calculate: Calculate Not Success");
                        lCardInfo = CalculatorTS.DateCards;
                        CardSum = lCardInfo.SumOnCard;
                        AmountDue = 0;                          // �� ���� ���������, ������� ������� 
                        LastCalcTime = DateTime.Now;
                    }
                }
                catch (Exception exc)
                {
                    ToLog(0, "Cashier: PenalCardRequest: Calculate Error " + exc.ToString());
                    AmountDue = 0;
                    LastCalcTime = DateTime.Now;
                }
                CardQuery = AmountDue - CardSum;
                if (CardQuery < 0)
                    CardQuery = 0;
                Transaction.PurchaseSum = -CardQuery;

                CardBalancePos = false;

                terminalStatus = TerminalStatusT.PenalCardPrePaiment;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: PenalCardRequestWork1: CardReadError terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;

                if (EmulatorEnabled)
                    EmulatorForm.Emulator.terminalStatus = terminalStatus;

                terminalStatusChangeInternal = false;

                ToLog(1, ResourceManager.GetString("ToLogPenalCardRequest", DefaultCulture));
                s = string.Format(ResourceManager.GetString("ToLogPenalCardSum", DefaultCulture), AmountDue.ToString(), sValuteLabel);
                ToLog(1, s);
                if ((CardQuery > 0) && BankModuleWork)
                {
                    if (Paiments != PaimentMethodT.PaimentsCash)
                    {
                        BankQuerySumm = CardQuery;
                        BankQuerySummRequested = true;
                        BankModuleResultDescription = "";
                    }
                }
                if (vCardInfo == null)
                    CardInfo = lCardInfo;
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: PenalCardRequest: Exceptiion " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return lCardInfo;
        }

        private void DoCheckGiveStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            string s;
            if (GiveDeliveryThread != null && !GiveDeliveryStatus)
            {
                #region GiveDeliveryThread
                GiveDeliveryThread = null;
                if (CashDispenser != null)
                    CashDispenser.Pool = true;

                ResDiff = ThreadDeliveryResDiff;

                ToLog(1, ThreadDeliveryToLog);
                if (!IsCancelled)
                {
                    #region ������� ������ 
                    if (!IsPenalCard)
                    {
                        #region ������� �����
                        if (ResDiff == 0)
                        {
                            #region ������� ����� � ������ �� 0 � ����� ������
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card GiveDelivery Success");
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card Save card info to DB");
                            deviceCMModel.CountDown = DeliveryDownCount;
                            deviceCMModel.CountUp = DeliveryUpCount;
                            deviceCMModel.Count1 = Delivery1Count;
                            deviceCMModel.Count2 = Delivery2Count;
                            deviceCMModel.RejectCount = RejectCount;
                            deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                            //"DeviceId", DeviceId
                            comDiskret |= 1;
                            if (SlaveController != null)
                                SlaveController.ComDiskret = comDiskret;
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card Enable light");
                            LightTime = DateTime.Now;
                            Thread.Sleep(100);

                            deliveryStatus = DeliveryStatusT.Delivery;
                            terminalStatusChangeInternal = false;
                            #endregion
                        }
                        else
                        {
                            #region ������� ����� � ������ �� 0 � ����� ������ � �������
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card GiveDelivery Error");
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card Save Delivery Count info to DB");
                            deviceCMModel.CountDown = DeliveryDownCount;
                            deviceCMModel.CountUp = DeliveryUpCount;
                            deviceCMModel.Count1 = Delivery1Count;
                            deviceCMModel.Count2 = Delivery2Count;
                            deviceCMModel.RejectCount = RejectCount;
                            deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            comDiskret |= 1;
                            if (SlaveController != null)
                                SlaveController.ComDiskret = comDiskret;
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Not Penal card GiveDelivery Error Enable light");
                            LightTime = DateTime.Now;
                            Thread.Sleep(100);

                            PaymentErrorInfo.DeliveryError = true;
                            PaymentErrorInfo.Error = true;
                            PaymentErrorInfo.Paid = Paid;
                            PaymentErrorInfo.CardQuery = CardQuery;
                            PaymentErrorInfo.DownDiff = DownDiff;
                            PaymentErrorInfo.ResDiff = ResDiff;
                            terminalStatusChangeInternal = false;
                            TerminalStatusLabel += Properties.Resources.GetParkingCard;
                            s = string.Format(ResourceManager.GetString("DeliveryError", DefaultCulture), ResDiff.ToString(), sValuteLabel);
                            ToLog(0, s);

                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                            SendPayTransaction(TransactionTypeT.Payment_aprowed_with_error);

                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region �������� �����
                        if (ResDiff == 0)
                        {
                            #region �������� ����� � ������ �� 0 � ����� ������
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card GiveDelivery Success");
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Save card info to DB");
                            deviceCMModel.CountDown = DeliveryDownCount;
                            deviceCMModel.CountUp = DeliveryUpCount;
                            deviceCMModel.Count1 = Delivery1Count;
                            deviceCMModel.Count2 = Delivery2Count;
                            deviceCMModel.RejectCount = RejectCount;
                            deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            comDiskret |= 1;
                            if (SlaveController != null)
                                SlaveController.ComDiskret = comDiskret;
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Enable light");
                            LightTime = DateTime.Now;
                            Thread.Sleep(100);

                            deliveryStatus = DeliveryStatusT.Delivery;
                            terminalStatusChangeInternal = false;
                            #endregion
                        }
                        else
                        {
                            #region �������� ����� � ������ �� 0 � ����� ������ � �������
                            // ������� ����� � ������ �� 0 � ������� ����� ����������� � �������
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card GiveDelivery Error");
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Save Delivery Count info to DB");
                            deviceCMModel.CountDown = DeliveryDownCount;
                            deviceCMModel.CountUp = DeliveryUpCount;
                            deviceCMModel.Count1 = Delivery1Count;
                            deviceCMModel.Count2 = Delivery2Count;
                            deviceCMModel.RejectCount = RejectCount;
                            deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }

                            comDiskret |= 1;
                            if (SlaveController != null)
                                SlaveController.ComDiskret = comDiskret;
                            ToLog(2, "Cashier: DoCheckGiveStatus: Payment Penal card GiveDelivery Error Enable light");
                            LightTime = DateTime.Now;
                            Thread.Sleep(100);

                            PaymentErrorInfo.DeliveryError = true;
                            PaymentErrorInfo.Error = true;
                            PaymentErrorInfo.Paid = Paid;
                            PaymentErrorInfo.CardQuery = CardQuery;
                            PaymentErrorInfo.DownDiff = DownDiff;
                            PaymentErrorInfo.ResDiff = ResDiff;
                            terminalStatusChangeInternal = false;
                            TerminalStatusLabel += Properties.Resources.GetParkingCard;
                            s = string.Format(ResourceManager.GetString("DeliveryError", DefaultCulture), ResDiff.ToString(), sValuteLabel);
                            ToLog(0, s);

                            Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                            SendPayTransaction(TransactionTypeT.Payment_aprowed_with_error);

                            #endregion
                        }
                        #endregion
                    }
                    #endregion ������� ������ 
                }
                else
                {
                    #region ������  
                    if (ResDiff == 0)
                    #region DeliveryCorrect && ReturnPossible && RegularCard && Paid != 0
                    // ������ - ������� ����� � ������ �� 0 � ������� ����� �����������
                    {
                        ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card ");
                        ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Save Delivery Count info to DB");
                        deviceCMModel.CountDown = DeliveryDownCount;
                        deviceCMModel.CountUp = DeliveryUpCount;
                        deviceCMModel.Count1 = Delivery1Count;
                        deviceCMModel.Count2 = Delivery2Count;
                        deviceCMModel.RejectCount = RejectCount;
                        deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                        lock (db_lock)
                        {
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Utils.Running Enter");
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Utils.Running Leave");
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }

                        comDiskret |= 1;
                        if (SlaveController != null)
                            SlaveController.ComDiskret = comDiskret;
                        ToLog(2, "Cashier: Cancell Not Penal card Enable light");
                        LightTime = DateTime.Now;
                        Thread.Sleep(100);

                        Paid = 0;
                        PaidBanknote = 0;
                        PaidCoin = 0;
                        DownDiff = 0;
                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.CancellPaymentAfterIncomeMoneyReturned;
                        SendPayTransaction(TransactionTypeT.Payment_is_canceled_by_the_client_and_money_returned);
                        if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                        {
                            if ((CardDispenser != null) && CardDispenser.IsOpen)
                            {
                                if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                                    !CardDispenser.CardDispenserStatus.CardAccepting &&
                                    !CardDispenser.CardDispenserStatus.CardDispensing)
                                {
                                    if (terminalStatus != TerminalStatusT.Empty)
                                        terminalStatus = TerminalStatusT.Empty;
                                }
                                else
                                {
                                    if (terminalStatus != TerminalStatusT.CardEjected)
                                        terminalStatus = TerminalStatusT.CardEjected;
                                }
                            }
                            else
                            {
                                if (terminalStatus != TerminalStatusT.CardEjected)
                                    terminalStatus = TerminalStatusT.CardEjected;
                            }
                        }
                        else
                        {
                            if (terminalStatus != TerminalStatusT.CardFree)
                                terminalStatus = TerminalStatusT.CardFree;
                        }
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: DoCheckGiveStatus1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;

                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                        ToLog(1, ResourceManager.GetString("ToLogParkingCardEjecting", DefaultCulture));
                        #endregion DeliveryCorrect && ReturnPossible && RegularCard && Paid != 0
                    }
                    else
                    #region DeliveryError && ReturnPossible && RegularCard && Paid != 0
                    {  // ������ - ������� ����� � ������ �� 0 � ������� ����� ����������� � �������
                        ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card GiveDelivery Error");
                        ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Save Delivery Count info to DB");
                        deviceCMModel.CountDown = DeliveryDownCount;
                        deviceCMModel.CountUp = DeliveryUpCount;
                        deviceCMModel.Count1 = Delivery1Count;
                        deviceCMModel.Count2 = Delivery2Count;
                        deviceCMModel.RejectCount = RejectCount;
                        deviceCMModel.CashAcceptorCurrent = CashAcceptorCurrent;
                        lock (db_lock)
                        {
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Utils.Running Enter");
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card Utils.Running Leave");
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown, "Count1", deviceCMModel.Count1, "Count2", deviceCMModel.Count2, "RejectCount", deviceCMModel.RejectCount, "CashAcceptorCurrent", deviceCMModel.CashAcceptorCurrent }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }

                        comDiskret |= 1;
                        if (SlaveController != null)
                            SlaveController.ComDiskret = comDiskret;
                        ToLog(2, "Cashier: DoCheckGiveStatus: Cancell Not Penal card GiveDelivery Error Enable light");
                        LightTime = DateTime.Now;
                        Thread.Sleep(100);

                        PaymentErrorInfo.DeliveryError = true;
                        PaymentErrorInfo.Error = true;
                        PaymentErrorInfo.Paid = Paid;
                        PaymentErrorInfo.CardQuery = Paid;
                        PaymentErrorInfo.DownDiff = DownDiff;
                        PaymentErrorInfo.ResDiff = ResDiff;
                        terminalStatusChangeInternal = false;

                        TerminalStatusLabel += Properties.Resources.GetParkingCard;
                        s = string.Format(ResourceManager.GetString("DeliveryError", DefaultCulture), ResDiff.ToString(), sValuteLabel);
                        ToLog(0, s);


                        Transaction.PaymentTransactionType = PaymentTransactionTypeT.UnclosedPayment;
                        SendPayTransaction(TransactionTypeT.Payment_is_canceled_by_the_client_and_money_should_be_returned);

                        ToLog(1, ResourceManager.GetString("ToLogDeliveryUnavailable", DefaultCulture));
                        #endregion DeliveryError && ReturnPossible && RegularCard && Paid != 0
                    }
                    #endregion ������  
                }
                #endregion GiveDeliveryThread
            }
            if (GiveChequeThread != null && !GiveChequeStatus)
            {
                #region GiveChequeThread
                GiveChequeThread = null;
                if (
                    (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                   )
                {
                    KKMResultCode = KKM.ResultCode;
                    KKMResultDescription = KKM.ResultDescription;
                    ToLog(2, "Cashier: DoCheckGiveStatus: KKMResultCode" + KKMResultCode.ToString());
                    if ((KKMResultCode != CommonKKM.ErrorCode.Sucsess) && (KKMResultCode != CommonKKM.ErrorCode.NearPaperEnd))
                    {
                        Params = KKM.Params;
                        ChequePrinted = false;
                        if (KKMResultCode == CommonKKM.ErrorCode.MaxSessionTime && (deviceCMModel.ShiftAutoClose != null && (bool)deviceCMModel.ShiftAutoClose))
                        {
                            ToLog(2, "Cashier: DoCheckGiveStatus: MaxSessionTime ShiftSwitch");
                            ShiftSwitch();

                            ToLog(2, "Cashier: DoCheckGiveStatus: MaxSessionTime Restart GiveCheque");
                            GiveChequeStatus = true;
                            GiveChequeThread = new Thread(PrintCheck);
                            GiveChequeThread.Name = "GiveChequeThread";
                            GiveChequeThread.IsBackground = true;
                            GiveChequeThread.Start(Params);
                        }
                        else
                        {
                            PaymentErrorInfo.ChequeError = true;
                            PaymentErrorInfo.Error = true;
                            terminalStatusChangeInternal = false;
                        }
                    }
                    else
                    {
                        Params = null;
                        ChequeDT = DateTime.Now;
                        ChequePrinted = true;
                    }
                }
                else if ((deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists) &&
                                    (!EmulatorEnabled || !EmulatorForm.Emulator.PrinterVirt.Checked))
                {
                    PrinterResultCode = Printer.ResultCode;
                    PrinterResultDescription = Printer.ResultDescription;

                    ToLog(2, "Cashier: DoCheckGiveStatus: PrinterResultCode" + PrinterResultCode.ToString());
                    if ((PrinterResultCode != CommonPrinter.ErrorCode.Sucsess))
                    {
                        ToLog(2, "Cashier: DoCheckGiveStatus: PrintCheque Error");
                        Params = Printer.Params;
                        ChequePrinted = false;
                        PaymentErrorInfo.ChequeError = true;
                        PaymentErrorInfo.Error = true;
                        terminalStatusChangeInternal = false;
                    }
                    else
                    {
                        Params = null;
                        ChequeDT = DateTime.Now;
                        ChequePrinted = true;
                    }
                }
                #endregion GiveChequeThread
            }
            if (GiveChequeThread == null && GiveDeliveryThread == null)
            {
                #region LongOperation End
                if (PaymentErrorInfo.Error)
                {
                    if (PaymentErrorInfo.CashAcceptorError &&
                        !PaymentErrorInfo.CardDispenserError &&
                        !PaymentErrorInfo.CardWriteError &&
                        !PaymentErrorInfo.ChequeError &&
                        !PaymentErrorInfo.DeliveryError)
                        CurrentTransactionType = TransactionTypeT.Payment_aprowed;
                    else
                        CurrentTransactionType = TransactionTypeT.Payment_aprowed_with_error;
                    SendPayTransaction(CurrentTransactionType);
                    ToLog(2, "Cashier: DoCheckGiveStatus: DoSwitchPaymentError");
                    DoSwitchTerminalStatusPaymentError();
                }
                else
                {
                    /*if (KKMModel == "PrimFAKKM")
                    {
                        if (Paiments == PaimentMethodT.PaimentsBankModule)
                            GiveCheque(3, 0, 0, 0, 0, null, BankSlip);
                    }*/

                    if ((Paiments != PaimentMethodT.PaimentsBankModule) || (BankModuleStatus == BankModuleStatusT.BankCardEmpty))
                    {
                        if ((CardDispenser != null) && CardDispenser.IsOpen /*&& !CardDispensed*/)
                        {
                            //if (CardDispenser.CardDispenserStatus.CardPreSendPos)
                            {
                                ToLog(2, "Cashier: DoCheckGiveStatus: ������, ��� � ����� ������: CardDispenser.SendCardToBezelAndHold");
                                CardDispenser.SendCardToBezelAndHold();
                                while (CardDispenser.CommandStatus != CommandStatusT.Completed)
                                    Thread.Sleep(100);
                                //CardDispensed = true;
                                if (CardTimeOutToBasket > 0)
                                    CardEjectTime = DateTime.Now;
                            }
                        }

                        if (CurrentTransactionType == TransactionTypeT.Undefined)
                            CurrentTransactionType = TransactionTypeT.Payment_aprowed;

                        SendPayTransaction(CurrentTransactionType);

                        if (CashierType != 2)
                        {
                            if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                            {
                                if ((CardDispenser != null) && CardDispenser.IsOpen)
                                {
                                    if (!CardDispenser.CardDispenserStatus.CardDespensePos &&
                                    !CardDispenser.CardDispenserStatus.CardAccepting &&
                                    !CardDispenser.CardDispenserStatus.CardDispensing)
                                    {
                                        if (terminalStatus != TerminalStatusT.Empty)
                                            terminalStatus = TerminalStatusT.Empty;
                                    }
                                    else
                                    {
                                        if (terminalStatus != TerminalStatusT.CardEjected)
                                            terminalStatus = TerminalStatusT.CardEjected;
                                    }
                                }
                                /*else
                                {
                                    if (terminalStatus != TerminalStatusT.CardEjected)
                                        terminalStatus = TerminalStatusT.CardEjected;
                                }*/
                            }
                            else
                            {
                                if (terminalStatus != TerminalStatusT.CardFree)
                                    terminalStatus = TerminalStatusT.CardFree;
                            }
                        }
                        else
                        {
                            if (terminalStatus != TerminalStatusT.Empty)
                                terminalStatus = TerminalStatusT.Empty;
                        }
                        if (terminalStatusOld != terminalStatus)
                            ToLog(2, "Cashier: DoCheckGiveStatus2: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                        terminalStatusOld = terminalStatus;

                        if (EmulatorEnabled)
                            EmulatorForm.Emulator.terminalStatus = terminalStatus;
                        terminalStatusChangeInternal = false;
                    }

                    if (EmulatorEnabled)
                    {
                        if (EmulatorForm.Emulator.CardDispenserGroupBox.Visible)
                        {
                            if (EmulatorForm.Emulator.CardReaderEditsGroupBox.Visible)
                                EmulatorForm.Emulator.CardReaderEditsGroupBox.Hide();
                        }
                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Visible)
                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                        if (EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Visible)
                            EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Visible)
                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox2.Hide();
                        if (EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Visible)
                            EmulatorForm.Emulator.CoinAcceptorEditsGroupBox1.Hide();
                        if (EmulatorForm.Emulator.BankModuleEditsGroupBox.Visible)
                            EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();

                        if (EmulatorForm.Emulator.InsertCard.Enabled)
                            EmulatorForm.Emulator.InsertCard.Enabled = false;
                        //if (Paiments == PaimentMethodT.PaimentsCash)
                            if (!EmulatorForm.Emulator.TakeCard.Enabled)
                                EmulatorForm.Emulator.TakeCard.Enabled = true;
                    }
                }
                #endregion LongOperation End
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        ToTariffPlan.DateCard CardsOnline(ToTariffPlan.DateCard CardInfo, CardsT card)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            if (card.DateSaveCard != null)
            {
                int o_dt = (int)((DateTime)card.DateSaveCard - datetime0).TotalSeconds;

                if (o_dt > CardInfo.DateSaveCard)
                //(((DateTime)card.DateSaveCard).CompareTo((datetime0 + TimeSpan.FromSeconds(CardInfo.DateSaveCard))) > 0))
                {
                    if (card.ClientGroupidFC != null)
                        CardInfo.ClientGroupidFC = (byte)card.ClientGroupidFC;
                    if (card.ClientTypidFC != null)
                        CardInfo.ClientTypidFC = (byte)card.ClientTypidFC;
                    if (card.DateSaveCard != null)
                        CardInfo.DateSaveCard = (int)((DateTime)card.DateSaveCard - datetime0).TotalSeconds;
                    if (card.LastPaymentTime != null)
                        CardInfo.LastPaymentTime = (int)((DateTime)card.LastPaymentTime - datetime0).TotalSeconds;
                    if (card.LastRecountTime != null)
                        CardInfo.LastRecountTime = (int)((DateTime)card.LastRecountTime - datetime0).TotalSeconds;
                    if (card.Nulltime1 != null)
                        CardInfo.Nulltime1 = (int)((DateTime)card.Nulltime1 - datetime0).TotalSeconds;
                    if (card.Nulltime2 != null)
                        CardInfo.Nulltime2 = (int)((DateTime)card.Nulltime2 - datetime0).TotalSeconds;
                    if (card.Nulltime3 != null)
                        CardInfo.Nulltime3 = (int)((DateTime)card.Nulltime3 - datetime0).TotalSeconds;
                    if (card.ParkingEnterTime != null)
                        CardInfo.ParkingEnterTime = (int)((DateTime)card.ParkingEnterTime - datetime0).TotalSeconds;
                    if (card.SumOnCard != null)
                        CardInfo.SumOnCard = (int)card.SumOnCard;
                    if (card.TKVP != null)
                        CardInfo.TKVP = (byte)card.TKVP;
                    if (card.TPidFC != null)
                        CardInfo.TPidFC = (byte)card.TPidFC;
                    if (card.TSidFC != null)
                        CardInfo.TSidFC = (byte)card.TSidFC;
                    if (card.TVP != null)
                        CardInfo.TVP = (int)((DateTime)card.TVP - datetime0).TotalSeconds;
                    if (card.ZoneidFC != null)
                        CardInfo.ZoneidFC = (byte)card.ZoneidFC;

                    CardReader.CardInfo = CardInfo;
                    CardReader.WriteData();
                }
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return CardInfo;
        }
        private void SetCardStatus()
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            uint cardid;
            try
            {
                if (CardReader.CardExists && ((terminalStatus == TerminalStatusT.Empty) || (terminalStatus == TerminalStatusT.CardInserted) || (terminalStatus == TerminalStatusT.PenalCardPaiment) || (terminalStatus == TerminalStatusT.PenalCardPrePaiment)))
                {
                    ReaderIDCardHex = CardReader.CardId;
                    uint.TryParse(ReaderIDCardHex, NumberStyles.HexNumber, null, out cardid);
                    ReaderIDCardDec = cardid.ToString();
                    CardInfo = CardReader.CardInfo;
                    ReaderEntranceTime = (datetime0 + TimeSpan.FromSeconds(CardInfo.ParkingEnterTime)).ToString(); // ����� ������

                    ReaderZoneTime = (datetime0 + TimeSpan.FromSeconds(CardInfo.LastRecountTime)).ToString(); // ����� ������ � ����
                    ReaderTariffID = CardInfo.TPidFC;

                    ReaderCardBalance = CardInfo.SumOnCard.ToString(); //������ �����
                    ClientType = CardInfo.ClientTypidFC;
                }
                else if ((terminalStatus == TerminalStatusT.CardInserted) && (CardReader.SoftStatus == SoftReaderStatusT.CardNotExists))
                {
                    terminalStatus = TerminalStatusT.CardReadedError;
                }
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: SetCardStatus: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
            }
            catch (Exception exc)
            {
                ToLog(0, "Cashier: SetCardStatus: exception " + exc.ToString());
            }
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
        }
        CardsT SetCard(DataRow Card)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            CardsT c = new CardsT();
            c.Blocked = (byte?)Card.GetInt("Blocked");
            c._IsDeleted = Card.GetBool("_IsDeleted");
            c.Sync = Card.GetDateTime("Sync");
            c.CardId = (long)Card.GetLong("CardId");
            c.ClientId = Card.GetGuid("ClientId");
            c.CompanyId = Card.GetGuid("CompanyId");
            c.ParkingEnterTime = Card.GetDateTime("ParkingEnterTime");
            c.LastRecountTime = Card.GetDateTime("LastRecountTime");
            c.LastPaymentTime = Card.GetDateTime("LastPaymentTime");
            c.TSidFC = (byte?)Card.GetInt("TSidFC");
            c.TPidFC = (byte?)Card.GetInt("TPidFC");
            c.ZoneidFC = (byte?)Card.GetInt("ZoneidFC");
            c.ClientGroupidFC = (byte?)Card.GetInt("ClientGroupidFC");
            c.SumOnCard = Card.GetInt("SumOnCard");
            c.Nulltime1 = Card.GetDateTime("Nulltime1");
            c.Nulltime2 = Card.GetDateTime("Nulltime2");
            c.Nulltime3 = Card.GetDateTime("Nulltime3");
            c.TVP = Card.GetDateTime("TVP");
            c.TKVP = Card.GetInt("TKVP");
            c.ClientTypidFC = Card.GetInt("ClientTypidFC");
            c.DateSaveCard = Card.GetDateTime("DateSaveCard");
            c.LastPlate = Card.GetString("LastPlate");
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return c;
        }
        public TariffScheduleModelT SetTarifiTS(DataRow TarifiTS)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            TariffScheduleModelT ts = new TariffScheduleModelT();
            ts._IsDeleted = (bool)TarifiTS.GetBool("_IsDeleted");
            ts.Sync = TarifiTS.GetDateTime("Sync");
            ts.Id = (Guid)TarifiTS.GetGuid("Id");
            ts.Name = TarifiTS.GetString("Name");
            ts.IdFC = (byte?)TarifiTS.GetInt("IdFC");
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return ts;
        }
        public TariffsT SetTarifiTP(DataRow TarifiTP)
        {
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Enter");
            TariffsT t = new TariffsT();
            t._IsDeleted = (bool)TarifiTP.GetBool("_IsDeleted");
            t.Sync = TarifiTP.GetDateTime("Sync");
            t.Id = (Guid)TarifiTP.GetGuid("Id");
            t.Name = TarifiTP.GetString("Name");
            t.IdFC = (byte?)TarifiTP.GetInt("IdFC");
            t.ChangingTypeId = TarifiTP.GetInt("ChangingTypeId");
            t.TypeId = (int)TarifiTP.GetInt("TypeId");
            t.Initial = (int)TarifiTP.GetInt("Initial");
            t.InitialTimeTypeId = TarifiTP.GetInt("InitialTimeTypeId");
            t.InitialAmount = (int)TarifiTP.GetInt("InitialAmount");
            t.ProtectedInterval = (int)TarifiTP.GetInt("ProtectedInterval");
            t.ProtectedIntervalTimeTypeId = TarifiTP.GetInt("ProtectedIntervalTimeTypeId");
            t.FreeTime = (int)TarifiTP.GetInt("FreeTime");
            t.FreeTimeTypeId = TarifiTP.GetInt("FreeTimeTypeId");
            if (DetailLog)
                ToLog(2, "Cashier: " + new StackTrace(false).GetFrame(0).GetMethod().Name + " Leave");
            return t;
        }
    }
}