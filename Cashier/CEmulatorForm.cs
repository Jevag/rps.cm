﻿using CommonClassLibrary;
using System;
using System.Windows.Forms;

namespace Cashier
{
    public partial class CEmulatorForm : Form
    {

        public MainWindow cashierForm = null;
        public bool smallTable = false;

        public bool PaintOn = false;
        public CEmulatorForm()
        {
            //            LockWindowUpdate(this.Handle);
            InitializeComponent();
        }
        public CEmulatorForm(MainWindow MainForm)
        {
            InitializeComponent();
            cashierForm = MainForm;
            //            button1.Hide();
            /*if (Emulator.TerminalWork == TerminalWorkT.TerminalClosed)
                button1.BackColor = System.Drawing.Color.Red;
            else
                button1.BackColor = System.Drawing.Color.Green;
             */
            /*
            if ((cashierForm.TechForm != null) && (cashierForm.TechForm.OtherSettingsPanel != null))
            {
                Emulator.CasseKey = cashierForm.TechForm.OtherSettingsPanel.CasseKey.Text;
            }
             */
        }
        private void ConsoleLog_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ConsoleLog.SelectionStart = ConsoleLog.Text.Length;
                ConsoleLog.ScrollToCaret();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        private void CEmulatorForm_Shown(object sender, EventArgs e)
        {
            try
            {
                ConsoleLog.ScrollToCaret();
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        private void StartCasse_Click(object sender, EventArgs e)
        {
            try
            {
                button1.Show();

                if (Emulator.TerminalWork == TerminalWorkT.TerminalClosed)
                {
                    Emulator.TerminalWork = TerminalWorkT.TerminalWork;
                    cashierForm.ExternalDeviceTimer.Enabled = true;
                }

                if (cashierForm.ExternalDeviceTimer.Enabled)
                    button1.BackColor = System.Drawing.Color.Green;
                else
                    button1.BackColor = System.Drawing.Color.Red;

                /*            if ((cashierForm.TechForm != null) && (cashierForm.TechForm.OtherSettingsPanel != null))
                            {
                                Emulator.CasseKey = cashierForm.TechForm.OtherSettingsPanel.CasseKey.Text;
                            }*/
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        private void CEmulatorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!cashierForm.WindowClosing)
                try
                {
                    cashierForm.Close();
                }
                catch (Exception exc)
                {
                    //cashierForm.ToLog(0, exc.ToString());
                }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                cashierForm.ExternalDeviceTimer.Enabled = !cashierForm.ExternalDeviceTimer.Enabled;
                if (cashierForm.ExternalDeviceTimer.Enabled)
                    button1.BackColor = System.Drawing.Color.Green;
                else
                    button1.BackColor = System.Drawing.Color.Red;
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        private void ExternalDeviceTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (cashierForm.ExternalDeviceTimer.Enabled)
                {
                    if (button1.BackColor != System.Drawing.Color.Green)
                        button1.BackColor = System.Drawing.Color.Green;
                }
                else
                {
                    if (button1.BackColor != System.Drawing.Color.Red)
                        button1.BackColor = System.Drawing.Color.Red;
                }

                if ((Emulator != null) && (cashierForm.TechForm != null))
                {
                    if (Emulator.CasseKey != cashierForm.TechForm.CashierKey.Password)
                        Emulator.CasseKey = cashierForm.TechForm.CashierKey.Password;
                }
                cashierForm.ExternalDeviceTimer_Tick(sender, e);
            }
            catch (Exception exc)
            {
                cashierForm.ToLog(0, exc.ToString());
            }
        }

        private void CEmulatorForm_Deactivate(object sender, EventArgs e)
        {
            //cashierForm.Topmost = true;
            //cashierForm.Activate();
            //this.Topmost = false;
        }

        private void CEmulatorForm_Enter(object sender, EventArgs e)
        {
            //            this.Topmost = true;
            //cashierForm.Topmost = false;
        }
    }
}
