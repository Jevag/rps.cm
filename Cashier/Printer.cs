﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Windows.Media;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime PrinterAlarmTime;
        void HardPrinter()
        {
            //ToLog(2, "Cashier: HardPrinter");
            try
            {
                if (GiveChequeThread == null)
                {
                    if (Printer != null)
                    {
                        if ((PrinterStatusCheckDT == null) || (PrinterStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                        {
                            if (!Printer.DeviceEnabled)
                            {
                                ToLog(2, "Cashier: HardPrinter: Printer.Open");
                                Printer.Open(PrinterPort, 9600, 8, 1, System.IO.Ports.Parity.None, CommonPrinter.FlowControlT.XON_XOFF);
                            }
                            PrinterStatusCheckDT = DateTime.Now;
                        }
                        PrinterResultDescription = Printer.ResultDescription;

                        if (!Printer.DeviceEnabled || Printer.HardStatus.Error || (PrinterResultCode != CommonPrinter.ErrorCode.Sucsess))
                        {
                            if (TechForm.PrinterPortLabel.Foreground != Brushes.Green)
                                TechForm.PrinterPortLabel.Foreground = Brushes.Green;
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("TechPrinterCheckReq", DefaultCulture) + Environment.NewLine;

                            if ((PrinterResultDescription != null) && (Printer.ResultDescription != ""))
                                ErrorsText += PrinterResultDescription + Environment.NewLine;

                            if (PrinterLevel != AlarmLevelT.Brown)
                            {
                                if (PrinterAlarmTime == DateTime.MinValue)
                                {
                                    PrinterAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (PrinterAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        PrinterLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.Printer;
                                        if ((PrinterResultCode == CommonPrinter.ErrorCode.NotConnect) || (PrinterResultCode == CommonPrinter.ErrorCode.PortAccessError))
                                        {
                                            Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                        }
                                        else
                                        {
                                            if (Printer.HardStatus.PaperEnd)
                                                Alarm.ErrorCode = AlarmCode.PaperEnd;
                                            else if (Printer.HardStatus.CoverOpen)
                                                Alarm.ErrorCode = AlarmCode.CoverOpen;
                                            else if (Printer.HardStatus.PrinterCutMechError)
                                                Alarm.ErrorCode = AlarmCode.PrinterCutMechError;
                                            else if (Printer.HardStatus.PrinterHeadHot)
                                                Alarm.ErrorCode = AlarmCode.PrinterHeadHot;
                                            else
                                                Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                        }

                                        SendAlarm(Alarm);
                                        PrinterAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                            return;
                        }
                        else if (Printer.HardStatus.PaperNearEnd)
                        {
                            if (PrinterLevel != AlarmLevelT.Yellow)
                            {
                                if (PrinterAlarmTime == DateTime.MinValue)
                                {
                                    PrinterAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (PrinterAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        PrinterLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                        Alarm.AlarmSource = AlarmSourceT.Printer;
                                        Alarm.ErrorCode = AlarmCode.PaperNearEnd;
                                        SendAlarm(Alarm);
                                        PrinterAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }

                        if (!Printer.DeviceEnabled && (terminalStatus != TerminalStatusT.CardPaid) && (terminalStatus != TerminalStatusT.PaymentError))
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void GetPrinterStatus()
        {
            //ToLog(2, "Cashier: GetPrinterStatus");
            try
            {
                if (deviceCMModel.PrinterExists != null && (bool)deviceCMModel.PrinterExists)
                {
                    if (EmulatorEnabled && EmulatorForm.Emulator.PrinterVirt.Checked)
                    {
                        if ((Printer != null) && (Printer.DeviceEnabled))
                        {
                            ToLog(2, "Cashier: GetPrinterStatus: Printer.Close");
                            Printer.Close();
                            PrinterWorkChanged = true;
                        }
                        if (!EmulatorForm.Emulator.PrinterWork.Checked)
                        {
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("TechPrinterCheckReq", DefaultCulture) + Environment.NewLine;
                            if (PrinterLevel != AlarmLevelT.Brown)
                            {
                                if (PrinterLevel != AlarmLevelT.Yellow)
                                {
                                    if (PrinterAlarmTime == DateTime.MinValue)
                                    {
                                        PrinterAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (PrinterAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.Printer;
                                            Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                            SendAlarm(Alarm);
                                            PrinterAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {

                            if (PrinterAlarmTime != DateTime.MinValue)
                                PrinterAlarmTime = DateTime.MinValue;
                            if (PrinterLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.Printer;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("PrinterWork", DefaultCulture);
                                SendAlarm(Alarm);
                                PrinterLevel = AlarmLevelT.Green;
                            }
                        }
                    }
                    else
                    {
                        if (Printer == null)
                        {
                            try
                            {
                                Printer = new TKiosk(log);
                            }
                            catch { Printer = null; }
                            PrinterWorkChanged = true;
                        }

                        HardPrinter();
                    }
                }
                else
                {
                    if ((Printer != null) && (Printer.DeviceEnabled))
                    {
                        ToLog(2, "Cashier: GetPrinterStatus: Printer.Close");
                        Printer.Close();
                        PrinterWorkChanged = true;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}