﻿using CommonClassLibrary;
using System;
using System.Windows;
using System.Globalization;
using System.Collections.Generic;
using Rps.ShareBase;
using SqlBaseImport.Model;
using System.Threading;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public void ShiftSwitch()
        {
            //ToLog(2, "Cashier: ShiftSwitch");
            try
            {
                ToLog(2, "Cashier: ShiftSwitch: ShiftClose");
                ShiftClose(true);
                ToLog(2, "Cashier: ShiftSwitch: ShiftOpen");
                ShiftOpen(true);

                /*if (EmulatorEnabled)
                    EmulatorForm.Emulator.ShiftStatusLabel1.Text = (cashierForm.ShiftOpened ? Properties.Resources.ShiftOpened : Properties.Resources.ShiftClosed);*/
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void ShiftOpen(bool Auto)
        {
            //ToLog(2, "Cashier: ShiftOpen");
            try
            {
                if ((deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                    )
                {
                    if ((KKM != null) && KKM.Work)
                    {
                        //ToLog(2, "Cashier: ShiftOpen: KKM.Shift_Open");
                        KKM.Shift_Open();
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if (KKMResultCode == CommonKKM.ErrorCode.Sucsess || KKMResultCode == CommonKKM.ErrorCode.ShiftAlreadyOpen) // Без ошибок
                        {
                            CurrentTransactionNumber = 1;
                            LastShiftOpenTime = DateTime.Now;
                            ShiftOpened = true;
                            deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                            deviceCMModel.LastShiftOpenTime = LastShiftOpenTime;
                            deviceCMModel.ShiftNumber++;
                            deviceCMModel.ShiftOpened = true;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: ShiftOpen Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: ShiftOpen Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                                        {   "DeviceId", DeviceId,
                                    "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                                    "LastShiftOpenTime", deviceCMModel.LastShiftOpenTime,
                                    "ShiftNumber", deviceCMModel.ShiftNumber,
                                    "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                                            {   "DeviceId", DeviceId,
                                    "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                                    "LastShiftOpenTime", deviceCMModel.LastShiftOpenTime,
                                    "ShiftNumber", deviceCMModel.ShiftNumber,
                                    "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            if (KKMLevel != AlarmLevelT.Brown)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                KKMLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.KKM;
                                if ((KKMResultCode == CommonKKM.ErrorCode.NotConnect) || (KKMResultCode == CommonKKM.ErrorCode.PaperOut) || (KKMResultCode == CommonKKM.ErrorCode.OtherError))
                                {
                                    if (KKM.HardStatus.PaperEnd)
                                        Alarm.ErrorCode = AlarmCode.PaperEnd;
                                    else if (KKM.HardStatus.CoverOpen)
                                        Alarm.ErrorCode = AlarmCode.CoverOpen;
                                    else if (KKM.HardStatus.PaperTransporByHand)
                                        Alarm.ErrorCode = AlarmCode.PaperTransporByHand;
                                    else if (KKM.HardStatus.PrinterCutMechError)
                                        Alarm.ErrorCode = AlarmCode.PrinterCutMechError;
                                    else if (KKM.HardStatus.PrinterFatalError)
                                        Alarm.ErrorCode = AlarmCode.PrinterFatalError;
                                    else if (KKM.HardStatus.PrinterHeadHot)
                                        Alarm.ErrorCode = AlarmCode.PrinterHeadHot;
                                    else if (KKM.HardStatus.PrinterMechError)
                                        Alarm.ErrorCode = AlarmCode.PrinterMechError;
                                    else if (KKM.HardStatus.StopByPaperEnd)
                                        Alarm.ErrorCode = AlarmCode.StopByPaperEnd;
                                    else
                                        Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                }
                                else
                                {
                                    Alarm.ErrorCode = AlarmCode.KKMModeError;
                                }
                                SendAlarm(Alarm);
                            }
                            ShiftOpened = false;
                        }
                    }
                }
                else
                {
                    CurrentTransactionNumber = 1;
                    LastShiftOpenTime = DateTime.Now;
                    ShiftOpened = true;
                    deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                    deviceCMModel.LastShiftOpenTime = LastShiftOpenTime;
                    deviceCMModel.ShiftNumber++;
                    deviceCMModel.ShiftOpened = true;
                    lock (db_lock)
                    {
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: ShiftOpen Utils.Running Enter");
                        //while (Utils.Running)
                        //    Thread.Sleep(50);
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: ShiftOpen Utils.Running Leave");
                        try
                        {
                            UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                                {   "DeviceId", DeviceId,
                                    "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                                    "LastShiftOpenTime", deviceCMModel.LastShiftOpenTime,
                                    "ShiftNumber", deviceCMModel.ShiftNumber,
                                    "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                                    {   "DeviceId", DeviceId,
                                    "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                                    "LastShiftOpenTime", deviceCMModel.LastShiftOpenTime,
                                    "ShiftNumber", deviceCMModel.ShiftNumber,
                                    "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                }
                if (ShiftOpened && (TerminalWork != TerminalWorkT.TerminalClosed))
                {
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                    {
                        if (BankModule != null)
                        {
                            if (BankModule.DeviceEnabled)
                            {
                                BankModule.WorkKey();
                                if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                                    WorkKeyLoaded = true;
                                else
                                    WorkKeyCheckTime = DateTime.Now + TimeSpan.FromMinutes(5);
                            }
                        }
                    }
                }
                if (ShiftOpened && (TerminalWork != TerminalWorkT.TerminalClosed))
                {
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                    {
                        if (CashDispenser != null)
                        {
                            bool res;
                            ToLog(2, "Cashier: ShiftOpen: CashDispenser.Open");
                            if (CashDispenserModel == "Multimech")
                                res = CashDispenser.Open(CashDispenserPort);
                            else
                            {
                                if (!CashDispenser.IsOpen)
                                    res = CashDispenser.Open(CashAcceptorPort);
                                else
                                    res = true;
                            }
                            if (res)
                            {
                                ToLog(2, "Cashier: ShiftOpen: CashDispenser.OpenCassette");
                                CashDispenser.OpenCassette();
                                ToLog(2, "Cashier: ShiftOpen: CashDispenser.ReadCassetteID");
                                CashDispenser.ReadCassetteID();
                                if (
                                    (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                    (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                    )
                                {
                                    ToLog(1, string.Format("Cashier: ShiftOpen: CashAcceptor.CashAcceptorStatus.Cassettes[1].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                    if (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                    {
                                        ToLog(1, "Cashier: ShiftOpen: DeliveryUpCount Empty");
                                        DeliveryUpCount = 0;
                                        CashDispenser.SetCount(1, 0);
                                        DeliveryUpCountChanged = true;
                                    }
                                }
                                if (
                                    (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                    (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                    )
                                {
                                    ToLog(1, string.Format("Cashier: ShiftOpen: CashAcceptor.CashAcceptorStatus.Cassettes[2].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                    if (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                    {
                                        ToLog(1, "Cashier: ShiftOpen: DeliveryDownCount Empty");
                                        DeliveryDownCount = 0;
                                        CashDispenser.SetCount(2, 0);
                                        DeliveryDownCountChanged = true;
                                    }
                                }
                                try
                                {
                                    if (DeliveryUpCountChanged || DeliveryDownCountChanged)
                                    {
                                        lock (db_lock)
                                        {
                                            deviceCMModel.CountUp = DeliveryUpCount;
                                            deviceCMModel.CountDown = DeliveryDownCount;
                                            lock (db_lock)
                                            {
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: ShiftOpen Utils.Running Enter");
                                                //while (Utils.Running)
                                                //    Thread.Sleep(50);
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: ShiftOpen Utils.Running Leave");
                                                try
                                                {
                                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                                }
                                                catch (Exception exc)
                                                {
                                                    ToLog(0, exc.ToString());
                                                    try
                                                    {
                                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                                    }
                                                    catch (Exception exc1)
                                                    {
                                                        ToLog(0, exc1.ToString());
                                                    }
                                                }
                                            }
                                        }
                                        DeliveryUpCountChanged = false;
                                        DeliveryDownCountChanged = false;
                                    }
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                }
                                if (CashDispenser.IsOpen)
                                    CashDispenser.Pool = true;
                            }
                        }
                    }
                    if (TerminalLevel != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.Terminal;
                        Alarm.ErrorCode = AlarmCode.ShiftOpened;
                        SendAlarm(Alarm);
                        TerminalLevel = AlarmLevelT.Green;
                    }
                    /*if (EmulatorEnabled)
                        EmulatorForm.Emulator.ShiftStatusLabel1.Text = ResourceManager.GetString("ShiftOpened", DefaultCulture);*/
                }
                ShiftStatusChanged = true;
                if (terminalStatus == TerminalStatusT.ShiftSwitchNeed)
                    terminalStatus = TerminalStatusT.Empty;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: ShiftOpen1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void ShiftClose(bool Auto)
        {
            //ToLog(2, "Cashier: ShiftClose");

            try
            {
                BMVerification();

                if ((deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                    )
                {
                    if ((KKM != null) && KKM.Work)
                    {
                        if (Auto)
                        {
                            //ToLog(2, "Cashier: ShiftClose: GiveCheque (Verification)");
                            //SaveVerificationReceipt(BankModuleAuthReceipt);
                            ToLog(2, "Cashier: ShiftClose: KKM.Shift_Close_Ex");
                            KKM.Shift_Close_Ex();
                        }
                        else
                        {
                                KKM.SetPrezenter(0, 0, 0);
                            ToLog(2, "Cashier: ShiftClose: KKM.Shift_Close");
                            KKM.Shift_Close();
                        }
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                    }
                    if ((KKM == null) ||
                        !KKM.DeviceEnabled ||
                        ((KKMResultCode != CommonKKM.ErrorCode.Sucsess) && // Без ошибок
                        (KKMResultCode != CommonKKM.ErrorCode.ZReportNeeded) && // Требуется общее гашение
                        (KKMResultCode != CommonKKM.ErrorCode.MaxSessionTime)) // Смена превысила 24 часа
                        )
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                        if (KKMLevel != AlarmLevelT.Brown)
                        {
                            AlarmT Alarm = new AlarmT();
                            KKMLevel = AlarmLevelT.Brown;
                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                            Alarm.AlarmSource = AlarmSourceT.KKM;
                            if ((KKMResultCode == CommonKKM.ErrorCode.NotConnect) || (KKMResultCode == CommonKKM.ErrorCode.PaperOut) || (KKMResultCode == CommonKKM.ErrorCode.OtherError))
                            {
                                if (KKM.HardStatus.PaperEnd)
                                    Alarm.ErrorCode = AlarmCode.PaperEnd;
                                else if (KKM.HardStatus.CoverOpen)
                                    Alarm.ErrorCode = AlarmCode.CoverOpen;
                                else if (KKM.HardStatus.PaperTransporByHand)
                                    Alarm.ErrorCode = AlarmCode.PaperTransporByHand;
                                else if (KKM.HardStatus.PrinterCutMechError)
                                    Alarm.ErrorCode = AlarmCode.PrinterCutMechError;
                                else if (KKM.HardStatus.PrinterFatalError)
                                    Alarm.ErrorCode = AlarmCode.PrinterFatalError;
                                else if (KKM.HardStatus.PrinterHeadHot)
                                    Alarm.ErrorCode = AlarmCode.PrinterHeadHot;
                                else if (KKM.HardStatus.PrinterMechError)
                                    Alarm.ErrorCode = AlarmCode.PrinterMechError;
                                else if (KKM.HardStatus.StopByPaperEnd)
                                    Alarm.ErrorCode = AlarmCode.StopByPaperEnd;
                                else
                                    Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                            }
                            else
                            {
                                Alarm.ErrorCode = AlarmCode.KKMModeError;
                            }
                            SendAlarm(Alarm);
                            return;
                        }
                    }
                    else
                        ShiftOpened = false;
                }
                else
                    ShiftOpened = false;

                deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                deviceCMModel.ShiftOpened = ShiftOpened;
                lock (db_lock)
                {
                    //if (DetailLog)
                    //    ToLog(2, "Cashier: ShiftClose Utils.Running Enter");
                    //while (Utils.Running)
                    //    Thread.Sleep(50);
                    //if (DetailLog)
                    //    ToLog(2, "Cashier: ShiftClose Utils.Running Leave");
                    try
                    {
                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                            {   "DeviceId", DeviceId,
                        "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                        "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                    }
                    catch (Exception exc)
                    {
                        ToLog(0, exc.ToString());
                        try
                        {
                            UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[]
                                {   "DeviceId", DeviceId,
                        "CurrentTransactionNumber", deviceCMModel.CurrentTransactionNumber,
                        "ShiftOpened", deviceCMModel.ShiftOpened }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                        }
                        catch (Exception exc1)
                        {
                            ToLog(0, exc1.ToString());
                        }
                    }
                }
                if (!ShiftOpened)
                {
                    if (!Auto)
                    {

                        if (TerminalLevel != AlarmLevelT.Red)
                        {
                            AlarmT Alarm = new AlarmT();
                            TerminalLevel = AlarmLevelT.Red;
                            Alarm.AlarmLevel = AlarmLevelT.Red;
                            Alarm.AlarmSource = AlarmSourceT.Terminal;
                            Alarm.ErrorCode = AlarmCode.ShiftClosed;
                            SendAlarm(Alarm);
                        }
                    }

                    LastShiftOpenTime = null;

                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                    {
                        if ((CashDispenser != null) && CashDispenser.IsOpen)
                            lock (CashDispenser)
                            {
                                CashDispenser.CloseCassette();
                            }
                    }
                    /*if (EmulatorEnabled)
                        EmulatorForm.Emulator.ShiftStatusLabel1.Text = Properties.Resources.ShiftClosed;*/
                }
                else
                {

                    if (TerminalLevel != AlarmLevelT.Red)
                    {
                        AlarmT Alarm = new AlarmT();
                        TerminalLevel = AlarmLevelT.Red;
                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                        Alarm.AlarmSource = AlarmSourceT.Terminal;
                        Alarm.ErrorCode = AlarmCode.KKMModeError;
                        SendAlarm(Alarm);
                    }
                }

                ShiftStatusChanged = true;

                if (terminalStatus == TerminalStatusT.ShiftSwitchNeed)
                    terminalStatus = TerminalStatusT.Empty;
                if (terminalStatusOld != terminalStatus)
                    ToLog(2, "Cashier: ShiftClose1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                terminalStatusOld = terminalStatus;

                if ((KKM != null) && KKM.Work)
                {

                    ToLog(2, "Cashier: ShiftClose: KKM.GetResources");
                    KKM.GetResources();
                    LastZReport = KKM.Resources.LastZNumber;
                }
                else
                {
                    LastZReport = 0;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void PrintXReport()
        {
            //ToLog(2, "Cashier: PrintXReport");
            try
            {
                if ((deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists) &&
                    (!EmulatorEnabled || !EmulatorForm.Emulator.KKMVirt.Checked)
                    )
                {
                    if ((KKM != null) && KKM.Work)
                    {
                        ToLog(2, "Cashier: PrintXReport: KKM.XReport");
                        KKM.XReport();
                        KKMResultCode = KKM.ResultCode;
                        KKMResultDescription = KKM.ResultDescription;
                        if (KKMResultCode != CommonKKM.ErrorCode.Sucsess) // Без ошибок
                        {
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            if (KKMLevel != AlarmLevelT.Brown)
                            {
                                AlarmT Alarm = new AlarmT();
                                KKMLevel = AlarmLevelT.Brown;
                                Alarm.AlarmLevel = AlarmLevelT.Brown;
                                Alarm.AlarmSource = AlarmSourceT.KKM;
                                if ((KKMResultCode == CommonKKM.ErrorCode.NotConnect) || (KKMResultCode == CommonKKM.ErrorCode.PaperOut) || (KKMResultCode == CommonKKM.ErrorCode.OtherError))
                                {
                                    if (KKM.HardStatus.PaperEnd)
                                        Alarm.ErrorCode = AlarmCode.PaperEnd;
                                    else if (KKM.HardStatus.CoverOpen)
                                        Alarm.ErrorCode = AlarmCode.CoverOpen;
                                    else if (KKM.HardStatus.PaperTransporByHand)
                                        Alarm.ErrorCode = AlarmCode.PaperTransporByHand;
                                    else if (KKM.HardStatus.PrinterCutMechError)
                                        Alarm.ErrorCode = AlarmCode.PrinterCutMechError;
                                    else if (KKM.HardStatus.PrinterFatalError)
                                        Alarm.ErrorCode = AlarmCode.PrinterFatalError;
                                    else if (KKM.HardStatus.PrinterHeadHot)
                                        Alarm.ErrorCode = AlarmCode.PrinterHeadHot;
                                    else if (KKM.HardStatus.PrinterMechError)
                                        Alarm.ErrorCode = AlarmCode.PrinterMechError;
                                    else if (KKM.HardStatus.StopByPaperEnd)
                                        Alarm.ErrorCode = AlarmCode.StopByPaperEnd;
                                    else
                                        Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                }
                                else
                                {
                                    Alarm.ErrorCode = AlarmCode.KKMModeError;
                                }
                                SendAlarm(Alarm);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        void ShiftFromKKM()
        {
            //ToLog(2, "Cashier: ShiftFromKKM");
            try
            {
                if ((KKM != null) && (KKM.Work || (KKM.Initialised && !KKM.Work)))
                {
                    ToLog(2, "Cashier: ShiftFromKKM: KKM.ShiftFromKKM");
                    bool sho = KKM.ShiftFromKKM();

                    if (ShiftOpened != sho)
                        ShiftStatusChanged = true;
                    ShiftOpened = sho;
                    KKMResultCode = KKM.ResultCode;
                    KKMResultDescription = KKM.ResultDescription;

                    if (ShiftOpened)
                    {
                        LastShiftOpenTime = KKM.Resources.LastShiftOpenTime;
                        if (LastShiftOpenTime != null)
                            ToLog(1, ResourceManager.GetString("LastOpenShiftTime", DefaultCulture) + ((DateTime)LastShiftOpenTime).ToString(ldt_format));
                        ToLog(1, ResourceManager.GetString("LastShiftNumber", DefaultCulture) + KKM.Resources.LastZNumber.ToString());
                        if (KKM.Resources.LastZNumber != 0 && deviceCMModel.ShiftNumber != KKM.Resources.LastZNumber + 1)
                        {
                            deviceCMModel.ShiftNumber = KKM.Resources.LastZNumber + 1;
                            lock (db_lock)
                            {
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: ShiftFromKKM Utils.Running Enter");
                                //while (Utils.Running)
                                //    Thread.Sleep(50);
                                //if (DetailLog)
                                //    ToLog(2, "Cashier: ShiftFromKKM Utils.Running Leave");
                                try
                                {
                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "ShiftNumber", deviceCMModel.ShiftNumber }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                }
                                catch (Exception exc)
                                {
                                    ToLog(0, exc.ToString());
                                    try
                                    {
                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "ShiftNumber", deviceCMModel.ShiftNumber }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                    }
                                    catch (Exception exc1)
                                    {
                                        ToLog(0, exc1.ToString());
                                    }
                                }
                            }
                        }
                        if (((KKMResultCode == CommonKKM.ErrorCode.MaxSessionTime) || ((LastShiftOpenTime + new TimeSpan(23, 59, 59)) < DateTime.Now)) && deviceCMModel.ShiftAutoClose != null && (bool)deviceCMModel.ShiftAutoClose)
                        {
                            if (CashierType != 2)
                            {
                                ToLog(2, "Cashier: ShiftFromKKM: ShiftSwitch");
                                ShiftSwitch();
                                CurrentTransactionNumber = 1;
                                deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                            }
                            else
                            {
                                terminalStatus = TerminalStatusT.ShiftSwitchNeed;
                            }
                            if (terminalStatusOld != terminalStatus)
                                ToLog(2, "Cashier: ShiftFromKKM1: terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                            terminalStatusOld = terminalStatus;
                        }
                    }
                    else
                    {
                        CurrentTransactionNumber = 0;
                        deviceCMModel.CurrentTransactionNumber = (int)CurrentTransactionNumber;
                    }

                    if (ShiftOpened)
                    {
                        if (TerminalLevel != AlarmLevelT.Green)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.Terminal;
                            Alarm.ErrorCode = AlarmCode.ShiftOpened;
                            SendAlarm(Alarm);
                            TerminalLevel = AlarmLevelT.Green;
                        }

                        if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                        {
                            if (CashDispenser != null)
                            {
                                bool res;
                                if (CashDispenserModel == "Multimech")
                                    res = CashDispenser.Open(CashDispenserPort);
                                else
                                {
                                    if (!CashDispenser.IsOpen)
                                        res = CashDispenser.Open(CashAcceptorPort);
                                    else
                                        res = true;
                                }
                                if (res)
                                {
                                    ToLog(2, "Cashier: ShiftFromKKM: CashDispenser.CheckStatus");
                                    CashDispenser.CheckStatus();
                                    ToLog(2, "Cashier: ShiftFromKKM: CashDispenser.OpenCassette");
                                    CashDispenser.OpenCassette();
                                    ToLog(2, "Cashier: ShiftFromKKM: CashDispenser.ReadCassetteID");
                                    CashDispenser.ReadCassetteID();
                                    if (
                                        (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                        (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                        )
                                    {
                                        ToLog(1, string.Format("Cashier: ShiftFromKKM: CashAcceptor.CashAcceptorStatus.Cassettes[1].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus.ToString()));
                                        if (CashDispenser.CashDispenserResult.Cassettes[1].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                        {
                                            ToLog(1, "Cashier: ShiftFromKKM: DeliveryUpCount Empty");
                                            DeliveryUpCount = 0;
                                            CashDispenser.SetCount(1, 0);
                                            DeliveryUpCountChanged = true;
                                        }
                                    }
                                    if (
                                        (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.LowLevel) &&
                                        (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus != CashDispenserStatusT.SuccessfulCommand)
                                        )
                                    {
                                        ToLog(1, string.Format("Cashier: ShiftFromKKM: CashAcceptor.CashAcceptorStatus.Cassettes[2].HopperStatus={0}", CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus.ToString()));
                                        if (CashDispenser.CashDispenserResult.Cassettes[2].HopperStatus == CashDispenserStatusT.EmptyCassette)
                                        {
                                            ToLog(1, "Cashier: ShiftFromKKM: DeliveryDownCount Empty");
                                            DeliveryDownCount = 0;
                                            CashDispenser.SetCount(2, 0);
                                            DeliveryDownCountChanged = true;
                                        }
                                    }
                                    try
                                    {
                                        if (DeliveryUpCountChanged || DeliveryDownCountChanged)
                                        {
                                            lock (db_lock)
                                            {
                                                deviceCMModel.CountUp = DeliveryUpCount;
                                                deviceCMModel.CountDown = DeliveryDownCount;
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: ShiftFromKKM Utils.Running Enter");
                                                //while (Utils.Running)
                                                //    Thread.Sleep(50);
                                                //if (DetailLog)
                                                //    ToLog(2, "Cashier: ShiftFromKKM Utils.Running Leave");
                                                try
                                                {
                                                    UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                                }
                                                catch (Exception exc)
                                                {
                                                    ToLog(0, exc.ToString());
                                                    try
                                                    {
                                                        UtilsBase.SetOne("DeviceCMModel", "DeviceId", new object[] { "DeviceId", DeviceId, "CountUp", deviceCMModel.CountUp, "CountDown", deviceCMModel.CountDown }, "DeviceId='" + DeviceId.ToString() + "'", null, db);
                                                    }
                                                    catch (Exception exc1)
                                                    {
                                                        ToLog(0, exc1.ToString());
                                                    }
                                                }
                                            }
                                            DeliveryUpCountChanged = false;
                                            DeliveryDownCountChanged = false;
                                        }
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                    }
                                    if (CashDispenser.IsOpen)
                                        CashDispenser.Pool = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!EmulatorEnabled || !EmulatorForm.Emulator.CashDispenserVirt.Checked)
                        {
                            if ((CashDispenser != null) && CashDispenser.IsOpen)
                            {
                                ToLog(2, "Cashier: ShiftFromKKM: CashDispenser.CloseCassette");
                                CashDispenser.CloseCassette();
                            }
                        }
                        LastShiftOpenTime = null;
                        if (TerminalLevel != AlarmLevelT.Red)
                        {
                            AlarmT Alarm = new AlarmT();
                            TerminalLevel = AlarmLevelT.Red;
                            Alarm.AlarmLevel = AlarmLevelT.Red;
                            Alarm.AlarmSource = AlarmSourceT.Terminal;
                            Alarm.ErrorCode = AlarmCode.ShiftClosed;
                            SendAlarm(Alarm);
                        }

                        //Delivery = true;
                        //deliveryStatus = DeliveryStatusT.Delivery;
                    }

                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void CancellWork()
        {
            ToLog(2, "Cashier: CancellWork");
            try
            {
                /*switch (FreeTimeTypeId)
                {
                    case 2:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromHours(FreeTime);
                        break;
                    case 3:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromDays(FreeTime);
                        break;
                    default:
                        DateExitEstimated1 = DateTime.Now + TimeSpan.FromMinutes(FreeTime);
                        break;
                }
                if (DateExitEstimated1 > DateExitEstimated)
                    DateExitEstimated = DateExitEstimated1;*/

            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}
