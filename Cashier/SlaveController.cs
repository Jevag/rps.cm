﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Linq;
using System.Data.SqlClient;
using RPS.ServiceLicencee.Compute;
using System.Windows.Media;
using Rps.ShareBase;
using SqlBaseImport.Model;
using System.Threading;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime SlaveAlarmTime;
        private void GetSlaveControllerStatus()
        {
            try
            {
                if (deviceCMModel.SlaveExist != null && (bool)deviceCMModel.SlaveExist)
                {
                    if (SlaveController == null)
                    {
                        SlaveController = new Slave(log);
                        SlaveController.SlavePort1 = "H";
                        SlaveController.SlavePort2 = " ";
                        SlaveController.SlavePort3 = " ";
                        SlaveController.SlavePort4 = " ";
                        SlaveController.OpenThread(SlaveControllerPort);
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, DateTime.Now.ToString("yyyy'/'MM'/'dd HH:mm:ss.fff") + " GetSlaveControllerStatus: " + exc.ToString());
            }
        }
        void CheckDoorStatus()
        {
            try
            {
                #region  Обмен со Slave
                string statSlave2 = "";
                if (deviceCMModel.SlaveExist != null && (bool)deviceCMModel.SlaveExist)
                {
                    if (!SlaveController.IsOpen && !SlaveController.Run)
                    {
                        SlaveController.OpenThread(SlaveControllerPort);
                    }

                    if (SlaveController.SoftStatus != Slave.SlaveSoftStatusT.Success)
                    {
                        SlaveControllerWork = false;
                        if (SlaveController.SoftStatus == Slave.SlaveSoftStatusT.CommonError)
                        {
                            if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                            if (SlaveLevel != AlarmLevelT.Brown)
                            {
                                if (SlaveLevel != AlarmLevelT.Yellow)
                                {
                                    if (SlaveAlarmTime == DateTime.MinValue)
                                    {
                                        SlaveAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (SlaveAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            SlaveLevel = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                            SendAlarm(Alarm);
                                            SlaveAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                    return;
                                }
                            }
                            else if (SlaveController.SoftStatus == Slave.SlaveSoftStatusT.DiskretError)
                            {
                                if (SlaveLevel != AlarmLevelT.Red)
                                {
                                    if (SlaveAlarmTime == DateTime.MinValue)
                                    {
                                        SlaveAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (SlaveAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            SlaveLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveControllerDiskretMduleEmpty;
                                            SendAlarm(Alarm);
                                            SlaveAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                    return;
                                }
                            }
                            else if (SlaveController.SoftStatus == Slave.SlaveSoftStatusT.VersionError)
                            {
                                if (SlaveLevel != AlarmLevelT.Red)
                                {
                                    if (SlaveAlarmTime == DateTime.MinValue)
                                    {
                                        SlaveAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (SlaveAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            SlaveLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveControllerVersionError;
                                            SendAlarm(Alarm);
                                            SlaveAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                    return;
                                }
                            }
                            else if (SlaveController.SoftStatus == Slave.SlaveSoftStatusT.SignatureError)
                            {
                                if (SlaveLevel != AlarmLevelT.Red)
                                {
                                    if (SlaveAlarmTime == DateTime.MinValue)
                                    {
                                        SlaveAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (SlaveAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            SlaveLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveControllerSignatureError;
                                            SendAlarm(Alarm);
                                            SlaveAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                                    return;
                                }
                            }

                        }
                        else
                            SlaveControllerWork = true;

                        // Обмен с Slave
                        if (!SlaveControllerWork)
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;

                        if (SlaveControllerWork)
                        {
                            if (SlaveController.VersionOK)
                                versionSlave = SlaveController.VersionSlave;


                            if (SlaveController.SignatureOK)
                            {
                                DateTime dt = DateTime.Now;
                                TimeSpan tt = dt - SlaveController.LastSignatureTime;
                                int ttt = (int)tt.TotalSeconds + 10;
                                SignatureSlave = SlaveController.SignatureSlave;
                                if (device.SlaveCode != SignatureSlave)
                                {
                                    // СОХРАНИТЬ В БАЗЕ
                                    device.SlaveCode = SignatureSlave;
                                    lock (db_lock)
                                    {
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: CheckDoorStatus Utils.Running Enter");
                                    //while (Utils.Running)
                                    //    Thread.Sleep(50);
                                    //if (DetailLog)
                                    //    ToLog(2, "Cashier: CheckDoorStatus Utils.Running Leave");
                                    try
                                    {
                                            UtilsBase.SetOne("DeviceModel", "Id", new object[] { "Id", device.Id, "SlaveCode", device.SlaveCode }, "Id='" + DeviceId.ToString() + "'", null, db);
                                        }
                                        catch (Exception exc)
                                        {
                                            ToLog(0, exc.ToString());
                                            try
                                            {
                                                UtilsBase.SetOne("DeviceModel", "Id", new object[] { "Id", device.Id, "SlaveCode", device.SlaveCode }, "Id='" + DeviceId.ToString() + "'", null, db);
                                            }
                                            catch (Exception exc1)
                                            {
                                                ToLog(0, exc1.ToString());
                                            }
                                        }
                                    }
                                    Compute comp = new Compute();
                                    ToLog(2, "Cashier: CheckDoorStatus: ComputeHash");
                                    LicenseWork = comp.ComputeHash(db, SignatureSlave);
                                }
                            }

                            int _StatDiskret = SlaveController.StatDiskret;

                            int ExternalDoorChk = _StatDiskret & 4;
                            int InternalDoorChk = _StatDiskret & 2;
                            if (diskret_check0)
                            {
                                _ExternalDoorOpen0 = (ExternalDoorChk == 4);
                                _InternalDoorOpen0 = (InternalDoorChk == 2);
                            }
                            else
                            {
                                _ExternalDoorOpen1 = (ExternalDoorChk == 4);
                                _InternalDoorOpen1 = (InternalDoorChk == 2);
                            }
                            diskret_check0 = !diskret_check0;
                        }
                        else
                        {
                        }
                        #region Старый Обмен со Slave
                        /*switch (etapSlave)
                        {
                            case 0:         // ожидание команды
                                etapSlave = 1;

                                break;
                            case 1:         // инициализация
                                if (SlaveController.Open(SlaveControllerPort))
                                {
                                    etapSlave = 2;
                                    timer_diskret = time_diskret;
                                }
                                else
                                {
                                    etapSlave = 0;
                                    statSlave = "не отвечает";
                                    TerminalStatusChanged = true;
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    SlaveControllerWork = false;
                                    if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red )
                                        TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                    if (SlaveLevel != AlarmLevelT.Brown)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        SlaveLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                        Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                        //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                        SendAlarm(Alarm);
                                        return;
                                    }
                                }
                                break;

                            #region // отправка команды
                            case 2:
                                // выбор команды

                                DateTime dt = DateTime.Now;
                                TimeSpan tt = dt - LastSignatureTime;
                                int ttt = (int)tt.TotalSeconds;
                                if (ttt > SignatureTime)
                                {
                                    // готовим для чтения SignatureSlave
                                    if (SlaveController.SignatureSlaveRead())
                                    {
                                        etapSlave = 3;              //ожидание ответа
                                    }
                                    else
                                    {
                                        etapSlave = 5;
                                        statSlave = "не отвечает";
                                        TerminalStatusChanged = true;
                                        if (terminalStatus != TerminalStatusT.PaymentError)
                                            TerminalWork = TerminalWorkT.TerminalDontWork;
                                        SlaveControllerWork = false;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                        if (SlaveLevel != AlarmLevelT.Brown)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            SlaveLevel = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            return;
                                        }
                                    }
                                }
                                else if (versionSlave == "")
                                {
                                    if (SlaveController.VersionSlaveRead())
                                    {
                                        etapSlave = 3;              //ожидание ответа
                                        timer_diskret = time_diskret;
                                        versionSlave = " ";

                                    }
                                    else
                                    {
                                        etapSlave = 5;
                                        statSlave = "не отвечает";
                                        TerminalStatusChanged = true;
                                        if (terminalStatus != TerminalStatusT.PaymentError)
                                            TerminalWork = TerminalWorkT.TerminalDontWork;
                                        SlaveControllerWork = false;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                        if (SlaveLevel != AlarmLevelT.Brown)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            SlaveLevel = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    // готовим для передачи дискрета
                                    SlaveController.SlavePort1 = "A";
                                    SlaveController.SlavePort2 = " ";
                                    SlaveController.SlavePort3 = " ";
                                    SlaveController.SlavePort4 = " ";
                                    SlaveController.ComDiskret = comDiskret;
                                    if (SlaveController.Diskret())
                                    {
                                        etapSlave = 3;              //ожидание ответа
                                                                    //timer_diskret = time_diskret;
                                    }
                                    else
                                    {
                                        etapSlave = 5;
                                        statSlave = "не отвечает";
                                        TerminalStatusChanged = true;
                                        if (terminalStatus != TerminalStatusT.PaymentError)
                                            TerminalWork = TerminalWorkT.TerminalDontWork;
                                        SlaveControllerWork = false;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                        if (SlaveLevel != AlarmLevelT.Brown)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            SlaveLevel = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            return;
                                        }
                                    }
                                }

                                break;
                            #endregion
                            case 3:         //ожидание ответа
                                if (SlaveController.Wait1())
                                {

                                    statSlave = SlaveController.Status;
                                    if (statSlave == "Diskret ok")
                                    {
                                        SlaveControllerWork = true;
                                        int _StatDiskret = SlaveController.StatDiskret;

                                        int chk1 = _StatDiskret & 1;
                                        int chk2 = _StatDiskret & 2;
                                        if (diskret_check0)
                                        {
                                            _ExternalDoorOpen0 = (chk1 == 1);
                                            _InternalDoorOpen0 = (chk2 == 2);
                                        }
                                        else
                                        {
                                            _ExternalDoorOpen1 = (chk1 == 1);
                                            _InternalDoorOpen1 = (chk2 == 2);
                                        }
                                        diskret_check0 = !diskret_check0;

                                        statDiskret = _StatDiskret;
                                        etapSlave = 2;
                                        timer_diskret = time_diskret;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Green)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Green;
                                        if (SlaveLevel != AlarmLevelT.Green)
                                        {
                                            SlaveControllerWork = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Green;
                                            SlaveLevel = AlarmLevelT.Green;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                                            Alarm.Status = ResourceManager.GetString("SlaveConfrollerWork", DefaultCulture);
                                            SendAlarm(Alarm);
                                        }
                                    }

                                    if (statSlave == "ожидание" & timer_diskret == 0)
                                    {
                                        statSlave = "не отвечает";
                                        etapSlave = 5;
                                        SlaveControllerWork = false;
                                        TerminalStatusChanged = true;
                                        if (terminalStatus != TerminalStatusT.PaymentError)
                                            TerminalWork = TerminalWorkT.TerminalDontWork;

                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                        if (SlaveLevel != AlarmLevelT.Brown)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Brown;
                                            SlaveLevel = AlarmLevelT.Brown;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                            //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                            SendAlarm(Alarm);
                                            return;
                                        }
                                    }

                                    if ((statSlave == "BCC er" | statSlave == "BCC in Slave er") & timer_diskret != 0)
                                    {
                                        etapSlave = 2;
                                    }

                                    if (statSlave == "ожидание" & timer_diskret != 0)
                                    {
                                        timer_diskret--;
                                        statSlave = statSlave2;
                                    }

                                    if (statSlave == "SignatureSlave ok")
                                    {
                                        SlaveControllerWork = true;
                                        SignatureSlave = SlaveController.SignatureSlave;
                                        // СОХРАНИТЬ В БАЗЕ
                                        //if (device.SlaveCode != SignatureSlave)
                                        {
                                            using (var tmp_db = new Entities())
                                            {
                                                var tmp_device = tmp_db.DeviceModel.FirstOrDefault(DeviceModel => DeviceModel.Type == DeviceType);
                                                tmp_device.SlaveCode = SignatureSlave;
                                                tmp_db.SaveChanges();
                                            }
                                        }
                                        device = db.DeviceModel.FirstOrDefault(DeviceModel => DeviceModel.Type == DeviceType); ;

                                        using (SqlConnection connect = new SqlConnection(conlocal))
                                        {
                                            connect.Open();
                                            Compute comp = new Compute();
                                            LicenseWork = comp.ComputeHash(connect, SignatureSlave);
                                        }
                                        LastSignatureTime = DateTime.Now;

                                        etapSlave = 2;
                                        timer_diskret = time_diskret;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Green)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Green;
                                        if (SlaveLevel != AlarmLevelT.Green)
                                        {
                                            SlaveControllerWork = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Green;
                                            SlaveLevel = AlarmLevelT.Green;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                                            Alarm.Status = ResourceManager.GetString("SlaveConfrollerWork", DefaultCulture);
                                            SendAlarm(Alarm);
                                        }
                                    }

                                    if (statSlave == "VersionSlave ok")
                                    {
                                        versionSlave = SlaveController.VersionSlave;
                                        // СОХРАНИТЬ В БАЗЕ

                                        etapSlave = 2;
                                        timer_diskret = time_diskret;
                                        if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Green)
                                            TechForm.SlaveControllerPortLabel.Foreground = Brushes.Green;
                                        if (SlaveLevel != AlarmLevelT.Green)
                                        {
                                            SlaveControllerWork = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Green;
                                            SlaveLevel = AlarmLevelT.Green;
                                            Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                                            Alarm.Status = ResourceManager.GetString("SlaveConfrollerWork", DefaultCulture);
                                            SendAlarm(Alarm);
                                        }
                                    }

                                    if (statSlave == " ")
                                    {
                                    }
                                }
                                else
                                {
                                    etapSlave = 5;
                                    statSlave = "не отвечает";
                                    TerminalStatusChanged = true;
                                    if (terminalStatus != TerminalStatusT.PaymentError)
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                    SlaveControllerWork = false;
                                    if (TechForm.SlaveControllerPortLabel.Foreground != Brushes.Red)
                                        TechForm.SlaveControllerPortLabel.Foreground = Brushes.Red;
                                    if (SlaveLevel != AlarmLevelT.Brown)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        SlaveLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.SlaveController;
                                        Alarm.ErrorCode = AlarmCode.SlaveContrllerNotConnect;
                                        //Alarm.Status = ResourceManager.GetString("SlaveControllerCommunicationError", DefaultCulture);
                                        SendAlarm(Alarm);
                                        return;
                                    }
                                }

                                break;
                            case 4:

                                break;
                            case 5:

                                if (Close())
                                {
                                    etapSlave = 0;

                                }
                                else
                                {
                                    etapSlave = 0;
                                }
                                break;

                            default:

                                break;
                        }*/
                        /*if (statSlave != "не отвечает")
                        {
                            TerminalStatusChanged = true;
                            TerminalWork = TerminalWorkT.TerminalWork;
                        }*/
                        #endregion
                    }
                    #endregion

                }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        void InternalDoorStatus()
        {
            if (_InternalDoorOpen0 == _InternalDoorOpen1)
            {
                if (InternalDoorOpen != _InternalDoorOpen0)
                {
                    InternalDoorOpen = _InternalDoorOpen0;
                    TerminalStatusChanged = true;
                    InternalDoorOpenChanged = true;
                    if (InternalDoorOpen)
                    {
                        ErrorsText += ResourceManager.GetString("ToLogInternalDoorOpened", DefaultCulture) + Environment.NewLine;
                    }
                    else
                    {
                        ErrorsText += ResourceManager.GetString("ToLogInternalDoorClosed", DefaultCulture) + Environment.NewLine;
                    }
                }
            }
        }
        void ExternalDoorStatus()
        {
            if (_ExternalDoorOpen0 == _ExternalDoorOpen1)
            {
                if (ExternalDoorOpen != _ExternalDoorOpen0)
                {
                    ExternalDoorOpen = _ExternalDoorOpen0;
                    TerminalStatusChanged = true;
                    ExternalDoorOpenChanged = true;
                    if (ExternalDoorOpen)
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                        ErrorsText += ResourceManager.GetString("ToLogExternalDoorOpened", DefaultCulture) + Environment.NewLine;
                    }
                    else
                    {
                        if (terminalStatus != TerminalStatusT.PaymentError)
                            TerminalWork = TerminalWorkT.TerminalWork;
                        ErrorsText += ResourceManager.GetString("ToLogExternalDoorClosed", DefaultCulture) + Environment.NewLine;
                    }
                }
            }
        }
        private void GetDoorStatus()
        {
            try
            {
                if (deviceCMModel.SlaveExist != null && (bool)deviceCMModel.SlaveExist)
                {
                    GetSlaveControllerStatus();
                    CheckDoorStatus();
                }
                else
                {
                    SlaveControllerWork = true;
                    if (SlaveAlarmTime != DateTime.MinValue)
                        SlaveAlarmTime = DateTime.MinValue;
                    if (SlaveLevel != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.SlaveController;
                        Alarm.ErrorCode = AlarmCode.DeviceWork;
                        Alarm.Status = ResourceManager.GetString("SlaveConfrollerWork", DefaultCulture);
                        SendAlarm(Alarm);
                        SlaveLevel = AlarmLevelT.Green;
                    }
                }

                if (EmulatorEnabled)
                {
                    #region  ExternalDoorVirt.Checked
                    if (EmulatorForm.Emulator.ExternalDoorVirt.Checked)
                    {
                        if (ExternalDoorOpen != EmulatorForm.Emulator.ExternalDoorOpen.Checked)
                        {

                            ExternalDoorOpen = EmulatorForm.Emulator.ExternalDoorOpen.Checked;
                            TerminalStatusChanged = true;
                            ExternalDoorOpenChanged = true;
                        };
                    }
                    #endregion
                    else
                    {
                        ExternalDoorStatus();
                    }
                    #region  InternalDoorVirt.Checked
                    if (CashierType == 0)
                    {
                        if (EmulatorForm.Emulator.InternalDoorVirt.Checked)
                        {
                            if (InternalDoorOpen != EmulatorForm.Emulator.InternalDoorOpen.Checked)
                            {

                                InternalDoorOpen = EmulatorForm.Emulator.InternalDoorOpen.Checked;
                                TerminalStatusChanged = true;
                                InternalDoorOpenChanged = true;
                            };
                        }
                        #endregion
                        else
                        {
                            InternalDoorStatus();
                        }
                    }
                }
                else
                {
                    ExternalDoorStatus();

                    if (CashierType == 0)
                    {
                        InternalDoorStatus();
                    }
                }
                if (ExternalDoorOpen)
                {
                    ErrorsText += ResourceManager.GetString("ToLogExternalDoorOpened", DefaultCulture) + Environment.NewLine;
                    if (ExternalDoorLevel != AlarmLevelT.Yellow)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Yellow;
                        ExternalDoorLevel = AlarmLevelT.Yellow;
                        Alarm.AlarmSource = AlarmSourceT.ExternalDoor;
                        Alarm.ErrorCode = AlarmCode.ExternalDoorOpen;
                        SendAlarm(Alarm);
                    }
                }
                else
                {
                    if (ExternalDoorLevel != AlarmLevelT.Green)
                    {
                        AlarmT Alarm = new AlarmT();
                        Alarm.AlarmLevel = AlarmLevelT.Green;
                        Alarm.AlarmSource = AlarmSourceT.ExternalDoor;
                        Alarm.ErrorCode = AlarmCode.ExternalDoorClosed;
                        SendAlarm(Alarm);
                        ExternalDoorLevel = AlarmLevelT.Green;
                    }
                }
                if (CashierType == 0)
                {
                    if (InternalDoorOpen)
                    {
                        ErrorsText += ResourceManager.GetString("ToLogInternalDoorOpened", DefaultCulture) + Environment.NewLine;
                        if (InternalDoorLevel != AlarmLevelT.Yellow)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                            InternalDoorLevel = AlarmLevelT.Yellow;
                            Alarm.AlarmSource = AlarmSourceT.InternalDoor;
                            Alarm.ErrorCode = AlarmCode.InternalDoorOpen;
                            SendAlarm(Alarm);
                        }
                    }
                    else
                    {
                        if (InternalDoorLevel != AlarmLevelT.Green)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.InternalDoor;
                            Alarm.ErrorCode = AlarmCode.InternalDoorClosed;
                            SendAlarm(Alarm);
                            InternalDoorLevel = AlarmLevelT.Green;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}