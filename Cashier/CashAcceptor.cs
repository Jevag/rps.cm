﻿using CommonClassLibrary;
using Communications;
using System;
using System.Windows;
using System.Windows.Media;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime CashAcceptorAlarmTime;
        void HardCashAcceptor()
        {
            try
            {
                if (CashAcceptor == null)
                {
                    if (CashAcceptorModel == "iVision")
                    {
                        ToLog(2, "Cashier: HardCashAcceptor: Create CashAcceptorJCM_ID_003");
                        CashAcceptor = new CashAcceptorJCM_ID_003(CashAcceptorLock, log, DefaultCulture);
                    }
                    else
                    {
                        CashAcceptorLock = CashDispenserLock;
                        if (CashRecycler == null)
                        {
                            ToLog(2, "Cashier: HardCashAcceptor: Create CashRecyclerJCM_RC_ID_003");
                            CashRecycler = new CashRecyclerJCM_RC_ID_003(CashDispenserLock, log, DefaultCulture);
                        }
                        CashAcceptor = CashRecycler;
                    }
                }
                if (!CashAcceptor.IsOpen)
                {
                    ErrorsText += ResourceManager.GetString("CashAcceptorCommunicationError", DefaultCulture) + Environment.NewLine;

                    if ((CashAcceptorStatusCheckDT == null) || (CashAcceptorStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                    {
                        if (CashAcceptor.Open(CashAcceptorPort))
                        {
                            if (CashAcceptorModel == "iVision")
                            {
                                ToLog(1, "CashAcceptor Version: " + CashAcceptor.Version);
                                ToLog(1, "CashAcceptor BootVersion: " + CashAcceptor.BootVersion);
                            }
                            else
                            {
                                ToLog(1, "CashRecycler Version: " + CashAcceptor.Version);
                                ToLog(1, "CashRecycler BootVersion: " + CashAcceptor.BootVersion);
                            }
                            if (!CashAcceptorWork)
                            {
                                CashAcceptorWorkChanged = true;
                                CashAcceptorWork = true;
                            }
                        }
                        else
                        {
                            if (CashAcceptorWork)
                            {
                                CashAcceptorWorkChanged = true;
                                CashAcceptorWork = false;
                            }
                            if (TechForm.CashAcceptorPortLabel.Foreground != Brushes.Red)
                                TechForm.CashAcceptorPortLabel.Foreground = Brushes.Red;
                            if (CashAcceptorLevel != AlarmLevelT.Brown)
                            {
                                if (CashAcceptorAlarmTime == DateTime.MinValue)
                                {
                                    CashAcceptorAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        CashAcceptorLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                        Alarm.ErrorCode = AlarmCode.CashAcceptorNotConnect;
                                        SendAlarm(Alarm);
                                        CashAcceptorAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        CashAcceptorStatusCheckDT = DateTime.Now;
                    }
                }

                if (CashAcceptor.IsOpen)
                {
                    if ((CashAcceptor.CashAcceptorStatus.CommunicationsError) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.CommError))
                    {
                        if (CashAcceptorCommError == 2)
                        {
                            CashAcceptor.Close();
                            if (CashAcceptor.Open(CashAcceptorPort))
                            {
                                if (CashAcceptorModel == "iVision")
                                {
                                    ToLog(1, "CashAcceptor Version: " + CashAcceptor.Version);
                                    ToLog(1, "CashAcceptor BootVersion: " + CashAcceptor.BootVersion);
                                }
                                else
                                {
                                    ToLog(1, "CashRecycler Version: " + CashAcceptor.Version);
                                    ToLog(1, "CashRecycler BootVersion: " + CashAcceptor.BootVersion);
                                }
                                if (!CashAcceptorWork)
                                {
                                    CashAcceptorWorkChanged = true;
                                    CashAcceptorWork = true;
                                }
                            }
                        }
                        if (CashAcceptorCommError > 2)
                        {
                            ErrorsText += ResourceManager.GetString("TechCashAcceptorCheckReq", DefaultCulture) + Environment.NewLine;

                            if (CashAcceptorWork)
                            {
                                CashAcceptorWorkChanged = true;
                                CashAcceptorWork = false;
                            }
                            if (TechForm.CashAcceptorPortLabel.Foreground != Brushes.Red)
                                TechForm.CashAcceptorPortLabel.Foreground = Brushes.Red;

                            if (CashAcceptorLevel != AlarmLevelT.Brown)
                            {
                                if (CashAcceptorAlarmTime == DateTime.MinValue)
                                {
                                    CashAcceptorAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        CashAcceptorLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                        Alarm.ErrorCode = AlarmCode.CashAcceptorNotConnect;
                                        SendAlarm(Alarm);
                                        CashAcceptorAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (CashAcceptorCommError < 3)
                                CashAcceptorCommError++;
                        }
                    }
                    else
                    {

                        if (TechForm.CashAcceptorPortLabel.Foreground != Brushes.Green)
                            TechForm.CashAcceptorPortLabel.Foreground = Brushes.Green;
                    }
                }
                if (
                        //(CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Unknown) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.CommError) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.StackerFull) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.StackerOpen) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Cheated) ||
                        //(CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.RecyclerError ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.JamInAcceptor) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.JamInStacker) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.InvalidCommand) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Initialize) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Failure)
                    )
                {
                    if (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Failure)
                                ErrorsText += ResourceManager.GetString(CashAcceptor.Failure.ToString(), DefaultCulture) + Environment.NewLine;
                    else
                    ErrorsText += ResourceManager.GetString(CashAcceptor.CashAcceptorStatus.Status.ToString(), DefaultCulture) + Environment.NewLine;

                    if (
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.JamInAcceptor) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.JamInStacker)
                        )
                    {
                        if (CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = false;
                        }
                        if (CashAcceptorLevel != AlarmLevelT.Red)
                        {
                            if (CashAcceptorAlarmTime == DateTime.MinValue)
                            {
                                CashAcceptorAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    CashAcceptorLevel = AlarmLevelT.Red;
                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                    Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                    Alarm.ErrorCode = AlarmCode.CashAcceptorJam;
                                    SendAlarm(Alarm);
                                    CashAcceptorAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                    }
                    else if (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.RecyclerError)
                    {
                        if (CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = false;
                        }
                        if (CashAcceptorLevel != AlarmLevelT.Red)
                        {
                            if (CashAcceptorAlarmTime == DateTime.MinValue)
                            {
                                CashAcceptorAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    CashAcceptorLevel = AlarmLevelT.Red;
                                    Alarm.AlarmLevel = AlarmLevelT.Red;
                                    Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                    Alarm.ErrorCode = AlarmCode.CashAcceptorError;
                                    SendAlarm(Alarm);
                                    CashAcceptorAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                    }
                    else if ((CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.CommError) ||
                        (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.StackerFull) ||
                   (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.StackerOpen) ||
                   (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Failure) ||
                   (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Cheated) ||
                   CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.Initialize)
                    {
                        if (CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = false;
                        }
                        if (CashAcceptor.CashAcceptorStatus.Status == CommonCashAcceptorStatusT.CommError)
                            if (TechForm.CashAcceptorPortLabel.Foreground != Brushes.Red)
                                TechForm.CashAcceptorPortLabel.Foreground = Brushes.Red;

                        if (CashAcceptorLevel != AlarmLevelT.Brown)
                        {
                            if (CashAcceptorAlarmTime == DateTime.MinValue)
                            {
                                CashAcceptorAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    CashAcceptorLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                    switch (CashAcceptor.CashAcceptorStatus.Status)
                                    {
                                        case CommonCashAcceptorStatusT.CommError:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorNotConnect;
                                            break;
                                        case CommonCashAcceptorStatusT.StackerFull:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorFull;
                                            break;
                                        case CommonCashAcceptorStatusT.StackerOpen:
                                        case CommonCashAcceptorStatusT.Initialize:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorBoxOpen;
                                            break;
                                        case CommonCashAcceptorStatusT.Cheated:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorCheated;
                                            break;
                                        case CommonCashAcceptorStatusT.JamInAcceptor:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorJamInAcceptor;
                                            break;
                                        case CommonCashAcceptorStatusT.JamInStacker:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorJamInStacker;
                                            break;
                                        case CommonCashAcceptorStatusT.Pause:
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorPause;
                                            break;
                                        case CommonCashAcceptorStatusT.Failure:
                                            switch (CashAcceptor.Failure)
                                            {
                                                case CashAcceptorFailureT.CashBoxNotReady:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorBoxNotReady;
                                                    break;
                                                case CashAcceptorFailureT.StackMotorFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorStackMotorFailure;
                                                    break;
                                                case CashAcceptorFailureT.TransportSpeedFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorTransportSpeedFailure;
                                                    break;
                                                case CashAcceptorFailureT.TransportFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorTransportFailure;
                                                    break;
                                                case CashAcceptorFailureT.SolenoidFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorSolenoidFailure;
                                                    break;
                                                case CashAcceptorFailureT.PBUnitFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorPBUnitFailure;
                                                    break;
                                                case CashAcceptorFailureT.CashAcceptorHeadRemove:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorCashAcceptorHeadRemove;
                                                    break;
                                                case CashAcceptorFailureT.BOOTROMFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorBOOTROMFailure;
                                                    break;
                                                case CashAcceptorFailureT.ExternalROMFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorExternalROMFailure;
                                                    break;
                                                case CashAcceptorFailureT.RAMFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorRAMFailure;
                                                    break;
                                                case CashAcceptorFailureT.ExternalROMWritingFailure:
                                                    Alarm.ErrorCode = AlarmCode.CashAcceptorExternalROMWritingFailure;
                                                    break;
                                            }
                                            break;
                                        case CommonCashAcceptorStatusT.InvalidCommand:
                                            break;
                                    }
                                    SendAlarm(Alarm);
                                    CashAcceptorAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                    }
                }
                else
                    if (CashAcceptorWork && (CashAcceptorCurrent > (CashAcceptorMaxCount - CashAcceptorMax)))
                {
                    ErrorsText += ResourceManager.GetString("CashAcceptorNeedIncasse", DefaultCulture) + Environment.NewLine;
                    if (CashAcceptorLevel != AlarmLevelT.Yellow)
                    {
                        if (CashAcceptorAlarmTime == DateTime.MinValue)
                        {
                            CashAcceptorAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                AlarmT Alarm = new AlarmT();
                                CashAcceptorLevel = AlarmLevelT.Yellow;
                                Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                Alarm.ErrorCode = AlarmCode.CashAcceptorNearFull;
                                SendAlarm(Alarm);
                                CashAcceptorAlarmTime = DateTime.MinValue;
                            }
                        }
                        if (!CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = true;
                        }
                    }
                }
                else
                    if (CashAcceptorWork && ((CashAcceptorCurrent > (CashAcceptorMaxCount - 5)) && (terminalStatus == TerminalStatusT.Empty)))
                {
                    ErrorsText += ResourceManager.GetString("CashAcceptorNeedIncasse", DefaultCulture) + Environment.NewLine;
                    if (CashAcceptorLevel != AlarmLevelT.Red)
                    {
                        if (CashAcceptorAlarmTime == DateTime.MinValue)
                        {
                            CashAcceptorAlarmTime = DateTime.Now;
                        }
                        else
                        {
                            if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                            {
                                AlarmT Alarm = new AlarmT();
                                CashAcceptorLevel = AlarmLevelT.Red;
                                Alarm.AlarmLevel = AlarmLevelT.Red;
                                Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                Alarm.ErrorCode = AlarmCode.CashAcceptorFull;
                                SendAlarm(Alarm);
                                CashAcceptorAlarmTime = DateTime.MinValue;
                            }
                        }
                        if (CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = false;
                        }
                    }
                }
                else
                {
                    if ((!CashAcceptor.CashAcceptorStatus.CommunicationsError) &&
             (CashAcceptor.CashAcceptorStatus.Status != CommonCashAcceptorStatusT.CommError))
                    {
                        if (!CashAcceptorWork)
                        {
                            CashAcceptorWorkChanged = true;
                            CashAcceptorWork = true;
                        }
                        if (TechForm.CashAcceptorPortLabel.Foreground != Brushes.Green)
                            TechForm.CashAcceptorPortLabel.Foreground = Brushes.Green;
                        if (CashAcceptorAlarmTime != DateTime.MinValue)
                            CashAcceptorAlarmTime = DateTime.MinValue;
                        if ((CashAcceptorLevel != AlarmLevelT.Green) && (CashAcceptorLevel != AlarmLevelT.Yellow))
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = ResourceManager.GetString("CashAcceptorWork", DefaultCulture);
                            SendAlarm(Alarm);
                            CashAcceptorLevel = AlarmLevelT.Green;
                        }
                        CashAcceptorCommError = 0;
                        if (terminalStatus == TerminalStatusT.Empty)
                        {
                            if (CashAcceptorStatus != CashAcceptor.CashAcceptorStatus.Status)
                            {
                                ToLog(2, "Cahsier: HardCashAcceptor: " + string.Format("CashAcceptor status changed from {0}  to {1}", CashAcceptorStatus, CashAcceptor.CashAcceptorStatus.Status));
                                CashAcceptorStatus = CashAcceptor.CashAcceptorStatus.Status;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void GetCashAcceptorStatus()
        {
            try
            {
                if (deviceCMModel.CashAcceptorExist != null && (bool)deviceCMModel.CashAcceptorExist)
                {
                    if (EmulatorEnabled)
                        EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled = true;
                    if (!EmulatorEnabled || !EmulatorForm.Emulator.CashAcceptorVirt.Checked)
                    {
                        if (CashAcceptor == null)
                            if (CashAcceptorModel == "iVision")
                            {
                                ToLog(2, "Cashier: GetCashAcceptorStatus: Create CashAcceptorJCM_ID_003");
                                CashAcceptor = new CashAcceptorJCM_ID_003(CashAcceptorLock, log, DefaultCulture);
                            }
                            else
                            {
                                CashDispenserLock = CashAcceptorLock;
                                if (CashRecycler == null)
                                {
                                    ToLog(2, "Cashier: GetCashAcceptorStatus: Create CashRecyclerJCM_RC_ID_003");
                                    CashRecycler = new CashRecyclerJCM_RC_ID_003(CashAcceptorLock, log, DefaultCulture);
                                }
                                CashAcceptor = CashRecycler;
                            }
                    }

                    #region CashAcceptorPanel.CashAcceptorExists.Checked
                    if (EmulatorEnabled && EmulatorForm.Emulator.CashAcceptorVirt.Checked)
                    {
                        #region EmulatorCashAcceptor
                        if (CashAcceptor != null)
                        {
                            if ((CashAcceptorModel == "iVision") && CashAcceptor.IsOpen)
                            {
                                CashAcceptor.InhibitOn();
                                CashAcceptor.Close();
                            }
                        }

                        EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled = true;
                        if (EmulatorForm.Emulator.CashAcceptorWork.Checked)
                        {
                            if (!CashAcceptorWork)
                                CashAcceptorWorkChanged = true;
                            CashAcceptorWork = true;
                            if (CashAcceptorCurrent > (CashAcceptorMaxCount - CashAcceptorMax))
                            {
                                ErrorsText += ResourceManager.GetString("CashAcceptorNeedIncasse", DefaultCulture) + Environment.NewLine;
                                if (CashAcceptorLevel != AlarmLevelT.Yellow)
                                {
                                    if (CashAcceptorAlarmTime == DateTime.MinValue)
                                    {
                                        CashAcceptorAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            CashAcceptorLevel = AlarmLevelT.Yellow;
                                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                            Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorNearFull;
                                            SendAlarm(Alarm);
                                            CashAcceptorAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            if (CashAcceptorCurrent > (CashAcceptorMaxCount - 5))
                            {
                                ErrorsText += ResourceManager.GetString("CashAcceptorNeedIncasse", DefaultCulture) + Environment.NewLine;
                                if (CashAcceptorLevel != AlarmLevelT.Red)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    if (CashAcceptorAlarmTime == DateTime.MinValue)
                                    {
                                        CashAcceptorAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            CashAcceptorLevel = AlarmLevelT.Red;
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                            Alarm.ErrorCode = AlarmCode.CashAcceptorFull;
                                            SendAlarm(Alarm);
                                            CashAcceptorAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (CashAcceptorAlarmTime != DateTime.MinValue)
                                    CashAcceptorAlarmTime = DateTime.MinValue;
                                if (CashAcceptorLevel != AlarmLevelT.Green)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Green;
                                    Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                    Alarm.ErrorCode = AlarmCode.DeviceWork;
                                    Alarm.Status = ResourceManager.GetString("CashAcceptorWork", DefaultCulture);
                                    SendAlarm(Alarm);
                                    CashAcceptorLevel = AlarmLevelT.Green;
                                }
                            }
                        }
                        else
                        {
                            if (EmulatorForm.Emulator.CashAcceptorNeeded.Checked)
                            {

                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("TechCashAcceptorCheckReq", DefaultCulture) + Environment.NewLine;
                                if (CashAcceptorWork)
                                {
                                    CashAcceptorWorkChanged = true;
                                    CashAcceptorWork = false;
                                }
                            }
                            else
                            {
                                if (CashAcceptorWork)
                                {
                                    CashAcceptorWorkChanged = true;
                                    CashAcceptorWork = false;
                                }
                            }
                            if (CashAcceptorLevel != AlarmLevelT.Brown)
                            {
                                if (CashAcceptorAlarmTime == DateTime.MinValue)
                                {
                                    CashAcceptorAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (CashAcceptorAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        CashAcceptorLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.CashAcceptor;
                                        Alarm.ErrorCode = AlarmCode.CashAcceptorNotConnect;
                                        SendAlarm(Alarm);
                                        CashAcceptorAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        #endregion EmulatorCashAcceptor
                    }
                    else
                    {
                        HardCashAcceptor();
                    }
                    #endregion CashAcceptorPanel.CashAcceptorExists.Checked
                }
                else
                {
                    #region CashAcceptorPanel.CashAcceptorExists.NotChecked
                    if (CashAcceptorWork)
                    {
                        CashAcceptorWorkChanged = true;
                        if (CashAcceptor.IsOpen)
                        {
                            CashAcceptor.InhibitOn();
                            CashAcceptor.Close();
                        }
                    }
                    CashAcceptorWork = false;
                    if (EmulatorEnabled)
                    {
                        EmulatorForm.Emulator.CashAcceptorGroupBox.Enabled = false;
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox1.Hide();
                        EmulatorForm.Emulator.CashAcceptorEditsGroupBox2.Hide();
                    }
                    #endregion CashAcceptorPanel.CashAcceptorExists.NotChecked
                }
                if (terminalStatus == TerminalStatusT.Empty)
                {
                    if (CashAcceptorCurrent > (CashAcceptorMaxCount - CashAcceptorMax))
                    {
                        TerminalWork = TerminalWorkT.TerminalDontWork;
                        TerminalStatusChanged = true;
                        if (TerminalWork_old != TerminalWork)
                            ToLog(1, ResourceManager.GetString("TechCashAcceptorIncasseReq", DefaultCulture));
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}