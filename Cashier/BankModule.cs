﻿using CommonClassLibrary;
using System;
using System.Windows;
using Communications;
using Rps.ShareBase;
using CashierWpfControlLibrary;
using System.Collections.Generic;
using SqlBaseImport.Model;
using System.Threading;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime BankModuleAlarmTime;
        void HardBankModule()
        {
            try
            {
                if (BankModule != null)
                {
                    if (ShiftOpened && (TerminalWork != TerminalWorkT.TerminalClosed))
                    {
                        if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
                        {
                            if (BankModule.DeviceEnabled)
                            {
                                if (!WorkKeyLoaded && DateTime.Now > WorkKeyCheckTime)
                                    BMWorkKey();
                            }
                        }
                    }

                    if (!BankModule.DeviceEnabled || BankModule.CheckResultCode != CommonBankModule.ErrorCode.Sucsess || !WorkKeyLoaded)
                    {
                        ErrorsText += ResourceManager.GetString("TechBankModuleCheckReq", DefaultCulture) + Environment.NewLine;
                        if (BankModuleWork)
                        {
                            BankModuleWorkChanged = true;
                            BankModuleWork = false;
                        }
                        if (BankModuleLevel != AlarmLevelT.Brown)
                        {
                            if (BankModuleAlarmTime == DateTime.MinValue)
                            {
                                BankModuleAlarmTime = DateTime.Now;
                            }
                            else
                            {
                                if (BankModuleAlarmTime.AddSeconds(3) < DateTime.Now)
                                {
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Brown;
                                    BankModuleLevel = AlarmLevelT.Brown;
                                    Alarm.AlarmSource = AlarmSourceT.BankModule;
                                    Alarm.ErrorCode = AlarmCode.BancModuleNotConnect;
                                    //Alarm.Status = ResourceManager.GetString("BankModuleCommunicationError", DefaultCulture);
                                    SendAlarm(Alarm);
                                    BankModuleAlarmTime = DateTime.MinValue;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!BankModuleWork)
                        {
                            BankModuleWorkChanged = true;
                            BankModuleWork = true;
                        }
                        if (BankModuleAlarmTime != DateTime.MinValue)
                            BankModuleAlarmTime = DateTime.MinValue;
                        if (BankModuleLevel != AlarmLevelT.Green)
                        {
                            AlarmT Alarm = new AlarmT();
                            Alarm.AlarmLevel = AlarmLevelT.Green;
                            Alarm.AlarmSource = AlarmSourceT.BankModule;
                            Alarm.ErrorCode = AlarmCode.DeviceWork;
                            Alarm.Status = ResourceManager.GetString("BankModuleWork", DefaultCulture);
                            SendAlarm(Alarm);
                            BankModuleLevel = AlarmLevelT.Green;
                        }
                    }
                }
            }

            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        bool BankModeuleNotUsing = false;
        private void GetBankModuleStatus()
        {
            try
            {
                if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist)
                {
                    if (EmulatorEnabled && EmulatorForm.Emulator.BankModuleVirt.Checked)
                    {
                        if (BankModule != null)
                        {
                            BankModule.Close();
                            BankModule = null;
                        }
                        EmulatorForm.Emulator.BankModuleGroupBox.Enabled = true;
                        if (!EmulatorForm.Emulator.BankModuleWork.Checked)
                        {
                            if (EmulatorForm.Emulator.BankModuleNeeded.Checked)
                            {
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("TechBankModuleCheckReq", DefaultCulture) + Environment.NewLine;
                                if (BankModuleWork)
                                {
                                    BankModuleWorkChanged = true;
                                    BankModuleWork = false;
                                }
                            }
                            else
                            {
                                if (BankModuleWork)
                                {
                                    BankModuleWorkChanged = true;
                                    BankModuleWork = false;
                                }
                            }
                            if (BankModuleLevel != AlarmLevelT.Brown)
                            {
                                if (BankModuleAlarmTime == DateTime.MinValue)
                                {
                                    BankModuleAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (BankModuleAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        BankModuleLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.BankModule;
                                        Alarm.ErrorCode = AlarmCode.BancModuleNotConnect;
                                        //Alarm.Status = ResourceManager.GetString("BankModuleCommunicationError", DefaultCulture);
                                        SendAlarm(Alarm);
                                        BankModuleAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!BankModuleWork)
                            {
                                BankModuleWorkChanged = true;
                                BankModuleWork = true;
                            }
                            if (BankModuleAlarmTime != DateTime.MinValue)
                                BankModuleAlarmTime = DateTime.MinValue;
                            if (BankModuleLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.BankModule;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("BankModuleWork", DefaultCulture);
                                SendAlarm(Alarm);
                                BankModuleLevel = AlarmLevelT.Green;
                            }
                        }
                    }
                    else
                    {
                        if (BankModule == null)
                        {
                            if (deviceCMModel.BankModuleExist != null && (bool)deviceCMModel.BankModuleExist)
                            {
                                if (BankModuleModel == "GPB")
                                {
                                    try
                                    {
                                        BankModule = new GPB(log);
                                        BankModeuleNotUsing = false;
                                    }
                                    catch (Exception exc)
                                    {
                                        ToLog(0, exc.ToString());
                                    }
                                }
                                else
                                {
                                    if (!BankModeuleNotUsing)
                                    {
                                        BankModeuleNotUsing = true;
                                        ToLog(0, BankModuleModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture));
                                    }
                                    ErrorsText += BankModuleModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture) + Environment.NewLine;
                                    return;
                                }
                                if (BankModule != null)
                                {
                                    BankModule.KKMNumber = (int)deviceCMModel.CashierNumber;//Установка номера кассы
                                    BankModule.ChequeNumber = CurrentTransactionNumber;
                                    BankModule.Open();
                                    if (BankModule.DeviceEnabled)
                                    {
                                        BankModule.WorkKey();
                                        if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                                            WorkKeyLoaded = true;
                                        else
                                            WorkKeyCheckTime = DateTime.Now + TimeSpan.FromMinutes(5);
                                    }
                                }
                            }
                        }
                        HardBankModule();
                    }
                }
                else
                {
                    if (BankModuleWork)
                        BankModuleWorkChanged = true;
                    BankModuleWork = false;
                    if (BankModule != null)
                    {
                        BankModule.Close();
                        BankModule = null;
                    }
                    if (EmulatorEnabled)
                    {
                        EmulatorForm.Emulator.BankModuleGroupBox.Enabled = false;
                        EmulatorForm.Emulator.BankModuleEditsGroupBox.Hide();
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void BMVerification(bool Manual = false)
        {
            string BankModuleAuthReceipt = "";
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.Verification();
                    BankModuleAuthReceipt = BankModule.AuthReceipt;
                    BankModule.AuthReceipt = "";
                    Guid Id = Guid.NewGuid();
                    lock (db_lock)
                    {
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: BMVerification Utils.Running Enter");
                        //while (Utils.Running)
                        //    Thread.Sleep(50);
                        //if (DetailLog)
                        //    ToLog(2, "Cashier: BMVerification Utils.Running Leave");
                        try
                        {
                            UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 4, "Receipt", BankModuleAuthReceipt });
                        }
                        catch (Exception exc)
                        {
                            ToLog(0, exc.ToString());
                            try
                            {
                                UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 4, "Receipt", BankModuleAuthReceipt });
                            }
                            catch (Exception exc1)
                            {
                                ToLog(0, exc1.ToString());
                            }
                        }
                    }
                    WorkKeyLoaded = false;
                    if (Manual)
                    {
                        DisplayResultT DisplayResult;
                        DisplayResult = new DisplayResultT();
                        DisplayResult.Topmost = true;
                        DisplayResult.TextBox = BankModuleAuthReceipt;
                        DisplayResult.ShowDialog();
                    }
                }
            }
        }
        public void BMLoadSW()
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.LoadSW();
                    MessageBox.Show(BankModule.ResultDescription + Environment.NewLine, "LoadSW result", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        public void BMLoadParams()
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.LoadParams();
                    MessageBox.Show(BankModule.ResultDescription + Environment.NewLine, "LoadParams result", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        public void BMWorkKey(bool Manual = false)
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.WorkKey();
                    if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                        WorkKeyLoaded = true;
                    else
                        WorkKeyCheckTime = DateTime.Now + TimeSpan.FromMinutes(5);
                    if (Manual)
                        MessageBox.Show(BankModule.ResultDescription + Environment.NewLine, "WorkKey result", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        public void BMTerminalInfo()
        {
            string BankModuleAuthReceipt = "";
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.TerminalInfo();
                    BankModuleAuthReceipt = BankModule.AuthReceipt;
                    BankModule.AuthReceipt = "";
                    DisplayResultT DisplayResult;
                    DisplayResult = new DisplayResultT();
                    DisplayResult.Topmost = true;
                    DisplayResult.TextBox = BankModuleAuthReceipt;
                    DisplayResult.ShowDialog();
                }
            }
        }
        public void BMEchoTest()
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.EchoTest();
                    MessageBox.Show(BankModule.ResultDescription + Environment.NewLine, "EchoTest result", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        public void BMCleanCache()
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.CleanCache();
                    MessageBox.Show(BankModule.ResultDescription + Environment.NewLine, "CleanCache result", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
        /*public void BMRevert()
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.TechnicalCancelation();
                }
            }
        }*/
        public void BMCancell(int ChequeNumber, decimal AmountDue)
        {
            if (!EmulatorEnabled || !EmulatorForm.Emulator.BankModuleVirt.Checked)
            {
                if (BankModule != null)
                {
                    BankModule.Cancel(ChequeNumber, AmountDue);
                    if (BankModule.ResultCode == CommonBankModule.ErrorCode.Sucsess)
                    {
                        List<string> BankSlip = new List<string>();
                        BankModuleStatus = BankModuleStatusT.BankCardSuccess;
                        string Result = BankModule.AuthResult;
                        BankModule.AuthResult = "";
                        string Receipt = BankModule.AuthReceipt;
                        BankModule.AuthReceipt = "";
                        BankModule.CommandStatus = CommandStatusT.Undeifned;
                        Guid Id = Guid.NewGuid();
                        lock (db_lock)
                        {
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: BMCancel Utils.Running Enter");
                            //while (Utils.Running)
                            //    Thread.Sleep(50);
                            //if (DetailLog)
                            //    ToLog(2, "Cashier: BMCancel Utils.Running Leave");
                            try
                            {
                                UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 2, "Result", Result, "Receipt", Receipt });
                            }
                            catch (Exception exc)
                            {
                                ToLog(0, exc.ToString());
                                try
                                {
                                    UtilsBase.InsertCommand("BankDocumets", db, null, new object[] { "Id", Id, "ShiftNumber", deviceCMModel.ShiftNumber, "TransactionNumber", CurrentTransactionNumber, "OperationCode", 2, "Result", Result, "Receipt", Receipt });
                                }
                                catch (Exception exc1)
                                {
                                    ToLog(0, exc1.ToString());
                                }
                            }
                        }
                        BankSlip.AddRange(Receipt.Split(new Char[] { '\n' }));
                        //GiveCheque(3, 0, 0, 0, 0, null, BankSlip);
                    }
                }
            }
            else
            {
                BankCardErrorDateTime = DateTime.Now;
                BankModuleStatus = BankModuleStatusT.BankCardNotSuccess;
                if (BankModule != null)
                    BankModule.CommandStatus = CommandStatusT.Undeifned;
            }

        }
    }
}