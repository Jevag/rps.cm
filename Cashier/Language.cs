﻿using Infralution.Localization.Wpf;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Data;
using Rps.ShareBase;
using CommonClassLibrary;

namespace Cashier
{
    public partial class MainWindow : Window
    {
        private bool CheckLanguage(string Language)
        {
            //ToLog(2, "Cashier: CheckLanguage: Language="+ Language);
            try
            {
                CultureInfo t = new CultureInfo(Language);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void ChangeLanguage(string Language)
        {
            //ToLog(2, "Cashier: ChangeLanguage: Language=" + Language);
            try
            {
                CultureInfo tmp;
                if (Country != "CN")
                {
                    tmp = new CultureInfo(DefaultLanguage);
                }
                else
                {
                    tmp = new CultureInfo("zh-Hans");
                }

                if (Language != "CN")
                {
                    CultureManager.UICulture = new CultureInfo(Language);
                }
                else
                {
                    CultureManager.UICulture = new CultureInfo("zh-Hans");
                }
                CultureManager.UICulture.NumberFormat.CurrencyDecimalSeparator = tmp.NumberFormat.CurrencyDecimalSeparator;
                if (DefaultLanguage != Language)
                {
                    if (LanguageTimeOut > 0)
                    {
                        LanguageChangeTime = DateTime.Now;
                    }
                }

                if (LanguagePanel.Visibility == Visibility.Visible)
                    LanguagePanel.Visibility = Visibility.Hidden;
                if (CashierType != 2)
                {
                    //ToLog(2, "Cashier: ChangeLanguage: GenerateLabels");
                    GenerateLabels();
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void SetLanguagesVisibility()
        {
            //ToLog(2, "Cashier: SetLanguagesVisibility");
            try
            {
                string[] L;
                int dlc = 0;
                foreach (DevicesLanguagesT d in DevicesLanguages)
                    if (d.Checked)
                        dlc++;

                int RowNum = 0, ColNum = 1, LangNum = 1, LanguagesCount = 0;

                if (dlc == 0)
                    return;
                LanguagesCount = dlc;

                Button Lang;
                LanguagePanel.Children.Clear();
                /*{
                    L = GetLanguage(DefaultLanguage);
                    Lang = CreateLangButton(L[0], L[1]);
                    Grid.SetRow(Lang, 0);
                    Grid.SetColumn(Lang, 1);
                    Lang.VerticalAlignment = VerticalAlignment.Center;
                    Lang.HorizontalAlignment = HorizontalAlignment.Center;
                    switch (LanguagesCount)
                    {
                        case 1:
                            Grid.SetColumnSpan(Lang, 4);
                            Grid.SetRowSpan(Lang, 2);
                            break;
                        case 2:
                            Grid.SetColumnSpan(Lang, 4);
                            break;
                        case 3:
                            Grid.SetRowSpan(Lang, 2);
                            break;
                    }
                    LanguagePanel.Children.Add(Lang);
                }*/

                for (int i = 0; i < DevicesLanguages.Count; i++)
                {
                    if (DevicesLanguages[i].Checked)
                    {
                        L = GetLanguage(DevicesLanguages[i].Language);
                        Lang = CreateLangButton(L[0], L[1]);

                        if (LangNum > 3)
                            RowNum = 1;
                        ColNum = LangNum - RowNum * 3;
                        if (ColNum > 2)
                            ColNum++;
                        Grid.SetRow(Lang, RowNum);
                        Grid.SetColumn(Lang, ColNum);
                        switch (LanguagesCount)
                        {
                            case 1:
                                Grid.SetColumnSpan(Lang, 4);
                                Grid.SetRowSpan(Lang, 2);
                                break;
                            case 2:
                                if (LangNum == 2)
                                    Grid.SetRow(Lang, 1);
                                Grid.SetColumn(Lang, 1);
                                Grid.SetColumnSpan(Lang, 4);
                                break;
                            case 3:
                                Grid.SetRowSpan(Lang, 2);
                                if (ColNum == 2)
                                    Grid.SetColumnSpan(Lang, 2);
                                break;
                            case 4:
                                if (LangNum == 4)
                                    Grid.SetColumnSpan(Lang, 4);
                                if (ColNum == 2)
                                    Grid.SetColumnSpan(Lang, 2);
                                break;
                            case 5:
                                if (LangNum == 4)
                                {
                                    Grid.SetColumn(Lang, 1);
                                    Grid.SetColumnSpan(Lang, 2);
                                }
                                else if (LangNum == 5)
                                {
                                    Grid.SetColumn(Lang, 3);
                                }
                                if (ColNum == 2)
                                    Grid.SetColumnSpan(Lang, 2);
                                break;
                            default:
                                if (ColNum == 2)
                                    Grid.SetColumnSpan(Lang, 2);
                                break;
                        }
                        LanguagePanel.Children.Add(Lang);
                        LangNum++;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private Button CreateLangButton(string Name, string Language)
        {
            try
            {
                Button Lang = new Button() { Margin = new Thickness(20), UseLayoutRounding = true, BorderBrush = null, Background = Brushes.Transparent, Name = Name };
                Grid newGrid = new Grid() { Margin = new Thickness(5) };
                Lang.Content = newGrid;
                newGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength3});
                newGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength1});
                //Добавим элемент Image в первую строку элемента newGrid.
                System.Windows.Controls.Image newImage = new Image() { Width = 250, Height = 125 };
                newGrid.Children.Add(newImage);
                //Создадим экземпляр класса BitmapImage, пропишем ему путь к ресурсу с картинкой, и установим режим создания BitmapImage.
                newImage.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Flags/" + Name + ".png")) { CreateOptions = BitmapCreateOptions.IgnoreImageCache };
                Grid.SetRow(newImage, 0);
                TextBlock newBlock = new TextBlock() { Text = Language, Foreground = Brushes.White, FontFamily = new FontFamily("DINPro"), FontSize = 26.667, HorizontalAlignment = HorizontalAlignment.Center };
                newGrid.Children.Add(newBlock);
                Grid.SetRow(newBlock, 1);
                return Lang;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return null;
            }
        }
        private string[] GetLanguage(string Name)
        {
            //ToLog(2, "Cashier: GetLanguage: Name=" + Name );
            try
            {
                CultureInfo tmp = CultureManager.UICulture;
                if (Name == "CN")
                    CultureManager.UICulture = new CultureInfo("zh-Hans");
                else
                    CultureManager.UICulture = new CultureInfo(Name);
                string[] Language = new string[2] { Name, Properties.Resources.LanguageName };
                CultureManager.UICulture = tmp;
                return Language;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
                return null;
            }
        }
        private void GetLanguages()
        {
            //ToLog(2, "Cashier: GetLanguages");
            try
            {
                string[] d = Directory.GetDirectories(".");
                for (int i = 0; i < d.Length; i++)
                    d[i] = d[i].Substring(2).ToUpper();
                List<string> L = new List<string>();
                L.Add("EN"); // Default
                for (int i = 0; i < d.Length; i++)
                    if (CheckLanguage(d[i]))
                    {
                        if (d[i] == "EN") // Default
                            continue;
                        else if (d[i] == "IMG")
                            continue;
                        else if (d[i] == "FLAGS")
                            continue;
                        else if (d[i] == "ZH-HANS") // China
                            L.Add("CN");
                        else
                            L.Add(d[i]);
                    }
                L.Sort();

                for (int i = 0; i < L.Count; i++)
                    Languages.Add(GetLanguage(L[i]));
            }
            catch
            {
            }
        }
    }
}