using CommonClassLibrary;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace Cashier
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void DisplaySmalTable()
        {
            //ToLog(2, "Cashier: DisplaySmalTable");
            try
            {
                smallTable = null;

                if (Balance > 0)
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x2B, 0x00));
                    //ColorRectangle.Opacity = 0.2;
                    FormPaymentFreeExit.Visibility = Visibility.Hidden;
                    if (Grid.GetRowSpan(FormAmountDueStatusLabel) != 2)
                        Grid.SetRowSpan(FormAmountDueStatusLabel, 2);
                    smallTableWorkPanel1.RowDefinitions[7].Height = GL0;
                    //AmountSumLabel.Foreground = Brushes.Red;
                }
                else
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xA5, 0x51));
                    //ColorRectangle.Opacity = 0.1;
                    FormPaymentFreeExit.Visibility = Visibility.Visible;
                    if (Grid.GetRowSpan(FormAmountDueStatusLabel) != 1)
                        Grid.SetRowSpan(FormAmountDueStatusLabel, 1);
                    //                    if (terminalStatus != TerminalStatusT.CardReadedMinus)
                    //                        smallTableWorkPanel1.RowDefinitions[7].Height = GL30P;
                }

                if (FormAmountDueStatusLabel.Text != AmountDueStatusLabel)
                    FormAmountDueStatusLabel.Text = AmountDueStatusLabel;

                if (FormAlarmTextLabel.Visibility == Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Hidden;

                if (WorkCancel.Visibility != Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Visible;

                if (CashierType != 2)
                    DoSwitchTSVusual();

                if (RegularGridVisibble && TSExists)
                {
                    if (RegularGrid.Visibility != Visibility.Visible)
                        RegularGrid.Visibility = Visibility.Visible;
                    smallTableWorkPanel1.RowDefinitions[19].Height = GL140P;

                    /*if (TSExists)
                    {
                        if (Nultime3Grid.Visibility != Visibility.Visible)
                            Nultime3Grid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (Nultime3Grid.Visibility != Visibility.Hidden)
                            Nultime3Grid.Visibility = Visibility.Hidden;
                        if (TS1.Visibility == Visibility.Visible)
                            TS1.Visibility = Visibility.Hidden;
                        if (TS2.Visibility == Visibility.Visible)
                            TS2.Visibility = Visibility.Hidden;
                        if (TS3.Visibility == Visibility.Visible)
                            TS3.Visibility = Visibility.Hidden;
                        if (TS4.Visibility == Visibility.Visible)
                            TS4.Visibility = Visibility.Hidden;
                    }*/
                }
                else
                {
                    if (RegularGrid.Visibility != Visibility.Hidden)
                        RegularGrid.Visibility = Visibility.Hidden;
                    if (CrossPayGrid.Visibility == Visibility.Visible)
                        smallTableWorkPanel1.RowDefinitions[19].Height = GL280P;
                    else
                        smallTableWorkPanel1.RowDefinitions[19].Height = GL0;
                }
                if (Paiments != PaimentMethodT.PaimentsBankModule)
                {
                    string s;
                    if (CashAcceptorWork /*&& CashAcceptorNominals.Count > 0*/)
                    #region CashAcceptorWork
                    {
                        if (CashAccepted.Visibility != Visibility.Visible)
                            CashAccepted.Visibility = Visibility.Visible;
                        for (int i = 0; i < 6; i++)
                        {
                            if (i < Banknotes.Count)
                                CashAccepted.ColumnDefinitions[i + 2].Width = GL1Auto;
                            else
                                CashAccepted.ColumnDefinitions[i + 2].Width = GL0;
                        }
                        for (int i = 0; i < Banknotes.Count; i++)
                        {
                            s = " " + Banknotes[i].Value.ToString();
                            if (i < Banknotes.Count - 1)
                                s += ",";
                            switch (i)
                            {
                                case 0:
                                    if (CashNominal1.Text != s)
                                        CashNominal1.Text = s;
                                    break;
                                case 1:
                                    if (CashNominal2.Text != s)
                                        CashNominal2.Text = s;
                                    break;
                                case 2:
                                    if (CashNominal3.Text != s)
                                        CashNominal3.Text = s;
                                    break;
                                case 3:
                                    if (CashNominal4.Text != s)
                                        CashNominal4.Text = s;
                                    break;
                                case 4:
                                    if (CashNominal5.Text != s)
                                        CashNominal5.Text = s;
                                    break;
                                case 5:
                                    if (CashNominal6.Text != s)
                                        CashNominal6.Text = s;
                                    break;
                            }

                            if (CashAcceptorNominals.Contains(Convert.ToDecimal(Banknotes[i].Value)))
                            {
                                switch (i)
                                {
                                    case 0:

                                        IMG_Krestik_Cash1.Visibility = Visibility.Collapsed;
                                        break;
                                    case 1:
                                        IMG_Krestik_Cash2.Visibility = Visibility.Collapsed;
                                        break;
                                    case 2:
                                        IMG_Krestik_Cash3.Visibility = Visibility.Collapsed;
                                        break;
                                    case 3:
                                        IMG_Krestik_Cash4.Visibility = Visibility.Collapsed;
                                        break;
                                    case 4:
                                        IMG_Krestik_Cash5.Visibility = Visibility.Collapsed;
                                        break;
                                    case 5:
                                        IMG_Krestik_Cash6.Visibility = Visibility.Collapsed;
                                        break;
                                }
                            }
                            else
                            {
                                switch (i)
                                {
                                    case 0:
                                        IMG_Krestik_Cash1.Visibility = Visibility.Visible;
                                        break;
                                    case 1:
                                        IMG_Krestik_Cash2.Visibility = Visibility.Visible;
                                        break;
                                    case 2:
                                        IMG_Krestik_Cash3.Visibility = Visibility.Visible;
                                        break;
                                    case 3:
                                        IMG_Krestik_Cash4.Visibility = Visibility.Visible;
                                        break;
                                    case 4:
                                        IMG_Krestik_Cash5.Visibility = Visibility.Visible;
                                        break;
                                    case 5:
                                        IMG_Krestik_Cash6.Visibility = Visibility.Visible;
                                        break;
                                }
                            }
                        }

                    }
                    #endregion
                    else
                    {
                        if (CashAccepted.Visibility != Visibility.Hidden)
                            CashAccepted.Visibility = Visibility.Hidden;
                    }

                    if (CoinAcceptorWork /*&& CoinAcceptorNominals.Count > 0*/)
                    #region CoinAcceptorWork
                    {
                        if (CoinAccepted.Visibility != Visibility.Visible)
                            CoinAccepted.Visibility = Visibility.Visible;

                        int ic = 0;
                        string Value = "";

                        for (int i = 0; i < Coins.Count; i++)
                        {
                            if (Value != cashierForm.Coins[i].Value)
                            {
                                Value = Coins[i].Value;
                                s = " " + Value;
                                if (ic < 5)
                                    s += ",";
                                switch (ic)
                                {
                                    case 0:
                                        if (CoinNominal1.Text != s)
                                            CoinNominal1.Text = s;
                                        break;
                                    case 1:
                                        if (CoinNominal2.Text != s)
                                            CoinNominal2.Text = s;
                                        break;
                                    case 2:
                                        if (CoinNominal3.Text != s)
                                            CoinNominal3.Text = s;
                                        break;
                                    case 3:
                                        if (CoinNominal4.Text != s)
                                            CoinNominal4.Text = s;
                                        break;
                                    case 4:
                                        if (CoinNominal5.Text != s)
                                            CoinNominal5.Text = s;
                                        break;
                                    case 5:
                                        if (CoinNominal6.Text != s)
                                            CoinNominal6.Text = s;
                                        break;
                                }
                                ic++;
                            }
                        }
                        for (int i = 0; i < 6; i++)
                        {
                            if (i < ic)
                                CoinAccepted.ColumnDefinitions[i + 2].Width = GL1Auto;
                            else
                                CoinAccepted.ColumnDefinitions[i + 2].Width = GL0;
                        }
                        ic = 0;
                        for (int i = 0; i < Coins.Count; i++)
                        {
                            if (Value != cashierForm.Coins[i].Value)
                            {
                                Value = Coins[i].Value;
                                if (CoinAcceptorNominals.Contains(Convert.ToDecimal(Value)))
                                {
                                    switch (ic)
                                    {
                                        case 0:
                                            IMG_Krestik_Coin1.Visibility = Visibility.Collapsed;
                                            break;
                                        case 1:
                                            IMG_Krestik_Coin2.Visibility = Visibility.Collapsed;
                                            break;
                                        case 2:
                                            IMG_Krestik_Coin3.Visibility = Visibility.Collapsed;
                                            break;
                                        case 3:
                                            IMG_Krestik_Coin4.Visibility = Visibility.Collapsed;
                                            break;
                                        case 4:
                                            IMG_Krestik_Coin5.Visibility = Visibility.Collapsed;
                                            break;
                                        case 5:
                                            IMG_Krestik_Coin6.Visibility = Visibility.Collapsed;
                                            break;
                                    }
                                }
                                else
                                {
                                    switch (ic)
                                    {
                                        case 0:
                                            IMG_Krestik_Coin1.Visibility = Visibility.Visible;
                                            break;
                                        case 1:
                                            IMG_Krestik_Coin2.Visibility = Visibility.Visible;
                                            break;
                                        case 2:
                                            IMG_Krestik_Coin3.Visibility = Visibility.Visible;
                                            break;
                                        case 3:
                                            IMG_Krestik_Coin4.Visibility = Visibility.Visible;
                                            break;
                                        case 4:
                                            IMG_Krestik_Coin5.Visibility = Visibility.Visible;
                                            break;
                                        case 5:
                                            IMG_Krestik_Coin6.Visibility = Visibility.Visible;
                                            break;
                                    }
                                }
                                ic++;
                            }
                        }
                    }
                    #endregion
                    else
                    {
                        if (CoinAccepted.Visibility != Visibility.Hidden)
                            CoinAccepted.Visibility = Visibility.Hidden;
                    }

                    if (CoinAccepted.Visibility == Visibility.Hidden)
                    {
                        CashPaymentGrid.RowDefinitions[4].Height = GL0;
                    }
                    else
                    {
                        CashPaymentGrid.RowDefinitions[4].Height = GL20S;
                    }
                    if (CashAccepted.Visibility == Visibility.Hidden)
                    {
                        CashPaymentGrid.RowDefinitions[3].Height = GL0;
                    }
                    else
                    {
                        CashPaymentGrid.RowDefinitions[3].Height = GL20S;
                    }

                    if ((CoinAccepted.Visibility == Visibility.Hidden || CashAccepted.Visibility == Visibility.Hidden) &&
                        AdditionSumButton.Visibility == Visibility.Hidden)
                    {
                        BankPaymentGrid.RowDefinitions[4].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[12].Height = GL0;
                    }
                    else
                    {
                        BankPaymentGrid.RowDefinitions[4].Height = GL20S;
                        smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                    }

                }

                if (Paiments != PaimentMethodT.PaimentsCash && BankModuleWork)
                {
                    if (BankPaymentGrid.Visibility != Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Visible;
                    PaymentGrid.ColumnDefinitions[1].Width = GL50S;
                }
                else
                {
                    if (BankPaymentGrid.Visibility != Visibility.Collapsed)
                        BankPaymentGrid.Visibility = Visibility.Collapsed;
                    PaymentGrid.ColumnDefinitions[1].Width = GL0;
                }
                if (Paiments != PaimentMethodT.PaimentsBankModule && (CashAcceptorWork || CoinAcceptorWork))
                {
                    if (CashPaymentGrid.Visibility != Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Visible;
                    PaymentGrid.ColumnDefinitions[0].Width = GL50S;
                }
                else
                {
                    if (CashPaymentGrid.Visibility != Visibility.Collapsed)
                        CashPaymentGrid.Visibility = Visibility.Collapsed;
                    PaymentGrid.ColumnDefinitions[0].Width = GL0;
                }

                if (((Paiments == PaimentMethodT.PaimentsBankModule)
                    || (Paiments == PaimentMethodT.PaimentsCash)
                    || !BankModuleWork
                    || !deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist
                    || (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                    )
                    && (AdditionSumButton.Visibility == Visibility.Visible)
                    )
                    AdditionSumButton.Visibility = Visibility.Hidden;


                if (DeliveryStatusLabel != "")
                {
                    if (DeliveryStatusLabel1.Text != DeliveryStatusLabel + ".")
                        DeliveryStatusLabel1.Text = DeliveryStatusLabel + ".";
                    if (deliveryStatus == DeliveryStatusT.NotDelivery)
                    {
                        if (DeliveryStatusLabel1.Foreground != Brushes.Red)
                            DeliveryStatusLabel1.Foreground = Brushes.Red;
                    }
                    else
                    {
                        if (DeliveryStatusLabel1.Foreground != Brushes.White)
                            DeliveryStatusLabel1.Foreground = Brushes.White;
                    }
                }
                else
                {
                    if (DeliveryStatusLabel1.Text != "")
                        DeliveryStatusLabel1.Text = "";
                }

                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility != Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderPlus()
        {
            //ToLog(2, "Cashier: DisplayCardReaderPlus");
            try
            {
                RegularGridVisibble = true;

                if (CrossPayGrid.Visibility != Visibility.Hidden)
                    CrossPayGrid.Visibility = Visibility.Hidden;

                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                {
                    DisplayBankModulePay();
                    return;
                }
                Balance = AmountDue - CardSum - Paid;

                ValuteLabel.Text = Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country);
                AmountSumLabel.Text = (-Balance).ToString("#,0.##");
                //AmountSumLabel.Foreground = Brushes.Green;

                if (Paiments != PaimentMethodT.PaimentsBankModule)
                {
                    if (CashPaymentGrid.Visibility != Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (CashPaymentGrid.Visibility == Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Hidden;
                }
                if (Paiments != PaimentMethodT.PaimentsCash)
                {
                    if (BankPaymentGrid.Visibility != Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (BankPaymentGrid.Visibility == Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Hidden;
                }

                DisplaySmalTable();

                if (AmountSumPanel.Visibility != Visibility.Visible)
                    AmountSumPanel.Visibility = Visibility.Visible;
                if (Paid > 0)
                {
                    if (EndPaymentGrid.Visibility != Visibility.Visible)
                        EndPaymentGrid.Visibility = Visibility.Visible;
                    smallTableWorkPanel1.RowDefinitions[20].Height = GL20S;
                    EndPaymentLabel.Text = Properties.Resources.ResourceManager.GetString("EndPayment");

                }
                else
                {
                    if (EndPaymentGrid.Visibility != Visibility.Hidden)
                        EndPaymentGrid.Visibility = Visibility.Hidden;
                    smallTableWorkPanel1.RowDefinitions[20].Height = GL0;
                }

                if (Paiments == PaimentMethodT.PaimentsCash)
                {
                    if (AdditionSumButton.Visibility == Visibility.Visible)
                        AdditionSumButton.Visibility = Visibility.Hidden;
                }
                else
                {
                    if (BankModuleWork && deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                    {
                        if (AdditionSumButton.Visibility != Visibility.Visible)
                            AdditionSumButton.Visibility = Visibility.Visible;
                    }
                }
                //smallTableWorkPanel1.RowDefinitions[1].Height = GL27P;
                smallTableWorkPanel1.RowDefinitions[5].Height = GL1P;
                smallTableWorkPanel1.RowDefinitions[6].Height = GL29P;
                smallTableWorkPanel1.RowDefinitions[7].Height = GL30P;
                smallTableWorkPanel1.RowDefinitions[8].Height = GL29P;
                smallTableWorkPanel1.RowDefinitions[9].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[10].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[11].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[13].Height = GL20P;
                smallTableWorkPanel1.RowDefinitions[14].Height = GL1P;
                smallTableWorkPanel1.RowDefinitions[15].Height = GL10P;
                smallTableWorkPanel1.RowDefinitions[16].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[17].Height = GL15P;

            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderMinus()
        {
            //ToLog(2, "Cashier: DisplayCardReaderMinus");
            try
            {
                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                {
                    DisplayBankModulePay();
                    return;
                }


                Balance = AmountDue - CardSum - Paid;

                string vl = "ValuteLabel" + Country;
                ValuteLabel.Text = Properties.Resources.ResourceManager.GetString(vl);
                AmountSumLabel.Text = (-Balance).ToString("#,0.##");

                if (CrossPay == crossPay.No)
                {
                    if (CrossPayGrid.Visibility != Visibility.Hidden)
                        CrossPayGrid.Visibility = Visibility.Hidden;
                }
                //if (ClientType > 0)
                {
                    if ((CardSum - AmountDue + Paid) < 0)
                    {
                        RegularGridVisibble = true;
                        //smallTableWorkPanel1.RowDefinitions[1].Height = GL27P;
                        smallTableWorkPanel1.RowDefinitions[5].Height = GL1P;
                        smallTableWorkPanel1.RowDefinitions[6].Height = GL29P;
                        smallTableWorkPanel1.RowDefinitions[7].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[8].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[9].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[10].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[11].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[13].Height = GL20P;
                        smallTableWorkPanel1.RowDefinitions[14].Height = GL1P;
                        smallTableWorkPanel1.RowDefinitions[15].Height = GL10P;
                        smallTableWorkPanel1.RowDefinitions[16].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[17].Height = GL15P;
                        //smallTableWorkPanel1.RowDefinitions[19].Height = GL15P;
                        if (EndPaymentGrid.Visibility != Visibility.Hidden)
                            EndPaymentGrid.Visibility = Visibility.Hidden;
                        smallTableWorkPanel1.RowDefinitions[20].Height = GL0;

                    }
                    else if (((CurrentSumm == 0) &&
                        ((CardSum - AmountDue + Paid) > 0) &&
                        (CrossPay == crossPay.No)) || (CrossPay == crossPay.First))
                    {
                        RegularGridVisibble = false;
                        if (PaymentGrid.Visibility != Visibility.Hidden)
                            PaymentGrid.Visibility = Visibility.Hidden;
                        // ���������� ������ ����� 0 - ���������� ��������� �� ������
                        CrossPay = crossPay.First;
                        CurrentSumm = CardSum - AmountDue + Paid;

                        AmountSumLabel.Text = "0";

                        //smallTableWorkPanel1.RowDefinitions[1].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[5].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[6].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[7].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[8].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[9].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[10].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[11].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[12].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[13].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[14].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[15].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[16].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[17].Height = GL0;
                        smallTableWorkPanel1.RowDefinitions[20].Height = GL0;
                        if (CrossPayGrid.Visibility != Visibility.Visible)
                            CrossPayGrid.Visibility = Visibility.Visible;
                        smallTableWorkPanel1.RowDefinitions[19].Height = GL280P;

                        EndPaymentDeliveryLabel1.Text = string.Format(Properties.Resources.EndPaymentDeliveryLabel1, CurrentSumm.ToString("#,0.##"), Properties.Resources.ResourceManager.GetString(vl));
                        EndPaymentDeliveryLabel2.Text = string.Format(Properties.Resources.EndPaymentDeliveryLabel2, CurrentSumm.ToString("#,0.##"), Properties.Resources.ResourceManager.GetString(vl));
                        EndPaymentToCardLabel1.Text = string.Format(Properties.Resources.EndPaymentToCardLabel1, CurrentSumm.ToString("#,0.##"), Properties.Resources.ResourceManager.GetString(vl));
                        EndPaymentToCardLabel2.Text = string.Format(Properties.Resources.EndPaymentToCardLabel2, CurrentSumm.ToString("#,0.##"), Properties.Resources.ResourceManager.GetString(vl));
                        EndPaymentContinueLabel1.Text = string.Format(Properties.Resources.EndPaymentContinueLabel1);
                        EndPaymentContinueLabel2.Text = string.Format(Properties.Resources.EndPaymentContinueLabel2);
                        if (EndPaymentGrid.Visibility != Visibility.Hidden)
                            EndPaymentGrid.Visibility = Visibility.Hidden;
                    }
                    else if ((CrossPay == crossPay.Last) ||
                        ((CardSum - AmountDue + Paid) == 0) ||
                        (
                        ((CardSum - AmountDue + Paid) > 0) &&
                        (CrossPay == crossPay.First) &&
                        (CurrentSumm < CardSum - AmountDue + Paid)
                        )
                        )
                    {
                        RegularGridVisibble = true;
                        if (CrossPay != crossPay.Last)
                            CrossPay = crossPay.Last;
                        if (PaymentGrid.Visibility != Visibility.Visible)
                            PaymentGrid.Visibility = Visibility.Visible;
                        //smallTableWorkPanel1.RowDefinitions[1].Height = GL27P;
                        smallTableWorkPanel1.RowDefinitions[5].Height = GL1P;
                        smallTableWorkPanel1.RowDefinitions[6].Height = GL29P;
                        //smallTableWorkPanel1.RowDefinitions[7].Height = GL30P;
                        smallTableWorkPanel1.RowDefinitions[8].Height = GL29P;
                        smallTableWorkPanel1.RowDefinitions[9].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[10].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[11].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[13].Height = GL20P;
                        smallTableWorkPanel1.RowDefinitions[14].Height = GL1P;
                        smallTableWorkPanel1.RowDefinitions[15].Height = GL10P;
                        smallTableWorkPanel1.RowDefinitions[16].Height = GL1Auto;
                        smallTableWorkPanel1.RowDefinitions[17].Height = GL15P;
                        //smallTableWorkPanel1.RowDefinitions[19].Height = GL15P;
                        // ���������� ������ 0 ��� ����� �������� ����� 0 ��� ������� - ���������� ��������� ��� �����
                        if (CrossPayGrid.Visibility != Visibility.Hidden)
                            CrossPayGrid.Visibility = Visibility.Hidden;
                        deliveryStatus = DeliveryStatusT.NotDelivery;

                        DeliveryStatusLabel = Properties.Resources.Attention + " " + Properties.Resources.WithoutDelivery;

                        if (EndPaymentGrid.Visibility != Visibility.Visible)
                            EndPaymentGrid.Visibility = Visibility.Visible;
                        smallTableWorkPanel1.RowDefinitions[20].Height = GL20S;
                        EndPaymentLabel.Text = Properties.Resources.ResourceManager.GetString("EndPayment");

                        /*if (FormPaymentStatusLabel.Text != (PaymentStatusLabel1 + ". " + DeliveryStatusLabel + "."))
                            FormPaymentStatusLabel.Text = PaymentStatusLabel1 + ". " + DeliveryStatusLabel + ".";*/
                    }
                    else
                    {
                        if (EndPaymentGrid.Visibility != Visibility.Visible)
                            EndPaymentGrid.Visibility = Visibility.Visible;
                        smallTableWorkPanel1.RowDefinitions[20].Height = GL20S;
                        EndPaymentLabel.Text = Properties.Resources.ResourceManager.GetString("EndPayment");
                    }
                }
                /*else
                {
                    if (PaymentGrid.Visibility != Visibility.Visible)
                        PaymentGrid.Visibility = Visibility.Visible;
                    //smallTableWorkPanel1.RowDefinitions[1].Height = GL27P;
                    smallTableWorkPanel1.RowDefinitions[5].Height = GL1P;
                    smallTableWorkPanel1.RowDefinitions[6].Height = GL29P;
                    smallTableWorkPanel1.RowDefinitions[7].Height = GL0;
                    smallTableWorkPanel1.RowDefinitions[8].Height = GL0;
                    smallTableWorkPanel1.RowDefinitions[9].Height = GL1Auto;
                    smallTableWorkPanel1.RowDefinitions[10].Height = GL1Auto;
                    smallTableWorkPanel1.RowDefinitions[11].Height = GL1Auto;
                    smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                    smallTableWorkPanel1.RowDefinitions[13].Height = GL20P;
                    smallTableWorkPanel1.RowDefinitions[14].Height = GL1P;
                    smallTableWorkPanel1.RowDefinitions[15].Height = GL10P;
                    smallTableWorkPanel1.RowDefinitions[16].Height = GL1Auto;
                    smallTableWorkPanel1.RowDefinitions[17].Height = GL15P;
                    //smallTableWorkPanel1.RowDefinitions[19].Height = GL15P;
                    {
                        if (EndPayment.Visibility == Visibility.Visible)
                            EndPayment.Visibility = Visibility.Hidden;
                        smallTableWorkPanel1.RowDefinitions[20].Height = GL0;
                    }
                }*/
                if (DeliveryStatusLabel != "")
                {
                    /*if (FormPaymentStatusLabel.Text != (PaymentStatusLabel1 + ". " + DeliveryStatusLabel + "."))
                        FormPaymentStatusLabel.Text = PaymentStatusLabel1 + ". " + DeliveryStatusLabel + ".";*/
                }
                /*if (FormPaymentStatusLabel.Visibility != Visibility.Visible)
                    FormPaymentStatusLabel.Visibility = Visibility.Visible;*/

                if (Paiments != PaimentMethodT.PaimentsBankModule)
                {
                    if (CashPaymentGrid.Visibility != Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (CashPaymentGrid.Visibility == Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Hidden;
                }
                if (Paiments != PaimentMethodT.PaimentsCash)
                {
                    if (BankPaymentGrid.Visibility != Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (BankPaymentGrid.Visibility == Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Hidden;
                }

                DisplaySmalTable();

                if (AmountSumPanel.Visibility != Visibility.Visible)
                    AmountSumPanel.Visibility = Visibility.Visible;

                if ((ClientType > 1) && EndPaymentEnabled)
                    if (EndPayment.Visibility != Visibility.Visible)
                    {
                        EndPayment.Visibility = Visibility.Visible;
                        EndPaymentLabel.Text = Properties.Resources.ResourceManager.GetString("EndPayment");
                    }
                    else
                    {
                        if (EndPayment.Visibility != Visibility.Hidden)
                            EndPayment.Visibility = Visibility.Hidden;
                    }

                if (Paiments == PaimentMethodT.PaimentsCash)
                {
                    if (AdditionSumButton.Visibility != Visibility.Visible)
                        AdditionSumButton.Visibility = Visibility.Hidden;
                }
                else
                {
                    if (BankModuleWork && deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist && (BankModuleStatus == BankModuleStatusT.BankCardEmpty))
                    {
                        if (AdditionSumButton.Visibility != Visibility.Visible)
                            AdditionSumButton.Visibility = Visibility.Visible;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderPenalCardRequest()
        {
            //ToLog(2, "Cashier: DisplayCardReaderPenalCardRequest");
            try
            {
                if (CrossPayGrid.Visibility != Visibility.Hidden)
                    CrossPayGrid.Visibility = Visibility.Hidden;
                if (EndPaymentGrid.Visibility != Visibility.Hidden)
                    EndPaymentGrid.Visibility = Visibility.Hidden;

                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                {
                    DisplayBankModulePay();
                    return;
                }

                Balance = AmountDue - CardSum - Paid;

                string vl = "ValuteLabel" + Country;
                ValuteLabel.Text = Properties.Resources.ResourceManager.GetString(vl);
                AmountSumLabel.Text = (-Balance).ToString("#,0.##");
                if (Balance > 0)
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x2B, 0x00));
                    ColorRectangle.Opacity = 0.2;
                    FormPaymentFreeExit.Visibility = Visibility.Hidden;
                    //AmountSumLabel.Foreground = Brushes.Red;
                }
                else
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xA5, 0x51));
                    ColorRectangle.Opacity = 0.1;
                    FormPaymentFreeExit.Visibility = Visibility.Visible;
                    //AmountSumLabel.Foreground = Brushes.Green;
                }

                if (Paiments != PaimentMethodT.PaimentsBankModule)
                {
                    if (CashPaymentGrid.Visibility != Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (CashPaymentGrid.Visibility == Visibility.Visible)
                        CashPaymentGrid.Visibility = Visibility.Hidden;
                }
                if (Paiments != PaimentMethodT.PaimentsCash)
                {
                    if (BankPaymentGrid.Visibility != Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (BankPaymentGrid.Visibility == Visibility.Visible)
                        BankPaymentGrid.Visibility = Visibility.Hidden;
                }
                if (AmountSumPanel.Visibility != Visibility.Visible)
                    AmountSumPanel.Visibility = Visibility.Visible;

                DisplaySmalTable();

                if (AdditionSumButton.Visibility == Visibility.Visible)
                    AdditionSumButton.Visibility = Visibility.Hidden;
                //smallTableWorkPanel1.RowDefinitions[1].Height = GL27P;
                smallTableWorkPanel1.RowDefinitions[5].Height = GL1P;
                smallTableWorkPanel1.RowDefinitions[7].Height = GL0;
                smallTableWorkPanel1.RowDefinitions[8].Height = GL0;
                smallTableWorkPanel1.RowDefinitions[9].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[10].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[11].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[12].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[13].Height = GL20P;
                smallTableWorkPanel1.RowDefinitions[14].Height = GL1P;
                smallTableWorkPanel1.RowDefinitions[15].Height = GL10P;
                smallTableWorkPanel1.RowDefinitions[16].Height = GL1Auto;
                smallTableWorkPanel1.RowDefinitions[17].Height = GL15P;
                //smallTableWorkPanel1.RowDefinitions[19].Height = GL15P;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        /*private void DisplayCardReaderPenalCardPaiment()
        {
            ToLog(2, "Cashier: DisplayCardReaderPenalCardPaiment");
            try
            {
                if (BankModuleStatus != BankModuleStatusT.BankCardEmpty)
                {
                    DisplayBankModulePay();
                    return;
                }

                decimal Balance = AmountDue - CardSum - Paid;

                string vl = "ValuteLabel" + Country;
                ValuteLabel.Text = Properties.Resources.ResourceManager.GetString(vl);
                AmountSumLabel.Text = (-Balance).ToString("#,0.##");
                if (Balance > 0)
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x2B, 0x00));
                    ColorRectangle.Opacity = 0.2;
                    FormPaymentFreeExit.Visibility = Visibility.Hidden;
                    //AmountSumLabel.Foreground = Brushes.Red;
                }
                else
                {
                    ColorRectangle.Fill = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xA5, 0x51));
                    ColorRectangle.Opacity = 0.1;
                    FormPaymentFreeExit.Visibility = Visibility.Visible;
                    //AmountSumLabel.Foreground = Brushes.Green;
                }

                if (FormAmountDueStatusLabel.Visibility != Visibility.Visible)
                    FormAmountDueStatusLabel.Visibility = Visibility.Visible;
                if (AmountSumPanel.Visibility != Visibility.Visible)
                    AmountSumPanel.Visibility = Visibility.Visible;
                if (EndPaymentD.Visibility == Visibility.Visible)
                    EndPaymentD.Visibility = Visibility.Hidden;
                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;

                DisplaySmalTable();

                if (FormAddingSummStatusLabel.Visibility == Visibility.Visible)
                    FormAddingSummStatusLabel.Visibility = Visibility.Hidden;
                if (AdditionSumButton.Visibility == Visibility.Visible)
                    AdditionSumButton.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }*/
        private void DisplayCardReaderEmpty()
        {
            //ToLog(2, "Cashier: DisplayCardReaderEmpty");
            try
            {
                smallTable = null;

                if (FormAlarmTextLabel.Visibility == Visibility.Visible)
                {
                    FormAlarmTextLabel.Visibility = Visibility.Hidden;
                }

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;

                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                {
                    TerminalStatusLabel = Properties.Resources.InsertCard;
                    if (FormCardReaderStatusPanel.Visibility == Visibility.Visible)
                        FormCardReaderStatusPanel.Visibility = Visibility.Hidden;
                    FormCardReaderStatusLabel.Content = TerminalStatusLabel;
                    if (FormCardReaderStatusLabel.Visibility != Visibility.Visible)
                        FormCardReaderStatusLabel.Visibility = Visibility.Visible;
                }
                else
                {
                    TerminalStatusLabel = Properties.Resources.CarNumber;
                    FormCardReaderStatusLabel1.Content = TerminalStatusLabel;
                    if (FormCardReaderStatusPanel.Visibility != Visibility.Visible)
                        FormCardReaderStatusPanel.Visibility = Visibility.Visible;
                    if (FormCardReaderStatusLabel.Visibility == Visibility.Visible)
                        FormCardReaderStatusLabel.Visibility = Visibility.Hidden;
                }


                /*if (TechForm.PenalCardTP.IsEnabled)
                {
                    if (PenalCardGrid.Visibility != Visibility.Visible)
                        PenalCardGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    if (PenalCardGrid.Visibility == Visibility.Visible)
                        PenalCardGrid.Visibility = Visibility.Hidden;
                }*/
                if (CashAcceptorWork || CoinAcceptorWork)
                    if (GiveDeliveryThread == null)
                        CheckNominals();

                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;

                if (Paiments != PaimentMethodT.PaimentsBankModule)
                {
                    if (deliveryStatus == DeliveryStatusT.Delivery)
                    {
                        if (Delivery && (CashDispenserWork || Hopper1Work || Hopper2Work))
                        {
                            DeliveryStatusLabel = Properties.Resources.WithDelivery;
                            if (!CashDispenserWork)
                            {
                                DeliveryStatusLabel = Properties.Resources.Attention + " " + Properties.Resources.WithDelivery;
                                DeliveryStatusLabel += " " + Properties.Resources.DeliveryCoin;
                            }
                            if (!Hopper1Work && !Hopper2Work)
                            {
                                DeliveryStatusLabel = Properties.Resources.Attention + " " + Properties.Resources.WithDelivery;
                                DeliveryStatusLabel += " " + Properties.Resources.DeliveryCash;
                            }
                        }
                        else
                        {
                            DeliveryStatusLabel = Properties.Resources.WithoutDelivery;
                            deliveryStatus = DeliveryStatusT.NotDelivery;
                        }
                    }
                    else if (deliveryStatus == DeliveryStatusT.NotDelivery)
                    {
                        DeliveryStatusLabel = Properties.Resources.WithoutDelivery;
                    }
                    else
                        DeliveryStatusLabel = "";

                    if ((CashAcceptorWork && CashAcceptorStatusLabel != "") && (CoinAcceptorWork && CoinAcceptorStatusLabel != ""))
                    {

                        Table3RowBig.PaymentStatusLabel.Text = PaymentStatusLabel /*+ ". "*/ + Environment.NewLine + DeliveryStatusLabel;
                        //if (DeliveryStatusLabel != "")
                        //    Table3RowBig.PaymentStatusLabel.Text += ".";
                        Table3RowBig.CoinAcceptStatusLabel.Text = CoinAcceptorStatusLabel;
                        Table3RowBig.CashAcceptorStatusLabel.Text = CashAcceptorStatusLabel;

                        if (Table2RowsNotesBigGrid.Visibility == Visibility.Visible)
                            Table2RowsNotesBigGrid.Visibility = Visibility.Hidden;
                        if (Table2RowsCoinsBigGrid.Visibility == Visibility.Visible)
                            Table2RowsCoinsBigGrid.Visibility = Visibility.Hidden;
                        if (Table1RowBMBigGrid.Visibility == Visibility.Visible)
                            Table1RowBMBigGrid.Visibility = Visibility.Hidden;
                        // 
                        // Table3RowsBigGrid
                        // 
                        if (Table3RowsBigGrid.Visibility != Visibility.Visible)
                            Table3RowsBigGrid.Visibility = Visibility.Visible;
                    }
                    else if (CashAcceptorWork && CashAcceptorStatusLabel != "")
                    {
                        Table2RowNotesBig.PaymentStatusLabel.Text = PaymentStatusLabel /*+ ". "*/ + Environment.NewLine + DeliveryStatusLabel;
                        /*if (DeliveryStatusLabel != "")
                            Table2RowNotesBig.PaymentStatusLabel.Text += ".";*/

                        Table2RowNotesBig.CashAcceptorStatusLabel.Text = CashAcceptorStatusLabel;

                        if (Table3RowsBigGrid.Visibility == Visibility.Visible)
                            Table3RowsBigGrid.Visibility = Visibility.Hidden;
                        if (Table2RowsCoinsBigGrid.Visibility == Visibility.Visible)
                            Table2RowsCoinsBigGrid.Visibility = Visibility.Hidden;
                        if (Table1RowBMBigGrid.Visibility == Visibility.Visible)
                            Table1RowBMBigGrid.Visibility = Visibility.Hidden;
                        // 
                        // Table2RowsNotesBigGrid
                        // 
                        if (Table2RowsNotesBigGrid.Visibility != Visibility.Visible)
                            Table2RowsNotesBigGrid.Visibility = Visibility.Visible;
                    }
                    else if (CoinAcceptorWork && CoinAcceptorStatusLabel != "")
                    {
                        Table2RowCoinsBig.PaymentStatusLabel.Text = PaymentStatusLabel /*+ ". "*/ + Environment.NewLine + DeliveryStatusLabel /*+ "."*/;
                        //if (DeliveryStatusLabel != "")
                        //    Table2RowCoinsBig.PaymentStatusLabel.Text += ".";
                        Table2RowCoinsBig.CoinAcceptStatusLabel.Text = CoinAcceptorStatusLabel;

                        if (Table3RowsBigGrid.Visibility == Visibility.Visible)
                            Table3RowsBigGrid.Visibility = Visibility.Hidden;
                        if (Table2RowsNotesBigGrid.Visibility == Visibility.Visible)
                            Table2RowsNotesBigGrid.Visibility = Visibility.Hidden;
                        if (Table1RowBMBigGrid.Visibility == Visibility.Visible)
                            Table1RowBMBigGrid.Visibility = Visibility.Hidden;
                        // 
                        // Table2RowsCoinsBig
                        // 
                        if (Table2RowsCoinsBigGrid.Visibility != Visibility.Visible)
                            Table2RowsCoinsBigGrid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        Table1RowBMBig.PaymentStatusLabel.Text = PaymentStatusLabel /*+ ". "*/ + Environment.NewLine + DeliveryStatusLabel /*+ "."*/;
                        //if (DeliveryStatusLabel != "")
                        //    Table1RowBMBig.PaymentStatusLabel.Text += ".";

                        if (Table3RowsBigGrid.Visibility == Visibility.Visible)
                            Table3RowsBigGrid.Visibility = Visibility.Hidden;
                        if (Table2RowsNotesBigGrid.Visibility == Visibility.Visible)
                            Table2RowsNotesBigGrid.Visibility = Visibility.Hidden;
                        if (Table2RowsCoinsBigGrid.Visibility == Visibility.Visible)
                            Table2RowsCoinsBigGrid.Visibility = Visibility.Hidden;
                        // 
                        // Table1RowBMBigGrid
                        // 
                        if (Table1RowBMBigGrid.Visibility != Visibility.Visible)
                            Table1RowBMBigGrid.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    Table1RowBMBig.PaymentStatusLabel.Text = PaymentStatusLabel /*+ ". "*/ + Environment.NewLine + DeliveryStatusLabel /*+ "."*/;
                    //if (DeliveryStatusLabel != "")
                    //    Table1RowBMBig.PaymentStatusLabel.Text += ".";

                    if (Table3RowsBigGrid.Visibility == Visibility.Visible)
                        Table3RowsBigGrid.Visibility = Visibility.Hidden;
                    if (Table2RowsNotesBigGrid.Visibility == Visibility.Visible)
                        Table2RowsNotesBigGrid.Visibility = Visibility.Hidden;
                    if (Table2RowsCoinsBigGrid.Visibility == Visibility.Visible)
                        Table2RowsCoinsBigGrid.Visibility = Visibility.Hidden;
                    // 
                    // Table1RowBMBigGrid
                    // 
                    if (Table1RowBMBigGrid.Visibility != Visibility.Visible)
                        Table1RowBMBigGrid.Visibility = Visibility.Visible;
                }

                if (deviceCMModel.CardDispenserExists != null && (bool)deviceCMModel.CardDispenserExists)
                {
                    if ((CardDispenser != null) && CardDispenser.IsOpen &&
                        (CardDispenserStatus.CardEmpty ||
                        CardDispenserStatus.CardPreSendPos ||
                        CardDispenserStatus.CardDespensePos ||
                        CardDispenserStatus.CardAccepting ||
                        CardDispenserStatus.CardDispensing
                        )
                   )
                    {
                        if (PenalCardGrid.Visibility == Visibility.Visible)
                            PenalCardGrid.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (TechForm.PenalCardTP.IsEnabled)
                        {
                            if (PenalCardGrid.Visibility == Visibility.Hidden)
                                PenalCardGrid.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            if (PenalCardGrid.Visibility == Visibility.Visible)
                                PenalCardGrid.Visibility = Visibility.Hidden;
                        }
                    }
                }
                else
                {
                    if (PenalCardGrid.Visibility == Visibility.Visible)
                        PenalCardGrid.Visibility = Visibility.Hidden;
                }
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility != Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Visible;

                /*if (CardDispenserStatus.CardDespensePos)
                {
                    if (EndPayment.Visibility == Visibility.Visible)
                        EndPayment.Visibility = Visibility.Hidden;
                    if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                        smallTableWorkPanel1.Visibility = Visibility.Hidden;
                    if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                        bigTableWorkPanel1.Visibility = Visibility.Hidden;
                    if (WorkCancel.Visibility == Visibility.Visible)
                        WorkCancel.Visibility = Visibility.Hidden;
                    if (PenalCardGrid.Visibility == Visibility.Visible)
                        PenalCardGrid.Visibility = Visibility.Hidden;


                    FormAlarmTextLabel.Text = Properties.Resources.GetParkingCard;

                    if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                        FormAlarmTextLabel.Visibility = Visibility.Visible;
                }*/
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayBankModulePay()
        {
            //ToLog(2, "Cashier: DisplayBankModulePay");
            try
            {
                smallTable = null;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;

                if (BankModuleStatus == BankModuleStatusT.BankCardSuccess)
                    TerminalStatusLabel = Properties.Resources.PaimentsThancs + Environment.NewLine + Properties.Resources.GetBankCard;
                else if (BankModuleStatus == BankModuleStatusT.BankCardNotSuccess)
                {
                    if (BankModule != null && BankModuleResultDescription != "")
                        TerminalStatusLabel = Properties.Resources.BankPaymentNotSuccess + Environment.NewLine + BankModuleResultDescription + Environment.NewLine + Properties.Resources.GetBankCard;
                    else
                        TerminalStatusLabel = Properties.Resources.BankPaymentNotSuccess + Environment.NewLine + Properties.Resources.GetBankCard;
                }
                /*else
                    TerminalStatusLabel = Properties.Resources.BankPayment;*/

                FormAlarmTextLabel.Text = TerminalStatusLabel;
                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderPaid()
        {
            //ToLog(2, "Cashier: DisplayCardReaderPaid");
            try
            {
                smallTable = null;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;

                if (Paid > 0)
                {
                    TerminalStatusLabel = Properties.Resources.PaimentsThancs;
                    if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetParkingCard;
                    if (Paiments != PaimentMethodT.PaimentsBankModule)
                    {
                        if ((CardSum + Paid) > 0)
                        {
                            if (deliveryStatus == DeliveryStatusT.Delivery)
                            {
                                if ((DownDiff - ResDiff) > 0)
                                {
                                    if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                                        TerminalStatusLabel += " " + Properties.Resources.AndDelivery;
                                    else
                                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.Delivery;
                                }
                            }
                        }

                        if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                        {
                            if (Paid > 0)
                                TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                        }
                        else
                        {
                            if (DownDiff > 0)
                                TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                            else
                                TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetCheque;
                        }
                    }
                    else
                    {
                        if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                        {
                            TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                        }
                        else
                        {
                            if (DownDiff > 0)
                                TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                            else
                                TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetCheque;
                        }
                    }
                }
                else
                {
                    if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                    {
                        TerminalStatusLabel = Properties.Resources.GetParkingCard;
                    }
                }

                FormAlarmTextLabel.Text = TerminalStatusLabel;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }

        private void DisplayCardReaderExitWithoutPayment()
        {
            //ToLog(2, "Cashier: DisplayCardReaderExitWithoutPayment");
            try
            {
                smallTable = null;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;


                FormAlarmTextLabel.Text = Properties.Resources.WithoutPayment + " " + Properties.Resources.GetParkingCard;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }

        private void DisplayCardReaderPreCancelled()
        {
            //ToLog(2, "Cashier: DisplayCardReaderPreCancelled");
            try
            {
                smallTable = null;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;

                if (Paid > 0)
                {
                    string addstr = "";
                    DownDiff = Paid;
                    string toLog = null;
                    bool delivery = false;
                    if (!IsPenalCard)
                    {
                        if (
                            (bool)TechForm.CancelRefund.IsChecked &&
                            CheckReturnPossible(DownDiff, ref toLog)
                           )
                            delivery = true;
                        else
                            delivery = false;


                        if (Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            if (delivery)
                            {
                                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                                    addstr += " " + Properties.Resources.AndMoney1;
                                else
                                    addstr += Environment.NewLine + Properties.Resources.Money;
                            }
                            else
                            {
                                if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                                    addstr += " " + Properties.Resources.AndCheque;
                            }
                        }
                        else // PaimentsBankModule
                        {
                            delivery = false;
                            if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                                addstr += " " + Properties.Resources.AndCheque;
                            else
                                addstr += Environment.NewLine + Properties.Resources.GetCheque;
                        }
                    }
                    if ((deliveryStatus == DeliveryStatusT.NotDelivery) && !delivery)
                    {
                        if (IsPenalCard)
                            TerminalStatusLabel = Properties.Resources.CancelOperation + Environment.NewLine + Properties.Resources.PenalCardWrited;
                        else
                            TerminalStatusLabel = Properties.Resources.CancelOperation + Environment.NewLine + Properties.Resources.CardWrited1;
                    }
                    else
                        TerminalStatusLabel = Properties.Resources.CancelOperation;

                    if ((deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist) && !IsPenalCard)
                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetParkingCard;

                    TerminalStatusLabel += addstr;

                    if (!IsPenalCard)
                    {
                    }
                }
                else
                {
                    if (!IsPenalCard)
                    {
                        TerminalStatusLabel = Properties.Resources.CancelOperation;
                        if (deviceCMModel.CardReaderExist != null && (bool)deviceCMModel.CardReaderExist)
                            TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetParkingCard;
                    }
                }

                FormAlarmTextLabel.Text = TerminalStatusLabel;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderCanceled()
        {
            //ToLog(2, "Cashier: DisplayCardReaderCanceled");
            try
            {
                smallTable = null;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;

                /*if (Paid > 0)
                {
                    if (deliveryStatus == DeliveryStatusT.NotDelivery)
                    {
                        if (IsPenalCard)
                            TerminalStatusLabel = Properties.Resources.CancelOperation + Environment.NewLine + Properties.Resources.PenalCardWrited;
                        else
                            TerminalStatusLabel = Properties.Resources.CancelOperation + Environment.NewLine + Properties.Resources.CardWrited1;
                    }
                    else
                        TerminalStatusLabel = Properties.Resources.CancelOperation;

                    if ((TechForm.CardReaderExists.Checked) && !IsPenalCard)
                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetParkingCard;

                    if (!IsPenalCard)
                    {
                        if (Paiments != PaimentMethodT.PaimentsBankModule)
                        {
                            if (deliveryStatus == DeliveryStatusT.Delivery)
                            {
                                if (TechForm.CardReaderExists.Checked)
                                    TerminalStatusLabel += " " + Properties.Resources.AndMoney;
                                else
                                    TerminalStatusLabel += Environment.NewLine + Properties.Resources.Money;
                            }
                            else
                            {
                                if (TechForm.CardReaderExists.Checked)
                                    TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                            }
                        }
                        else // PaimentsBankModule
                        {
                            if (TechForm.CardReaderExists.Checked)
                                TerminalStatusLabel += " " + Properties.Resources.AndCheque;
                            else
                                TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetCheque;
                        }
                    }
                }
                else
                {
                    if (!IsPenalCard)
                    {
                        TerminalStatusLabel = Properties.Resources.CancelOperation;
                        if (TechForm.CardReaderExists.Checked)
                            TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetParkingCard;
                    }
                }*/

                FormAlarmTextLabel.Text = TerminalStatusLabel;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;

                if (Paiments != PaimentMethodT.PaimentsAll)
                {
                    ToLog(2, "Cashier: DisplayCardReaderCanceled: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                    Paiments = PaimentMethodT.PaimentsAll;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DisplayCardReaderError()
        {
            //ToLog(2, "Cashier: DisplayCardReaderError");
            try
            {
                smallTable = null;

                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;

                FormAlarmTextLabel.Text = TerminalStatusLabel;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;

                if (Paiments != PaimentMethodT.PaimentsAll)
                {
                    ToLog(2, "Cashier: DisplayCardReaderError: Paiments changed from " + Paiments.ToString() + " to " + PaimentMethodT.PaimentsAll.ToString());
                    Paiments = PaimentMethodT.PaimentsAll;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }

        private void DisplayPaymentError()
        {
            //ToLog(2, "Cashier: DisplayPaymentError");
            try
            {
                smallTable = null;

                TerminalStatusLabel = Properties.Resources.PaimentsThancs;

                if (EndPayment.Visibility == Visibility.Visible)
                    EndPayment.Visibility = Visibility.Hidden;
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTableWorkPanel1.Visibility = Visibility.Hidden;
                if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    bigTableWorkPanel1.Visibility = Visibility.Hidden;
                if (WorkCancel.Visibility == Visibility.Visible)
                    WorkCancel.Visibility = Visibility.Hidden;
                if (PenalCardGrid.Visibility == Visibility.Visible)
                    PenalCardGrid.Visibility = Visibility.Hidden;


                if (PaymentErrorInfo.DeliveryError)
                {
                    string ss = string.Format(Properties.Resources.DeliveryError1, PaymentErrorInfo.ResDiff.ToString(), Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country));
                    TerminalStatusLabel += Environment.NewLine + ss;
                }
                else
                {
                    if (DownDiff > 0 && deliveryStatus == DeliveryStatusT.Delivery)
                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.Delivery /*+ "."*/;

                    if (PaymentErrorInfo.CardWriteError)
                        TerminalStatusLabel += Environment.NewLine + Properties.Resources.ToLogCardWriteError;
                    else
                    {
                        if (PaymentErrorInfo.ChequeError)
                        {
                            TerminalStatusLabel += Environment.NewLine + Properties.Resources.ChequePrintError;
                        }
                        else
                            TerminalStatusLabel += Environment.NewLine + Properties.Resources.GetCheque;
                    }
                }
                TerminalStatusLabel += Environment.NewLine + Properties.Resources.WaitOperator;

                FormAlarmTextLabel.Text = TerminalStatusLabel;

                if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                    FormAlarmTextLabel.Visibility = Visibility.Visible;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void GenerateLabels()
        {
            //ToLog(2, "Cashier: GenerateLabels");
            try
            {
                string s = "";
                decimal b = CardSum - AmountDue + Paid;
                if (!Delivery)
                    deliveryStatus = DeliveryStatusT.NotDelivery;
                EntranceTimeStatusLabel = Properties.Resources.ToLogEntranceTime + " " + ReaderEntranceTime;
                if (b < 0)
                    AmountDueStatusLabel = Properties.Resources.CardDebt;
                else
                    AmountDueStatusLabel = Properties.Resources.CardBalance;
                if (BankQuerySumm > 0)
                {
                    BanqQueryStatusLabel = Properties.Resources.ResourceManager.GetString("AddingSymm");
                    AddingSummStatusLabel = BankQuerySumm.ToString("#,0.##") + " " + Properties.Resources.ResourceManager.GetString("ValuteLabel" + Country);
                }
                else
                {
                    BanqQueryStatusLabel = Properties.Resources.ResourceManager.GetString("PresToAdd");
                }
                if (CashAcceptorWork || CoinAcceptorWork)
                    s = Properties.Resources.ResourceManager.GetString("Income");

                if (CashAcceptorWork && CoinAcceptorWork)
                {
                    s += " " + Properties.Resources.ResourceManager.GetString("CashIn") + " " + Properties.Resources.ResourceManager.GetString("AndIn") + " " + Properties.Resources.ResourceManager.GetString("CoinIn");
                }
                else if (CashAcceptorWork)
                {
                    s += " " + Properties.Resources.ResourceManager.GetString("CashIn");
                }
                else if (CoinAcceptorWork)
                {
                    s += " " + Properties.Resources.ResourceManager.GetString("CoinIn");
                }

                CashIn.Text = s;
                if (terminalStatus == TerminalStatusT.CardReadedPlus) // ������ ����� >= 0 - � �� �� ��������� ��� ����������� �����
                {
                    if (BankModuleWork)
                    {
                        if (Paiments == PaimentMethodT.PaimentsCash) // ��������
                            PaymentStatusLabel1 = Properties.Resources.CashPlus;
                        else if (Paiments == PaimentMethodT.PaimentsBankModule) // ���������� �����
                            PaymentStatusLabel1 = Properties.Resources.BankPlus;
                        else if (BankQuerySumm > 0) // ��� ������ + ��������� ���. �����
                            PaymentStatusLabel1 = Properties.Resources.CashAndBankPlus;
                        else // ��� ������ + ���. ����� �� ��������� 
                             // ������ ����� > 0 - � �� �� ��������� ��� ����������� �����
                            PaymentStatusLabel1 = Properties.Resources.CashAndBankPlus1;
                    }
                }
                else if (
                    (terminalStatus == TerminalStatusT.CardReadedMinus) || // ����� ����� < 0 - � �� �� ��������� ��������� ������� ����� �� ����� � ����� ������
                    (terminalStatus == TerminalStatusT.Empty) ||
                    (terminalStatus == TerminalStatusT.PenalCardPrePaiment)
                    )
                {
                    if (BankModuleWork && (Paiments != PaimentMethodT.PaimentsCash))
                    {
                        if (Paiments == PaimentMethodT.PaimentsAll && (CoinAcceptorWork || CashAcceptorWork))
                        {
                            PaymentStatusLabel = Properties.Resources.CashAndBank;
                            if (BankQuerySumm > 0) // ��� ������ + � �� ��������� ����� ����� 
                            {
                                PaymentStatusLabel1 = Properties.Resources.CashAndBank1;
                            }
                            else // ��� ������ + ���. ����� �� ���������
                            {
                                PaymentStatusLabel1 = Properties.Resources.Cash1;
                            }
                        }
                        else if (Paiments == PaimentMethodT.PaimentsBankModule)
                        {
                            if (BankQuerySumm > 0) // ��� ������ + � �� ��������� ����� ����� 
                            {
                                PaymentStatusLabel = Properties.Resources.Bank;
                                PaymentStatusLabel1 = Properties.Resources.Bank1;
                            }
                        }
                        else if (CoinAcceptorWork || CashAcceptorWork)
                        {
                            PaymentStatusLabel = Properties.Resources.Cash;
                            PaymentStatusLabel1 = Properties.Resources.Cash1;
                        }
                    }
                    else if (CoinAcceptorWork || CashAcceptorWork) // ��������
                    {
                        PaymentStatusLabel = Properties.Resources.Cash;
                        PaymentStatusLabel1 = Properties.Resources.Cash1;
                    }
                    if (BankModuleStatus == BankModuleStatusT.BankCardSuccess)
                        PaymentStatusLabel = "";
                }


                if (deliveryStatus == DeliveryStatusT.Delivery)
                {
                    if (Delivery && (CashDispenserWork || Hopper1Work || Hopper2Work))
                    {
                        DeliveryStatusLabel = Properties.Resources.WithDelivery;
                        if (!CashDispenserWork)
                        {
                            DeliveryStatusLabel = Properties.Resources.Attention + " " + Properties.Resources.WithDelivery;
                            DeliveryStatusLabel += " " + Properties.Resources.DeliveryCoin;
                        }
                        if (!Hopper1Work && !Hopper2Work)
                        {
                            DeliveryStatusLabel = Properties.Resources.Attention + " " + Properties.Resources.WithDelivery;
                            DeliveryStatusLabel += " " + Properties.Resources.DeliveryCash;
                        }
                    }
                    else
                    {
                        DeliveryStatusLabel = Properties.Resources.WithoutDelivery;
                        deliveryStatus = DeliveryStatusT.NotDelivery;
                    }
                }
                else if (deliveryStatus == DeliveryStatusT.NotDelivery)
                {
                    DeliveryStatusLabel = Properties.Resources.WithoutDelivery;
                }
                else
                    DeliveryStatusLabel = "";

                if ((string)FormCardReaderStatusLabel.Content != TerminalStatusLabel)
                    FormCardReaderStatusLabel.Content = TerminalStatusLabel;

                if (BankQuerySumm > 0)
                {
                    if (AdditionSumButton.MainText.Text != BanqQueryStatusLabel)
                        AdditionSumButton.MainText.Text = BanqQueryStatusLabel;
                    if (Grid.GetColumnSpan(AdditionSumButton.MainText) != 1)
                        Grid.SetColumnSpan(AdditionSumButton.MainText, 1);
                    if (Grid.GetRowSpan(AdditionSumButton.MainText) != 1)
                        Grid.SetRowSpan(AdditionSumButton.MainText, 1);
                    if (AdditionSumButton.SummText.Text != AddingSummStatusLabel)
                        AdditionSumButton.SummText.Text = AddingSummStatusLabel;
                    if (AdditionSumButton.SummText.Visibility != Visibility.Visible)
                        AdditionSumButton.SummText.Visibility = Visibility.Visible;
                    if (AdditionSumButton.ChangeText.Visibility != Visibility.Visible)
                        AdditionSumButton.ChangeText.Visibility = Visibility.Visible;
                }
                else
                {
                    if (AdditionSumButton.MainText.Text != BanqQueryStatusLabel)
                        AdditionSumButton.MainText.Text = BanqQueryStatusLabel;
                    if (Grid.GetColumnSpan(AdditionSumButton.MainText) != 2)
                        Grid.SetColumnSpan(AdditionSumButton.MainText, 2);
                    if (Grid.GetRowSpan(AdditionSumButton.MainText) != 2)
                        Grid.SetRowSpan(AdditionSumButton.MainText, 2);
                    if (AdditionSumButton.SummText.Visibility != Visibility.Hidden)
                        AdditionSumButton.SummText.Visibility = Visibility.Hidden;
                    if (AdditionSumButton.ChangeText.Visibility != Visibility.Hidden)
                        AdditionSumButton.ChangeText.Visibility = Visibility.Hidden;
                }
                lock (TariffPlanTariffScheduleList)
                {
                    if (TSExists)
                    {
                        if (TSRulesCount > 0)
                        {
                            if (b >= TariffPlanTariffScheduleList[0].ConditionAbonementPrice)
                            {
                                if (!TS1.IsEnabled)
                                    TS1.IsEnabled = true;
                            }
                            else
                            {
                                if (TS1.IsEnabled)
                                    TS1.IsEnabled = false;
                            }
                        }
                        if (TSRulesCount > 1)
                        {
                            if (b >= TariffPlanTariffScheduleList[1].ConditionAbonementPrice)
                            {
                                if (!TS2.IsEnabled)
                                    TS2.IsEnabled = true;
                            }
                            else
                            {
                                if (TS2.IsEnabled)
                                    TS2.IsEnabled = false;
                            }
                        }
                        if (TSRulesCount > 2)
                        {
                            if (b >= TariffPlanTariffScheduleList[2].ConditionAbonementPrice)
                            {
                                if (!TS3.IsEnabled)
                                    TS3.IsEnabled = true;
                            }
                            else
                            {
                                if (TS3.IsEnabled)
                                    TS3.IsEnabled = false;
                            }
                        }
                        if (TSRulesCount > 3)
                        {
                            if (b >= TariffPlanTariffScheduleList[3].ConditionAbonementPrice)
                            {
                                if (!TS4.IsEnabled)
                                    TS4.IsEnabled = true;
                            }
                            else
                            {
                                if (TS4.IsEnabled)
                                    TS4.IsEnabled = false;
                            }
                        }
                        if (TSRulesCount > 4)
                        {
                            if (b >= TariffPlanTariffScheduleList[4].ConditionAbonementPrice)
                            {
                                if (!TS5.IsEnabled)
                                    TS5.IsEnabled = true;
                            }
                            else
                            {
                                if (TS5.IsEnabled)
                                    TS5.IsEnabled = false;
                            }
                        }
                        if (TSRulesCount > 5)
                        {
                            if (b >= TariffPlanTariffScheduleList[5].ConditionAbonementPrice)
                            {
                                if (!TS6.IsEnabled)
                                    TS6.IsEnabled = true;
                            }
                            else
                            {
                                if (TS6.IsEnabled)
                                    TS6.IsEnabled = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void InService()
        {
            //ToLog(2, "Cashier: InService");
            try
            {
                if (Talk != null)
                    Talk.Init();

                if (smallTable != null)
                {
                    if (smallTable == true)
                    {
                        if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                        {
                            bigTableWorkPanel1.Visibility = Visibility.Hidden;
                            DisplayStatusChanged = true;
                        }
                        if (smallTableWorkPanel1.Visibility != Visibility.Visible)
                        {
                            smallTableWorkPanel1.Visibility = Visibility.Visible;
                            DisplayStatusChanged = true;
                        }
                    }
                    else if (smallTable == false)
                    {
                        if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                        {
                            smallTableWorkPanel1.Visibility = Visibility.Hidden;
                            DisplayStatusChanged = true;
                        }
                        if (bigTableWorkPanel1.Visibility != Visibility.Visible)
                        {
                            bigTableWorkPanel1.Visibility = Visibility.Visible;
                            DisplayStatusChanged = true;
                        }
                    }
                    if (FormAlarmTextLabel.Visibility == Visibility.Visible)
                    {
                        FormAlarmTextLabel.Visibility = Visibility.Hidden;
                    }
                }
                if (ButtonsBar.Visibility != Visibility.Visible)
                {
                    ButtonsBar.Visibility = Visibility.Visible;
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        public void OutOfService()
        {
            //ToLog(2, "Cashier: OutOfService");
            try
            {
                if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    smallTable = true;
                else
                    smallTable = false;

                if (EndPayment.Visibility == Visibility.Visible)
                {
                    EndPayment.Visibility = Visibility.Hidden;
                    DisplayStatusChanged = true;
                }
                if (smallTable == true)
                {
                    if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    {
                        smallTableWorkPanel1.Visibility = Visibility.Hidden;
                        DisplayStatusChanged = true;
                    }
                    if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    {
                        bigTableWorkPanel1.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    if (smallTableWorkPanel1.Visibility == Visibility.Visible)
                    {
                        smallTableWorkPanel1.Visibility = Visibility.Hidden;
                    }
                    if (bigTableWorkPanel1.Visibility == Visibility.Visible)
                    {
                        bigTableWorkPanel1.Visibility = Visibility.Hidden;
                        DisplayStatusChanged = true;
                    }
                }
                if (CasseStatusLabel != "")
                {
                    if (FormAlarmTextLabel.Text != CasseStatusLabel)
                        FormAlarmTextLabel.Text = CasseStatusLabel;
                    if (FormAlarmTextLabel.Visibility != Visibility.Visible)
                        FormAlarmTextLabel.Visibility = Visibility.Visible;
                }
                else
                {
                    if (FormAlarmTextLabel.Visibility != Visibility.Hidden)
                        FormAlarmTextLabel.Visibility = Visibility.Hidden;
                }
                if (ButtonsBar.Visibility == Visibility.Visible)
                    ButtonsBar.Visibility = Visibility.Hidden;
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        private void DoSwitchTSVusual()
        {
            try
            {
                //if (ClientType > 0)
                {
                    if (TSExists)
                    {
                        string Period;
                        if (RegularGrid.Visibility != Visibility.Visible)
                            RegularGrid.Visibility = Visibility.Visible;

                        if (TSRulesCount > 0)
                        {
                            if (TS1.Visibility != Visibility.Visible)
                            {
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS1.Visibility = Visibility.Visible");
                                TS1.Visibility = Visibility.Visible;
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[0].ConditionBackTime, (int)TariffPlanTariffScheduleList[0].ConditionBackTimeTypeId);
                            TS1.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[0].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[0].ConditionAbonementPrice.ToString() + " " + sValuteLabel;
                        }
                        if (TSRulesCount > 1)
                        {
                            if (TS2.Visibility != Visibility.Visible)
                            {
                                TS2.Visibility = Visibility.Visible;
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS2.Visibility = Visibility.Visible");
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[1].ConditionBackTime, (int)TariffPlanTariffScheduleList[1].ConditionBackTimeTypeId);
                            TS2.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[1].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[1].ConditionAbonementPrice.ToString() + " " + sValuteLabel;
                        }
                        if (TSRulesCount > 2)
                        {
                            if (TS3.Visibility != Visibility.Visible)
                            {
                                TS3.Visibility = Visibility.Visible;
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS3.Visibility = Visibility.Visible");
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[2].ConditionBackTime, (int)TariffPlanTariffScheduleList[2].ConditionBackTimeTypeId);
                            TS3.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[2].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[2].ConditionAbonementPrice.ToString() + " " + sValuteLabel;
                        }
                        if (TSRulesCount > 3)
                        {
                            if (TS4.Visibility != Visibility.Visible)
                            {
                                TS4.Visibility = Visibility.Visible;
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS4.Visibility = Visibility.Visible");
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[3].ConditionBackTime, (int)TariffPlanTariffScheduleList[3].ConditionBackTimeTypeId);
                            TS4.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[3].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[3].ConditionAbonementPrice.ToString() + " " + sValuteLabel;
                        }
                        if (TSRulesCount > 4)
                        {
                            if (TS5.Visibility != Visibility.Visible)
                            {
                                TS5.Visibility = Visibility.Visible;
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS5.Visibility = Visibility.Visible");
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[4].ConditionBackTime, (int)TariffPlanTariffScheduleList[4].ConditionBackTimeTypeId);
                            TS5.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[4].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[4].ConditionAbonementPrice.ToString() + " " + sValuteLabel;
                        }
                        if (TSRulesCount > 5)
                        {
                            if (TS6.Visibility != Visibility.Visible)
                            {
                                TS6.Visibility = Visibility.Visible;
                                ToLog(2, "Cashier: DoSwitchTSVusual: TS6.Visibility = Visibility.Visible");
                            }
                            Period = GetPeriodName((int)TariffPlanTariffScheduleList[5].ConditionBackTime, (int)TariffPlanTariffScheduleList[5].ConditionBackTimeTypeId);
                            TS6.ButtonLabel.Content = "  " + TariffPlanTariffScheduleList[5].ConditionBackTime.ToString() + " " + Period + " - " + TariffPlanTariffScheduleList[5].ConditionAbonementPrice.ToString() + " " + sValuteLabel;

                            if (TS6.Visibility != Visibility.Visible)
                                TS6.Visibility = Visibility.Visible;
                        }
                        if (TSRulesCount < 6)
                        {
                            if (TS6.Visibility == Visibility.Visible)
                                TS6.Visibility = Visibility.Hidden;
                        }
                        if (TSRulesCount < 5)
                        {
                            if (TS5.Visibility == Visibility.Visible)
                                TS5.Visibility = Visibility.Hidden;
                        }
                        if (TSRulesCount < 4)
                        {
                            if (TS4.Visibility == Visibility.Visible)
                                TS4.Visibility = Visibility.Hidden;
                        }
                        if (TSRulesCount < 3)
                        {
                            if (TS3.Visibility == Visibility.Visible)
                                TS3.Visibility = Visibility.Hidden;
                        }
                        if (TSRulesCount < 2)
                        {
                            if (TS2.Visibility == Visibility.Visible)
                                TS2.Visibility = Visibility.Hidden;
                        }

                        if (TSRulesCount == 1)
                        {
                            Grid.SetRowSpan(TS1, 2);
                            Grid.SetColumnSpan(TS1, 3);
                            TS1.Margin = new Thickness(5, 0, 5, 0);

                            TS1.ButtonLabel.FontSize = 24;
                        }
                        if (TSRulesCount == 2)
                        {
                            Grid.SetRowSpan(TS1, 1);
                            Grid.SetColumnSpan(TS1, 3);
                            Grid.SetRowSpan(TS2, 1);
                            Grid.SetColumnSpan(TS2, 3);
                            Grid.SetRow(TS2, 1);
                            Grid.SetColumn(TS2, 1);
                            TS1.Margin = new Thickness(5, 0, 5, 5);
                            TS2.Margin = new Thickness(5, 5, 5, 0);

                            TS1.ButtonLabel.FontSize = 24;
                            TS2.ButtonLabel.FontSize = 24;
                        }
                        if (TSRulesCount == 3)
                        {
                            Grid.SetRowSpan(TS1, 2);
                            Grid.SetColumnSpan(TS1, 1);
                            Grid.SetRowSpan(TS2, 2);
                            Grid.SetColumnSpan(TS2, 1);
                            Grid.SetRowSpan(TS3, 2);
                            Grid.SetColumnSpan(TS3, 1);
                            Grid.SetRow(TS2, 0);
                            Grid.SetColumn(TS2, 2);
                            TS1.Margin = new Thickness(5, 0, 5, 0);
                            TS2.Margin = new Thickness(5, 0, 5, 0);
                            TS3.Margin = new Thickness(5, 0, 5, 0);

                            TS1.ButtonLabel.FontSize = 16;
                            TS2.ButtonLabel.FontSize = 16;
                        }
                        if (TSRulesCount == 4)
                        {
                            Grid.SetRowSpan(TS1, 1);
                            Grid.SetColumnSpan(TS1, 1);
                            Grid.SetRowSpan(TS2, 1);
                            Grid.SetColumnSpan(TS2, 1);
                            Grid.SetRowSpan(TS3, 1);
                            Grid.SetColumnSpan(TS3, 1);

                            Grid.SetRow(TS2, 0);
                            Grid.SetColumn(TS2, 2);

                            TS1.Margin = new Thickness(5, 0, 5, 5);
                            TS2.Margin = new Thickness(5, 0, 5, 5);
                            TS3.Margin = new Thickness(5, 0, 5, 5);

                            TS1.ButtonLabel.FontSize = 16;
                            TS2.ButtonLabel.FontSize = 16;
                        }
                        if (TSRulesCount == 5)
                        {
                            Grid.SetRowSpan(TS1, 1);
                            Grid.SetColumnSpan(TS1, 1);
                            Grid.SetRowSpan(TS2, 1);
                            Grid.SetColumnSpan(TS2, 1);
                            Grid.SetRowSpan(TS3, 1);
                            Grid.SetColumnSpan(TS3, 1);

                            Grid.SetRow(TS2, 0);
                            Grid.SetColumn(TS2, 2);

                            TS1.Margin = new Thickness(5, 0, 5, 5);
                            TS2.Margin = new Thickness(5, 0, 5, 5);
                            TS3.Margin = new Thickness(5, 0, 5, 5);

                            TS1.ButtonLabel.FontSize = 16;
                            TS2.ButtonLabel.FontSize = 16;
                        }
                        DateTime Nulltime3 = datetime0 + TimeSpan.FromSeconds(CardInfo.Nulltime3);
                        card.Nulltime3 = Nulltime3;
                        if (Nulltime3 <= datetime0)
                        {
                            Nultime3Status.Content = Properties.Resources.SubscriptionIsntPaid;
                        }
                        else if (Nulltime3 < DateTime.Now)
                        {
                            Nultime3Status.Content = Properties.Resources.SubscriptionExpired + " " + Nulltime3.ToString(d_format);
                        }
                        else
                        {
                            Nultime3Status.Content = Properties.Resources.SubscriptionIsPaid + " " + Nulltime3.ToString(d_format);
                        }
                    }
                    else
                    {
                        if (RegularGrid.Visibility != Visibility.Hidden)
                            RegularGrid.Visibility = Visibility.Hidden;
                        if (TS1.Visibility == Visibility.Visible)
                            TS1.Visibility = Visibility.Hidden;
                        if (TS2.Visibility == Visibility.Visible)
                            TS2.Visibility = Visibility.Hidden;
                        if (TS3.Visibility == Visibility.Visible)
                            TS3.Visibility = Visibility.Hidden;
                        if (TS4.Visibility == Visibility.Visible)
                            TS4.Visibility = Visibility.Hidden;
                        if (TS5.Visibility == Visibility.Visible)
                            TS5.Visibility = Visibility.Hidden;
                        if (TS6.Visibility == Visibility.Visible)
                            TS6.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }

    }
}
