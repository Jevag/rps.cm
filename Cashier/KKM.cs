﻿using CommonClassLibrary;
using System;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using Communications;

namespace Cashier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime KKMAlarmTime;
        void HardKKM()
        {
            try
            {
                if (GiveChequeThread == null)
                {
                    if (KKM != null)
                    {
                        if (!KKM.DeviceEnabled)
                        {
                            LastShiftOpenTime = null;
                            if ((KKMStatusCheckDT == null) || (KKMStatusCheckDT < DateTime.Now - new TimeSpan(0, 0, 5)))
                            {
                                try
                                {
                                    if (KKM.PortNumber != Convert.ToInt16(KKMPort.Substring(3)))
                                        KKM.PortNumber = Convert.ToInt16(KKMPort.Substring(3));
                                }
                                catch { }
                                /*KKM.AccessPassword = TechForm.CommandPassword.Text;
                                KKM.UseAccessPassword = true;
                                KKM.DefaultPassword = TechForm.RegistrationPass.Text;*/
                                ShiftFromKKMChecked = false;
                                ToLog(2, "Cashier: HardKKM: KKM.FR_Connect");
                                KKM.FR_Connect(deviceCMModel.CashierNumber.ToString(), KKMPassword, KKMModePassword);
                                KKMStatusCheckDT = DateTime.Now;
                            }
                        }
                        else if (KKM.Initialised && !KKM.Work)
                        {
                            if (deviceCMModel.ChequeWork== null || deviceCMModel.ChequeWork == 0)
                            KKM.SetPrezenter(0, 0, 0);

                            KKM.GetStatus();

                            if (KKM.ResultCode == CommonKKM.ErrorCode.Sucsess)
                                ShiftFromKKM();
                            /*if (KKM.ResultCode == CommonKKM.ErrorCode.Sucsess)
                                KKM.GetResources();*/
                            KKMResultCode = KKM.ResultCode;
                            KKMResultDescription = KKM.ResultDescription;
                            LastZReport = KKM.Resources.LastZNumber;
                            TerminalStatusChanged = true;
                            KKM.Work = true;
                            KKMWorkChanged = true;
                        }

                        if (KKM.Work)
                        {
                            if ((!ShiftFromKKMChecked) && KKM.Work)
                            {
                                ToLog(2, "Cashier: HardKKM: ShiftFromKKM");
                                ShiftFromKKM();
                                ShiftFromKKMChecked = true;
                            }
                            KKMResultCode = KKM.ResultCode;
                            if (KKM.HardStatus.Error || KKM.HardStatus.PrinterMechError || KKM.HardStatus.PrinterCutMechError
                                || KKM.HardStatus.PrinterFatalError || KKM.HardStatus.PrinterHeadHot)
                            {
                                KKM.SetPrezenter(0, 0, 2);
                                KKMError++;
                            }
                            else
                                KKMError = 0;
                        }
                        if (!KKM.Work || (KKM.CurrentMode != 0) || (KKMError > 3)
                        || KKM.HardStatus.Error
                        || ((KKMResultCode != CommonKKM.ErrorCode.Sucsess) && (KKMResultCode != CommonKKM.ErrorCode.NearPaperEnd)))
                        {
                            KKMResultCode = KKM.ResultCode;
                            KKMWork = false;
                            if ((KKMResultCode == CommonKKM.ErrorCode.NotConnect))
                            {
                                if (KKMLevel != AlarmLevelT.Red)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            if ((KKMResultCode == CommonKKM.ErrorCode.NotConnect) || (KKMResultCode == CommonKKM.ErrorCode.PortAccessError))
                                                Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                                ErrorsText += ResourceManager.GetString("TechKKMCheckReq", DefaultCulture) + " " + ResourceManager.GetString("NotConnect", DefaultCulture) + Environment.NewLine;
                            }
                            else if (((KKMCommError > 3) || (KKMError > 3)))
                            {
                                if (TechForm.KKMPortLabel.Foreground != Brushes.Green)
                                    TechForm.KKMPortLabel.Foreground = Brushes.Green;
                                ErrorsText += ResourceManager.GetString("TechKKMCheckReq", DefaultCulture) + Environment.NewLine;

                                if ((KKM.ResultDescription != null) && (KKM.ResultDescription != ""))
                                    ErrorsText += KKM.ResultDescription + Environment.NewLine;

                                if (KKM.HardStatus.StopByPaperEnd)
                                {
                                    if (KKMLevel != AlarmLevelT.Brown)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        KKMLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.KKM;
                                        Alarm.ErrorCode = AlarmCode.StopByPaperEnd;
                                    }
                                }
                                else if (KKMLevel != AlarmLevelT.Red)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            if (KKMResultCode == CommonKKM.ErrorCode.PortAccessError)
                                                Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                            else if (KKMResultCode == CommonKKM.ErrorCode.IncorrectPass)
                                                Alarm.ErrorCode = AlarmCode.KKMIncorrectPass;
                                            else if (KKMResultCode == CommonKKM.ErrorCode.MaxSessionTime)
                                                Alarm.ErrorCode = AlarmCode.MaxShiftTime;
                                            else if (KKM.HardStatus.PaperEnd)
                                                Alarm.ErrorCode = AlarmCode.PaperEnd;
                                            else if (KKM.HardStatus.CoverOpen)
                                                Alarm.ErrorCode = AlarmCode.CoverOpen;
                                            else if (KKM.HardStatus.PaperTransporByHand)
                                                Alarm.ErrorCode = AlarmCode.PaperTransporByHand;
                                            else if (KKM.HardStatus.PrinterCutMechError)
                                                Alarm.ErrorCode = AlarmCode.PrinterCutMechError;
                                            else if (KKM.HardStatus.PrinterFatalError)
                                                Alarm.ErrorCode = AlarmCode.PrinterFatalError;
                                            else if (KKM.HardStatus.PrinterHeadHot)
                                                Alarm.ErrorCode = AlarmCode.PrinterHeadHot;
                                            else if (KKM.HardStatus.PrinterMechError)
                                                Alarm.ErrorCode = AlarmCode.PrinterMechError;
                                            else
                                                Alarm.ErrorCode = AlarmCode.KKMNotConnect;

                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                                return;
                            }
                            else if (KKM.CurrentMode != 0)
                            {
                                if (terminalStatus != TerminalStatusT.PaymentError)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("TechKKMCheckReq", DefaultCulture) + Environment.NewLine;

                                if (KKMResultCode != CommonKKM.ErrorCode.Sucsess)
                                    ErrorsText += KKMResultDescription + Environment.NewLine;

                                if (KKMLevel != AlarmLevelT.Red)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            AlarmT Alarm = new AlarmT();
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            Alarm.ErrorCode = AlarmCode.KKMModeError;
                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (TechForm.KKMPortLabel.Foreground != Brushes.Red)
                                    TechForm.KKMPortLabel.Foreground = Brushes.Red;
                                if (KKMCommError < 3)
                                    KKMCommError++;
                            }
                        }
                        else
                        {

                            KKMCommError = 0;
                            KKMWork = true;
                            if (KKM.HardStatus.CoverOpen)
                            {
                                if (terminalStatus == TerminalStatusT.Empty)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;

                                ErrorsText += ResourceManager.GetString("KKMCoverOpen", DefaultCulture) + Environment.NewLine;

                                if (KKMLevel != AlarmLevelT.Red)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            KKMWorkChanged = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            Alarm.ErrorCode = AlarmCode.CoverOpen;
                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else if (((KKM.HardStatus.PaperEnd) /*|| (KKM.HardStatus.PaperEnd1)*/) && (terminalStatus == TerminalStatusT.Empty))
                            {
                                if (terminalStatus == TerminalStatusT.Empty)
                                    TerminalWork = TerminalWorkT.TerminalDontWork;
                                ErrorsText += ResourceManager.GetString("KKMPaperOut", DefaultCulture) + Environment.NewLine;
                                if (KKMLevel != AlarmLevelT.Red)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            KKMWorkChanged = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Red;
                                            KKMLevel = AlarmLevelT.Red;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            Alarm.ErrorCode = AlarmCode.PaperEnd;
                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else if ((KKM.HardStatus.PaperNearEnd) /*|| (KKM.HardStatus.PaperNearEnd1)*/)
                            {
                                ErrorsText += ResourceManager.GetString("KKMPaperNearEnd", DefaultCulture) + Environment.NewLine;
                                if (KKMLevel != AlarmLevelT.Yellow)
                                {
                                    if (KKMAlarmTime == DateTime.MinValue)
                                    {
                                        KKMAlarmTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                        {
                                            KKMWorkChanged = true;
                                            AlarmT Alarm = new AlarmT();
                                            Alarm.AlarmLevel = AlarmLevelT.Yellow;
                                            KKMLevel = AlarmLevelT.Yellow;
                                            Alarm.AlarmSource = AlarmSourceT.KKM;
                                            Alarm.ErrorCode = AlarmCode.PaperNearEnd;
                                            SendAlarm(Alarm);
                                            KKMAlarmTime = DateTime.MinValue;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if ((KKMResultCode == CommonKKM.ErrorCode.MaxSessionTime) && deviceCMModel.ShiftAutoClose != null && (bool)deviceCMModel.ShiftAutoClose)
                                {
                                    if (CashierType != 2)
                                    {
                                        ToLog(2, "Cashier: HardKKM: ShiftSwitch");
                                        ShiftSwitch();
                                    }
                                    else
                                    {
                                        TerminalWork = TerminalWorkT.TerminalDontWork;
                                        terminalStatus = TerminalStatusT.ShiftSwitchNeed;
                                        if (terminalStatusOld != terminalStatus)
                                            ToLog(2, "Cashier: HardKKM1: CardReadedPlus terminalStatus changed from " + terminalStatusOld.ToString() + " to " + terminalStatus.ToString());
                                        terminalStatusOld = terminalStatus;
                                    }
                                    return;
                                }

                                if (KKMAlarmTime != DateTime.MinValue)
                                    KKMAlarmTime = DateTime.MinValue;
                                if (KKMLevel != AlarmLevelT.Green)
                                {
                                    KKMWorkChanged = true;
                                    AlarmT Alarm = new AlarmT();
                                    Alarm.AlarmLevel = AlarmLevelT.Green;
                                    Alarm.AlarmSource = AlarmSourceT.KKM;
                                    Alarm.ErrorCode = AlarmCode.DeviceWork;
                                    Alarm.Status = ResourceManager.GetString("KKMWork", DefaultCulture);
                                    SendAlarm(Alarm);
                                    KKMLevel = AlarmLevelT.Green;
                                }
                            }
                        }

                        if (!KKM.Work && (terminalStatus != TerminalStatusT.CardPaid) && (terminalStatus != TerminalStatusT.PaymentError))
                            TerminalWork = TerminalWorkT.TerminalDontWork;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
        bool KKMNotUsing = false;
        private void GetKKMStatus()
        {
            try
            {
                if (deviceCMModel.KKMExists != null && (bool)deviceCMModel.KKMExists)
                {
                    if (EmulatorEnabled && EmulatorForm.Emulator.KKMVirt.Checked)
                    {
                        if ((KKM != null) && KKM.DeviceEnabled)
                        {
                            ToLog(2, "Cashier: GetKKMStatus: KKM.FR_Disconnect");
                            KKM.FR_Disconnect();
                            KKMWorkChanged = true;
                        }
                        if (!EmulatorForm.Emulator.KKMWork.Checked)
                        {
                            KKMWork = false;
                            if (terminalStatus != TerminalStatusT.PaymentError)
                                TerminalWork = TerminalWorkT.TerminalDontWork;
                            ErrorsText += ResourceManager.GetString("TechKKMCheckReq", DefaultCulture) + Environment.NewLine;
                            if (KKMLevel != AlarmLevelT.Brown)
                            {
                                if (KKMAlarmTime == DateTime.MinValue)
                                {
                                    KKMAlarmTime = DateTime.Now;
                                }
                                else
                                {
                                    if (KKMAlarmTime.AddSeconds(3) < DateTime.Now)
                                    {
                                        AlarmT Alarm = new AlarmT();
                                        Alarm.AlarmLevel = AlarmLevelT.Brown;
                                        KKMLevel = AlarmLevelT.Brown;
                                        Alarm.AlarmSource = AlarmSourceT.KKM;
                                        Alarm.ErrorCode = AlarmCode.KKMNotConnect;
                                        //Alarm.Status = ResourceManager.GetString("KKMCommunicationError", DefaultCulture);
                                        SendAlarm(Alarm);
                                        KKMAlarmTime = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        else
                        {

                            KKMWork = true;
                            if (KKMAlarmTime != DateTime.MinValue)
                                KKMAlarmTime = DateTime.MinValue;
                            if (KKMLevel != AlarmLevelT.Green)
                            {
                                AlarmT Alarm = new AlarmT();
                                Alarm.AlarmLevel = AlarmLevelT.Green;
                                Alarm.AlarmSource = AlarmSourceT.KKM;
                                Alarm.ErrorCode = AlarmCode.DeviceWork;
                                Alarm.Status = ResourceManager.GetString("KKMWork", DefaultCulture);
                                SendAlarm(Alarm);
                                KKMLevel = AlarmLevelT.Green;
                            }
                        }
                    }
                    else
                    {
                        if (KKM == null)
                        {
                            try
                            {
                                if (KKMModel == "IskraKKM")
                                {
                                    KKM = new IskraKKM(log);
                                    KKMNotUsing = false;
                                }
                                else if (KKMModel == "PrimFAKKM")
                                {
                                    KKM = new PrimFAKKM(log/*, device.Name*/);
                                    KKMNotUsing = false;
                                }
                                else if (KKMModel == "AtolKKM")
                                {
                                    KKM = new AtolKKM(log);
                                    KKMNotUsing = false;
                                }
                                else
                                {
                                    if (!KKMNotUsing)
                                    {
                                        KKMNotUsing = true;
                                        ToLog(0, KKMModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture));
                                    }
                                        ErrorsText += KKMModel + " " + ResourceManager.GetString("DeviceNotUsing", DefaultCulture) + Environment.NewLine;

                                }
                            }
                            catch (Exception exc) { KKM = null; }
                            KKMWorkChanged = true;
                        }

                        HardKKM();
                    }
                }
                else
                {
                    if ((KKM != null) && KKM.DeviceEnabled)
                    {
                        ToLog(2, "Cashier: GetKKMStatus: KKM.FR_Disconnect");
                        KKM.FR_Disconnect();
                        KKMWorkChanged = true;
                    }
                }
            }
            catch (Exception exc)
            {
                ToLog(0, exc.ToString());
            }
        }
    }
}